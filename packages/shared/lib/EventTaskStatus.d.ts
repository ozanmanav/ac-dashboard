export declare enum EventTaskStatus {
    Cancel = 0,
    Started = 1,
    InProgress = 2,
    Completed = 4
}
//# sourceMappingURL=EventTaskStatus.d.ts.map