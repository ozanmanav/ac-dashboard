var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Subscriber } from "rxjs";
import { map } from "rxjs/operators";
import React, { useCallback, useMemo, createContext, useContext, useState } from "react";
import { ajax } from "rxjs/ajax";
import { isObject } from "util";
import { createAjaxErrorFromAjaxResponse } from "./createAjaxErrorFromAjaxResponse";
import { createAjaxResult } from "./createAjaxResult";
export var tuple = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return args;
};
function jsonToFormData(json) {
    if (json === void 0) { json = {}; }
    var data = new FormData();
    Object.keys(json).forEach(function (key) {
        data.append(key, json[key]);
    });
    return data;
}
var actions = [
    "@@SERVICE/POST",
    "@@SERVICE/GET",
    "@@SERVICE/DELETE",
    "@@SERVICE/PATCH"
];
// const options: () => RequestInit = () => ({
//   mode: "cors",
//   // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
//   credentials: "same-origin",
//   headers: {}
// });
var ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY";
export function createAjaxErrorFromHookEvent(props) {
    return {
        name: "AjaxError",
        headers: "",
        ok: false,
        status: 0,
        statusText: props.json.eventAction,
        message: props.json.message,
        type: props.json.eventName,
        json: props.json.data,
        url: ""
    };
}
export var REQUEST_TIMEOUT_DELAY = 10000;
// const ERRORS = {
//   504: "Request timed out",
//   401: "Not authorised"
// };
// declare interface checkResponse {
//   <T>(response: IAjaxResponse<T>): Result<T>;
// }
function checkResponse(response) {
    return isOkRequest(response.status) && response
        ? createAjaxResult(response)
        : createAjaxErrorFromAjaxResponse(response);
}
function dataMaybe(data) {
    return typeof data === "string" ? data : jsonToFormData(data);
}
function jsonHeaderMaybe(data) {
    return typeof data === "string" ? { "Content-Type": "application/json" } : {};
}
var createAjax = function (input) {
    return ajax(__assign(__assign({}, input), { async: true, crossDomain: true })).pipe(map(checkResponse));
};
function createAjaxServiceMethod(method) {
    return function (_a) {
        var url = _a.endpoint, data = _a.data, headers = _a.headers, _b = _a.responseType, responseType = _b === void 0 ? "json" : _b;
        var observer = isObject(url) && url.progressObserver;
        var progressSubscriber = observer
            ? new Subscriber(observer)
            : undefined;
        return createAjax({
            url: isObject(url) ? url.url : url,
            method: method,
            progressSubscriber: progressSubscriber,
            body: data && dataMaybe(data),
            headers: __assign(__assign({}, jsonHeaderMaybe(data)), headers),
            responseType: responseType
        });
    };
}
export function isOkRequest(status) {
    return status >= 200 && status < 300;
}
export function AjaxAdapter() {
    return {
        get: createAjaxServiceMethod("GET"),
        post: createAjaxServiceMethod("POST"),
        delete: createAjaxServiceMethod("DELETE"),
        patch: createAjaxServiceMethod("PATCH"),
        put: createAjaxServiceMethod("PUT")
    };
}
export function createSubscription(setState) {
    return function (resp) {
        setState(resp);
    };
}
export function createRestServiceAction(action, url, data) {
    return {
        type: action,
        payload: {
            data: data,
            url: url
        }
    };
}
export function createAjaxAdapter() { }
var EmptyToken = {
    access_token: "",
    expires_in: -1,
    token_type: "",
    active_organizationId: "",
    picture: "",
    preferred_username: "",
    sub: "",
    team_ids: ""
};
// createHookConnection(tokendata.sub as string)
// Creates service layer
export function createServiceBoundry(apiUrl, authApiUrl) {
    var ServiceBase = createContext({
        token: EmptyToken,
        updateToken: function () { },
        removeToken: function () { }
    });
    var useServiceContext = function () {
        var _a = useContext(ServiceBase), token = _a.token, updateToken = _a.updateToken, removeToken = _a.removeToken;
        var value = useMemo(function () {
            var createApiConsumer = createRestServiceWrappper(token, apiUrl);
            var createAuthConsumer = createRestServiceWrappper(token, authApiUrl);
            return {
                token: token,
                updateToken: updateToken,
                removeToken: removeToken,
                createApiConsumer: createApiConsumer,
                createAuthConsumer: createAuthConsumer
            };
        }, [removeToken, token, updateToken]);
        return value;
    };
    function ServiceBaseProvider(props) {
        var _a = useState(function () { return (__assign({}, getTokenFromLocalStorage())); }), token = _a[0], updateToken = _a[1];
        var updateFn = useCallback(function (token) {
            updateToken(token);
            token.expires_date = getExpireDate(token.expires_in);
            localStorage.setItem(ACCESS_TOKEN_KEY, JSON.stringify(token));
        }, []);
        var removeFn = useCallback(function () {
            localStorage.clear();
            updateToken(getTokenFromLocalStorage());
        }, []);
        var value = useMemo(function () { return ({
            token: token,
            updateToken: updateFn,
            removeToken: removeFn
        }); }, [removeFn, token, updateFn]);
        return (React.createElement(ServiceBase.Provider, { value: value }, props.children));
    }
    return [useServiceContext, ServiceBaseProvider];
}
// Creates consumer wrapper
export function createRestServiceWrappper(token, baseUrl, ajaxAdapter) {
    if (ajaxAdapter === void 0) { ajaxAdapter = AjaxAdapter(); }
    return function createServiceContext(subUrl) {
        function getUrl(url) {
            return baseUrl + "/" + url;
        }
        return function (props) { return function (progressObserver) {
            var _headers = {};
            if (token) {
                Object.assign(_headers, {
                    Authorization: "Bearer " + token.access_token
                });
            }
            props.headers = __assign(__assign({}, _headers), props.headers);
            var context;
            switch (props.method) {
                case "POST":
                    context = ajaxAdapter.post(__assign(__assign({}, props), { endpoint: {
                            url: getUrl(props.endpoint),
                            progressObserver: progressObserver
                        } }));
                    break;
                case "GET":
                    context = ajaxAdapter.get(__assign(__assign({}, props), { endpoint: {
                            url: getUrl(props.endpoint),
                            progressObserver: progressObserver
                        } }));
                    break;
                case "PUT":
                    context = ajaxAdapter.put(__assign(__assign({}, props), { endpoint: {
                            url: getUrl(props.endpoint),
                            progressObserver: progressObserver
                        } }));
                    break;
                case "PATCH":
                    context = ajaxAdapter.patch(__assign(__assign({}, props), { endpoint: {
                            url: getUrl(props.endpoint),
                            progressObserver: progressObserver
                        } }));
                    break;
                case "DELETE":
                    context = ajaxAdapter.delete(__assign(__assign({}, props), { endpoint: {
                            url: getUrl(props.endpoint),
                            progressObserver: progressObserver
                        } }));
                    break;
                default:
                    throw new TypeError("Invalid method type");
            }
            return context;
        }; };
    };
}
function getExpireDate(expires_in) {
    var curDate = new Date();
    curDate.setSeconds(curDate.getSeconds() + expires_in);
    return curDate;
}
function getTokenFromLocalStorage() {
    try {
        var tokenValue = localStorage.getItem(ACCESS_TOKEN_KEY) || "";
        var token = JSON.parse(tokenValue);
        token.expires_date = new Date(token.expires_date || 1);
        if (new Date(token.expires_date).getTime() > new Date().getTime()) {
            return token;
        }
    }
    catch (error) { }
    return EmptyToken;
}
//# sourceMappingURL=createService.js.map