import { RefObject } from "react";
export declare function useGlobalClickService(excludeTargetClassNames: string[] | null, rootRef: RefObject<HTMLElement>, onOutsideClick: (() => void) | null, type?: "click" | "mousedown"): void;
//# sourceMappingURL=useGlobalClickService.d.ts.map