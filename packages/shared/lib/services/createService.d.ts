import { Observable, PartialObserver } from "rxjs";
import React, { PropsWithChildren } from "react";
import { AjaxResponse } from "rxjs/ajax";
import { TaskFactoryFn } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ApiResultType } from "@appcircle/core/lib/services/ApiResultType";
import { AuthTokenType } from "./AuthTokenType";
import { ReqestMethod } from "@appcircle/core/lib/ReqestMethod";
import { Result } from "@appcircle/core/lib/services/Result";
import { HookResultEvent } from "./HookResultEvent";
import { AjaxResultError } from "./AjaxResultError";
export declare type Lit = string | number | boolean | undefined | null | void | {};
export declare const tuple: <T extends Lit[]>(...args: T) => T;
export declare type HeadersType = {
    [key: string]: any;
    "Content-Type"?: string;
    Authorization?: string;
};
export declare type ResponseTypeof<T> = T extends TypedResponse<infer R> ? R : never;
export declare type ObservableAjaxDispatch<T extends (props: AjaxProps) => Observable<ApiResultType<any>>> = T extends (props: AjaxProps) => infer R ? (props: AjaxProps) => ObservableValueof<Observable<R>> : (props: AjaxProps) => ObservableValueof<any>;
export declare type AjaxMethodProps<TResponse = any> = {
    url: string;
    data: any;
    headers: HeadersType;
    progressObserver: PartialObserver<TResponse>;
};
export declare type AjaxServiceMethodType = <TResponse = any>(props: AjaxProps<TResponse>) => Observable<ApiResultType<TResponse>>;
export declare type ObservableAjax = {
    get: AjaxServiceMethodType;
    post: AjaxServiceMethodType;
    delete: AjaxServiceMethodType;
    patch: AjaxServiceMethodType;
    put: AjaxServiceMethodType;
};
declare type ObservableValueof<T extends Observable<any>> = T extends Observable<TypedResponse<infer R>> ? Observable<R> : Observable<any>;
export declare type ObservableResponse<T> = T extends infer R ? Observable<Result<R>> : Observable<Result<never>>;
export declare type ServiceDispatch<T extends ObservableAjaxDispatch<any>> = T extends ObservableAjaxDispatch<infer T> ? ObservableAjaxDispatch<T> : ObservableAjaxDispatch<any>;
export interface Body<TJson = any> {
    readonly body: ReadableStream<Uint8Array> | null;
    readonly bodyUsed: boolean;
    arrayBuffer(): Promise<ArrayBuffer>;
    blob(): Promise<Blob>;
    formData(): Promise<FormData>;
    json(): Promise<TJson>;
    text(): Promise<string>;
}
export interface TypedResponse<TJson = any> extends Body<TJson> {
    readonly headers: Headers;
    readonly ok: boolean;
    readonly redirected: boolean;
    readonly status: number;
    readonly statusText: string;
    readonly trailer: Promise<Headers>;
    readonly type: ResponseType;
    readonly url: string;
    clone(): TypedResponse;
}
declare const actions: readonly ["@@SERVICE/POST", "@@SERVICE/GET", "@@SERVICE/DELETE", "@@SERVICE/PATCH"];
export declare type ServiceActionType = typeof actions[number];
export interface IAjaxResponse<T> extends AjaxResponse {
    response: ApiResultType<T>;
}
export declare type ApiInnerError = {
    propertyName: string;
    message: string;
    attemptedValue: string;
    code: string;
};
export declare function createAjaxErrorFromHookEvent(props: HookResultEvent): AjaxResultError;
export declare type RequestTimeoutErrorType = {
    status: number;
    error: string;
};
export declare const REQUEST_TIMEOUT_DELAY = 10000;
export declare function isOkRequest(status: number): boolean;
export declare function AjaxAdapter(): ObservableAjax;
export declare type BaseServiceProviderPropsType<TState, TService> = PropsWithChildren<{
    value?: TState;
    token?: any;
    setToken: () => void;
    service?: TService;
}>;
export declare function createSubscription<TResp>(setState: (resp: TResp) => void): (resp: TResp) => void;
export declare type RestServiceAction = {
    type: ServiceActionType;
    payload: {
        url: string;
        data: any;
    };
};
export declare type ServiceProviderPropsType<TState, TService> = PropsWithChildren<{
    initialState?: TState;
    service?: TService;
}>;
export declare function createRestServiceAction(action: ServiceActionType, url: string, data: any): RestServiceAction;
export declare function createAjaxAdapter(): void;
export declare type AjaxProps<TData = any, TResponse = any> = {
    method: ReqestMethod;
    endpoint: string | AjaxMethodProps;
    data?: TData;
    abortController?: AbortController;
    headers?: HeadersType;
    responseType?: "json" | "arraybuffer" | "application/text";
};
export declare type ConsumerFactoryFn = (url?: string) => ServiceFactoryFn;
export declare type ServiceFactoryFn = <TResponse = any, TData = any>(props: AjaxProps<TData, TResponse>) => TaskFactoryFn<TResponse>;
declare type ServiceContextType = {
    token: AuthTokenType;
    updateToken: (token: AuthTokenType) => void;
    removeToken: () => void;
    createApiConsumer: ConsumerFactoryFn;
    createAuthConsumer: ConsumerFactoryFn;
};
declare type ServiceProviderType = (props: {
    children?: React.ReactNode;
}) => JSX.Element;
export declare function createServiceBoundry(apiUrl: string, authApiUrl: string): [() => ServiceContextType, ServiceProviderType];
export declare type RestServiceType = (params: AjaxProps) => Observable<ApiResultType<any>>;
export declare function createRestServiceWrappper<TData = any>(token: AuthTokenType, baseUrl: string, ajaxAdapter?: ObservableAjax): ConsumerFactoryFn;
export {};
//# sourceMappingURL=createService.d.ts.map