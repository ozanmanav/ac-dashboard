import { AjaxResponse } from "rxjs/ajax";
import { Result } from "@appcircle/core/lib/services/Result";
export declare function createAjaxResult<TResponse = any>(props: Partial<AjaxResponse>): Result<TResponse>;
//# sourceMappingURL=createAjaxResult.d.ts.map