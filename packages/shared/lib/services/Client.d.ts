/// <reference types="react" />
import { Subject } from "rxjs";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { HookResultEvent } from "./HookResultEvent";
import { AuthTokenType } from "./AuthTokenType";
import { Message } from "../Module";
import { DisconnectedState } from "./DisconnectedContext";
export declare function createHookServerBoundry(url: string): (token: AuthTokenType, pushMessage: (message: Message) => void, pushEvent: (data: HookEvent<any>) => void, setDisconnected: import("react").Dispatch<import("react").SetStateAction<DisconnectedState>>) => [EventSource, Subject<HookResultEvent>];
//# sourceMappingURL=Client.d.ts.map