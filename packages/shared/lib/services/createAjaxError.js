import { createAjaxErrorFromAjaxResponse } from "./createAjaxErrorFromAjaxResponse";
import { createAjaxErrorFromHookEvent } from "./createService";
export function createAjaxError(props) {
    return props.hasOwnProperty("json")
        ? createAjaxErrorFromHookEvent(props)
        : createAjaxErrorFromAjaxResponse(props);
}
//# sourceMappingURL=createAjaxError.js.map