import { useEffect } from "react";
import { checkMousePointerInElement } from "@appcircle/core/lib/utils/checkMousePointerInElement";
export function useGlobalClickService(excludeTargetClassNames, rootRef, onOutsideClick, type) {
    if (type === void 0) { type = "click"; }
    useEffect(function () {
        if (onOutsideClick === null)
            return;
        var clickHandler = function (e) {
            if ((rootRef.current &&
                ((e.clientX < 0 && e.clientY < 0) ||
                    (e.layerX < 0 && e.layerY < 0) ||
                    (e.target || { tagName: "" }).tagName ===
                        "OPTION" ||
                    (e.target || { tagName: "" }).tagName ===
                        "SELECT" ||
                    ((e.target || { tagName: "" }).tagName ===
                        "INPUT" &&
                        e.target.FileList) ||
                    checkMousePointerInElement(e.clientX, e.clientY, rootRef.current.getBoundingClientRect()))) ||
                (excludeTargetClassNames &&
                    checkIfInExcludeTarget(e.target, excludeTargetClassNames)))
                return;
            onOutsideClick();
        };
        document.addEventListener(type, clickHandler);
        return function () {
            document.removeEventListener(type, clickHandler);
        };
    }, [rootRef, onOutsideClick, excludeTargetClassNames]);
}
function checkIfInExcludeTarget(target, excludeClassNames) {
    if (excludeClassNames.some(function (classanme) { return target.classList.contains(classanme); }))
        return true;
    if (target.parentElement)
        return checkIfInExcludeTarget(target.parentElement, excludeClassNames);
    return false;
}
//# sourceMappingURL=useGlobalClickService.js.map