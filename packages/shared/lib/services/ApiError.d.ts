import { ApiInnerError } from "./createService";
export declare class ApiError {
    code: string | null;
    message: string | null;
    cultureInfo: string | null;
    statusCode: number;
    traceId: string | null;
    innerErrors: ApiInnerError[];
    constructor(props: ApiError);
}
//# sourceMappingURL=ApiError.d.ts.map