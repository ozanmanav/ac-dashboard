var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useState, useMemo, useContext, useCallback, createContext } from "react";
export var ModalContext = createContext({
    DEPRECATED_initialState: null,
    // modalPath: "",
    clear: function () {
        throw Error("Function not implmented yet");
    },
    DEPRECATED_setInitialState: function () {
        throw Error("Function not implmented yet");
    }
});
export var ModalStatusContext = React.createContext({});
export var ModalSetStatusContext = React.createContext(function () { });
export var ActiveModalContext = React.createContext(undefined);
/**
 * @param id Desired Modal's id
 * @return Return setStatus function, active modal id, and desired modal by id
 */
export function useModalStatus(id) {
    var statuses = useContext(ModalStatusContext);
    var setStatus = useContext(ModalSetStatusContext);
    var active = useContext(ActiveModalContext);
    var value = useMemo(function () { return [setStatus, active, id ? statuses[id] : undefined]; }, [
        active,
        setStatus,
        statuses,
        id
    ]);
    return value;
}
function ModalStatusContextProvider(props) {
    var _a = useState(function () { return ({}); }), statuses = _a[0], setStatuses = _a[1];
    var setStatus = useCallback(function (id, status) {
        var _a;
        statuses[id] !== status && setStatuses(__assign(__assign({}, statuses), (_a = {}, _a[id] = status, _a)));
    }, 
    // eslint-disable-next-line
    []);
    var activeModal = useMemo(function () { return Object.keys(statuses).find(function (key) { return statuses[key] === "opened"; }); }, [statuses]);
    return (React.createElement(ModalStatusContext.Provider, { value: statuses },
        React.createElement(ModalSetStatusContext.Provider, { value: setStatus },
            React.createElement(ActiveModalContext.Provider, { value: activeModal }, props.children))));
}
export function ModalContextProvider(props) {
    var _a = useState(), modalCallback = _a[0], setModalCallback_ = _a[1];
    // const [modalPath, setModalPath] = useState<string>("");
    var _b = useState(null), DEPRECATED_initialState = _b[0], DEPRECATED_setInitialState = _b[1];
    var _c = useState({}), statuses = _c[0], setStatuses = _c[1];
    var value = useMemo(function () { return ({
        // get modalPath() {
        //   return modalPath;
        // },
        get DEPRECATED_initialState() {
            return DEPRECATED_initialState;
        },
        setBackButton: function (fn) { },
        setStatuses: function () { },
        statuses: statuses,
        DEPRECATED_setInitialState: DEPRECATED_setInitialState,
        // setModalData: (data: any, closeModal: () => void) =>
        // modalCallback && modalCallback(data, closeModal),
        setModalCallback: function (fn) { return setModalCallback_(function () { return fn; }); },
        clear: function () {
            //TODO: The following functions rerenders the modal component with empty state. This leads to errors.
            //TODO: need a way to solve this
            // setModalCallback_(null);
            // setInitialState(null);
        }
    }); }, [DEPRECATED_initialState, statuses]);
    return (React.createElement(ModalContext.Provider, { value: value },
        React.createElement(ModalStatusContextProvider, null, props.children)));
}
//# sourceMappingURL=ModalContext.js.map