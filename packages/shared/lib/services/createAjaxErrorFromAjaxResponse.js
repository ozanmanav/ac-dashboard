import { ApiAuthError } from "./ApiAuthError";
import { ApiError } from "./ApiError";
import { instanceOfApiAuthError } from "./instanceOfApiAuthError";
import { instanceOfApiError } from "./instanceOfApiError";
export function createAjaxErrorFromAjaxResponse(props) {
    var title = (props.response && props.response.title) || "";
    var statusText = (props.xhr && props.xhr.statusText) || title;
    return {
        name: "AjaxError",
        headers: (props.xhr && props.xhr.getAllResponseHeaders()) || "",
        ok: false,
        error: instanceOfApiError(props.response)
            ? new ApiError(props.response)
            : (instanceOfApiAuthError(props.response) &&
                new ApiAuthError(props.response)) ||
                props.response,
        status: props.status,
        statusText: statusText,
        message: (props.response && props.response.error_description) || title,
        type: props.responseType,
        json: "",
        url: (props.xhr && props.xhr.responseURL) || ""
    };
}
//# sourceMappingURL=createAjaxErrorFromAjaxResponse.js.map