var _a;
import { createServiceBoundry } from "./createService";
export var useServiceContext = (_a = createServiceBoundry(process.env.REACT_APP_API_SERVER ||
    // @ts-ignore
    window.REACT_APP_API_SERVER, process.env.REACT_APP_AUTH_SERVER ||
    // @ts-ignore
    window.REACT_APP_AUTH_SERVER), _a[0]), ServiceBaseProvider = _a[1];
//# sourceMappingURL=useServiceContext.js.map