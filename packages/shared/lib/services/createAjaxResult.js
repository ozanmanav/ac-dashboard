import { isOkRequest } from "./createService";
export function createAjaxResult(props) {
    return {
        headers: props.xhr ? props.xhr.getAllResponseHeaders() : "",
        ok: props.status ? isOkRequest(props.status) : true,
        status: props.status ? props.status : 200,
        statusText: props.xhr ? props.xhr.statusText : "",
        type: props.responseType ? props.responseType : "",
        json: props.response ? props.response : "",
        url: props.xhr ? props.xhr.responseURL : ""
    };
}
//# sourceMappingURL=createAjaxResult.js.map