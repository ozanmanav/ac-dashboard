import { Subject } from "rxjs";
import { createQueryString } from "@appcircle/core/lib/utils/createQueryString";
var publisher$;
var eventSource;
export function createHookServerBoundry(url) {
    return function (token, pushMessage, pushEvent, setDisconnected) {
        function connect() {
            !!eventSource && eventSource.close();
            eventSource = new EventSource(url +
                "/hooks" +
                createQueryString(["id", token.sub], ["organizationId", token.active_organizationId]));
        }
        if (token.sub) {
            connect();
            var failCount_1 = 0;
            eventSource.onmessage = function (e) {
                console.log(e);
            };
            eventSource.addEventListener("hook", function (evt) {
                var data = JSON.parse(evt.data);
                data.taskId &&
                    publisher$.next({
                        ok: data.isSuccess,
                        message: "",
                        json: data
                    });
                data.data && pushEvent(data);
                console.log(data);
            });
            eventSource.onopen = function (e) {
                setDisconnected({
                    isDisconnected: false,
                    isExternalError: true,
                    statusText: "Connected to server."
                });
                pushMessage({
                    messageType: "success",
                    message: "Hooks Connected"
                });
            };
            eventSource.onmessage = function (e) {
                pushMessage({
                    messageType: "general",
                    message: "Hook message. Find out in the console",
                    data: e.data
                });
                console.log("message from hook : ", e.data);
            };
            eventSource.onerror = function (e) {
                failCount_1++;
                setDisconnected({
                    isDisconnected: true,
                    isExternalError: true,
                    statusText: "Cannot connect to server. Please try again momentarily."
                });
                pushMessage({
                    messageType: "error",
                    message: "Hooks Connnection Error"
                });
                if (failCount_1 === 5) {
                    eventSource.close();
                    setTimeout(connect, 5000);
                    failCount_1 = 0;
                }
                console.log("EventSource failed.", "Hooks Connnection Error");
            };
        }
        publisher$ && publisher$.unsubscribe();
        publisher$ = new Subject();
        return [eventSource, publisher$];
    };
}
//# sourceMappingURL=Client.js.map