import { ComponentType, PropsWithChildren } from "react";
export declare function AuthServiceReducer(state: false): false;
export declare function AuthPagesProvider(props: PropsWithChildren<{
    isLoggedIn: boolean;
    ForgotPassword: ComponentType;
    ResetPassword: ComponentType<{
        data: string;
    }>;
    Login: ComponentType<{
        routerState: object;
    }>;
    SignUp: ComponentType;
    clearUserData: () => void;
}>): JSX.Element;
//# sourceMappingURL=AuthPages.d.ts.map