import { AjaxResultError } from "./AjaxResultError";
export declare function getErrorCodeFromAjaxErrorResult(response: AjaxResultError): string;
//# sourceMappingURL=getErrorCodeFromAjaxErrorResult.d.ts.map