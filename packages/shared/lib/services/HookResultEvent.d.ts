import { HookEvent } from "@appcircle/sse/lib/HookEvent";
export declare type HookResultEvent = {
    ok: boolean;
    message: string;
    json: HookEvent;
};
//# sourceMappingURL=HookResultEvent.d.ts.map