import React, { PropsWithChildren } from "react";
export declare type ModalCallback = (data: any, closeModal: () => void) => void;
export declare const ModalContext: React.Context<{
    DEPRECATED_initialState: any;
    DEPRECATED_setInitialState: React.Dispatch<any>;
    setModalData?: ((data: any, closeModal: () => void) => void) | undefined;
    setModalCallback?: ((fn: ModalCallback) => void) | undefined;
    clear: () => void;
}>;
export declare const ModalStatusContext: React.Context<{
    [id: string]: ModalStatusType;
}>;
export declare const ModalSetStatusContext: React.Context<(id: string, statuses: ModalStatusType) => void>;
export declare const ActiveModalContext: React.Context<string | undefined>;
export declare type ModalStatusType = "willOpen" | "willClose" | "closing" | "opening" | "opened" | "closed";
/**
 * @param id Desired Modal's id
 * @return Return setStatus function, active modal id, and desired modal by id
 */
export declare function useModalStatus(id?: string): [(id: string, status: ModalStatusType) => void, string | undefined, string | undefined];
export declare function ModalContextProvider(props: PropsWithChildren<{}>): JSX.Element;
//# sourceMappingURL=ModalContext.d.ts.map