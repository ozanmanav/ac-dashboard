export function instanceOfApiError(obj) {
    return obj && "innerErrors" in obj;
}
//# sourceMappingURL=instanceOfApiError.js.map