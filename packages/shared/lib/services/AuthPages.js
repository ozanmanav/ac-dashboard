import React, { useEffect } from "react";
import { Switch, Route } from "react-router-dom";
// import { store } from "App";
// import { ForgotPassword } from "modules/auth/ForgotPassword";
// import { ResetPassword } from "modules/auth/ResetPassword";
export function AuthServiceReducer(state) {
    return state;
}
export function AuthPagesProvider(props) {
    // const { token } = useContext(AuthServiceContext);
    useEffect(function () {
        //Remove User Info when the access_token is falsy eg. on logout
        if (!props.isLoggedIn) {
            props.clearUserData();
        }
    }, [props.isLoggedIn]);
    return props.isLoggedIn ? (React.createElement(React.Fragment, null, props.children)) : (React.createElement(Switch, null,
        React.createElement(Route, { key: "/signup", path: "/signup", render: function () { return React.createElement(props.SignUp, null); } }),
        React.createElement(Route, { key: "/forgotPassword", path: "/forgotPassword", render: function () { return React.createElement(props.ForgotPassword, null); } }),
        React.createElement(Route, { key: "/resetPassword", path: "/resetPassword", render: function (_a) {
                var location = _a.location;
                return (React.createElement(props.ResetPassword, { data: location.search }));
            } }),
        React.createElement(Route, { key: "/login", path: "/", render: function (_a) {
                var match = _a.match, location = _a.location, history = _a.history;
                return (React.createElement(props.Login, { routerState: location.state }));
            } })));
}
// const RouterWrapper = withRouter(AuthPages);
// export AuthPages as AuthPagesProvider;
// connect((props: AppStateType) => ({
//   isLoggedIn: props.login.isLoggedIn
// }))(RouterWrapper);
//# sourceMappingURL=AuthPages.js.map