import { UserResponse } from "./UserResponse";
export declare type AuthTokenType = {
    access_token: string;
    expires_in: number;
    token_type: string;
    expires_date?: Date;
} & UserResponse;
//# sourceMappingURL=AuthTokenType.d.ts.map