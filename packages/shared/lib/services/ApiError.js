var ApiError = /** @class */ (function () {
    function ApiError(props) {
        this.code = null;
        this.message = null;
        this.cultureInfo = null;
        this.statusCode = 0;
        this.traceId = null;
        this.innerErrors = [];
        Object.assign(this, props);
    }
    return ApiError;
}());
export { ApiError };
//# sourceMappingURL=ApiError.js.map