var ApiAuthError = /** @class */ (function () {
    function ApiAuthError(props) {
        this.error = "";
        this.error_description = "";
        Object.assign(this, props);
    }
    return ApiAuthError;
}());
export { ApiAuthError };
//# sourceMappingURL=ApiAuthError.js.map