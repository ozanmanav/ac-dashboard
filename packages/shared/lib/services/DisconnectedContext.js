import React, { useContext, useState, useMemo, useEffect } from "react";
var DisconnectedDefaultState = {
    isDisconnected: false,
    isExternalError: false,
    statusText: ""
};
var DisconnectContext = React.createContext([
    DisconnectedDefaultState,
    function () { return null; }
]);
export function useDisconnected() {
    var contextValue = useContext(DisconnectContext);
    return contextValue;
}
export function DisconectedProvider(props) {
    var _a = useState(DisconnectedDefaultState), disconnectedState = _a[0], setDisconnected = _a[1];
    var value = useMemo(function () { return [disconnectedState, setDisconnected]; }, [disconnectedState]);
    useEffect(function () {
        var handleInternetConnectionChange = function () {
            var isExternalError = disconnectedState.isExternalError;
            var internetOnline = navigator.onLine;
            setDisconnected &&
                !isExternalError &&
                setDisconnected({
                    isDisconnected: !navigator.onLine,
                    statusText: internetOnline
                        ? "Connected to Internet"
                        : "No network connection. Please check your connection and try again."
                });
        };
        window.addEventListener("online", handleInternetConnectionChange);
        window.addEventListener("offline", handleInternetConnectionChange);
        return function () {
            window.removeEventListener("online", handleInternetConnectionChange);
            window.removeEventListener("offline", handleInternetConnectionChange);
        };
    }, []);
    return (React.createElement(DisconnectContext.Provider, { value: value }, props.children));
}
//# sourceMappingURL=DisconnectedContext.js.map