export function instanceOfApiAuthError(obj) {
    return obj && "error_description" in obj && "error" in obj;
}
//# sourceMappingURL=instanceOfApiAuthError.js.map