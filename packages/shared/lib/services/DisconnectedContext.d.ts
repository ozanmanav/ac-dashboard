import React, { PropsWithChildren } from "react";
export declare type DisconnectedState = {
    isDisconnected: boolean;
    isExternalError?: boolean;
    statusText?: string;
};
declare type DisconnectedValue = [DisconnectedState, React.Dispatch<React.SetStateAction<DisconnectedState>>];
export declare function useDisconnected(): DisconnectedValue;
export declare function DisconectedProvider(props: PropsWithChildren<{}>): JSX.Element;
export {};
//# sourceMappingURL=DisconnectedContext.d.ts.map