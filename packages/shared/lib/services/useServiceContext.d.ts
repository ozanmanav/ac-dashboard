/// <reference types="react" />
export declare const useServiceContext: () => {
    token: import("./AuthTokenType").AuthTokenType;
    updateToken: (token: import("./AuthTokenType").AuthTokenType) => void;
    removeToken: () => void;
    createApiConsumer: import("./createService").ConsumerFactoryFn;
    createAuthConsumer: import("./createService").ConsumerFactoryFn;
}, ServiceBaseProvider: (props: {
    children?: import("react").ReactNode;
}) => JSX.Element;
//# sourceMappingURL=useServiceContext.d.ts.map