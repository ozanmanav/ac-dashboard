export function getErrorCodeFromAjaxErrorResult(response) {
    var error = response.error;
    return ((response && response.error && error.code) ||
        "Error code not matched with any text.");
}
//# sourceMappingURL=getErrorCodeFromAjaxErrorResult.js.map