import { AjaxError } from "rxjs/ajax";
import { HookResultEvent } from "./HookResultEvent";
export declare function createAjaxError(props: HookResultEvent | AjaxError): import("./AjaxResultError").AjaxResultError;
//# sourceMappingURL=createAjaxError.d.ts.map