import { AjaxError } from "rxjs/ajax";
import { ApiAuthError } from "./ApiAuthError";
import { ApiError } from "./ApiError";
import { AjaxResultError } from "./AjaxResultError";
interface IAjaxError extends AjaxError {
    response: ApiError | ApiAuthError | undefined | any;
}
export declare function createAjaxErrorFromAjaxResponse(props: IAjaxError): AjaxResultError;
export {};
//# sourceMappingURL=createAjaxErrorFromAjaxResponse.d.ts.map