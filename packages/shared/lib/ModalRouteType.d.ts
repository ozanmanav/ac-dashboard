import { ModalComponentType } from "./Module";
export declare type ModalRouteType = {
    path: string;
    component: ModalComponentType<any, any, any>;
};
//# sourceMappingURL=ModalRouteType.d.ts.map