import { Context, ComponentType } from "react";
import { Dispatch, AnyAction, Action, Store } from "redux";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { ModalRouteType } from "./ModalRouteType";
export declare type MessageType = "error" | "success" | "general";
export declare type Message = {
    id?: string;
    messageType?: MessageType;
    date?: string;
    message: string;
    messageId?: boolean;
    life?: number;
    isActive?: boolean;
    isNotification?: boolean;
    data?: HookEvent;
    title?: string;
};
export declare type Notification = {
    messageType: MessageType;
    message: string;
};
export declare type ModuleNavBarDataType = {
    header: string;
    items: ModuleNavBarDataItemType[];
    selectedItemIndex: number;
};
export declare type ModuleNavBarDataItemType = {
    length?: number;
    text: string;
    link: string;
    isSelected?: boolean;
};
export declare const InitialModuleContext: ModuleContext;
export declare type servicePost<T = any> = (url: string, data: any) => Promise<T>;
export declare type serviceGet<T = any> = (url: string, data: any) => Promise<T>;
export declare type ModuleProps = {
    context: ModuleContext;
};
export declare type ModuleContext<TStore extends Store = Store<any, any>> = {
    createModuleNavBar: (data: ModuleNavBarDataType) => void;
    getCurrentModuleNavBarData: () => ModuleNavBarDataType;
    registerModalRoutes: (routes: ModalRouteType[]) => void;
    getModulePath(): string;
    createLinkUrl(url: string): string;
    createModalUrl<TProps extends any[] = []>(url: string, ...params: PipeTuple<TProps>[]): string;
    changeModuleNavSelectedIndex(index: number): void;
};
export declare type Module = React.ComponentType<{
    context: ModuleContext;
}>;
export declare type KeyValTupleFrom<T extends any[] = []> = SliceTuple<T>;
declare type SliceTuple<T extends any[], TMaxItem extends number = 2, R extends any[] = [], P extends any[] = [], I extends any[] = []> = {
    0: SliceTuple<T, TMaxItem, R, Prepend<T[Pos<I>], P>, Next<I>>;
    1: Reverse<Prepend<Reverse<P>, R>>;
    2: SliceTuple<T, TMaxItem, Prepend<Reverse<P>, R>, [T[Pos<I>]], Next<I>>;
    3: [];
}[T[0] extends undefined ? 3 : Pos<I> extends Length<T> ? 1 : Length<P> extends TMaxItem ? 2 : 0];
declare type MergeTuple<T extends any[], R extends {
    [key: string]: any;
} = {}, I extends any[] = []> = {
    0: MergeTuple<T, {
        [key in T[Pos<I>][0]]: T[Pos<I>][1];
    } & R, Next<I>>;
    1: R;
}[Pos<I> extends [] ? 1 : Pos<I> extends Length<T> ? 1 : 0];
declare type PipeTuple<T extends any[], R extends any[] = [], I extends any[] = []> = {
    0: PipeTuple<T, T[Pos<I>] | R, Next<I>>;
    1: R;
}[T[0] extends undefined ? 1 : Pos<I> extends [] ? 1 : Pos<I> extends Length<T> ? 1 : 0];
export declare type Iterator<Index extends number = 0, From extends any[] = [], I extends any[] = []> = {
    0: Iterator<Index, Next<From>, Next<I>>;
    1: From;
}[Pos<I> extends Index ? 1 : 0];
export declare type Length<T extends any[]> = T["length"];
export declare type Prepend<E, T extends any[]> = ((head: E, ...args: T) => any) extends (...args: infer U) => any ? U : T;
export declare type Pos<I extends any[]> = Length<I>;
export declare type Reverse<T extends any[], R extends any[] = [], I extends any[] = []> = {
    0: Reverse<T, Prepend<T[Pos<I>], R>, Next<I>>;
    1: R;
}[Pos<I> extends Length<T> ? 1 : 0];
export declare type Concat<T1 extends any[], T2 extends any[]> = Reverse<Reverse<T1> extends infer R ? Cast<R, any[]> : never, T2>;
export declare type Next<I extends any[]> = Prepend<any, I>;
export declare type Cast<X, Y> = X extends Y ? X : Y;
export declare type Append<E, T extends any[]> = Concat<T, [E]>;
export declare type ModalComponentPropsType<TProps = any, TRouteState extends {
    [key: string]: any;
} = any, TRouteParams extends any[] = KeyValTupleFrom<[]>> = {
    modalID?: string;
    initialState?: any;
    onModalClose: () => void;
    onModalData?: (data: any, closeModal: () => {}) => void;
    routeProps: {
        state: TRouteState;
        params: MergeTuple<TRouteParams>;
    };
} & TProps;
export declare type ModalComponentType<TProps = any, TRouteState = any, TRouteParams extends any[] = []> = ComponentType<ModalComponentPropsType<TProps, TRouteState, TRouteParams>>;
export declare type DashboardModule<TContext extends ModuleContext, TProps = any> = {
    Component: (props: TProps) => any;
    Context(ctx: ModuleContext): Context<TContext>;
};
export declare function storeConnect<TStoreState = {}, TProps = {}, TActions extends Action = AnyAction>(stateMap: (state: TStoreState) => TProps, dispathMap: (dispatch: Dispatch<TActions>) => {
    [key: string]: () => TActions;
}, component: any): import("react-redux").ConnectedComponent<any, Pick<unknown, never>>;
export declare type ModuleServiceContextType<T, R> = {
    state: T;
    services: R;
};
export {};
//# sourceMappingURL=Module.d.ts.map