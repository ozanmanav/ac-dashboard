export var EventTaskStatus;
(function (EventTaskStatus) {
    EventTaskStatus[EventTaskStatus["Cancel"] = 0] = "Cancel";
    EventTaskStatus[EventTaskStatus["Started"] = 1] = "Started";
    EventTaskStatus[EventTaskStatus["InProgress"] = 2] = "InProgress";
    EventTaskStatus[EventTaskStatus["Completed"] = 4] = "Completed";
})(EventTaskStatus || (EventTaskStatus = {}));
//# sourceMappingURL=EventTaskStatus.js.map