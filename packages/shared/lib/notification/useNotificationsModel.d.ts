export declare function useNotificationsModel(): {
    getNotifications(): import("../Module").Message[];
    getMessages(): import("../Module").Message[];
    getIsNewNotificationExistInfo(): boolean;
};
//# sourceMappingURL=useNotificationsModel.d.ts.map