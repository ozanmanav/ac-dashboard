import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { Message } from "../Module";
export declare function useNotificationsService(): {
    pushMessage(message: Message): void;
    pushNotification(message: Message): void;
    removeMessage(message: Message): void;
    removeNotification(id: string): void;
    pushEvent(event: HookEvent<any>): void;
    markAsReadAllNotifications(): void;
};
//# sourceMappingURL=useNotificationsService.d.ts.map