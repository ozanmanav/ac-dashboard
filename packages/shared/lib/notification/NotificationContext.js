import React, { createContext, useEffect, useMemo, useReducer } from "react";
import produce from "immer";
import { ClientMessageType } from "../ClientMessageType";
import shortid from "shortid";
var initialNotifications = JSON.parse((window && window.localStorage.getItem("notifications")) || "[]");
export var messageReducer = function (state, action) {
    return produce(state, function (draft) {
        switch (action.type) {
            case "app/message/add":
                var newMessage = action.payload;
                newMessage.id = shortid.generate();
                newMessage.isActive = true;
                newMessage.isNotification && state.hookNotifications.push(newMessage);
                draft.all.push(newMessage);
                break;
            case "app/message/delete":
                draft.all = draft.all.filter(function (item) { return item.id !== action.payload.id; });
                break;
            case "app/notification/delete":
                draft.hookNotifications = draft.hookNotifications.filter(function (notification) { return notification.id !== action.id; });
                break;
            case "app/notifications/load":
                draft.hookNotifications = action.payload;
                break;
            case "app/event/add":
                draft.isNewNotificationExist = true;
                if (action.payload.clientMessageType ===
                    ClientMessageType.InAppNotification)
                    draft.hookNotifications.unshift({
                        id: shortid.generate(),
                        isActive: true,
                        isNotification: true,
                        message: action.payload.message,
                        data: action.payload,
                        title: action.payload.title,
                        date: action.payload.notificationSendDate
                    });
                break;
            case "app/notifications/markAsReadAll":
                draft.isNewNotificationExist = false;
        }
    });
};
var initialState = {
    all: [],
    hookNotifications: initialNotifications,
    isNewNotificationExist: false
};
export var NotificationContext = createContext({
    dispatch: function () { }
});
export var NotificationModelContext = createContext(initialState);
export function NotificationProvider(props) {
    var _a = useReducer(messageReducer, initialState), state = _a[0], dispatch = _a[1];
    var services = useMemo(function () { return ({ state: state, dispatch: dispatch }); }, [state]);
    useEffect(function () {
        props.context.registerModalRoutes([
            {
                path: "app/notifications",
                component: props.modal
            }
        ]);
        // eslint-disable-next-line
    }, []);
    useEffect(function () {
        window &&
            window.localStorage.setItem("notifications", JSON.stringify(state.hookNotifications));
    }, [state.hookNotifications]);
    return (React.createElement(NotificationContext.Provider, { value: services },
        React.createElement(NotificationModelContext.Provider, { value: state }, props.children)));
}
//# sourceMappingURL=NotificationContext.js.map