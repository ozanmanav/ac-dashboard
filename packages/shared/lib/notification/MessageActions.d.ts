import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { Message } from "../Module";
export declare type MessageActions = {
    type: "app/notification/delete";
    id: string;
} | {
    type: "app/message/add" | "app/message/delete";
    payload: Message;
} | {
    type: "app/event/add";
    payload: HookEvent;
} | {
    type: "app/notifications/load";
    payload: Message[];
} | {
    type: "app/notifications/markAsReadAll";
};
//# sourceMappingURL=MessageActions.d.ts.map