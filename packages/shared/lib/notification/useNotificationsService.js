import { useContext, useMemo } from "react";
import { NotificationContext } from "./NotificationContext";
export function useNotificationsService() {
    var notifications = useContext(NotificationContext);
    var service = useMemo(function () { return ({
        pushMessage: function (message) {
            notifications.dispatch({
                type: "app/message/add",
                payload: message
            });
        },
        pushNotification: function (message) {
            notifications.dispatch({
                type: "app/message/add",
                payload: message
            });
        },
        removeMessage: function (message) {
            notifications.dispatch({
                type: "app/message/delete",
                payload: message
            });
        },
        removeNotification: function (id) {
            notifications.dispatch({
                type: "app/notification/delete",
                id: id
            });
        },
        pushEvent: function (event) {
            notifications.dispatch({
                type: "app/event/add",
                payload: event
            });
        },
        markAsReadAllNotifications: function () {
            notifications.dispatch({
                type: "app/notifications/markAsReadAll"
            });
        }
    }); }, 
    // eslint-disable-next-line
    []);
    return service;
}
//# sourceMappingURL=useNotificationsService.js.map