import { useContext, useMemo } from "react";
import { NotificationModelContext } from "./NotificationContext";
export function useNotificationsModel() {
    var state = useContext(NotificationModelContext);
    var model = useMemo(function () { return ({
        getNotifications: function () {
            return state.hookNotifications;
        },
        getMessages: function () {
            return state.all;
        },
        getIsNewNotificationExistInfo: function () {
            return state.isNewNotificationExist;
        }
    }); }, [state.all, state.hookNotifications, state.isNewNotificationExist]);
    return model;
}
//# sourceMappingURL=useNotificationsModel.js.map