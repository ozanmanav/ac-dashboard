import React, { PropsWithChildren, Reducer } from "react";
import { MessageActions } from "./MessageActions";
import { Message, ModalComponentType } from "../Module";
import { ModalRouteType } from "../ModalRouteType";
export declare type MessageState = {
    all: Message[];
    hookNotifications: Message[];
    isNewNotificationExist: boolean;
};
export declare const messageReducer: Reducer<MessageState, MessageActions>;
export declare const NotificationContext: React.Context<{
    dispatch: React.Dispatch<MessageActions>;
}>;
export declare const NotificationModelContext: React.Context<{
    all: Message[];
    hookNotifications: Message[];
    isNewNotificationExist: boolean;
}>;
export declare function NotificationProvider(props: PropsWithChildren<{
    context: {
        registerModalRoutes: (routes: ModalRouteType[]) => void;
    };
    modal: ModalComponentType<any, any, any>;
}>): JSX.Element;
//# sourceMappingURL=NotificationContext.d.ts.map