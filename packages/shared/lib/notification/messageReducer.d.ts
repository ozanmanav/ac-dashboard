import { Message } from "../Module";
export declare type MessageState = {
    all: Message[];
    hookNotifications: Message[];
    isNewNotificationExist: boolean;
};
export declare const MAX_MESSAGES = 10;
//# sourceMappingURL=messageReducer.d.ts.map