export declare function profileSortHelper<T extends {
    isTemporary: boolean;
    pinned: boolean;
}>(a: T, b: T): 0 | 1 | -1 | 2 | -2;
//# sourceMappingURL=profileSortHelper.d.ts.map