var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Route } from "react-router";
import React from "react";
export function PageRoute(props) {
    return React.createElement(Route, __assign({ exact: true }, props), function (params) {
        return React.createElement("div", { className: "modulePage" }, props.children(params));
    });
}
//# sourceMappingURL=PageRoute.js.map