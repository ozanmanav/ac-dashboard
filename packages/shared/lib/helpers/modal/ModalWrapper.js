import React from "react";
import { ModalComponentWrapper } from "./ModalComponentWrapper";
import { Portal } from "@appcircle/core/lib/portal/Portal";
export var ModalWrapper = function (_a) {
    var history = _a.history, match = _a.match, location = _a.location, component = _a.component;
    return (component && (React.createElement(Portal, null,
        React.createElement("div", { className: "modalContainer" },
            React.createElement(ModalComponentWrapper, { history: history, location: location, match: match, component: component })))));
};
//# sourceMappingURL=ModalWrapper.js.map