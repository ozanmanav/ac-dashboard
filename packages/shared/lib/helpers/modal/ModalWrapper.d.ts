import React from "react";
import { match } from "react-router";
import { History, Location } from "history";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
export declare const ModalWrapper: ({ history, match, location, component }: {
    history: History<History.PoorMansUnknown>;
    component: React.ComponentClass<any, any> | React.FunctionComponent<any> | typeof Nocomp;
    match: match<any>;
    location: Location<History.PoorMansUnknown>;
}) => JSX.Element;
//# sourceMappingURL=ModalWrapper.d.ts.map