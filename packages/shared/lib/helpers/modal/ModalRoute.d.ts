import { PropsWithChildren } from "react";
import { ModalRouteType } from "../../ModalRouteType";
export declare function ModalRoute(props: PropsWithChildren<{
    prefix: string;
    modalRoutes: ModalRouteType[];
}>): JSX.Element;
//# sourceMappingURL=ModalRoute.d.ts.map