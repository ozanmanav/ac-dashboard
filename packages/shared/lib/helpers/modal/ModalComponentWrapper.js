var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useContext, useEffect, useMemo, useCallback } from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
import { FormContext } from "@appcircle/form/lib/FormContext";
import shortid from "shortid";
import { ModalContext, useModalStatus } from "../../services/ModalContext";
export var ModalComponentWrapper = function (props) {
    var _a = useContext(ModalContext), setModalData = _a.setModalData, initialState = _a.DEPRECATED_initialState, clear = _a.clear;
    var state = useContext(FormContext).state;
    var setModalStatus = useModalStatus()[0];
    var modalID = useMemo(function () {
        return shortid.generate();
    }, []);
    useEffect(function () {
        var keyDownEventListener = function (e) {
            if (e.keyCode === KeyCode.ESC_KEY) {
                onModalClose();
            }
            return false;
        };
        document.addEventListener("keydown", keyDownEventListener);
        return function () { return document.removeEventListener("keydown", keyDownEventListener); };
        // eslint-disable-next-line
    }, [state, modalID]);
    var close = useCallback(function () {
        props.history.push(props.history.createHref(__assign(__assign({}, props.history.location), { search: "" })));
        clear();
    }, [clear, props.history]);
    var Component = useMemo(function () { return props.component || Nocomp; }, [props.component]);
    var onModalClose = useCallback(function (force) {
        close();
    }, [state, modalID]);
    useEffect(function () {
        setModalStatus(modalID, "opened");
        return function () { return setModalStatus(modalID, "closed"); };
    }, [modalID, setModalStatus]);
    var params = useMemo(function () {
        return (props.location.search.match(new RegExp("([^?=&]+)(=([^&]*))?", "g")) || []).reduce(function (result, each, n, every) {
            var _a = each.split("="), key = _a[0], value = _a[1];
            result[key] = value;
            return result;
        }, {});
    }, [props.location.search]);
    return (React.createElement(Component, { modalID: modalID, routeProps: {
            state: props.location.state,
            params: params
        }, initialState: initialState, onModalData: setModalData, onModalClose: onModalClose }));
};
//# sourceMappingURL=ModalComponentWrapper.js.map