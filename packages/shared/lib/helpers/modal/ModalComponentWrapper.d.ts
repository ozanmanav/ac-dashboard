import React from "react";
import { match } from "react-router";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
import { History, Location } from "history";
export declare const ModalComponentWrapper: (props: {
    component: React.ComponentClass<any, any> | React.FunctionComponent<any> | typeof Nocomp;
    history: History<History.PoorMansUnknown>;
    match: match<any>;
    location: Location<History.PoorMansUnknown>;
}) => JSX.Element;
//# sourceMappingURL=ModalComponentWrapper.d.ts.map