import { Route } from "react-router";
import { useMemo } from "react";
import React from "react";
import { ModalWrapper } from "./ModalWrapper";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
export function ModalRoute(props) {
    return (React.createElement(Route, { key: "modal", path: "/**/", render: function (_a) {
            var match = _a.match, location = _a.location, history = _a.history;
            return (location.search.indexOf("modal=" + (props.prefix || "")) > -1 && (React.createElement(ModalRouteWrapper, { match: match, location: location, history: history, modalRoutes: props.modalRoutes })));
        } }));
}
function ModalRouteWrapper(props) {
    var component = useMemo(function () {
        var component = null;
        if (props.location.search.indexOf("modal=") > -1) {
            props.modalRoutes.some(function (route, index) {
                if (props.location.search.indexOf("modal=" + route.path) > -1) {
                    component = route.component;
                    return true;
                }
                return false;
            });
            return component;
        }
        return null;
    }, [props.location.search, props.modalRoutes]);
    return (React.createElement(ModalWrapper, { match: props.match, location: props.location, history: props.history, component: component || Nocomp }));
}
//# sourceMappingURL=ModalRoute.js.map