export function profileSortHelper(a, b) {
    var l = a;
    var r = b;
    if (l.isTemporary && !r.isTemporary)
        return -2;
    else if (!l.isTemporary && r.isTemporary)
        return 2;
    else if (l.pinned && !r.pinned)
        return -1;
    else if (!l.pinned && r.pinned)
        return 1;
    return 0;
}
//# sourceMappingURL=profileSortHelper.js.map