import { RouteProps, RouteChildrenProps } from "react-router";
import React from "react";
export declare function PageRoute(props: RouteProps & {
    children: ((props: RouteChildrenProps<any>) => React.ReactNode);
}): JSX.Element;
//# sourceMappingURL=PageRoute.d.ts.map