import { Store } from "redux";
import { ModuleContext } from "./Module";
import { Dispatch, PropsWithChildren } from "react";
export declare type ExtractState<T> = T extends Store<infer S, any> ? S : never;
export declare type ExtractActions<T> = T extends Store<any, infer A> ? A : never;
export declare type ExtractStore<S extends Store> = Store<ExtractState<S>, ExtractActions<S>>;
export declare type ModuleContextProviderPropsType<C extends ModuleContext, S extends Store<any, any>> = PropsWithChildren<{
    context: C;
    store: S;
}>;
export declare type ModuleContextValueType<S> = ModuleContext & {
    state: ExtractState<S>;
    dispatch: Dispatch<ExtractActions<S>>;
};
export declare function createModuleContextProviderValue<S extends Store<any, any>>(): (props: {
    context: ModuleContext<Store<any, any>>;
    store: ExtractStore<S>;
}) => ModuleContextValueType<S>;
//# sourceMappingURL=ModuleContext.d.ts.map