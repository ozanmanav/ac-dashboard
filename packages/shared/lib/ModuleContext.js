var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { useState, useEffect, useMemo } from "react";
export function createModuleContextProviderValue() {
    return function (props) {
        var _a = useState(function () {
            return props.store.getState();
        }), state = _a[0], setState = _a[1];
        useEffect(function () {
            if (props.store === undefined)
                return;
            var unsubscribe = props.store.subscribe(function () {
                props.store && setState(props.store.getState());
            });
            return function () { return unsubscribe(); };
            // eslint-disable-next-line
        }, [props.store]);
        var value = useMemo(function () { return (__assign(__assign({}, (props.context || {})), { state: state, dispatch: props.store.dispatch })); }, [props.context, props.store, state]);
        return value;
    };
}
//# sourceMappingURL=ModuleContext.js.map