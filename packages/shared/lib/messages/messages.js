var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { defineMessages } from "react-intl";
import { CommonMessages } from "./common";
export default defineMessages(__assign({ logout: {
        id: "app.actions.logout",
        defaultMessage: "Are you sure you want to log out?"
    } }, CommonMessages));
//# sourceMappingURL=messages.js.map