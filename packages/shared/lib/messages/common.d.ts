export declare const CommonMessages: {
    appCommonDownload: {
        id: string;
        defaultMessage: string;
    };
    appCommonDownloadLogs: {
        id: string;
        defaultMessage: string;
    };
    appCommonCancel: {
        id: string;
        defaultMessage: string;
    };
    appCommonFetch: {
        id: string;
        defaultMessage: string;
    };
    appCommonSavedSuccess: {
        id: string;
        defaultMessage: string;
    };
    appCommonSavedError: {
        id: string;
        defaultMessage: string;
    };
    deleteModalDialogTitle: {
        id: string;
        defaultMessage: string;
    };
    appCommonBuild: {
        id: string;
        defaultMessage: string;
    };
    appCommonDistribution: {
        id: string;
        defaultMessage: string;
    };
    appCommonPlugins: {
        id: string;
        defaultMessage: string;
    };
    appCommonAdvanced: {
        id: string;
        defaultMessage: string;
    };
    appCommonWorkflow: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnvironmentVariables: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnvVars: {
        id: string;
        defaultMessage: string;
    };
    appCommonNone: {
        id: string;
        defaultMessage: string;
    };
    appCommonSessionExpired: {
        id: string;
        defaultMessage: string;
    };
    appCommonSentDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonSessionExpiredLong: {
        id: string;
        defaultMessage: string;
    };
    appCommonSubjectExists: {
        id: string;
        defaultMessage: string;
    };
    appCommonMustNotbeEmpty: {
        id: string;
        defaultMessage: string;
    };
    appCommonWarning: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroup: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroups: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroupName: {
        id: string;
        defaultMessage: string;
    };
    appCommonBrowse: {
        id: string;
        defaultMessage: string;
    };
    appCommonUploading: {
        id: string;
        defaultMessage: string;
    };
    appCommonProcessing: {
        id: string;
        defaultMessage: string;
    };
    appCommonUploadCompleted: {
        id: string;
        defaultMessage: string;
    };
    appCommonPreparing: {
        id: string;
        defaultMessage: string;
    };
    appCommonWaiting: {
        id: string;
        defaultMessage: string;
    };
    appCommonUpload: {
        id: string;
        defaultMessage: string;
    };
    appCommonChange: {
        id: string;
        defaultMessage: string;
    };
    appCommonRetry: {
        id: string;
        defaultMessage: string;
    };
    appCommonSave: {
        id: string;
        defaultMessage: string;
    };
    appCommonSaving: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnabled: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnable: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSend: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSending: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonLogin: {
        id: string;
        defaultMessage: string;
    };
    appCommonLogout: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonLoginProgress: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSignup: {
        id: string;
        defaultMessage: string;
    };
    appField: {
        id: string;
        defaultMessage: string;
    };
    appFieldValue: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSignupProgress: {
        id: string;
        defaultMessage: string;
    };
    appCommonEmail: {
        id: string;
        defaultMessage: string;
    };
    appCommonUserName: {
        id: string;
        defaultMessage: string;
    };
    appCommonPassword: {
        id: string;
        defaultMessage: string;
    };
    appCommonConfirmPassword: {
        id: string;
        defaultMessage: string;
    };
    appCommonDisabled: {
        id: string;
        defaultMessage: string;
    };
    appCommonSearch: {
        id: string;
        defaultMessage: string;
    };
    appCommonSelect: {
        id: string;
        defaultMessage: string;
    };
    appCommonPin: {
        id: string;
        defaultMessage: string;
    };
    appCommonUnpin: {
        id: string;
        defaultMessage: string;
    };
    appCommonRename: {
        id: string;
        defaultMessage: string;
    };
    appCommonDuplicate: {
        id: string;
        defaultMessage: string;
    };
    appCommonDelete: {
        id: string;
        defaultMessage: string;
    };
    appCommonDeleting: {
        id: string;
        defaultMessage: string;
    };
    appCommonSuccessful: {
        id: string;
        defaultMessage: string;
    };
    appCommonUnsuccessful: {
        id: string;
        defaultMessage: string;
    };
    appCommonDragFileNotification: {
        id: string;
        defaultMessage: string;
    };
    appCommonVersion: {
        id: string;
        defaultMessage: string;
    };
    appCommonStatus: {
        id: string;
        defaultMessage: string;
    };
    appCommonSettings: {
        id: string;
        defaultMessage: string;
    };
    appCommonAuthentication: {
        id: string;
        defaultMessage: string;
    };
    appCommonAutoSent: {
        id: string;
        defaultMessage: string;
    };
    appCommonShare: {
        id: string;
        defaultMessage: string;
    };
    appCommonSharing: {
        id: string;
        defaultMessage: string;
    };
    appCommonSend: {
        id: string;
        defaultMessage: string;
    };
    appCommonSending: {
        id: string;
        defaultMessage: string;
    };
    appCommonNext: {
        id: string;
        defaultMessage: string;
    };
    appCommonEdit: {
        id: string;
        defaultMessage: string;
    };
    appCommonAddNew: {
        id: string;
        defaultMessage: string;
    };
    appCommonAdd: {
        id: string;
        defaultMessage: string;
    };
    appCommonLoading: {
        id: string;
        defaultMessage: string;
    };
    appCommonNoResult: {
        id: string;
        defaultMessage: string;
    };
    appCommonNoResultFor: {
        id: string;
        defaultMessage: string;
    };
    appCommonElapsedTime: {
        id: string;
        defaultMessage: string;
    };
    appCommonDuration: {
        id: string;
        defaultMessage: string;
    };
    appCommonActions: {
        id: string;
        defaultMessage: string;
    };
    appCommonStartDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonEndDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorTooLong: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorContainsIvalidChars: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorTooShort: {
        id: string;
        defaultMessage: string;
    };
};
declare const _default: {
    appCommonDownload: {
        id: string;
        defaultMessage: string;
    };
    appCommonDownloadLogs: {
        id: string;
        defaultMessage: string;
    };
    appCommonCancel: {
        id: string;
        defaultMessage: string;
    };
    appCommonFetch: {
        id: string;
        defaultMessage: string;
    };
    appCommonSavedSuccess: {
        id: string;
        defaultMessage: string;
    };
    appCommonSavedError: {
        id: string;
        defaultMessage: string;
    };
    deleteModalDialogTitle: {
        id: string;
        defaultMessage: string;
    };
    appCommonBuild: {
        id: string;
        defaultMessage: string;
    };
    appCommonDistribution: {
        id: string;
        defaultMessage: string;
    };
    appCommonPlugins: {
        id: string;
        defaultMessage: string;
    };
    appCommonAdvanced: {
        id: string;
        defaultMessage: string;
    };
    appCommonWorkflow: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnvironmentVariables: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnvVars: {
        id: string;
        defaultMessage: string;
    };
    appCommonNone: {
        id: string;
        defaultMessage: string;
    };
    appCommonSessionExpired: {
        id: string;
        defaultMessage: string;
    };
    appCommonSentDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonSessionExpiredLong: {
        id: string;
        defaultMessage: string;
    };
    appCommonSubjectExists: {
        id: string;
        defaultMessage: string;
    };
    appCommonMustNotbeEmpty: {
        id: string;
        defaultMessage: string;
    };
    appCommonWarning: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroup: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroups: {
        id: string;
        defaultMessage: string;
    };
    appCommonGroupName: {
        id: string;
        defaultMessage: string;
    };
    appCommonBrowse: {
        id: string;
        defaultMessage: string;
    };
    appCommonUploading: {
        id: string;
        defaultMessage: string;
    };
    appCommonProcessing: {
        id: string;
        defaultMessage: string;
    };
    appCommonUploadCompleted: {
        id: string;
        defaultMessage: string;
    };
    appCommonPreparing: {
        id: string;
        defaultMessage: string;
    };
    appCommonWaiting: {
        id: string;
        defaultMessage: string;
    };
    appCommonUpload: {
        id: string;
        defaultMessage: string;
    };
    appCommonChange: {
        id: string;
        defaultMessage: string;
    };
    appCommonRetry: {
        id: string;
        defaultMessage: string;
    };
    appCommonSave: {
        id: string;
        defaultMessage: string;
    };
    appCommonSaving: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnabled: {
        id: string;
        defaultMessage: string;
    };
    appCommonEnable: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSend: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSending: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonLogin: {
        id: string;
        defaultMessage: string;
    };
    appCommonLogout: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonLoginProgress: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSignup: {
        id: string;
        defaultMessage: string;
    };
    appField: {
        id: string;
        defaultMessage: string;
    };
    appFieldValue: {
        id: string;
        defaultMessage: string;
    };
    appSubmitButtonSignupProgress: {
        id: string;
        defaultMessage: string;
    };
    appCommonEmail: {
        id: string;
        defaultMessage: string;
    };
    appCommonUserName: {
        id: string;
        defaultMessage: string;
    };
    appCommonPassword: {
        id: string;
        defaultMessage: string;
    };
    appCommonConfirmPassword: {
        id: string;
        defaultMessage: string;
    };
    appCommonDisabled: {
        id: string;
        defaultMessage: string;
    };
    appCommonSearch: {
        id: string;
        defaultMessage: string;
    };
    appCommonSelect: {
        id: string;
        defaultMessage: string;
    };
    appCommonPin: {
        id: string;
        defaultMessage: string;
    };
    appCommonUnpin: {
        id: string;
        defaultMessage: string;
    };
    appCommonRename: {
        id: string;
        defaultMessage: string;
    };
    appCommonDuplicate: {
        id: string;
        defaultMessage: string;
    };
    appCommonDelete: {
        id: string;
        defaultMessage: string;
    };
    appCommonDeleting: {
        id: string;
        defaultMessage: string;
    };
    appCommonSuccessful: {
        id: string;
        defaultMessage: string;
    };
    appCommonUnsuccessful: {
        id: string;
        defaultMessage: string;
    };
    appCommonDragFileNotification: {
        id: string;
        defaultMessage: string;
    };
    appCommonVersion: {
        id: string;
        defaultMessage: string;
    };
    appCommonStatus: {
        id: string;
        defaultMessage: string;
    };
    appCommonSettings: {
        id: string;
        defaultMessage: string;
    };
    appCommonAuthentication: {
        id: string;
        defaultMessage: string;
    };
    appCommonAutoSent: {
        id: string;
        defaultMessage: string;
    };
    appCommonShare: {
        id: string;
        defaultMessage: string;
    };
    appCommonSharing: {
        id: string;
        defaultMessage: string;
    };
    appCommonSend: {
        id: string;
        defaultMessage: string;
    };
    appCommonSending: {
        id: string;
        defaultMessage: string;
    };
    appCommonNext: {
        id: string;
        defaultMessage: string;
    };
    appCommonEdit: {
        id: string;
        defaultMessage: string;
    };
    appCommonAddNew: {
        id: string;
        defaultMessage: string;
    };
    appCommonAdd: {
        id: string;
        defaultMessage: string;
    };
    appCommonLoading: {
        id: string;
        defaultMessage: string;
    };
    appCommonNoResult: {
        id: string;
        defaultMessage: string;
    };
    appCommonNoResultFor: {
        id: string;
        defaultMessage: string;
    };
    appCommonElapsedTime: {
        id: string;
        defaultMessage: string;
    };
    appCommonDuration: {
        id: string;
        defaultMessage: string;
    };
    appCommonActions: {
        id: string;
        defaultMessage: string;
    };
    appCommonStartDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonEndDate: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorTooLong: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorContainsIvalidChars: {
        id: string;
        defaultMessage: string;
    };
    appCommonFormFieldErrorTooShort: {
        id: string;
        defaultMessage: string;
    };
};
export default _default;
//# sourceMappingURL=common.d.ts.map