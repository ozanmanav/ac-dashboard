var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { defineMessages } from "react-intl";
var app = "app.common.";
export var CommonMessages = {
    appCommonDownload: {
        id: app + "common.download",
        defaultMessage: "Download"
    },
    appCommonDownloadLogs: {
        id: app + "common.download",
        defaultMessage: "Download Logs"
    },
    appCommonCancel: {
        id: app + "common.cancel",
        defaultMessage: "Cancel"
    },
    appCommonFetch: {
        id: app + "common.fetch",
        defaultMessage: "Fetch"
    },
    appCommonSavedSuccess: {
        id: app + "saved.success",
        defaultMessage: "Successfully saved."
    },
    appCommonSavedError: {
        id: app + "saved.error",
        defaultMessage: "Cannot be saved."
    },
    deleteModalDialogTitle: {
        id: app + "modal.delete.title",
        defaultMessage: "Are you sure you want to delete the following {subject}?"
    },
    appCommonBuild: {
        id: app + ".build",
        defaultMessage: "Build"
    },
    appCommonDistribution: {
        id: app + ".distribution",
        defaultMessage: "Distribution"
    },
    appCommonPlugins: {
        id: app + ".plugins",
        defaultMessage: "Plugins"
    },
    appCommonAdvanced: {
        id: app + ".advanced",
        defaultMessage: "Advanced"
    },
    appCommonWorkflow: {
        id: app + ".workflow",
        defaultMessage: "Workflow"
    },
    appCommonEnvironmentVariables: {
        id: app + ".environmentVariables",
        defaultMessage: "Environment Variables"
    },
    appCommonEnvVars: {
        id: app + ".envVars",
        defaultMessage: "Env. Variables"
    },
    appCommonNone: {
        id: app + ".none",
        defaultMessage: "None"
    },
    appCommonSessionExpired: {
        id: app + ".sessionExpired",
        defaultMessage: "Your session has expired."
    },
    appCommonSentDate: {
        id: app + ".sentDate",
        defaultMessage: "Sent Date"
    },
    appCommonSessionExpiredLong: {
        id: app + ".sessionExpiredLong",
        defaultMessage: "Your session has expired. Please login to reactivate your session."
    },
    appCommonSubjectExists: {
        id: app + ".subjectExists",
        defaultMessage: "{subject} already exists"
    },
    appCommonMustNotbeEmpty: {
        id: app + ".mustnotbeEmpty",
        defaultMessage: "{subject} cannot be empty"
    },
    appCommonWarning: {
        id: app + ".warning",
        defaultMessage: "Warning"
    },
    appCommonGroup: {
        id: app + ".group",
        defaultMessage: "Group"
    },
    appCommonGroups: {
        id: app + ".groups",
        defaultMessage: "Groups"
    },
    appCommonGroupName: {
        id: app + ".groupName",
        defaultMessage: "Group Name"
    },
    appCommonBrowse: {
        id: app + ".browse",
        defaultMessage: "Browse"
    },
    appCommonUploading: {
        id: app + ".uploading",
        defaultMessage: "Uploading..."
    },
    appCommonProcessing: {
        id: app + ".processing",
        defaultMessage: "Processing..."
    },
    appCommonUploadCompleted: {
        id: app + ".processing",
        defaultMessage: "File Uploaded."
    },
    appCommonPreparing: {
        id: app + ".preparing",
        defaultMessage: "Preparing..."
    },
    appCommonWaiting: {
        id: app + ".waiting",
        defaultMessage: "Waiting..."
    },
    appCommonUpload: {
        id: app + ".upload",
        defaultMessage: "Upload"
    },
    appCommonChange: {
        id: app + ".change",
        defaultMessage: "Change"
    },
    appCommonRetry: {
        id: app + ".retry",
        defaultMessage: "Retry"
    },
    appCommonSave: {
        id: app + ".save",
        defaultMessage: "Save"
    },
    appCommonSaving: {
        id: app + ".saving",
        defaultMessage: "Saving..."
    },
    appCommonEnabled: {
        id: app + ".enabled",
        defaultMessage: "Enabled"
    },
    appCommonEnable: {
        id: app + ".enable",
        defaultMessage: "Enable"
    },
    appSubmitButtonSend: {
        id: app + "send",
        defaultMessage: "Send"
    },
    appSubmitButtonSending: {
        id: app + "sending",
        defaultMessage: "Processing..."
    },
    appSubmitButtonLogin: {
        id: app + "login",
        defaultMessage: "Log in"
    },
    appCommonLogout: {
        id: app + "logout",
        defaultMessage: "Log out"
    },
    appSubmitButtonLoginProgress: {
        id: app + "login",
        defaultMessage: "Logging in..."
    },
    appSubmitButtonSignup: {
        id: app + "signup",
        defaultMessage: "Sign up"
    },
    appField: {
        id: app + "field",
        defaultMessage: "Field"
    },
    appFieldValue: {
        id: app + "fieldValue",
        defaultMessage: "Field's value"
    },
    appSubmitButtonSignupProgress: {
        id: app + "signingup",
        defaultMessage: "Signing up..."
    },
    appCommonEmail: {
        id: app + "email",
        defaultMessage: "Email"
    },
    appCommonUserName: {
        id: app + "userName",
        defaultMessage: "User Name"
    },
    appCommonPassword: {
        id: app + "password",
        defaultMessage: "Password"
    },
    appCommonConfirmPassword: {
        id: app + "password",
        defaultMessage: "Confirm your password"
    },
    appCommonDisabled: {
        id: app + ".disabled",
        defaultMessage: "Disabled"
    },
    appCommonSearch: {
        id: app + ".search",
        defaultMessage: "Search"
    },
    appCommonSelect: {
        id: app + ".select",
        defaultMessage: "Select"
    },
    appCommonPin: {
        id: app + ".pin",
        defaultMessage: "Pin Item"
    },
    appCommonUnpin: {
        id: app + ".unpin",
        defaultMessage: "Remove pin"
    },
    appCommonRename: {
        id: app + ".rename",
        defaultMessage: "Rename"
    },
    appCommonDuplicate: {
        id: app + ".duplicate",
        defaultMessage: "Duplicate"
    },
    appCommonDelete: {
        id: app + ".delete",
        defaultMessage: "Delete"
    },
    appCommonDeleting: {
        id: app + ".deleting",
        defaultMessage: "Deleting..."
    },
    appCommonSuccessful: {
        id: app + ".delete",
        defaultMessage: "Successful"
    },
    appCommonUnsuccessful: {
        id: app + ".delete",
        defaultMessage: "Unsuccessful"
    },
    appCommonDragFileNotification: {
        id: app + ".dragFileNotification",
        defaultMessage: "Drag and drop the {file} file here to upload or"
    },
    appCommonVersion: {
        id: app + ".version",
        defaultMessage: "Version"
    },
    appCommonStatus: {
        id: app + ".status",
        defaultMessage: "Status"
    },
    appCommonSettings: {
        id: app + ".settings",
        defaultMessage: "Settings"
    },
    appCommonAuthentication: {
        id: app + ".authentication",
        defaultMessage: "Authentication"
    },
    appCommonAutoSent: {
        id: app + ".autoSent",
        defaultMessage: "Auto Send"
    },
    appCommonShare: {
        id: app + ".share",
        defaultMessage: "Share"
    },
    appCommonSharing: {
        id: app + ".sharing",
        defaultMessage: "Sharing"
    },
    appCommonSend: {
        id: app + ".send",
        defaultMessage: "Send"
    },
    appCommonSending: {
        id: app + ".sending",
        defaultMessage: "Sending..."
    },
    appCommonNext: {
        id: app + ".next",
        defaultMessage: "Next"
    },
    appCommonEdit: {
        id: app + ".edit",
        defaultMessage: "Edit"
    },
    appCommonAddNew: {
        id: app + ".addNew",
        defaultMessage: "Add New"
    },
    appCommonAdd: {
        id: app + ".add",
        defaultMessage: "Add"
    },
    appCommonLoading: {
        id: app + ".loading",
        defaultMessage: "Loading..."
    },
    appCommonNoResult: {
        id: app + ".loading",
        defaultMessage: "No Results"
    },
    appCommonNoResultFor: {
        id: app + ".loading",
        defaultMessage: "No Results found for {keyword}"
    },
    appCommonElapsedTime: {
        id: app + ".elapsedTime",
        defaultMessage: "Elapsed Time"
    },
    appCommonDuration: {
        id: app + ".duration",
        defaultMessage: "Duration"
    },
    appCommonActions: {
        id: app + ".actions",
        defaultMessage: "Actions"
    },
    appCommonStartDate: {
        id: app + ".startDate",
        defaultMessage: "Start Date"
    },
    appCommonEndDate: {
        id: app + ".endDate",
        defaultMessage: "End Date"
    },
    appCommonFormFieldErrorTooLong: {
        id: app + ".error.tooLong",
        defaultMessage: "{subject} is too long!"
    },
    appCommonFormFieldErrorContainsIvalidChars: {
        id: app + ".error.containsIvalidChars",
        defaultMessage: "{subject} contains invalid characters. You can use only {specials}"
    },
    appCommonFormFieldErrorTooShort: {
        id: app + ".error.tooShort",
        defaultMessage: "{subject} is too short!"
    }
};
export default defineMessages(__assign({}, CommonMessages));
//# sourceMappingURL=common.js.map