export var ClientAlertType;
(function (ClientAlertType) {
    ClientAlertType[ClientAlertType["Primary"] = 0] = "Primary";
    ClientAlertType[ClientAlertType["Secondary"] = 1] = "Secondary";
    ClientAlertType[ClientAlertType["Success"] = 2] = "Success";
    ClientAlertType[ClientAlertType["Danger"] = 3] = "Danger";
    ClientAlertType[ClientAlertType["Warning"] = 4] = "Warning";
    ClientAlertType[ClientAlertType["Info"] = 5] = "Info";
    ClientAlertType[ClientAlertType["Light"] = 6] = "Light";
    ClientAlertType[ClientAlertType["Dark"] = 7] = "Dark";
})(ClientAlertType || (ClientAlertType = {}));
//# sourceMappingURL=ClientAlertType.js.map