export var ClientMessageType;
(function (ClientMessageType) {
    ClientMessageType[ClientMessageType["Hidden"] = 0] = "Hidden";
    ClientMessageType[ClientMessageType["ToastMessage"] = 1] = "ToastMessage";
    ClientMessageType[ClientMessageType["ToastMessageWithCloseButton"] = 2] = "ToastMessageWithCloseButton";
    ClientMessageType[ClientMessageType["InAppNotification"] = 3] = "InAppNotification";
    ClientMessageType[ClientMessageType["TopBarNotification"] = 4] = "TopBarNotification";
})(ClientMessageType || (ClientMessageType = {}));
//# sourceMappingURL=ClientMessageType.js.map