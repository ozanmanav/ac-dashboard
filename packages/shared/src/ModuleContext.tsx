import { Store, AnyAction } from "redux";

import { ModuleContext } from "./Module";

import {
  Dispatch,
  useState,
  useEffect,
  useMemo,
  PropsWithChildren
} from "react";

export type ExtractState<T> = T extends Store<infer S, any> ? S : never;
export type ExtractActions<T> = T extends Store<any, infer A> ? A : never;
export type ExtractStore<S extends Store> = Store<
  ExtractState<S>,
  ExtractActions<S>
>;

export type ModuleContextProviderPropsType<
  C extends ModuleContext,
  S extends Store<any, any>
> = PropsWithChildren<{ context: C; store: S }>;
export type ModuleContextValueType<S> = ModuleContext & {
  state: ExtractState<S>;
  dispatch: Dispatch<ExtractActions<S>>;
};
export function createModuleContextProviderValue<S extends Store<any, any>>() {
  return (props: { context: ModuleContext; store: ExtractStore<S> }) => {
    const [state, setState] = useState<ExtractState<S>>(() =>
      props.store.getState()
    );
    useEffect(() => {
      if (props.store === undefined) return;
      const unsubscribe = props.store.subscribe(() => {
        props.store && setState(props.store.getState());
      });
      return () => unsubscribe();
      // eslint-disable-next-line
    }, [props.store]);
    const value = useMemo<ModuleContextValueType<S>>(
      () => ({
        // TODO: Change this appContext: props.context
        ...(props.context || {}),
        state,
        dispatch: props.store.dispatch
      }),
      [props.context, props.store, state]
    );
    return value;
  };
}
