import { ApiAuthError } from "./ApiAuthError";
export function instanceOfApiAuthError(obj: any): obj is ApiAuthError {
  return obj && "error_description" in obj && "error" in obj;
}
