import { ApiInnerError } from "./createService";
export class ApiError {
  public code: string | null = null;
  public message: string | null = null;
  public cultureInfo: string | null = null;
  public statusCode: number = 0;
  public traceId: string | null = null;
  public innerErrors: ApiInnerError[] = [];
  constructor(props: ApiError) {
    Object.assign(this, props);
  }
}
