import { HookEventActions } from "@appcircle/sse/lib/HookEventActions";
import { Result } from "@appcircle/core/lib/services/Result";
import { ApiAuthError } from "./ApiAuthError";
import { ApiError } from "./ApiError";
export interface AjaxResultError extends Error, Result {
  error?:
    | ApiError
    | ApiAuthError
    | {
        [key: string]: any;
      };
  readonly statusText: string | HookEventActions;
}
