import { ApiError } from "./ApiError";
export function instanceOfApiError(obj: any): obj is ApiError {
  return obj && "innerErrors" in obj;
}
