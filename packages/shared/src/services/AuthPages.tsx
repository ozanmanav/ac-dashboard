import { RouteComponentProps, StaticContext } from "react-router";
import React, { ComponentType, PropsWithChildren, useEffect } from "react";
// import { connect } from "react-redux";
// import { AppStateType } from "../reducers";
// import { SignUp } from "../modules/auth/SignUp";
// import { Login } from "../modules/auth/Login";
// import { AuthServiceContext } from "../modules/auth/service/AuthService";
import { AuthTokenType } from "./AuthTokenType";
import { Switch, Route } from "react-router-dom";
// import { store } from "App";
// import { ForgotPassword } from "modules/auth/ForgotPassword";
// import { ResetPassword } from "modules/auth/ResetPassword";

export function AuthServiceReducer(state: false) {
  return state;
}
export function AuthPagesProvider(
  props: PropsWithChildren<{
    isLoggedIn: boolean;
    ForgotPassword: ComponentType;
    ResetPassword: ComponentType<{ data: string }>;
    Login: ComponentType<{ routerState: object }>;
    SignUp: ComponentType;
    clearUserData: () => void;
  }>
) {
  // const { token } = useContext(AuthServiceContext);
  useEffect(() => {
    //Remove User Info when the access_token is falsy eg. on logout
    if (!props.isLoggedIn) {
      props.clearUserData();
    }
  }, [props.isLoggedIn]);

  return props.isLoggedIn ? (
    <React.Fragment>{props.children}</React.Fragment>
  ) : (
    <Switch>
      <Route key="/signup" path="/signup" render={() => <props.SignUp />} />
      <Route
        key="/forgotPassword"
        path="/forgotPassword"
        render={() => <props.ForgotPassword />}
      />
      <Route
        key="/resetPassword"
        path="/resetPassword"
        render={({ location }) => (
          <props.ResetPassword data={location.search} />
        )}
      />
      <Route
        key="/login"
        path="/"
        render={({ match, location, history }) => (
          <props.Login routerState={location.state} />
        )}
      />
    </Switch>
  );
}

// const RouterWrapper = withRouter(AuthPages);

// export AuthPages as AuthPagesProvider;
// connect((props: AppStateType) => ({
//   isLoggedIn: props.login.isLoggedIn
// }))(RouterWrapper);
