import { HookEvent } from "@appcircle/sse/lib/HookEvent";
export type HookResultEvent = {
  ok: boolean;
  message: string;
  json: HookEvent;
};
