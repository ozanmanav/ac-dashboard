import { Observable, Subscriber, PartialObserver } from "rxjs";
import { map } from "rxjs/operators";
import React, {
  PropsWithChildren,
  useCallback,
  useMemo,
  createContext,
  useContext,
  useState
} from "react";
import { ajax, AjaxRequest, AjaxResponse, AjaxError } from "rxjs/ajax";
import { isObject } from "util";
import { TaskFactoryFn } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ApiResultType } from "@appcircle/core/lib/services/ApiResultType";
import { AuthTokenType } from "./AuthTokenType";
import { createAjaxErrorFromAjaxResponse } from "./createAjaxErrorFromAjaxResponse";
import { createAjaxResult } from "./createAjaxResult";
import { ReqestMethod } from "@appcircle/core/lib/ReqestMethod";
import { Result } from "@appcircle/core/lib/services/Result";
import { HookResultEvent } from "./HookResultEvent";
import { AjaxResultError } from "./AjaxResultError";
export type Lit = string | number | boolean | undefined | null | void | {};
export const tuple = <T extends Lit[]>(...args: T) => args;

export type HeadersType = {
  [key: string]: any;
  "Content-Type"?: string;
  Authorization?: string;
};

function jsonToFormData(json: { [key: string]: any } = {}) {
  const data = new FormData();
  Object.keys(json).forEach(key => {
    data.append(key, json[key]);
  });
  return data;
}

export type ResponseTypeof<T> = T extends TypedResponse<infer R> ? R : never;
export type ObservableAjaxDispatch<
  T extends (props: AjaxProps) => Observable<ApiResultType<any>>
> = T extends (props: AjaxProps) => infer R
  ? (props: AjaxProps) => ObservableValueof<Observable<R>>
  : (props: AjaxProps) => ObservableValueof<any>;

export type AjaxMethodProps<TResponse = any> = {
  url: string;
  data: any;
  headers: HeadersType;
  progressObserver: PartialObserver<TResponse>;
};

export type AjaxServiceMethodType = <TResponse = any>(
  props: AjaxProps<TResponse>
) => Observable<ApiResultType<TResponse>>;

export type ObservableAjax = {
  get: AjaxServiceMethodType;
  post: AjaxServiceMethodType;
  delete: AjaxServiceMethodType;
  patch: AjaxServiceMethodType;
  put: AjaxServiceMethodType;
};
type ObservableValueof<T extends Observable<any>> = T extends Observable<
  TypedResponse<infer R>
>
  ? Observable<R>
  : Observable<any>;

export type ObservableResponse<T> = T extends infer R
  ? Observable<Result<R>>
  : Observable<Result<never>>;

export type ServiceDispatch<
  T extends ObservableAjaxDispatch<any>
> = T extends ObservableAjaxDispatch<infer T>
  ? ObservableAjaxDispatch<T>
  : ObservableAjaxDispatch<any>;
export interface Body<TJson = any> {
  readonly body: ReadableStream<Uint8Array> | null;
  readonly bodyUsed: boolean;
  arrayBuffer(): Promise<ArrayBuffer>;
  blob(): Promise<Blob>;
  formData(): Promise<FormData>;
  json(): Promise<TJson>;
  text(): Promise<string>;
}

export interface TypedResponse<TJson = any> extends Body<TJson> {
  readonly headers: Headers;
  readonly ok: boolean;
  readonly redirected: boolean;
  readonly status: number;
  readonly statusText: string;
  readonly trailer: Promise<Headers>;
  readonly type: ResponseType;
  readonly url: string;
  clone(): TypedResponse;
}

const actions = [
  "@@SERVICE/POST",
  "@@SERVICE/GET",
  "@@SERVICE/DELETE",
  "@@SERVICE/PATCH"
] as const;

// const options: () => RequestInit = () => ({
//   mode: "cors",
//   // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
//   credentials: "same-origin",
//   headers: {}
// });
const ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY";
export type ServiceActionType = typeof actions[number];
export interface IAjaxResponse<T> extends AjaxResponse {
  response: ApiResultType<T>;
}

export type ApiInnerError = {
  propertyName: string;
  message: string;
  attemptedValue: string;
  code: string;
};

export function createAjaxErrorFromHookEvent(
  props: HookResultEvent
): AjaxResultError {
  return {
    name: "AjaxError",
    headers: "",
    ok: false,
    status: 0,
    statusText: props.json.eventAction,
    message: props.json.message,
    type: props.json.eventName,
    json: props.json.data,
    url: ""
  };
}

export type RequestTimeoutErrorType = {
  status: number;
  error: string;
};

export const REQUEST_TIMEOUT_DELAY = 10000;

// const ERRORS = {
//   504: "Request timed out",
//   401: "Not authorised"
// };
// declare interface checkResponse {
//   <T>(response: IAjaxResponse<T>): Result<T>;
// }

function checkResponse<T>(response: IAjaxResponse<T> | AjaxError) {
  return isOkRequest(response.status) && response
    ? createAjaxResult(response as IAjaxResponse<T>)
    : createAjaxErrorFromAjaxResponse(response as AjaxError);
}

function dataMaybe(data: any) {
  return typeof data === "string" ? data : jsonToFormData(data);
}

function jsonHeaderMaybe(data: any): HeadersType {
  return typeof data === "string" ? { "Content-Type": "application/json" } : {};
}

const createAjax: <T>(
  input: AjaxRequest
) => Observable<ApiResultType<T>> = input => {
  return ajax({
    ...input,
    async: true,
    crossDomain: true
  }).pipe(map(checkResponse));
};

function createAjaxServiceMethod(method: ReqestMethod): AjaxServiceMethodType {
  return function<TResponse>({
    endpoint: url,
    data,
    headers,
    responseType = "json"
  }: AjaxProps) {
    const observer = isObject(url) && (url as AjaxMethodProps).progressObserver;
    const progressSubscriber = observer
      ? new Subscriber<TResponse>(observer)
      : undefined;
    return createAjax<TResponse>({
      url: isObject(url) ? (url as AjaxMethodProps).url : (url as string),
      method,
      progressSubscriber,
      body: data && dataMaybe(data),
      headers: { ...jsonHeaderMaybe(data), ...headers },
      responseType
    });
  };
}

export function isOkRequest(status: number) {
  return status >= 200 && status < 300;
}

export function AjaxAdapter(): ObservableAjax {
  return {
    get: createAjaxServiceMethod("GET"),
    post: createAjaxServiceMethod("POST"),
    delete: createAjaxServiceMethod("DELETE"),
    patch: createAjaxServiceMethod("PATCH"),
    put: createAjaxServiceMethod("PUT")
  };
}

export type BaseServiceProviderPropsType<TState, TService> = PropsWithChildren<{
  value?: TState;
  token?: any;
  setToken: () => void;
  service?: TService;
}>;

export function createSubscription<TResp>(setState: (resp: TResp) => void) {
  return (resp: TResp) => {
    setState(resp);
  };
}

export type RestServiceAction = {
  type: ServiceActionType;
  payload: { url: string; data: any };
};

export type ServiceProviderPropsType<TState, TService> = PropsWithChildren<{
  initialState?: TState;
  service?: TService;
}>;

export function createRestServiceAction(
  action: ServiceActionType,
  url: string,
  data: any
): RestServiceAction {
  return {
    type: action,
    payload: {
      data,
      url
    }
  };
}

export function createAjaxAdapter() {}

export type AjaxProps<TData = any, TResponse = any> = {
  method: ReqestMethod;
  endpoint: string | AjaxMethodProps;
  data?: TData;
  abortController?: AbortController;
  headers?: HeadersType;
  responseType?: "json" | "arraybuffer" | "application/text";
};
export type ConsumerFactoryFn = (url?: string) => ServiceFactoryFn;

export type ServiceFactoryFn = <TResponse = any, TData = any>(
  props: AjaxProps<TData, TResponse>
) => TaskFactoryFn<TResponse>;

// export type TaskFactoryFn<TResponse = any> = (
//   progressObserver?: PartialObserver<TResponse>
// ) => TaskObervableType<TResponse>;

type ServiceContextType = {
  token: AuthTokenType;
  updateToken: (token: AuthTokenType) => void;
  removeToken: () => void;
  createApiConsumer: ConsumerFactoryFn;
  createAuthConsumer: ConsumerFactoryFn;
};

type ServiceProviderType = (props: {
  children?: React.ReactNode;
}) => JSX.Element;

const EmptyToken = {
  access_token: "",
  expires_in: -1,
  token_type: "",
  active_organizationId: "",
  picture: "",
  preferred_username: "",
  sub: "",
  team_ids: ""
};
// createHookConnection(tokendata.sub as string)
// Creates service layer
export function createServiceBoundry(
  apiUrl: string,
  authApiUrl: string
): [() => ServiceContextType, ServiceProviderType] {
  const ServiceBase = createContext<{
    readonly token: AuthTokenType;
    updateToken: ServiceContextType["updateToken"];
    removeToken: () => void;
    expires_date?: Date;
  }>({
    token: EmptyToken,
    updateToken: () => {},
    removeToken: () => {}
  });

  const useServiceContext = () => {
    const { token, updateToken, removeToken } = useContext(ServiceBase);

    const value = useMemo<ServiceContextType>(() => {
      const createApiConsumer: ConsumerFactoryFn = createRestServiceWrappper(
        token,
        apiUrl
      );
      const createAuthConsumer: ConsumerFactoryFn = createRestServiceWrappper(
        token,
        authApiUrl
      );
      return {
        token,
        updateToken,
        removeToken,
        createApiConsumer,
        createAuthConsumer
      };
    }, [removeToken, token, updateToken]);
    return value;
  };
  function ServiceBaseProvider(props: PropsWithChildren<{}>) {
    const [token, updateToken] = useState<AuthTokenType>(() => ({
      ...getTokenFromLocalStorage()
    }));

    const updateFn = useCallback((token: AuthTokenType) => {
      updateToken(token);
      token.expires_date = getExpireDate(token.expires_in);
      localStorage.setItem(ACCESS_TOKEN_KEY, JSON.stringify(token));
    }, []);

    const removeFn = useCallback(() => {
      localStorage.clear();
      updateToken(getTokenFromLocalStorage());
    }, []);

    const value = useMemo(
      () => ({
        token,
        updateToken: updateFn,
        removeToken: removeFn
      }),
      [removeFn, token, updateFn]
    );
    return (
      <ServiceBase.Provider value={value}>
        {props.children}
      </ServiceBase.Provider>
    );
  }

  return [useServiceContext, ServiceBaseProvider];
}
export type RestServiceType = (
  params: AjaxProps
) => Observable<ApiResultType<any>>;
// Creates consumer wrapper
export function createRestServiceWrappper<TData = any>(
  token: AuthTokenType,
  baseUrl: string,
  ajaxAdapter: ObservableAjax = AjaxAdapter()
): ConsumerFactoryFn {
  return function createServiceContext(subUrl?: string) {
    function getUrl(url: string) {
      return `${baseUrl}/${url}`;
    }

    return (props: AjaxProps) => progressObserver => {
      const _headers: HeadersType = {};
      if (token) {
        Object.assign(_headers, {
          Authorization: "Bearer " + token.access_token
        });
      }

      props.headers = {
        ..._headers,
        ...props.headers
      };
      let context: Observable<ApiResultType<any>>;

      switch (props.method) {
        case "POST":
          context = ajaxAdapter.post<any>({
            ...props,
            endpoint: {
              url: getUrl(props.endpoint as string),
              progressObserver
            } as AjaxMethodProps
          });
          break;
        case "GET":
          context = ajaxAdapter.get<any>({
            ...props,
            endpoint: {
              url: getUrl(props.endpoint as string),
              progressObserver
            } as AjaxMethodProps
          });
          break;
        case "PUT":
          context = ajaxAdapter.put<any>({
            ...props,
            endpoint: {
              url: getUrl(props.endpoint as string),
              progressObserver
            } as AjaxMethodProps
          });
          break;
        case "PATCH":
          context = ajaxAdapter.patch<any>({
            ...props,
            endpoint: {
              url: getUrl(props.endpoint as string),
              progressObserver
            } as AjaxMethodProps
          });
          break;
        case "DELETE":
          context = ajaxAdapter.delete<any>({
            ...props,
            endpoint: {
              url: getUrl(props.endpoint as string),
              progressObserver
            } as AjaxMethodProps
          });
          break;
        default:
          throw new TypeError("Invalid method type");
      }

      return context;
    };
  };
}

function getExpireDate(expires_in: number): Date {
  const curDate = new Date();
  curDate.setSeconds(curDate.getSeconds() + expires_in);
  return curDate;
}

function getTokenFromLocalStorage(): AuthTokenType {
  try {
    const tokenValue = localStorage.getItem(ACCESS_TOKEN_KEY) || "";
    const token = JSON.parse(tokenValue) as AuthTokenType;
    token.expires_date = new Date(token.expires_date || 1);
    if (new Date(token.expires_date).getTime() > new Date().getTime()) {
      return token;
    }
  } catch (error) {}
  return EmptyToken;
}
