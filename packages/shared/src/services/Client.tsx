import { Subject } from "rxjs";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { createQueryString } from "@appcircle/core/lib/utils/createQueryString";
import { HookResultEvent } from "./HookResultEvent";
import { AuthTokenType } from "./AuthTokenType";
import { Message } from "../Module";
import { DisconnectedState } from "./DisconnectedContext";

let publisher$: Subject<HookResultEvent>;
let eventSource: EventSource;

export function createHookServerBoundry(url: string) {
  return (
    token: AuthTokenType,
    pushMessage: (message: Message) => void,
    pushEvent: (data: HookEvent) => void,
    setDisconnected: React.Dispatch<React.SetStateAction<DisconnectedState>>
  ): [EventSource, Subject<HookResultEvent>] => {
    function connect() {
      !!eventSource && eventSource.close();
      eventSource = new EventSource(
        url +
          "/hooks" +
          createQueryString(
            ["id", token.sub],
            ["organizationId", token.active_organizationId]
          )
      );
    }
    if (token.sub) {
      connect();
      let failCount = 0;
      eventSource.onmessage = e => {
        console.log(e);
      };

      eventSource.addEventListener("hook", evt => {
        const data: HookEvent = JSON.parse((evt as MessageEvent).data);
        data.taskId &&
          publisher$.next({
            ok: data.isSuccess,
            message: "",
            json: data
          });
        data.data && pushEvent(data);
        console.log(data);
      });

      eventSource.onopen = function(e) {
        setDisconnected({
          isDisconnected: false,
          isExternalError: true,
          statusText: "Connected to server."
        });
        pushMessage({
          messageType: "success",
          message: "Hooks Connected"
        });
      };

      eventSource.onmessage = function(e) {
        pushMessage({
          messageType: "general",
          message: "Hook message. Find out in the console",
          data: e.data
        });

        console.log("message from hook : ", e.data);
      };

      eventSource.onerror = function(e) {
        failCount++;
        setDisconnected({
          isDisconnected: true,
          isExternalError: true,
          statusText: "Cannot connect to server. Please try again momentarily."
        });
        pushMessage({
          messageType: "error",
          message: "Hooks Connnection Error"
        });
        if (failCount === 5) {
          eventSource.close();
          setTimeout(connect, 5000);
          failCount = 0;
        }
        console.log("EventSource failed.", "Hooks Connnection Error");
      };
    }
    publisher$ && publisher$.unsubscribe();
    publisher$ = new Subject<HookResultEvent>();
    return [eventSource, publisher$];
  };
}
