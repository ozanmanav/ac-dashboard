import { AjaxError } from "rxjs/ajax";
import { createAjaxErrorFromAjaxResponse } from "./createAjaxErrorFromAjaxResponse";
import { createAjaxErrorFromHookEvent } from "./createService";
import { HookResultEvent } from "./HookResultEvent";
export function createAjaxError(props: HookResultEvent | AjaxError) {
  return props.hasOwnProperty("json")
    ? createAjaxErrorFromHookEvent(props as HookResultEvent)
    : createAjaxErrorFromAjaxResponse(props as AjaxError);
}
