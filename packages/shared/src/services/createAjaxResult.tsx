import { AjaxResponse } from "rxjs/ajax";
import { isOkRequest } from "./createService";
import { Result } from "@appcircle/core/lib/services/Result";
export function createAjaxResult<TResponse = any>(
  props: Partial<AjaxResponse>
): Result<TResponse> {
  return {
    headers: props.xhr ? props.xhr.getAllResponseHeaders() : "",
    ok: props.status ? isOkRequest(props.status) : true,
    status: props.status ? props.status : 200,
    statusText: props.xhr ? props.xhr.statusText : "",
    type: props.responseType ? props.responseType : "",
    json: props.response ? props.response : "",
    url: props.xhr ? props.xhr.responseURL : ""
  };
}
