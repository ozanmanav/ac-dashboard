import React, {
  PropsWithChildren,
  useContext,
  useState,
  useMemo,
  useEffect
} from "react";

const DisconnectedDefaultState = {
  isDisconnected: false,
  isExternalError: false,
  statusText: ""
};

export type DisconnectedState = {
  isDisconnected: boolean;
  isExternalError?: boolean;
  statusText?: string;
};

type DisconnectedValue = [
  DisconnectedState,
  React.Dispatch<React.SetStateAction<DisconnectedState>>
];
const DisconnectContext = React.createContext<DisconnectedValue>([
  DisconnectedDefaultState,
  () => null
]);
export function useDisconnected() {
  const contextValue = useContext(DisconnectContext);

  return contextValue;
}

export function DisconectedProvider(props: PropsWithChildren<{}>) {
  const [disconnectedState, setDisconnected] = useState<DisconnectedState>(
    DisconnectedDefaultState
  );

  const value = useMemo<DisconnectedValue>(
    () => [disconnectedState, setDisconnected],
    [disconnectedState]
  );

  useEffect(() => {
    const handleInternetConnectionChange = () => {
      const { isExternalError } = disconnectedState;

      const internetOnline = navigator.onLine;

      setDisconnected &&
        !isExternalError &&
        setDisconnected({
          isDisconnected: !navigator.onLine,
          statusText: internetOnline
            ? "Connected to Internet"
            : "No network connection. Please check your connection and try again."
        });
    };

    window.addEventListener("online", handleInternetConnectionChange);
    window.addEventListener("offline", handleInternetConnectionChange);

    return () => {
      window.removeEventListener("online", handleInternetConnectionChange);
      window.removeEventListener("offline", handleInternetConnectionChange);
    };
  }, []);

  return (
    <DisconnectContext.Provider value={value}>
      {props.children}
    </DisconnectContext.Provider>
  );
}
