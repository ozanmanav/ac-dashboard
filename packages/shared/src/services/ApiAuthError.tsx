export class ApiAuthError {
  public error: string = "";
  public error_description: string = "";
  constructor(props: ApiAuthError) {
    Object.assign(this, props);
  }
}
