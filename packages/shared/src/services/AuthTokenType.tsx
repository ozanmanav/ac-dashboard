import { UserResponse } from "./UserResponse";
export type AuthTokenType = {
  access_token: string;
  expires_in: number;
  token_type: string;
  expires_date?: Date;
} & UserResponse;
