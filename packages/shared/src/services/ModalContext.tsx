import React, {
  PropsWithChildren,
  useState,
  useMemo,
  useContext,
  useCallback,
  createContext
} from "react";

export type ModalCallback = (data: any, closeModal: () => void) => void;
export const ModalContext = createContext<{
  // modalPath: string;
  DEPRECATED_initialState: any;
  DEPRECATED_setInitialState: React.Dispatch<any>;
  setModalData?: (data: any, closeModal: () => void) => void;
  setModalCallback?: (fn: ModalCallback) => void;
  clear: () => void;
}>({
  DEPRECATED_initialState: null,
  // modalPath: "",
  clear: () => {
    throw Error("Function not implmented yet");
  },
  DEPRECATED_setInitialState: () => {
    throw Error("Function not implmented yet");
  }
});

export const ModalStatusContext = React.createContext<{
  [id: string]: ModalStatusType;
}>({});

export const ModalSetStatusContext = React.createContext<
  (id: string, statuses: ModalStatusType) => void
>(() => {});

export const ActiveModalContext = React.createContext<string | undefined>(
  undefined
);
export type ModalStatusType =
  | "willOpen"
  | "willClose"
  | "closing"
  | "opening"
  | "opened"
  | "closed";

/**
 * @param id Desired Modal's id
 * @return Return setStatus function, active modal id, and desired modal by id
 */
export function useModalStatus(id?: string) {
  const statuses = useContext(ModalStatusContext);
  const setStatus = useContext(ModalSetStatusContext);
  const active = useContext(ActiveModalContext);
  const value = useMemo<
    [
      (id: string, status: ModalStatusType) => void,
      string | undefined,
      string | undefined
    ]
  >(() => [setStatus, active, id ? statuses[id] : undefined], [
    active,
    setStatus,
    statuses,
    id
  ]);

  return value;
}

function ModalStatusContextProvider(props: PropsWithChildren<{}>) {
  const [statuses, setStatuses] = useState<{
    [id: string]: ModalStatusType;
  }>(() => ({}));
  const setStatus = useCallback(
    (id: string, status: ModalStatusType) => {
      statuses[id] !== status && setStatuses({ ...statuses, [id]: status });
    },
    // eslint-disable-next-line
    []
  );

  const activeModal = useMemo(
    () => Object.keys(statuses).find(key => statuses[key] === "opened"),
    [statuses]
  );

  return (
    <ModalStatusContext.Provider value={statuses}>
      <ModalSetStatusContext.Provider value={setStatus}>
        <ActiveModalContext.Provider value={activeModal}>
          {props.children}
        </ActiveModalContext.Provider>
      </ModalSetStatusContext.Provider>
    </ModalStatusContext.Provider>
  );
}

export function ModalContextProvider(props: PropsWithChildren<{}>) {
  const [modalCallback, setModalCallback_] = useState<ModalCallback | null>();
  // const [modalPath, setModalPath] = useState<string>("");

  const [DEPRECATED_initialState, DEPRECATED_setInitialState] = useState<any>(
    null
  );
  const [statuses, setStatuses] = useState<{
    [id: string]: ModalStatusType;
  }>({});
  const value = useMemo(
    () => ({
      // get modalPath() {
      //   return modalPath;
      // },
      get DEPRECATED_initialState() {
        return DEPRECATED_initialState;
      },
      setBackButton(fn: () => void | null) {},
      setStatuses() {},
      statuses,
      DEPRECATED_setInitialState: DEPRECATED_setInitialState,
      // setModalData: (data: any, closeModal: () => void) =>
      // modalCallback && modalCallback(data, closeModal),
      setModalCallback: (fn: ModalCallback) => setModalCallback_(() => fn),
      clear: () => {
        //TODO: The following functions rerenders the modal component with empty state. This leads to errors.
        //TODO: need a way to solve this
        // setModalCallback_(null);
        // setInitialState(null);
      }
    }),
    [DEPRECATED_initialState, statuses]
  );
  return (
    <ModalContext.Provider value={value}>
      <ModalStatusContextProvider>{props.children}</ModalStatusContextProvider>
    </ModalContext.Provider>
  );
}
