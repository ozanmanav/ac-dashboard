import { ApiError } from "./ApiError";
import { AjaxResultError } from "./AjaxResultError";

export function getErrorCodeFromAjaxErrorResult(response: AjaxResultError) {
  const error = response.error as ApiError;

  return (
    (response && response.error && error.code) ||
    "Error code not matched with any text."
  );
}
