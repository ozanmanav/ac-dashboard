import { useEffect, RefObject, MouseEventHandler } from "react";
import { checkMousePointerInElement } from "@appcircle/core/lib/utils/checkMousePointerInElement";

type GlobalClickServiceProps = {
  excludeTargetClassNames: string[];
};

export function useGlobalClickService(
  excludeTargetClassNames: string[] | null,
  rootRef: RefObject<HTMLElement>,
  onOutsideClick: (() => void) | null,
  type: "click" | "mousedown" = "click"
) {
  useEffect(() => {
    if (onOutsideClick === null) return;
    const clickHandler: MouseEventHandler = e => {
      if (
        (rootRef.current &&
          ((e.clientX < 0 && e.clientY < 0) ||
            ((e as any).layerX < 0 && (e as any).layerY < 0) ||
            ((e.target as HTMLElement) || { tagName: "" }).tagName ===
              "OPTION" ||
            ((e.target as HTMLElement) || { tagName: "" }).tagName ===
              "SELECT" ||
            (((e.target as HTMLElement) || { tagName: "" }).tagName ===
              "INPUT" &&
              (e.target as any).FileList) ||
            checkMousePointerInElement(
              e.clientX,
              e.clientY,
              rootRef.current.getBoundingClientRect()
            ))) ||
        (excludeTargetClassNames &&
          checkIfInExcludeTarget(
            e.target as HTMLElement,
            excludeTargetClassNames
          ))
      )
        return;
      onOutsideClick();
    };
    document.addEventListener(type, clickHandler as any);
    return () => {
      document.removeEventListener(type, clickHandler as any);
    };
  }, [rootRef, onOutsideClick, excludeTargetClassNames]);
}

function checkIfInExcludeTarget(
  target: HTMLElement,
  excludeClassNames: string[]
): boolean {
  if (excludeClassNames.some(classanme => target.classList.contains(classanme)))
    return true;
  if (target.parentElement)
    return checkIfInExcludeTarget(target.parentElement, excludeClassNames);
  return false;
}
