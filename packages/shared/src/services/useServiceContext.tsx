import { createServiceBoundry } from "./createService";

export const [useServiceContext, ServiceBaseProvider] = createServiceBoundry(
  process.env.REACT_APP_API_SERVER ||
    // @ts-ignore
    window.REACT_APP_API_SERVER,
  process.env.REACT_APP_AUTH_SERVER ||
    // @ts-ignore
    window.REACT_APP_AUTH_SERVER
);
