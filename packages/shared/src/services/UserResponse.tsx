export interface UserResponse {
  sub: string;
  preferred_username: string;
  picture: string;
  team_ids: string;
  active_organizationId: string;
}
