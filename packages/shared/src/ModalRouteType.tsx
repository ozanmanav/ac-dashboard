import { ModalComponentType } from "./Module";
export type ModalRouteType = {
  path: string;
  component: ModalComponentType<any, any, any>;
};
