export enum ClientMessageType {
  Hidden = 0,
  ToastMessage = 1,
  ToastMessageWithCloseButton = 2,
  InAppNotification = 3,
  TopBarNotification = 4
}
