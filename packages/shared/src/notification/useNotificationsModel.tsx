import { useContext, useMemo } from "react";
import { NotificationModelContext } from "./NotificationContext";
export function useNotificationsModel() {
  const state = useContext(NotificationModelContext);
  const model = useMemo(
    () => ({
      getNotifications() {
        return state.hookNotifications;
      },
      getMessages() {
        return state.all;
      },
      getIsNewNotificationExistInfo() {
        return state.isNewNotificationExist;
      }
    }),
    [state.all, state.hookNotifications, state.isNewNotificationExist]
  );
  return model;
}
