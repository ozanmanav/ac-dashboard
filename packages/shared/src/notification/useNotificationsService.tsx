import { useContext, useMemo } from "react";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { NotificationContext } from "./NotificationContext";
import { Message } from "../Module";

export function useNotificationsService() {
  const notifications = useContext(NotificationContext);
  const service = useMemo(
    () => ({
      pushMessage(message: Message) {
        notifications.dispatch({
          type: "app/message/add",
          payload: message
        });
      },
      pushNotification(message: Message) {
        notifications.dispatch({
          type: "app/message/add",
          payload: message
        });
      },
      removeMessage(message: Message) {
        notifications.dispatch({
          type: "app/message/delete",
          payload: message
        });
      },
      removeNotification(id: string) {
        notifications.dispatch({
          type: "app/notification/delete",
          id: id
        });
      },
      pushEvent(event: HookEvent) {
        notifications.dispatch({
          type: "app/event/add",
          payload: event
        });
      },
      markAsReadAllNotifications() {
        notifications.dispatch({
          type: "app/notifications/markAsReadAll"
        });
      }
    }),
    // eslint-disable-next-line
    []
  );
  return service;
}
