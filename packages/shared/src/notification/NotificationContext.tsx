import React, {
  createContext,
  PropsWithChildren,
  useEffect,
  useMemo,
  useReducer,
  Reducer,
  Dispatch
} from "react";
import { MessageActions } from "./MessageActions";
import produce from "immer";
import { ClientMessageType } from "../ClientMessageType";
import { Message, ModalComponentType } from "../Module";
import { ModalRouteType } from "../ModalRouteType";
import shortid from "shortid";

const initialNotifications: Message[] = JSON.parse(
  (window && window.localStorage.getItem("notifications")) || "[]"
);

export type MessageState = {
  all: Message[];
  hookNotifications: Message[];
  isNewNotificationExist: boolean;
};

export const messageReducer: Reducer<MessageState, MessageActions> = (
  state: MessageState,
  action: MessageActions
) => {
  return produce(state, draft => {
    switch (action.type) {
      case "app/message/add":
        let newMessage = action.payload;
        newMessage.id = shortid.generate();
        newMessage.isActive = true;
        newMessage.isNotification && state.hookNotifications.push(newMessage);
        draft.all.push(newMessage);
        break;
      case "app/message/delete":
        draft.all = draft.all.filter(item => item.id !== action.payload.id);
        break;
      case "app/notification/delete":
        draft.hookNotifications = draft.hookNotifications.filter(
          notification => notification.id !== action.id
        );
        break;
      case "app/notifications/load":
        draft.hookNotifications = action.payload;
        break;
      case "app/event/add":
        draft.isNewNotificationExist = true;
        if (
          action.payload.clientMessageType ===
          ClientMessageType.InAppNotification
        )
          draft.hookNotifications.unshift({
            id: shortid.generate(),
            isActive: true,
            isNotification: true,
            message: action.payload.message,
            data: action.payload,
            title: action.payload.title,
            date: action.payload.notificationSendDate
          });
        break;
      case "app/notifications/markAsReadAll":
        draft.isNewNotificationExist = false;
    }
  });
};

const initialState = {
  all: [],
  hookNotifications: initialNotifications,
  isNewNotificationExist: false
};

export const NotificationContext = createContext<{
  dispatch: Dispatch<MessageActions>;
}>({
  dispatch: () => {}
});

export const NotificationModelContext = createContext<{
  all: Message[];
  hookNotifications: Message[];
  isNewNotificationExist: boolean;
}>(initialState);

export function NotificationProvider(
  props: PropsWithChildren<{
    context: {
      registerModalRoutes: (routes: ModalRouteType[]) => void;
    };
    modal: ModalComponentType<any, any, any>;
  }>
) {
  const [state, dispatch] = useReducer(messageReducer, initialState);
  const services = useMemo(() => ({ state, dispatch }), [state]);

  useEffect(() => {
    props.context.registerModalRoutes([
      {
        path: "app/notifications",
        component: props.modal
      }
    ]);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    window &&
      window.localStorage.setItem(
        "notifications",
        JSON.stringify(state.hookNotifications)
      );
  }, [state.hookNotifications]);

  return (
    <NotificationContext.Provider value={services}>
      <NotificationModelContext.Provider value={state}>
        {props.children}
      </NotificationModelContext.Provider>
    </NotificationContext.Provider>
  );
}
