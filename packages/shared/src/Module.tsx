import { Context, ComponentType } from "react";
import { Dispatch, AnyAction, Action, Store } from "redux";
import { connect } from "react-redux";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { ModalRouteType } from "./ModalRouteType";

export type MessageType = "error" | "success" | "general";
export type Message = {
  id?: string;
  messageType?: MessageType;
  date?: string;
  message: string;
  messageId?: boolean;
  life?: number;
  isActive?: boolean;
  isNotification?: boolean;
  data?: HookEvent;
  title?: string;
};

export type Notification = {
  messageType: MessageType;
  message: string;
};

export type ModuleNavBarDataType = {
  header: string;
  items: ModuleNavBarDataItemType[];
  selectedItemIndex: number;
};

export type ModuleNavBarDataItemType = {
  length?: number;
  text: string;
  link: string;
  isSelected?: boolean;
};

export const InitialModuleContext: ModuleContext = {
  changeModuleNavSelectedIndex(index: number) {
    throw new Error("Not implemented function");
  },
  createModuleNavBar: data => {},
  getCurrentModuleNavBarData: () => {
    return {} as ModuleNavBarDataType;
  },
  registerModalRoutes: routes => {},
  getModulePath() {
    throw new Error("Not implemented function");
  },
  createLinkUrl(url) {
    throw new Error("Not implemented function");
  },
  createModalUrl(url) {
    throw new Error("Not implemented function");
  }
};

export type servicePost<T = any> = (url: string, data: any) => Promise<T>;
export type serviceGet<T = any> = (url: string, data: any) => Promise<T>;
export type ModuleProps = {
  context: ModuleContext;
};
export type ModuleContext<TStore extends Store = Store<any, any>> = {
  createModuleNavBar: (data: ModuleNavBarDataType) => void;
  getCurrentModuleNavBarData: () => ModuleNavBarDataType;
  registerModalRoutes: (routes: ModalRouteType[]) => void;
  getModulePath(): string;
  createLinkUrl(url: string): string;
  createModalUrl<TProps extends any[] = []>(
    url: string,
    ...params: PipeTuple<TProps>[]
  ): string;
  changeModuleNavSelectedIndex(index: number): void;
};

// type ExtractKeys<TProps extends { [key: string]: any }> = [
//   keyof TProps,
//   TProps[keyof TProps]
// ];

// type ExtractValues<
//   TProps extends { [key: string]: any } = any
// > = TProps[keyof TProps];

export type Module = React.ComponentType<{ context: ModuleContext }>;

// type TupleWrapper<T extends [string, any] | null = [string, any]> = T extends [
//   string,
//   any
// ]
//   ? {
//       [key in T[0]]: T[1];
//     }
//   : {};

// export type Tuple2<T extends string = string, R = any> = [T, R];
// export type TupleObj<T extends Tuple2 = [string, any]> = {
//   [key in T[0]]: T[1];
// };
// export type Tuple2Merge<T extends TupleObj[]> = T;

// export type Tuple2Pipe<
//   T extends TupleObj[],
//   Z extends TupleObj[] = [],
//   U extends TupleObj[] = [],
//   R extends TupleObj[] = [],
//   O extends TupleObj[] = [],
//   P extends TupleObj[] = []
// > = T | Z | U | R | O | P;

// type tp = [
//   Tuple2<"profileId", string>,
//   Tuple2<"profileId2", string>,
//   Tuple2<"profileId3", string>
// ];
// type tpo = TupleObj<tp>;
// type tpm = Tuple2Merge<{ profileId: string }>;
// const tpm: tpm = { profileId: 1 };

// type t = Tuple2Merge<
//   TupleObj<Tuple2<"profileId", string>>,
//   TupleObj<["commitId", string]>,
//   TupleObj<["scope", "build" | "fetch"]>,
//   TupleObj<["method", "get"]>
// >;
// KeyValTupleFromList
export type KeyValTupleFrom<T extends any[] = []> = SliceTuple<T>;

// Tuples Tests
type TestTuple = [
  "key1",
  "union type 1" | "union type 2" | "union type 3",
  "key2",
  string,
  "key3",
  number,
  "key4",
  [string, number, null | string]
];

type slicedTuple = SliceTuple<TestTuple, 2>; // [["key1", "union type 1" | "union type 2" | "union type 3"], ["key2", string], ["key3", number], ["key4", [string, number, string | null]]]
type slicedTupleItem = slicedTuple[0]; // ["name", "cenk", "asdfasdf"]
type breakedTuple = KeyValTupleFrom<TestTuple>;
type testListToTypeAlias = MergeTuple<breakedTuple>; // {key4: "some type of string";} & {key3: number;} & {key2: string;} & {key1: "type 1";};
type testListToUnion = PipeTuple<breakedTuple>; // [] | ["key1", "type 1"] | ["key2", string] | ["key3", number] | ["key4", "some type of string"]
type alias = testListToTypeAlias["key4"]; // sometype
type keys = testListToUnion[0]; // "key1" | "key2" | "key3" | "key4" | undefined
type typeValues = testListToUnion[1]; // string | number | undefined

// End of Tuples Tests
type SliceTuple<
  T extends any[],
  TMaxItem extends number = 2,
  R extends any[] = [],
  P extends any[] = [],
  I extends any[] = []
> = {
  0: SliceTuple<T, TMaxItem, R, Prepend<T[Pos<I>], P>, Next<I>>;
  1: Reverse<Prepend<Reverse<P>, R>>;
  2: SliceTuple<T, TMaxItem, Prepend<Reverse<P>, R>, [T[Pos<I>]], Next<I>>;
  // if empty value
  3: [];
}[T[0] extends undefined
  ? 3
  : Pos<I> extends Length<T>
  ? 1
  : Length<P> extends TMaxItem
  ? 2
  : 0];

type MergeTuple<
  T extends any[],
  R extends { [key: string]: any } = {},
  I extends any[] = []
> = {
  0: MergeTuple<T, { [key in T[Pos<I>][0]]: T[Pos<I>][1] } & R, Next<I>>;
  1: R;
}[Pos<I> extends [] ? 1 : Pos<I> extends Length<T> ? 1 : 0];

type PipeTuple<T extends any[], R extends any[] = [], I extends any[] = []> = {
  0: PipeTuple<T, T[Pos<I>] | R, Next<I>>;
  1: R;
}[T[0] extends undefined
  ? 1
  : Pos<I> extends []
  ? 1
  : Pos<I> extends Length<T>
  ? 1
  : 0];

// ------- Typing Operators -------
// https://github.com/pirix-gh/medium/blob/master/types-curry-ramda/src/index.ts

export type Iterator<
  Index extends number = 0,
  From extends any[] = [],
  I extends any[] = []
> = {
  0: Iterator<Index, Next<From>, Next<I>>;
  1: From;
}[Pos<I> extends Index ? 1 : 0];

export type Length<T extends any[]> = T["length"];
export type Prepend<E, T extends any[]> = ((
  head: E,
  ...args: T
) => any) extends (...args: infer U) => any
  ? U
  : T;
export type Pos<I extends any[]> = Length<I>;

export type Reverse<
  T extends any[],
  R extends any[] = [],
  I extends any[] = []
> = {
  0: Reverse<T, Prepend<T[Pos<I>], R>, Next<I>>;
  1: R;
}[Pos<I> extends Length<T> ? 1 : 0];

export type Concat<T1 extends any[], T2 extends any[]> = Reverse<
  Reverse<T1> extends infer R ? Cast<R, any[]> : never,
  T2
>;

export type Next<I extends any[]> = Prepend<any, I>;

export type Cast<X, Y> = X extends Y ? X : Y;

export type Append<E, T extends any[]> = Concat<T, [E]>;

// End of Typing Operators------

export type ModalComponentPropsType<
  TProps = any,
  TRouteState extends { [key: string]: any } = any,
  TRouteParams extends any[] = KeyValTupleFrom<[]>
> = {
  modalID?: string;
  initialState?: any;
  onModalClose: () => void;
  onModalData?: (data: any, closeModal: () => {}) => void;
  routeProps: {
    state: TRouteState;
    params: MergeTuple<TRouteParams>;
  };
} & TProps;

export type ModalComponentType<
  TProps = any,
  TRouteState = any,
  TRouteParams extends any[] = []
> = ComponentType<ModalComponentPropsType<TProps, TRouteState, TRouteParams>>;

export type DashboardModule<TContext extends ModuleContext, TProps = any> = {
  Component: (props: TProps) => any;
  Context(ctx: ModuleContext): Context<TContext>;
};

export function storeConnect<
  TStoreState = {},
  TProps = {},
  TActions extends Action = AnyAction
>(
  stateMap: (state: TStoreState) => TProps,
  dispathMap: (
    dispatch: Dispatch<TActions>
  ) => { [key: string]: () => TActions },
  component: any
) {
  return connect(stateMap, dispathMap)(component);
}

export type ModuleServiceContextType<T, R> = {
  state: T;
  services: R;
};
