import { defineMessages } from "react-intl";
import { CommonMessages } from "./common";

export default defineMessages({
  logout: {
    id: "app.actions.logout",
    defaultMessage: "Are you sure you want to log out?"
  },
  ...CommonMessages
});
