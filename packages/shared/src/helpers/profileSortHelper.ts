export function profileSortHelper<
  T extends { isTemporary: boolean; pinned: boolean }
>(a: T, b: T) {
  const l = a;
  const r = b;
  if (l.isTemporary && !r.isTemporary) return -2;
  else if (!l.isTemporary && r.isTemporary) return 2;
  else if (l.pinned && !r.pinned) return -1;
  else if (!l.pinned && r.pinned) return 1;
  return 0;
}
