import { Route, RouteProps, RouteChildrenProps } from "react-router";
import React from "react";

export function PageRoute(props: RouteProps & { children: ((props: RouteChildrenProps<any>) => React.ReactNode)}){
  return <Route exact {...props}>
    {
      (params) => 
        <div className="modulePage">
          {props.children(params)}
        </div>
    }
    </Route>
}