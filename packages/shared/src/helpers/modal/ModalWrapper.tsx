import React from "react";
import { match } from "react-router";
import { History, Location } from "history";
import { ModalComponentWrapper } from "./ModalComponentWrapper";
import { Portal } from "@appcircle/core/lib/portal/Portal";
import { ModalComponentType } from "../../Module";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
export const ModalWrapper = ({
  history,
  match,
  location,
  component
}: {
  history: History;
  component: ModalComponentType<any> | typeof Nocomp;
  match: match<any>;
  location: Location;
}) => {
  return (
    component && (
      <Portal>
        <div className="modalContainer">
          <ModalComponentWrapper
            history={history}
            location={location}
            match={match}
            component={component}
          />
        </div>
      </Portal>
    )
  );
};
