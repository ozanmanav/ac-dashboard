import React, { useContext, useEffect, useMemo, useCallback } from "react";
import { match } from "react-router";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import { Nocomp } from "@appcircle/core/lib/Nocomp";
import { FormContext } from "@appcircle/form/lib/FormContext";
import { History, Location } from "history";
import shortid from "shortid";
import { ModalComponentType } from "../../Module";
import { ModalContext, useModalStatus } from "../../services/ModalContext";

export const ModalComponentWrapper = (props: {
  component: ModalComponentType<any> | typeof Nocomp;
  history: History;
  match: match<any>;
  location: Location;
}) => {
  const {
    setModalData,
    DEPRECATED_initialState: initialState,
    clear
  } = useContext(ModalContext);
  const { state } = useContext(FormContext);
  const [setModalStatus] = useModalStatus();
  const modalID = useMemo(() => {
    return shortid.generate();
  }, []);
  useEffect(() => {
    const keyDownEventListener = (e: KeyboardEvent) => {
      if (e.keyCode === KeyCode.ESC_KEY) {
        onModalClose();
      }
      return false;
    };
    document.addEventListener("keydown", keyDownEventListener);
    return () => document.removeEventListener("keydown", keyDownEventListener);
    // eslint-disable-next-line
  }, [state, modalID]);
  const close = useCallback(() => {
    props.history.push(
      props.history.createHref({ ...props.history.location, search: "" })
    );
    clear();
  }, [clear, props.history]);
  const Component = useMemo(() => props.component || Nocomp, [props.component]);

  const onModalClose = useCallback(
    (force?: boolean) => {
      close();
    },
    [state, modalID]
  );
  useEffect(() => {
    setModalStatus(modalID, "opened");
    return () => setModalStatus(modalID, "closed");
  }, [modalID, setModalStatus]);
  const params = useMemo(() => {
    return (
      props.location.search.match(new RegExp("([^?=&]+)(=([^&]*))?", "g")) || []
    ).reduce<{
      [key: string]: string;
    }>(function(result, each, n, every) {
      let [key, value] = each.split("=");
      result[key] = value;
      return result;
    }, {});
  }, [props.location.search]);
  return (
    <Component
      modalID={modalID}
      routeProps={{
        state: props.location.state,
        params
      }}
      initialState={initialState}
      onModalData={setModalData}
      onModalClose={onModalClose}
    />
  );
};
