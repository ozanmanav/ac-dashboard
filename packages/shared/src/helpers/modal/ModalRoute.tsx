import { Route } from "react-router";
import { PropsWithChildren, useMemo, ComponentProps } from "react";
import React from "react";
import { ModalRouteType } from "../../ModalRouteType";
import { ModalWrapper } from "./ModalWrapper";
import { Nocomp } from "@appcircle/core/lib/Nocomp";

export function ModalRoute(
  props: PropsWithChildren<{ prefix: string; modalRoutes: ModalRouteType[] }>
) {
  return (
    <Route
      key="modal"
      path={"/**/"}
      render={({ match, location, history }) => {
        return (
          location.search.indexOf("modal=" + (props.prefix || "")) > -1 && (
            <ModalRouteWrapper
              match={match}
              location={location}
              history={history}
              modalRoutes={props.modalRoutes}
            />
          )
        );
      }}
    />
  );
}

function ModalRouteWrapper(
  props: Omit<ComponentProps<typeof ModalWrapper>, "component"> & {
    modalRoutes: ModalRouteType[];
  }
) {
  const component = useMemo(() => {
    let component = null;

    if (props.location.search.indexOf("modal=") > -1) {
      props.modalRoutes.some((route, index) => {
        if (props.location.search.indexOf("modal=" + route.path) > -1) {
          component = route.component;
          return true;
        }
        return false;
      });
      return component;
    }
    return null;
  }, [props.location.search, props.modalRoutes]);

  return (
    <ModalWrapper
      match={props.match}
      location={props.location}
      history={props.history}
      component={component || Nocomp}
    />
  );
}
