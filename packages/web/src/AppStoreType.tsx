import { AppStateType } from "./reducers";
import { Store } from "redux";
import { AppActions } from "./reducers/AppActions";
export type AppStoreType = Store<AppStateType, AppActions>;
