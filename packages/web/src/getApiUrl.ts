export function getApiUrl() {
  return (
    process.env.REACT_APP_API_SERVER ||
    // @ts-ignore
    window.REACT_APP_API_SERVER
  );
}

export function getAuthApiUrl() {
  return (
    process.env.REACT_APP_AUTH_SERVER ||
    // @ts-ignore
    window.REACT_APP_AUTH_SERVER
  );
}

export function getLanguageUrl(lang: string) {
  return getApiUrl() + "/languages/" + lang + ".json";
}
