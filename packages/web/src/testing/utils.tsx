import React from "react";
import { Router } from "react-router";
import { createMemoryHistory } from "history";
import { Provider } from "react-redux";
import { render } from "react-testing-library";
import "jest-dom/extend-expect";
import "react-testing-library/cleanup-after-each";
import "jest-dom/extend-expect";
import { store } from "../App";
import { AppContextProvider } from "AppContext";

function renderWithRouter(
  children: JSX.Element,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] }),
    ...renderOptions
  }: { route?: string; history?: any } = {}
) {
  return {
    ...render(
      <Router history={history}>
        <Provider store={store}>
          <AppContextProvider store={store}>{children}</AppContextProvider>
        </Provider>
      </Router>,
      renderOptions
    ),
    history
  };
}

// re-export everything
export * from "react-testing-library";

// override render method
export { renderWithRouter as render };
