import { createContext, PropsWithChildren, useState, useContext } from "react";
import { AppStoreType } from "./AppStoreType";
import { createModuleContext } from "./createModuleContext";
import { ServiceBaseProvider } from "@appcircle/shared/lib/services/useServiceContext";
import { AuthServiceProvider } from "modules/auth/service/AuthService";
import React from "react";
import { ModuleContext } from "@appcircle/shared/lib/Module";
import { FormContextProvider } from "@appcircle/form/lib/FormContextProvider";
import { ModuleContextProvider } from "AppModuleContext";
import {
  TaskManagerProvider,
  MiddlewareFn
} from "@appcircle/taskmanager/lib/TaskManagerProvider";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { HookProvider } from "@appcircle/sse/lib/HookProvider";
import { Observable } from "rxjs";
import { useHookService } from "useHookService";

export type AppContextType = ModuleContext<AppStoreType>;
export function createAppContext(store: AppStoreType): ModuleContext {
  return {
    ...createModuleContext("", store)
  };
}

const middlewares: MiddlewareFn[] = [
  (task: Observable<any>, props: TaskPropsType) => {
    return task;
  }
];

export const AppContext = createContext({});
export const AppContextProvider = (
  props: PropsWithChildren<{ store: AppStoreType }>
) => {
  const [services] = useState(() => createAppContext(props.store));
  return (
    // Application logic
    <AppContext.Provider value={services}>
      {/* Module Layer. Active module's context */}
      <ModuleContextProvider context={services}>
        {/* Service layer */}
        <ServiceBaseProvider>
          {/* Application UI Layer */}
          <AppContainer>{props.children}</AppContainer>
        </ServiceBaseProvider>
      </ModuleContextProvider>
    </AppContext.Provider>
  );
};

function AppContainer(props: PropsWithChildren<{}>) {
  // connects ui-server and returns a stream to listen Server-Sent-Events
  const hookListener = useHookService();

  return (
    <HookProvider hookListener$={hookListener}>
      <TaskManagerProvider middlewares={middlewares}>
        <AuthServiceProvider>
          <FormContextProvider>{props.children}</FormContextProvider>
        </AuthServiceProvider>
      </TaskManagerProvider>
    </HookProvider>
  );
}
