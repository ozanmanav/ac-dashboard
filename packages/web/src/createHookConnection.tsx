import { createHookServerBoundry } from "@appcircle/shared/lib/services/Client";
export const createHookConnection = createHookServerBoundry(
  process.env.REACT_APP_UI_SERVER || window.REACT_APP_UI_SERVER
);
