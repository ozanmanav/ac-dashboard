import { ModuleContext } from "@appcircle/shared/lib/Module";
import { createQueryString } from "@appcircle/core/lib/utils/createQueryString";
import { AppStoreType } from "./AppStoreType";
import { join } from "@appcircle/core/lib/utils/join";
import { addSlashtoPath } from "@appcircle/core/lib/utils/addSlashtoPath";
// TODO: Create Notfoccation Model
export function createModuleContext(
  modulePath: string,
  store: AppStoreType
): ModuleContext<AppStoreType> {
  function createLinkUrl(url: string) {
    return join(addSlashtoPath(modulePath), addSlashtoPath(url));
  }
  store.dispatch({
    type: "app/module/inititalize",
    payload: addSlashtoPath(modulePath)
  });
  return {
    changeModuleNavSelectedIndex(index: number) {
      store.dispatch({
        type: "app/module-nav/selectedIndex/update",
        index
      });
    },
    createModuleNavBar(data) {
      const newItems = data.items.map(item => ({
        ...item,
        link: item.link
      }));
      store.dispatch({
        type: "create-module-navbar-data",
        data: {
          items: newItems,
          header: data.header,
          selectedItemIndex: data.selectedItemIndex
        }
      });
    },
    getCurrentModuleNavBarData() {
      return store.getState().moduleNavBar;
    },
    registerModalRoutes(routes) {
      return store.dispatch({
        type: "app/modal/register",
        payload: { modulePath: addSlashtoPath(modulePath), routes }
      });
    },
    getModulePath() {
      return modulePath;
    },
    createLinkUrl(url) {
      return createLinkUrl(url);
    },
    createModalUrl(url, ...params) {
      return createQueryString(["modal", createLinkUrl(url)], ...params);
    }
  };
}
