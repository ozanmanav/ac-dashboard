import { moduleNavBarReducer } from "./moduleNavbarReducer";
import { mainNavBarReducer } from "./mainNavBarData";
import { modalRoutesReducer } from "./modalRoutes";
import { loginReducer } from "./loginReducer";
import { userInfoReducer } from "./userInfoReducer";
import { combineReducers } from "redux";

export const AppReducers = combineReducers({
  moduleNavBar: moduleNavBarReducer,
  mainNavBar: mainNavBarReducer,
  modalRoutes: modalRoutesReducer,
  login: loginReducer,
  userInfo: userInfoReducer
});

export type AppStateType = ReturnType<typeof AppReducers>;
