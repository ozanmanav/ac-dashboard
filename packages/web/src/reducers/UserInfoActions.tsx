import { UserResponse } from "@appcircle/shared/lib/services/UserResponse";

export type UserInfoActions =
  | {
      type: "ADD_USER_INFO_SUCCESS";
      userInfo: UserResponse;
    }
  | {
      type: "REMOVE_USER_INFO";
    };
