import { ModuleNavBarDataItemType } from "@appcircle/shared/lib/Module";
export type ModuleNavBarActions =
  | {
      type: "app/module-nav/change-selected-by-path";
      path: string;
    }
  | {
      type: "app/module-nav/selectedIndex/update";
      index: number;
    }
  | {
      type: "create-module-navbar-data";
      data: {
        items: ModuleNavBarDataItemType[];
        header: string;
        selectedItemIndex: number;
      };
    }
  | {
      type: "app/module/inititalize";
      payload: string;
    };
