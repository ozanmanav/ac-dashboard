import { ModalRouteType } from "@appcircle/shared/lib/ModalRouteType";
import produce from "immer";

export type ModalRoutesState = ModalRouteType[];
export type RegisterModalsAction = {
  type: "app/modal/register";
  payload: { routes: ModalRouteType[]; modulePath: string };
};

export function modalRoutesReducer(
  state: ModalRoutesState = [],
  action: RegisterModalsAction
) {
  return produce(state, draft => {
    switch (action.type) {
      case "app/modal/register":
        const { routes, modulePath } = action.payload;
        routes.forEach(route =>
          draft.push({
            path: modulePath + route.path,
            component: route.component
          })
        );
        break;
    }
  });
}
