export type LoginActions =
  | {
      type: "LOG_IN_SUCCESS";
      token: string;
    }
  | {
      type: "LOG_IN_ERROR";
      error: string;
    }
  | {
      type: "LOG_IN_REQUEST";
      user: {
        email: string;
        pass: string;
      };
    };
