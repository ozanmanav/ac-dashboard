import produce from "immer";
import { Reducer } from "redux";
import { LoginActions } from "./LoginActions";

export interface LoginState {
  isLoggedIn: boolean;
  token: string;
}

const initialUserState: LoginState = {
  isLoggedIn: false,
  token: ""
};

// TODO: Remove this solution to use useUserService everywhere
export const loginReducer: Reducer<LoginState, LoginActions> = (
  state = initialUserState,
  action
): LoginState => {
  return produce(state, draft => {
    switch (action.type) {
      case "LOG_IN_SUCCESS":
        draft.isLoggedIn = !!action.token;
        draft.token = action.token;
        break;
      case "LOG_IN_REQUEST":
        draft.isLoggedIn = true;
        break;
    }
  });
};
