import produce from "immer";
import { ModuleNavBarActions } from "./ModuleNavBarActions";
import { ModuleNavBarDataItemType } from "@appcircle/shared/src/Module";

export type ModuleNavBarState = {
  module: string;
  header: string;
  items: any[];
  selectedItemIndex: number;
  selectedItem: ModuleNavBarDataItemType;
};

const initialState: ModuleNavBarState = {
  header: "",
  items: [],
  selectedItemIndex: 0,
  selectedItem: {
    text: "",
    link: ""
  },
  module: ""
};
export const moduleNavBarReducer = (
  state: ModuleNavBarState = initialState,
  action: ModuleNavBarActions
) => {
  return produce(state, draft => {
    switch (action.type) {
      case "app/module/inititalize":
        draft.module = action.payload;
        break;
      case "create-module-navbar-data":
        draft.items = action.data.items;
        draft.header = action.data.header;
        draft.selectedItemIndex = action.data.selectedItemIndex;
        draft.selectedItem = action.data.items[action.data.selectedItemIndex];
        break;
      case "app/module-nav/selectedIndex/update":
        if (draft.selectedItemIndex !== action.index) {
          draft.selectedItemIndex = action.index;
          draft.selectedItem = draft.items[action.index];
        }

        break;
    }
  });
};
