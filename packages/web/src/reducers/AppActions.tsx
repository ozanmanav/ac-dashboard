import { ModuleNavBarActions } from "./ModuleNavBarActions";
import { MainNavBarActions } from "./mainNavBarData";
import { RegisterModalsAction } from "./modalRoutes";
import { LoginActions } from "./LoginActions";
import { UserInfoActions } from "./UserInfoActions";
export type AppActions =
  | RegisterModalsAction
  | LoginActions
  | UserInfoActions
  | ModuleNavBarActions
  | MainNavBarActions;
