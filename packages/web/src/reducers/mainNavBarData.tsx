import { FunctionComponent, SVGProps } from "react";
import { setIdsTo } from "@appcircle/core/lib/utils";
import produce from "immer";
import { SvgHelp } from "@appcircle/assets/lib/Help";
import { SvgSettings } from "@appcircle/assets/lib/Settings";
import { SvgNotification } from "@appcircle/assets/lib/Notification";
import { SvgDistribute } from "@appcircle/assets/lib/Distribute";
import { SvgBuild } from "@appcircle/assets/lib/Build";
import { SvgSidIdle } from "icons/SidIdle";
import { SvgAppstore } from "@appcircle/assets/lib/Appstore";
export type MainNavBarItemType = {
  link: string;
  icon: FunctionComponent<SVGProps<SVGSVGElement>>;
  setSelectedItemIndex: () => void;
  id?: any;
  name?: "notifications";
  openMenu?: boolean;
  toolTipText?: string;
};

export interface MainNavBarState {
  topItems: MainNavBarItemType[];
  bottomItems: MainNavBarItemType[];
  selectedItemIndex: number;
}

// export interface IChangeSelectTopItemIndexAction
//   extends Action<typeof CHANGE_SELECTED_TOP_ITEM_INDEX> {
//   readonly index: number;
// }

const initialState: MainNavBarState = {
  topItems: [
    {
      link: "/build",
      icon: SvgBuild,
      setSelectedItemIndex: () => {},
      toolTipText: "Build"
    },
    {
      link: "/signing_identities",
      icon: SvgSidIdle,
      setSelectedItemIndex: () => {},
      toolTipText: "Signing Identities"
    },
    {
      link: "/distribute",
      icon: SvgDistribute,
      setSelectedItemIndex: () => {},
      toolTipText: "Distribute"
    },
    {
      link: "/store_submit",
      icon: SvgAppstore,
      setSelectedItemIndex: () => {},
      toolTipText: "Store Submission"
    }
  ],
  bottomItems: [
    {
      link: "/help",
      icon: SvgHelp,
      setSelectedItemIndex: () => {},
      toolTipText: "Help"
    },
    {
      link: "?modal=/app/notifications",
      icon: SvgNotification,
      setSelectedItemIndex: () => {},
      openMenu: true,
      toolTipText: "Notifications",
      name: "notifications"
    },
    {
      link: "/settings",
      icon: SvgSettings,
      setSelectedItemIndex: () => {},
      toolTipText: "Settings"
    }
  ],
  selectedItemIndex: 2
};

export type MainNavBarActions = {
  type: "app/navbar/selectedIndex/update";
  index: number;
};

setIdsTo(initialState.topItems);
setIdsTo(initialState.bottomItems);

export function mainNavBarReducer(
  state: MainNavBarState = initialState,
  action: MainNavBarActions
) {
  return produce(state, draft => {
    switch (action.type) {
      case "app/navbar/selectedIndex/update":
        draft.selectedItemIndex = action.index;
        break;
    }
  });
}
