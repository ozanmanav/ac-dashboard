import { UserInfoActions } from "./UserInfoActions";
import { UserResponse } from "@appcircle/shared/lib/services/UserResponse";

const initialUserState: UserResponse | null = null;

export function userInfoReducer(
  state: UserResponse | null = initialUserState,
  action: UserInfoActions
): UserResponse | null {
  switch (action.type) {
    case "ADD_USER_INFO_SUCCESS":
      return { ...state, ...action.userInfo };
    case "REMOVE_USER_INFO":
      return null;
    default:
      return state;
  }
}
