import { createStore, combineReducers } from "redux";

export function createRootStore(reducers: {}) {
	return createStore(combineReducers(reducers));
}
