import React, { Suspense, useContext, useEffect } from "react";
import "./App.scss";
import { Route, RouteComponentProps, Switch, Redirect } from "react-router-dom";
import "@formatjs/intl-relativetimeformat/polyfill";
import { Layout } from "./shared/layout/MainLayout";
import { ProfileModule } from "./modules/profiles";
import { Provider, useStore } from "react-redux";
import { EnterpriseAppStoreModule } from "./modules/enterprise";
import { AppStoreModule } from "./modules/appstore";
import { SingleLinkModule } from "./modules/singlelink";
import { AppReducers, AppStateType } from "./reducers";
import { createStore, Store } from "redux";
import { AuthPagesProvider } from "@appcircle/shared/lib/services/AuthPages";
import { ToastService } from "@appcircle/ui/lib/ToastService";
import ConnectionStatusBar from "@appcircle/ui/lib/ConnectionStatusBar";
import { SupportModule } from "modules/support";
import { AccountModule } from "modules/account";
import "simplebar"; // or "import SimpleBar from 'simplebar';" if you want to use it manually.
import { useIntl } from "react-intl";
import Cookie from "js-cookie";
import messages from "@appcircle/shared/lib/messages/messages";
import { SessionExpiredModalView } from "modules/account/SessionExpiredModalView";
import { PageRoute } from "@appcircle/shared/lib/helpers/PageRoute";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { SettingsModule } from "modules/settings";
import { createModuleContext } from "./createModuleContext";
import { slashCount } from "@appcircle/core/lib/utils/slashCount";
import { AppActions } from "reducers/AppActions";
import { Login } from "modules/auth/Login";
import { SignUp } from "modules/auth/SignUp";
import { ResetPassword } from "modules/auth/ResetPassword";
import { ForgotPassword } from "modules/auth/ForgotPassword";
import { AuthServiceContext } from "modules/auth/service/AuthService";
import { AppI18NContext } from "./AppI18NContext";
import {
  DisconectedProvider,
  useDisconnected
} from "@appcircle/shared/lib/services/DisconnectedContext";
import { useGTMService } from "shared/domain/useGTMService";

// export const rootReducer = AppReducers;
export const locale = Cookie.get("locale") || "en";

// LazyLoad Modules definitions
const BuildModule = React.lazy(() => import("modules/build/BuildModule"));
const DistributeModule = React.lazy(() =>
  import("modules/distribute/DistributeModule")
);
const SigningIdentitiesModule = React.lazy(() =>
  import("modules/signing_identities/SIDModule")
);
const StoreSubmitModule = React.lazy(() =>
  import("modules/store_submit/StoreSubmitModule")
);

export enum AppModalPaths {
  sessionExpired = "/app/modal/auth/sessionExpired"
}

export const store: Store<AppStateType, AppActions> = createStore<
  AppStateType,
  AppActions,
  {},
  {}
>(AppReducers, {
  modalRoutes: [
    { path: AppModalPaths.sessionExpired, component: SessionExpiredModalView }
  ]
} as any);
export const lang = {};
function AppRoot() {
  return (
    <Provider store={store}>
      <DisconectedProvider>
        <AppI18NContext />
      </DisconectedProvider>
    </Provider>
  );
}
// state ac moduleData setState,
export function App(props: RouteComponentProps) {
  const intl = useIntl(),
    store = useStore<AppStateType, AppActions>(),
    name = props.location.pathname.split("/")[1];
  const { token } = useContext(AuthServiceContext);
  const [{ isDisconnected, statusText }] = useDisconnected();

  useGTMService({ initialize: true });

  return (
    <div
      className={
        isDisconnected ? "BodyContainer BodyContainer--narrow" : "BodyContainer"
      }
    >
      <ConnectionStatusBar
        isDisconnected={isDisconnected}
        statusText={statusText}
      />
      <div className="toastContainer">
        <ToastService />
      </div>
      <AuthPagesProvider
        isLoggedIn={!!token?.access_token}
        Login={Login}
        SignUp={SignUp}
        ResetPassword={ResetPassword}
        ForgotPassword={ForgotPassword}
        clearUserData={() => store.dispatch({ type: "REMOVE_USER_INFO" })}
      >
        <Layout
          history={props.history}
          match={props.match}
          location={props.location}
          isHome={slashCount(props.location.pathname) <= 3}
        >
          <TransitionGroup className="transitionWrapper">
            <CSSTransition key={name} classNames="modulePage" timeout={300}>
              <Switch location={props.location}>
                <PageRoute path="/_profiles_">
                  {() => (
                    <ProfileModule
                      context={createModuleContext("_profiles_", store)}
                    />
                  )}
                </PageRoute>
                <PageRoute path="/build">
                  {() => {
                    store.dispatch({
                      type: "app/navbar/selectedIndex/update",
                      index: 0
                    });
                    return (
                      <Suspense
                        fallback={
                          <div className="loaderFallback">
                            {intl.formatMessage(messages.appCommonLoading)}
                          </div>
                        }
                      >
                        <BuildModule
                          context={createModuleContext("build", store)}
                        />
                      </Suspense>
                    );
                  }}
                </PageRoute>
                <PageRoute path="/signing_identities">
                  {() => {
                    store.dispatch({
                      type: "app/navbar/selectedIndex/update",
                      index: 1
                    });
                    return (
                      <Suspense
                        fallback={
                          <div className="loaderFallback">
                            {intl.formatMessage(messages.appCommonLoading)}
                          </div>
                        }
                      >
                        <SigningIdentitiesModule
                          context={createModuleContext(
                            "signing_identities",
                            store
                          )}
                        />
                      </Suspense>
                    );
                  }}
                </PageRoute>
                <PageRoute path="/distribute">
                  {() => {
                    store.dispatch({
                      type: "app/navbar/selectedIndex/update",
                      index: 2
                    });
                    return (
                      <Suspense
                        fallback={
                          <div className="loaderFallback">
                            {intl.formatMessage(messages.appCommonLoading)}
                          </div>
                        }
                      >
                        <DistributeModule
                          context={createModuleContext("distribute", store)}
                        />
                      </Suspense>
                    );
                  }}
                </PageRoute>
                <PageRoute path="/store_submit">
                  {() => {
                    store.dispatch({
                      type: "app/navbar/selectedIndex/update",
                      index: 3
                    });
                    return (
                      <Suspense
                        fallback={
                          <div className="loaderFallback">
                            {intl.formatMessage(messages.appCommonLoading)}
                          </div>
                        }
                      >
                        <StoreSubmitModule
                          context={createModuleContext("store_submit", store)}
                        />
                      </Suspense>
                    );
                  }}
                </PageRoute>

                <PageRoute path="/enterprise">
                  {() => (
                    <EnterpriseAppStoreModule
                      context={createModuleContext("enterprise", store)}
                    />
                  )}
                </PageRoute>
                <PageRoute path="/buappstoreild">
                  {() => (
                    <AppStoreModule
                      context={createModuleContext("buappstoreild", store)}
                    />
                  )}
                </PageRoute>
                <PageRoute path="/singlelink">
                  {() => (
                    <SingleLinkModule
                      context={createModuleContext("singlelink", store)}
                    />
                  )}
                </PageRoute>
                <Route
                  exact
                  path="(/|/login)"
                  render={() => <Redirect to="/build/profiles" />}
                />
                <Route
                  path="/support"
                  render={() => (
                    <SupportModule
                      context={createModuleContext("support", store)}
                    />
                  )}
                />
                <Route
                  path="/settings"
                  render={() => {
                    store.dispatch({
                      type: "app/navbar/selectedIndex/update",
                      index: 5
                    });
                    return (
                      <SettingsModule
                        context={createModuleContext("settings", store)}
                      />
                    );
                  }}
                />
                <Route
                  path="/account"
                  render={() => (
                    <AccountModule
                      context={createModuleContext("account", store)}
                    />
                  )}
                />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </Layout>
      </AuthPagesProvider>
    </div>
  );
}
export default AppRoot;
