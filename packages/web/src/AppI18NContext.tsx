import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { AppContextProvider } from "./AppContext";
import { IntlProvider } from "react-intl";
import { getLanguageUrl } from "getApiUrl";
import { lang, locale, store, App } from "./App";
export function AppI18NContext() {
  const [messages, setMessages] = useState(lang);
  useEffect(() => {
    fetch(getLanguageUrl("en-us"), {
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(resp => {
        return resp.json();
      })
      .then(json => {
        setMessages(json);
        return;
      });
  }, []);
  console.log(messages);
  return (
    <IntlProvider locale={locale} messages={messages}>
      <Router>
        <AppContextProvider store={store}>
          <Route path="/:filter?" component={App} />
        </AppContextProvider>
      </Router>
    </IntlProvider>
  );
}
