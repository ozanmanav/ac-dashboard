import React, { useMemo, useContext, useCallback, useEffect } from "react";
import { ModalWindow, ModalWindowType } from "@appcircle/ui/lib/ModalWindow";
import { ReactComponent as NoItems } from "./icon/notification-icon.svg";
import { Message } from "@appcircle/shared/lib/Module";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import moment from "moment";
import { TinyCloseButton } from "@appcircle/ui/lib/TinyCloseButton";
import { ClientAlertType } from "@appcircle/shared/lib/ClientAlertType";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useNotificationsModel } from "@appcircle/shared/lib/notification/useNotificationsModel";
export type NotificationModalProps = ModalWindowType<{}>;
export type NoticationItemStatusType = "error" | "success" | "warning";
function NotificationListItem(props: {
  message?: string;
  title: string;
  id: string;
  type: NoticationItemStatusType;
  date: string;
  onDelete: (id: string) => void;
}) {
  const onDelete = useCallback(() => props.onDelete(props.id), [props]);
  return (
    <div className="NotificationListItem">
      <div className="NotificationListItem_body">
        <header
          className={
            "NotificationListItem_header NotificationListItem_header-" +
            props.type
          }
        >
          {props.title}
        </header>
        <div className="NotificationListItem_message">{props.message}</div>
        <div className="NotificationListItem_calendar">
          {useMemo(() => moment(props.date).calendar(), [props.date])}
        </div>
      </div>
      <div className="NotificationListItem_tools">
        <TinyCloseButton onClick={onDelete} />
      </div>
    </div>
  );
}

const alerts: { [key: number]: string } = {
  [ClientAlertType.Danger]: "Fail",
  [ClientAlertType.Success]: "Success",
  [ClientAlertType.Warning]: "Warning"
};

const styles: { [key: number]: NoticationItemStatusType } = {
  [ClientAlertType.Danger]: "error",
  [ClientAlertType.Success]: "success",
  [ClientAlertType.Warning]: "warning"
};

function NotificationList({ data }: { data: Message[] }) {
  const service = useNotificationsService();
  const onDelete = useCallback(
    (id: string) => {
      service.removeNotification(id);
    },
    [service]
  );

  useEffect(() => {
    service.markAsReadAllNotifications();
  }, [service]);

  return (
    <div className="NotificationList" data-simplebar>
      <div>
        {data.map(message => {
          const alert =
            message.data && isEmptyValue(message.data.clientAlertType)
              ? message.data.clientAlertType
              : ClientAlertType.Success;
          return (
            !!message &&
            !!message.data && (
              <NotificationListItem
                key={message.id}
                id={message.id || ""}
                type={styles[alert]}
                title={message.title || message.data.title || alerts[alert]}
                message={message.message}
                date={message.date || ""}
                onDelete={onDelete}
              />
            )
          );
        })}
      </div>
    </div>
  );
}

export function NotificationModal(props: NotificationModalProps) {
  const { getNotifications } = useNotificationsModel();

  const hookNotifications = getNotifications();

  const notifications = useMemo(() => hookNotifications, [hookNotifications]);

  return (
    <ModalWindow header="Notifications" onModalClose={props.onModalClose}>
      <div className="NotificationModal">
        {notifications && notifications.length ? (
          <NotificationList data={notifications} />
        ) : (
          <div className="NotificationModal_noItems">
            <NoItems />
            <div>You have no notifications</div>
          </div>
        )}
      </div>
    </ModalWindow>
  );
}
