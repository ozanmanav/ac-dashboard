import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type SupportContextType = ModuleContext & {};

export const SupportContext = createContext<SupportContextType>({
  ...InitialModuleContext
});
