import React, { useEffect } from "react";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";
import { SupportContext } from "./SupportContext";
import { Support } from "./Support";

const buildModuleNavBarData: ModuleNavBarDataType = {
  header: "Support",
  items: [
    {
      text: "Guides & Reference",
      link: "/guides_reference"
    },
    {
      text: "Videos",
      link: "/videos"
    },
    {
      text: "Release Notes",
      link: "/release_notes"
    },
    {
      text: "Blog",
      link: "/blog"
    },
    {
      text: "Feedback",
      link: "/feedback"
    }
  ],
  selectedItemIndex: 0
};

export function SupportModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted SupportModule.");
    props.context.createModuleNavBar(buildModuleNavBarData);
    return () => console.log("Unmounting SupportModule ...");
  }, []);
  return (
    <SupportContext.Provider value={{ ...props.context }}>
      <Support navbarData={buildModuleNavBarData} />
    </SupportContext.Provider>
  );
}
