import { useContext } from "react";
import React from "react";
import { EmptyModule } from "../EmptyModule";
import { SupportContext } from "./SupportContext";

export function Support(props: any) {
  const { createLinkUrl } = useContext(SupportContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
