import { useContext } from "react";
import { RemoteAppUpdateContext } from "./RemoteAppUpdateContext";
import React from "react";
import { EmptyModule } from "../EmptyModule";

export function RemoteAppUpdate(props: any) {
  const { createLinkUrl } = useContext(RemoteAppUpdateContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
