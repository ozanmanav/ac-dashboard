import { createContext } from "react";
import { ModuleContext, InitialModuleContext } from "@appcircle/shared/lib/Module";

type RemoteAppUpdateContextType = ModuleContext & {};

export const RemoteAppUpdateContext = createContext<RemoteAppUpdateContextType>(
	{
		...InitialModuleContext
	}
);
