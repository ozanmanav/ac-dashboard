import React, { useEffect } from "react";
import { RemoteAppUpdate } from "./RemoteAppUpdate";
import { RemoteAppUpdateContext } from "./RemoteAppUpdateContext";
import { ModuleNavBarDataType, ModuleProps } from "@appcircle/shared/lib/Module";

const remoteAppUpdateModuleNavBarData: ModuleNavBarDataType = {
  header: "Remote App Update",
  items: [
    {
      text: "Profiles",
      link: "/profiles"
    },
    {
      text: "Groups",
      link: "/groups"
    },
    {
      text: "Reports",
      link: "/reports"
    },
    {
      text: "Start Guide",
      link: "/startGuide"
    }
  ],
  selectedItemIndex: 0
};

export function RemoteAppUpdateModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted RemoteAppUpdate.");
    props.context.createModuleNavBar(remoteAppUpdateModuleNavBarData);
    return () => console.log("Unmounting RemoteAppUpdate ...");
  }, []);
  return (
    <RemoteAppUpdateContext.Provider value={props.context}>
      <RemoteAppUpdate navbarData={remoteAppUpdateModuleNavBarData} />
    </RemoteAppUpdateContext.Provider>
  );
}
