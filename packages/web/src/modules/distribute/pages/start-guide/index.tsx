import React from "react";
import { StartGuideLayout } from "shared/startguides/StartGuideLayout";

export default () => (
  <StartGuideLayout>
    <h1 className="content_title">
      Getting Started with the Distribute Module
    </h1>
    <p>
      New to Appcircle Distribute module? Follow our quick start guide to
      preview and share your mobile applications easily.
    </p>
    <p>
      The Distribute module allows you to preview your apps in your browser and
      distribute them for testing. This module provides you a platform to serve
      binaries with detailed management and tracking of applications, versions,
      testers and teams.
    </p>

    <div className="callout">
      With the Distribute module, you can automate your continuous delivery
      processes to achieve a NoOps environment.
    </div>

    <h4 className="section_title">1.Create testing groups</h4>
    <p>
      Create groups of testers for easy sharing in the Testing Groups submodule.
      You can share your apps with groups without the need for entering emails
      every time.
    </p>

    <img className="screen" src="/assets/img/Testing-Groups.png" />

    <h4 className="section_title">2. Create a distribution profile</h4>
    <p>By providing a binary through</p>

    <ul>
      <li>Manual upload of IPA or APK files for iOS and Android</li>
    </ul>
    <div className="callout">
      All kinds of apps developed with any framework are supported for
      distribution.
    </div>

    <p>or</p>

    <ul>
      <li>Deployment from the Appcircle Build module</li>
    </ul>

    <div className="callout">
      You can automate deployments from the Build module to the Distribute
      module.
    </div>

    <img className="screen" src="/assets/img/Distribute-Module.png" />

    <h4 className="section_title">3. Preview your app on a virtual device</h4>
    <p>
      If you deploy your app through the Appcircle Build module, you can preview
      your app on a virtual device in your browser.
    </p>
    <p>
      Simply press preview and you can run your app on different iOS and Android
      devices and on different OS versions.
    </p>

    <div className="callout">
      You can preview your applications in your browser instantly without the
      need for an actual device and downloads.
    </div>

    <img className="screen" src="/assets/img/Preview-on-Device.png" />

    <h4 className="section_title">
      4. Select the authentication method for sharing
    </h4>
    <p>
      Select an authentication method that suits your distribution preferences:
    </p>
    <ul>
      <li>No authentication</li>
      <li>Static user name and password</li>
      <li>With user-specific enrollment</li>
    </ul>
    <div className="callout">
      With enrollment, the users can access multiple apps using a single user
      name and password.
    </div>
    <img className="screen" src="/assets/img/Authentication-Settings.png" />

    <h4 className="section_title">5. Share your application with testers</h4>
    <p>
      Add testing groups or individual testers for sharing your app along with
      the release notes. You can then track progress of each tester individually
      as they download the app.
    </p>
    <div className="callout">
      You can automate distribution to the testers using tags on the versions.
    </div>

    <img className="screen" src="/assets/img/Distribute-Module-2.png" />
  </StartGuideLayout>
);
