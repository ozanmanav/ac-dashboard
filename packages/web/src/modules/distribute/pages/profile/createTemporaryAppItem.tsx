import { DistributeProfileAppType } from "modules/distribute/model/DistributeAppVersion";
import { OS } from "@appcircle/core/lib/enums/OS";

export default function createTemporaryAppItem(
  os: OS,
  id?: string
): DistributeProfileAppType {
  return {
    id: id,
    version: `NA`,
    size: `- MB`,
    uploadTime: new Date().toDateString(),
    testers: [],
    bundleName: "",
    tags: [],
    sendingDate: "",
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300",
    selectedTesters: [],
    message: "",
    platformType: os,
    name: "",
    appPlayerUrl: null,
    appSimulatorResourceId: "",
    buildId: "",
    profileId: "",
    appResourceId: "",
    appIconResourceId: "",
    uniqueName: "",
    fileSize: 0,
    appVersionTestings: [],
    createDate: "",
    updateDate: "",
    lastSharedDate: null,
    versionTags: [],
    iconUrl: ""
  } as DistributeProfileAppType;
}
