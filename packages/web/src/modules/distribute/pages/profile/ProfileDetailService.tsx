import {
  TaskPropsType,
  useTaskManager
} from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ConsumerFactoryFn } from "@appcircle/shared/lib/services/createService";
import { TagComponentProps } from "@appcircle/ui/lib/TagComponent";
import {
  DistributeProfileAppType,
  DistributeProfileAppResponseType
} from "modules/distribute/model/DistributeAppVersion";
import React, { useMemo } from "react";
import { DistributeProfileResponseType } from "modules/distribute/model/DistributeProfile";
import { FileExtenions } from "@appcircle/core/lib/enums/FileExtenions";
import { OS } from "@appcircle/core/lib/enums/OS";
import { OSValue } from "@appcircle/core/lib/enums/OSValue";
import {
  DistributeProfilesModel,
  useDistributeProfilesModel
} from "../profiles/ProfilesService";
import { patchFn } from "@appcircle/core/lib/utils/patchFn";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import {
  DistStoreType,
  DistributeModuleApiBase
} from "modules/distribute/DistributeModule";
import { IntlShape, useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import {
  TesterStatusResultType,
  DistributeTesterResponseType
} from "modules/distribute/model/DistributeTester";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import createTemporaryAppItem from "./createTemporaryAppItem";
import { Result } from "@appcircle/core/lib/services/Result";
import { TaskManagerContextType } from "@appcircle/taskmanager/lib/TaskManagerContextType";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { createFakeID, isFakeID } from "@appcircle/core/lib/utils/fakeID";

export type ProfileDetailServiceType = {
  changeSelectedAppItem: (profileID: string, appID: string) => void;
  changeSelectedOS: (profileID: string, os: OS) => void;
  deleteAppFromProfile: (
    profileID: string,
    appID: string,
    os: OS,
    taskProps: TaskPropsType<Result>
  ) => void;
  addNewAppToProfile: (
    profileID: string,
    tempAppID: string,
    os: OS,
    file: File | null
  ) => void;
  updateProfileVersionTags: (
    profileID: string,
    appID: string,
    os: OS,
    tags: TagComponentProps[],
    taskProps?: TaskPropsType<Result>
  ) => void;
  updateProfile: () => void;
  updateAppTestersStatus: (
    profileID: string,
    appID: string,
    testerIDs: string[],
    status: TesterStatusResultType
  ) => void;
  updateAppSelectedTesters: (profileID: string, testerIDs: any[]) => void;
  updateAppTester: (
    profileID: string,
    appID: string,
    tester: DistributeTesterResponseType
  ) => void;
  updateAppItem: (profileID: string, appID: string) => void;
  addNewTemporaryAppItem: (os: OS, appItem?: DistributeProfileAppType) => void;
  deleteSelectedAppItem: (os: OS) => void;
  deleteTemporaryAppItem: (appID: string, os?: OS) => void;
};
export const ProfileDetailContext = React.createContext<
  ProfileDetailServiceType
>({} as any);

export function useProfileService(id: string) {
  const _context = useDistributeModuleContext();
  const model = useDistributeProfilesModel();
  const { createApiConsumer, token } = useServiceContext();
  const taskManager = useTaskManager();
  const intl = useIntl();
  const service = useMemo(
    () =>
      createProfileDetailServiceBoundry(
        model,
        _context,
        createApiConsumer,
        id,
        taskManager,
        token.access_token,
        intl
      ),
    [
      _context,
      createApiConsumer,
      id,
      intl,
      model,
      taskManager,
      token.access_token
    ]
  );

  return service;
}

export function createProfileDetailServiceBoundry(
  model: DistributeProfilesModel,
  context: ModuleContextValueType<DistStoreType>,
  createApiConsumer: ConsumerFactoryFn,
  profileID: string,
  taskManager: TaskManagerContextType,
  accessToken: string,
  intl: IntlShape
): ProfileDetailServiceType {
  const serviceDispatch = createApiConsumer();
  const { addTask, createApiTask } = taskManager;

  function updateAppItem(profileID: string, appID: string) {
    const task = createApiTask(
      serviceDispatch({
        method: "GET",
        endpoint: `${DistributeModuleApiBase}/profiles/${profileID}/app-versions/${appID}`
      }),
      {
        onSuccess: result => {
          context.dispatch &&
            context.dispatch({
              type: "UPDATE_APP_ITEM",
              profileID,
              appItem: (result as Result)
                .json as DistributeProfileAppResponseType,
              accessToken
            });
        }
      }
    );
    task.entityID = appID;
    addTask(task);
  }

  function addNewTemporaryAppItem(os: OS, appItem?: DistributeProfileAppType) {
    const tempItem = appItem || createTemporaryAppItem(os, createFakeID());
    context.dispatch({
      type: "ADD_NEW_TEMPLATE_APP_ITEM",
      profileID,
      os,
      appItem: appItem || tempItem
    });

    return tempItem && tempItem.id;
  }

  function deleteAppFromProfile(
    profileID: string,
    appID: string,
    os: OS,
    taskProps: TaskPropsType<Result>
  ) {
    let onSuccess = (result: any) => {
      context.dispatch({
        type: "DELETE_APP_ITEM_FROM_PROFILE",
        profileID,
        os,
        appID
      });
      updateProfile(profileID);
    };
    if (taskProps.onSuccess) {
      onSuccess = patchFn(onSuccess, taskProps.onSuccess);
    }
    const task = createApiTask(
      serviceDispatch({
        method: "DELETE",
        endpoint: `${DistributeModuleApiBase}/profiles/${profileID}/app-versions/${appID}`
      }),
      {
        ...taskProps,
        onSuccess
      }
    );
    task.entityID = appID;
    addTask(task);
  }
  function updateProfileVersionTags(
    profileID: string,
    appID: string,
    os: OS,
    tags: TagComponentProps[],
    taskProps?: TaskPropsType<Result>
  ) {
    const task = createApiTask(
      serviceDispatch({
        method: "PATCH",
        endpoint: `${DistributeModuleApiBase}/profiles/${profileID}/app-versions/${appID}`,
        data: JSON.stringify({
          versionTags: tags.map(tag => tag.text) || []
        }),
        headers: {
          "Content-Type": "application/json"
        }
      }),
      {
        ...taskProps,
        onSuccess: result => {
          context.dispatch({
            type: "UPDATE_PROFILE_VERSION_TAGS",
            profileID,
            os,
            appID,
            tags: (result as Result).json.versionTags
          });
        }
      }
    );
    task.entityID = appID;
    task && addTask(task);
  }

  function updateProfile(id?: string) {
    const task = createApiTask(
      serviceDispatch({
        method: "GET",
        endpoint: `${DistributeModuleApiBase}/profiles/${profileID}`
      }),
      {
        onSuccess: result => {
          context.dispatch({
            type: "UPDATE_PROFILE_DATA",
            profile: (result as Result).json as DistributeProfileResponseType,
            accessToken
          });
        }
      }
    );

    task.entityID = profileID;
    addTask(task);
  }
  return {
    updateProfile,
    changeSelectedAppItem: (profileID: string, appID: string) => {
      context.dispatch({
        type: "CHANGE_SELECTED_APP_ITEM",
        profileID,
        appID
      });
      !isFakeID(appID) && updateAppItem(profileID, appID);
    },
    changeSelectedOS: (profileID, os) => {
      context.dispatch({
        type: "CHANGE_PROFILE_SELECTED_OS",
        profileID,
        os
      });
    },
    deleteAppFromProfile,
    addNewAppToProfile: (profileID, tempAppID, os, file) => {
      const task = createApiTask(
        serviceDispatch({
          method: "POST",
          endpoint: `${DistributeModuleApiBase}/profiles/${profileID}/app-versions`,
          data: {
            [file ? file.name : `app.${FileExtenions[OSValue[os]]}`]: file
          }
        }),
        {
          retryCount: 2,
          onSuccess: result => {
            const data = result as Result<
              HookEvent<DistributeProfileResponseType>
            >;

            context.dispatch({
              type: "DELETE_APP_ITEM_FROM_PROFILE",
              appID: tempAppID,
              profileID,
              os
            });
            data.json.data &&
              data.json.data.appVersions &&
              context.dispatch({
                type: "UPDATE_PROFILE_DATA",
                accessToken,
                profile: data.json.data
              });
          },
          onError: () => {
            context.dispatch({
              type: "DELETE_APP_ITEM_FROM_PROFILE",
              profileID,
              os,
              appID: tempAppID
            });
          },
          successMessage: () =>
            intl.formatMessage(
              messages.distributeProfileDetailUploadFileSuccess
            ),
          errorMessage: () =>
            intl.formatMessage(messages.distributeProfileDetailUploadFileError)
        }
      );
      task.entityID = tempAppID;
      addTask(task);
    },
    updateProfileVersionTags,
    updateAppSelectedTesters: (profileID: string, testerIDs: string[]) => {
      context.dispatch &&
        context.dispatch({
          type: "UPDATE_APP_SELECTED_TESTERS",
          profileID,
          testerIDs
        });
    },
    updateAppTestersStatus: (
      profileID: string,
      appID: string,
      testerIDs: string[],
      status: TesterStatusResultType
    ) => {
      context.dispatch &&
        context.dispatch({
          type: "UPDATE_APP_TESTERS_STATUS",
          profileID,
          appID,
          testerIDs,
          status
        });
    },
    updateAppTester: (
      profileID: string,
      appID: string,
      tester: DistributeTesterResponseType
    ) => {
      context.dispatch({
        type: "UPDATE_APP_TESTER",
        profileID,
        appID,
        tester
      });
    },
    updateAppItem,
    addNewTemporaryAppItem,
    deleteSelectedAppItem: os => {
      context.dispatch({
        type: "DELETE_SELECTED_APP_ITEM",
        profileID,
        os
      });
    },
    deleteTemporaryAppItem: (appID, os?) => {
      context.dispatch({
        type: "DELETE_TEMPLATE_APP_ITEM",
        profileID,
        os,
        appID
      });
    }
  };
}
