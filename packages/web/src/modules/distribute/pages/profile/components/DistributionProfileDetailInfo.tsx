import React, { useState, useContext, useCallback, useMemo } from "react";
import { OS } from "@appcircle/core/lib/enums/OS";
import {
  TagComponentProps,
  TagComponent
} from "@appcircle/ui/lib/TagComponent";
import { ProfileDetailContext } from "../ProfileDetailService";
import shortid from "shortid";
import { TextField } from "@appcircle/ui/lib/TextField";
import { IconButton, IconButtonState } from "@appcircle/ui/lib/IconButton";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ProfileDetailInfo } from "shared/profile-detail-layout/ProfileDetailInfo";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { Result } from "@appcircle/core/lib/services/Result";
import { SvgPlus } from "@appcircle/assets/lib/Plus";

enum TagActionType {
  ADD = 1,
  DELETE = 2
}

export type DistributionProfileDetailInfoProps = {
  os: OS;
  size: string;
  fileName?: string;
  bundleName: string;
  tags: TagComponentProps[];
  qrcode: string;
  profileID: string;
  appID: string;
};

export function DistributionProfileDetailInfo(
  props: DistributionProfileDetailInfoProps
) {
  const [creationMode, setCreationMode] = useState(false);
  const [isNewTagDisabled, setNewTagDisabled] = useState(false);
  const [idToRemove, setIdToRemove] = useState(null);
  // const { addTask, createApiTask } = useTaskManager();
  const services = useContext(ProfileDetailContext);
  const modifiableTags: TagComponentProps[] = useMemo(
    () => props.tags.filter(tag => tag.modifiable),
    [props.tags]
  );
  const intl = useIntl();

  const updateProfileVersionTags = useCallback(
    (modifiableTags: TagComponentProps[], successMessage: string) => {
      services.updateProfileVersionTags(
        props.profileID,
        props.appID,
        props.os,
        modifiableTags,
        {
          ...createProfileVersionTagProps(successMessage),
          onFinally: () => {
            setNewTagDisabled(false);
            setCreationMode(false);
            setIdToRemove(null);
          }
        }
      );
    },
    [services, props.profileID, props.appID, props.os]
  );

  const onClose = useCallback(
    (id: any) => {
      if (!id) return;
      setIdToRemove(id);
      let indexToRemove = modifiableTags.findIndex(tag => tag.id === id);
      modifiableTags.splice(indexToRemove, 1);
      setNewTagDisabled(true);
      updateProfileVersionTags(
        modifiableTags,
        getSuccessMessage(TagActionType.DELETE)
      );
    },
    [modifiableTags, updateProfileVersionTags]
  );

  const onChangeEnd = useCallback(
    (text: string) => {
      if (!text) return;

      modifiableTags.push({ id: shortid.generate(), text });
      setNewTagDisabled(true);
      updateProfileVersionTags(
        modifiableTags,
        getSuccessMessage(TagActionType.ADD)
      );
    },
    [modifiableTags, updateProfileVersionTags]
  );

  const onClick = useCallback(() => {
    setCreationMode(true);
  }, []);

  return (
    <ProfileDetailInfo className="DistributionProfileDetailInfo">
      <div className="DistributionProfileDetailInfo_fileName">
        {props.fileName}
      </div>
      <div className="DistributionProfileDetailInfo_bundleName">
        {props.bundleName}
      </div>
      <div className="DistributionProfileDetailInfo_tags">
        {props.tags.map((tag: TagComponentProps) => (
          <TagComponent
            {...tag}
            key={shortid.generate()}
            onClose={onClose}
            isDisabled={tag.id === idToRemove ? true : false}
          />
        ))}
        {creationMode ? (
          <div className="DistributionProfileDetailInfo_tags_newTag">
            <TextField
              disabled={isNewTagDisabled}
              text={""}
              placeHolder={intl.formatMessage(
                messages.distributeProfileDetailNewTag
              )}
              editMode={true}
              onChangeEnd={onChangeEnd}
              onBlurOrEscape={() => {
                setCreationMode(false);
              }}
            />
          </div>
        ) : null}
        <IconButton
          icon={SvgPlus}
          onClick={onClick}
          size="xsm"
          state={
            !creationMode ? IconButtonState.ENABLED : IconButtonState.DISABLED
          }
        />
      </div>
      {/* <img
        className="DistributionProfileDetailInfo_qrcode"
        src={
          props.qrcode ||
          "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
        }
        alt="qr code"
      /> */}
    </ProfileDetailInfo>
  );
}

function createProfileVersionTagProps(
  successMessage: string
): TaskPropsType<Result> {
  return {
    successMessage: () => successMessage,
    errorMessage: result =>
      `${(result as Result).status} ${
        (result as Result).statusText
      }, please try again`,
    retryCount: 3
  };
}

function getSuccessMessage(actionType: TagActionType) {
  switch (actionType) {
    case TagActionType.ADD:
      return "Tag Added";
    case TagActionType.DELETE:
      return "Tag Removed";
    default:
      return "Tag Updated";
  }
}
