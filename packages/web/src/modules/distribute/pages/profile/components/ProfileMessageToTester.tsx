import React from "react";
import classNames from "classnames";
import { ProgressView } from "@appcircle/ui/lib/ProgressView";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { SvgCopy } from "@appcircle/assets/lib/Copy";

export type ProfileMessageToTesterProps = {
  message?: string;
  inProgress?: boolean;
};

export function ProfileMessageToTester(props: ProfileMessageToTesterProps) {
  const { pushMessage } = useNotificationsService();
  const intl = useIntl();
  return (
    <div className="ProfileMessageToTester">
      <div className="ProfileMessageToTester_head">
        <div className="ProfileMessageToTester_head_title">
          Message to Testers
        </div>
        <SvgCopy
          className={classNames("ProfileMessageToTester_head_icon", {
            disabled: !props.message
          })}
          onClick={e => {
            if (props.message) {
              fallbackCopyTextToClipboard(props.message);
              pushMessage({
                messageType: "success",
                message: intl.formatMessage(
                  messages.distributeProfileDetailCopyMessage
                )
              });
            }
          }}
        />
      </div>
      <ProgressView isOverlay={true} isInProgress={!!props.inProgress}>
        {
          <div
            className={classNames("ProfileMessageToTester_content", {
              "ProfileMessageToTester_content-nomessage": !props.message
            })}
          >
            {props.message ||
              intl.formatMessage(messages.distributeProfileDetailMoMessage)}
          </div>
        }
      </ProgressView>
    </div>
  );
}

//https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
function fallbackCopyTextToClipboard(text: string) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
  }

  document.body.removeChild(textArea);
}
