import React, {
  PropsWithChildren,
  useState,
  useContext,
  useCallback,
  useMemo
} from "react";
import { SwitchButtonList } from "@appcircle/ui/lib/SwitchButtonList";
import { SearchField } from "@appcircle/ui/lib/SearchField";
import { DistributeProfileType } from "../../../model/DistributeProfile";
import { IconButtonState } from "@appcircle/ui/lib/IconButton";
import { DistributeProfileAppType } from "modules/distribute/model/DistributeAppVersion";
import { OS } from "@appcircle/core/lib/enums/OS";
import { ProfileDetailContext } from "../ProfileDetailService";
import { DistributionProfileDetailList } from "./DistributionProfileDetailList";
import { ProfileDetailListHeader } from "shared/profile-detail-layout/ProfileDetailListHeader";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { OSIcon } from "@appcircle/ui/lib/OSIcon";
import { SvgApple } from "@appcircle/assets/lib/Apple";
import { SvgAndroidSmall } from "@appcircle/assets/lib/AndroidSmall";
import { AddButton } from "@appcircle/ui/lib/AddButton";
import { useGTMService } from "shared/domain/useGTMService";

// const AppleIcon = icons.apple;
// const AndroSmallIcon = icons.android_small;
export type ProfileVersionProps = PropsWithChildren<{
  addNewFileModalSearch?: string;
  setIsSiderBarExpandedsOpen?: React.Dispatch<React.SetStateAction<boolean>>;
  profile: DistributeProfileType;
  selectedAppProfile: DistributeProfileAppType;
  appProfiles: DistributeProfileAppType[];
  archiveNum: number;
  selectedOS: OS;
}>;

export function ProfileVersions(props: ProfileVersionProps) {
  const [searchText, setSearchText] = useState("");
  const services = useContext(ProfileDetailContext);
  const os = props.selectedOS;
  const { triggerGTMEvent } = useGTMService();
  const appVersions = Object.values(
    props.profile.selectedOS === OS.android
      ? props.profile.appsByAndroid
      : props.profile.appsByIOS
  )
    .sort((a, b) => a - b)
    .map(index => props.profile.apps[index]);
  const appProfiles = useMemo(() => filterVersions(appVersions, searchText), [
    appVersions,
    searchText
  ]);
  const onSwitchButtonChanged = useCallback(
    (index: number) => {
      services.changeSelectedOS(
        props.profile.id,
        index === 0 ? OS.ios : OS.android
      );
    },
    [props.profile.id, services]
  );
  const switchButtonItems = useMemo(() => [SvgApple, SvgAndroidSmall], []);
  const onSearch = useCallback((_text: string) => {
    setSearchText(_text);
  }, []);
  const onNewAdd = useCallback(() => {
    triggerGTMEvent("dist_profile_version_new");
    services.addNewTemporaryAppItem(props.selectedOS);
  }, [props.selectedOS, services]);
  const intl = useIntl();
  return (
    <div className="ProfileVersions">
      <ProfileDetailListHeader>
        <div className="ProfileVersions_appInfo">
          {props.selectedAppProfile.iconUrl &&
          props.selectedAppProfile.iconUrl.indexOf("defaultApp.png") === -1 ? (
            <img
              alt=""
              className="ProfileVersions_appInfo_icon"
              src={props.selectedAppProfile.iconUrl}
              style={{ width: "48px", height: "48px" }}
            />
          ) : (
            <OSIcon display="appIcon" os={props.selectedOS}></OSIcon>
          )}
          <div className="ProfileVersions_appInfo_name">
            {props.profile.name}
          </div>
          {!!appProfiles.length && (
            <AddButton
              state={
                isFakeID(props.selectedAppProfile.id)
                  ? IconButtonState.DISABLED
                  : IconButtonState.ENABLED
              }
              text={""}
              onClick={onNewAdd}
            />
          )}
        </div>
        <div className="ProfileVersions_actions">
          <SwitchButtonList
            items={switchButtonItems}
            selectedIndex={os === OS.ios ? 0 : 1}
            onChange={onSwitchButtonChanged}
          />
          <SearchField
            text={searchText}
            placeHolder={"Search"}
            onChange={onSearch}
          />
        </div>
      </ProfileDetailListHeader>

      <div className="ProfileVersions_versions">
        <DistributionProfileDetailList
          isOpen={props.setIsSiderBarExpandedsOpen}
          selectedAppProfileID={
            appProfiles.length ? props.selectedAppProfile.id : ""
          }
          appProfiles={appProfiles}
          selectedOS={props.selectedOS}
          profileID={props.profile.id}
          inProgress={!props.profile.isLoaded}
          inProgressText={intl.formatMessage(
            messages.distributeProfileDetailVersionsLoading
          )}
          fallback={
            <div className="ProfileVersions_emptyVersion">
              {intl.formatMessage(
                messages.distributeProfileDetailVersionsNoVersions
              )}
            </div>
          }
        />
        {props.appProfiles.length > 0 ? (
          <div className="ProfileVersions_archiveInfo">
            {`${appProfiles.length} previous version(s) available.`}
          </div>
        ) : null}
      </div>
    </div>
  );
}

function filterVersions(
  versionItems: DistributeProfileAppType[],
  searchText: string
): DistributeProfileAppType[] {
  if (!searchText) return versionItems;
  let lowerCaseSearchText = searchText.toLowerCase();
  let filteredItems = versionItems.filter(item => {
    return (
      item.testers.filter(tester =>
        tester.email.toLocaleLowerCase().includes(lowerCaseSearchText)
      ).length > 0 ||
      item.tags.filter(tag =>
        tag.text.toLocaleLowerCase().includes(lowerCaseSearchText)
      ).length > 0 ||
      item.message.includes(lowerCaseSearchText) ||
      item.version.includes(lowerCaseSearchText)
    );
  });
  return filteredItems;
}
