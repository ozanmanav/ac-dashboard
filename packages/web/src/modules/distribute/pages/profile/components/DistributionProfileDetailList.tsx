import React, {
  PropsWithChildren,
  useContext,
  useMemo,
  useCallback,
  useState
} from "react";

import { OS } from "@appcircle/core/lib/enums/OS";
import { DistributeProfileAppType } from "modules/distribute/model/DistributeAppVersion";
import { ProfileDetailContext } from "../ProfileDetailService";
import { ProfileDetailListItem } from "shared/profile-detail-layout/ProfileDetailListItem";
import { ProfileDetailList } from "shared/profile-detail-layout/ProfileDetailList";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { SvgProfile } from "@appcircle/assets/lib/Profile";

export type ProfileDetailListProps = PropsWithChildren<{
  header?: string;
  appProfiles: Array<DistributeProfileAppType>;
  selectedAppProfileID: string;
  isOpen?: React.Dispatch<React.SetStateAction<boolean>>;
  profileID: string;
  selectedOS?: OS;
  inProgress?: boolean;
  inProgressText?: string;
  fallback?: JSX.Element;
}>;

type ProfileDetailListActionProps = {
  changeSelectedItem?: (index: number) => void;
};

export function DistributionProfileDetailList(
  props: ProfileDetailListProps & ProfileDetailListActionProps
) {
  const services = useContext(ProfileDetailContext);
  const onItemClick = useCallback(
    item => {
      // console.log(" item ... ", item);
      !item.isSelected &&
        services.changeSelectedAppItem(props.profileID, item.id);
      props.isOpen && props.isOpen(false);
    },
    [props, services]
  );
  const items = useMemo(
    () =>
      props.appProfiles
        .filter(item => item !== undefined)
        .map((item: DistributeProfileAppType, index: number) => (
          <DistributionProfileDetailListItem
            data={item}
            selectedOS={props.selectedOS}
            isSelected={item.id === props.selectedAppProfileID}
            key={item.id}
            onClick={onItemClick}
          />
        )),

    [
      onItemClick,
      props.appProfiles,
      props.selectedAppProfileID,
      props.selectedOS
    ]
  );

  return (
    <TaskProgressView
      entityID={props.profileID}
      isOverlay={true}
      hasRelativeRoot={true}
    >
      {items.length ? (
        <ProfileDetailList className={"DistributionProfileList"}>
          <React.Fragment>{items}</React.Fragment>
        </ProfileDetailList>
      ) : (
        <div className={"DistributionProfileList"}>
          {props.fallback || null}
        </div>
      )}
    </TaskProgressView>
  );
}

type DistributionProfileDetailListItem = {
  onClick?: (data: DistributeProfileAppType) => void;
  selectedOS?: OS;
  data: DistributeProfileAppType;
  isSelected: boolean;
};
function DistributionProfileDetailListItem(
  props: DistributionProfileDetailListItem
) {
  const versionParts = props.data.version.split("-");
  const [autoKill, setAutoKill] = useState(true);
  const { deleteTemporaryAppItem } = useContext(ProfileDetailContext);
  const taskManager = useTaskManager();
  const task = taskManager.getStatusByEntityID(props.data.id);
  // const {} = props;
  const onKillFocus = useCallback(
    () =>
      !task &&
      autoKill &&
      deleteTemporaryAppItem(props.data.id, props.selectedOS),
    [autoKill, deleteTemporaryAppItem, props.data.id, props.selectedOS, task]
  );
  const onClick = useCallback(
    () => props.onClick && props.onClick(props.data),
    // eslint-disable-next-line
    [props.onClick, props.data]
  );

  return (
    <ProfileDetailListItem
      isSelected={props.isSelected}
      isTemporary={isFakeID(props.data.id) || false}
      onClick={onClick}
      onKillFocus={onKillFocus}
      excludeKillTarget="ProfileDetail_detail"
    >
      <TaskProgressView
        entityID={props.data.id}
        progressType="stripe"
        hasRelativeRoot={true}
      >
        <div className="ProfileDetailListItem_info">
          <div className="ProfileDetailListItem_info_version">
            {versionParts[0] + " "}
            <span className="ProfileDetailListItem_info_version_channel">
              {versionParts[1]}
            </span>
          </div>
          <div className="ProfileDetailListItem_info_size">
            {props.data.size}
          </div>
        </div>
        <div className="ProfileDetailListItem_detail">
          <div className="ProfileDetailListItem_detail_uploadTime">
            {props.data.uploadTime}
          </div>
          <div className="ProfileDetailListItem_detail_testersNum">
            <SvgProfile className="ProfileDetailListItem_detail_testersNum_icon" />
            {props.data.testers.length}
          </div>
        </div>
      </TaskProgressView>
    </ProfileDetailListItem>
  );
}
