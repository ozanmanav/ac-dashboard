import React, { PropsWithChildren, useCallback, useMemo } from "react";
import { DistributeProfileType } from "modules/distribute/model/DistributeProfile";
import { DistributeProfileAppType } from "modules/distribute/model/DistributeAppVersion";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { Modals } from "modules/distribute/DistributeModule";
import { ProfileDetailHeader } from "shared/profile-detail-layout/ProfileDetailHeader";
import { ThreeDotsToggle } from "@appcircle/ui/lib/ThreeDotsToggle";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { Link } from "react-router-dom";
import { DistributionProfileAppDeleteModalDialogRouteParams } from "modules/distribute/modals/DistributionProfileAppDeleteModalDialog";
import { ContextMenuItemProps } from "@appcircle/ui/lib/menus/ContextMenuItem";
import { ContextMenu } from "@appcircle/ui/lib/menus/ContextMenu";
import { SvgSubmitPublicStore } from "@appcircle/assets/lib/SubmitPublicStore";
import { SvgSubmitEnterpriseStore } from "@appcircle/assets/lib/SubmitEnterpriseStore";
import { SvgDeleteBox } from "@appcircle/assets/lib/DeleteBox";

export type DistributionProfileDetailHeaderProps = PropsWithChildren<{
  profile: DistributeProfileType;
  profileApp: DistributeProfileAppType;
}>;

export function DistributionProfileDetailHeader(
  props: DistributionProfileDetailHeaderProps
) {
  const { createModalUrl } = useDistributeModuleContext();
  const versionParts = props.profileApp.version.split("-");
  const intl = useIntl();

  const contextMenuItems = useMemo<Array<ContextMenuItemProps>>(
    () => [
      {
        icon: SvgSubmitPublicStore,
        text: intl.formatMessage(
          messages.distributeProfileDetailMenu_SubmitPublic
        )
      },
      {
        icon: SvgSubmitEnterpriseStore,
        text: intl.formatMessage(
          messages.distributeProfileDetailMenu_SubmitEnterprise
        )
      },
      {
        icon: SvgDeleteBox,
        text: intl.formatMessage(messages.appCommonDelete),
        linkProps: {
          to: {
            search: createModalUrl<
              DistributionProfileAppDeleteModalDialogRouteParams
            >(
              Modals.ProfileAppDelete,
              ["profileAppId", props.profileApp.id],
              ["profileId", props.profile.id]
            )
          }
        }
      }
    ],
    [createModalUrl, intl, props.profileApp.id, props.profile.id]
  );

  const onContextMenuItemSelected = useCallback((selectedIndex: number) => {},
  []);
  const headerLeftItem = useMemo(
    () => (
      <React.Fragment>
        <div className="DistributionProfileDetailHeader_title">
          <div className="DistributionProfileDetailHeader_title_versions">
            <span>{versionParts[0] + " "}</span>
            <span className="DistributionProfileDetailHeader_title_versions_channel">
              {versionParts[1]}
            </span>
          </div>
          <div className="DistributionProfileDetailHeader_title_badges">
            <div className="DistributionProfileDetailHeader_title_badges-V">
              <span>
                {intl.formatMessage(
                  messages.distributeProfileVersionsBadgesVersion
                )}
              </span>
            </div>
            {props.profileApp.isSubmitted ? (
              <div className="DistributionProfileDetailHeader_title_badges-S">
                <span>
                  {intl.formatMessage(
                    messages.distributeProfileVersionsBadgesSubmitted
                  )}
                </span>
              </div>
            ) : null}
          </div>
        </div>
        <div className="DistributionProfileDetailHeader_subtitle">
          {props.profileApp.uploadTime}
        </div>
      </React.Fragment>
    ),
    // eslint-disable-next-line
    [
      props.profileApp.isSubmitted,
      props.profileApp.uploadTime,
      props.profileApp.version
    ]
  );
  const headerRightItem = useMemo(
    () => (
      <React.Fragment>
        <Link
          to={`?${createModalUrl(Modals.Preview)}&profileId=${
            props.profile.id
          }&appId=${props.profileApp.id}`}
          className="Button Button-md Button-light"
        >
          Preview on Device
        </Link>
        <ThreeDotsToggle>
          <ContextMenu
            items={contextMenuItems}
            onMenuItemSelected={onContextMenuItemSelected}
          />
        </ThreeDotsToggle>
      </React.Fragment>
    ),
    [
      contextMenuItems,
      createModalUrl,
      onContextMenuItemSelected,
      props.profile.id,
      props.profileApp.id
    ]
  );
  return (
    <ProfileDetailHeader
      leftItem={headerLeftItem}
      rightItem={headerRightItem}
    />
  );
}
