import React from "react";
import classNames from "classnames";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import moment from "moment";
import { DistributeTesterStatus } from "modules/distribute/model/DistributeTester";
const DOWNLOADED = "downloaded";

export type ProfileDetailsTableProps = {
  id: string;
  email: string;
  team: string;
  version: string;
  status: string;
  sendingDate: string;
};
export function ProfileTestersTableHeaderTemplate(
  props: ProfileDetailsTableProps
) {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="ProfileTestersTableItem_emailteam flex-column">
        <div>{intl.formatMessage(messages.appCommonEmail)}</div>
      </div>
      <div className={"ProfileTestersTableItem_sentDate flex-column"}>
        <div>{intl.formatMessage(messages.appCommonSentDate)}</div>
      </div>
      <div className={"ProfileTestersTableItem_version flex-column"}>
        <div>{intl.formatMessage(messages.appCommonVersion)}</div>
      </div>
      <div
        className={classNames("ProfileTestersTableItem_status", "flex-column", {
          "ProfileTestersTableItem_status-completed":
            props.status === DOWNLOADED
        })}
      >
        <div>{intl.formatMessage(messages.appCommonStatus)}</div>
      </div>
    </React.Fragment>
  );
}

export function ProfileTestersTableRowSeparatorTemplate() {
  return <div className="ProfileTestersTableItem_seperator" />;
}

const statuses = {
  Clicked: "Clicked",
  ClickedNotEnrolled: "Clicked, Not Enrolled",
  ClickedNotLoggedIn: "Clicked, No Login",
  Downloaded: "Downloaded",
  LoggedIn: "Login, No Download",
  Pending: "Pending",
  Processing: "Processing"
};

export function ProfileTestersTableRowTemplate(props: {
  data: ProfileDetailsTableProps;
}) {
  return (
    <React.Fragment>
      <div className="ProfileTestersTableItem_emailteam">
        <div>{props.data.email}</div>
      </div>
      <div className={"ProfileTestersTableItem_sentDate"}>
        <div>
          {props.data.sendingDate
            ? moment(props.data.sendingDate).calendar()
            : "NA"}
        </div>
      </div>
      <div className={"ProfileTestersTableItem_version"}>
        <div>{props.data.version}</div>
      </div>
      <div
        className={classNames("ProfileTestersTableItem_status", {
          "ProfileTestersTableItem_status-completed":
            props.data.status.toLowerCase() === DOWNLOADED
        })}
      >
        {props.data.status ? (
          <div>{statuses[props.data.status as keyof typeof statuses]}</div>
        ) : null}
      </div>
    </React.Fragment>
  );
}
