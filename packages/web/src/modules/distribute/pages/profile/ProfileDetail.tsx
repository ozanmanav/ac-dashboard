import React, {
  useState,
  useContext,
  useEffect,
  useMemo,
  useCallback
} from "react";
import { ProfileVersions } from "./components/ProfileVersions";
import { TextButton } from "@appcircle/ui/lib/TextButton";
import { ProfileMessageToTester } from "./components/ProfileMessageToTester";
import {
  ProfileTestersTableHeaderTemplate,
  ProfileTestersTableRowTemplate,
  ProfileTestersTableRowSeparatorTemplate
} from "./components/ProfileTestersTable";
import { AddNewFile } from "@appcircle/ui/lib/AddNewFile";
import { Link } from "react-router-dom";
import { Modals } from "../../DistributeModule";
import { ModalContext } from "@appcircle/shared/lib/services/ModalContext";
import { FileExtenions } from "@appcircle/core/lib/enums/FileExtenions";
import { OSValue } from "@appcircle/core/lib/enums/OSValue";
import { OS } from "@appcircle/core/lib/enums/OS";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { ProfileDetailContext } from "./ProfileDetailService";
import createTemporaryAppItem from "./createTemporaryAppItem";
import { useDistributeProfilesModel } from "../profiles/ProfilesService";
import { ProfileDetailTable } from "shared/profile-detail-layout/ProfileDetailTable";
import { DistributionProfileDetailHeader } from "./components/DistributionProfileDetailHeader";
import { DistributionProfileDetailInfo } from "./components/DistributionProfileDetailInfo";
import { ProfileDetailLayout } from "shared/profile-detail-layout/ProfileDetailLayout";
import { SelectedItemsType } from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import { convertFakeID, isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { DistributeProfileAppType } from "modules/distribute/model/DistributeAppVersion";
import { DistributeProfileType } from "modules/distribute/model/DistributeProfile";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { ScrollView } from "@appcircle/ui/lib/ScrollView";
import { createEmptyDistributeProfile } from "modules/distribute/helper/createEmptyDistributeProfile";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import { SvgFileApk } from "@appcircle/assets/lib/FileApk";
import { SvgFileIpa } from "@appcircle/assets/lib/FileIpa";
import { SvgProfile } from "@appcircle/assets/lib/Profile";
import { useGTMService } from "shared/domain/useGTMService";

type ProfileDetailProps = {
  id: string;
};

const autoUploadID = convertFakeID("td-profileDetail-autoupload");
export function ProfileDetail(props: ProfileDetailProps) {
  useGTMService({ triggerEvent: "dist_profile_preview" });
  const [isSiderBarExpanded, setIsSiderBarExpandedsOpen] = useState(true);
  const [tableInProgress, setTableInProgress] = useState(false);
  const { createModalUrl, state: moduleState } = useDistributeModuleContext();

  const profilesModel = useDistributeProfilesModel();
  // const { emptyProfile, emptyAppProfile } = profilesModel.toObject();
  const profile = useMemo(
    () => profilesModel.findById(props.id) || createEmptyDistributeProfile(""),
    [profilesModel, props.id]
  );

  const services = useContext(ProfileDetailContext);

  // TODO: Use Route state as initial state
  const { DEPRECATED_setInitialState: setInitialState } = useContext(
    ModalContext
  );
  const selectedOS = profile.selectedOS;
  const icon =
    FileExtenions[OSValue[selectedOS]] === "ipa" ? SvgFileIpa : SvgFileApk;

  const apps = useMemo(
    () =>
      selectedOS === OS.android ? profile.appsByAndroid : profile.appsByIOS,
    [profile.appsByAndroid, profile.appsByIOS, selectedOS]
  );

  // TODO: Move this logic to model
  const selectedAppProfile = useMemo(
    () =>
      (profile.selectedAppProfileID[OSValue[selectedOS]] &&
        profile.apps[
          profile.appsByID[profile.selectedAppProfileID[OSValue[selectedOS]]]
        ]) ||
      createTemporaryAppItem(OS.ios, ""),
    [profile, selectedOS]
  );

  const selectedTesters = useMemo(
    () => selectedAppProfile.selectedTesters || [],
    [selectedAppProfile.selectedTesters]
  );
  const tempAppProfiles = useMemo(
    () =>
      Object.values(profile.tempAppProfilesByID).map(
        index => profile.apps[index]
      ),
    [profile.apps, profile.tempAppProfilesByID]
  );

  const appProfiles = useMemo(() => {
    return tempAppProfiles
      .filter(item => item && item.platformType === selectedOS)
      .concat(Object.values(apps).map(index => profile.apps[index]));
  }, [apps, profile.apps, selectedOS, tempAppProfiles]);

  useEffect(() => {
    tableInProgress && setTableInProgress(false);
  }, [selectedTesters, selectedTesters.length, tableInProgress]);

  const onItemSelected = useCallback(
    (selectedTesters: string[]) => {
      services.updateAppSelectedTesters(profile.id, selectedTesters);
    },
    [profile.id, services]
  );

  const onSendToTest = useCallback(() => {
    setInitialState({
      profile,
      messageToTester: selectedAppProfile.message,
      groupsByName: moduleState.groups.groupsNames,
      updateAppItem: (profileID: string, appID: string) => {
        setTableInProgress(true);
        services.updateAppItem(profileID, appID);
      }
    });
  }, [
    moduleState.groups.groupsNames,
    profile,
    selectedAppProfile.message,
    services,
    setInitialState
  ]);

  const onFileUpload = useCallback(
    (file: File) => {
      // const os = FileExtenions[OSValue[selectedOS]] === "ipa" ? OS.ios : OS.android;
      const tempId =
        Object.keys(
          selectedOS === OS.ios ? profile.appsByIOS : profile.appsByAndroid
        ).length === 0 &&
        services.addNewTemporaryAppItem(
          selectedOS,
          createTemporaryAppItem(selectedOS, autoUploadID)
        );

      services.addNewAppToProfile(
        profile.id,
        tempId || selectedAppProfile.id,
        selectedOS,
        file
      );
    },
    [profile, selectedAppProfile, selectedOS, services]
  );

  // Creates valid data to selected testers
  const selectedItems = useMemo(
    () =>
      selectedTesters.reduce<SelectedItemsType>((acc, item) => {
        acc[item] = true;
        return acc;
      }, {}),
    // eslint-disable-next-line
    [selectedTesters]
  );
  const intl = useIntl();

  return (
    <ProfileDetailLayout
      inProgress={!profile.isLoaded || selectedAppProfile.isInprogress}
      progressText={intl.formatMessage(messages.distributeProfileDetailLoading)}
      isSideBarExpanded={isSiderBarExpanded}
      leftHandlerClick={() => {
        setIsSiderBarExpandedsOpen(true);
      }}
      sideBar={
        <ProfileVersions
          setIsSiderBarExpandedsOpen={setIsSiderBarExpandedsOpen}
          profile={profile}
          selectedAppProfile={selectedAppProfile}
          selectedOS={selectedOS}
          appProfiles={appProfiles}
          archiveNum={apps.archivedNum}
        />
      }
      details={
        <DetailComponent
          key={selectedAppProfile.id}
          selectedAppProfile={selectedAppProfile}
          emptyAppProfile={useMemo(
            () => createTemporaryAppItem(OS.ios, ""),
            []
          )}
          profile={profile}
          selectedOS={selectedOS}
          onItemSelected={onItemSelected}
          tableInProgress={tableInProgress}
          selectedItems={selectedItems}
          createModalUrl={createModalUrl}
          selectedTesters={selectedTesters}
          onSendToTest={onSendToTest}
          icon={icon}
          onFileUpload={onFileUpload}
        />
      }
    />
  );
}
function DetailComponent({
  selectedAppProfile,
  emptyAppProfile,
  profile,
  selectedOS,
  onItemSelected,
  tableInProgress,
  selectedItems,
  createModalUrl,
  selectedTesters,
  onSendToTest,
  icon,
  onFileUpload
}: {
  selectedAppProfile: DistributeProfileAppType;
  emptyAppProfile: DistributeProfileAppType;
  profile: DistributeProfileType;
  selectedOS: OS;
  onItemSelected: (selectedTesters: string[]) => void;
  tableInProgress: boolean;
  selectedItems: SelectedItemsType;
  createModalUrl: ModuleContextValueType<any>["createModalUrl"];
  selectedTesters: string[];
  onSendToTest: () => void;
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  onFileUpload: (file: File) => void;
}) {
  const intl = useIntl();
  const services = useContext(ProfileDetailContext);
  useEffect(() => {
    return () =>
      profile &&
      selectedItems &&
      services.updateAppSelectedTesters(profile.id, []);
    // eslint-disable-next-line
  }, []);

  return (
    <div className="ProfileDetail_detail">
      {((profile.selectedOS === OS.ios && profile.iOSVersion) ||
        (profile.selectedOS === OS.android && profile.androidVersion)) &&
      (!selectedAppProfile || !isFakeID(selectedAppProfile.id)) ? (
        <TaskProgressView entityID={selectedAppProfile.id}>
          <ScrollView>
            <div>
              <DistributionProfileDetailHeader
                profile={profile}
                profileApp={selectedAppProfile}
              />
            </div>
            <div>
              <DistributionProfileDetailInfo
                os={selectedOS}
                // fileName={''}
                bundleName={selectedAppProfile.bundleName}
                size={selectedAppProfile.size}
                tags={selectedAppProfile.tags}
                qrcode={selectedAppProfile.qrcode}
                profileID={profile.id}
                appID={selectedAppProfile.id}
              />
            </div>
            <ProfileDetailTable
              gridHeaderClassName="ProfileTestersTableItem ProfileTestersTableItem-header"
              onItemSelected={onItemSelected}
              gridData={selectedAppProfile.testers}
              updateParameter={selectedOS + selectedAppProfile.id}
              inProgress={tableInProgress}
              gridHeaderTemplate={ProfileTestersTableHeaderTemplate}
              gridRowTemplate={ProfileTestersTableRowTemplate}
              gridRowSeperatorTemplate={ProfileTestersTableRowSeparatorTemplate}
              defaultSelected={selectedItems}
              header={
                <React.Fragment>
                  <span>
                    {intl.formatMessage(messages.distributeProfileTesters)} (
                    {selectedAppProfile.testers.length})
                  </span>
                  <Link
                    to={{
                      search: createModalUrl(Modals.SendToTesters)
                    }}
                  >
                    <TextButton
                      title={intl.formatMessage(
                        messages.distributeProfileDetailSendtoTest,
                        {
                          count: selectedTesters.length
                            ? `(${selectedTesters.length})`
                            : ""
                        }
                      )}
                      onClick={onSendToTest}
                    />
                  </Link>
                </React.Fragment>
              }
              fallback={
                <div className="ProfileGroups_notesters ProfileDetail_notesters">
                  <SvgProfile />
                  <span>
                    {intl.formatMessage(messages.distributeGroupsNoTesters)}
                  </span>
                </div>
              }
            />

            <div className="ProfileDetail_messageToTester">
              <ProfileMessageToTester
                message={selectedAppProfile.message}
                inProgress={tableInProgress}
              />
            </div>
          </ScrollView>
        </TaskProgressView>
      ) : (
        <div className="ProfileDetail_detail AddNewFileModalView">
          <AddNewFile
            icons={[icon]}
            onFileUpload={onFileUpload}
            types={[FileExtenions[OSValue[selectedOS]]]}
            text={intl.formatMessage(messages.appCommonDragFileNotification, {
              file: FileExtenions[OSValue[selectedOS]]
            })}
            entityID={selectedAppProfile.id}
          />
        </div>
      )}
    </div>
  );
}
