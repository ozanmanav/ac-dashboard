import React, { PropsWithChildren, useMemo, useEffect } from "react";
import {
  ProfileDetailServiceType,
  ProfileDetailContext,
  useProfileService
} from "./ProfileDetailService";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import { DistStoreType } from "modules/distribute/DistributeModule";
import { useHooksSubcriber } from "@appcircle/taskmanager/lib/useHooksSubcriber";

export type sendMessageToTestersType = {
  profileID: string;
  appVersionId: string;
  testers: string[];
  message: string;
};

export function ProfileDetailContextProvider(
  props: PropsWithChildren<{
    services?: ProfileDetailServiceType;
    context?: ModuleContextValueType<DistStoreType>;
    profileID: string;
  }>
) {
  const { services, profileID } = props;
  const response = useHooksSubcriber(["Distribution|TestingStatusChanged"]);
  const _services = useProfileService(profileID);

  useEffect(() => {
    if (response !== null) {
      _services.updateAppTester(
        response.data.profileId,
        response.data.appVersionId,
        response.data.data
      );
    }
  }, [response]);

  useEffect(() => {
    _services.updateProfile();
  }, []);

  const value = useMemo(() => {
    return services || _services;
  }, [_services, services]);
  return (
    <ProfileDetailContext.Provider value={value}>
      {props.children}
    </ProfileDetailContext.Provider>
  );
}
