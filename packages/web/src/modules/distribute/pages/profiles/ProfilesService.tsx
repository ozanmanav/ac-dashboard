import React, { useMemo } from "react";
import {
  TesterProfileState,
  EmptyProfileState
} from "../../reducers/profiles/reducer";
import { useDistributeModuleContext } from "../../TestingDistributionContext";
import { ConsumerFactoryFn } from "@appcircle/shared/lib/services/createService";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import {
  DistributeProfileResponseType,
  DistributeProfileType
} from "../../model/DistributeProfile";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ISearchable, Model } from "@appcircle/core/lib/Model";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import {
  DistStoreType,
  DistributeModuleApiBase
} from "modules/distribute/DistributeModule";
import { Message } from "@appcircle/shared/lib/Module";
import { TaskManagerContextType } from "@appcircle/taskmanager/lib/TaskManagerContextType";
import { Result } from "@appcircle/core/lib/services/Result";

type ProfilesServiceProps = {
  testingContext?: ModuleContextValueType<DistStoreType>;
};
export function useProfilesService(props: ProfilesServiceProps) {
  const _context = useDistributeModuleContext();
  const { createApiConsumer, token } = useServiceContext();
  const taskManager = useTaskManager();
  const model = useDistributeProfilesModel();
  const { pushMessage } = useNotificationsService();

  const _services = useMemo<ProfilesContextServiceType>(
    () =>
      createServiceBoundry(
        model,
        _context,
        createApiConsumer,
        taskManager,
        token.access_token,
        pushMessage
      ),
    [
      _context,
      createApiConsumer,
      model,
      pushMessage,
      taskManager,
      token.access_token
    ]
  );

  return _services;
}

export class DistributeProfilesModel
  extends Model<TesterProfileState, DistributeProfileType>
  implements ISearchable {
  protected sortedState: DistributeProfileType[] = [];
  protected init(): void {}

  isLoaded(): boolean {
    return this.state.isLoaded || false;
  }
  doesNameExist = (name: string) => {
    return this.state.profiles.some(profile => profile.name === name);
  };
  findById(id: string) {
    return (
      this.state &&
      this.state.profiles &&
      this.state.profiles[this.state.profilesByID[id]]
    );
  }

  toObject() {
    return this.state;
  }

  // getAndroidAppsFromProfile(id: string) {
  //   const group = this.findById(id);
  //   // group
  //   //   ? group.appsByIOS.map(index => {
  //   //       return group.apps[index];
  //   //     })
  //   //   : [];
  // }

  checkProfilesNameExists(name: string, id: string): boolean {
    name = name.toLocaleLowerCase();
    return this.state.profiles.some(
      p => p.name.toLocaleLowerCase() === name && p.id != id
    );
  }

  search(text: string) {
    return this.state.profilesSorted.filter(
      d => d.name.toLowerCase().indexOf(text.toLowerCase()) > -1
    );
  }

  getAll() {
    return this.state.profilesSorted;
  }

  hasTemporaryProfile() {
    return this.sortedState[0] && this.state.profiles[0].isTemporary;
  }

  length() {
    return this.state.profiles.length;
  }
}

export function useDistributeProfileModel(id: string) {
  const model = useDistributeProfilesModel();
  const profile = model.findById(id);
  // const androidApps = useMemo(
  //   () => profile.apps.filter(profile => profile.platformType === "android"),
  //   [profile.apps]
  // );
  // const iosApps = useMemo(
  //   () => profile.apps.filter(profile => profile.platformType === "ios"),
  //   [profile.apps]
  // );
  const value = useMemo(
    () =>
      profile
        ? {
            getProfile() {
              return profile;
            },
            findAppById(id: string) {
              return profile.apps.find(app => app.id === id);
            }
          }
        : undefined,
    [profile]
  );

  return value;
}

export function useDistributeProfilesModel() {
  const { state } = useDistributeModuleContext();
  const model = useMemo(
    () =>
      new DistributeProfilesModel(
        state && state.profiles ? state.profiles : EmptyProfileState
      ),
    [state]
  );

  return model;
}

export type ProfilesModelType = ReturnType<typeof useDistributeProfilesModel>;

export type ProfilesContextServiceType = {
  addNewTemporaryProfile: () => void;
  delete: (id: string, onFinally?: () => void, isTemporary?: boolean) => void;
  pinProfile: (id: string, status: boolean) => void;
  renameProfile: (id: string, newName: string, isTemporary?: boolean) => void;
  updateProfiles: (notifyMe?: boolean) => void;
};
// export const ProfilesService = React.createContext<TesterProfileState>(
//   {} as any
// );

export const UpdateProfilesTaskID = "modules/distribute/profiles/update";

function createServiceBoundry(
  model: DistributeProfilesModel,
  context: ModuleContextValueType<DistStoreType>,
  createApiConsumer: ConsumerFactoryFn,
  taskManager: TaskManagerContextType,
  accessToken: string,
  pushMessage: (message: Message) => void
): ProfilesContextServiceType {
  const { dispatch } = context;
  const serviceDispatch = createApiConsumer();
  const { addTask, createApiTask } = taskManager;
  return {
    updateProfiles: (notifyMe?: boolean) => {
      const task = createApiTask(
        serviceDispatch({
          method: "GET",
          endpoint: `${DistributeModuleApiBase}/profiles`
        }),
        {
          onSuccess: result => {
            dispatch({
              type: "UPDATE_PROFILES_DATA",
              profiles: (result as Result)
                .json as DistributeProfileResponseType[],
              accessToken
            });
          },
          // successMessage: notifyMe ? () => "Profiles updated." : undefined,
          progressMessage: notifyMe ? () => "Profiles updating." : undefined,
          errorMessage: notifyMe ? () => "Profiles updating failed!" : undefined
        }
      );
      task.entityID = UpdateProfilesTaskID;
      task && addTask(task, 1);
    },
    delete: (id: string, onFinally?: () => void, isTemporary?: boolean) => {
      if (id.startsWith("fake"))
        return dispatch({
          type: "DELETE_TESTER_PROFILE",
          id
        });
      const profileName = model.findById(id).name;
      const task = createApiTask(
        serviceDispatch({
          method: "DELETE",
          endpoint: `${DistributeModuleApiBase}/profiles/${id}`
        }),
        {
          onSuccess: result => {
            dispatch({
              type: "DELETE_TESTER_PROFILE",
              id
            });
          },
          onFinally,
          successMessage: result => `${profileName} has been deleted`,
          progressMessage: result => `${profileName} is deleting...`,
          errorMessage: result => `${profileName} has not been deleted!`
        }
      );
      task.entityID = id;
      task && addTask(task);
    },
    addNewTemporaryProfile: () => {
      dispatch({ type: "ADD_NEW_TEMPLATE_TESTER_PROFILE" });
    },
    pinProfile: (id: string, status: boolean) => {
      const task = createApiTask(
        serviceDispatch({
          method: "PATCH",
          endpoint: `${DistributeModuleApiBase}/profiles/${id}`,
          data: JSON.stringify({ pinned: status })
        }),
        {
          onStart: () => {
            dispatch({
              type: "SET_INPROGRESS_STATE_TESTER_PROFILE",
              id
            });
          },
          onSuccess: result => {
            dispatch({
              type: "PIN_TESTER_PROFILE",
              id,
              status
            });
            dispatch({
              type: "CLEAR_STATE_TESTER_PROFILE",
              id
            });
          },
          successMessage: () => `Profile updated`,
          progressMessage: () => `Profile updating..`,
          errorMessage: () => `Profile has not been updated!`
        }
      );
      task.entityID = id;
      addTask(task);
    },
    renameProfile: (id: string, newName: string, isTemporary?: boolean) => {
      const profileName = model.findById(id).name;
      const doesNameExist = model.checkProfilesNameExists(newName, id);
      if (doesNameExist) {
        pushMessage({
          messageType: "error",
          message: `'${newName}' already exists.`
        });
        return;
      }

      dispatch({ type: "RENAME_TESTER_PROFILE", id, newName });

      if (isTemporary) {
        // create new profile
        const task = createApiTask(
          serviceDispatch({
            method: "POST",
            endpoint: `${DistributeModuleApiBase}/profiles`,
            data: JSON.stringify({
              name: newName
            })
          }),
          {
            onSuccess: result => {
              dispatch({
                type: "DELETE_TESTER_PROFILE",
                id
              });
              dispatch({
                type: "ADD_TESTER_PROFILE",
                profile: (result as Result)
                  .json as DistributeProfileResponseType,
                tempProfileID: id,
                accessToken
              });
            },
            successMessage: () =>
              `Profile '${newName}' has successfully created.`,
            progressMessage: () => `Profile updating..`,
            errorMessage: () => `Profile creation failed!`
          }
        );
        task.entityID = id;
        addTask(task);
        return task.id;
      } else {
        // rename existing profile
        const task = createApiTask(
          serviceDispatch({
            method: "PATCH",
            endpoint: `${DistributeModuleApiBase}/profiles/${id}?name=${newName}`,
            data: JSON.stringify({ name: newName })
          }),
          {
            onStart: () => {
              dispatch({
                type: "SET_INPROGRESS_STATE_TESTER_PROFILE",
                id
              });
            },
            onSuccess: result => {
              dispatch({
                type: "CLEAR_STATE_TESTER_PROFILE",
                id
              });
            },
            onError: () => {
              dispatch({
                type: "RENAME_TESTER_PROFILE",
                id,
                newName: profileName
              });
              dispatch({
                type: "CLEAR_STATE_TESTER_PROFILE",
                id
              });
            },
            successMessage: () =>
              `Profile '${profileName}' has successfully renamed as '${newName}'.`,
            progressMessage: () => `Profile renaming...`,
            errorMessage: () => `Profile renaming failed!`
          }
        );
        task.entityID = id;
        task && addTask(task);
        return task.id;
      }
    }
  };
}
