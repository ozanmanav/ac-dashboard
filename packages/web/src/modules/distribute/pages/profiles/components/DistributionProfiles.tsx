import React, { useCallback } from "react";
import { SearchLayout } from "@appcircle/ui/lib/SearchLayout";
import { DistributeProfileType } from "modules/distribute/model/DistributeProfile";
import {
  useDistributeProfilesModel,
  useProfilesService,
  UpdateProfilesTaskID
} from "modules/distribute/pages/profiles/ProfilesService";
import { DistributionProfileCard } from "./DistributionProfileCard";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { ProfileGridView } from "@appcircle/ui/lib/ProfileGridView";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { SvgProfile } from "@appcircle/assets/lib/Profile";
import { useGTMService } from "shared/domain/useGTMService";

export function DistributionProfiles() {
  const { triggerGTMEvent } = useGTMService();
  const model = useDistributeProfilesModel();

  const { createLinkUrl } = useDistributeModuleContext();
  const services = useProfilesService({});
  const onAddElement = useCallback(() => {
    triggerGTMEvent("dist_profile_new");
    services.addNewTemporaryProfile();
  }, [services]);
  const intl = useIntl();
  const taskManager = useTaskManager();
  const task = taskManager.getStatusByEntityID(UpdateProfilesTaskID);
  return (
    <SearchLayout<any, DistributeProfileType>
      header={intl.formatMessage(messages.distributeModulePagesProfiles)}
      noElementsIcon={SvgProfile}
      model={model}
      isLoaded={!task || task.status === "COMPLETED"}
      noElementsText={intl.formatMessage(messages.distributeProfilesNoItems)}
      onAddElement={onAddElement}
    >
      {profilesData => {
        console.log(JSON.stringify(profilesData));
        return (
          <ProfileGridView<DistributeProfileType>
            profileCardComponent={DistributionProfileCard}
            profilesData={profilesData}
            link={createLinkUrl("/detail/")}
          />
        );
      }}
    </SearchLayout>
  );
}
