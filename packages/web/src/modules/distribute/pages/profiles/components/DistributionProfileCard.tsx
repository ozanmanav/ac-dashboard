import React, { useCallback, useMemo, useState, ReactNode } from "react";

import classNames from "classnames";
import {
  DistributeProfileType,
  ProfileAuthTypesbyValue
} from "modules/distribute/model/DistributeProfile";
import { getProfileCardProgressMessage } from "modules/distribute/helper/getProfileCardProgressMessage";
import { Spinner } from "@appcircle/ui/lib/Spinner";
import {
  ProfileCard,
  ProfileCardFooterData,
  ProfileCardDataType,
  ProfileCardServiceType
} from "@appcircle/ui/lib/ProfileCard";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import {
  useProfilesService,
  ProfilesContextServiceType,
  useDistributeProfilesModel
} from "../ProfilesService";
import {
  useDistributeGroupsModel,
  DistributeGroupsModel
} from "../../groups/DistributeGroupsContext";
import { Modals } from "modules/distribute/DistributeModule";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useIntl, IntlShape } from "react-intl";
import messages from "modules/distribute/messages";
import { ProfileGridViewProfileCardProps } from "@appcircle/ui/lib/ProfileGridView";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import { DistrbutionProfileDeleteModalRouteParams } from "modules/distribute/modals/DistributionProfileDeleteModalDialog";
import { DistributeProfileSettingsRouteParams } from "modules/distribute/modals/SettingModalView";
import { OSIcon } from "@appcircle/ui/lib/OSIcon";
import { TaskStatus } from "@appcircle/taskmanager/lib/TaskStatus";
import { ContextMenuItemDataType } from "@appcircle/ui/lib/menus/ContextMenuItem";
import { OSType } from "@appcircle/core/lib/enums/OSType";
import { OS } from "@appcircle/core/lib/enums/OS";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { SvgCardPin } from "@appcircle/assets/lib/CardPin";
import { SvgDeleteBox } from "@appcircle/assets/lib/DeleteBox";
import { SvgEdit } from "@appcircle/assets/lib/Edit";
import { SvgSettings } from "@appcircle/assets/lib/Settings";
import { SvgProfile } from "@appcircle/assets/lib/Profile";

export type TimeInfoType = {
  header: JSX.Element | string;
  text: string;
};

export type SubtitlesType = {
  text: string;
  type: OSType;
};

function getContextMenuItems(
  model: DistributeGroupsModel,
  item: DistributeProfileType,
  createModalUrl: ModuleContextValueType<any>["createModalUrl"],
  services: ProfilesContextServiceType,
  index: number,
  intl: IntlShape
): ContextMenuItemDataType[] {
  const iconURL =
    item.selectedOS === OS.android ? item.androidIconUrl : item.iOSIconUrl;
  return [
    {
      icon: SvgCardPin,
      text: item.pinned
        ? intl.formatMessage(messages.appCommonUnpin)
        : intl.formatMessage(messages.appCommonPin)
    },
    { icon: SvgEdit, text: intl.formatMessage(messages.appCommonRename) },
    {
      icon: SvgDeleteBox,
      text: intl.formatMessage(messages.appCommonDelete),
      linkProps: {
        to: {
          search: createModalUrl<DistrbutionProfileDeleteModalRouteParams>(
            Modals.ProfileDelete,
            ["profileId", item.id],
            ["iconUrl", btoa(iconURL)],
            ["name", item.name]
          )
        }
      }
    },
    {
      icon: SvgSettings,
      text: intl.formatMessage(messages.appCommonSettings),
      linkProps: {
        to: {
          search: createModalUrl<DistributeProfileSettingsRouteParams>(
            Modals.Settings,
            ["id", item.id]
          ),
          state: {
            name: item.name,
            iconURL,
            autoSentGroups: model.getAutoSentGroups()
          }
        }
      }
    }
  ];
}

export function DistributionProfileCard(
  props: ProfileGridViewProfileCardProps<DistributeProfileType>
) {
  const [status, setStatus] = useState<TaskStatus | undefined>(undefined);
  const { pushMessage } = useNotificationsService();
  const { createModalUrl } = useDistributeModuleContext();
  const intl = useIntl();
  const profilesModel = useDistributeProfilesModel();
  const timeInfo = useMemo(() => getTimeInfo(props.data, status, intl), [
    props.data,
    status,
    intl
  ]);
  const subtitles = useMemo(() => getSubtitles(props.data), [props.data]);
  const footerData = useMemo(() => getFooterData(props.data, intl), [
    props.data,
    intl
  ]);

  const { dispatch } = useDistributeModuleContext();
  const services = useProfilesService({});
  const model = useDistributeGroupsModel();
  // const iconURL = props.data.selectedOS === 'android' ? props.data.androidIconUrl : props.data.iOSIconUrl

  const proflleData = useMemo<ProfileCardDataType>(
    () => ({
      id: props.data.id,
      isPinned: !!props.data.pinned,
      isTemporary: !!props.data.isTemporary,
      name: props.data.name
    }),
    [props.data.id, props.data.pinned, props.data.isTemporary, props.data.name]
  );

  const contextMenuItems = useMemo(
    () =>
      getContextMenuItems(
        model,
        props.data,
        createModalUrl,
        services,
        props.index,
        intl
      ),
    [createModalUrl, intl, model, props.data, props.index, services]
  );

  const profileService = useMemo<ProfileCardServiceType>(
    () => ({
      delete: (id: string) => services.delete(id),
      rename: services.renameProfile,
      pin: services.pinProfile,
      doesNameExist: profilesModel.doesNameExist
    }),
    [services]
  );

  const [editMode, setEditMode] = useState(props.data.isTemporary);
  const onRenameError = useCallback(
    (state, newProfileName) => {
      const message =
        state === NameValidState.TOO_LONG
          ? intl.formatMessage(messages.appCommonFormFieldErrorTooLong, {
              subject: newProfileName
            })
          : state === NameValidState.TOO_SHORT
          ? intl.formatMessage(messages.appCommonFormFieldErrorTooShort, {
              subject: newProfileName
            })
          : state === NameValidState.INVALID_CHARACTER
          ? intl.formatMessage(
              messages.appCommonFormFieldErrorContainsIvalidChars,
              {
                subject: newProfileName
              }
            )
          : state === NameValidState.NAME_EXISTS &&
            intl.formatMessage(messages.appCommonSubjectExists, {
              subject: newProfileName
            });
      pushMessage({
        messageType: "error",
        message: message || ""
      });
    },
    [pushMessage, intl]
  );
  const onContextMenuItemSelected = useCallback(
    (selectedIndex: number) => {
      if (selectedIndex === 0) {
        services.pinProfile(props.data.id, !props.data.pinned);
      } else if (selectedIndex === 1) {
        setEditMode(true);
      } else if (selectedIndex === 2) {
        // setModalCallback &&
        //   setModalCallback((data: any, closeModal: () => void) => {
        //     if (data.confirmed) services.delete(props.data.id, closeModal);
        //   });
      }
    },
    [props.data.id, props.data.pinned, services]
  );

  const onBlur = useCallback(() => {
    isFakeID(props.data.id) &&
      dispatch({
        type: "DELETE_TESTER_PROFILE",
        id: props.data.id
      });
  }, [dispatch, props.data.id]);

  // const Icon = useMemo(
  //   () =>
  //     iconURL ? (

  //     ) : (
  //         <GenericAppIcon className="ProfileCard_head_icon" />
  //       ),
  //   [iconURL]
  // );

  const SubtitlesMarkup = (
    <React.Fragment>
      <div className="ProfileCard_subTitles">
        {subtitles.map((item, index: number) => {
          const iconUrl =
            item.type === "android"
              ? props.data.androidIconUrl
              : props.data.iOSIconUrl;

          return (
            <div
              key={index}
              className={classNames("ProfileCard_subTitles_item", {
                "ProfileCard_subTitles_item-empty": !item.text
              })}
            >
              {iconUrl && iconUrl.indexOf("defaultApp.png") === -1 ? (
                <img
                  width={35}
                  height={35}
                  className="icon-selected"
                  src={iconUrl}
                />
              ) : (
                <OSIcon
                  style={!!item.text ? "dark" : "light"}
                  os={item.type === "android" ? OS.android : OS.ios}
                ></OSIcon>
                // <Icon
                //   className={
                //     "DistributeProfileCard_osIcon DistributeProfileCard_osIcon-hasVersion--" +
                //     !!item.text
                //   }
                // />
              )}
              <div>
                {item.text ? "v " + item.text : "No versions available"}
              </div>
            </div>
          );
        })}
      </div>
      <div className="ProfileCard_upseperator" />
    </React.Fragment>
  );
  const onStatusChange = useCallback((status?: TaskStatus) => {
    setStatus(status);
  }, []);
  const TimeInfoMarkup = useMemo(
    () => (
      <div className="ProfileCard_info">
        <div className="ProfileCard_info_time">
          {timeInfo.map((item, index: number) => (
            <div className="ProfileCard_info_time_item" key={index}>
              <div>
                <span>{item.header}</span> <span>{item.text}</span>
              </div>
            </div>
          ))}
        </div>
        <div className="ProfileCard_info_archive">
          <SvgProfile
            className={classNames({
              "icon-unselected": props.data.lastAppVersionTestingCount === 0
            })}
          />
          <div>{`${props.data.lastAppVersionDownloadedCount || 0} / ${props.data
            .lastAppVersionTestingCount || 0}`}</div>
        </div>
      </div>
    ),
    [
      props.data.lastAppVersionDownloadedCount,
      props.data.lastAppVersionTestingCount,
      timeInfo
    ]
  );

  const onEditModeChange = useCallback(
    editMode => {
      props.onEditMode(editMode, props.index);
      setEditMode(editMode);
    },
    [props]
  );

  return (
    <ProfileCard
      onEditModeChange={onEditModeChange}
      onStatusChange={onStatusChange}
      onContextMenuItemSelected={onContextMenuItemSelected}
      editMode={!!editMode}
      service={profileService}
      onBlur={onBlur}
      data={proflleData}
      profileDetailLink={props.data.isTemporary ? "" : props.detailUrl}
      footerData={footerData}
      index={props.index}
      contextMenuItems={contextMenuItems}
      onRenameError={onRenameError}
      // IconTemplate={Icon}
    >
      {SubtitlesMarkup}
      {TimeInfoMarkup}
    </ProfileCard>
  );
}

function getTimeInfo(
  item: DistributeProfileType,
  status: TaskStatus | undefined,
  intl: IntlShape
): TimeInfoType[] {
  return [
    {
      header:
        status === "ADDED" || status === "IN_PROGRESS" ? (
          <Spinner />
        ) : item.lastUpdated ? (
          intl.formatMessage(messages.distributeProfileCardLastUpdated)
        ) : (
          intl.formatMessage(messages.distributeProfileCardCreating)
        ),
      text:
        status === "ADDED" || status === "IN_PROGRESS"
          ? getProfileCardProgressMessage(item)
          : item.lastUpdated
    },
    {
      header: item.lastShared
        ? intl.formatMessage(messages.distributeProfileCardLastShared)
        : "",
      text:
        item.lastShared ||
        intl.formatMessage(messages.distributeProfileCardNotShared)
    }
  ];
}

function getSubtitles(item: DistributeProfileType): SubtitlesType[] {
  return [
    {
      type: "ios",
      text: item.iOSVersion
    },
    {
      text: item.androidVersion,
      type: "android"
    }
  ];
}

function getFooterData(
  item: DistributeProfileType,
  intl: IntlShape
): ProfileCardFooterData[] {
  return [
    {
      header: intl.formatMessage(messages.appCommonAuthentication),
      text:
        ProfileAuthTypesbyValue[
          item.settings && item.settings.authenticationType
            ? item.settings.authenticationType - 1
            : 1
        ],
      enable: !!((item.settings?.authenticationType || 1) - 1)
    },
    {
      header: intl.formatMessage(messages.appCommonAutoSent),
      text: item.autoSentGroups.length
        ? item.autoSentGroups.join(", ")
        : intl.formatMessage(messages.appCommonDisabled),
      enable: !!item.autoSentGroups.length
    }
  ];
}
