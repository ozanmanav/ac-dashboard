import React, {
  PropsWithChildren,
  useState,
  useEffect,
  useContext,
  useCallback,
  useMemo
} from "react";
import { ProfileGroupHeader } from "./components/ProfileGroupHeader";
import { ProfileGroupsTestersTable } from "./components/ProfileGroupsTestersTable";
import { AddEmailComponent } from "@appcircle/ui/lib/AddEmailComponent";
import { TextButton } from "@appcircle/ui/lib/TextButton";
import {
  ProfileGroupContext,
  useDistributeGroupsModel
} from "./DistributeGroupsContext";
import { ProgressView } from "@appcircle/ui/lib/ProgressView";
import { DistributeGroupType } from "modules/distribute/model/TesterGroup";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { GroupList } from "@appcircle/ui/lib/GroupList";
import { alphaNummericTextMaybe } from "@appcircle/core/lib/utils/alphaNumericTextMaybe";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { GroupsDetailLayout } from "@appcircle/ui/lib/GroupsDetailLayout";
import { ButtonType } from "@appcircle/ui/lib/Button";
import { SvgProfile } from "@appcircle/assets/lib/Profile";
import { SvgTestingProfile } from "@appcircle/assets/lib/TestingProfile";
import { useGTMService } from "shared/domain/useGTMService";

export type ProfileGroupsProps = PropsWithChildren<{}>;

export function ProfileGroups(props: ProfileGroupsProps) {
  const model = useDistributeGroupsModel();
  const { dispatch } = useDistributeModuleContext();
  const { selectedGroupID, isLoaded } = model.toObject();
  const { triggerGTMEvent } = useGTMService();
  const emptyGroup = useMemo(
    () => ({
      name: "",
      id: null,
      autoSentProfileNames: [],
      isTemporary: false,
      testers: []
    }),
    []
  );
  const services = useContext(ProfileGroupContext);
  const { id, name, isTemporary, testers } = useMemo<
    DistributeGroupType
  >(() => {
    return model.getSelectedGroup() || emptyGroup;
  }, [emptyGroup, model]);
  const groups = useMemo(() => {
    return model.getGroups();
  }, [model]);
  const [state, setState] = useState(() => ({
    groupName: name,
    inProgress: false
  }));

  useEffect(() => {
    setState({ ...state, groupName: name, inProgress: false });
    // eslint-disable-next-line
  }, [name, testers]);

  const [deleteTaskId, setDeleteTaskId] = useState("");
  const taskManager = useTaskManager();
  const deleteTask = taskManager.getStatusByTaskId(deleteTaskId);
  useEffect(() => {
    deleteTask && deleteTask.status === "COMPLETED" && setDeleteTesters([]);
  }, [deleteTask, deleteTaskId]);

  const [deleteTesters, setDeleteTesters] = useState<string[]>([]);

  // useEffect(() => {
  //   setState({ ...state, groupName: name, inProgress: false });
  //   // eslint-disable-next-line
  // }, [name, testers]);

  const [mobileMode, setMobileMode] = useState(true);

  const closeGroupsList = useCallback(item => {
    setMobileMode(false);
    services.changeSelectedGroupItem(item.id);
  }, []);

  // const openGroupsList = useCallback(() => {
  //   setMobileMode(true);
  // }, []);

  const testersList = useMemo(
    () =>
      testers.map((tester: string) => ({
        id: tester, // use email as id
        email: tester
      })),
    [testers]
  );

  const onTableSelect = useCallback((items: string[]) => {
    setDeleteTesters(items);
  }, []);

  const onTableRowDelete = useCallback(() => {
    setState({ ...state, inProgress: true });
    const taskId = services.deleteTestersFromGroup(id, deleteTesters);
    setDeleteTaskId(taskId);
  }, [deleteTesters, id, services, state]);
  const intl = useIntl();
  const notification = useNotificationsService();

  const tableDeleteText = useMemo(
    () =>
      `${intl.formatMessage(messages.appCommonDelete)} (${
        deleteTesters.length
      })`,
    [deleteTesters.length, intl]
  );

  return (
    <div className="ProfileGroups">
      <GroupsDetailLayout
        onNew={useCallback(() => {
          services.addNewTemporaryGroup();
        }, [services])}
        title={intl.formatMessage(messages.distributeModulePagesGroups)}
        onMobileModeChange={setMobileMode}
        mobileMode={mobileMode}
        sideBar={
          <ProgressView
            isInProgress={!isLoaded}
            progressText="Loading groups"
            isOverlay={true}
          >
            <GroupList
              items={groups}
              selectedGroupID={selectedGroupID}
              onItemSelect={closeGroupsList}
              onChange={useCallback(
                (name: string) => {
                  setState({ ...state, groupName: name });
                },
                [state]
              )}
              onError={useCallback(
                message => {
                  notification.pushMessage({
                    messageType: "error",
                    message
                  });
                },
                [notification]
              )}
              onEnter={useCallback(
                groupName => {
                  setState({ ...state, inProgress: true, groupName });
                  services.setEditModeGroupName(id, false);
                  services.renameDistributeGroup(id, groupName);
                },
                [services]
              )}
              onBlurOrEscape={useCallback(() => {
                const group = model.getSelectedGroup();
                if (
                  !taskManager.getStatusByEntityID(group.id) &&
                  isFakeID(group.id)
                ) {
                  dispatch({
                    type: "DELETE_PROFILE_GROUP",
                    id: group.id
                  });
                } else {
                  services.setEditModeGroupName(group.id, false);
                }
              }, [taskManager, services, model, dispatch])}
              // onEnter={onNameChangeEnd}
              onValidate={alphaNummericTextMaybe}
              fallback={
                <div className="ProfileVersions_emptyVersion">
                  {intl.formatMessage(messages.distributeGroupsNoGroups)}
                </div>
              }
            />
          </ProgressView>
        }
        detail={
          <ProgressView isInProgress={!isLoaded} progressText="Loading testers">
            {name ? (
              <div className="ProfileGroups_detail">
                <div className="ProfileGroupHeader">
                  <ProfileGroupHeader name={state.groupName} />
                  <AddEmailComponent
                    emails={testers}
                    onAddEmail={(emails: string[]) => {
                      triggerGTMEvent("dist_tester_new");
                      setState({ ...state, inProgress: true });
                      services.addTestersToGroup(id, emails);
                    }}
                  />
                </div>
                {testers.length ? (
                  <div className="ProfileGroups_testersTable">
                    <div className="ProfileGroups_testersTable_testers">
                      <span>Testers ({testers.length})</span>
                      {deleteTesters.length && !state.inProgress ? (
                        <TextButton
                          type={ButtonType.DANGER}
                          title={tableDeleteText}
                          onClick={onTableRowDelete}
                        />
                      ) : null}
                    </div>
                    <ProfileGroupsTestersTable
                      items={testersList}
                      onTableSelect={onTableSelect}
                      inProgress={state.inProgress}
                    />
                  </div>
                ) : (
                  <div className="ProfileGroups_notesters">
                    <SvgProfile />
                    <span>
                      {intl.formatMessage(messages.distributeGroupsNoTesters)}
                    </span>
                  </div>
                )}
              </div>
            ) : (
              <div className="NoProfile">
                <SvgTestingProfile className="NoProfile_icon" />
                <div className="NoProfile_text">
                  {intl.formatMessage(
                    messages.distributeGroupsNoTestersandGroups
                  )}
                </div>
              </div>
            )}
          </ProgressView>
        }
      />
    </div>
  );
}
