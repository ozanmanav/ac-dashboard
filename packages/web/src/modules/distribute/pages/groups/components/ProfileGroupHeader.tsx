import React, {
  PropsWithChildren,
  useContext,
  useMemo,
  useCallback
} from "react";
import { Modals } from "../../../DistributeModule";
import { ThreeDotsToggle } from "@appcircle/ui/lib/ThreeDotsToggle";
import { ModalContext } from "@appcircle/shared/lib/services/ModalContext";
import {
  ProfileGroupContext,
  useDistributeGroupsModel
} from "../DistributeGroupsContext";

import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { ProfileDetailHeader } from "shared/profile-detail-layout/ProfileDetailHeader";
import { DistributionTesterGroupDeleteModalDialogRouteParams } from "modules/distribute/modals/DistributionTestersGroupDeleteModalDialog";
import { ContextMenuItemProps } from "@appcircle/ui/lib/menus/ContextMenuItem";
import { ContextMenu } from "@appcircle/ui/lib/menus/ContextMenu";
import { SvgEdit } from "@appcircle/assets/lib/Edit";
import { SvgDuplicate } from "@appcircle/assets/lib/Duplicate";
import { SvgDeleteBox } from "@appcircle/assets/lib/DeleteBox";

export type ProfileGroupHeaderProps = PropsWithChildren<{
  name: string;
}>;
export function ProfileGroupHeader(props: ProfileGroupHeaderProps) {
  let { createModalUrl } = useDistributeModuleContext();
  const { setModalCallback } = useContext(ModalContext);
  const model = useDistributeGroupsModel();
  const services = useContext(ProfileGroupContext);
  const { autoSentProfileNames, id } = model.getSelectedGroup() || {
    id: null,
    autoSentProfileNames: []
  };
  const selectedGroup = model.getSelectedGroup();
  const intl = useIntl();
  const contextMenuItems = useMemo<Array<ContextMenuItemProps>>(
    () => [
      {
        icon: SvgEdit,
        text: intl.formatMessage(messages.appCommonRename)
      },
      //TODO { icon: icons.import, text: "Import" },
      {
        icon: SvgDuplicate,
        text: intl.formatMessage(messages.appCommonDuplicate)
      },
      {
        icon: SvgDeleteBox,
        text: intl.formatMessage(messages.appCommonDelete),
        linkProps: {
          to: {
            search: createModalUrl<
              DistributionTesterGroupDeleteModalDialogRouteParams
            >(
              Modals.TesterGroupDelete,
              ["profileId", id],
              ["name", selectedGroup.name]
            )
          }
        }
      }
    ],
    // eslint-disable-next-line
    [createModalUrl, props.name]
  );
  const onContextMenuItemSelected = useCallback(
    (selectedIndex: number) => {
      if (selectedIndex === 0) {
        services.setEditModeGroupName(id, true);
      } else if (selectedIndex === 1) {
        services.duplicateGroup(id);
      } else if (selectedIndex === 2) {
        setModalCallback &&
          setModalCallback((data: any, closeModal: () => void) => {
            if (data.confirmed) {
              services.deleteGroup(id, closeModal);
            }
          });
      }
      // eslint-disable-next-line
    },
    [id, services, setModalCallback]
  );
  return (
    <ProfileDetailHeader
      leftItem={
        <div className="ProfileGroupHeader_title_main">
          <div className="ProfileGroupHeader_title_main_h1">{props.name}</div>
          <div className="ProfileGroupHeader_title_main_h2">
            Auto Send:{" "}
            {autoSentProfileNames.length
              ? autoSentProfileNames.join(", ")
              : "NA"}
          </div>
        </div>
      }
      rightItem={
        <ThreeDotsToggle>
          <ContextMenu
            items={contextMenuItems}
            onMenuItemSelected={onContextMenuItemSelected}
          />
        </ThreeDotsToggle>
      }
    />
  );
}
