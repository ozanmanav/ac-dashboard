import React from "react";
import {
  BasicDataGridWithProgress,
  BasicDataGridItemsSelectedHandler
} from "@appcircle/ui/lib/datagrid/BasicDataGrid";

function HeaderTemplate(props: { headers: string[] }) {
  return (
    <React.Fragment>
      <div className="ProfileTestersTableItem_emailteam">
        <div>Email</div>
      </div>
    </React.Fragment>
  );
}

function RowSeperatorTemplate() {
  return <div className="ProfileTestersTableItem_seperator" />;
}

function rowTemplate(props: {
  data: {
    id: string;
    email: string;
    team?: string;
  };
}) {
  return (
    <React.Fragment>
      <div className="ProfileTestersTableItem_emailteam">
        <div>{props.data.email}</div>
        {/*props.data.team ? <div>{props.data.team}</div> : null*/}
      </div>
    </React.Fragment>
  );
}
export type ProfileGroupTestersTableItemProps = {
  id: string;
  email: string;
  team?: string;
  hasSeperator?: boolean;
};
export type ProfileTestersTableProps = {
  items: Array<ProfileGroupTestersTableItemProps>;
  onTableSelect?: BasicDataGridItemsSelectedHandler;
  inProgress?: boolean;
};
export function ProfileGroupsTestersTable(props: ProfileTestersTableProps) {
  return (
    <BasicDataGridWithProgress
      onItemsSelected={props.onTableSelect}
      data={props.items}
      headerClassName={"ProfileTestersTableItem ProfileTestersTableItem-header"}
      headerTemplate={HeaderTemplate}
      rowClassName={"ProfileTestersTableItem"}
      rowTemplate={rowTemplate}
      rowSeperatorTemplate={RowSeperatorTemplate}
      inProgress={props.inProgress}
    />
  );
}
