import React, { PropsWithChildren, useCallback } from "react";
import classNames from "classnames";
import { TextField } from "@appcircle/ui/lib/TextField";
import { alphaNummericTextMaybe } from "@appcircle/core/lib/utils/alphaNumericTextMaybe";
import { SvgProfile } from "@appcircle/assets/lib/Profile";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";

export type ProfileGroupListItemType = {
  id: any;
  name: string;
  memberNum?: number;
  editMode?: boolean;
  testers: string[];
  isTemporary?: boolean;
};
export type ProfileGroupListItemProps = {
  isSelected: boolean;
  index: number;
  onClick?: (target: ProfileGroupListItemType) => void;
  onNameChange?: (text: string) => void;
  onNameChangeEnd?: (text: string) => void;
};

export function ProfileGroupListItem(
  props: PropsWithChildren<
    ProfileGroupListItemProps & {
      data: ProfileGroupListItemType;
    }
  >
) {
  const onClick = useCallback(() => {
    props.onClick && props.onClick(props.data);
  }, [props]);

  return (
    <div
      className={classNames("ProfileGroupListItem", {
        "ProfileGroupListItem-selected": props.isSelected
      })}
      onClick={onClick}
    >
      <div className="ProfileGroupListItem_detail">
        <TextField
          onValidate={alphaNummericTextMaybe}
          className="ProfileGroupListItem_detail_name"
          text={props.data.isTemporary ? "" : props.data.name}
          placeHolder={props.data.isTemporary ? props.data.name : ""}
          editMode={isFakeID(props.data.id) || props.data.editMode}
          onChange={props.onNameChange}
          onChangeEnd={props.onNameChangeEnd}
        />
        <div className="ProfileGroupListItem_detail_testersNum">
          <SvgProfile className="ProfileGroupListItem_detail_testersNum_icon" />
          {props.data.testers.length || 0}
        </div>
      </div>
    </div>
  );
}
