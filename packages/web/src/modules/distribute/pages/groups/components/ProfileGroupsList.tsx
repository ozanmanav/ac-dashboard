import React, {
  PropsWithChildren,
  useContext,
  useMemo,
  useCallback,
  ReactElement
} from "react";
import {
  ProfileGroupListItem,
  ProfileGroupListItemType
} from "./ProfileGroupListItem";
import { ProfileGroupContext } from "../DistributeGroupsContext";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { ScrollView } from "@appcircle/ui/lib/ScrollView";

export type ProfileGroupListItem = {};
export type ProfileGroupsListProps = PropsWithChildren<{
  items: Array<ProfileGroupListItemType>;
  selectedGroupID: string;
  onNameChange?: (name: string) => void;
  onNameChangeEnd?: (name: string) => void;
  onItemSelect?: (index: number) => void;
  fallback: ReactElement;
}>;

export function ProfileGroupsList(props: ProfileGroupsListProps) {
  const services = useContext(ProfileGroupContext);
  const onClick = useCallback<(item: ProfileGroupListItemType) => void>(
    item => {
      item.id !== props.selectedGroupID &&
        services.changeSelectedGroupItem(item.id);
      props.onItemSelect && props.onItemSelect(item.id);
    },
    [props, services]
  );
  const children = useMemo(() => {
    return props.items.map((item, index: number) => (
      <div className="ProfileGroupsList_item" key={item.id}>
        <TaskProgressView entityID={item.id}>
          <ProfileGroupListItem
            data={item}
            index={index}
            isSelected={item.id === props.selectedGroupID}
            onClick={onClick}
            onNameChange={item.editMode ? props.onNameChange : undefined}
            onNameChangeEnd={item.editMode ? props.onNameChangeEnd : undefined}
          />
        </TaskProgressView>
      </div>
    ));
  }, [
    onClick,
    props.items,
    props.onNameChange,
    props.onNameChangeEnd,
    props.selectedGroupID
  ]);
  return (
    <div key={0} className="ProfileGroupsList">
        <ScrollView className="ProfileGroupsList_scrollbar">
          {children}
        </ScrollView>
        {props.items.length === 0 && <div>{props.fallback}</div>}
    </div>
  );
}
