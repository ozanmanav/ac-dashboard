import React, { PropsWithChildren, useEffect, Context, useMemo } from "react";
import { DistributeGroupState } from "../../reducers/groups/reducer";
import { useDistributeModuleContext } from "../../TestingDistributionContext";
import {
  DistributeGroupResponseType,
  DistributeGroupType,
  DistributeAutoSentGroupsType
} from "../../model/TesterGroup";
import { ConsumerFactoryFn } from "@appcircle/shared/lib/services/createService";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import {
  DistStoreType,
  DistributeModuleApiBase
} from "modules/distribute/DistributeModule";
import { Message } from "@appcircle/shared/lib/Module";
import { useIntl, IntlShape } from "react-intl";
import messages from "modules/distribute/messages";
import { createCopyName } from "@appcircle/core/lib/utils/createCopyName";
import { zip } from "rxjs/operators";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { TaskManagerContextType } from "@appcircle/taskmanager/lib/TaskManagerContextType";
import { Result } from "@appcircle/core/lib/services/Result";

export class DistributeGroupsModel {
  constructor(private state: DistributeGroupState) {}
  toObject() {
    return this.state;
  }
  findById(id: string) {
    return this.state.groups[this.state.groupsByID[id]];
  }
  getGroups() {
    return this.state.groups;
  }
  isLoaded() {
    return this.state.isLoaded;
  }
  getSelectedGroup() {
    return this.state.groups[this.state.groupsByID[this.state.selectedGroupID]];
  }
  isExistingName(name: string, id: string) {
    return this.state.groups.some(
      group => group.name === name && group.id !== id
    );
  }
  length() {
    return this.state.groupsByID;
  }
  doesGroupExist(groupName: string) {
    return this.state.groups.some(
      group => group.name === groupName && !group.isTemporary
    );
  }
  filter(fn: (item: DistributeGroupType, index: number) => boolean) {
    return this.state.groups.filter((item, i) => {
      return fn(item, i);
    });
  }
  getAutoSentGroups() {
    //TODO : Add tags when api responses ok
    return this.state.groups.map(
      (group: DistributeGroupType) =>
        ({
          id: group.id,
          tags: [],
          name: group.name,
          isActive: false
        } as DistributeAutoSentGroupsType)
    );
  }
}

type ProfileGroupContextServiceType = {
  changeSelectedGroupItem: (id: string) => void;
  addTestersToGroup: (id: string, testers: string[]) => void;
  deleteTestersFromGroup: (id: string, testers: string[]) => string;
  addNewTemporaryGroup: () => void;
  renameDistributeGroup: (id: string, neName: string) => void;
  setEditModeGroupName: (id: string, value: boolean) => void;
  duplicateGroup: (id: string) => void;
  deleteGroup: (id: string, onFinally: () => void) => void;
  updateGroups: (notifyMe?: boolean) => void;
};
export const ProfileGroupContext = React.createContext<
  ProfileGroupContextServiceType
>({} as any);

function createServiceBoundry(
  model: DistributeGroupsModel,
  context: ModuleContextValueType<DistStoreType>,
  createApiConsumer: ConsumerFactoryFn,
  taskManager: TaskManagerContextType,
  pushMessage: (message: Message) => void,
  intl: IntlShape
): ProfileGroupContextServiceType {
  const serviceCreate = createApiConsumer();
  const { addTask, createApiTask } = taskManager;

  return {
    updateGroups: (notifyMe?: boolean) => {
      const getTesters = serviceCreate({
        method: "GET",
        endpoint: `${DistributeModuleApiBase}/groups/testers`
      });
      const getGroups = serviceCreate({
        method: "GET",
        endpoint: `${DistributeModuleApiBase}/groups`
      });
      const task = createApiTask(getGroups, {
        pipe: [zip(getTesters({ next: () => {} }))],
        onSuccess: results => {
          context.dispatch({
            type: "UPDATE_GROUP_DATA",
            groups: (results as Result[])[0]
              .json as DistributeGroupResponseType[],
            testers: (results as Result[])[1].json as string[]
          });
        },
        errorMessage: notifyMe
          ? () => intl.formatMessage(messages.distributeGroupsUpdateError)
          : undefined
      });
      task.entityID = "update-distribute-groups";
      addTask(task);
    },
    changeSelectedGroupItem: (id: string) => {
      context.dispatch({
        type: "CHANGE_SELECTED_GROUP_ITEM",
        id
      });
    },
    addTestersToGroup: (id: string, testers: string[]) => {
      const task = createApiTask(
        serviceCreate({
          method: "POST",
          endpoint: `${DistributeModuleApiBase}/groups/${id}/testers`,
          data: JSON.stringify(testers)
        }),
        {
          onSuccess: result => {
            context.dispatch({
              type: "ADD_TESTERS_TO_GROUP",
              id,
              testers
            });
          },
          successMessage: () =>
            intl.formatMessage(messages.distributeGroupsAddTestersSuccess),
          errorMessage: () =>
            intl.formatMessage(messages.distributeGroupsAddTestersError)
        }
      );
      task.entityID = id;
      addTask(task);
    },
    deleteTestersFromGroup: (id: string, testers: string[]) => {
      const task = createApiTask(
        serviceCreate({
          method: "DELETE",
          endpoint: `${DistributeModuleApiBase}/groups/${id}/testers`,
          data: JSON.stringify(testers)
        }),
        {
          onSuccess: () => {
            context.dispatch({
              type: "DELETE_TESTERS_FROM_GROUP",
              id,
              testers
            });
          },
          successMessage: () =>
            intl.formatMessage(messages.distributeGroupsDeleteTestersSuccess),
          errorMessage: () =>
            intl.formatMessage(messages.distributeGroupsDeleteTestersError)
        }
      );
      task.entityID = id;
      addTask(task);
      return task.id;
    },
    addNewTemporaryGroup: () => {
      context.dispatch({
        type: "ADD_NEW_TEMPLATE_PROFILE_GROUP"
      });
    },
    renameDistributeGroup: (id: string, groupName: string) => {
      groupName = groupName.trim();
      const group = model.findById(id);
      const isTemp = isFakeID(id);
      const oldName = group.name;
      context.dispatch({
        type: "RENAME_PROFILE_GROUP_NAME",
        id,
        newName: groupName
      });
      if (isTemp) {
        //create new group.
        const task = createApiTask<DistributeGroupResponseType>(
          serviceCreate({
            method: "POST",
            endpoint: `${DistributeModuleApiBase}/groups`,
            data: JSON.stringify({
              name: groupName,
              testers: []
            })
          }),
          {
            onSuccess: result => {
              context.dispatch({
                type: "DELETE_PROFILE_GROUP",
                id
              });
              context.dispatch({
                type: "ADD_PROFILE_GROUP",
                group: (result as Result).json
              });
            },
            successMessage: () =>
              intl.formatMessage(messages.distributeGroupsCreateGroupSuccess, {
                name: groupName
              }),
            errorMessage: () =>
              intl.formatMessage(messages.distributeGroupsCreateGroupError, {
                name: groupName
              })
          }
        );
        task.entityID = id;
        addTask(task);
      } else {
        // rename existing group.
        const task = createApiTask(
          serviceCreate({
            method: "PATCH",
            endpoint: `${DistributeModuleApiBase}/groups/${id}?name=${groupName}`,
            data: ""
          }),
          {
            onSuccess: result => {
              // context.dispatch({
              //   type: "RENAME_PROFILE_GROUP_NAME",
              //   id,
              //   newName: groupName
              // });
            },
            onError() {
              context.dispatch({
                type: "RENAME_PROFILE_GROUP_NAME",
                id,
                newName: oldName
              });
            },
            successMessage: () =>
              intl.formatMessage(messages.distributeGroupsRenameGroupSuccess, {
                name: groupName
              }),
            errorMessage: () =>
              intl.formatMessage(messages.distributeGroupsRenameGroupError, {
                name: groupName
              })
          }
        );
        task.entityID = id;
        addTask(task);
        return task.id;
      }
      // }
      // }
    },
    setEditModeGroupName: (id: string, value: boolean) => {
      context.dispatch({
        type: "SET_EDIT_MODE_PROFILE_GROUP_NAME",
        id,
        value
      });
    },
    duplicateGroup: (id: string) => {
      const group = model.findById(id);
      const duplicateGroupName = createCopyName(
        group,
        model.isExistingName.bind(model)
      );
      // `${group.name} ${intl.formatMessage(
      //   messages.distributeGroupsDuplicationSuffix
      // )}`;
      const suffixMatch = new RegExp(`^${duplicateGroupName}\\s[0-9]*$`);
      const duplicateGroupNameSuffix = model.filter(g =>
        suffixMatch.test(g.name)
      ).length;
      const duplicateGroupNameWithSuffix = `${duplicateGroupName}${duplicateGroupNameSuffix ||
        ""}`;
      const task = createApiTask(
        serviceCreate({
          method: "POST",
          endpoint: `${DistributeModuleApiBase}/groups`,
          data: JSON.stringify({
            name: `${duplicateGroupNameWithSuffix}`,
            testers: group.testers
          })
        }),
        {
          onSuccess: result => {
            context.dispatch({
              type: "ADD_PROFILE_GROUP",
              group: (result as Result).json as DistributeGroupResponseType
            });
          },
          successMessage: () =>
            intl.formatMessage(messages.distributeGroupsDuplicateGroupSuccess, {
              name: group.name
            }),
          errorMessage: () =>
            intl.formatMessage(messages.distributeGroupsDuplicateGroupError, {
              name: group.name
            })
        }
      );
      task && addTask(task);
    },
    deleteGroup: (id: string, onFinally: () => void) => {
      const group = model.findById(id);
      const task = createApiTask(
        serviceCreate({
          method: "DELETE",
          endpoint: `${DistributeModuleApiBase}/groups/${id}`,
          data: ""
        }),
        {
          onSuccess: result => {
            context.dispatch({
              type: "DELETE_PROFILE_GROUP",
              id
            });
          },
          onFinally,
          successMessage: () =>
            intl.formatMessage(messages.distributeGroupsDeleteGroupSuccess, {
              name: group.name
            }),
          errorMessage: () =>
            intl.formatMessage(messages.distributeGroupsDeleteGroupError, {
              name: group.name
            })
        }
      );
      task && addTask(task);
    }
  };
}

export function useDistributeGroupsModel() {
  const { state } = useDistributeModuleContext();
  const model = useMemo(() => new DistributeGroupsModel(state.groups), [
    state.groups
  ]);
  return model;
}

// TODO: Rename to useProfileGroupsService
export function ProfileGroupContextProvider(
  props: PropsWithChildren<{
    services?: ProfileGroupContextServiceType;
    context?: Context<ModuleContextValueType<DistStoreType>>;
  }>
) {
  const model = useDistributeGroupsModel();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const _context = useDistributeModuleContext();
  const { pushMessage } = useNotificationsService();
  const intl = useIntl();

  const _services = useMemo(
    () =>
      createServiceBoundry(
        model,
        _context,
        createApiConsumer,
        taskManager,
        pushMessage,
        intl
      ),
    [_context, createApiConsumer, model, pushMessage, taskManager, intl]
  );

  useEffect(() => {
    _services.updateGroups(true);
    // eslint-disable-next-line
  }, []);

  return (
    <ProfileGroupContext.Provider value={_services}>
      {props.children}
    </ProfileGroupContext.Provider>
  );
}
