import { defineMessages } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";

const module = "app.modules.distribute.";

export default defineMessages({
  distributeModuleHeader: {
    id: module + "header",
    defaultMessage: "Distribute"
  },
  distributeModulePagesProfiles: {
    id: module + "pages.profiles",
    defaultMessage: "Distribution Profiles"
  },
  distributeModulePagesGroups: {
    id: module + "pages.groups",
    defaultMessage: "Testing Groups"
  },
  distributeModulePagesStartGuide: {
    id: module + "pages.startGuide",
    defaultMessage: "Start Guide"
  },
  distributeModulePagesReports: {
    id: module + "pages.reports",
    defaultMessage: "Reports"
  },
  distributeModalDeleteGroupTitle: {
    id: module + ".modal.deleteGroup.title",
    defaultMessage: "Are you sure you want to delete the following group?"
  },
  distributeModalDeleteGroupPlaceHolder: {
    id: module + ".modal.deleteGroup.placeholder",
    defaultMessage: "Type the profile name to confirm deletion"
  },
  distributeModalDeleteGroupSuccess: {
    id: module + ".modal.deleteGroup.succcess",
    defaultMessage: "Tester group {name} deleted"
  },
  distributeModalDeleteGroupError: {
    id: module + ".modal.deleteGroup.error",
    defaultMessage: "Tester group {name} cannot be deleted"
  },
  distributeGroupsUpdateSuccess: {
    id: module + ".groups.update.success",
    defaultMessage: "Testing groups updated."
  },
  distributeGroupsUpdateError: {
    id: module + ".groups.update.error",
    defaultMessage: "Testing groups cannot be updated"
  },
  distributeGroupsAddTestersSuccess: {
    id: module + ".groups.addTesters.success",
    defaultMessage: "Tester added"
  },
  distributeGroupsAddTestersError: {
    id: module + ".groups.addTesters.error",
    defaultMessage: "Tester cannot be added"
  },
  distributeGroupsDeleteTestersError: {
    id: module + ".groups.deleteTesters.error",
    defaultMessage: "Tester(s) cannot be deleted"
  },
  distributeGroupsDeleteTestersSuccess: {
    id: module + ".groups.deleteTesters.success",
    defaultMessage: "Tester(s) deleted"
  },
  distributeGroupsCreateGroupSuccess: {
    id: module + ".groups.createGroup.success",
    defaultMessage: "Testing group {name} added"
  },
  distributeGroupsCreateGroupError: {
    id: module + ".groups.createGroup.error",
    defaultMessage: "Testing group {name} cannot be added"
  },
  distributeGroupsRenameGroupSuccess: {
    id: module + ".groups.renameGroup.success",
    defaultMessage: "Testing group {name} renamed"
  },
  distributeGroupsRenameGroupError: {
    id: module + ".groups.renameGroup.error",
    defaultMessage: "Testing group {name} cannot be renamed"
  },
  distributeGroupsDuplicateGroupSuccess: {
    id: module + ".groups.duplicateGroup.success",
    defaultMessage: "Testing group {name} duplicated"
  },
  distributeGroupsDuplicateGroupError: {
    id: module + ".groups.duplicateGroup.error",
    defaultMessage: "Testing group {name} cannot be duplicated"
  },
  distributeGroupsDeleteGroupSuccess: {
    id: module + ".groups.deleteGroup.error",
    defaultMessage: "Testing group {name} deleted"
  },
  distributeGroupsDeleteGroupError: {
    id: module + ".groups.deleteGroup.error",
    defaultMessage: "Testing group {name} cannot be deleted"
  },
  distributeGroupsRenameTooLong: {
    id: module + ".groups.renameGroup.tooLong",
    defaultMessage: "Group name is too long"
  },
  distributeGroupsRenameTooShort: {
    id: module + ".groups.renameGroup.tooShort",
    defaultMessage: "Group name is too short"
  },
  distributeGroupsRenameInvalid: {
    id: module + ".groups.renameGroup.invalid",
    defaultMessage: "Group name contains invalid characters"
  },
  distributeGroupsRenameExistingName: {
    id: module + ".groups.renameGroup.existingName",
    defaultMessage: "Group name {name} already exists."
  },
  distributeGroupsDuplicationSuffix: {
    id: module + ".groups.duplicationSuffix",
    defaultMessage: "Copy"
  },
  distributeGroupsNoGroups: {
    id: module + ".groups.noGroups",
    defaultMessage: "No testing groups available"
  },
  distributeProfileTesters: {
    id: module + ".groups.testers",
    defaultMessage: "Testers"
  },
  distributeGroupsNoTesters: {
    id: module + ".groups.noTesters",
    defaultMessage: "No testers available"
  },
  distributeGroupsNoTestersandGroups: {
    id: module + ".groups.noTestersandGroups",
    defaultMessage:
      "No testing groups available.\nCreate a group to assign testers."
  },
  distributeProfilesNoItems: {
    id: module + ".profiles.noItems",
    defaultMessage:
      "No distribution profiles available.\nCreate a distribution profile to share apps."
  },
  distributeProfileDetailLoading: {
    id: module + ".profiles.detail.loading",
    defaultMessage: "Loading version details..."
  },
  distributeProfileDetailMenu_SubmitPublic: {
    id: module + ".profiles.detail.menu.submitPublic",
    defaultMessage: "Submit to Public App Stores"
  },
  distributeProfileDetailMenu_SubmitEnterprise: {
    id: module + ".profiles.detail.menu.submitEnterprise",
    defaultMessage: "Submit to Enterprise App Store"
  },
  distributeProfileModalDeleteProfileVersionTitle: {
    id: module + ".profiles.modal.deleteProfileVersion.title",
    defaultMessage: "Are you sure you want to delete the following version?"
  },
  distributeProfileModalDeleteProfileVersionPlaceholder: {
    id: module + ".profiles.modal.deleteProfileVersion.placeholder",
    defaultMessage: "Type the version to confirm deletion"
  },
  distributeProfileModalDeleteProfileVersionSuccess: {
    id: module + ".profiles.modal.deleteProfileVersion.success",
    defaultMessage: "App version deleted"
  },
  distributeProfileModalDeleteProfileVersionError: {
    id: module + ".profiles.modal.deleteProfileVersion.error",
    defaultMessage: "Version cannot be deleted"
  },
  distributeProfileVersionsBadgesVersion: {
    id: module + ".profiles.versions.badges.version",
    defaultMessage: "version"
  },
  distributeProfileVersionsBadgesSubmitted: {
    id: module + ".profiles.versions.badges.submitted",
    defaultMessage: "submitted"
  },
  distributeProfileDetailNewTag: {
    id: module + ".profiles.detail.newTag",
    defaultMessage: "Enter Tag"
  },
  distributeProfileDetailSendtoTest: {
    id: module + ".profiles.detail.sendtoTest",
    defaultMessage: "Share with Testers {count}"
  },
  distributeProfileUpdateSuccess: {
    id: module + ".profiles.update.success",
    defaultMessage: "Distribution profile updated"
  },
  distributeProfileDetailCopyMessage: {
    id: module + ".profiles.detail.copyMessage",
    defaultMessage: "Copied to clipboard"
  },
  distributeProfileDetailMoMessage: {
    id: module + ".profiles.detail.noMessage",
    defaultMessage: "No Message"
  },
  distributeProfileDetailVersionsLoading: {
    id: module + ".profiles.detail.versions.loading",
    defaultMessage: "Loading versions..."
  },
  distributeProfileDetailVersionsNoVersions: {
    id: module + ".profiles.detail.versions.noVersions",
    defaultMessage: "No versions available"
  },
  distributeProfileDetailUploadFileSuccess: {
    id: module + ".profiles.detail.uploadFile.success",
    defaultMessage: "File upload complete"
  },
  distributeProfileDetailUploadFileError: {
    id: module + ".profiles.detail.uploadFile.error",
    defaultMessage: "File cannot be uploaded!"
  },
  distributeProfileModalDeleteProfileTitle: {
    id: module + ".profiles.detail.deleteProfile.title",
    defaultMessage:
      "Are you sure you want to delete the following distribution profile?"
  },
  distributeProfileModalDeleteProfilePlaceHolder: {
    id: module + ".profiles.detail.deleteProfile.placeHolder",
    defaultMessage: "Type the profile name to confirm deletion"
  },
  distributeProfileModalDeleteProfileSuccess: {
    id: module + ".profiles.detail.deleteProfile.success",
    defaultMessage: "Distribution profile deleted"
  },
  distributeProfileModalDeleteProfileError: {
    id: module + ".profiles.detail.deleteProfile.error",
    defaultMessage: "Distribution profile cannot be deleted"
  },
  distributeProfileCardLastUpdated: {
    id: module + ".profiles.profileCard.lasUpdated",
    defaultMessage: "Last Updated: "
  },
  distributeProfileCardCreating: {
    id: module + ".profiles.profileCard.creating",
    defaultMessage: "Creating..."
  },
  distributeProfileCardLastShared: {
    id: module + ".profiles.profileCard.lastShared",
    defaultMessage: "Last Shared: "
  },
  distributeProfileCardNotShared: {
    id: module + ".profiles.profileCard.notShared",
    defaultMessage: "Not Shared"
  },
  distributeModalSendToTesters: {
    id: module + ".modal.sendtoTestesters",
    defaultMessage: "Share with Testers: "
  },
  distributeModalSendToTestersSuccess: {
    id: module + ".modal.sendtoTestesters.success",
    defaultMessage: "Version shared with the testers"
  },
  distributeModalSendToTestersError: {
    id: module + ".modal.sendtoTestesters.error",
    defaultMessage: "{status} {error}, please try again."
  },
  distributeModalSendToTestersEmailsTitle: {
    id: module + ".modal.sendtoTestesters.emails",
    defaultMessage: "RECIPIENTS (TESTING GROUPS OR INDIVIDUAL TESTERS)"
  },
  distributeModalSendToTestersEmailsFinalTitle: {
    id: module + ".modal.sendtoTestesters.emails",
    defaultMessage: "CONFIRM RECIPIENTS"
  },
  distributeModalSendToTestersEmailsSubtitle: {
    id: module + ".modal.sendtoTestesters.emails.subtitle",
    defaultMessage:
      "You can add testing group names or email addresses, separated by commas"
  },
  distributeModalSendToTestersEmailsFinalSubtitle: {
    id: module + ".modal.sendtoTestesters.emails.subtitle",
    defaultMessage: "Please confirm the following recipients"
  },
  distributeModalSendToTestersEmailDuplicationError: {
    id: module + ".modal.sendtoTestesters.emails.duplicationError",
    defaultMessage: "Duplicate entries present, please check your inputs"
  },
  distributeModalSendToTestersEmailInvalidError: {
    id: module + ".modal.sendtoTestesters.emails.invalidError",
    defaultMessage: "Invalid entries present, please check your inputs"
  },
  distributeModalSendToTestersMessage: {
    id: module + ".modal.sendtoTestesters.message",
    defaultMessage: "MESSAGE TO TESTERS"
  },
  distributeModalSendToTestersFinalMessage: {
    id: module + ".modal.sendtoTestesters.message",
    defaultMessage: "CONFIRM THE MESSAGE TO TESTERS"
  },
  distributeModalSendToTestersMessageSubtitle: {
    id: module + ".modal.sendtoTestesters.message.subtitle",
    defaultMessage:
      "Write testing instructions, release notes or any other message to the testers"
  },
  distributeModalSendToTestersMessageFinalSubtitle: {
    id: module + ".modal.sendtoTestesters.message.subtitle",
    defaultMessage:
      "Plase confirm the following message accompanying the shared version"
  },
  distributeModalSettings: {
    id: module + ".modal.settings",
    defaultMessage: "Profile Settings"
  },
  distributeModalSettingsAuthenticationType: {
    id: module + ".modal.settings.authenticationType",
    defaultMessage: "Select Authentication Type"
  },
  distributeModalSettingsAuthenticationTypeTip: {
    id: module + ".modal.settings.authenticationTypeTip",
    defaultMessage:
      "Select a method for users to verify their identity before downloading apps"
  },
  distributeModalSettingsAutoSend: {
    id: module + ".modal.settings.autoSend",
    defaultMessage: "Select Auto-Send Recipient Groups"
  },
  distributeModalSettingsAutoSendTip: {
    id: module + ".modal.settings.autoSend",
    defaultMessage:
      "Select testing groups that will receive the new versions automatically"
  },
  ...CommonMessages
});
