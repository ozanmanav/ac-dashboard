import { DistributeProfileType } from "../model/DistributeProfile";
import { ProfileCardStatus } from "@appcircle/ui/lib/ProfileCard";
import { OS } from "@appcircle/core/lib/enums/OS";
export function createEmptyDistributeProfile(id: string) {
  const emptyCard: DistributeProfileType = {
    id,
    name: "New Profile",
    androidIconUrl: "",
    iOSIconUrl: "",
    lastUpdated: "",
    lastShared: "",
    autoSentGroups: [],
    apps: [],
    appsByID: {},
    appsByIOS: {},
    appsByAndroid: {},
    tempAppProfilesByID: {},
    totalAppCount: 24,
    selectedOS: OS.ios,
    organizationId: "",
    pinned: false,
    isTemporary: true,
    state: ProfileCardStatus.EDITING,
    selectedAppProfileID: { ios: "", android: "" },
    androidVersion: "",
    appVersions: [],
    createDate: "",
    iOSVersion: "",
    lastAppVersionClickedCount: 0,
    lastAppVersionCreateDate: "",
    lastAppVersionDownloadedCount: 0,
    lastAppVersionSharedDate: "",
    lastAppVersionTestingCount: 0,
    updateDate: ""
  };
  return emptyCard;
}
