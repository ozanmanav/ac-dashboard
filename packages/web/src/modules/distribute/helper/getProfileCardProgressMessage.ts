import { ProfileCardStatus } from "@appcircle/ui/lib/ProfileCard";
import { DistributeProfileType } from "modules/distribute/model/DistributeProfile";
export function getProfileCardProgressMessage(
  profile: DistributeProfileType
): string {
  let res = "Now";
  if (profile.state === ProfileCardStatus.INPROGRESS) {
    if (profile.isTemporary) {
      res = "\tCreating...";
    } else {
      res = "\tUpdating...";
    }
  }
  return res;
}
