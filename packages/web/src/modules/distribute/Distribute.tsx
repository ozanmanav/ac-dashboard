import { PropsWithChildren, useEffect, Suspense } from "react";
import React from "react";
import { Switch, Route } from "react-router";
import { ProfileDetailContextProvider } from "./pages/profile/ProfileDetailContext";
import { ProfileDetail } from "./pages/profile/ProfileDetail";
import { ProfileGroups } from "./pages/groups/ProfileGroups";
import { DistributionProfiles } from "./pages/profiles/components/DistributionProfiles";
import { useDistributeModuleContext } from "./TestingDistributionContext";
import { useProfilesService } from "./pages/profiles/ProfilesService";
import { ModalRoute } from "@appcircle/shared/lib/helpers/modal/ModalRoute";
import {
  DistributeModals,
  DistributeModulePathPrefix
} from "./DistributeModule";
import { EmptyModule } from "modules/EmptyModule";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { useIntl } from "react-intl";

export function Modal() {
  return <div className="modal" />;
}
const StartGuide = React.lazy(() => import("./pages/start-guide"));

export type TestingDistributionProps = PropsWithChildren<{}>;

export function TestingDistribution() {
  const {
    createLinkUrl,
    changeModuleNavSelectedIndex
  } = useDistributeModuleContext();
  const services = useProfilesService({});

  useEffect(() => {
    services.updateProfiles(true);
  }, []);
  const intl = useIntl();

  return (
    <div className="TestingDistribution">
      <ModalRoute
        prefix={DistributeModulePathPrefix}
        modalRoutes={DistributeModals}
      ></ModalRoute>
      <Switch>
        <Route
          path={createLinkUrl("/detail/:id")}
          render={({ match }) => {
            return (
              <div className="modulePage">
                <ProfileDetailContextProvider profileID={match.params.id}>
                  <ProfileDetail id={match.params.id} />{" "}
                </ProfileDetailContextProvider>
              </div>
            );
          }}
        />
        <Route
          path={createLinkUrl("/groups")}
          render={match => {
            changeModuleNavSelectedIndex(1);
            return <ProfileGroups />;
          }}
        />
        <Route
          path={createLinkUrl("/start-guide")}
          render={({ match }) => {
            changeModuleNavSelectedIndex(3);
            return (
              <Suspense
                fallback={intl.formatMessage(CommonMessages.appCommonLoading)}
              >
                <StartGuide />
              </Suspense>
            );
          }}
        />
        <Route
          exact={true}
          path={createLinkUrl("(/profiles|)")}
          render={match => {
            changeModuleNavSelectedIndex(0);
            return <DistributionProfiles />;
          }}
        />
        <EmptyModule createLinkUrl={createLinkUrl} />
      </Switch>
    </div>
  );
}
