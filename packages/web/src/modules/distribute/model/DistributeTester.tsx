
export enum DistributeTesterStatus {
  Processing = 0,
  Pending = 1,
  Clicked = 2,
  ClickedNotEnrolled = 3,
  ClickedNotLoggedIn = 4,
  LoggedIn = 5,
  Downloaded = 6
}

export type DistributeTester = {
  id: any;
  email: string;
  team?: string;
  device: string;
  version: string;
  status: TesterStatusResultType;
  sendingDate: string;
  updateDate: string;
};

export type TesterStatusResultType = keyof typeof DistributeTesterStatus;

export type DistributeTesterResponseType = {
  id: string;
  appVersionId: string;
  tester: string;
  link: string;
  device: string | null;
  status: number;
  sendingDate: string;
  createDate: string;
  updateDate: string;
};

export const TesterStatusResult: TesterStatusResultType[] = [
  "Processing",
  "Pending",
  "Clicked",
  "ClickedNotEnrolled",
  "ClickedNotLoggedIn",
  "LoggedIn",
  "Downloaded"
]


// export const TESTER_STATUS = ["Sending", "Pending", "Downloaded"];
