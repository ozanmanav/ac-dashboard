import { Tag } from "@appcircle/ui/lib/TaggableTextField/Tags";

export type DistributeGroupType = {
  id: string;
  name: string;
  testers: string[];
  autoSentProfileNames: string[];
  isSelected?: boolean;
  isTemporary?: boolean;
  editMode?: boolean;
  inProgress?: boolean;
};

export type DistributeGroupResponseType = {
  createDate: string;
  id: string;
  name: string;
  organizationId: string;
  testers: string[];
  updateDate: string;
};

export type DistributeAutoSentGroupsType = {
  id: string;
  name: string;
  isActive?: boolean;
  tags: Tag[];
};
