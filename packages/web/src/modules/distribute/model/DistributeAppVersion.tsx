import {
  DistributeTester,
  DistributeTesterResponseType
} from "./DistributeTester";
import { TagComponentProps } from "@appcircle/ui/lib/TagComponent";
import { OS } from "@appcircle/core/lib/enums/OS";

export type DistributeProfileAppType = Omit<
  DistributeProfileAppResponseType,
  "tags"
> & {
  size: string; // from bytes
  isLastVersion?: boolean;
  isSubmitted?: boolean;
  uploadTime: string;
  // fileName?: string; // may be remove
  bundleName: string;
  testers: DistributeTester[];
  tags: TagComponentProps[];
  qrcode: string;
  isSelected?: boolean;
  selectedTesters?: string[];
  message: string;
  iconUrl?: string;
  isInprogress?: boolean;
  name: string;
  // isTemporary?: boolean;
};

export type DistributeProfileAppResponseType = {
  appPlayerUrl: string | null;
  appSimulatorResourceId: string;
  id: string | "";
  buildId: string;
  profileId: string;
  appResourceId: string;
  appIconResourceId: string;
  name: string;
  uniqueName: string;
  version: string;
  platformType: OS;
  fileSize: number;
  message: string | null;
  appVersionTestings: DistributeTesterResponseType[];
  createDate: string;
  updateDate: string;
  lastSharedDate: string | null;
  tags: [];
  versionTags: [];
  iconUrl: string;
};

export type DeleteAppFromProfileResponseType = {
  taskId: string;
};
