import {
  DistributeProfileAppType,
  DistributeProfileAppResponseType
} from "./DistributeAppVersion";
import { ProfileCardStatus } from "@appcircle/ui/lib/ProfileCard";
import { OS } from "@appcircle/core/lib/enums/OS";

export const ProfileAuthTypes: { [key: string]: number } = {
  None: 1,
  "Individual Enrollment": 2,
  "Static Username and Password": 3
};

export const ProfileAuthTypesbyValue = [
  "None",
  "Individual Enrollment",
  "Static Username and Password"
];

export type DistributeProfileType = DistributeProfileResponseType & {
  // lastAddedVersion: {
  //   ios: string;
  //   android: string;
  // };
  // lastTestingCount: {
  //   sent: number;
  //   clicked: number;
  // };
  lastUpdated: string;
  lastShared: string;
  autoSentGroups: string[];
  // TODO: Rename apps to resources
  apps: DistributeProfileAppType[];
  appsByID: { [key: string]: number };
  appsByIOS: { [key: string]: number };
  appsByAndroid: { [key: string]: number };
  tempAppProfilesByID: { [key: string]: number };
  totalAppCount: number;
  selectedOS: OS;
  selectedAppProfileID: { ios: string; android: string };
  pinned?: boolean;
  isTemporary?: boolean;
  state?: ProfileCardStatus;
  isLoaded?: boolean;
};

export type DistributeProfileResponseType = {
  id: string;
  organizationId: string;
  name: string;
  pinned: boolean;
  androidIconUrl: string;
  iOSIconUrl: string;
  settings?: {
    authenticationType?: 1 | 2 | 3;
    username?: string;
    password?: string;
  };
  appVersions: DistributeProfileAppResponseType[];
  createDate: string;
  updateDate: string;
  lastAppVersionDownloadedCount: number;
  lastAppVersionCreateDate: string | null;
  lastAppVersionSharedDate: string | null;
  lastAppVersionClickedCount: number;
  lastAppVersionTestingCount: number;
  iOSVersion: string;
  androidVersion: string;
  testingGroupIds?: string[];
};
