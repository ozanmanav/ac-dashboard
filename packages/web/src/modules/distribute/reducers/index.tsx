import { testerGroupReducer, DistributeGroupState } from "./groups/reducer";
import { testerProfilesReducer, TesterProfileState } from "./profiles/reducer";
import { DistributeGroupActions } from "./groups/actions";
import { DistributeProfileActions } from "./profiles/actions";
import { ModuleNavBarDataType } from "@appcircle/shared/lib/Module";

export type DistributeModuleState = {
  groups: DistributeGroupState;
  profiles: TesterProfileState;
  navbarData: ModuleNavBarDataType;
};
export const DistributeModuleReducers = {
  groups: testerGroupReducer,
  profiles: testerProfilesReducer
  // navbarData: navbarDataReducer
};

export type DistributeActions =
  | DistributeGroupActions
  | DistributeProfileActions
  | {
      type: "UPDATE_NAVBAR_DATA";
      data: ModuleNavBarDataType;
    };
