import {
  DistributeProfileAppType,
  DistributeProfileAppResponseType
} from "../../model/DistributeAppVersion";
import { DistributeProfileResponseType } from "../../model/DistributeProfile";
import { OS } from "@appcircle/core/lib/enums/OS";
import {
  TesterStatusResultType,
  DistributeTesterResponseType
} from "modules/distribute/model/DistributeTester";

export type AddNewTemporaryVerionAppItem = {
  type: "ADD_NEW_TEMPLATE_APP_ITEM";
  profileID: string;
  appItem: DistributeProfileAppType;
  os: OS;
};

export type DistributeProfileActions =
  | {
      type: "ADD_TESTER_PROFILE";
      profile: DistributeProfileResponseType;
      tempProfileID?: string;
      accessToken: string;
    }
  | {
      type: "ADD_NEW_TEMPLATE_TESTER_PROFILE";
    }
  | { type: "RENAME_TESTER_PROFILE"; id: string; newName: string }
  | { type: "PIN_TESTER_PROFILE"; id: string; status: boolean }
  | { type: "DELETE_TESTER_PROFILE"; id: string }
  | {
      type: "CHANGE_PROFILE_SELECTED_OS";
      profileID: string;
      os: OS;
    }
  | {
      type: "DELETE_APP_ITEM_FROM_PROFILE";
      profileID: string;
      os: OS;
      appID: string;
    }
  | {
      type: "CHANGE_SELECTED_APP_ITEM";
      profileID: string;
      appID: string;
    }
  | {
      type: "ADD_NEW_APP_ITEM_TO_PROFILE";
      profileID: string;
      os: OS;
      appItem: DistributeProfileAppResponseType;
      accessToken: string;
    }
  | {
      type: "UPDATE_PROFILE_VERSION_TAGS";
      profileID: string;
      appID: string;
      os: OS;
      tags: string[];
    }
  | {
      type: "UPDATE_PROFILES_DATA";
      profiles: DistributeProfileResponseType[];
      accessToken: string;
    }
  | {
      type: "UPDATE_PROFILE_DATA";
      profile: DistributeProfileResponseType;
      accessToken: string;
    }
  | {
      type: "UPDATE_APP_SELECTED_TESTERS";
      profileID: string;
      testerIDs: string[];
    }
  | {
      type: "UPDATE_APP_ITEM";
      profileID: string;
      appItem: DistributeProfileAppResponseType;
      accessToken: string;
    }
  | {
      type: "CLEAR_STATE_TESTER_PROFILE";
      id: string;
    }
  | {
      type: "SET_INPROGRESS_STATE_TESTER_PROFILE";
      id: string;
    }
  | {
      type: "SET_INPROGRESS_APP_ITEM";
      profileID: string;
      os: OS;
      appID: string;
    }
  | {
      type: "DELETE_SELECTED_APP_ITEM";
      profileID: string;
      os: OS;
    }
  | {
      type: "UPDATE_APP_TESTERS_STATUS";
      profileID: string;
      appID: string;
      status: TesterStatusResultType;
      testerIDs: string[];
    }
  | {
      type: "UPDATE_APP_TESTER";
      profileID: string;
      appID: string;
      tester: DistributeTesterResponseType;
    }
  | {
      type: "DELETE_TEMPLATE_APP_ITEM";
      profileID: string;
      appID: string;
      os?: OS;
    }
  | AddNewTemporaryVerionAppItem;

export const TESTER_PROFILE_ACTIONS = [
  "ADD_NEW_TEMPLATE_TESTER_PROFILE",
  "RENAME_TESTER_PROFILE",
  "PIN_TESTER_PROFILE",
  "DELETE_TESTER_PROFILE",
  "CHANGE_PROFILE_SELECTED_OS",
  "DELETE_APP_ITEM_FROM_PROFILE",
  "CHANGE_SELECTED_APP_ITEM",
  "ADD_NEW_APP_ITEM_TO_PROFILE",
  "UPDATE_PROFILE_VERSION_TAGS",
  "UPDATE_APP_TESTERS_LIST",
  "UPDATE_APP_TESTER"
];
