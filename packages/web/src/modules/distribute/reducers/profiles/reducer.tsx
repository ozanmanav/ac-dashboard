import { DistributeProfileActions } from "./actions";
import { createEmptyDistributeProfile } from "../../helper/createEmptyDistributeProfile";
import {
  DistributeProfileType,
  DistributeProfileResponseType
} from "../../model/DistributeProfile";
import {
  DistributeProfileAppType,
  DistributeProfileAppResponseType
} from "../../model/DistributeAppVersion";
import moment from "moment";
import {
  DistributeTester,
  DistributeTesterResponseType,
  TesterStatusResult
} from "../../model/DistributeTester";
import { ProfileCardStatus } from "@appcircle/ui/lib/ProfileCard";
import { TagComponentProps } from "@appcircle/ui/lib/TagComponent";
import produce from "immer";
import { StateMutator } from "@appcircle/core/lib/Mutator";
import { profileSortHelper } from "@appcircle/shared/lib/helpers/profileSortHelper";
import createTemporaryAppItem from "modules/distribute/pages/profile/createTemporaryAppItem";
import { OS } from "@appcircle/core/lib/enums/OS";
import { createFakeID, isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { OSValue } from "@appcircle/core/lib/enums/OSValue";
import shortid from "shortid";
import bytes from "bytes";
import { SvgApple } from "@appcircle/assets/lib/Apple";
import { SvgAndroid } from "@appcircle/assets/lib/Android";

export type TesterProfileState = {
  profilesByID: { [key: string]: number };
  profiles: DistributeProfileType[];
  profilesSorted: DistributeProfileType[];
  emptyAppProfile: DistributeProfileAppType;
  emptyProfile: DistributeProfileType;
  lastAction: DistributeProfileActions | null;
  isLoaded?: boolean;
};
// let profiles = Object.values(sampleProfilesData) as TesterProfileType[];
let testerProfileInitialState: TesterProfileState = {
  profilesByID: {},
  profiles: [],
  profilesSorted: [],
  emptyAppProfile: createTemporaryAppItem(OS.ios, createFakeID()),
  emptyProfile: {
    ...createEmptyDistributeProfile(""),
    name: "Loading..."
  },
  lastAction: null
};

export const EmptyProfileState = {
  profilesByID: {},
  profiles: [],
  profilesSorted: [],
  emptyAppProfile: { ...testerProfileInitialState.emptyAppProfile },
  emptyProfile: { ...testerProfileInitialState.emptyProfile },
  lastAction: null,
  isLoaded: false
};

export function getInitialState() {
  return testerProfileInitialState;
}

export function testerProfilesReducer(
  state: TesterProfileState = testerProfileInitialState,
  action: DistributeProfileActions
): TesterProfileState {
  if (reducers[action.type]) {
    const newState = produce(state, draft => {
      draft.lastAction = action;
      reducers[action.type](draft, action);
    });
    return newState;
  }

  return state;
}

function getDefaultTags(os: OS, size: string): TagComponentProps[] {
  return [
    {
      id: shortid.generate(),
      text: OSValue[os],
      icon: os === OS.ios ? SvgApple : SvgAndroid,
      modifiable: false
    },
    {
      id: shortid.generate(),
      text: size,
      modifiable: false
    }
  ];
}

function createProfileAppMutator(profile: DistributeProfileType) {
  return new StateMutator<DistributeProfileAppType>(profile, "apps");
}

function createProfileMutator(state: TesterProfileState) {
  return new StateMutator<DistributeProfileType>(state, "profiles");
}

function createProfileIndexesByOS(profile: DistributeProfileType) {
  profile.appsByAndroid = {};
  profile.appsByIOS = {};
  profile.apps.forEach((app, index) => {
    (app.platformType === OS.ios ? profile.appsByIOS : profile.appsByAndroid)[
      app.id
    ] = index;
  });
}

function deleteAppVersion(
  state: TesterProfileState,
  profileID: string,
  appId: string
) {
  new StateMutator<DistributeProfileType>(state, "profiles").find(
    profileID,
    (profile, self) => {
      createProfileAppMutator(profile)
        .delete(appId)
        .invalidateIndexesByKey();
      createProfileIndexesByOS(profile);
      profile.selectedAppProfileID[OSValue[profile.selectedOS]] = "";
      setDefaultSelectecdApps(profile);
    }
  );
}

function setDefaultSelectecdApps(newProfile: DistributeProfileType) {
  !newProfile.selectedAppProfileID["android"] &&
    // !newProfile.appsByID[newProfile.selectedAppProfileID["android"]] &&
    (newProfile.selectedAppProfileID["android"] = (
      newProfile.apps.find(app => app.platformType === OS.android) || {
        id: ""
      }
    ).id);
  !newProfile.selectedAppProfileID["ios"] &&
    // !newProfile.appsByID[newProfile.selectedAppProfileID["ios"]] &&
    (newProfile.selectedAppProfileID["ios"] = (
      newProfile.apps.find(app => app.platformType === OS.ios) || {
        id: ""
      }
    ).id);
}

const reducers: {
  [key: string]: (
    draft: TesterProfileState,
    action: DistributeProfileActions
  ) => void;
} = {
  ADD_NEW_TEMPLATE_TESTER_PROFILE: (draft, action) => {
    if (action.type === "ADD_NEW_TEMPLATE_TESTER_PROFILE")
      new StateMutator(draft, "profiles")
        .unshift(createEmptyDistributeProfile(createFakeID()))
        .createSorted(profileSortHelper)
        .invalidateIndexesByKey();
  },
  ADD_NEW_APP_ITEM_TO_PROFILE: (draft, action) => {
    if (action.type === "ADD_NEW_APP_ITEM_TO_PROFILE") {
      createProfileMutator(draft).find(action.profileID, (profile, mutator) => {
        const newAppItem = createProfileAppVersion(
          action.appItem,
          action.accessToken
        );

        createProfileAppMutator(profile).unshift(newAppItem);
        profile.appVersions.unshift(action.appItem);
        mutator.createSorted(profileSortHelper);
        createProfileIndexesByOS(profile);
        profile.selectedAppProfileID[OSValue[action.os]] = action.appItem.id;
      });
    }
  },
  PIN_TESTER_PROFILE: (draft: TesterProfileState, action) => {
    if (action.type === "PIN_TESTER_PROFILE") {
      createProfileMutator(draft).find(action.id, (profile, mutator) => {
        profile.pinned = action.status;
        mutator.createSorted(profileSortHelper);
      });
    }
  },
  DELETE_TESTER_PROFILE: (draft: TesterProfileState, action) => {
    if (action.type === "DELETE_TESTER_PROFILE") {
      new StateMutator(draft, "profiles")
        .delete(action.id)
        .createSorted(profileSortHelper)
        .invalidateIndexesByKey();
    }
  },
  RENAME_TESTER_PROFILE: (draft: TesterProfileState, action) => {
    action.type === "RENAME_TESTER_PROFILE" &&
      createProfileMutator(draft).find(action.id, (item, mutator) => {
        item.name = action.newName;
        mutator.createSorted(profileSortHelper);
      });
  },
  CHANGE_PROFILE_SELECTED_OS: (draft: TesterProfileState, action) => {
    action.type === "CHANGE_PROFILE_SELECTED_OS" &&
      new StateMutator<DistributeProfileType>(draft, "profiles").find(
        action.profileID,
        profile => {
          profile && (profile.selectedOS = action.os);
        }
      );
  },
  CHANGE_SELECTED_APP_ITEM: (draft: TesterProfileState, action) => {
    if (action.type === "CHANGE_SELECTED_APP_ITEM")
      new StateMutator<DistributeProfileType>(draft, "profiles").find(
        action.profileID,
        profile => {
          profile.selectedAppProfileID[OSValue[profile.selectedOS]] =
            action.appID;
        }
      );
  },
  DELETE_APP_ITEM_FROM_PROFILE: (draft: TesterProfileState, action) => {
    if (action.type === "DELETE_APP_ITEM_FROM_PROFILE") {
      deleteAppVersion(draft, action.profileID, action.appID);
    }
  },
  DELETE_TEMPLATE_APP_ITEM: (draft: TesterProfileState, action) => {
    if (action.type === "DELETE_TEMPLATE_APP_ITEM") {
      createProfileMutator(draft).find(action.profileID, (profile, self) => {
        // deleteAppVersion(draft, action.profileID, action.appID);
        // delete profile.tempAppProfilesByID[action.appID];
        const app = profile.apps[profile.appsByID[action.appID]];
        createProfileAppMutator(profile).delete(action.appID);
        if (app) profile.selectedAppProfileID[OSValue[app.platformType]] = "";
        createProfileIndexesByOS(profile);
        setDefaultSelectecdApps(profile);
      });
    }
  },
  UPDATE_PROFILE_VERSION_TAGS: (draft, action) => {
    if (action.type === "UPDATE_PROFILE_VERSION_TAGS")
      createProfileMutator(draft).find(action.profileID, profile => {
        draft.isLoaded = true;
        createProfileAppMutator(profile).find(action.appID, app => {
          app.tags = getDefaultTags(app.platformType, app.size);
          action.tags.forEach((t: string) => {
            app.tags.push({
              id: shortid.generate(),
              text: t,
              modifiable: true
            });
          });
        });
      });
  },
  UPDATE_APP_SELECTED_TESTERS: (draft, action) => {
    if (action.type === "UPDATE_APP_SELECTED_TESTERS") {
      createProfileMutator(draft).find(action.profileID, profile => {
        const app =
          profile.apps[
            profile.appsByID[
              profile.selectedAppProfileID[OSValue[profile.selectedOS]]
            ]
          ];
        app && (app.selectedTesters = action.testerIDs);
      });
    }
  },
  UPDATE_PROFILES_DATA: (draft, action) => {
    if (action.type === "UPDATE_PROFILES_DATA") {
      const profilesService = createProfileMutator(draft);
      draft.profiles = action.profiles
        .reverse()
        .map((profile: DistributeProfileResponseType) => {
          updateProfileData(draft, {
            profile,
            accessToken: action.accessToken
          });
          return createTesterProfile(
            profile,
            action.accessToken,
            profilesService.find(profile.id)
          );
        });

      profilesService.createSorted(profileSortHelper).invalidateIndexesByKey();
    }
  },
  ADD_TESTER_PROFILE: (draft, action) => {
    if (action.type === "ADD_TESTER_PROFILE") {
      if (action.tempProfileID) {
        delete draft.profilesByID[action.tempProfileID];
      }

      createProfileMutator(draft)
        .unshift(createTesterProfile(action.profile, action.accessToken))
        .createSorted(profileSortHelper)
        .invalidateIndexesByKey();
    }
  },
  UPDATE_PROFILE_DATA: (draft, action) => {
    if (action.type === "UPDATE_PROFILE_DATA") {
      if (!action.profile) {
        return;
      }

      updateProfileData(draft, action);
      // profileService.find(profile.id, (newProfile) => {

      // });
    }
  },
  UPDATE_APP_ITEM: (draft, action) => {
    if (action.type === "UPDATE_APP_ITEM") {
      createProfileMutator(draft).find(action.profileID, profile => {
        createProfileAppMutator(profile).find(action.appItem.id, app => {
          const newAppItem = createProfileAppVersion(
            action.appItem,
            action.accessToken
          );
          updateProfileAppVersion(app, newAppItem);
        });
      });
    }
  },
  UPDATE_APP_TESTERS_STATUS: (draft, action) => {
    if (action.type === "UPDATE_APP_TESTERS_STATUS") {
      createProfileMutator(draft).find(action.profileID, profile => {
        createProfileAppMutator(profile).find(action.appID, app => {
          app.testers
            .filter(tester => action.testerIDs.some(id => tester.id === id))
            .forEach(tester => {
              tester.status = action.status;
            });
        });
      });
    }
  },
  UPDATE_APP_TESTER: (draft, action) => {
    if (action.type === "UPDATE_APP_TESTER") {
      createProfileMutator(draft).find(action.profileID, profile => {
        createProfileAppMutator(profile).find(action.appID, app => {
          const index = app.testers.findIndex(
            tester => action.tester.id === tester.id
          );
          app.testers[index] = getTesters(action.tester, app.version);
        });
      });
    }
  },
  CLEAR_STATE_TESTER_PROFILE: (draft, action) => {
    if (action.type === "CLEAR_STATE_TESTER_PROFILE")
      createProfileMutator(draft).find(action.id, profile => {
        profile.state = ProfileCardStatus.READY;
      });
  },
  SET_INPROGRESS_STATE_TESTER_PROFILE: (draft, action) => {
    if (action.type === "SET_INPROGRESS_STATE_TESTER_PROFILE")
      createProfileMutator(draft).find(action.id, profile => {
        profile.state = ProfileCardStatus.INPROGRESS;
      });
  },
  ADD_NEW_TEMPLATE_APP_ITEM: (draft, action) => {
    if (action.type === "ADD_NEW_TEMPLATE_APP_ITEM")
      createProfileMutator(draft).find(action.profileID, (profile, self) => {
        !action.appItem.id && (action.appItem.id = createFakeID());
        const os = action.os;
        // profile.tempAppProfilesByID[action.appItem.id] = profile.apps.length;
        createProfileAppMutator(profile).unshift(action.appItem);
        profile.selectedAppProfileID[OSValue[os]] = action.appItem.id;
        createProfileIndexesByOS(profile);
      });
  },
  SET_INPROGRESS_APP_ITEM: (draft, action) => {
    action.type === "SET_INPROGRESS_APP_ITEM" &&
      createProfileMutator(draft).find(action.profileID, profile => {
        createProfileAppMutator(profile).find(action.appID, app => {
          app.isInprogress = true;
        });
      });
  },
  DELETE_SELECTED_APP_ITEM: (draft, action) => {
    if (action.type === "DELETE_SELECTED_APP_ITEM")
      createProfileMutator(draft).find(action.profileID, profile => {
        deleteAppVersion(
          draft,
          action.profileID,
          profile.selectedAppProfileID[OSValue[profile.selectedOS]]
        );
      });
  }
};

function updateProfileData(
  draft: TesterProfileState,
  {
    profile,
    accessToken
  }: { profile: DistributeProfileResponseType; accessToken: string }
) {
  const profileService = createProfileMutator(draft);
  const newProfile = createTesterProfile(profile, accessToken);
  profileService.find(profile.id, oldProfile => {
    oldProfile.isLoaded = true;
    // saves temp-apps in order to redisplay uploading progressions.
    const tempApps = oldProfile.apps.filter(app => isFakeID(app.id));
    newProfile.selectedOS = oldProfile.selectedOS;
    newProfile.apps = tempApps.concat(
      profile.appVersions
        .reverse()
        .map(version => createProfileAppVersion(version, accessToken))
    );
    newProfile.selectedAppProfileID = oldProfile.selectedAppProfileID;
    // if (oldProfile.apps.length) {
    // }
  });
  setDefaultSelectecdApps(newProfile);
  profileService.updateOrAdd(newProfile);
  createProfileIndexesByOS(newProfile);
  profileService.createSorted(profileSortHelper).invalidateIndexesByKey();
  const appService = createProfileAppMutator(newProfile);
  appService.invalidateIndexesByKey();
}

function createTesterProfile(
  profile: DistributeProfileResponseType,
  accessToken: string,
  oldProfile?: DistributeProfileType
): DistributeProfileType {
  return {
    ...oldProfile,
    ...profile,
    id: profile.id,
    name: profile.name,
    androidIconUrl: `${profile.androidIconUrl}?accessToken=${accessToken}`,
    iOSIconUrl: `${profile.iOSIconUrl}?accessToken=${accessToken}`,
    settings: profile.settings,
    lastUpdated: moment(profile.updateDate).fromNow(),
    lastShared: profile.lastAppVersionSharedDate
      ? moment(profile.lastAppVersionSharedDate).fromNow()
      : "",
    autoSentGroups: [],
    selectedOS: (oldProfile && oldProfile.selectedOS) || OS.ios,
    selectedAppProfileID: oldProfile
      ? oldProfile.selectedAppProfileID
      : { ios: "", android: "" },
    apps: oldProfile ? oldProfile.apps : [],
    pinned: profile.pinned,
    appsByAndroid: {},
    appsByID: {},
    appsByIOS: {},
    totalAppCount: 0,
    tempAppProfilesByID: {}
  };
}
type createAppProfilesType = {
  ios: DistributeProfileAppType[];
  android: DistributeProfileAppType[];
  allAppItems: { [key: string]: DistributeProfileAppType };
  [key: string]: any;
};

// TODO: Remove this and keep original resource
function createProfileAppVersion(
  appResp: DistributeProfileAppResponseType,
  accessToken: string
): DistributeProfileAppType {
  const createDate = moment(appResp.updateDate);
  const platformType = appResp.platformType;
  const appSize = bytes(appResp.fileSize, { unitSeparator: " " });
  const defaultTags = getDefaultTags(platformType, appSize);
  const currentTags: TagComponentProps[] = appResp.versionTags.map(tag => ({
    id: shortid.generate(),
    text: tag,
    modifiable: true
  }));
  return {
    // ...createTemporaryAppItem(OS.ios, createFakeID()),
    ...appResp,
    id: appResp.id,
    version: appResp.version,
    size: appSize,
    uploadTime: `${createDate.fromNow()} ${createDate
      .format("LT")
      .toLowerCase()}`,
    bundleName: appResp.uniqueName,
    testers: appResp.appVersionTestings
      // .reverse()
      .sort(
        (a, b) =>
          new Date(b.updateDate).getTime() - new Date(a.updateDate).getTime()
      )
      .map(testerItem => getTesters(testerItem, appResp.version)),
    tags: [...defaultTags, ...currentTags],
    qrcode: "",
    message: appResp.message || "",
    platformType,
    iconUrl: `${appResp.iconUrl}?accessToken=${accessToken}`,
    name: appResp.name
  };
}

function updateProfileAppVersion(
  curentApp: DistributeProfileAppType,
  newApp: DistributeProfileAppType
) {
  curentApp.id = newApp.id;
  curentApp.version = newApp.version;
  curentApp.size = newApp.size;
  curentApp.uploadTime = newApp.uploadTime;
  curentApp.bundleName = newApp.bundleName;
  curentApp.testers = newApp.testers;
  curentApp.tags = newApp.tags;
  curentApp.qrcode = newApp.qrcode;
  curentApp.message = newApp.message;
}

export function getTesters(
  testerItem: DistributeTesterResponseType,
  version: string
): DistributeTester {
  return {
    id: testerItem.id || testerItem.tester,
    email: testerItem.tester,
    device: testerItem.device || "Unknown",
    team: "",
    status: TesterStatusResult[testerItem.status] || "Pending",
    version,
    sendingDate: testerItem.sendingDate,
    updateDate: testerItem.updateDate
  };
}

// TODO: Remove this and replace with ProfileService
export function getAppProfileHelper(
  state: TesterProfileState,
  profileID: string,
  appID: string
): DistributeProfileAppType | null {
  const profile = createProfileMutator(state).find(profileID);
  if (!profile) return null;
  return createProfileAppMutator(profile).find(appID) || null;
}

export function checkProfilesNameExists(
  profiles: TesterProfileState,
  name: string
): boolean {
  return profiles.profiles.some(p => p.name === name && !p.isTemporary);
}
