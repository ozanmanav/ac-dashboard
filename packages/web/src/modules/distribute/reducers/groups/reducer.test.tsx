import "jest-dom/extend-expect";
import "react-testing-library/cleanup-after-each";
import { copy } from "@appcircle/core/lib/copy";
import {
  GroupsReducers,
  DistributeGroupState,
  createEmptyGroupsState
} from "./reducer";
const groupsData = require("./mock/groups.json");
describe("Groups Reducers", () => {
  let mockData: object[];
  let state: DistributeGroupState;
  beforeEach(() => {
    mockData = copy(groupsData);
    state = createEmptyGroupsState();
  });
  it("Updates groups data", () => {
    GroupsReducers.UPDATE_GROUP_DATA(state, {
      type: "UPDATE_GROUP_DATA",
      groups: groupsData,
      testers: []
    });
    expect(state.groups).toEqual(groupsData);
  });
  it("changes group selection", () => {
    GroupsReducers.UPDATE_GROUP_DATA(state, {
      type: "UPDATE_GROUP_DATA",
      groups: groupsData,
      testers: []
    });
    GroupsReducers.CHANGE_SELECTED_GROUP_ITEM(state, {
      type: "CHANGE_SELECTED_GROUP_ITEM",
      id: "040ef3e9-70d6-4225-a12e-9d227740f882"
    });
    expect(state.selectedGroupID).toEqual(
      "040ef3e9-70d6-4225-a12e-9d227740f882"
    );

    GroupsReducers.CHANGE_SELECTED_GROUP_ITEM(state, {
      type: "CHANGE_SELECTED_GROUP_ITEM",
      id: "8af9252f-d86a-4442-97bc-79e6ab27c566"
    });
    expect(state.selectedGroupID).toEqual(
      "8af9252f-d86a-4442-97bc-79e6ab27c566"
    );
  });
});
