import { DistributeGroupResponseType } from "../../model/TesterGroup";

export type DistributeGroupActions =
  | {
      type: "SET_EDIT_MODE_PROFILE_GROUP_NAME";
      id: string;
      value: boolean;
    }
  | {
      type: "ADD_PROFILE_GROUP";
      group: DistributeGroupResponseType;
      tempGroupID?: string;
    }
  | {
      type: "ADD_NEW_TEMPLATE_PROFILE_GROUP";
    }
  | {
      type: "RENAME_PROFILE_GROUP_NAME";
      id: string;
      newName: string;
    }
  | {
      type: "CHANGE_SELECTED_GROUP_ITEM";
      id: string;
    }
  | {
      type: "ADD_TESTERS_TO_GROUP";
      id: string;
      testers: string[];
    }
  | {
      type: "DELETE_TESTERS_FROM_GROUP";
      id: string;
      testers: string[];
    }
  | {
      type: "DELETE_PROFILE_GROUP";
      id: string;
    }
  | {
      type: "UPDATE_GROUP_DATA";
      groups: DistributeGroupResponseType[];
      testers: string[];
    };

export const TESTER_GROUP_ACTIONS = [
  "SET_EDIT_MODE_PROFILE_GROUP_NAME",
  "DUPLICATE_PROFILE_GROUP",
  "ADD_NEW_TEMPLATE_PROFILE_GROUP",
  "RENAME_PROFILE_GROUP_NAME",
  "CHANGE_SELECTED_GROUP_ITEM",
  "ADD_TESTERS_TO_GROUP",
  "DELETE_TESTERS_FROM_GROUP",
  "DELETE_PROFILE_GROUP"
];
