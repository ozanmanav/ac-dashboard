import {
  DistributeGroupType,
  DistributeGroupResponseType
} from "../../model/TesterGroup";
import { DistributeGroupActions } from "./actions";
import produce from "immer";
import { StateMutator } from "@appcircle/core/lib/Mutator";
import { createFakeID } from "@appcircle/core/lib/utils/fakeID";

export type DistributeGroupState = {
  groups: DistributeGroupType[];
  groupsNames: string[];
  groupsByName: { [key: string]: number };
  groupsByID: { [key: string]: number };
  selectedGroupID: string;
  testers: string[];
  oldSelectedGroupID: string;
  lastAction: DistributeGroupActions | null;
  isLoaded?: boolean;
  autoSentProfileNamesByGroupId: { [key: string]: string[] };
};

let testerGroupInitialState: DistributeGroupState = createEmptyGroupsState();

export function createEmptyGroupsState(): DistributeGroupState {
  return {
    groupsNames: [],
    groupsByID: {},
    groupsByName: {},
    testers: [],
    selectedGroupID: "",
    oldSelectedGroupID: "",
    lastAction: null,
    autoSentProfileNamesByGroupId: {},
    groups: []
  };
}
export function getInitialState() {
  return testerGroupInitialState;
}
export function testerGroupReducer(
  state: DistributeGroupState = testerGroupInitialState,
  action: DistributeGroupActions
): DistributeGroupState {
  const newState = { ...state, lastAction: action };
  testerGroupInitialState = newState;
  newState.lastAction = action;
  if (GroupsReducers[action.type]) {
    return produce(state, draft => {
      GroupsReducers[action.type](draft, action);
    });
  }
  return state;
}

export function createGroupsMutator(state: DistributeGroupState) {
  return new StateMutator<DistributeGroupType>(state, "groups", "ByID");
}

function getGroupsNames(draft: DistributeGroupState) {
  return draft.groups.map((group, i) => {
    draft.groupsByName[group.name] = i;
    return group.name;
  });
}

export const GroupsReducers: {
  [key: string]: (
    state: DistributeGroupState,
    action: DistributeGroupActions
  ) => void;
} = {
  CHANGE_SELECTED_GROUP_ITEM: (draft: DistributeGroupState, action) => {
    if (action.type === "CHANGE_SELECTED_GROUP_ITEM") {
      draft.oldSelectedGroupID = draft.selectedGroupID;
      draft.selectedGroupID = action.id;
    }
  },
  ADD_TESTERS_TO_GROUP: (draft: DistributeGroupState, action) => {
    if (action.type === "ADD_TESTERS_TO_GROUP")
      action.testers.forEach((email: string) => {
        createGroupsMutator(draft).find(action.id, group => {
          group.testers.unshift(email);
        });
      });
  },
  DELETE_TESTERS_FROM_GROUP: (draft: DistributeGroupState, action) => {
    if (action.type === "DELETE_TESTERS_FROM_GROUP")
      action.testers.forEach((email: string) => {
        createGroupsMutator(draft).find(action.id, group => {
          group.testers = group.testers.filter(e => email != e);
        });
      });
  },
  DELETE_PROFILE_GROUP: (draft: DistributeGroupState, action) => {
    if (action.type === "DELETE_PROFILE_GROUP") {
      draft.selectedGroupID = draft.groupsByID[draft.oldSelectedGroupID]
        ? draft.oldSelectedGroupID
        : draft.groups[0] && draft.groups[0].id;
      createGroupsMutator(draft).delete(action.id);
      draft.groupsByName = {};
      draft.groupsNames = draft.groups.map((group, i) => {
        draft.groupsByName[group.name] = i;
        return group.name;
      });
      draft.groupsNames = getGroupsNames(draft);
    }
  },
  ADD_NEW_TEMPLATE_PROFILE_GROUP: (draft: DistributeGroupState, action) => {
    if (action.type === "ADD_NEW_TEMPLATE_PROFILE_GROUP") {
      const newID = createFakeID();
      createGroupsMutator(draft).unshift({
        autoSentProfileNames: [],
        testers: [],
        id: newID,
        name: "",
        isTemporary: true,
        editMode: true
      });
      draft.oldSelectedGroupID = draft.selectedGroupID;
      draft.selectedGroupID = newID;
    }
  },
  SET_EDIT_MODE_PROFILE_GROUP_NAME: (draft, action) => {
    if (action.type === "SET_EDIT_MODE_PROFILE_GROUP_NAME") {
      createGroupsMutator(draft).find(action.id, group => {
        group.editMode = action.value;
      });
    }
  },
  RENAME_PROFILE_GROUP_NAME: (draft: DistributeGroupState, action) => {
    if (action.type === "RENAME_PROFILE_GROUP_NAME") {
      createGroupsMutator(draft).find(action.id, group => {
        draft.groupsByName[action.newName] = draft.groupsByName[group.name];
        group.isTemporary = false;
        group.editMode = false;
        group.name = action.newName;
      });
      draft.groupsByName = {};
      draft.groupsNames = getGroupsNames(draft);
    }
  },
  UPDATE_GROUP_DATA: (draft: DistributeGroupState, action) => {
    if (action.type === "UPDATE_GROUP_DATA") {
      draft.groupsByID = {};
      draft.isLoaded = true;
      draft.groups = [];
      action.groups.reverse();
      draft.groupsNames = [];
      draft.groupsByName = {};
      action.groups.forEach(
        (newGroup: DistributeGroupResponseType, i: number) => {
          createGroupsMutator(draft).add({
            ...newGroup,
            testers: newGroup.testers.reverse(),
            autoSentProfileNames: []
          });
          draft.groupsByName[newGroup.name] = i;
          draft.groupsNames.push(newGroup.name);
        }
      );

      draft.testers = action.testers;
      if (action.groups.length) {
        draft.selectedGroupID = draft.groupsByID[draft.selectedGroupID]
          ? draft.selectedGroupID
          : action.groups[0].id;
        draft.oldSelectedGroupID = draft.groupsByID[draft.oldSelectedGroupID]
          ? draft.oldSelectedGroupID
          : action.groups[0].id;
      }

      draft.groupsNames = getGroupsNames(draft);
    }
  },
  ADD_PROFILE_GROUP: (draft, action) => {
    if (action.type === "ADD_PROFILE_GROUP") {
      const group = createGroupsMutator(draft)
        .delete(action.tempGroupID || "")
        .updaOrUnshift({
          ...action.group,
          autoSentProfileNames: []
        });

      draft.oldSelectedGroupID = draft.selectedGroupID;
      draft.selectedGroupID = action.group.id;
      draft.groupsNames = getGroupsNames(draft);
    }
  }
};
