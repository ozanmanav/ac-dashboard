import { copy } from "@appcircle/core/lib/copy";
import shortid from "shortid";

export const testerMembers = [
  {
    id: shortid.generate(),
    email: "osman@innovationbox.com",
    team: "InnovationBox",
    device: "Iphone XSMAX",
    version: "1.0.52",
    status: "Pending"
  },
  {
    id: shortid.generate(),
    email: "burak@smartface.io",
    team: "Smartface",
    device: "Iphone X",
    version: "1.0.52",
    status: "Downloaded"
  },
  {
    id: shortid.generate(),
    email: "osman@smartface.io",
    team: "Smartface",
    device: "Iphone 8 Plus",
    version: "1.0.52",
    status: "Downloaded"
  },
  {
    id: shortid.generate(),
    email: "faruk@innovationbox.com",
    team: "InnovationBox",
    device: "Iphone 5",
    version: "1.0.52",
    status: "Pending"
  },
  {
    id: shortid.generate(),
    email: "ali@smartface.io",
    team: "InnovationBox",
    device: "Iphone XSMAX",
    version: "1.0.52",
    status: "Pending"
  },
  {
    id: shortid.generate(),
    email: "burakc@smartface.io",
    team: "Smartface",
    device: "Iphone X",
    version: "1.0.52",
    status: "Downloaded"
  },
  {
    id: shortid.generate(),
    email: "ali@innovationbox.com",
    team: "InnovationBox",
    device: "Iphone 5",
    version: "1.0.52",
    status: "Pending"
  },
  {
    id: shortid.generate(),
    email: "cenk@smartface.io",
    team: "InnovationBox",
    device: "Iphone XSMAX",
    version: "1.0.52",
    status: "Downloaded"
  },
  {
    id: shortid.generate(),
    email: "ugur@smartface.io",
    team: "Smartface",
    device: "Iphone 8 Plus",
    version: "1.0.52",
    status: "Downloaded"
  },
  {
    id: shortid.generate(),
    email: "yalcin@innovationbox.com",
    team: "InnovationBox",
    device: "Iphone 5",
    version: "1.0.52",
    status: "Pending"
  },
  {
    id: shortid.generate(),
    email: "yunus@smartface.io",
    team: "Smartface",
    device: "Iphone XSMAX",
    version: "1.0.52",
    status: "Pending"
  }
];
export const versionFileItemsData = [
  {
    id: shortid.generate(),
    version: "1.7.27-beta.04",
    size: "15.12 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator1_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.6.26-alpha.02",
    size: "12.10 MB",
    isLastVersion: true,
    isSubmitted: true,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator2_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }, { text: "Production" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.5.25-beta.03",
    size: "15.1 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.4.25-beta.02",
    size: "15.1 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.1.25-beta.02",
    size: "20.10 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.07.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demos",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }, { text: "Production" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.3.25-beta.02",
    size: "15.1 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "1.0.25-beta.5",
    size: "21 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "11.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },

  {
    id: shortid.generate(),
    version: "2.4.25-beta.02",
    size: "15.23 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "2.1.25-beta.02",
    size: "20.10 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.07.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demos",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }, { text: "Production" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "2.3.25-beta.02",
    size: "14.12 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "10.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  },
  {
    id: shortid.generate(),
    version: "2.0.25-beta.5",
    size: "22 MB",
    isLastVersion: false,
    isSubmitted: false,
    uploadTime: "11.06.2019 8:40 p.m",
    fileName: "Calculator_App_Android_apk file",
    bundleName: "com.innovationbox.demo",
    testers: testerMembers.slice(),
    tags: [{ text: "UAT" }, { text: "Test" }],
    qrcode: "https://tr.gravatar.com/205e460b479e2e5b48aec07710c08d50.qr?s=300"
  }
];

export const testerEmails = testerMembers.map(member => member.email);

const groups = [
  {
    name: "Individual Testers",
    autoSentProfileNames: ["Calculator", "Convert"]
  },
  {
    name: "Smartface Management Team",
    autoSentProfileNames: ["Calculator"]
  },
  {
    name: "InnovationBox Team",
    autoSentProfileNames: [],
    members: []
  }
];

export const smapleGroupData: any = {};
[0, 1, 2].forEach(i => {
  const newID = shortid.generate();
  smapleGroupData[newID] = {
    id: newID,
    ...groups[i],
    isSelected: false,
    testers:
      i === 0 ? testerEmails.slice() : i === 1 ? testerEmails.reverse() : []
  };
});

const smartfaceGravatar = `https://s.gravatar.com/avatar/fa00145b86180f33d7bc97326d03289c?s=80`;

const CardData = {
  name: "Calculator",
  iconURL: smartfaceGravatar,
  lastAddedVersion: {
    android: "0.4.21-alpha.1",
    ios: "1.6.26-alpha.02"
  },
  lastUpdated: "2 days ago",
  lastShared: "36 minutes ago",
  lastTestingCount: {
    sent: 63,
    clicked: 3
  },
  authentication: "LDAP",
  autoSentGroups: ["Smartface", "InnovationBox Test"],
  apps: {
    selectedOS: "ios",
    ios: {
      selectedVersionID: 2,
      archivedNum: 256,
      appProfiles: versionFileItemsData.slice()
    },
    android: {
      selectedVersionID: 1,
      archivedNum: 24,
      appProfiles: versionFileItemsData.slice().reverse()
    }
  },
  isPinned: false
};

export const sampleProfilesData: any = {};
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach(index => {
  const newID = shortid.generate();
  sampleProfilesData[newID] = {
    ...copy(CardData),
    id: newID,
    isPinned: index % 3 === 0,
    name: CardData.name + index,
    lastTestingCount: {
      sent: index % 3 === 0 ? 0 : index,
      clicked: index % 3 === 0 ? 0 : index % 5
    },
    lastAddedVersion:
      index % 3 === 0
        ? { ios: "", android: "" }
        : {
            ...CardData.lastAddedVersion,
            android: CardData.lastAddedVersion.android + index,
            ios: CardData.lastAddedVersion.ios + index
          },
    apps:
      index % 3 === 0
        ? {
            allAppProfiles: {},
            ...copy(CardData.apps),
            selectedOS: index % 3 === 0 ? "Android" : "iOS",
            ios: {
              ...copy(CardData.apps.ios),
              appProfiles: []
            },
            android: {
              ...copy(CardData.apps.android.appProfiles),
              appProfiles: []
            }
          }
        : index === 4
        ? {
            allAppProfiles: {},
            ...copy(CardData.apps),
            ios: {
              ...copy(CardData.apps.ios),
              appProfiles: [
                {
                  ...CardData.apps.ios.appProfiles[0],
                  testers: []
                },
                ...CardData.apps.ios.appProfiles.slice(1)
              ]
            },
            android: {
              ...CardData.apps.android,
              appProfiles: [
                {
                  ...copy(CardData.apps.android.appProfiles[0]),
                  testers: []
                },
                ...copy(CardData.apps.ios.appProfiles.slice(1))
              ]
            }
          }
        : copy(CardData.apps),
    lastUpdated: `${index === 1 ? "" : CardData.lastUpdated}`,
    lastShared: `${index === 1 ? "" : CardData.lastShared}`,
    autoSentGroups: index === 1 ? [] : copy(CardData.autoSentGroups)
  };
});
