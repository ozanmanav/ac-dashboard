import { ModuleNavBarDataType } from "@appcircle/shared/lib/Module";
// const testingModuleNavBarData: ModuleNavBarDataType = {
//   header: language.tdHeader,
//   items: [
//     {
//       text: language.profiles,
//       link: "/profiles"
//     },
//     {
//       text: language.testerGroups,
//       link: "/groups"
//     },
//     {
//       text: language.reports,
//       link: "/reports"
//     },
//     {
//       text: language.startGuide,
//       link: "/startGuide"
//     }
//   ],
//   selectedItemIndex: 0
// };
// export function navbarDataReducer(
//   state: ModuleNavBarDataType = testingModuleNavBarData,
//   action: {
//     type: "UPDATE_NAVBAR_DATA";
//     data: ModuleNavBarDataType;
//   }
// ): ModuleNavBarDataType {
//   if (action.type === "UPDATE_NAVBAR_DATA") {
//     return { ...action.data };
//   }
//   return state;
// }

export const CHANGE_NAVBAR_DATA_ACTIONS_TYPE = [
  "ADD_NEW_TEMPLATE_TESTER_PROFILE",
  "DELETE_TESTER_PROFILE",
  "ADD_NEW_TEMPLATE_PROFILE_GROUP",
  "DELETE_PROFILE_GROUP",
  "UPDATE_GROUP_DATA",
  "UPDATE_PROFILES_DATA"
];
