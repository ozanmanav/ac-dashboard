import React, { useEffect } from "react";
import { TestingDistribution } from "./Distribute";
import {
  DistributeModuleContextProvider,
  useDistributeModuleContext
} from "./TestingDistributionContext";
import {
  ModuleProps,
  ModuleContext,
  ModuleNavBarDataType
} from "@appcircle/shared/lib/Module";
import { SettingModalView } from "./modals/SettingModalView";
import { AddNewFileModalView } from "./modals/AddNewFileModalView";

import { createStore, combineReducers, Store } from "redux";
import {
  DistributeModuleReducers,
  DistributeActions,
  DistributeModuleState
} from "./reducers";
import { SendToTestersModalView } from "./modals/SendToTestersModalView";
import { useAppModuleContext } from "AppModuleContext";
import { IntlShape, useIntl } from "react-intl";
import messages from "./messages";
import { PreviewonDeviceModalView } from "./modals/PreviewonDeviceModalView";
import { DistributionProfileDeleteModalDialog } from "./modals/DistributionProfileDeleteModalDialog";
import { DistributionTesterGroupDeleteModalDialog } from "./modals/DistributionTestersGroupDeleteModalDialog";
import { DistributionProfileAppDeleteModalDialog } from "./modals/DistributionProfileAppDeleteModalDialog";

export type DistStoreType = Store<DistributeModuleState, DistributeActions>;
const distStore: DistStoreType = createStore(
  combineReducers(DistributeModuleReducers)
);

export enum Modals {
  Settings = "/modal/settings",
  AddNewFile = "/modal/add_new_file",
  ProfileDelete = "/modal/profile/delete",
  ProfileAppDelete = "/modal/profile/app/delete",
  TesterGroupDelete = "/modal/group/delete",
  SendToTesters = "/modal/send_to_testers",
  Preview = "/modal/preview_on_device"
}
const navbarData: (intl: IntlShape) => ModuleNavBarDataType = intl => ({
  header: intl.formatMessage(messages.distributeModuleHeader),
  items: [
    {
      text: intl.formatMessage(messages.distributeModulePagesProfiles),
      link: "/profiles",
      length: 0
    },
    {
      text: intl.formatMessage(messages.distributeModulePagesGroups),
      link: "/groups",
      length: 0
    },
    {
      text: intl.formatMessage(messages.distributeModulePagesReports),
      link: "/reports"
    },
    {
      text: intl.formatMessage(messages.distributeModulePagesStartGuide),
      link: "/start-guide"
    }
  ],
  selectedItemIndex: 0
});

export const DistributeModulePrefix = "distribute";
export const DistributeModulePathPrefix = "/" + DistributeModulePrefix;
export const DistributeModuleApiBase = "distribution/v1";
export const DistributeModals = [
  {
    path: DistributeModulePathPrefix + Modals.Settings,
    component: SettingModalView
  },
  {
    path: DistributeModulePathPrefix + Modals.Preview,
    component: PreviewonDeviceModalView
  },
  {
    path: DistributeModulePathPrefix + Modals.AddNewFile,
    component: AddNewFileModalView
  },
  {
    path: DistributeModulePathPrefix + Modals.ProfileDelete,
    component: DistributionProfileDeleteModalDialog
  },
  {
    path: DistributeModulePathPrefix + Modals.ProfileAppDelete,
    component: DistributionProfileAppDeleteModalDialog
  },
  {
    path: DistributeModulePathPrefix + Modals.TesterGroupDelete,
    component: DistributionTesterGroupDeleteModalDialog
  },
  {
    path: DistributeModulePathPrefix + Modals.SendToTesters,
    component: SendToTestersModalView
  }
];
export default DistributeModule;

export function DistributeModule(props: ModuleProps) {
  const { setContext, setStore } = useAppModuleContext<
    DistStoreType,
    ModuleContext
  >();

  const intl = useIntl();
  const context = useDistributeModuleContext();

  useEffect(() => {
    setContext(props.context);
    setStore(distStore);
    props.context.createModuleNavBar(navbarData(intl));
    return () => {
      console.log("Unmounting Testing ...");
      // unsubscribe();
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const current = props.context.getCurrentModuleNavBarData();
    if (current && current.items && current.items.length) {
      if (current.items[0] && current.items[0].length) {
        current.items[0].length =
          context.state &&
          context.state.profiles &&
          context.state.profiles.profiles
            ? context.state.profiles.profiles.length
            : 0;
      }

      if (current.items[1] && current.items[1].length) {
        current.items[1].length =
          context.state && context.state.groups && context.state.groups.groups
            ? context.state.groups.groups.length
            : 0;
      }

      props.context.createModuleNavBar(current);
    }
    // eslint-disable-next-line
  }, [props.context, context, intl]);

  return (
    <DistributeModuleContextProvider context={props.context} store={distStore}>
      <TestingDistribution />
    </DistributeModuleContextProvider>
  );
}
