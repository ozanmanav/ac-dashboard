import { createContext, PropsWithChildren, useContext, useEffect } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";
import { DistStoreType } from "./DistributeModule";
import React from "react";
import {
  ModuleContextValueType,
  createModuleContextProviderValue
} from "@appcircle/shared/lib/ModuleContext";
import { ProfileGroupContextProvider } from "./pages/groups/DistributeGroupsContext";

const DistributionContext = createContext<
  ModuleContextValueType<DistStoreType>
>({
  ...InitialModuleContext
} as any);

export function useDistributeModuleContext() {
  const value = useContext(DistributionContext);
  return value;
}

export function DistributeModuleContextProvider(
  props: PropsWithChildren<{ context: ModuleContext; store: DistStoreType }>
) {
  const value = useContextValue({ context: props.context, store: props.store });

  return (
    <DistributionContext.Provider value={value}>
      <ProfileGroupContextProvider>
        {props.children}
      </ProfileGroupContextProvider>
    </DistributionContext.Provider>
  );
}

const useContextValue = createModuleContextProviderValue();
