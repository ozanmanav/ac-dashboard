import React from "react";
import {
  DeleteModalDialog,
  DeleteModalDialogProps
} from "@appcircle/ui/lib/modal/DeleteModalDialog";
import { useIntl } from "react-intl";
import messages from "../messages";
import { DistributeModuleApiBase } from "../DistributeModule";
import { useDistributeModuleContext } from "../TestingDistributionContext";
import {
  ModalComponentPropsType,
  KeyValTupleFrom
} from "@appcircle/shared/lib/Module";

export type DistrbutionProfileDeleteModalRouteParams = KeyValTupleFrom<
  ["name", string, "profileId", string, "iconUrl", string]
>;
export function DistributionProfileDeleteModalDialog(
  props: ModalComponentPropsType<
    DeleteModalDialogProps,
    {},
    DistrbutionProfileDeleteModalRouteParams
  >
) {
  const intl = useIntl();
  const { dispatch } = useDistributeModuleContext();
  return (
    <DeleteModalDialog
      {...props}
      onSuccess={() => {
        dispatch({
          type: "DELETE_TESTER_PROFILE",
          id: props.routeProps.params.profileId
        });
        props.onModalClose();
      }}
      placeHolder={intl.formatMessage(
        messages.distributeProfileModalDeleteProfilePlaceHolder
      )}
      successMessage={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileSuccess
      )}
      errorMessage={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileError
      )}
      title={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileTitle
      )}
      ignoreTaskId={true}
      onBeforeSubmmit={() => {
        return {};
      }}
      endpoint={`${DistributeModuleApiBase}/profiles/${props.routeProps.params.profileId}`}
      name={decodeURI(props.routeProps.params.name)}
      // TODO: get iconurl from service
      iconURL={
        (props.routeProps.params.iconUrl &&
          atob(props.routeProps.params.iconUrl)) ||
        ""
      }
    ></DeleteModalDialog>
  );
}
