import React, { useMemo, ComponentProps } from "react";
import { TabView } from "@appcircle/ui/lib/TabView";
import { AutoSentSettings } from "./components/AutoSentSettings";
import { ModalWindow } from "@appcircle/ui/lib/ModalWindow";
import { useIntl } from "react-intl";
import messages from "../messages";
import { SettingsAuthTab } from "./components/SettingsAuthTab";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { DistributeAutoSentGroupsType } from "../model/TesterGroup";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { useDistributeModuleContext } from "../TestingDistributionContext";
import { useGTMService } from "shared/domain/useGTMService";

export type DistributeProfileSettingsRouteParams = [["id", string]];
export type DistributeProfileSettingsType = ModalComponentPropsType<
  {
    autoSentGroups: DistributeAutoSentGroupsType[];
    id: string;
  },
  { autoSentGroups: DistributeAutoSentGroupsType[] },
  DistributeProfileSettingsRouteParams
>;
export function SettingModalView(props: DistributeProfileSettingsType) {
  const { autoSentGroups } = props.routeProps.state;
  const intl = useIntl();
  // const service = useDistributeProfilesModel();

  useGTMService({ triggerEvent: "dist_profile_settings" });

  const { state } = useDistributeModuleContext();
  const profileIndex =
    state &&
    state.profiles &&
    state.profiles.profilesByID[props.routeProps.params.id];
  const profile =
    profileIndex != undefined && state.profiles
      ? state.profiles.profiles[profileIndex]
      : undefined;
  const tabs = useMemo<ComponentProps<typeof TabView>["tabs"]>(
    () => [
      {
        title: "Authentication",
        component: <SettingsAuthTab profile={profile} />
      },
      {
        title: intl.formatMessage(messages.appCommonAutoSent),
        component: (
          <AutoSentSettings groups={autoSentGroups} profile={profile} />
        )
      }
    ],
    [autoSentGroups, intl, profile]
  );
  return (
    <ModalWindow
      className="SettingsModalView"
      header="Profile Settings"
      onModalClose={() => {
        props.onModalClose();
      }}
    >
      <TaskProgressView
        entityID="update-distribute-groups"
        hasRelativeRoot={true}
        isOverlay={true}
      >
        <TabView selectedIndex={0} tabs={tabs} />
      </TaskProgressView>
    </ModalWindow>
  );
}
