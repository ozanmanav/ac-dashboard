import React, { useCallback } from "react";
import { ModalDialog } from "@appcircle/ui/lib/ModalDialog";
import {
  AddNewFile,
  AddNewFileState
} from "@appcircle/ui/lib/AddNewFile";

export type AddNewFileModalViewProps = {
  icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
  header: string;
  text: string;
  onStateChange?: (state: AddNewFileState) => void;
  onFileUpload?: (file: File) => void;
  [key: string]: any;
};

export function AddNewFileModalView(props: any) {
  const fileType = (props.routeProps &&
    props.routeProps.state &&
    props.routeProps.state.fileType) || ["ipa"];
  const onFileUpload = useCallback(
    (file) => {
      props.onFileUpload && props.onFileUpload(file);
      props.onModalClose && props.onModalClose();
    },
    [props]
  );
  return (
    <ModalDialog header={props.header} className="AddNewFileModalView">
      <AddNewFile
        icons={props.icons}
        onFileUpload={onFileUpload}
        onStateChange={props.onStateChange}
        types={fileType}
        text={props.text}
      />
    </ModalDialog>
  );
}
