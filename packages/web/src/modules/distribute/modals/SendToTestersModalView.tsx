import React, {
  useState,
  useCallback,
  useContext,
  useMemo,
  useEffect
} from "react";
import { ModalWindow } from "@appcircle/ui/lib/ModalWindow";
import classNames from "classnames";
import { SendToTestersModalFooter } from "./components/SendToTestersModalFooter";
import { DistributeTester } from "../model/DistributeTester";
import { DistributeProfileAppType } from "../model/DistributeAppVersion";
import { isValidEmail } from "@appcircle/core/lib/utils/isValidEmail";
import { TaggableTextField } from "@appcircle/ui/lib/TaggableTextField/TaggableTextField";
import { Tag, TagComponents } from "@appcircle/ui/lib/TaggableTextField/Tags";
import { ModalContext } from "@appcircle/shared/lib/services/ModalContext";
import {
  Form,
  FormFieldType,
  FormStatus,
  FormState
} from "@appcircle/form/lib/Form";

import shortid from "shortid";
import { DistributeProfileType } from "../model/DistributeProfile";
import { useIntl, MessageDescriptor } from "react-intl";
import messages from "../messages";
import { DistributeModuleApiBase } from "../DistributeModule";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { AutoSuggestionField } from "@appcircle/ui/lib/AutoSuggestionField";
import { FormBody } from "@appcircle/ui/lib/form/FormBody";
import { FormComponentError } from "@appcircle/form/lib/FormComponentError";
import { useProfileService } from "../pages/profile/ProfileDetailService";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useDistributeModuleContext } from "../TestingDistributionContext";
import { OSValue } from "@appcircle/core/lib/enums/OSValue";
import { OS } from "@appcircle/core/lib/enums/OS";
import { useGTMService } from "shared/domain/useGTMService";

export const SendToTestersModalView = (props: ModalComponentPropsType<any>) => {
  useGTMService({ triggerEvent: "dist_profile_share" });
  const { DEPRECATED_initialState: initialModalState } = useContext(
    ModalContext
  );

  const {
    profile,
    messageToTester
  }: {
    profile: DistributeProfileType;
    messageToTester: any;
  } = initialModalState || { profile: null, messageToTester: null };

  // TODO: Move this to Model
  const os = profile && OSValue[profile.selectedOS];
  const selectedAppProfileId = profile && profile.selectedAppProfileID[os];
  const selectedAppProfile: DistributeProfileAppType | "" =
    selectedAppProfileId &&
    profile.apps[profile.appsByID[selectedAppProfileId]];
  const selectedTesters: DistributeTester[] = (selectedAppProfile
    ? selectedAppProfile.testers
    : []
  ).filter(
    tester =>
      selectedAppProfile &&
      selectedAppProfile.selectedTesters &&
      selectedAppProfile.selectedTesters.indexOf(tester.id) > -1
  );
  const moduleContext = useDistributeModuleContext();
  const [tags, setTags] = useState<Array<Tag>>(generateTags(selectedTesters));
  // To close when url is directly opened
  // becasue modalview's inital state is set in its parent page.
  // If the user enters the url directly then application raises an error.
  useEffect(() => {
    if (!profile) {
      props.onModalClose();
    }
  }, [profile, props]);
  const [isReadyToSend, setReadyToSend] = useState(false);
  const [isSendingEnabled, setSendingEnabled] = useState(true);
  const [tagError, setTagError] = useState<MessageDescriptor>();
  const [message, setMessage] = useState(messageToTester || "");
  const intl = useIntl();
  const groups: string[] =
    (moduleContext.state.groups && moduleContext.state.groups.groupsNames) ||
    [];
  const { dispatch } = useDistributeModuleContext();
  const profileService = useProfileService((profile || {}).id);

  const fields = useMemo<FormFieldType[]>(
    () => [
      {
        required: true,
        value: message,
        name: "message"
      },
      {
        required: true,
        value: tags.map(tag => tag.text),
        name: "testers"
      }
    ],
    // eslint-disable-next-line
    [message, tags]
  );
  const onSuccess = useCallback(
    (resp: any) => {
      dispatch({
        type: "UPDATE_APP_TESTERS_STATUS",
        profileID: profile.id,
        appID: selectedAppProfileId,
        testerIDs: selectedTesters.map(tester => tester.id),
        status: "Pending"
      });
      profileService.updateAppItem(profile.id, selectedAppProfileId);
      // setSendingEnabled && setSendingEnabled(true);
      props.onModalClose && props.onModalClose();
    },
    [profile ? profile.id : "", props.onModalClose, selectedAppProfile]
  );
  const onStart = useCallback(() => {
    dispatch({
      type: "UPDATE_APP_SELECTED_TESTERS",
      profileID: profile.id,
      testerIDs: []
    });
    dispatch({
      type: "UPDATE_APP_TESTERS_STATUS",
      profileID: profile.id,
      appID: selectedAppProfileId,
      testerIDs: selectedTesters.map(tester => tester.id),
      status: "Processing"
    });
  }, [profile ? profile.id : "", selectedAppProfile, dispatch]);
  const successMessage = useCallback(
    () => intl.formatMessage(messages.distributeModalSendToTestersSuccess),
    [intl]
  );
  const errorMessage = useCallback(
    result =>
      intl.formatMessage(messages.distributeModalSendToTestersError, {
        status: result.status,
        error: result.statusText
      }),
    [intl]
  );
  const onStatusChange = useCallback((state: FormState) => {
    if (state.status.submission === FormStatus.SENDING)
      setSendingEnabled(false);
    else setSendingEnabled(true);
  }, []);

  const selectedAppProfileID = selectedAppProfile ? selectedAppProfile.id : "";
  const onFilter = useCallback(
    (tagText?: string) => {
      return tagText
        ? groups.filter(
            name =>
              name.toLocaleLowerCase().indexOf(tagText.toLocaleLowerCase()) > -1
          )
        : groups;
    },
    [groups]
  );
  const taskProps = useMemo<TaskPropsType>(
    () => ({
      onSuccess,
      successMessage,
      errorMessage,
      onStart,
      entityIDs: selectedTesters
        .map(tester => tester.id)
        .concat([selectedAppProfileId])
    }),
    [
      onSuccess,
      successMessage,
      errorMessage,
      onStart,
      selectedTesters,
      selectedAppProfileId
    ]
  );

  function validate(tagText: string) {
    return (
      initialModalState.groupsByName.indexOf(tagText) > -1 ||
      isValidEmail(tagText) ||
      groups.some(name => tagText === name)
    );
  }
  return !selectedAppProfile ? (
    <span />
  ) : (
    <ModalWindow
      header={
        // intl.formatMessage(messages.distributeModalSendToTesters) +
        !isReadyToSend ? "Enter Recipients and Message" : "Preview and Share"
      }
      className="SendToTesterModalView"
      onModalClose={props.onModalClose}
      id={props.modalID}
    >
      <Form
        formID={props.modalID || "sent-to-testers"}
        fields={fields}
        targetPath={`${DistributeModuleApiBase}/profiles/${profile.id}/app-versions/${selectedAppProfileID}?action=sendToTesting`}
        onBeforeSubmit={data => {
          const newData = {
            ...data,
            testers: data.testers.map((tester: string) =>
              tester.indexOf("@") === -1
                ? moduleContext.state.groups.groups[
                    moduleContext.state.groups.groupsByName[tester]
                  ].id
                : tester
            )
          };
          return isReadyToSend ? newData : null;
        }}
        taskProps={taskProps}
        onStatusChange={onStatusChange}
      >
        <div className="SendToTesterModalView_header">
          <div className="SendToTesterModalView_header_left">
            <img
              alt=""
              className="ProfileVersions_appInfo_icon"
              src={
                profile.selectedOS === OS.android
                  ? profile.androidIconUrl
                  : profile.iOSIconUrl
              }
            />
            <div className="ProfileVersions_appInfo_name">{profile.name}</div>
          </div>
          <div className="SendToTesterModalView_header_right">
            {selectedAppProfile.version}
          </div>
        </div>
        <FormBody>
          <div className="SendToTesterModalView_body">
            <div
              className={classNames("SendToTesterModalView_body_emails", {
                "SendToTesterModalView_body-autoHeight": isReadyToSend
              })}
            >
              <div className="SendToTesterModalView_info">
                <div className="SendToTesterModalView_info_title">
                  {intl.formatMessage(
                    isReadyToSend
                      ? messages.distributeModalSendToTestersEmailsFinalTitle
                      : messages.distributeModalSendToTestersEmailsTitle
                  )}
                </div>
                <div className="SendToTesterModalView_info_subtitle">
                  {intl.formatMessage(
                    isReadyToSend
                      ? messages.distributeModalSendToTestersEmailsFinalSubtitle
                      : messages.distributeModalSendToTestersEmailsSubtitle
                  )}
                </div>
              </div>
              <div className="SendToTesterModalView_body_tags">
                {isReadyToSend ? (
                  <FinalTags tags={tags} />
                ) : (
                  <AutoSuggestionField onFilter={onFilter}>
                    <TaggableTextField
                      autofocus={true}
                      matchRule={validate}
                      onError={errors => {
                        setTagError(errors);
                      }}
                      value={tags || []}
                      onChange={(newTag: Tag[]) => {
                        setTags([...tags, ...newTag]);
                      }}
                      deleteTag={id => {
                        let _tags = [...tags];
                        let indexToRemove = _tags.findIndex(
                          _tag => _tag.id === id
                        );
                        _tags.splice(indexToRemove, 1);
                        setTags(_tags);
                      }}
                      separator={/[,;]+/g}
                      allowDuplicates={false}
                      errors={{
                        duplicate:
                          messages.distributeModalSendToTestersEmailDuplicationError,
                        match:
                          messages.distributeModalSendToTestersEmailInvalidError
                      }}
                    />
                  </AutoSuggestionField>
                )}
              </div>
            </div>
            {tagError && <FormComponentError errorMessage={tagError} name="" />}
            <div className="SendToTesterModalView_body_message">
              <div className="SendToTesterModalView_info">
                <div className="SendToTesterModalView_info_title">
                  {intl.formatMessage(
                    isReadyToSend
                      ? messages.distributeModalSendToTestersFinalMessage
                      : messages.distributeModalSendToTestersMessage
                  )}
                </div>
                <div className="SendToTesterModalView_info_subtitle">
                  {intl.formatMessage(
                    isReadyToSend
                      ? messages.distributeModalSendToTestersMessageFinalSubtitle
                      : messages.distributeModalSendToTestersMessageSubtitle
                  )}
                </div>
              </div>
              <div className="SendToTesterModalView_body_message_textarea">
                <textarea
                  value={message}
                  onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
                    setMessage(e.target.value)
                  }
                  disabled={isReadyToSend}
                  className={classNames({
                    "SendToTesterModalView_body_message_textarea-final": isReadyToSend
                  })}
                />
              </div>
            </div>
          </div>
        </FormBody>
        <footer>
          <SendToTestersModalFooter
            formID={props.modalID || "sent-to-testers"}
            selectedAppProfileID={selectedAppProfile.id}
            profileId={profile.id}
            message={message}
            tags={tags}
            isReadyToSend={isReadyToSend}
            setReadyToSend={setReadyToSend}
            isSendingEnabled={isSendingEnabled}
          />
        </footer>
      </Form>
    </ModalWindow>
  );
};
function generateTags(testers: DistributeTester[]): Tag[] {
  return testers.map(tester => ({
    id: shortid.generate(),
    text: tester.email
  }));
}

export type FinalTagsProps = {
  tags: Tag[];
};
function FinalTags(props: FinalTagsProps) {
  return (
    <div className="SendToTesterModalView_body_emails-final">
      <TagComponents tags={props.tags} showCloseIcon={false} />
    </div>
  );
}
