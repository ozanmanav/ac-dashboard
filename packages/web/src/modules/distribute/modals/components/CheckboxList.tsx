import React, { PropsWithChildren, useState } from "react";
import classNames from "classnames";
import { TagComponent } from "@appcircle/ui/lib/TagComponent";

export type CheckboxListProps = PropsWithChildren<{
  items: Array<{
    text: string;
    isChecked: boolean;
  }>;
}>;

export function CheckboxList(props: CheckboxListProps) {
  const [items, setItems] = useState(props.items || []);
  return (
    <div className="CheckboxList">
      {items.map(
        (item: { text: string; isChecked: boolean }, index: number) => (
          <TagComponent
            className={classNames("CheckboxList_tag", {
              "CheckboxList_tag-checked": item.isChecked
            })}
            text={item.text}
            onClick={() => {
              items[index].isChecked = !item.isChecked;
              setItems(items.slice());
            }}
            key={item.text}
          />
        )
      )}
    </div>
  );
}
