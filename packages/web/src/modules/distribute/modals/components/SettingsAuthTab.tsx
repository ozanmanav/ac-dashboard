import React, { useMemo, useEffect, useCallback } from "react";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { Form } from "@appcircle/form/lib/Form";
import { FormBody } from "@appcircle/ui/lib/form/FormBody";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import {
  DistributeProfileType,
  ProfileAuthTypes,
  DistributeProfileResponseType
} from "modules/distribute/model/DistributeProfile";
import { TextInput, TextInputProps } from "@appcircle/ui/lib/form/TextInput";
import classNames from "classnames";
import { useFormContext } from "@appcircle/form/lib/useFormContext";
import { AjaxError } from "rxjs/ajax";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { DistributeModuleApiBase } from "modules/distribute/DistributeModule";
import { Result } from "@appcircle/core/lib/services/Result";
import { RadioGroup } from "@appcircle/ui/lib/form/RadioGroup";

export type DistributeProfileSettingsType = {
  profile?: DistributeProfileType;
};

const ProfileAuthTypesData = Object.keys(ProfileAuthTypes).map<{
  label: string;
  value: number;
}>(key => ({
  label: key,
  value: ProfileAuthTypes[key]
}));

export function SettingsAuthTab(props: DistributeProfileSettingsType) {
  const intl = useIntl();
  const [formState, formServices] = useFormContext(
    "distribute-auth-settings-form"
  );
  const services = useNotificationsService();
  const { dispatch } = useDistributeModuleContext();
  const { token } = useServiceContext();
  const showFields = formState?.fields.authenticationType?.value === 3;
  const settings = props.profile && props.profile.settings;

  useEffect(() => {
    formServices.updateField({
      name: "username",
      required: showFields,
      value: showFields
        ? formState?.data.username
        : (settings && settings.username) || ""
    });
    formServices.updateField({
      name: "password",
      required: showFields,
      value: showFields
        ? formState?.data.password
        : (settings && settings.password) || ""
    });
  }, [showFields, settings]);

  const form = (
    <Form<DistributeProfileResponseType>
      method="PATCH"
      formID="distribute-auth-settings-form"
      targetPath={`${DistributeModuleApiBase}/profiles/${props.profile &&
        props.profile.id}`}
      ready={!!props.profile?.settings}
      taskProps={useMemo(
        () => ({
          successMessage: () =>
            intl.formatMessage(messages.distributeProfileUpdateSuccess),
          onSuccess: resp => {
            dispatch({
              type: "UPDATE_PROFILE_DATA",
              profile: (resp as Result<DistributeProfileResponseType>).json,
              accessToken: token.access_token
            });
          },
          onError: resp => {
            const errors =
              (resp as AjaxError).response &&
              (resp as AjaxError).response.errors;
            if (errors)
              Object.keys(errors).forEach(error => {
                services.pushMessage({
                  messageType: "error",
                  message: errors[error][0],
                  life: 10000
                });
              });
            else
              services.pushMessage({
                messageType: "error",
                message: (resp as AjaxError).message
              });
          }
        }),
        [intl, dispatch, token.access_token, services]
      )}
      onBeforeSubmit={useCallback(
        data => {
          const newData = { ...data };
          if (!showFields) {
            delete newData.password;
            delete newData.username;
          }
          return {
            settings: newData,
            name: props.profile && props.profile.name,
            pinned: props.profile && props.profile.pinned
          };
        },
        [props.profile, showFields]
      )}
    >
      <FormBody>
        <FormComponent
          formID="distribute-auth-settings-form"
          formFieldLabel={intl.formatMessage(
            messages.distributeModalSettingsAuthenticationType
          )}
          tip={intl.formatMessage(
            messages.distributeModalSettingsAuthenticationTypeTip
          )}
          items={ProfileAuthTypesData}
          formProps={useMemo(() => {
            return {
              name: "authenticationType",
              required: true
            };
          }, [])}
          value={props.profile?.settings?.authenticationType}
          element={RadioGroup}
        />
        <div className="grid">
          <div
            className={classNames("grid-row2", {
              "Form_field-disabled": !showFields
            })}
          >
            <FormComponent<TextInputProps>
              formID="distribute-auth-settings-form"
              formFieldLabel={intl.formatMessage(messages.appCommonUserName)}
              formProps={useMemo(
                () => ({
                  name: "username",
                  required: false,
                  rule: [
                    {
                      type: "string-min",
                      min: 6
                    }
                  ]
                }),
                []
              )}
              value={
                props.profile && props.profile.settings
                  ? props.profile.settings.username
                  : undefined
              }
              element={TextInput}
            />
          </div>
          <div
            className={classNames("grid-row2", {
              "Form_field-disabled": !showFields
            })}
          >
            <FormComponent<TextInputProps>
              formID="distribute-auth-settings-form"
              formFieldLabel={intl.formatMessage(messages.appCommonPassword)}
              formProps={useMemo(
                () => ({
                  name: "password",
                  required: false,
                  rule: [
                    {
                      type: "string-min",
                      min: 6
                    }
                  ]
                }),
                []
              )}
              value={
                props.profile && props.profile.settings
                  ? props.profile.settings.password
                  : undefined
              }
              element={TextInput}
            />
          </div>
        </div>
      </FormBody>
      <ModalFooter formID="distribute-auth-settings-form" />
    </Form>
  );
  return props.profile ? form : null;
}
