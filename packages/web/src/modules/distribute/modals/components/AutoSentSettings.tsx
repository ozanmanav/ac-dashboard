import React, { PropsWithChildren, useMemo } from "react";
import { DistributeAutoSentGroupsType } from "../../model/TesterGroup";
import { SwitchPanelGroup } from "@appcircle/ui/lib/SwitchPanelGroup";
import { Form } from "@appcircle/form/lib/Form";
import { FormBody } from "@appcircle/ui/lib/form/FormBody";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import { DistributeModuleApiBase } from "modules/distribute/DistributeModule";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { ComponentGroupItemType } from "@appcircle/ui/lib/form/ComponentGroup";
import { matchUnorderedArrays } from "@appcircle/core/lib/utils/matchUnorderedArrays";
import {
  DistributeProfileType,
  DistributeProfileResponseType
} from "modules/distribute/model/DistributeProfile";
import messages from "modules/distribute/messages";
import { useDistributeModuleContext } from "modules/distribute/TestingDistributionContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { Result } from "@appcircle/core/src/services/Result";
import { useIntl } from "react-intl";
import { useProfilesService } from "modules/distribute/pages/profiles/ProfilesService";

export type AutoSentSettinsProps = PropsWithChildren<{
  groups: DistributeAutoSentGroupsType[];
  profile?: DistributeProfileType;
}>;

export function AutoSentSettings(props: AutoSentSettinsProps) {
  const { dispatch } = useDistributeModuleContext();
  const { token } = useServiceContext();
  const intl = useIntl();
  const services = useProfilesService({});

  const selectedGroupItemIDs = useMemo(
    () => (props.profile && props.profile.testingGroupIds) || [],
    [props.groups]
  );

  const groupItems = useMemo<ComponentGroupItemType[]>(
    () => props.groups.map(group => ({ label: group.name, value: group.id })),
    [props.groups]
  );

  return (
    <Form
      method="PATCH"
      formID="td-auto-sent-settings"
      targetPath={`${DistributeModuleApiBase}/profiles/${props.profile &&
        props.profile.id}`}
      taskProps={{
        successMessage: () =>
          intl.formatMessage(messages.distributeProfileUpdateSuccess),
        onSuccess: resp => {
          dispatch({
            type: "UPDATE_PROFILE_DATA",
            profile: (resp as Result<DistributeProfileResponseType>).json,
            accessToken: token.access_token
          });
          services.updateProfiles();
        }
      }}
    >
      <FormBody>
        <div className="AutoSentSettins">
          <FormComponent
            formFieldLabel={intl.formatMessage(
              messages.distributeModalSettingsAutoSend
            )}
            tip={intl.formatMessage(
              messages.distributeModalSettingsAutoSendTip
            )}
            formID={"td-auto-sent-settings"}
            formProps={{
              disableErrorIndicator: true,
              required: true,
              name: "testingGroupIds",
              errorClass: "Form_field-error",
              getDirty(value: string[], initialValue: string[]) {
                return !matchUnorderedArrays(
                  value.slice(),
                  initialValue.slice()
                );
              }
            }}
            value={selectedGroupItemIDs}
            items={groupItems}
            element={SwitchPanelGroup}
          />
        </div>
      </FormBody>
      <ModalFooter formID="td-auto-sent-settings" />
    </Form>
  );
}
