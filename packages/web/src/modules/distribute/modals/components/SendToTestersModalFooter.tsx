import React, { useCallback } from "react";
import { Tag } from "@appcircle/ui/lib/TaggableTextField/Tags";
import {
  SubmitButton,
  SubmitButtonState
} from "@appcircle/ui/lib/SubmitButton";
import { useIntl } from "react-intl";
import messages from "modules/distribute/messages";
import { useFormContext } from "@appcircle/form/lib/useFormContext";
import { FormStatus } from "@appcircle/form/lib/Form";
import classNames from "classnames";
import { Button } from "@appcircle/ui/lib/Button";

type SendToTestersModalFooterType = {
  tags: Tag[];
  isReadyToSend: boolean;
  setReadyToSend: React.Dispatch<React.SetStateAction<boolean>>;
  selectedAppProfileID: string;
  profileId: string;
  message: string;
  isSendingEnabled: boolean;
  formID: string;
};
export function SendToTestersModalFooter(props: SendToTestersModalFooterType) {
  const onClickPrimary = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      if (!props.isReadyToSend) {
        e.preventDefault();
        props.setReadyToSend(true);
      }
    },
    [props]
  );
  const onClickEdit = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      props.setReadyToSend(false);
    },
    [props]
  );
  const [formState] = useFormContext(props.formID);

  const intl = useIntl();

  return (
    <React.Fragment>
      {props.isReadyToSend ? (
        <Button
          className={classNames("Button", "SubmitButton", "Button-primmary", {
            "SubmitButton-disabled":
              !props.isReadyToSend ||
              (formState && formState.status.submission === FormStatus.SENDING)
          })}
          tabIndex={-1}
          onClick={onClickEdit}
        >
          <span>{intl.formatMessage(messages.appCommonEdit)}</span>
        </Button>
      ) : null}
      <SubmitButton
        formID={props.formID}
        className={"SendToTesterModalView_primaryButton"}
        state={detectButtonState(props.tags, props.isSendingEnabled)}
        onClick={onClickPrimary}
        inProgressText={intl.formatMessage(messages.appCommonSharing)}
        showFocus={true}
      >
        {props.isReadyToSend
          ? intl.formatMessage(messages.appCommonShare)
          : intl.formatMessage(messages.appCommonNext)}
      </SubmitButton>
    </React.Fragment>
  );
}

function detectButtonState(
  tags: Tag[],
  isSendingEnabled: boolean
): SubmitButtonState {
  if (tags.length > 0) {
    if (!isSendingEnabled) return SubmitButtonState.INPROGRESS;
    return SubmitButtonState.ENABLED;
  }
  return SubmitButtonState.DISABLED;
}
