import React, { PropsWithChildren } from "react";
import { useIntl } from "react-intl";
import messages from "../messages";

export type SettingModalViewHeaderProps = PropsWithChildren<{
  image?: string;
  title?: string;
  onSave?: (e?: MouseEvent) => void;
  onCancel?: (e?: MouseEvent) => void;
}>;

const gravatar = `https://s.gravatar.com/avatar/fa00145b86180f33d7bc97326d03289c?s=80`;

export function SettingModalViewHeader(props: SettingModalViewHeaderProps) {
  const intl = useIntl();
  return (
    <div className="SettingModalViewHeader">
      <div className="SettingModalViewHeader_title">
        {intl.formatMessage(messages.distributeModalSettings)}
      </div>
    </div>
  );
}
