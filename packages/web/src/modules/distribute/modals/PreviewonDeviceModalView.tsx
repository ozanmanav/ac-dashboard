import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { ModalWindow } from "@appcircle/ui/lib/ModalWindow";
import React, { useEffect, useState, useMemo, useRef } from "react";
import { useIntl, defineMessages } from "react-intl";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { SelectBox } from "@appcircle/ui/lib/form/SelectBox";
import messages from "../messages";
import { OSType } from "@appcircle/core/lib/enums/OSType";
import { DistributeModuleApiBase } from "../DistributeModule";
import { useDistributeProfileModel } from "../pages/profiles/ProfilesService";
import { isEmptyId } from "@appcircle/core/lib/utils/isEmptyId";
import { UserWarning } from "@appcircle/ui/lib/UserWarning";
import { Result } from "@appcircle/core/lib/services/Result";

const previewMessages = defineMessages({
  distributeModulePreviewModalHeader: {
    id: "app.modules.distribute.modal.previewOnDevice.header",
    defaultMessage: "Preview on Device"
  }
});

const defaultVersions = {
  ios: ["10.3", "11.4", "12.4"],
  android: ["4.4", "5.1", "6.0", "7.1", "8.1", "9.0"]
};

const defultDevices = {
  ios: {
    device: "iphone8",
    os: "12.4"
  },
  android: {
    device: "nexus7",
    os: "8.1"
  }
};

const _availableDevices: { [key in OSType]: { [key: string]: string[] } } = {
  ios: {
    iphone4s: ["8.4", "9.3"],
    iphone5s: ["8.4", "9.3", "10.3", "11.4", "12.4"],
    iphone6: ["8.4", "9.3", "10.3", "11.4", "12.4"],
    iphone6plus: ["8.4", "9.3", "10.3", "11.4", "12.4"],
    iphone6s: ["9.3", "10.3", "11.4", "12.4"],
    iphone6splus: ["9.3", "10.3", "11.4", "12.4"],
    iphone7: ["10.3", "11.4", "12.4"],
    iphone7plus: ["10.3", "11.4", "12.4"],
    iphone8: ["11.4", "12.4"],
    iphone8plus: ["11.4", "12.4"],
    iphonex: ["11.4", "12.4"],
    iphonexs: ["12.4"],
    iphonexsmax: ["12.4"],
    ipadair: ["8.4", "9.3", "10.3", "11.4", "12.4"],
    ipadair2: ["9.3", "10.3", "11.4", "12.4"]
  },
  android: {
    nexus5: ["4.4", "5.1", "6.0", "7.1", "8.1", "9.0"],
    nexus7: ["4.4", "5.1", "6.0", "7.1", "8.1", "9.0"],
    nexus9: ["4.4", "5.1", "6.0", "7.1", "8.1", "9.0"],
    hammerhead: ["6.0.1"]
  }
};

export function PreviewonDeviceModalView(props: ModalComponentPropsType<any>) {
  const intl = useIntl();
  const taskManager = useTaskManager();
  const { createApiConsumer } = useServiceContext();
  const [url, setUrl] = useState<string>("");
  const model = useDistributeProfileModel(
    props.routeProps.params.profileId || ""
  );
  const app = model && model.findAppById(props.routeProps.params.appId);
  const isAvailable = !!app && !isEmptyId(app.appSimulatorResourceId);
  const [availableDevices, setAvailableDevices] = useState(_availableDevices);
  useEffect(() => {
    if (!isAvailable) return;
    const service = createApiConsumer();
    taskManager.addTask(
      taskManager.createApiTask(
        service({
          method: "GET",
          endpoint: "distribution/static-files/appetizeAvailableDevices.json"
        }),
        {
          onSuccess: resp => {
            setAvailableDevices(resp.json);
          }
        }
      )
    );
    const task = taskManager.createApiTask<any>(
      service({
        method: "POST",
        endpoint: `${DistributeModuleApiBase}/profiles/${props.routeProps.params.profileId}/app-versions/${props.routeProps.params.appId}?action=sendToPlayer`
      }),
      {
        onSuccess: result => {
          setUrl(
            (result as Result).json.appPlayerUrl ||
              (result as Result).json.data.appPlayerUrl
          );
          const os =
            (result as Result).json.platformType === 1 ||
            ((result as Result).json.data &&
              (result as Result).json.data.platformType === 1)
              ? "ios"
              : "android";
          setOS(os);
          setDevice(defultDevices[os]["device"]);
          setOSVersion(defultDevices[os]["os"]);
        }
      }
    );

    task.entityID = props.routeProps.params.appId;
    taskManager.addTask(task);

    return () => {
      iframeRef.current &&
        iframeRef.current.contentWindow &&
        iframeRef.current.contentWindow.postMessage("endSession", "*");
    };
    // eslint-disable-next-line
  }, []);
  const [os, setOS] = useState<OSType>("ios");
  const [osVersion, setOSVersion] = useState<string>("");
  const [device, setDevice] = useState<string>("");
  useEffect(() => {
    iframeRef.current &&
      iframeRef.current.contentWindow &&
      iframeRef.current.contentWindow.postMessage("endSession", "*");
  }, [device, osVersion]);
  const devicelList = useMemo<string[]>(
    () => Object.keys(availableDevices[os]),
    [os, availableDevices]
  );
  const iframeRef = useRef<HTMLIFrameElement>(null);

  return (
    <ModalWindow
      header={intl.formatMessage(
        previewMessages.distributeModulePreviewModalHeader
      )}
      onModalClose={props.onModalClose}
      id={props.modalID}
    >
      <div className="PreviewonDeviceModalView">
        <TaskProgressView
          hasRelativeRoot={true}
          entityID={props.routeProps.params.appId as string}
          progressText={intl.formatMessage(messages.appCommonLoading)}
        >
          {task => {
            return (
              <React.Fragment>
                <div className="PreviewonDeviceModalView_header grid">
                  <div className="grid-row2">
                    <SelectBox
                      visible={isAvailable}
                      onChange={value => {
                        if (typeof value === "string") {
                          setDevice(value);
                          const versions = availableDevices[os][value];
                          setOSVersion(
                            versions.some(version => osVersion === version)
                              ? osVersion
                              : versions[versions.length - 1]
                          );
                        }
                      }}
                      value={device}
                    >
                      {devicelList.map(dev => (
                        <option
                          key={dev}
                          value={dev}
                          // selected={device === dev ? true : false}
                        >
                          {dev}
                        </option>
                      ))}
                    </SelectBox>
                  </div>
                  <div className="grid-row2">
                    <SelectBox
                      visible={isAvailable}
                      onChange={value => {
                        typeof value === "string" && setOSVersion(value);
                      }}
                      value={osVersion}
                    >
                      {(!!device
                        ? availableDevices[os][device]
                        : defaultVersions[os]
                      ).map(version => (
                        <option
                          key={device + "_" + version}
                          value={version}
                          // selected={osVersion === version ? true : false}
                        >
                          {version}
                        </option>
                      ))}
                    </SelectBox>
                  </div>
                </div>
                <div className="PreviewonDeviceModalView_frame">
                  {!!url && isAvailable ? (
                    <iframe
                      ref={iframeRef}
                      scrolling="no"
                      title=""
                      src={`${url}?centered=true&osVersion=${osVersion}&device=${device}`}
                    />
                  ) : (
                    (!app ||
                      (app && isEmptyId(app.appSimulatorResourceId))) && (
                      <UserWarning>
                        There are no simulator compatible artifacts in your
                        build. Please enable simulator-compatible (x86) builds
                        in your build settings to use the Preview on Device
                        feature.
                      </UserWarning>
                    )
                  )}
                </div>
              </React.Fragment>
            );
          }}
        </TaskProgressView>
      </div>
    </ModalWindow>
  );
}
