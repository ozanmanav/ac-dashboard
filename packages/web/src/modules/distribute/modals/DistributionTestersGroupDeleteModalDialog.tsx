import React from "react";
import {
  DeleteModalDialog,
  DeleteModalDialogProps
} from "@appcircle/ui/lib/modal/DeleteModalDialog";
import { useIntl } from "react-intl";
import messages from "../messages";
import { DistributeModuleApiBase } from "../DistributeModule";
import { useDistributeModuleContext } from "../TestingDistributionContext";
import {
  ModalComponentPropsType,
  KeyValTupleFrom
} from "@appcircle/shared/lib/Module";

export type DistributionTesterGroupDeleteModalDialogRouteParams = KeyValTupleFrom<
  ["name", string, "profileId", string]
>;
export function DistributionTesterGroupDeleteModalDialog(
  props: ModalComponentPropsType<
    DeleteModalDialogProps,
    {},
    DistributionTesterGroupDeleteModalDialogRouteParams
  >
) {
  const intl = useIntl();
  const { dispatch } = useDistributeModuleContext();
  return (
    <DeleteModalDialog
      {...props}
      onSuccess={() => {
        dispatch({
          type: "DELETE_PROFILE_GROUP",
          id: props.routeProps.params.profileId
        });
        props.onModalClose();
      }}
      placeHolder={intl.formatMessage(
        messages.distributeModalDeleteGroupPlaceHolder
      )}
      successMessage={intl.formatMessage(
        messages.distributeModalDeleteGroupSuccess,
        { name: props.routeProps.params.name }
      )}
      errorMessage={intl.formatMessage(
        messages.distributeModalDeleteGroupError,
        { name: props.routeProps.params.name }
      )}
      title={intl.formatMessage(messages.distributeModalDeleteGroupTitle)}
      endpoint={`${DistributeModuleApiBase}/groups/${props.routeProps.params.profileId}`}
      name={decodeURI(props.routeProps.params.name)}
    ></DeleteModalDialog>
  );
}
