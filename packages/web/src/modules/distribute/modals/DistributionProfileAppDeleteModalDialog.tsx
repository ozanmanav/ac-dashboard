import React from "react";
import {
  DeleteModalDialog,
  DeleteModalDialogProps
} from "@appcircle/ui/lib/modal/DeleteModalDialog";
import { useIntl } from "react-intl";
import messages from "../messages";
import { DistributeModuleApiBase } from "../DistributeModule";
import { useDistributeModuleContext } from "../TestingDistributionContext";
import { useDistributeProfileModel } from "../pages/profiles/ProfilesService";
import { OS } from "@appcircle/core/lib/enums/OS";
import {
  ModalComponentPropsType,
  KeyValTupleFrom
} from "@appcircle/shared/lib/Module";
import { useProfileService } from "../pages/profile/ProfileDetailService";

export type DistributionProfileAppDeleteModalDialogRouteParams = KeyValTupleFrom<
  ["profileId", string, "profileAppId", string]
>;

export function DistributionProfileAppDeleteModalDialog(
  props: ModalComponentPropsType<
    DeleteModalDialogProps,
    {},
    DistributionProfileAppDeleteModalDialogRouteParams
  >
) {
  const intl = useIntl();
  const model = useDistributeProfileModel(props.routeProps.params.profileId);
  const profile = model && model.getProfile();
  const app = model && model.findAppById(props.routeProps.params.profileAppId);

  const iconURL = !!profile
    ? profile.selectedOS === OS.android
      ? profile.androidIconUrl
      : profile.iOSIconUrl
    : "";
  const { dispatch } = useDistributeModuleContext();
  const { updateProfile } = useProfileService(
    props.routeProps.params.profileId
  );
  const endpoint =
    profile && app
      ? `${DistributeModuleApiBase}/profiles/${profile.id}/app-versions/${app.id}`
      : "";
  return (
    <DeleteModalDialog
      {...props}
      onSuccess={() => {
        dispatch({
          type: "DELETE_APP_ITEM_FROM_PROFILE",
          appID: app ? app.id : "",
          os: profile ? profile.selectedOS : 1,
          profileID: profile ? profile.id : ""
        });
        updateProfile();
        props.onModalClose();
      }}
      placeHolder={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileVersionPlaceholder
      )}
      successMessage={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileVersionSuccess
      )}
      errorMessage={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileVersionError
      )}
      title={intl.formatMessage(
        messages.distributeProfileModalDeleteProfileVersionTitle
      )}
      endpoint={endpoint}
      name={app ? app.name + "@" + app.version : ""}
      iconURL={iconURL}
    ></DeleteModalDialog>
  );
}
