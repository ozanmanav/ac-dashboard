import { useContext } from "react";
import { StoreSubmitContext } from "./StoreSubmitContext";
import React from "react";
import { Switch, Route } from "react-router";
import { EmptyModule } from "modules/EmptyModule";
import { StoreSubmitProfiles } from "./pages/profiles/StoreSubmitProfiles";
import { StoreSubmitProfileDetail } from "./pages/profile/StoreSubmitProfileDetail";

const StoreSubmit = () => {
  const { createLinkUrl, changeModuleNavSelectedIndex } = useContext(
    StoreSubmitContext
  );

  return (
    <div className="StoreSubmit">
      <Switch>
        <Route
          path={createLinkUrl("/detail/:id")}
          render={props => {
            return <StoreSubmitProfileDetail {...props} />;
          }}
        />

        <Route
          path={createLinkUrl("/customize")}
          render={({ match }) => {
            changeModuleNavSelectedIndex(1);
            return <EmptyModule />;
          }}
        />
        <Route
          path={createLinkUrl("/settings")}
          render={({ match }) => {
            changeModuleNavSelectedIndex(2);
            return <EmptyModule />;
          }}
        />
        <Route
          path={createLinkUrl("/reports")}
          render={({ match }) => {
            changeModuleNavSelectedIndex(3);
            return <EmptyModule />;
          }}
        />

        <Route
          path={createLinkUrl("(/profiles|)")}
          render={props => {
            changeModuleNavSelectedIndex(0);
            return <StoreSubmitProfiles {...props} />;
          }}
        />
      </Switch>
    </div>
  );
};

export default StoreSubmit;
