import { OS } from "@appcircle/core/lib/enums/OS";
import { ProfileCardStatus } from "@appcircle/ui/lib/ProfileCard";

export type StoreSubmitProfileType = {
  id: string;
  name: string;
  androidIconUrl: string;
  iOSIconUrl: string;
  lastUpdated: string;
  lastShared: string;
  autoSentGroups: string[];
  // TODO: Rename apps to resources
  appsByID: { [key: string]: number };
  appsByIOS: { [key: string]: number };
  appsByAndroid: { [key: string]: number };
  tempAppProfilesByID: { [key: string]: number };
  totalAppCount: number;
  selectedOS: OS;
  selectedAppProfileID: { ios: string; android: string };
  pinned?: boolean;
  isTemporary?: boolean;
  state?: ProfileCardStatus;
  isLoaded?: boolean;
  // appVersions: StoreSubmitProfileAppVersionResponseType[];
};

export type StoreSubmitProfileAppVersionResponseType = {
  appPlayerUrl: string | null;
  appSimulatorResourceId: string;
  id: string | "";
  buildId: string;
  profileId: string;
  appResourceId: string;
  appIconResourceId: string;
  name: string;
  uniqueName: string;
  version: string;
  platformType: OS;
  fileSize: number;
  message: string | null;
  createDate: string;
  updateDate: string;
  lastSharedDate: string | null;
  tags: [];
  versionTags: [];
  iconUrl: string;
};

export type StoreSubmitProfilesState = {
  profiles: StoreSubmitProfileType[];
  profilesByID: { [id: string]: number };
  profilesSorted: StoreSubmitProfileType[];
  selectedProfileID: string;
  previousSelectedProfileID: string;
};

export type StoreSubmitProfileDetailState = {
  branches: any[];
  branchesByID: { [id: string]: number };
};
