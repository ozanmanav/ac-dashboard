import React, { Context } from "react";
import { createContext, useContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";
import {
  ModuleContextProviderPropsType,
  createModuleContextProviderValue,
  ModuleContextValueType
} from "@appcircle/shared/lib/ModuleContext";
import { StoreSubmitStoreType } from "./store";
import { useAppModuleContext } from "AppModuleContext";

const useContextValue = createModuleContextProviderValue<
  StoreSubmitStoreType
>();

type StoreSubmitContextType = ModuleContext & {};

export const StoreSubmitContext: Context<ModuleContextValueType<
  StoreSubmitStoreType
>> = createContext({
  ...InitialModuleContext
} as any);

export function useStoreSubmitModuleContext() {
  const value = useContext(StoreSubmitContext);
  return value;
}

export function StoreSubmitContextProvider(
  props: ModuleContextProviderPropsType<ModuleContext, StoreSubmitStoreType>
) {
  const { setContext, setStore } = useAppModuleContext<
    StoreSubmitStoreType,
    StoreSubmitContextType
  >();

  setContext(props.context);
  setStore(props.store);
  const value = useContextValue({ context: props.context, store: props.store });

  return (
    <StoreSubmitContext.Provider value={value}>
      {props.children}
    </StoreSubmitContext.Provider>
  );
}
