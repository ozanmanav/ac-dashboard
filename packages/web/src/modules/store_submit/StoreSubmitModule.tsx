import React, { useEffect } from "react";
import { StoreSubmitContextProvider } from "./StoreSubmitContext";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";
import { StoreSubmitStore } from "./store";
import { useAppModuleContext } from "AppModuleContext";
import { IntlShape, useIntl } from "react-intl";
import messages from "./messages";
import StoreSubmit from "./StoreSubmit";

const StoreSubmitModuleNavBarData: (
  intl: IntlShape
) => ModuleNavBarDataType = intl => ({
  header: intl.formatMessage(messages.storeSubmitModuleHeader),
  items: [
    {
      text: intl.formatMessage(messages.storeSubmitModulePagesProfiles),
      isSelected: false,
      link: "/profiles"
    },
    {
      text: intl.formatMessage(messages.storeSubmitModulePagesCustomize),
      isSelected: false,
      link: "/customize"
    },
    {
      text: intl.formatMessage(messages.storeSubmitModulePagesSettings),
      isSelected: false,
      link: "/settings"
    },
    {
      text: intl.formatMessage(messages.storeSubmitModulePagesReport),
      isSelected: false,
      link: "/reports"
    }
  ],
  selectedItemIndex: 0
});

export const StoreSubmitModulePrefix = "store_submit";
export const StoreSubmitModulePathPrefix = "/" + StoreSubmitModulePrefix;
export const StoreSubmitModuleApiBase = "store_submit/v1";

export function StoreSubmitModule(props: ModuleProps) {
  const intl = useIntl();
  const { setContext, setStore } = useAppModuleContext<
    typeof StoreSubmitStore,
    ModuleContext
  >();
  useEffect(() => {
    setContext(props.context);
    setStore(StoreSubmitStore);
    props.context.createModuleNavBar(StoreSubmitModuleNavBarData(intl));
    return () => console.log("Unmounting Store Submit ...");
    // eslint-disable-next-line
  }, []);

  return (
    <StoreSubmitContextProvider
      context={props.context}
      store={StoreSubmitStore}
    >
      <StoreSubmit />
    </StoreSubmitContextProvider>
  );
}

export default StoreSubmitModule;
