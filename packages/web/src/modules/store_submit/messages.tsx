import { defineMessages } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";

const storeSubmit = "app.modules.store_submit.";
const storeSubmitProfile = storeSubmit + "profile.";

export default defineMessages({
  storeSubmitModuleHeader: {
    id: storeSubmit + "header",
    defaultMessage: "Store Submission"
  },
  storeSubmitModulePagesProfiles: {
    id: storeSubmit + "moduleNav.profiles",
    defaultMessage: "Submit Profiles"
  },
  storeSubmitModulePagesCustomize: {
    id: storeSubmit + "moduleNav.profiles",
    defaultMessage: "Customize"
  },
  storeSubmitModulePagesSettings: {
    id: storeSubmit + "moduleNav.settings",
    defaultMessage: "Settings"
  },
  storeSubmitModulePagesReport: {
    id: storeSubmit + "moduleNav.reports",
    defaultMessage: "Reports"
  },
  ...CommonMessages
});
