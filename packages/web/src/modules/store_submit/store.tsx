import { createStore, combineReducers } from "redux";

import {
  StoreSubmitProfilesState,
  StoreSubmitProfileDetailState
} from "./models/StoreSubmitProfile";

import {
  reducer as StoreSubmitProfilesReducer,
  StoreSubmitProfilesActions
} from "./pages/profiles/reducer";

import {
  reducer as StoreSubmitProfileDetailReducer,
  StoreSubmitProfileDetailActions
} from "./pages/profile/reducer";

export type StoreSubmitStoreState = {
  storeSubmitProfiles: StoreSubmitProfilesState;
  storeSubmitProfileDetails: StoreSubmitProfileDetailState;
};

export type StoreSubmitStoreActions =
  | StoreSubmitProfilesActions
  | StoreSubmitProfileDetailActions;

export const StoreSubmitStore = createStore<
  StoreSubmitStoreState,
  StoreSubmitStoreActions,
  {},
  {}
>(
  combineReducers({
    storeSubmitProfiles: StoreSubmitProfilesReducer,
    storeSubmitProfileDetails: StoreSubmitProfileDetailReducer
  })
);
export type StoreSubmitStoreType = typeof StoreSubmitStore;
