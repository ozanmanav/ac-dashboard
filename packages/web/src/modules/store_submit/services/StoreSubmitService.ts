import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useMemo } from "react";
import { useBuildModuleContext } from "modules/build/BuildContext";
import { Result } from "@appcircle/core/lib/services/Result";
import dummyStoreSubmitProfilesData from "./dummyStoreSubmitProfilesData";
import { useStoreSubmitModuleContext } from "../StoreSubmitContext";

const baseUrl = "store-submit/v1/";

export type updateResultType = {
  id: string;
};
export type StoreSubmitProfilesServiceType = {
  updateProfiles: () => void;
};

export function useStoreSubmitProfilesService(): StoreSubmitProfilesServiceType {
  const moduleContext = useStoreSubmitModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const { pushMessage } = useNotificationsService();
  const serviceDispatch = useMemo(() => createApiConsumer(), [
    createApiConsumer
  ]);

  const services = useMemo<StoreSubmitProfilesServiceType>(
    () => ({
      updateProfiles() {
        moduleContext.dispatch({
          type: "store_submit/profiles/update",
          // elements: (result as Result).json,
          elements: dummyStoreSubmitProfilesData
        });
        // const task = taskManager.createApiTask<updateResultType>(
        //   serviceDispatch({
        //     method: "GET",
        //     endpoint: baseUrl + `profiles`
        //   }),
        //   {
        //     onSuccess: result => {

        //     }
        //   }
        // );

        // task.entityID = "modules/store_submit/profiles/update";
        // taskManager.addTask(task, 1);
      },
      delete(id: string) {}
    }),
    [moduleContext, pushMessage, serviceDispatch, taskManager]
  );

  return services;
}
