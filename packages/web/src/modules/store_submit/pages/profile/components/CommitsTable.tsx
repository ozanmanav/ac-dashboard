import React, { useCallback, useMemo, useState, useEffect } from "react";
import {
  BasicDataGridProps,
  BasicDataGrid
} from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import { SearchField } from "@appcircle/ui/lib/SearchField";
import { CommitTableCommitRowTemplate } from "./CommitTableCommitRowTemplate";
import {
  BuildCommitType,
  BuildProfileType
} from "modules/build/models/BuildProfile";
import { useIntl } from "react-intl";
import messages from "../../../messages";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { CommitTableCommitsHeaderTemplate } from "./CommitTableCommitsHeaderTemplate";
import CommitApiActions from "modules/build/models/CommitApiActions";
import { InfoMessage } from "@appcircle/ui/lib/InfoMessage";
import { createEmptyBuildProfile } from "modules/build/helper/createEmptyBuildProfile";
import { ContextMenuItemDataType } from "@appcircle/ui/lib/menus/ContextMenuItem";
import { SvgKey } from "@appcircle/assets/lib/Key";
import { useStoreSubmitProfileDetailModel } from "../StoreSubmitProfileDetailModel";
import { useStoreSubmitProfilesService } from "modules/store_submit/services/StoreSubmitService";

export type CommitsTableMenuItems = (
  commit: BuildCommitType
) => ContextMenuItemDataType[] | ContextMenuItemDataType[];

export type CommitsTableProps = {
  header: JSX.Element;
  gridHeaderClassName?: string;
  onMenuItemSelected: (
    action: CommitApiActions | "showBuild",
    commit: BuildCommitType
  ) => void;
  fallback?: JSX.Element;
  commitsData?: BuildCommitType[];
  onRowClick?: BasicDataGridProps<BuildCommitType>["onRowClick"];
};

export type CollapsedDataType = { [key: string]: boolean | undefined };

export function CommitsTable(props: CommitsTableProps) {
  const [colapsedData, setCollapsedData] = useState<CollapsedDataType>({});
  const detailModel = useStoreSubmitProfileDetailModel();
  const [data, setData] = useState<BuildCommitType[]>(props.commitsData || []);

  const buildService = useStoreSubmitProfilesService();
  const intl = useIntl();

  const createMenuItems = useCallback<
    (commit: BuildCommitType) => ContextMenuItemDataType[]
  >(
    commit => {
      return [
        {
          icon: SvgKey,
          text: "intl.formatMessage(messages.buildProfileDetailMenuBuildNow)",
          disabled: false
        }
      ];
    },
    [intl, buildService]
  );

  useEffect(() => {
    setData(props.commitsData || []);
    setSearchText("");
  }, [props.commitsData]);
  const [searchText, setSearchText] = useState("");

  const onItemSelected = useCallback(item => {}, []);

  const onCollapseToggle = useCallback(
    (id: string) => {
      setCollapsedData({
        ...colapsedData,
        [id]: !!!colapsedData[id]
      });
    },
    [colapsedData]
  );

  const onChange = useCallback(
    text => {
      setSearchText(text);
    },
    [props.commitsData]
  );

  useEffect(() => {
    if (!searchText) {
      setData(props.commitsData || []);
    } else {
      const filteredData = (props.commitsData || []).filter(item => {
        let _text = searchText.toLowerCase();
        return (
          item.hash.includes(_text) ||
          item.message.toLowerCase().includes(_text) ||
          item.author.toLowerCase().includes(_text)
        );
      });
      setData(filteredData);
    }
  }, [searchText, props.commitsData]);

  const profile = detailModel.getSelectedProfile();

  const CommitRowTemplate = useMemo(
    () =>
      createRowTemplate(
        createMenuItems,
        (action, commit) => {
          if (action === "showBuild" && commit.builds.length > 0) {
            setSearchText(commit.hash);
            setRowType("build");
          } else props.onMenuItemSelected(action, commit);
        },
        onCollapseToggle,
        colapsedData,
        "commit",
        createEmptyBuildProfile()
      ),
    [
      onCollapseToggle,
      createMenuItems,
      props.onMenuItemSelected,
      colapsedData,
      profile
    ]
  );

  const rowSeparator = useMemo(() => RowSeparator, []);
  const [rowType, setRowType] = useState<"build" | "commit">("commit");

  return (
    <div className="ProfileDetailTable CommitTable">
      <div className="CommitTable_header">
        <div className="ProfileDetailTable_header">{props.header}</div>
        <SearchField
          text={searchText}
          placeHolder={intl.formatMessage(messages.appCommonSearch)}
          onChange={onChange}
        />
      </div>
      <TaskProgressView
        // entityID={detailModel.getSelectedBranchID()}
        hasRelativeRoot={true}
      >
        <BasicDataGrid<BuildCommitType>
          showAutoProgressOnRow={false}
          onItemsSelected={onItemSelected}
          keyField={rowType === "commit" ? "id" : "loog-index"}
          data={data}
          headerClassName={props.gridHeaderClassName || ""}
          headerTemplate={CommitTableCommitsHeaderTemplate}
          rowClassName={"ProfileTestersTableItem"}
          rowTemplate={CommitRowTemplate}
          rowSeperatorTemplate={rowSeparator}
          inProgress={false}
          withCheckbox={false}
          onRowClick={rowType === "commit" ? props.onRowClick : undefined}
        />

        {searchText === "" && rowType !== "commit" ? (
          <InfoMessage>
            {/* {props.commitsData && props.commitsData.length
              ? intl.formatMessage(messages.buildProfileNoBuild)
              : ""} */}
          </InfoMessage>
        ) : null}
        {/* {!task ||
          (task.status === "COMPLETED" && data.length === 0 && fallback)} */}
      </TaskProgressView>
    </div>
  );
}

export function RowSeparator() {
  return <div className="ProfileTestersTableItem_seperator" />;
}
export function createRowTemplate(
  contextMenuItems: CommitsTableMenuItems,
  onMenuItemSelected: CommitsTableProps["onMenuItemSelected"],
  onCollapseToggle: (id: string) => void,
  colapsedData: CollapsedDataType,
  type: "build" | "commit" = "commit",
  profile: BuildProfileType
) {
  const useMenuItems = (data: BuildCommitType) => {
    return useMemo(
      () =>
        typeof contextMenuItems === "function"
          ? contextMenuItems(data)
          : contextMenuItems,
      [data]
    );
  };

  function getCommitLink(hash: string) {
    return profile.gitProviderName === "Github"
      ? `https://github.com/${profile.repositoryName}/commit/${hash}`
      : `https://bitbucket.com/${profile.repositoryName}/commits/${hash}`;
  }

  return (props: { data: BuildCommitType }) => {
    const menuItems = useMenuItems(props.data);
    return (
      <CommitTableCommitRowTemplate
        contextMenuItems={menuItems}
        onCollapseToggle={onCollapseToggle}
        colapsedData={colapsedData}
        data={props.data}
        link={getCommitLink(props.data.hash)}
      />
    );
  };
}
