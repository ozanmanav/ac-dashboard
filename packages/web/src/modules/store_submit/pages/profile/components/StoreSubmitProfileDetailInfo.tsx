import React from "react";
import { ProfileDetailInfo } from "shared/profile-detail-layout/ProfileDetailInfo";
import { StoreSubmitProfileType } from "modules/store_submit/models/StoreSubmitProfile";

export type StoreSubmitProfileDetailInfoProps = {
  profile: StoreSubmitProfileType;
};
export function StoreSubmitProfileDetailInfo(
  props: StoreSubmitProfileDetailInfoProps
) {
  return (
    <ProfileDetailInfo>
      <div className="StoreSubmitProfileDetailInfo">
        <div className="StoreSubmitProfileDetailInfo_left">
          <div className="ProfileVersions_appInfo">
            <img
              alt=""
              className="ProfileVersions_appInfo_icon"
              src={props.profile.iOSIconUrl}
            />
            <div className="ProfileVersions_appInfo_info">
              <div className="ProfileVersions_appInfo_name">
                {props.profile.name}
              </div>
              <div className="ProfileVersions_appInfo_info-package">
                {props.profile.name}
              </div>
            </div>
          </div>
        </div>
        <div className="StoreSubmitProfileDetailInfo_right">
          <div className="StoreSubmitProfileDetailInfo_right-child">
            <span className="StoreSubmitProfileDetailInfo_right-child--label">
              Last Binary Received:{" "}
            </span>
            <span className="StoreSubmitProfileDetailInfo_right-child--value">
              06/20/2019 16:15 PM
            </span>
          </div>
          <div className="StoreSubmitProfileDetailInfo_right-child">
            <span className="StoreSubmitProfileDetailInfo_right-child--label">
              Last Submission:{" "}
            </span>
            <span className="StoreSubmitProfileDetailInfo_right-child--value">
              06/20/2019 16:15 PM
            </span>
          </div>
        </div>
      </div>
    </ProfileDetailInfo>
  );
}
