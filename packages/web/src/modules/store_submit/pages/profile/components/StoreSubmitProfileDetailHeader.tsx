import React, { useMemo, useCallback } from "react";
import { ProfileDetailHeader } from "shared/profile-detail-layout/ProfileDetailHeader";
import { SwitchButtonList } from "@appcircle/ui/lib/SwitchButtonList";
import { SvgAndroidSmall } from "@appcircle/assets/lib/AndroidSmall";
import { SvgApple } from "@appcircle/assets/lib/Apple";
import { OS } from "@appcircle/core/lib/enums/OS";
import { SearchField } from "@appcircle/ui/lib/SearchField";

export type StoreSubmitProfileDetailHeaderProps = {
  branchName: string;
  date: string;
  repoName: string;
  type: any;
};

export function StoreSubmitProfileDetailHeader(
  props: StoreSubmitProfileDetailHeaderProps
) {
  const switchButtonItems = useMemo(() => [SvgApple, SvgAndroidSmall], []);

  // const LeftItem = useMemo(
  //   () => (
  //     <React.Fragment>
  //       <SearchField
  //         text={""}
  //         placeHolder={"Search"}
  //         onChange={() => console.log("ok")}
  //       />
  //     </React.Fragment>
  //   ),
  //   [props.branchName, props.date, props.repoName]
  // );

  const RightItem = useMemo(
    () => (
      <React.Fragment>
        <SwitchButtonList
          items={switchButtonItems}
          selectedIndex={1 === OS.ios ? 0 : 1}
          // onChange={onSwitchButtonChanged}
        />
      </React.Fragment>
    ),
    [props.branchName, props.date, props.repoName]
  );
  return (
    <ProfileDetailHeader
      className="StoreSubmitProfileDetailHeader"
      // leftItem={LeftItem}
      rightItem={RightItem}
    />
  );
}
