import React from "react";

import { useIntl } from "react-intl";
import messages from "../../../messages";

export function CommitTableCommitsHeaderTemplate() {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="CommitTable_gridHeader CommitTable_colSize3 CommitTable_gridHeader_hash">
        HASH
      </div>
      <div className="CommitTable_gridHeader CommitTable_colSize8 CommitTable_gridHeader_message">
        MESSAGES
      </div>
      <div className="CommitTable_gridHeader  CommitTable_colSize3 CommitTable_gridHeader_author">
        AUTHOR
      </div>
      <div className="CommitTable_gridHeader CommitTable_colSize5 CommitTable_gridHeader_builds">
        "build"
      </div>
      <div className="CommitTable_gridHeader CommitTable_colSize2">"colm"</div>
    </React.Fragment>
  );
}
