import React, { useEffect, useCallback, useRef } from "react";
import { BuildProfileDetailHeader } from "modules/build/components/BuildProfileDetailHeader";
import { BuildProfileDetailInfo } from "modules/build/components/BuildProfileDetailInfo";

import { RouteComponentProps } from "react-router";

import { StoreSubmitProfileType } from "modules/store_submit/models/StoreSubmitProfile";
import { StoreSubmitProfileDetailHeader } from "./StoreSubmitProfileDetailHeader";
import { StoreSubmitProfileDetailInfo } from "./StoreSubmitProfileDetailInfo";
import { CommitsTable } from "./CommitsTable";

export const StoreSubmitProfileDetailView = (props: {
  // versions: BuildCommitType[];
  profile: StoreSubmitProfileType | undefined;
  history: RouteComponentProps["history"];
}) => {
  return (
    <React.Fragment>"5432z21"
      <StoreSubmitProfileDetailHeader
        repoName={(props.profile && props.profile.name) || ""}
        branchName=""
        date=""
        type=""
      />
      {props.profile && (
        <React.Fragment>
          <StoreSubmitProfileDetailInfo profile={props.profile} />
          <CommitsTable
            // commitsData={versionsData}
            header={<span>Version History</span>}
            // contextMenuItems={createMenuItems}
            onMenuItemSelected={() => null}
            onRowClick={() => null}
          />
          {/* <BuildProfileDetailInfo
            profile={props.profile}
            tags={props.tags}
            os={
              props.profile
                ? props.profile.os === 1
                  ? "ios"
                  : "android"
                : "ios"
            }
          />
          <CommitsTable
            commitsData={props.commits}
            header={<span>Commits and Builds</span>}
            // contextMenuItems={createMenuItems}
            onMenuItemSelected={props.onMenuItemSelected}
            onRowClick={onRowClick}
          /> */}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
