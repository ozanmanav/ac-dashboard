import React, { useMemo } from "react";
import { TagComponent } from "@appcircle/ui/lib/TagComponent";
import classNames from "classnames";
// import { CommitBuildStatus } from "../models/BuildProfile";
import { useIntl } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import moment from "moment";
import { SvgPlay } from "@appcircle/assets/lib/Play";
import { useGTMService } from "shared/domain/useGTMService";

export function CommitTableCommitRowTemplate(props: any) {
  // const hasCollapsable = props.data.builds.length > 2;
  const intl = useIntl();
  const buildsData = useMemo(
    () =>
      props.data.builds.length > 0 ? props.data.builds.slice().reverse() : [],
    [props.data.builds]
  );
  // const dates = useMemo(
  //   () => buildsData.map(build => moment(build.endDate).calendar()),
  //   [buildsData]
  // );
  // const isBuildsListOpened = props.colapsedData[props.data.id] !== undefined ? !!props.colapsedData[props.data.id] : 'none';

  const { triggerGTMEvent } = useGTMService();

  const builds = useMemo(
    () =>
      buildsData.length > 0 ? (
        <div key={buildsData[0].id}>
          <div
          // className={classNames(
          //   "CommitTable_col_buildStatus_child CommitTable_col_buildStatus_child_hasBuild--true",
          //   {
          //     "CommitTable_col_buildStatus-success":
          //       buildsData[0].status === CommitBuildStatus.Success,
          //     "CommitTable_col_buildStatus-failed":
          //       buildsData[0].status === CommitBuildStatus.Failed,
          //     "CommitTable_col_buildStatus-canceled":
          //       buildsData[0].status === CommitBuildStatus.Canceled,
          //     "CommitTable_col_buildStatus-timeout":
          //       buildsData[0].status === CommitBuildStatus.Timeout,
          //     "CommitTable_col_buildStatus-building":
          //       buildsData[0].status === CommitBuildStatus.Building
          //   }
          // )}
          >
            {/* {CommitBuildStatus[buildsData[0].status]} */}
          </div>
          <div className="CommitTable_col_date">"123"</div>
        </div>
      ) : (
        <div className="CommitTable_col_buildStatus_child CommitTable_buildStatus-none">
          {intl.formatMessage(CommonMessages.appCommonNone)}
        </div>
      ),
    [buildsData, intl]
  );
  return (
    <>
      {" "}
      <div className="CommitTable_col CommitTable_colSize3 CommitTable_col_hash">
        <a href={props.link} target="_blank" rel="noopener noreferrer">
          {props.data.hash}
        </a>
      </div>
      <div className="CommitTable_col CommitTable_colSize8 CommitTable_col_message">
        <span>{props.data.message}</span>
        <div className="CommitTable_tags">"tags"</div>
      </div>
      <div className="CommitTable_col CommitTable_colSize3 CommitTable_col_author">
        <div className="circle_text">
          {useMemo(() => {
            if (props.data.author) {
              const authorArray = props.data.author.split(" ") || [];

              if (authorArray.length === 1) {
                // Bitbucket only coming e-mail.
                return authorArray[0][0] + authorArray[0][1];
              }

              // Single Name Author
              if (authorArray.length === 3) {
                return authorArray[0][0] + authorArray[1][0];
              }

              // Double Name Author
              if (authorArray.length === 4) {
                return authorArray[0][0] + authorArray[2][0];
              }
            }

            return "";
          }, [props.data.author])}
        </div>
      </div>
      <div
        className="CommitTable_col  CommitTable_colSize5 CommitTable_buildRow"
        onClick={() =>
          props.onMenuItemSelected && props.onMenuItemSelected(1, props.data)
        }
      >
        {builds}
      </div>
      <div className="CommitTable_col CommitTable_col_toolbox CommitTable_colSize2">
        {
          <SvgPlay
            className="playButton"
            onClick={() => {
              triggerGTMEvent("build_profile_build");
              props.onMenuItemSelected(0, props.data);
            }}
          />
        }
      </div>{" "}
    </>
  );
}
