import produce, { immerable } from "immer";
import { StoreSubmitProfileDetailState } from "modules/store_submit/models/StoreSubmitProfile";

const initialState: StoreSubmitProfileDetailState = {
  branches: [],
  branchesByID: {}
};

export type StoreSubmitProfileDetailActions = {
  type: "store_submit/profile/branches/update";
  profileID: string;
  branches: any[];
};

export function reducer(
  state: StoreSubmitProfileDetailState = initialState,
  action: StoreSubmitProfileDetailActions
) {
  return produce(state, draft => {
    switch (action.type) {
      case "store_submit/profile/branches/update":
        // draft.branches = action.branches;
        // action.branches.length > 0 &&
        //   (draft.selectedBranchID =
        //     draft.selectedBranchID || action.branches[0].id);
        // createBranchesMutator(draft).invalidateIndexesByKey();
        break;

      default:
        break;
    }
  });
}
