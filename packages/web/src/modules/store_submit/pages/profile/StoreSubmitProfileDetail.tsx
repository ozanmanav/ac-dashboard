import React, { useMemo } from "react";
import { ProfileDetailLayout } from "shared/profile-detail-layout/ProfileDetailLayout";
import { RouteComponentProps } from "react-router-dom";
import { ProgressView } from "@appcircle/ui/lib/ProgressView";
import { useStoreSubmitProfilesModel } from "../profiles/StoreSubmitProfilesModel";
import { StoreSubmitProfileDetailView } from "./components/StoreSubmitProfileDetailView";

export function StoreSubmitProfileDetail(
  props: RouteComponentProps<{ id: string }>
) {
  const profilesModel = useStoreSubmitProfilesModel();
  // const { emptyProfile, emptyAppProfile } = profilesModel.toObject();
  const profile = useMemo(() => profilesModel.findById(props.match.params.id), [
    profilesModel,
    props.match.params.id
  ]);

  return (
    <ProfileDetailLayout
      className="StoreSubmitProfileDetailSidebar"
      progressText={""}
      isSideBarExpanded={false}
      leftHandlerClick={() => {}}
      sideBar={null}
      details={
        <div className="StoreSubmitProfileDetail">
          <ProgressView
            isOverlay={true}
            isInProgress={false}
            progressText={"message"}
          >
            <StoreSubmitProfileDetailView
              profile={profile}
              history={props.history}
            />
          </ProgressView>
        </div>
      }
    />
  );
}
