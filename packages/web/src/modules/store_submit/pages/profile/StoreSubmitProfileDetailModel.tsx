import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import { StoreSubmitStoreState } from "modules/store_submit/store";
import { useStoreSubmitModuleContext } from "modules/store_submit/StoreSubmitContext";

export type StoreSubmitProfileDetailResult = { id: string };
export class StoreSubmitProfileDetailModel
  extends Model<StoreSubmitStoreState, StoreSubmitProfileDetailResult>
  implements ISearchable, ModelStateBase {
  protected init(): void {}
  length(): number {
    return 1;
  }
  isLoaded(): boolean {
    return true;
  }
  search(text: string) {
    return (this.state.storeSubmitProfiles.profiles || []).filter(
      d => d.name.toLowerCase().indexOf(text.toLowerCase()) > -1
    );
  }
  findById(id: string) {
    return undefined;
  }
  getAll() {
    return [];
  }
  getSelectedProfileID(): string {
    return this.state.storeSubmitProfiles.selectedProfileID;
  }
  getSelectedProfile() {
    return this.getSelectedProfileID()
      ? this.getProfileByID(this.getSelectedProfileID())
      : undefined;
  }
  getProfileByID(id: string) {
    return this.state.storeSubmitProfiles.profilesByID[id] !== undefined
      ? this.state.storeSubmitProfiles.profiles[
          this.state.storeSubmitProfiles.profilesByID[id]
        ]
      : undefined;
  }
}

export function useStoreSubmitProfileDetailModel(): StoreSubmitProfileDetailModel {
  const { state } = useStoreSubmitModuleContext();
  const model = useMemo(() => new StoreSubmitProfileDetailModel(state), [
    state
  ]);

  return model;
}
