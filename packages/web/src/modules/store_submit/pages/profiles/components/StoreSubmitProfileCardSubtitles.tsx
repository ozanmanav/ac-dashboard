import React, { useMemo } from "react";
import classNames from "classnames";
import { useIntl } from "react-intl";
import { SvgGithubBadge } from "@appcircle/assets/lib/GithubBadge";
import { SvgBitbucketBadge } from "@appcircle/assets/lib/BitbucketBadge";
import { SvgCode } from "@appcircle/assets/lib/Code";
import { OSIcon } from "@appcircle/ui/lib/OSIcon";
import { OS } from "@appcircle/core/lib/enums/OS";

export type StoreSubmitProfileCardSubtitlesProps = {
  item: any;
};
export function StoreSubmitProfileCardSubtitles(
  props: StoreSubmitProfileCardSubtitlesProps
) {
  console.log(props);
  const intl = useIntl();

  const subtitles = useMemo(
    () => [
      {
        type: "ios",
        text: props.item.iOSVersion
      },
      {
        text: props.item.androidVersion,
        type: "android"
      }
    ],
    [intl]
  );

  return (
    <React.Fragment>
      <div className="ProfileCard_subTitles">
        {subtitles.map((item, index: number) => {
          const iconUrl =
            item.type === "android"
              ? props.item.androidIconUrl
              : props.item.iOSIconUrl;

          return (
            <div
              key={index}
              className={classNames("ProfileCard_subTitles_item", {
                "ProfileCard_subTitles_item-empty": !item.text
              })}
            >
              {iconUrl && iconUrl.indexOf("defaultApp.png") === -1 ? (
                <img
                  width={35}
                  height={35}
                  className="icon-selected"
                  src={iconUrl}
                />
              ) : (
                <OSIcon
                  style={!!item.text ? "dark" : "light"}
                  os={item.type === "android" ? OS.android : OS.ios}
                ></OSIcon>
              )}
              <div>
                {item.text ? "v " + item.text : "No versions available"}
              </div>
            </div>
          );
        })}
      </div>
      <div className="ProfileCard_upseperator" />
    </React.Fragment>
  );
}
