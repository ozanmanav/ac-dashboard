import React, { useCallback, useState, useMemo } from "react";
import { ProfileGridViewProfileCardProps } from "@appcircle/ui/lib/ProfileGridView";
import {
  ProfileCard,
  ProfileCardServiceType,
  ProfileCardDataType
} from "@appcircle/ui/lib/ProfileCard";
import { StoreSubmitProfileCardSubtitles } from "./StoreSubmitProfileCardSubtitles";
import { StoreSubmitProfileType } from "modules/store_submit/models/StoreSubmitProfile";

export type TimeInfoType = {
  header: JSX.Element | string;
  text: string;
};

export type SubtitlesType = {
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  text: string;
};

export function StoreSubmitProfileCard(
  props: ProfileGridViewProfileCardProps<StoreSubmitProfileType>
) {
  const storeSubmitProfileData = useMemo<ProfileCardDataType>(
    () => ({
      id: props.data.id,
      isPinned: false,
      isTemporary: false,
      name: props.data.name
    }),
    [props.data.id, props.data.name]
  );

  const storeSubmitProfileService = useMemo<ProfileCardServiceType>(
    () => ({
      delete: () => null,
      rename: () => null,
      pin: () => null,
      doesNameExist: () => false
    }),
    []
  );

  const footerData = useMemo(
    () => [
      {
        header: "Last Binary Received",
        text: "a few seconds ago",
        enable: false
      },
      {
        header: "Last Submission",
        text: "a few seconds ago",
        enable: false
      }
    ],
    [props.data]
  );

  const contextMenuItems = useMemo(() => [], []);

  return (
    <div className="StoreSubmitProfileCard">
      <ProfileCard
        onEditModeChange={() => null}
        onStatusChange={() => null}
        onContextMenuItemSelected={() => null}
        editMode={false}
        service={storeSubmitProfileService}
        onBlur={() => null}
        data={storeSubmitProfileData}
        profileDetailLink={props.data.isTemporary ? "" : props.detailUrl}
        footerData={footerData}
        index={props.index}
        contextMenuItems={contextMenuItems}
        onRenameError={() => null}
      >
        <StoreSubmitProfileCardSubtitles item={props.data} />
      </ProfileCard>
    </div>
  );
}
