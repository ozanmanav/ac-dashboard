import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import {
  StoreSubmitProfilesState,
  StoreSubmitProfileType
} from "../../models/StoreSubmitProfile";
import { useStoreSubmitModuleContext } from "modules/store_submit/StoreSubmitContext";

export type StoreSubmitProfilesResult = { id: string };
export class StoreSubmitProfilesModel
  extends Model<StoreSubmitProfilesState, StoreSubmitProfilesResult>
  implements ISearchable, ModelStateBase {
  protected init(): void {}

  length(): number {
    return this.state.profiles.length;
  }

  isLoaded(): boolean {
    return true;
  }

  search(text: string) {
    return text
      ? this.state.profilesSorted.filter(
          d => d.name.toLowerCase().indexOf(text.toLowerCase()) > -1
        )
      : this.getAll();
  }

  findById(id: string): StoreSubmitProfileType | undefined {
    let index = this.state.profilesByID[id];
    return this.state.profiles[index];
  }

  doesNameExist = (name: string) => {
    return this.state.profiles.some(profile => profile.name === name);
  };

  selectedProfile() {
    const selectedProfile = this.state.profiles[
      this.state.profilesByID[this.state.selectedProfileID]
    ];

    return selectedProfile;
  }

  getAll() {
    return this.state.profilesSorted;
  }
}

export function useStoreSubmitProfilesModel(): StoreSubmitProfilesModel {
  const { state } = useStoreSubmitModuleContext();
  const model = useMemo(
    () => new StoreSubmitProfilesModel(state.storeSubmitProfiles),
    [state.storeSubmitProfiles]
  );

  return model;
}
