import React, { useEffect } from "react";
import { RouteComponentProps } from "react-router";
import { TaskProgressView } from "@appcircle/ui/lib/TaskProgressView";
import { useStoreSubmitProfilesService } from "modules/store_submit/services/StoreSubmitService";
import { SearchLayout } from "@appcircle/ui/lib/SearchLayout";
import { SvgEmptyProfiles } from "@appcircle/assets/lib/EmptyProfiles";
import { useStoreSubmitModuleContext } from "modules/store_submit/StoreSubmitContext";
import { ProfileGridView } from "@appcircle/ui/lib/ProfileGridView";
import { StoreSubmitProfileType } from "modules/store_submit/models/StoreSubmitProfile";
import { useStoreSubmitProfilesModel } from "./StoreSubmitProfilesModel";
import { StoreSubmitProfileCard } from "./components/StoreSubmitProfileCard";

export function StoreSubmitProfiles(props: RouteComponentProps) {
  const services = useStoreSubmitProfilesService();
  const { createLinkUrl } = useStoreSubmitModuleContext();
  const model = useStoreSubmitProfilesModel();

  useEffect(
    () => services.updateProfiles(),
    // eslint-disable-next-line
    []
  );

  return (
    <TaskProgressView>
      <SearchLayout<any, StoreSubmitProfileType>
        header={"header"}
        model={model}
        isLoaded={true}
        onAddElement={() => null}
        noElementsText={"no elements"}
        noElementsIcon={SvgEmptyProfiles}
      >
        {profilesData => {
          return profilesData.length ? (
            <ProfileGridView<StoreSubmitProfileType>
              profileCardComponent={StoreSubmitProfileCard}
              profilesData={profilesData}
              link={createLinkUrl("/detail/")}
            />
          ) : (   
            <></>
          );
        }}
      </SearchLayout>
    </TaskProgressView>
  );
}
