import produce from "immer";
import {
  StoreSubmitProfileType,
  StoreSubmitProfilesState
} from "modules/store_submit/models/StoreSubmitProfile";
import { StateMutator } from "@appcircle/core/lib/Mutator";
import { profileSortHelper } from "@appcircle/shared/lib/helpers/profileSortHelper";

const initialState = {
  profiles: [],
  profilesByID: {},
  profilesSorted: [],
  selectedProfileID: "",
  previousSelectedProfileID: ""
};

export type StoreSubmitProfilesActions = {
  type: "store_submit/profiles/update";
  elements: StoreSubmitProfileType[];
};

function createStoreSubmitProfilesMutator(parent: StoreSubmitProfilesState) {
  return new StateMutator<StoreSubmitProfileType>(parent, "profiles");
}

export function reducer(
  state: StoreSubmitProfilesState = initialState,
  action: StoreSubmitProfilesActions
) {
  return produce(state, draft => {
    console.log(action);
    switch (action.type) {
      case "store_submit/profiles/update":
        draft.profiles = action.elements.reverse();
        // draft.profiles.forEach(profile => profile.branches.reverse());
        action.elements.length > 0 &&
          (draft.selectedProfileID =
            draft.selectedProfileID || action.elements[0].id);
        createStoreSubmitProfilesMutator(draft)
          .invalidateIndexesByKey()
          .createSorted(profileSortHelper);
        break;
      default:
        break;
    }
  });
}
