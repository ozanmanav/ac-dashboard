import { useContext } from "react";
import React from "react";
import { Route, Redirect } from "react-router";
import { EmptyModule } from "../EmptyModule";
import { AccountContext } from "./AccountContext";
import { AuthServiceContext } from "../auth/service/AuthService";
import { ModalRoute } from "@appcircle/shared/lib/helpers/modal/ModalRoute";
import { LogoutModalDialogVIew } from "./LogoutModalDialogVIew";

const AccountModals = [{ path: "/account/modal/logout", component: LogoutModalDialogVIew }];

export function Account(props: any) {
  const { createLinkUrl } = useContext(AccountContext);
  const { service } = useContext(AuthServiceContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl}>
      <Route
        path={"/account/logut"}
        render={match => {
          service.logout();
          return (
            <div key={"logout_fake"}>
              <div>{"\n\n\nLog out..."}</div>
              <Redirect to="" />
            </div>
          );
        }}
      />
      <ModalRoute prefix="/account" modalRoutes={AccountModals}></ModalRoute>

    </EmptyModule>
  );
}
