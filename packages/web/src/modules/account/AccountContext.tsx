import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type AccountContextType = ModuleContext & {};

export const AccountContext = createContext<AccountContextType>({
  ...InitialModuleContext
});
