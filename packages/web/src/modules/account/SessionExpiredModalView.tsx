import React from "react";
import { ConfirmationModalDialog } from "@appcircle/ui/lib/modal/ConfirmationModalDialog";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { Link } from "react-router-dom";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";

export function SessionExpiredModalView(props: ModalComponentPropsType<{}>) {
  const intl = useIntl();
  return (
    <ConfirmationModalDialog
      modalID="logout-modal"
      onModalClose={props.onModalClose}
      routeProps={props.routeProps}
      title={intl.formatMessage(messages.appCommonSessionExpiredLong)}
      okButton={
        <Link
          to={{ pathname: "/account/logut" }}
          className="Button Button-danger"
        >
          {intl.formatMessage(messages.appSubmitButtonLogin)}
        </Link>
      }
    />
  );
}
