import React, { useEffect } from "react";
import {
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";
import { AccountContext } from "./AccountContext";
import { Account } from "./Account";

const buildModuleNavBarData: ModuleNavBarDataType = {
  header: "My Account",
  items: [
    {
      text: "Profile",
      link: "/profile"
    },
    {
      text: "Billing",
      link: "/billing"
    },
    {
      text: "Team Operations",
      link: "/team_operations"
    },
    {
      text: "Log Out",
      link: "?modal=/account/modal/logout"
    }
  ],
  selectedItemIndex: 0
};


export function AccountModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted AccountModule.");
    props.context.createModuleNavBar(buildModuleNavBarData);
    return () => console.log("Unmounting AccountModule ...");
  }, []);
  return (
    <AccountContext.Provider value={{ ...props.context }}>
      <Account navbarData={buildModuleNavBarData} />
    </AccountContext.Provider>
  );
}
