import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type SettingsContextType = ModuleContext & {};

export const SettingsContext = createContext<SettingsContextType>({
  ...InitialModuleContext
});
