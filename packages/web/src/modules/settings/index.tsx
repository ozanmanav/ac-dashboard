import React, { useEffect } from "react";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";
import { SettingsContext } from "./SettingsContext";
import { Settings } from "./Settings";

const buildModuleNavBarData: ModuleNavBarDataType = {
  header: "Settings",
  items: [
    {
      text: "Branding",
      link: "/branding"
    },
    {
      text: "Authentication",
      link: "/authentication"
    },
    {
      text: "Version Control",
      link: "/version_control"
    },
    {
      text: "Notifications",
      link: "/notifications"
    },
    {
      text: "Webhook",
      link: "/webhook"
    }
  ],
  selectedItemIndex: 0
};

export function SettingsModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted SettingsModule.");
    props.context.createModuleNavBar(buildModuleNavBarData);
    return () => console.log("Unmounting SettingsModule ...");
  }, []);
  return (
    <SettingsContext.Provider value={{ ...props.context }}>
      <Settings navbarData={buildModuleNavBarData} />
    </SettingsContext.Provider>
  );
}
