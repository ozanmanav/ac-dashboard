import { useContext } from "react";
import React from "react";
import { EmptyModule } from "../EmptyModule";
import { SettingsContext } from "./SettingsContext";

export function Settings(props: any) {
  const { createLinkUrl } = useContext(SettingsContext);
  return (
    <div className="Settings">
      <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
    </div>
  );
}
