import { useContext } from "react";
import { SingleLinkContext } from "./SingleLinkContext";
import React from "react";
import { EmptyModule } from "../EmptyModule";

export function SingleLink(props: any) {
  const { createLinkUrl } = useContext(SingleLinkContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
