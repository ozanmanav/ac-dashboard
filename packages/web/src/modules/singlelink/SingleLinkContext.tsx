import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type SingleLinkContextType = ModuleContext & {};

export const SingleLinkContext = createContext<SingleLinkContextType>({
  ...InitialModuleContext
});
