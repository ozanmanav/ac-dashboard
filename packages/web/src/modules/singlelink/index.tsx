import React, { useEffect } from "react";
import { SingleLink } from "./SingleLink";
import { SingleLinkContext } from "./SingleLinkContext";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";

const singleLinkModuleNavBarData: ModuleNavBarDataType = {
  header: "Single Link Distribution",
  items: [
    {
      text: "Profiles",
      link: "/profiles"
    },
    {
      text: "Groups",
      link: "/groups"
    },
    {
      text: "Reports",
      link: "/reports"
    },
    {
      text: "Start Guide",
      link: "/startGuide"
    }
  ],
  selectedItemIndex: 0
};

export function SingleLinkModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted SingleLink.");
    props.context.createModuleNavBar(singleLinkModuleNavBarData);
    return () => console.log("Unmounting SingleLink ...");
  }, []);
  return (
    <SingleLinkContext.Provider value={props.context}>
      <SingleLink navbarData={singleLinkModuleNavBarData} />
    </SingleLinkContext.Provider>
  );
}
