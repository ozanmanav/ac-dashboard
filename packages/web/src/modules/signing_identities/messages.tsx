import { defineMessages } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";

const module = "app.modules.sid.";

export default defineMessages({
  SIDModuleHeader: {
    id: module + "header",
    defaultMessage: "Signing Identities"
  },
  SIDPageCertificates: {
    id: module + "pages.ios.certificates",
    defaultMessage: "iOS Certificates"
  },
  SIDPageCertificatesSingular: {
    id: module + "pages.ios.certificates.singular",
    defaultMessage: "iOS Certificates"
  },
  SIDPageCSRCertificates: {
    id: module + "pages.ios.csr",
    defaultMessage: "iOS Certificate Signing Requests"
  },
  SIDPageCSRCertificatesSingular: {
    id: module + "pages.ios.csr.singular",
    defaultMessage: "iOS Certificate Signing Request"
  },
  SIDPageCSRCertificatesShort: {
    id: module + "pages.ios.csr",
    defaultMessage: "iOS CSRs"
  },
  SIDPageCSRCertificatesShortSingular: {
    id: module + "pages.ios.csr.singular",
    defaultMessage: "iOS CSR"
  },
  SIDPageIOSProvProfiles: {
    id: module + "pages.ios.provProfiles",
    defaultMessage: "iOS Provisioning Profiles"
  },
  SIDPageIOSProvProfilesSingular: {
    id: module + "pages.ios.provProfiles.singular",
    defaultMessage: "iOS Provisioning Profile"
  },
  SIDPageAndroidKeystores: {
    id: module + "pages.android.provProfiles",
    defaultMessage: "Android Keystores"
  },
  SIDPageStartGuide: {
    id: module + "pages.startGuide",
    defaultMessage: "Start Guide"
  },
  SIDAndroidKeystoreSearchHeader: {
    id: module + "android.keystore.search.header",
    defaultMessage: "Android Keystore"
  },
  SIDAndroidKeystoreSearchNoKeystore: {
    id: module + "android.keystore.search.header",
    defaultMessage: "No keystores available"
  },
  SIDAndroidKeystoreUpdateSuccess: {
    id: module + "android.keystore.updateSuccess",
    defaultMessage: "Keystores updated"
  },
  SIDAndroidKeystoreDeletionSuccess: {
    id: module + "android.keystore.deletionSuccess",
    defaultMessage: "Keystore deleted"
  },
  SIDAndroidKeystoreDeletionError: {
    id: module + "android.keystore.deletionError",
    defaultMessage: "Keystore cannot be deleted"
  },
  SIDCertsCSRUpdateSuccess: {
    id: module + "certificates.csr.updateSuccess",
    defaultMessage: "CSR files updated"
  },
  SIDCertsCSRDeletionSuccess: {
    id: module + "certificates.csr.deletionSuccess",
    defaultMessage: "CSR file deleted."
  },
  SIDCertsCSRDeletionError: {
    id: module + "certificates.csr.deletionError",
    defaultMessage: "CSR file cannot be deleted"
  },
  SIDCertsCSRDownloadSuccess: {
    id: module + "certificates.csr.downloadSuccess",
    defaultMessage: "CSR file downloaded"
  },
  SIDCertsCSRDownloadError: {
    id: module + "certificates.csr.downloadError",
    defaultMessage: "CSR file cannot be downloaded"
  },
  SIDCertsNoCertificates: {
    id: module + "certificates.noCertificates",
    defaultMessage: "No certificates available"
  },
  SIDCertsNoProfiles: {
    id: module + "certificates.noProfiles",
    defaultMessage: "No provisioning profiles available"
  },
  SIDCertsUpdateSuccess: {
    id: module + "certificates.updateSuccess",
    defaultMessage: "iOS certificates updated"
  },
  SIDCertsUpdateError: {
    id: module + "certificates.updateError",
    defaultMessage: "iOS Certificates cannot be updated"
  },
  SIDCertsDeletionSuccess: {
    id: module + "certificates.deletetionSuccess",
    defaultMessage: "iOS certificate deleted"
  },
  SIDCertsDeletionError: {
    id: module + "certificates.deletetionError",
    defaultMessage: "iOS certificate cannot be deleted"
  },
  SIDCertsDownloadSuccess: {
    id: module + "certificates.downloadSuccess",
    defaultMessage: "iOS certificate downloaded."
  },
  SIDCertsDownloadError: {
    id: module + "certificates.downloadError",
    defaultMessage: "iOS certificate cannot be downloaded"
  },
  SIDCertsAddElement: {
    id: module + "certificates.downloadError",
    defaultMessage: "iOS certificate cannot be downloaded"
  },
  SIDPPRUpdateSuccess: {
    id: module + "ppr.updateSuccess",
    defaultMessage: "Provisioning profiles updated"
  },
  SIDPPRUpdateError: {
    id: module + "ppr.updateSuccess",
    defaultMessage: "Provisioning profiles cannot be updated"
  },
  SIDPPRDeleteSuccess: {
    id: module + "ppr.deletionSuccess",
    defaultMessage: "Provisioning profile deleted"
  },
  SIDPPRDeleteError: {
    id: module + "ppr.deletionError",
    defaultMessage: "Provisioning profile cannot be deleted"
  },
  SIDCertificatesTableHeader_Name: {
    id: module + "certificates.table.name",
    defaultMessage: "Certificate Name"
  },
  SIDCertificatesTableHeader_Type: {
    id: module + "certificates.table.type",
    defaultMessage: "Type"
  },
  SIDCertificatesTableHeader_Extension: {
    id: module + "certificates.table.extension",
    defaultMessage: "Extension"
  },
  SIDCertificatesTableHeader_Expires: {
    id: module + "certificates.table.expires",
    defaultMessage: "Expires"
  },
  SIDKeystoreTableHeader_Name: {
    id: module + "keystore.table.name",
    defaultMessage: "Keystore Name"
  },
  SIDKeystoreTableHeader_Aliases: {
    id: module + "keystore.table.aliases",
    defaultMessage: "Alias(es)"
  },
  SIDKeystoreTableHeader_Expires: {
    id: module + "keystore.table.expires",
    defaultMessage: "Expires"
  },
  SIDModalKeyStoreUploadWizardHeader: {
    id: module + "modal.keystoreUploadWizard.header",
    defaultMessage: "Select Upload Type"
  },
  SIDModalKeyStoreUploadWizardOption0: {
    id: module + "modal.keystoreUploadWizard.option0",
    defaultMessage: "Generate Keystore"
  },
  SIDModalKeyStoreUploadWizardOption1: {
    id: module + "modal.keystoreUploadWizard.option1",
    defaultMessage: "Upload Keystore File (.jks or .keystore)"
  },
  SIDModalCertificateUploadWizardHeader: {
    id: module + "modal.certificateUploadWizard.header",
    defaultMessage: "Select Upload Type"
  },
  SIDModalCertificateUploadWizardOption0: {
    id: module + "modal.certificateUploadWizard.option0",
    defaultMessage: "Generate signing request to create certificates"
  },
  SIDModalCertificateUploadWizardOption1: {
    id: module + "modal.certificateUploadWizard.option1",
    defaultMessage: "Upload certificate bundle (.p12)"
  },
  SIDModalGenerateCSRDesc: {
    id: module + "modal.generateCSR.desc",
    defaultMessage:
      "iOS certificates require a public/private key pair. You can generate a CSR for use in Apple Developer Center or upload your own key bundle in p12 format."
  },
  SIDModalGenerateCSRNAME: {
    id: module + "modal.generateCSR.name",
    defaultMessage: "NAME"
  },
  SIDModalGenerateCSREMAIL: {
    id: module + "modal.generateCSR.email",
    defaultMessage: "EMAIL ADDRESS"
  },
  SIDModalGenerateCSRCountries: {
    id: module + "modal.generateCSR.countries",
    defaultMessage: "COUNTRY"
  },
  SIDModalGenerateCSRSelectCountry: {
    id: module + "modal.selectCountry",
    defaultMessage: "Select a Country"
  },
  SIDModalGenerateCSRSuccess: {
    id: module + "modal.generateCSR.success",
    defaultMessage: "CSR file generated"
  },
  SIDModalGenerateCSRError: {
    id: module + "modal.generateCSR.error",
    defaultMessage: "CSR file cannot be generated"
  },
  SIDModalUploadCertDesc: {
    id: module + "modal.uploadCert.desc",
    defaultMessage:
      "Upload a previously generated key bundle. You can export your certificates from the Keychain Access app on macOS."
  },
  SIDModalUploadCertSuccess: {
    id: module + "modal.uploadCert.success",
    defaultMessage: "iOS certificate uploaded"
  },
  SIDModalUploadCertError: {
    id: module + "modal.uploadCert.error",
    defaultMessage: "iOS certificate cannot be uploaded"
  },
  SIDModalUploadCertPassword: {
    id: module + "modal.uploadCert.password",
    defaultMessage: "Password"
  },
  SIDModalUploadCertPasswordDesc: {
    id: module + "modal.uploadCert.password.desc",
    defaultMessage:
      "The password is required for checking certificate-provisioning profile match and for signing the binary during the build."
  },
  SIDModalGenerateKeystoreSuccess: {
    id: module + "modal.generateKeystore.success",
    defaultMessage: "Keystore generated"
  },
  SIDModalGenerateKeystoreError: {
    id: module + "modal.generateKeystore.error",
    defaultMessage: "Keystore cannot be generated"
  },
  SIDModalGenerateKeystorePassword: {
    id: module + "modal.generateKeystore.password",
    defaultMessage: "KEYSTORE PASSWORD"
  },
  SIDModalGenerateKeystoreConfirmPassword: {
    id: module + "modal.generateKeystore.confirmPassword",
    defaultMessage: "CONFIRM PASSWORD"
  },
  SIDModalGenerateKeystoreAlias: {
    id: module + "modal.generateKeystore.alias",
    defaultMessage: "ALIAS"
  },
  SIDModalGenerateKeystoreAliasPassword: {
    id: module + "modal.generateKeystore.aliasPassword",
    defaultMessage: "ALIAS PASSWORD"
  },
  SIDModalGenerateKeystoreAliasConfirmPassword: {
    id: module + "modal.generateKeystore.aliasConfirmPassword",
    defaultMessage: "CONFIRM PASSWORD"
  },
  SIDModalGenerateKeystoreValidity: {
    id: module + "modal.generateKeystore.validity",
    defaultMessage: "VALIDITY (YEARS)"
  },
  SIDModalGenerateKeystoreFullname: {
    id: module + "modal.generateKeystore.fullName",
    defaultMessage: "FIRST & LAST NAME"
  },
  SIDModalUploadKeystoreSuccess: {
    id: module + "modal.uploadKeystore.success",
    defaultMessage: "Keystore uploaded"
  },
  SIDModalUploadKeystoreError: {
    id: module + "modal.uploadKeystore.error",
    defaultMessage: "Keystore cannot be uploaded"
  },
  SIDModalUploadKeystoreDesc: {
    id: module + "modal.uploadKeystore.desc",
    defaultMessage:
      "You can create a keystore from scratch of if you already have a keystore, you can upload it."
  },
  SIDModalUploadKeystorePassword: {
    id: module + "modal.uploadKeystore.password",
    defaultMessage: "KEYSTORE PASSWORD"
  },
  SIDModalUploadKeystorePasswordDesc: {
    id: module + "modal.uploadKeystore.passwordTip",
    defaultMessage:
      "The passwords are required to use the keystore for signing. (Please note that the alias password may be empty.)"
  },
  SIDModalUploadKeystoreAliasPassword: {
    id: module + "modal.uploadKeystore.aliasPassword",
    defaultMessage: "ALIAS PASSWORD"
  },
  SIDPPRTableHeader_Name: {
    id: module + "ppr.table.header.name",
    defaultMessage: "Provisioning Profile Name"
  },
  SIDPPRTableHeader_AppID: {
    id: module + "ppr.table.header.appid",
    defaultMessage: "Associated App ID"
  },
  SIDPPRTableHeader_Certificate: {
    id: module + "ppr.table.header.certificate",
    defaultMessage: "Certificate"
  },
  SIDPPRTableHeader_Expires: {
    id: module + "ppr.table.header.expires",
    defaultMessage: "Expires"
  },
  ...CommonMessages
});
