import React, { useCallback, useMemo } from "react";
import classNames from "classnames";
import { BasicDataGridWithProgress } from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import moment from "moment";
import { SIDKeystoreType } from "../pages/android-keystore/reducer";
import { useIntl } from "react-intl";
import messages from "../messages";
import { SvgTrash } from "@appcircle/assets/lib/Trash";

export type SIDKeystoreDataTableProps = {
  header?: JSX.Element;
  gridData: SIDKeystoreType[];
  gridHeaders?: string[];
  gridHeaderClassName?: string;
  onItemDeleted: (data: SIDProvisioningProfilesDataType) => void;
  inProgress?: boolean;
  fallback?: JSX.Element;
};

export function SIDKeystoreDataTable(props: SIDKeystoreDataTableProps) {
  let RowTemplate = useMemo(() => getRowTemplate(props.onItemDeleted), [
    props.onItemDeleted
  ]);
  return (
    <div className="SIDKeystoreDataTable SIDDataTable">
      <BasicDataGridWithProgress
        key={"grid"}
        data={props.gridData}
        headerClassName={props.gridHeaderClassName || ""}
        headerTemplate={HeaderTemplate}
        rowTemplate={RowTemplate}
        rowSeperatorTemplate={RowSeparatorTemplate}
        inProgress={props.inProgress}
        withCheckbox={false}
      />

      {props.fallback && props.gridData.length === 0 && props.fallback}
    </div>
  );
}
type SIDProvisioningProfilesDataType = SIDKeystoreType;

function HeaderTemplate() {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="SIDKeystoreDataTable_name">
        <div>{intl.formatMessage(messages.SIDKeystoreTableHeader_Name)}</div>
      </div>
      <div className={classNames("SIDKeystoreDataTable_aliases")}>
        <div>{intl.formatMessage(messages.SIDKeystoreTableHeader_Aliases)}</div>
      </div>
      <div className="SIDKeystoreDataTable_expired">
        <div>{intl.formatMessage(messages.SIDKeystoreTableHeader_Expires)}</div>
      </div>
      <div className="SIDKeystoreDataTable_delete">
        <div />
      </div>
    </React.Fragment>
  );
}

export function RowSeparatorTemplate() {
  return <div className="ProfileTestersTableItem_seperator" />;
}

export function getRowTemplate(onItemDeleted: (data: any) => void) {
  return (props: { data: SIDKeystoreType }) => {
    const onClick = useCallback(() => onItemDeleted(props.data), [props.data]);
    const date = useMemo(() => moment(props.data.expireDate), [
      props.data.expireDate
    ]);
    const today = moment();
    const isExpired = today.diff(date) > 0;

    return (
      <React.Fragment>
        <div className="SIDKeystoreDataTable_name SIDKeystoreDataTable_row_name">
          <div
            className={classNames({
              // SIDDataTable_disabled: props.data.disabled,
              SIDDataTable_warning: isExpired
            })}
          >
            {props.data.name}
          </div>
        </div>
        <div className={classNames("SIDKeystoreDataTable_aliases")}>
          <div>{props.data.alias}</div>
        </div>
        <div className="SIDKeystoreDataTable_expired">
          <div>{date.calendar()}</div>
        </div>
        <div
          className="SIDKeystoreDataTable_delete dataTableToolsRow"
          onClick={onClick}
        >
          <SvgTrash />
        </div>
      </React.Fragment>
    );
  };
}
