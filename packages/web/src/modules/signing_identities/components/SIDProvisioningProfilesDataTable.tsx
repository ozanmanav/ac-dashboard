import React, { useCallback, useMemo } from "react";
import classNames from "classnames";
import { BasicDataGridWithProgress } from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import { SIDPPRType } from "../pages/ios-provisioning-profile/reducer";
import moment from "moment";
import { useIntl } from "react-intl";
import messages from "../messages";
import { useSIDPPRService } from "../pages/ios-provisioning-profile/SIDIOS_PPRService";
import { SvgDownload } from "@appcircle/assets/lib/Download";
import { SvgTrash } from "@appcircle/assets/lib/Trash";

export type SIDProvisioningProfilesDataTableProps = {
  header?: JSX.Element;
  gridData: SIDProvisioningProfilesDataType[];
  gridHeaders?: string[];
  gridHeaderClassName?: string;
  onItemDeleted: (data: SIDProvisioningProfilesDataType) => void;
  inProgress?: boolean;
  fallback?: JSX.Element;
};

export function SIDProvisioningProfilesDataTable(
  props: SIDProvisioningProfilesDataTableProps
) {
  let RowTemplate = useMemo(() => getRowTemplate(props.onItemDeleted), [
    props.onItemDeleted
  ]);
  return (
    <div className="SIDProvisioningProfileDataTable SIDDataTable">
      <BasicDataGridWithProgress
        key={"grid"}
        data={props.gridData}
        headerClassName={props.gridHeaderClassName || ""}
        headerTemplate={HeaderTemplate}
        rowTemplate={RowTemplate}
        rowSeperatorTemplate={RowSeparatorTemplate}
        inProgress={props.inProgress}
        withCheckbox={false}
      />
      {props.fallback && props.gridData.length === 0 && props.fallback}
    </div>
  );
}
type SIDProvisioningProfilesDataType = SIDPPRType;

function HeaderTemplate() {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="SIDProvisioningProfileDataTable_name">
        <div>{intl.formatMessage(messages.SIDPPRTableHeader_Name)}</div>
      </div>
      <div className={classNames("SIDProvisioningProfileDataTable_appID")}>
        <div>{intl.formatMessage(messages.SIDPPRTableHeader_AppID)}</div>
      </div>
      <div className="SIDProvisioningProfileDataTable_certificate">
        <div>{intl.formatMessage(messages.SIDPPRTableHeader_Certificate)}</div>
      </div>
      <div className="SIDProvisioningProfileDataTable_expired">
        <div>{intl.formatMessage(messages.SIDPPRTableHeader_Expires)}</div>
      </div>
      <div className="SIDProvisioningProfileDataTable_tools">
        <div />
      </div>
    </React.Fragment>
  );
}

export function RowSeparatorTemplate() {
  return <div className="ProfileTestersTableItem_seperator" />;
}

export function getRowTemplate(onItemDeleted: (data: any) => void) {
  return (props: { data: SIDProvisioningProfilesDataType }) => {
    const onClick = useCallback(() => onItemDeleted(props.data), [props.data]);
    const date = useMemo(() => moment(props.data.expireDate), [
      props.data.expireDate
    ]);
    const today = moment();
    const isExpired = today > date;
    const service = useSIDPPRService();

    return (
      <React.Fragment>
        <div className="SIDProvisioningProfileDataTable_name SIDProvisioningProfileDataTable_row_name">
          <div
            className={classNames({
              // SIDDataTable_disabled: props.data.disabled,
              SIDDataTable_warning: isExpired
            })}
          >
            {props.data.name}
          </div>
        </div>
        <div className={classNames("SIDProvisioningProfileDataTable_appID")}>
          <div>{props.data.appId}</div>
        </div>
        <div
          className={classNames(
            "SIDProvisioningProfileDataTable_certificate SIDProvisioningProfileDataTable_row_certificate",
            {
              // SIDDataTable_disabled: props.data.disabled,
              SIDDataTable_warning: !props.data.hasCertificate
            }
          )}
        >
          <div>{props.data.hasCertificate ? "✔" : "x"}</div>
        </div>
        <div className="SIDProvisioningProfileDataTable_expired">
          <div>{date.calendar()}</div>
        </div>
        <div className="SIDProvisioningProfileDataTable_tools dataTableToolsRow">
          <SvgDownload
            onClick={useCallback(() => {
              service.download(props.data.id);
            }, [service])}
          />
          <SvgTrash onClick={onClick} />
        </div>
      </React.Fragment>
    );
  };
}
