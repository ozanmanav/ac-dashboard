import React, { useMemo } from "react";
import classNames from "classnames";
import { BasicDataGridWithProgress } from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import {
  SIDCertificate,
  CertificateCommonType,
  SIDCSR
} from "../pages/ios-certificates/reducer";
import moment from "moment";
import { useIntl } from "react-intl";
import messages from "../messages";
import { SvgDownload } from "@appcircle/assets/lib/Download";
import { SvgUpload } from "@appcircle/assets/lib/Upload";
import { SvgTrash } from "@appcircle/assets/lib/Trash";

type SIDCertificatesEventType = (data: CertificateCommonType) => void;

export type SIDCertificatesDataTableProps<TData> = {
  header?: JSX.Element;
  gridData: CertificateCommonType[];
  gridHeaders: any;
  gridHeaderClassName?: string;
  inProgress?: boolean;
  fallback?: JSX.Element;
  onDownload: SIDCertificatesEventType;
  onUpload: SIDCertificatesEventType;
  onDelete: SIDCertificatesEventType;
};

export function SIDCertificatesDataTable(
  props: SIDCertificatesDataTableProps<CertificateCommonType>
) {
  let RowTemplate = useMemo(
    () => getRowTemplate(props.onDownload, props.onUpload, props.onDelete),
    [props.onDelete, props.onDownload, props.onUpload]
  );
  return (
    <div className="SIDCertificatesDataTable SIDDataTable">
      <div className="SIDDataTable_header">{props.header}</div>

      <BasicDataGridWithProgress<CertificateCommonType>
        key={"grid"}
        data={props.gridData}
        headerClassName={props.gridHeaderClassName || ""}
        headerData={props.gridHeaders}
        headerTemplate={HeaderTemplate}
        rowTemplate={RowTemplate}
        rowSeperatorTemplate={RowSeparatorTemplate}
        inProgress={props.inProgress}
        withCheckbox={false}
      />

      {!props.inProgress &&
        props.fallback &&
        props.gridData.length === 0 &&
        props.fallback}
    </div>
  );
}
export type SIDCertificatesDataType = {
  id: string;
  name: string;
  type: string;
  extension: string;
  expires: string;
  disabled?: boolean;
  warning?: boolean;
};

function HeaderTemplate() {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="SIDCertificatesDataTable_name">
        <div>
          {intl.formatMessage(messages.SIDCertificatesTableHeader_Name)}
        </div>
      </div>
      <div className={classNames("SIDCertificatesDataTable_type")}>
        <div>
          {intl.formatMessage(messages.SIDCertificatesTableHeader_Type)}
        </div>
      </div>
      <div className="SIDCertificatesDataTable_extension">
        <div>
          {intl.formatMessage(messages.SIDCertificatesTableHeader_Extension)}
        </div>
      </div>
      <div className="SIDCertificatesDataTable_expired">
        <div>
          {intl.formatMessage(messages.SIDCertificatesTableHeader_Expires)}
        </div>
      </div>
      <div className="SIDCertificatesDataTable_toolbar">
        <div />
      </div>
    </React.Fragment>
  );
}

export function RowSeparatorTemplate() {
  return <div className="ProfileTestersTableItem_seperator" />;
}

export function getRowTemplate(
  onDownload: SIDCertificatesEventType,
  onUpload: SIDCertificatesEventType,
  onDelete: SIDCertificatesEventType
) {
  return (props: { data: CertificateCommonType }) => {
    const today = moment();
    const expireDate = useMemo(
      () => (props.data.expireDate ? moment(props.data.expireDate) : null),
      [props.data.expireDate]
    );
    const isExpired = expireDate ? today > expireDate : false;
    const expireDateText = expireDate ? expireDate.calendar() : "";
    return (
      <React.Fragment>
        <div className="SIDCertificatesDataTable_name SIDCertificatesDataTable_row_name">
          <div
            className={classNames({
              // SIDDataTable_disabled: props.data.disabled,
              SIDDataTable_warning: isExpired
            })}
          >
            {props.data.name}
          </div>
        </div>
        <div className={classNames("SIDCertificatesDataTable_type")}>
          <div>{props.data.type}</div>
        </div>
        <div className="SIDCertificatesDataTable_extension SIDCertificatesDataTable_row_extension">
          <div>{props.data instanceof SIDCertificate ? "P12" : "CSR"}</div>
        </div>
        <div className="SIDCertificatesDataTable_expired">
          <div>{expireDateText}</div>
        </div>
        <div className="SIDCertificatesDataTable_toolbar dataTableToolsRow">
          <SvgDownload onClick={() => onDownload(props.data)} />
          {props.data instanceof SIDCSR && (
            <SvgUpload onClick={() => onUpload(props.data)} />
          )}
          <SvgTrash onClick={() => onDelete(props.data)} />
        </div>
      </React.Fragment>
    );
  };
}
