import React from "react";

type SIDAndroidKeyStoresEventType = (data: []) => void;

export type SIDAndroidKeyStoresDataTableProps = {
  header?: JSX.Element;
  gridData: [];
  gridHeaderClassName?: string;
  inProgress?: boolean;
  fallback?: JSX.Element;
  onDownload: SIDAndroidKeyStoresEventType;
  onDelete: SIDAndroidKeyStoresEventType;
};

export function SIDAndroidKeyStoresDataTable(
  props: SIDAndroidKeyStoresDataTableProps
) {
  return (
    <div className="SIDAndroidKeyStoresDataTable SIDDataTable">
      <div className="SIDDataTable_header">{props.header}</div>

      {props.fallback && props.gridData.length === 0 && props.fallback}
    </div>
  );
}
