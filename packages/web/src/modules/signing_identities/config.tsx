export const MODULE_API_ROOT = "signing-identity/v1/";

export const CERTIFICATE_API_ROOT = MODULE_API_ROOT + `certificates`;
export const CSR_API_ROOT = MODULE_API_ROOT + `csr`;
export const PROVPROF_API_ROOT = MODULE_API_ROOT + `provisioning-profiles`;
export const KEYSTORE_API_ROOT = MODULE_API_ROOT + `keystores`;
