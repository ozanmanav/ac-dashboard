import { FunctionComponent, SVGProps } from "react";

import { ReactComponent as P12Icon } from "../icons/p12.svg";
import { ReactComponent as PFXIcon } from "../icons/pfx.svg";
import { ReactComponent as ProvisionIcon } from "../icons/provision.svg";
import { ReactComponent as KeystoreIcon } from "../icons/keystore.svg";
import { ReactComponent as JKSIcon } from "../icons/jks.svg";
import { ReactComponent as CerIcon } from "../icons/cer.svg";

export type FileUploadDialogIconsType = {
  p12: FunctionComponent<SVGProps<SVGSVGElement>>;
  pfx: FunctionComponent<SVGProps<SVGSVGElement>>;
  prov: FunctionComponent<SVGProps<SVGSVGElement>>;
  keystore: FunctionComponent<SVGProps<SVGSVGElement>>;
  jks: FunctionComponent<SVGProps<SVGSVGElement>>;
  cer: FunctionComponent<SVGProps<SVGSVGElement>>;
};

export const SIDIcons: FileUploadDialogIconsType = {
  p12: P12Icon,
  pfx: PFXIcon,
  prov: ProvisionIcon,
  keystore: KeystoreIcon,
  jks: JKSIcon,
  cer: CerIcon
};

export type SIDIcon = keyof FileUploadDialogIconsType;

export type FileIconsType = keyof FileUploadDialogIconsType;
