import React, { useMemo } from "react";
import { UploadKeyStore } from "./keystore/UploadKeyStore";
import { GenerateKeyStore } from "./keystore/GenerateKeystore";
import {
  WizardPageType,
  ModalWizard
} from "@appcircle/ui/lib/modal/wizard/ModalWizard";
import { useIntl } from "react-intl";
import messages from "../messages";

export const SIDAndroidKeyStoreUploadWizardModal = (props: any) => {
  const intl = useIntl();

  const wizardData = useMemo<WizardPageType[]>(() => {
    return [
      {
        name: "GENERATE_CERT",
        header: intl.formatMessage(
          messages.SIDModalKeyStoreUploadWizardOption0
        ),
        component: <GenerateKeyStore onModalClose={props.onModalClose} />
      },
      {
        name: "UPLOAD_CERT",
        header: intl.formatMessage(
          messages.SIDModalKeyStoreUploadWizardOption1
        ),
        component: (
          <UploadKeyStore
            formID={props.modalID}
            onModalClose={props.onModalClose}
          />
        )
      }
    ];
  }, [intl, props.modalID, props.onModalClose]);

  return (
    <ModalWizard
      header={intl.formatMessage(messages.SIDModalKeyStoreUploadWizardHeader)}
      onModalClose={props.onModalClose}
      subPages={wizardData}
    />
  );
};
