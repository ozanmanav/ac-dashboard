import { SIDIcons, FileIconsType, SIDIcon } from "../icons";
import { useMemo } from "react";
// import {
//   AddNewFileModalDialog,
//   AddNewFileModalViewProps,
//   AddNewFileModalState
// } from "@appcircle/ui/lib/modal/AddNewFileModalDialog";
import React from "react";
import { useSIDModuleContext } from "../SIDContext";
import { useSIDIOSCertificatesService } from "../pages/ios-certificates/SIDIOSCertificatesService";
import { useSIDPPRService } from "../pages/ios-provisioning-profile/SIDIOS_PPRService";
import { useSIDCSRService } from "../pages/ios-certificates/SIDCSRService";
import {
  AddNewFileModalViewProps,
  AddNewFileModalState,
  AddNewFileModalDialog
} from "shared/modal/AddNewFileModalDialog";

export type FileUploadDialogState = AddNewFileModalState<any, SIDIcon[]>;
export function FileUploadDialog(
  props: AddNewFileModalViewProps<FileUploadDialogState>
) {
  const fileIcons = useMemo(
    () =>
      (((props.routeProps.state && props.routeProps.state.icons) ||
        []) as FileIconsType[]).map(icon => SIDIcons[icon]),
    [props.routeProps.state]
  );

  const context = useSIDModuleContext();
  const certifacesService = useSIDIOSCertificatesService();
  const provService = useSIDPPRService();
  const csrService = useSIDCSRService();

  return (
    <AddNewFileModalDialog
      {...props}
      dispatch={context.dispatch}
      icons={fileIcons}
      onSuccess={resp => {
        if (props.routeProps.state.fileTypes[0] === "cer") {
          certifacesService.update();
          csrService.update();
          provService.update();
        } else if (props.routeProps.state.fileTypes[0] === ".mobileprovision")
          provService.update();
      }}
    />
  );
}
