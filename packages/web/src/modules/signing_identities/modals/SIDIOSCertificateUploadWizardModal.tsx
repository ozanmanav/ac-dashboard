import React, { useMemo } from "react";
import { UploadCert } from "./certificate/UploadCert";
import {
  ModalWizard,
  WizardPageType
} from "@appcircle/ui/lib/modal/wizard/ModalWizard";
import { GenerateCSR } from "./certificate/GenerateCSR";
import { useIntl } from "react-intl";
import messages from "../messages";

export const SIDIOSCertificateUploadWizardModal = (props: any) => {
  const intl = useIntl();
  const subPages = useMemo<WizardPageType[]>(
    () => [
      {
        header: intl.formatMessage(
          messages.SIDModalCertificateUploadWizardOption1
        ),
        name: "UploadCert",
        component: (
          <UploadCert
            formID={"upload-certificate"}
            onModalClose={props.onModalClose}
          />
        )
      },
      {
        header: intl.formatMessage(
          messages.SIDModalCertificateUploadWizardOption0
        ),
        name: "GenerateSCR",
        component: <GenerateCSR onModalClose={props.onModalClose} />
      }
    ],
    [intl, props.onModalClose]
  );

  return (
    <ModalWizard
      header={intl.formatMessage(
        messages.SIDModalCertificateUploadWizardHeader
      )}
      onModalClose={props.onModalClose}
      subPages={subPages}
    />
  );
};

// type ModalFooterType = {
//   state?: SubmitButtonState;
//   onSave?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
// };
// export function ModalFooter(props: ModalFooterType) {
//   return (
//     <div className="SIDIOSCertificateUploadWizardModal_footer">
//       <SubmitButton
//         className={"SendToTesterModalView_primaryButton"}
//         state={props.state}
//         onClick={props.onSave}
//         inProgressText={"Sending..."}
//       >
//         Send
//       </SubmitButton>
//     </div>
//   );
// }
