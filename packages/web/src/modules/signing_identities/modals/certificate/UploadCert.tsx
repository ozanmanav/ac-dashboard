import React, { useState, useCallback, useMemo } from "react";
import { AddNewFile } from "@appcircle/ui/lib/AddNewFile";
import {
  Form,
  FormFieldType,
  FormStatus,
  FormState
} from "@appcircle/form/lib/Form";
import { SubmitButtonState } from "@appcircle/ui/lib/SubmitButton";
import { SIDCertificate } from "../../pages/ios-certificates/reducer";
import { ReactComponent as P12Icon } from "../../icons/p12.svg";
import { FormBody } from "@appcircle/ui/lib/form/FormBody";
import { SIDApiBase } from "modules/signing_identities/SIDModule";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useSIDPPRService } from "modules/signing_identities/pages/ios-provisioning-profile/SIDIOS_PPRService";
import { Result } from "@appcircle/core/lib/services/Result";
import { getErrorCodeFromAjaxErrorResult } from "@appcircle/shared/lib/services/getErrorCodeFromAjaxErrorResult";

export type UploadCertType = {
  formID: any;
  onModalClose: (force?: boolean) => void;
};
export function UploadCert(props: UploadCertType) {
  const [password, setPassword] = useState("");
  const [file, setFile] = useState();
  const { dispatch } = useSIDModuleContext();
  const [submitButtonStatus, setSubmitButtonStatus] = useState(
    SubmitButtonState.ENABLED
  );
  const intl = useIntl();
  const fields: FormFieldType[] = useMemo(
    () => [
      {
        required: true,
        rule: [],
        value: file,
        name: "binary"
      },
      {
        required: false,
        rule: [
          {
            type: "string"
          }
        ],
        value: password,
        name: "password"
      }
    ],
    [file, password]
  );
  const onFileUpload = useCallback(file => setFile(file), []);
  const onChange = useCallback(e => setPassword(e.target.value), []);
  const onStatusChange = useCallback(
    (state: FormState) => {
      if (state.status.submission === FormStatus.SENDING) {
        setSubmitButtonStatus(SubmitButtonState.INPROGRESS);
      } else if (state.status.submission === FormStatus.DONE_WITH_SUCCESS) {
        props.onModalClose();
      } else {
        setSubmitButtonStatus(SubmitButtonState.ENABLED);
      }
    },
    [props]
  );
  const pprService = useSIDPPRService();
  const [formError, setFormError] = useState(false);

  return (
    <Form
      formID={props.formID}
      fields={fields}
      targetPath={SIDApiBase + "certificates"}
      onStatusChange={onStatusChange}
      isJSON={false}
      taskProps={{
        successMessage: () =>
          intl.formatMessage(messages.SIDModalUploadCertSuccess),
        errorMessage: result =>
          intl.formatMessage({
            id: getErrorCodeFromAjaxErrorResult(result),
            defaultMessage: intl.formatMessage(messages.SIDModalUploadCertError)
          }),
        onSuccess(resp) {
          setFormError(false);
          dispatch({
            type: "sid/certificates/add",
            element: new SIDCertificate((resp as Result).json as SIDCertificate)
          });
          pprService.update();
        },
        onError(resp) {
          setFormError(true);
        }
      }}
    >
      <FormBody>
        <div className="Form_text">
          {intl.formatMessage(messages.SIDModalUploadCertDesc)}
        </div>
        <AddNewFile
          autoUpload={false}
          icons={[P12Icon]}
          onFileUpload={onFileUpload}
          types={["p12"]}
          text={intl.formatMessage(messages.appCommonDragFileNotification, {
            file: ".p12"
          })}
          status={
            submitButtonStatus === SubmitButtonState.INPROGRESS
              ? "uploading"
              : formError
              ? "file-selected"
              : "idle"
          }
        />
        <div className="Form_text Form-inner">
          {intl.formatMessage(messages.SIDModalUploadCertPasswordDesc)}
        </div>

        <label htmlFor={"password"} className="Form_label Form-inner">
          {intl.formatMessage(messages.SIDModalUploadCertPassword)}
        </label>
        <input
          type="password"
          id="password"
          className="Form_field"
          value={password}
          onChange={onChange}
        />
      </FormBody>
      <ModalFooter formID={props.formID} state={submitButtonStatus} />
    </Form>
  );
}
