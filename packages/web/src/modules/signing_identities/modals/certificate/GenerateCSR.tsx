import React, { useMemo } from "react";
import { Form } from "@appcircle/form/lib/Form";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { TextInput } from "@appcircle/ui/lib/form/TextInput";
import { SelectBox } from "@appcircle/ui/lib/form/SelectBox";
import { SIDApiBase } from "modules/signing_identities/SIDModule";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import { SIDCSR } from "modules/signing_identities/pages/ios-certificates/reducer";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { CountriesList } from "@appcircle/shared/lib/messages/countries";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { ModalBody } from "@appcircle/ui/lib/modal/ModalBody";
import { Result } from "@appcircle/core/lib/services/Result";
export type GenerateCSRProps = {
  formID?: any;
  onModalClose: (force?: boolean) => void;
};
export function GenerateCSR(props: GenerateCSRProps) {
  const { dispatch } = useSIDModuleContext();
  const intl = useIntl();
  const taskProps = useMemo<TaskPropsType>(
    () => ({
      successMessage: () =>
        intl.formatMessage(messages.SIDModalGenerateCSRSuccess),
      errorMessage: () => intl.formatMessage(messages.SIDModalGenerateCSRError),
      onSuccess(resp) {
        dispatch({
          type: "sid/csr/add",
          element: new SIDCSR((resp as Result).json as SIDCSR)
        });
        props.onModalClose();
      }
    }),
    [intl, dispatch, props]
  );

  return (
    <Form
      method="POST"
      formID={"generate-csr"}
      targetPath={SIDApiBase + "csr"}
      taskProps={taskProps}
    >
      <ModalBody>
        <div className="Form_text">
          {intl.formatMessage(messages.SIDModalGenerateCSRDesc)}
        </div>
        <FormComponent
          formID="generate-csr"
          formProps={{
            required: true,
            name: "name",
            rule: { type: "string" },
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          formFieldLabel={intl.formatMessage(messages.SIDModalGenerateCSRNAME)}
        />
        <FormComponent
          formID="generate-csr"
          formProps={{
            required: true,
            name: "email",
            rule: { type: "email" },
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          formFieldLabel={intl.formatMessage(messages.SIDModalGenerateCSREMAIL)}
        />
        <FormComponent
          formID="generate-csr"
          formProps={{
            required: true,
            name: "countryCode",
            rule: { type: "string" },
            errorClass: "Form_field-error"
          }}
          element={SelectBox}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateCSRCountries
          )}
        >
          <option value={""}>
            {intl.formatMessage(messages.SIDModalGenerateCSRSelectCountry)}
          </option>
          {CountriesList.map(([name, code]) => (
            <option key={name + code} value={code}>
              {name}
            </option>
          ))}
        </FormComponent>
      </ModalBody>
      <ModalFooter formID="generate-csr" />
    </Form>
  );
}
