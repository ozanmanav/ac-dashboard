import { useState, useMemo, useCallback } from "react";
import { SubmitButtonState } from "@appcircle/ui/lib/SubmitButton";
import {
  FormFieldType,
  FormStatus,
  Form,
  FormState
} from "@appcircle/form/lib/Form";
import { AddNewFile } from "@appcircle/ui/lib/AddNewFile";
import { SIDIcons } from "modules/signing_identities/icons";
import React from "react";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import { FormBody } from "@appcircle/ui/lib/form/FormBody";
import { SIDApiBase } from "../../SIDModule";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { TextInput, TextInputProps } from "@appcircle/ui/lib/form/TextInput";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { Result } from "@appcircle/core/lib/services/Result";

type UploadKeyStoreProps = {
  formID: any;
  onModalClose: (force?: boolean) => void;
  animationStyles?: {};
};
export function UploadKeyStore(props: UploadKeyStoreProps) {
  const [file, setFile] = useState();
  const [keystorePassword, setkeystorePassword] = useState("");
  const [aliasPassword, setAliasPassword] = useState("");
  const [submitButtonStatus, setSubmitButtonStatus] = useState(
    SubmitButtonState.ENABLED
  );

  const fields = useMemo<FormFieldType[]>(
    () => [
      {
        required: true,
        value: file,
        name: "binary"
      },
      {
        rule: [
          {
            type: "string"
          }
        ],
        value: keystorePassword,
        name: "keystorePassword"
      },
      {
        rule: [
          {
            type: "string"
          }
        ],
        value: aliasPassword,
        name: "aliasPassword"
      }
    ],
    [aliasPassword, file, keystorePassword]
  );

  const { dispatch } = useSIDModuleContext();
  const intl = useIntl();
  const onFileUpload = useCallback(file => {
    setFile(file);
  }, []);
  const onKeyStorePasswordChange = useCallback(
    val => setkeystorePassword(val),
    []
  );
  const onAliasPasswordChange = useCallback(val => setAliasPassword(val), []);
  const onStatusChange = useCallback(
    (state: FormState) => {
      if (state.status.submission === FormStatus.SENDING) {
        setSubmitButtonStatus(SubmitButtonState.INPROGRESS);
      } else if (state.status.submission === FormStatus.DONE_WITH_SUCCESS) {
        props.onModalClose();
      } else {
        setSubmitButtonStatus(SubmitButtonState.ENABLED);
      }
    },
    [props]
  );
  const taskProps = useMemo(
    () => ({
      successMessage: () =>
        intl.formatMessage(messages.SIDModalUploadKeystoreSuccess),
      errorMessage: () =>
        intl.formatMessage(messages.SIDModalUploadKeystoreError),
      onSuccess(resp: any) {
        dispatch({
          type: "sid/keystore/add",
          element: (resp as Result).json
        });
      }
    }),
    [dispatch, intl]
  );

  return (
    <Form
      formID={props.formID}
      fields={fields}
      targetPath={SIDApiBase + "keystores/binary"}
      onStatusChange={onStatusChange}
      isJSON={false}
      className="UploadKeyStoreForm"
      taskProps={taskProps}
    >
      <FormBody>
        <div className="Form_text">
          {intl.formatMessage(messages.SIDModalUploadKeystoreDesc)}
        </div>
        <AddNewFile
          icons={[SIDIcons.keystore, SIDIcons.jks]}
          onFileUpload={onFileUpload}
          autoUpload={false}
          types={["keystore", "jks"]}
          text={intl.formatMessage(messages.appCommonDragFileNotification, {
            file: ".keystore or .jks"
          })}
        />
        <div className="SIDAndroidKeyStoreUploadWizardModal_help SIDAndroidKeyStoreUploadWizardModal-inner">
          {intl.formatMessage(messages.SIDModalUploadKeystorePasswordDesc)}
        </div>
        <FormComponent<TextInputProps>
          formID={props.formID}
          formProps={{
            name: "keystorePassword",
            errorClass: "Form_field-error"
          }}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalUploadKeystorePassword
          )}
          value={keystorePassword}
          onChange={onKeyStorePasswordChange}
          element={TextInput}
        />
        <FormComponent<TextInputProps>
          formID={props.formID}
          formProps={{
            name: "aliasPassword",
            errorClass: "Form_field-error"
          }}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalUploadKeystoreAliasPassword
          )}
          value={aliasPassword}
          onChange={onAliasPasswordChange}
          element={TextInput}
        />
      </FormBody>
      <ModalFooter formID={props.formID} state={submitButtonStatus} />
    </Form>
  );
}
