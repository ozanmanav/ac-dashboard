import { useState, useCallback } from "react";
import { SubmitButtonState } from "@appcircle/ui/lib/SubmitButton";
import { FormStatus, Form, FormState } from "@appcircle/form/lib/Form";
import { ModalFooter } from "@appcircle/ui/lib/modal/ModalFooter";
import React from "react";
import {
  NumberStepperInput,
  NumberStepperInputProps
} from "@appcircle/ui/lib/form/NumberStepperInput";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { TextInputProps, TextInput } from "@appcircle/ui/lib/form/TextInput";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { FormErrorsMessages } from "@appcircle/form/lib/formErrors";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { ModalBody } from "@appcircle/ui/lib/modal/ModalBody";
import { Result } from "@appcircle/core/lib/services/Result";

type GenerateKeyStoreProps = {
  onModalClose: (force?: boolean) => void;
};

export function GenerateKeyStore(props: GenerateKeyStoreProps) {
  const [submitButtonStatus, setSubmitButtonStatus] = useState(
    SubmitButtonState.ENABLED
  );
  const { dispatch } = useSIDModuleContext();
  const intl = useIntl();
  const onStatusChange = useCallback(
    (state: FormState) => {
      if (state.status.submission === FormStatus.SENDING) {
        setSubmitButtonStatus(SubmitButtonState.INPROGRESS);
      } else if (state.status.submission === FormStatus.DONE_WITH_SUCCESS) {
        props.onModalClose();
      } else {
        setSubmitButtonStatus(SubmitButtonState.ENABLED);
      }
    },
    [props]
  );
  return (
    <Form
      formID={"generate_keystore"}
      targetPath={"signing-identity/v1/keystores"}
      onStatusChange={onStatusChange}
      taskProps={{
        successMessage: () =>
          intl.formatMessage(messages.SIDModalGenerateKeystoreSuccess),
        errorMessage: () =>
          intl.formatMessage(messages.SIDModalGenerateKeystoreError),
        onSuccess(resp: any) {
          dispatch({
            type: "sid/keystore/add",
            element: (resp as Result).json
          });
          props.onModalClose();
        }
      }}
    >
      <ModalBody>
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            required: true,
            name: "password",
            rule: [
              {
                type: "regex",
                testPattern: ".{6,}",
                errorMessage: intl.formatMessage(
                  FormErrorsMessages["password-min"],
                  { min: 6 }
                )
              },
              {
                type: "confirm-field",
                confirmfield: "passwordConfirm"
              }
            ],
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystorePassword
          )}
          type="password"
        />
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            required: true,
            name: "passwordConfirm",
            rule: { type: "confirm" },
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          type="password"
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreConfirmPassword
          )}
        />
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            name: "alias",
            required: true,
            rule: { type: "string" },
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreAlias
          )}
        />
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            name: "aliasPassword",
            required: true,
            rule: [
              {
                type: "regex",
                testPattern: ".{6,}",
                errorMessage: intl.formatMessage(
                  FormErrorsMessages["password-min"],
                  { min: 6 }
                )
              },
              {
                type: "confirm-field",
                confirmfield: "aliasPasswordConfirm"
              }
            ],
            errorClass: "Form_field-error"
          }}
          type="password"
          element={TextInput}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreAliasPassword
          )}
        />
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            required: true,
            name: "aliasPasswordConfirm",
            rule: { type: "confirm" },
            errorClass: "Form_field-error"
          }}
          type="password"
          element={TextInput}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreAliasConfirmPassword
          )}
        />
        <FormComponent<NumberStepperInputProps>
          formID={"generate_keystore"}
          formProps={{
            required: true,
            name: "validity",
            rule: { type: "number" },
            errorClass: "Form_field-error",
            value: "25"
          }}
          element={NumberStepperInput}
          max={100}
          min={25}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreValidity
          )}
        />
        <FormComponent<TextInputProps>
          formID={"generate_keystore"}
          formProps={{
            name: "name",
            required: true,
            rule: [
              {
                type: "string-min",
                min: 5
              },
              {
                type: "regex",
                testPattern: "[a-zA-Z]{2,}\\s+[a-zA-Z]{2,}",
                customType: "fullname"
              }
            ],
            errorClass: "Form_field-error"
          }}
          element={TextInput}
          formFieldLabel={intl.formatMessage(
            messages.SIDModalGenerateKeystoreFullname
          )}
        />
      </ModalBody>
      <ModalFooter formID="generate_keystore" state={submitButtonStatus} />
    </Form>
  );
}
