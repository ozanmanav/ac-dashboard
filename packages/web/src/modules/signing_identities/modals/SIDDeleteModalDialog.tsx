import React, { useCallback } from "react";
import { DeleteModalDialog } from "@appcircle/ui/lib/modal/DeleteModalDialog";
import {
  ModalComponentPropsType,
  KeyValTupleFrom
} from "@appcircle/shared/lib/Module";
import {
  CSR_API_ROOT,
  CERTIFICATE_API_ROOT,
  KEYSTORE_API_ROOT,
  PROVPROF_API_ROOT
} from "../config";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { MessageDescriptor, useIntl } from "react-intl";
import messages from "../messages";
import { useSIDModuleContext } from "../SIDContext";

export type DeleteModalRouteParams = KeyValTupleFrom<
  ["id", string, "name", string]
>;
type Props = ModalComponentPropsType<{}, {}, DeleteModalRouteParams>;
type DeleteModalParams = {
  successAction:
    | "sid/csr/delete"
    | "sid/certificates/delete"
    | "sid/keystore/delete"
    | "sid/ppr/delete";
  subject: MessageDescriptor;
  endpoint: string;
  errorMessage: MessageDescriptor;
  successMessage: MessageDescriptor;
};
function createSIDDeleteModalDialog(params: DeleteModalParams) {
  return (props: Props) => {
    const { dispatch } = useSIDModuleContext();
    const intl = useIntl();
    return (
      <DeleteModalDialog
        header={""}
        skipNameValidation={false}
        entityID={props.routeProps.params.id}
        routeProps={props.routeProps}
        endpoint={params.endpoint + "/" + props.routeProps.params.id}
        errorMessage={intl.formatMessage(params.errorMessage)}
        modalID={props.modalID}
        name={decodeURI(props.routeProps.params.name)}
        placeHolder={props.routeProps.params.name}
        onModalClose={props.onModalClose}
        successMessage={intl.formatMessage(params.successMessage)}
        onSuccess={useCallback(() => {
          props.onModalClose();
          dispatch({
            type: params.successAction,
            payload: props.routeProps.params.id
          });
        }, [dispatch, props.routeProps])}
        title={intl.formatMessage(CommonMessages.deleteModalDialogTitle, {
          subject: intl.formatMessage(params.subject)
        })}
      />
    );
  };
}

export const CertificatesDeleteModalView = createSIDDeleteModalDialog({
  successAction: "sid/certificates/delete",
  subject: messages.SIDPageCertificates,
  endpoint: CERTIFICATE_API_ROOT,
  errorMessage: messages.SIDCertsDeletionError,
  successMessage: messages.SIDCertsDeletionSuccess
});
export const CSRDeleteModalView = createSIDDeleteModalDialog({
  successAction: "sid/csr/delete",
  subject: messages.SIDPageCSRCertificatesShort,
  endpoint: CSR_API_ROOT,
  errorMessage: messages.SIDCertsCSRDeletionError,
  successMessage: messages.SIDCertsCSRDeletionSuccess
});
export const KeystoreDeleteModalView = createSIDDeleteModalDialog({
  successAction: "sid/keystore/delete",
  subject: messages.SIDPageAndroidKeystores,
  endpoint: KEYSTORE_API_ROOT,
  errorMessage: messages.SIDAndroidKeystoreDeletionError,
  successMessage: messages.SIDAndroidKeystoreDeletionSuccess
});
export const ProvProfilleDeleteModalView = createSIDDeleteModalDialog({
  successAction: "sid/ppr/delete",
  subject: messages.SIDPageIOSProvProfiles,
  endpoint: PROVPROF_API_ROOT,
  errorMessage: messages.SIDPPRDeleteError,
  successMessage: messages.SIDPPRDeleteSuccess
});
