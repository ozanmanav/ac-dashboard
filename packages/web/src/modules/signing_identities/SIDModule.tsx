import React, { useEffect } from "react";
import { SigningIdentities } from "./SigningIdentities";
import { ModuleNavBarDataType, ModuleProps } from "@appcircle/shared/lib/Module";
import { SIDStore, SIDStoreType } from "./store";
import { SIDIOSCertificateUploadWizardModal } from "./modals/SIDIOSCertificateUploadWizardModal";
import { SIDAndroidKeyStoreUploadWizardModal } from "./modals/SIDAndroidKeyStoreUploadWizardModal";
import { SIDContextProvider, SIDContextType } from "./SIDContext";
import { useAppModuleContext } from "AppModuleContext";
import { FileUploadDialog } from "./modals/FileUploadDialog";
import { IntlShape, useIntl } from "react-intl";
import messages from "./messages";
import {
  CSRDeleteModalView,
  CertificatesDeleteModalView,
  KeystoreDeleteModalView,
  ProvProfilleDeleteModalView
} from "./modals/SIDDeleteModalDialog";

const buildModuleNavBarData: (
  intl: IntlShape
) => ModuleNavBarDataType = intl => ({
  header: intl.formatMessage(messages.SIDModuleHeader),
  items: [
    {
      text: intl.formatMessage(messages.SIDPageCertificates),
      link: "/ios-certificates"
    },
    {
      text: intl.formatMessage(messages.SIDPageIOSProvProfiles),
      link: "/ios-provisionings"
    },
    {
      text: intl.formatMessage(messages.SIDPageAndroidKeystores),
      link: "/android-keystores"
    },
    {
      text: intl.formatMessage(messages.SIDPageStartGuide),
      link: "/start-guide"
    }
  ],
  selectedItemIndex: 0
});
export enum SDModals {
  SDIOSCertificateUploadWizard = "/modal/SDIOSCertificateUploadWizard",
  SIDAndroidKeyStoreUploadWizardModal = "/modal/SIDAndroidKeyStoreUploadWizardModal",
  FileUploadDialog = "/modal/AddNewFileModalDialog",
  CertificateDeleteDialog = "/modal/CertificateDeleteModal",
  KeystoreDeleteDialog = "/modal/KeystoreDeleteDialog",
  CSRDeleteDialog = "/modal/CSRDeleteDialog",
  ProvProfileDeleteDialog = "/modal/ProvProfileDeleteDialog"
}

export const SIDModulePrefix = "signing_identities";
export const SIDModulePathPrefix = "/" + SIDModulePrefix;
export const SIDApiBase = "signing-identity/v1/";

export const SIDModals = [
  {
    path: SIDModulePathPrefix + SDModals.SDIOSCertificateUploadWizard,
    component: SIDIOSCertificateUploadWizardModal
  },
  {
    path: SIDModulePathPrefix + SDModals.SIDAndroidKeyStoreUploadWizardModal,
    component: SIDAndroidKeyStoreUploadWizardModal
  },
  {
    path: SIDModulePathPrefix + SDModals.FileUploadDialog,
    component: FileUploadDialog
  },
  {
    path: SIDModulePathPrefix + SDModals.CSRDeleteDialog,
    component: CSRDeleteModalView
  },
  {
    path: SIDModulePathPrefix + SDModals.CertificateDeleteDialog,
    component: CertificatesDeleteModalView
  },
  {
    path: SIDModulePathPrefix + SDModals.KeystoreDeleteDialog,
    component: KeystoreDeleteModalView
  },
  {
    path: SIDModulePathPrefix + SDModals.ProvProfileDeleteDialog,
    component: ProvProfilleDeleteModalView
  }
];

export default SigningIdentitiesModule;
export function SigningIdentitiesModule(props: ModuleProps) {
  const { setContext, setStore } = useAppModuleContext<
    SIDStoreType,
    SIDContextType
  >();
  const intl = useIntl();

  useEffect(() => {
    setContext(props.context);
    setStore(SIDStore);
    props.context.createModuleNavBar(buildModuleNavBarData(intl));

    return () => console.log("Unmounting SigningIdentitiesModule ...");
    // eslint-disable-next-line
  }, []);
  return (
    <SIDContextProvider context={props.context} store={SIDStore}>
      <SigningIdentities />
    </SIDContextProvider>
  );
}
