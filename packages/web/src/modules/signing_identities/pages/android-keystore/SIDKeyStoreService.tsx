import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useMemo } from "react";
import { SIDKeystoreType } from "./reducer";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { Result } from "@appcircle/core/lib/services/Result";

const baseUrl = "signing-identity/v1/keystores";

export function useSIDKeystoreService() {
  const moduleContext = useSIDModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const { pushMessage } = useNotificationsService();
  const serviceDispatch = useMemo(() => createApiConsumer(), [
    createApiConsumer
  ]);
  const intl = useIntl();

  const services = useMemo(
    () => ({
      update() {
        const task = taskManager.createApiTask<SIDKeystoreType>(
          serviceDispatch({
            method: "GET",
            endpoint: baseUrl
          }),
          {
            onSuccess: result => {
              // pushMessage({
              //   messageType: "success",
              //   message: intl.formatMessage(
              //     messages.SIDAndroidKeystoreUpdateSuccess
              //   )
              // });
              moduleContext.dispatch({
                type: "sid/keystore/update",
                elements: (result as Result).json as SIDKeystoreType[]
              });
            }
          }
        );
        task.entityID = "sid/keystore/update";
        taskManager.addTask(task, 1);
      },
      getByID(PPRID: string) {
        const task = taskManager.createApiTask<SIDKeystoreType>(
          serviceDispatch({
            method: "GET",
            endpoint: `${baseUrl}/${PPRID}`
          }),
          {
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/keystore/update",
                elements: (result as Result).json as SIDKeystoreType[]
              });
            }
          }
        );
        task.entityID = PPRID;
        taskManager.addTask(task);
      },
      delete(PPRID: string) {
        const task = taskManager.createApiTask<SIDKeystoreType>(
          serviceDispatch({
            method: "DELETE",
            endpoint: `${baseUrl}/${PPRID}`
          }),
          {
            errorMessage: () =>
              intl.formatMessage(messages.SIDAndroidKeystoreDeletionError),
            successMessage: () =>
              intl.formatMessage(messages.SIDAndroidKeystoreDeletionSuccess),
            onError() {},
            onSuccess: result => {
              this.update();
            }
          }
        );
        task.entityID = PPRID;
        taskManager.addTask(task);
      }
    }),
    [intl, moduleContext, pushMessage, serviceDispatch, taskManager]
  );

  return services;
}
