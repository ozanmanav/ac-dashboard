import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { SIDKeystoreState, SIDKeystoreType } from "./reducer";

// export type SIDKeystoreModelResult = { id: string };
export class SIDKeystoreModel extends Model<SIDKeystoreState, SIDKeystoreType>
  implements ISearchable, ModelStateBase {
  protected init(): void {}
  length(): number {
    return this.state.keystore.length;
  }
  isLoaded(): boolean {
    return true;
  }
  search(text: string): any[] {
    return this.state.keystore.filter(item => {
      return item.name.indexOf(text) > -1;
    });
  }
  findById(id: string) {
    return this.state.keystore[this.state.keystoreByID[id]];
  }
  getAll() {
    return this.state.keystore;
  }
}
export function useSIDKeystoreModel(): SIDKeystoreModel {
  const { state } = useSIDModuleContext();
  const model = useMemo(() => new SIDKeystoreModel(state.keystores), [
    state.keystores
  ]);

  return model;
}
