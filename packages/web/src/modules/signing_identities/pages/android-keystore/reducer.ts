import produce from "immer";
import { StateMutator } from "@appcircle/core/lib/Mutator";

const initialState = {
  keystore: [],
  keystoreByID: {}
};

export type SIDKeystoreType = {
  id: string;
  organizationId: string;
  name: string;
  appId: string;
  hasCertificate: boolean;
  certificateThumbPrints: string;
  alias: string;
  expireDate: string;
  createDate: string;
};

export type SIDKeystoreState = {
  keystore: SIDKeystoreType[];
  keystoreByID: { [id: string]: number };
};

// const mockState: SIDKeystoreType[] = [
//   {
//     id: "e4af104b-fafa-44e2-b25b-cd615da36c6f",
//     teamId: "3c0d9191-e29e-423b-b9a0-43b4c4d021d2",
//     name: "Smartface In-House Distribution (Enterprise)",
//     appId: "*",
//     hasCertificate: false,
//     certificateThumbPrints: "E744BC358C003D86F8D1AADF6115E33B39627F0A",
//     expireDate: "2019-05-27T16:14:07.9732447+00:00",
//     createDate: "2019-05-27T16:14:07.9732454+00:00"
//   },
//   {
//     id: "5ee8444b-56d0-420b-a791-efd78495bae4",
//     teamId: "3c0d9191-e29e-423b-b9a0-43b4c4d021d2",
//     name: "Smartface In-House Distribution (Enterprise)",
//     appId: "*",
//     hasCertificate: false,
//     certificateThumbPrints: "E744BC358C003D86F8D1AADF6115E33B39627F0A",
//     expireDate: "2019-05-27T16:14:39.000713+00:00",
//     createDate: "2019-05-27T16:14:39.0007133+00:00"
//   }
// ];

export type SIDKeystoreActions =
  | {
      type: "sid/keystore/update";
      elements: SIDKeystoreType[];
    }
  | { type: "sid/keystore/add"; element: SIDKeystoreType }
  | { type: "sid/keystore/delete"; payload: string };

function createMutator(parent: SIDKeystoreState) {
  return new StateMutator<SIDKeystoreType>(parent, "keystore");
}

export function reducer(
  state: SIDKeystoreState = initialState,
  action: SIDKeystoreActions
) {
  return produce(state, draft => {
    switch (action.type) {
      case "sid/keystore/update":
        draft.keystore = action.elements.reverse();
        createMutator(draft).invalidateIndexesByKey();
        break;
      case "sid/keystore/add":
        createMutator(draft).unshift(action.element);
        break;
      case "sid/keystore/delete":
        createMutator(draft).delete(action.payload);
        break;
      default:
        break;
    }
  });
}
