import React, { useCallback } from "react";
import { SearchLayout } from "@appcircle/ui/lib/SearchLayout";
import { useSIDKeystoreModel } from "./SIDKeystoreModel";
import {
  SIDKeystoreState,
  SIDKeystoreActions,
  SIDKeystoreType
} from "./reducer";
import { withRouter, RouteComponentProps } from "react-router";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { SIDKeystoreDataTable } from "modules/signing_identities/components/SIDKeystoreDataTable";
import { SDModals } from "modules/signing_identities/SIDModule";
import { ReactComponent as NoElements } from "../../icons/cert-empty.svg";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { AddNewFileModalState } from "shared/modal/AddNewFileModalDialog";
import { useGTMService } from "shared/domain/useGTMService";
type SIDKeystoreProps = {};

export function SIDKeystore(props: RouteComponentProps & SIDKeystoreProps) {
  const { triggerGTMEvent } = useGTMService();
  const model = useSIDKeystoreModel();
  const context = useSIDModuleContext();
  const taskManager = useTaskManager();
  const taskStatus = taskManager.getStatusByEntityID("sid/keystore/update");
  const inProgress =
    taskStatus === undefined || taskStatus.status !== "COMPLETED";
  // const router = useContext(__RouterContext);

  const onItemDeleted = useCallback(
    data => {
      // service.delete(data.id);
      props.history.push(
        props.history.createHref({
          pathname: props.location.pathname,
          search:
            context.createModalUrl(SDModals.KeystoreDeleteDialog) +
            "&id=" +
            data.id +
            "&name=" +
            data.name
        })
      );
    },
    [props.history, context, props.location]
  );
  const intl = useIntl();

  return (
    <SearchLayout<SIDKeystoreState, SIDKeystoreType>
      header={intl.formatMessage(messages.SIDAndroidKeystoreSearchHeader)}
      model={model}
      isLoaded={!inProgress}
      noElementsIcon={NoElements}
      noElementsText={intl.formatMessage(
        messages.SIDAndroidKeystoreSearchNoKeystore
      )}
      onAddElement={() => {
        triggerGTMEvent("signing_keystore_new");
        props.history.push(
          props.history.createHref({
            pathname: props.location.pathname,
            search: context.createModalUrl(
              SDModals.SIDAndroidKeyStoreUploadWizardModal
            )
          }),
          {
            endpoint: "signing-identity/v1/keystores",
            fileTypes: [],
            key: "binary",
            action: "sid/keystore/add"
          } as AddNewFileModalState<SIDKeystoreActions>
        );
      }}
    >
      {data => (
        <SIDKeystoreDataTable
          onItemDeleted={onItemDeleted}
          gridData={data}
          inProgress={inProgress}
          fallback={
            <div className="ProfileGroups_notesters ProfileDetail_notesters">
              <span>
                {intl.formatMessage(
                  messages.SIDAndroidKeystoreSearchNoKeystore
                )}
              </span>
            </div>
          }
        />
      )}
    </SearchLayout>
  );
}

export const SIDKeystoreWrapper = withRouter(SIDKeystore);
