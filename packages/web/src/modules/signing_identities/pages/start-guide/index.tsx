import React from "react";
import { StartGuideLayout } from "shared/startguides/StartGuideLayout";

export default () => (
  <StartGuideLayout>
    <h1 className="content_title">
      Getting Started with the Signing Identities Module
    </h1>
    <p>
      New to Appcircle Signing Identities module? Follow our quick start guide
      to manage your signing identities to be able to sign your builds for
      distribution.
    </p>

    <div className="callout">
      This module provides a centralized platform to store and manage all of
      your mobile code signing assets.
    </div>

    <h4 className="section_title">1. Generate or upload iOS certificates</h4>
    <ul>
      <li>
        Create a certificate signing request (CSR) to generate a new certificate
        from the Apple Developer portal and then upload the certificate (CER) to
        create a certificate bundle (P12). No Mac needed.
      </li>
    </ul>
    <p>or</p>

    <ul>
      <li>
        Upload your readily available certificate bundle (P12) along with the
        bundle password.
      </li>
    </ul>

    <div className="callout">
      All types of iOS certificates are supported, including development, ad
      hoc, in-house or App Store distribution.
    </div>

    <img className="screen" src="/assets/img/iOS-Certificates.png" />

    <h4 className="section_title">2. Upload iOS provisioning profiles</h4>
    <p>
      Simply upload your provisioning profiles obtained from the Apple Developer
      portal.
    </p>

    <div className="callout">
      Provisioning profile and certificate matching is done automatically.
    </div>

    <img className="screen" src="/assets/img/iOS-Provisioning.png" />

    <h4 className="section_title">3. Generate or upload Android keystores</h4>
    <ul>
      <li>
        Create a keystore just by entering the necessary information. No
        additional software needed.
      </li>
    </ul>
    <p>or</p>

    <ul>
      <li>
        Upload your readily available keystore along with the password(s).
      </li>
    </ul>

    <div className="callout">
      Multi-alias keystores and in-project keystores are also supported for
      various use cases.
    </div>

    <img className="screen" src="/assets/img/Android-Keystores.png" />

    <h4 className="section_title">
      4. Assign signing identities in the Build module for distribution
    </h4>
    <p>
      For your iOS or Android build projects, select your signing identities for
      distribution. The distribution-ready binaries will be signed with the
      selected signing identities both in manual and automatic distribution
      cases.
    </p>

    <div className="callout">
      With the Signing Identities module, you don’t need any specific hardware
      or a third-party software to create and manage your signing identities and
      build your applications.
    </div>
    <img className="screen" src="/assets/img/Signing-Settings.png" />
  </StartGuideLayout>
);
