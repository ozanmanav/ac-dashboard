import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { IOSPPRState } from "./reducer";

export type SIDIOS_PPRModelResult = { id: string };
export class SIDIOS_PPRModel extends Model<IOSPPRState, SIDIOS_PPRModelResult>
  implements ISearchable, ModelStateBase {
  protected init(): void {}
  length(): number {
    return this.state.ppr.length;
  }
  isLoaded(): boolean {
    return true;
  }
  search(text: string): any[] {
    return this.state.ppr.filter(item => {
      return item.name.indexOf(text) > -1;
    });
  }
  findById(id: string) {
    return this.state.ppr[this.state.pprByID[id]];
  }
  getAll() {
    return this.state.ppr;
  }
}
export function useSIDIOS_PPRModel(): SIDIOS_PPRModel {
  const { state } = useSIDModuleContext();
  const model = useMemo(() => new SIDIOS_PPRModel(state.pprs), [state.pprs]);

  return model;
}
