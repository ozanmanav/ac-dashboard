import produce from "immer";
import { StateMutator } from "@appcircle/core/lib/Mutator";

const initialState = {
  ppr: [],
  pprByID: {}
};
export type SIDPPRType = {
  id: string;
  organizationId: string;
  name: string;
  appId: string;
  hasCertificate: boolean;
  certificateThumbPrints: string;
  expireDate: string;
  createDate: string;
};

export type IOSPPRState = {
  ppr: SIDPPRType[];
  pprByID: { [id: string]: number };
};

// const mockState: SIDPPRType[] = [
//   {
//     id: "e4af104b-fafa-44e2-b25b-cd615da36c6f",
//     teamId: "3c0d9191-e29e-423b-b9a0-43b4c4d021d2",
//     name: "Smartface In-House Distribution (Enterprise)",
//     appId: "*",
//     hasCertificate: false,
//     certificateThumbPrints: "E744BC358C003D86F8D1AADF6115E33B39627F0A",
//     expireDate: "2019-05-27T16:14:07.9732447+00:00",
//     createDate: "2019-05-27T16:14:07.9732454+00:00"
//   },
//   {
//     id: "5ee8444b-56d0-420b-a791-efd78495bae4",
//     teamId: "3c0d9191-e29e-423b-b9a0-43b4c4d021d2",
//     name: "Smartface In-House Distribution (Enterprise)",
//     appId: "*",
//     hasCertificate: false,
//     certificateThumbPrints: "E744BC358C003D86F8D1AADF6115E33B39627F0A",
//     expireDate: "2019-05-27T16:14:39.000713+00:00",
//     createDate: "2019-05-27T16:14:39.0007133+00:00"
//   }
// ];

export type SIDPPRActions =
  | {
      type: "sid/ppr/update";
      payload: SIDPPRType[];
    }
  | { type: "sid/ppr/add"; payload: SIDPPRType }
  | { type: "sid/ppr/delete"; payload: string };

function createMutator(parent: IOSPPRState) {
  return new StateMutator<SIDPPRType>(parent, "ppr");
}

export function reducer(
  state: IOSPPRState = initialState,
  action: SIDPPRActions
) {
  return produce(state, draft => {
    switch (action.type) {
      case "sid/ppr/update":
        draft.ppr = action.payload;
        createMutator(draft).invalidateIndexesByKey();
        break;
      case "sid/ppr/add":
        createMutator(draft).unshift(action.payload);
        break;
      case "sid/ppr/delete":
        createMutator(draft).delete(action.payload);
        break;
      default:
        break;
    }
  });
}
