import React, { useCallback } from "react";
import { SearchLayout } from "@appcircle/ui/lib/SearchLayout";
import { useSIDIOS_PPRModel } from "./SIDIOS_PPRModel";
import { IOSPPRState, SIDPPRType } from "./reducer";
import { SIDProvisioningProfilesDataTable } from "modules/signing_identities/components/SIDProvisioningProfilesDataTable";
import { withRouter, RouteComponentProps } from "react-router";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { SDModals } from "modules/signing_identities/SIDModule";
import { FileUploadDialogState } from "modules/signing_identities/modals/FileUploadDialog";
import { ReactComponent as NoElements } from "../../icons/cert-empty.svg";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { useGTMService } from "shared/domain/useGTMService";

type SIDIOS_PPRProps = {};

export function SIDIOS_PPR(props: RouteComponentProps & SIDIOS_PPRProps) {
  const { triggerGTMEvent } = useGTMService();
  const model = useSIDIOS_PPRModel();
  const context = useSIDModuleContext();
  const taskManager = useTaskManager();
  const taskStatus = taskManager.getStatusByEntityID("sid/ppr/update");
  const intl = useIntl();
  const inProgress =
    taskStatus === undefined || taskStatus.status !== "COMPLETED";
  const onItemDeleted = useCallback(
    data => {
      // service.delete(data.id);
      props.history.push(
        props.history.createHref({
          pathname: props.location.pathname,
          search:
            context.createModalUrl(SDModals.ProvProfileDeleteDialog) +
            "&id=" +
            data.id +
            "&name=" +
            data.name
        })
      );
    },
    [props.history, context, props.location]
  );
  return (
    <SearchLayout<IOSPPRState, SIDPPRType>
      header={intl.formatMessage(messages.SIDPageIOSProvProfiles)}
      noElementsIcon={NoElements}
      noElementsText={intl.formatMessage(messages.SIDCertsNoProfiles)}
      model={model}
      isLoaded={!inProgress}
      onAddElement={() => {
        triggerGTMEvent("signing_iosprov_new");
        props.history.push(
          props.history.createHref({
            pathname: props.location.pathname,
            search: context.createModalUrl(SDModals.FileUploadDialog)
          }),
          {
            endpoint: "signing-identity/v1/provisioning-profiles",
            fileTypes: [".mobileprovision"],
            key: "binary",
            action: "sid/ppr/add",
            icons: ["prov"],
            text: intl.formatMessage(messages.appCommonDragFileNotification, {
              file: ".mobileprovision"
            })
          } as FileUploadDialogState
        );
      }}
    >
      {data => (
        <SIDProvisioningProfilesDataTable
          onItemDeleted={onItemDeleted}
          gridData={data}
          inProgress={inProgress}
          fallback={
            <div className="ProfileGroups_notesters ProfileDetail_notesters">
              <span>{intl.formatMessage(messages.SIDCertsNoProfiles)}</span>
            </div>
          }
        />
      )}
    </SearchLayout>
  );
}

export const SIDIOS_PPRWrapper = withRouter(SIDIOS_PPR);
