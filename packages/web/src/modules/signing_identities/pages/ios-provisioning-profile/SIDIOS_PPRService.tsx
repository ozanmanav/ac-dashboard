import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useMemo } from "react";
import { SIDPPRType } from "./reducer";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { save } from "@appcircle/core/lib/save";
import { readFilename } from "@appcircle/core/lib/utils/readContentDisposition";
import { Result } from "@appcircle/core/lib/services/Result";

const baseUrl = "signing-identity/v1/provisioning-profiles";

export function useSIDPPRService() {
  const moduleContext = useSIDModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const serviceCreate = useMemo(() => createApiConsumer(), [createApiConsumer]);
  const intl = useIntl();
  const services = useMemo(
    () => ({
      update() {
        const task = taskManager.createApiTask<SIDPPRType>(
          serviceCreate({
            method: "GET",
            endpoint: baseUrl
          }),
          {
            errorMessage: () => intl.formatMessage(messages.SIDPPRUpdateError),
            // successMessage: () =>
            //   intl.formatMessage(messages.SIDPPRUpdateSuccess),
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/ppr/update",
                payload: (result as Result).json as SIDPPRType[]
              });
            }
          }
        );
        task.entityID = "sid/ppr/update";
        taskManager.addTask(task, 1);
      },
      getByID(PPRID: string) {
        const task = taskManager.createApiTask<SIDPPRType>(
          serviceCreate({
            method: "GET",
            endpoint: `${baseUrl}/${PPRID}`
          }),
          {
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/ppr/update",
                payload: (result as Result).json as SIDPPRType[]
              });
            }
          }
        );
        task.entityID = PPRID;
        taskManager.addTask(task);
      },
      delete(PPRID: string) {
        const task = taskManager.createApiTask<SIDPPRType>(
          serviceCreate({
            method: "DELETE",
            endpoint: `${baseUrl}/${PPRID}`
          }),
          {
            errorMessage: () => intl.formatMessage(messages.SIDPPRDeleteError),
            successMessage: () =>
              intl.formatMessage(messages.SIDPPRDeleteSuccess),
            onError() {},
            onSuccess: result => {
              services.update();
            }
          }
        );
        task.entityID = PPRID;
        taskManager.addTask(task);
      },
      download(PPRID: string) {
        const task = taskManager.createApiTask<SIDPPRType>(
          serviceCreate({
            method: "GET",
            endpoint: `${baseUrl}/${PPRID}`,
            responseType: "arraybuffer"
          }),
          {
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsDownloadError),
            successMessage: () =>
              intl.formatMessage(messages.SIDCertsDownloadSuccess),
            onSuccess: result => {
              save(
                (result as Result).json,
                [
                  readFilename((result as Result).headers),
                  "downloaded.mobileprovision"
                ],
                "text"
              );
            }
          }
        );
        task.entityID = PPRID;
        taskManager.addTask(task);
      }
    }),
    [intl, moduleContext, serviceCreate, taskManager]
  );

  return services;
}
