import React, { useMemo, useCallback } from "react";
import { SearchLayout } from "@appcircle/ui/lib/SearchLayout";

import { SIDCertificatesDataTable } from "modules/signing_identities/components/SIDCertificatesDataTable";
import {
  SIDCertificatesState,
  SIDCertificate,
  SIDCSR,
  CertificateCommonType
} from "./reducer";
import { RouteComponentProps } from "react-router";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { SDModals } from "modules/signing_identities/SIDModule";
import { useSIDIOSCertificatesService } from "./SIDIOSCertificatesService";
import { useSIDCSRService } from "./SIDCSRService";
import { Model } from "@appcircle/core/lib/Model";
import { ReactComponent as NoElements } from "../../icons/cert-empty.svg";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { AddNewFileModalState } from "shared/modal/AddNewFileModalDialog";
import { useGTMService } from "shared/domain/useGTMService";

type SIDIOSCertificatesProps = {};
class CerticatesModel extends Model<
  SIDCertificatesState,
  CertificateCommonType
> {
  private model: CertificateCommonType[] = [];
  protected init(): void {}
  constructor(state: SIDCertificatesState) {
    super(state);
    this.model = [...this.state.certificates, ...this.state.csr];
  }
  findById(id: string) {
    return this.model.find(item => item.id === id);
  }
  search(text: string) {
    return text
      ? this.model.filter(item => item.name.indexOf(text) > -1)
      : this.model;
  }
  getAll() {
    return this.model;
  }
  length(): number {
    return this.model.length;
  }
  isLoaded(): boolean {
    throw new Error("Method not implemented.");
  }
}

export function SIDIOSCertificates(
  props: RouteComponentProps & SIDIOSCertificatesProps
) {
  const { triggerGTMEvent } = useGTMService();
  const { state } = useSIDModuleContext();
  // const crtModel = useCRTMod();
  // const csrModel = useSIDIOSCertificatesModel();
  const model = new CerticatesModel(state.certificates);
  // useMemo<CerticatesModel>(
  //   () => ,
  //   [state]
  // );
  const gridHeader = useMemo(
    () => ({
      name: "Name",
      appID: "App ID",
      certificate: "Certificate",
      expires: "Expires"
    }),
    []
  );

  const context = useSIDModuleContext();

  const onAddElement = useCallback(() => {
    triggerGTMEvent("signing_ioscer_new");
    props.history.push(
      props.history.createHref({
        ...props.location,
        search: context.createModalUrl(SDModals.SDIOSCertificateUploadWizard),
        state: {}
      })
    );
  }, [context, props.history, props.location]);
  const cerService = useSIDIOSCertificatesService();
  const csrService = useSIDCSRService();
  const onDelete = useCallback(
    data => {
      // service.delete(data.id);
      const modal =
        data instanceof SIDCertificate
          ? SDModals.CertificateDeleteDialog
          : SDModals.CSRDeleteDialog;
      // else if (data instanceof SIDCSR) csrService.delete(data.id);
      props.history.push(
        props.history.createHref({
          pathname: props.location.pathname,
          search:
            context.createModalUrl(modal) +
            "&id=" +
            data.id +
            "&name=" +
            data.name
        })
      );
    },
    [props.history, context, props.location]
  );

  const onDownload = useCallback(
    data => {
      if (data instanceof SIDCertificate)
        cerService.download(data.id, data.name);
      else if (data instanceof SIDCSR) csrService.downloadCSR(data.id);
    },
    [cerService, csrService]
  );
  const taskManager = useTaskManager();
  const taskStatus = taskManager.getStatusByEntityID("sid/certificates/update");
  const inProgress =
    taskStatus === undefined || taskStatus.status !== "COMPLETED";
  const intl = useIntl();
  return (
    <SearchLayout<SIDCertificatesState, CertificateCommonType>
      header={intl.formatMessage(messages.SIDPageCertificates)}
      noElementsIcon={NoElements}
      noElementsText={intl.formatMessage(messages.SIDCertsNoCertificates)}
      model={model}
      isLoaded={!inProgress}
      onAddElement={onAddElement}
    >
      {data => (
        <SIDCertificatesDataTable
          onDelete={onDelete}
          onDownload={onDownload}
          onUpload={data => {
            props.history.push(
              props.history.createHref({
                ...props.location,
                search: context.createModalUrl(SDModals.FileUploadDialog)
              }),
              {
                key: "binary",
                endpoint: "signing-identity/v1/csr/" + data.id,
                icons: ["cer"],
                fileTypes: ["cer"],
                text: intl.formatMessage(messages.SIDPageCSRCertificates)
              } as AddNewFileModalState
            );
          }}
          gridData={data}
          gridHeaders={gridHeader}
          inProgress={inProgress}
          // header={header}
          fallback={
            <div className="ProfileGroups_notesters ProfileDetail_notesters">
              <span>{intl.formatMessage(messages.SIDCertsNoCertificates)}</span>
            </div>
          }
        />
      )}
    </SearchLayout>
  );
}
