import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useMemo } from "react";
import { SIDCSRType, SIDCSR } from "./reducer";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { save } from "@appcircle/core/lib/save";
import { readFilename } from "@appcircle/core/lib/utils/readContentDisposition";
import { Result } from "@appcircle/core/lib/services/Result";

const baseUrl = "signing-identity/v1/";

export function createSIDCSR(element: SIDCSRType) {
  return new SIDCSR(element);
}

export function useSIDCSRService() {
  const moduleContext = useSIDModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const serviceDispatch = useMemo(() => createApiConsumer(), [
    createApiConsumer
  ]);
  const intl = useIntl();
  const services = useMemo(
    () => ({
      update() {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceDispatch({
            method: "GET",
            endpoint: baseUrl + `csr`
          }),
          {
            onSuccess: result => {
              // pushMessage({
              //   messageType: "success",
              //   message: intl.formatMessage(messages.SIDCertsCSRUpdateSuccess)
              // });
              moduleContext.dispatch({
                type: "sid/csr/update",
                elements: (result as Result).json.map(createSIDCSR) as SIDCSR[]
              });
            }
          }
        );
        taskManager.addTask(task, 1);
      },
      delete(id: string) {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceDispatch({
            method: "DELETE",
            endpoint: `${baseUrl}csr/${id}`
          }),
          {
            successMessage: () =>
              intl.formatMessage(messages.SIDCertsCSRDeletionSuccess),
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsCSRDeletionError),
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/csr/delete",
                payload: id
              });
            }
          }
        );

        task.entityID = id;
        taskManager.addTask(task, 1);
      },
      upload() {},
      downloadCSR(id: string) {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceDispatch({
            method: "GET",
            endpoint: `${baseUrl}csr/${id}`,
            responseType: "arraybuffer"
          }),
          {
            successMessage: () =>
              intl.formatMessage(messages.SIDCertsCSRDownloadSuccess),
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsCSRDownloadError),
            onSuccess: result => {
              save((result as Result).json, [
                readFilename((result as Result).headers),
                "dowload.csr"
              ]);
            }
          }
        );

        task.entityID = id;
        taskManager.addTask(task, 1);
      }
    }),
    [intl, moduleContext, serviceDispatch, taskManager]
  );

  return services;
}
