import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import { SIDCertificatesState, SIDCSRType } from "./reducer";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";

export class SIDCSRModel extends Model<SIDCertificatesState, SIDCSRType>
  implements ISearchable, ModelStateBase {
  protected init(): void {}
  length(): number {
    throw new Error("Method not implemented.");
  }
  isLoaded(): boolean {
    return true;
  }
  search(text: string): any[] {
    return [];
  }
  findById(id: string) {
    return undefined;
  }
  getAll() {
    return this.state.csr;
  }
}

export function useCSRModel(): SIDCSRModel {
  const { state } = useSIDModuleContext();
  const model = useMemo(() => new SIDCSRModel(state.certificates), [state]);

  return model;
}
