import { useSIDModuleContext } from "modules/signing_identities/SIDContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useMemo } from "react";
import { SIDCSRType, SIDCertificate, SIDCertificateType } from "./reducer";
import { useIntl } from "react-intl";
import messages from "modules/signing_identities/messages";
import { MODULE_API_ROOT } from "modules/signing_identities/config";
import { save } from "@appcircle/core/lib/save";
import { readFilename } from "@appcircle/core/lib/utils/readContentDisposition";
import { Result } from "@appcircle/core/lib/services/Result";

export function createSIDCertificate(element: SIDCertificateType) {
  return new SIDCertificate(element);
}

export function useSIDIOSCertificatesService() {
  const moduleContext = useSIDModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const serviceCreate = useMemo(() => createApiConsumer(), [createApiConsumer]);
  const intl = useIntl();

  const services = useMemo(
    () => ({
      update() {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceCreate({
            method: "GET",
            endpoint: MODULE_API_ROOT + `certificates`
          }),
          {
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsUpdateError),
            // successMessage: () =>
            //   intl.formatMessage(messages.SIDCertsUpdateSuccess),
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/certificates/update",
                elements: (result as Result).json.map(createSIDCertificate)
              });
            }
          }
        );
        task.entityID = "sid/certificates/update";
        taskManager.addTask(task, 1);
      },
      delete(id: string) {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceCreate({
            method: "DELETE",
            endpoint: `${MODULE_API_ROOT}certificates/${id}`
          }),
          {
            successMessage: () =>
              intl.formatMessage(messages.SIDCertsDeletionSuccess),
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsDeletionError),
            onSuccess: result => {
              moduleContext.dispatch({
                type: "sid/certificates/delete",
                payload: id
              });
            }
          }
        );
        task.entityID = id;
        taskManager.addTask(task, 1);
      },
      download(id: string, name: string = "download") {
        const task = taskManager.createApiTask<SIDCSRType>(
          serviceCreate({
            method: "GET",
            endpoint: `${MODULE_API_ROOT}certificates/${id}`,
            responseType: "arraybuffer"
          }),
          {
            successMessage: () =>
              intl.formatMessage(messages.SIDCertsDownloadSuccess),
            errorMessage: () =>
              intl.formatMessage(messages.SIDCertsDownloadError),
            onSuccess: result => {
              save((result as Result).json, [
                readFilename((result as Result).headers),
                name + ".p12"
              ]);
            }
          }
        );

        task.entityID = id;
        taskManager.addTask(task, 1);
      }
    }),
    [intl, moduleContext, serviceCreate, taskManager]
  );

  return services;
}
