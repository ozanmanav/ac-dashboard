import { useMemo } from "react";
import { ISearchable, Model, ModelStateBase } from "@appcircle/core/lib/Model";
import { SIDCertificatesState, SIDCertificateType } from "./reducer";
import { useSIDModuleContext } from "modules/signing_identities/SIDContext";

export class SIDIOSCertificatesModel
  extends Model<SIDCertificatesState, SIDCertificateType>
  implements ISearchable, ModelStateBase {
  protected init(): void { }
  length(): number {
    throw new Error("Method is not implemented.");
  }
  isLoaded(): boolean {
    return true;
  }
  search(text: string): any[] {
    return [];
  }
  findById(id: string) {
    return undefined;
  }
  getAll() {
    return this.state.certificates;
  }
}

export function useSIDIOSCertificatesModel(): SIDIOSCertificatesModel {
  const { state } = useSIDModuleContext();
  const model = useMemo(() => new SIDIOSCertificatesModel(state.certificates), [
    state
  ]);

  return model;
}
