import produce, { immerable } from "immer";
import { StateMutator } from "@appcircle/core/lib/Mutator";

const initialState = {
  csr: [],
  csrByID: {},
  certificates: [],
  certificatesByID: {}
};
export type SIDCSRType = {
  id: string;
  organizationId: string;
  name: string;
  email: string;
  countryCode: string;
  file: string;
  type?: number;
  createDate: string;
  expireDate: string;
};

export type SIDCertificateType = {
  id: string;
  organizationId: string;
  name: string;
  type: number;
  p12Password: string;
  p12Binary: string;
  thumbprint: string;
  expireDate: string;
  createDate: string;
  updateDate: string;
};

export class SIDCertificate {
  [immerable] = true;
  public id: string = "";
  public organizationId: string = "";
  public name: string = "";
  public type: number = 1;
  public p12Password: string = "";
  public p12Binary: string = "";
  public thumbprint: string = "";
  public expireDate: string = "";
  public createDate: string = "";
  public updateDate: string = "";
  constructor(props: SIDCertificate | SIDCertificateType) {
    Object.assign(this, props);
  }
}

export class SIDCSR {
  [immerable] = true;
  public id: string = "";
  public organizationId: string = "";
  public name: string = "";
  public email: string = "";
  public countryCode: string = "";
  public file: string = "";
  public createDate: string = "";
  public expireDate: string = "";
  constructor(props: SIDCSR | SIDCSRType) {
    Object.assign(this, props);
  }
}

export type SIDCertificatesState = {
  certificates: SIDCertificateType[];
  certificatesByID: { [id: string]: number };
  csr: SIDCSR[];
  csrByID: { [id: string]: number };
};
export type CertificateCommonType = SIDCertificateType | SIDCSRType;

export type SIDCertifacatesActions =
  | {
      type: "sid/certificates/update";
      elements: SIDCertificateType[];
    }
  | { type: "sid/certificates/add"; element: SIDCertificateType }
  | { type: "sid/certificates/delete"; payload: string }
  | {
      type: "sid/csr/update";
      elements: SIDCSR[];
    }
  | {
      type: "sid/csr/delete";
      payload: string;
    }
  | {
      type: "sid/csr/add";
      element: SIDCSR;
    };

function createCertMutator(parent: SIDCertificatesState) {
  return new StateMutator<SIDCertificateType>(parent, "certificates");
}
function createCSRMutator(parent: SIDCertificatesState) {
  return new StateMutator<SIDCSR>(parent, "csr");
}

export function reducer(
  state: SIDCertificatesState = initialState,
  action: SIDCertifacatesActions
) {
  return produce(state, draft => {
    switch (action.type) {
      case "sid/certificates/update":
        draft.certificates = action.elements;
        createCertMutator(draft).invalidateIndexesByKey();
        break;
      case "sid/certificates/add":
        createCertMutator(draft).unshift(action.element);
        break;
      case "sid/certificates/delete":
        createCertMutator(draft).delete(action.payload);
        break;
      case "sid/csr/update":
        draft.csr = action.elements;
        createCSRMutator(draft).invalidateIndexesByKey();
        break;
      case "sid/csr/add":
        createCSRMutator(draft).unshift(action.element);
        break;
      case "sid/csr/delete":
        createCSRMutator(draft).delete(action.payload);
        break;
      default:
        break;
    }
  });
}
