import React, { useEffect, Suspense } from "react";
import { Switch, Route } from "react-router";
import { SIDIOSCertificates } from "./pages/ios-certificates/SIDIOSCertificates";
import { useSIDModuleContext } from "./SIDContext";
import { useSIDIOSCertificatesService } from "./pages/ios-certificates/SIDIOSCertificatesService";
import { SIDIOS_PPRWrapper } from "./pages/ios-provisioning-profile/SIDIOS_PPR";
import { useSIDPPRService } from "./pages/ios-provisioning-profile/SIDIOS_PPRService";
import { useSIDCSRService } from "./pages/ios-certificates/SIDCSRService";
import { SIDKeystore } from "./pages/android-keystore/SIDKeystore";
import { useSIDKeystoreService } from "./pages/android-keystore/SIDKeyStoreService";
import { ModalRoute } from "@appcircle/shared/lib/helpers/modal/ModalRoute";
import { SIDModals, SIDModulePathPrefix } from "./SIDModule";
import { useIntl } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
const StartGuide = React.lazy(() => import("./pages/start-guide"));

export function SigningIdentities() {
  const { createLinkUrl, changeModuleNavSelectedIndex } = useSIDModuleContext();
  const certificatesService = useSIDIOSCertificatesService();
  const csrService = useSIDCSRService();
  const pprService = useSIDPPRService();
  const keystoreService = useSIDKeystoreService();
  useEffect(() => {
    // initializes module state
    certificatesService.update();
    csrService.update();
    pprService.update();
    keystoreService.update();
    // eslint-disable-next-line
  }, []);
  const intl = useIntl();

  return (
    <div className="TestingDistribution">
      <ModalRoute
        prefix={SIDModulePathPrefix}
        modalRoutes={SIDModals}
      ></ModalRoute>
      <Switch>
        <Route
          path={createLinkUrl("/ios-provisionings")}
          render={props => {
            changeModuleNavSelectedIndex(1);
            return <SIDIOS_PPRWrapper />;
          }}
        />

        <Route
          path={createLinkUrl("/android-keystores")}
          render={props => {
            changeModuleNavSelectedIndex(2);
            return <SIDKeystore {...props} />;
          }}
        />

        <Route
          path={createLinkUrl("/start-guide")}
          render={({ match }) => {
            changeModuleNavSelectedIndex(3);
            return (
              <Suspense
                fallback={intl.formatMessage(CommonMessages.appCommonLoading)}
              >
                <StartGuide />
              </Suspense>
            );
          }}
        />
        <Route
          path={createLinkUrl("(/ios-certificates|)")}
          render={props => {
            changeModuleNavSelectedIndex(0);
            return <SIDIOSCertificates {...props} />;
          }}
        />

        {/* <Route
            path={createLinkUrl("/groups")}
            render={match => <ProfileGroups />}
          />
          <Route
            exact={true}
            path={createLinkUrl("(/profiles|)")}
            render={match => <DistributionProfiles />}
          />
          <Route
            path={""}
            render={match => (
              <div className="EmptyPageContent">
                <div>
                  {" "}
                  {match.location.pathname} {"\n\n\nComing Soon..."}
                </div>
              </div>
            )}
          /> */}
      </Switch>
    </div>
  );
}
