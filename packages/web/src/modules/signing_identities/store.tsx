import { createStore, combineReducers } from "redux";
import {
  reducer as certificatesReducer,
  SIDCertifacatesActions,
  SIDCertificatesState
} from "./pages/ios-certificates/reducer";
import {
  reducer as pprReducer,
  IOSPPRState,
  SIDPPRActions
} from "./pages/ios-provisioning-profile/reducer";
import {
  reducer as keystoreReducer,
  SIDKeystoreActions,
  SIDKeystoreState
} from "./pages/android-keystore/reducer";

export type SIDStoreState = {
  certificates: SIDCertificatesState;
  pprs: IOSPPRState;
  keystores: SIDKeystoreState;
};

export type SIDStoreActions =
  | SIDCertifacatesActions
  | SIDPPRActions
  | SIDKeystoreActions;

export const SIDStore = createStore<SIDStoreState, SIDStoreActions, {}, {}>(
  combineReducers({
    certificates: certificatesReducer,
    pprs: pprReducer,
    keystores: keystoreReducer
  })
);

export type SIDStoreType = typeof SIDStore;
