import React, { Context } from "react";
import { createContext, useContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";
import {
  ModuleContextValueType,
  createModuleContextProviderValue,
  ModuleContextProviderPropsType
} from "@appcircle/shared/lib/ModuleContext";
import { SIDStoreType } from "./store";
import { useAppModuleContext } from "AppModuleContext";

export type SIDContextType = ModuleContext & {};
export const SIDContext: Context<ModuleContextValueType<
  SIDStoreType
>> = createContext({
  ...InitialModuleContext
} as any);

export function useSIDAppContext() {
  const context = useAppModuleContext<SIDStoreType, SIDContextType>();
  return context.value;
}

export function useSIDModuleContext() {
  const value = useContext(SIDContext);
  return value;
}

export function SIDContextProvider(
  props: ModuleContextProviderPropsType<ModuleContext, SIDStoreType>
) {
  const value = useContextValue({ context: props.context, store: props.store });

  return (
    <SIDContext.Provider value={value}>{props.children}</SIDContext.Provider>
  );
}

const useContextValue = createModuleContextProviderValue<SIDStoreType>();
