import { useContext } from "react";
import { EnterpriseAppStoreContext } from "./EnterpriseAppStoreContext";
import React from "react";
import { EmptyModule } from "../EmptyModule";

export function EnterpriseAppStore(props: any) {
  const { createLinkUrl } = useContext(EnterpriseAppStoreContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
