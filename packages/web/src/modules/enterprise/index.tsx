import React, { useEffect } from "react";
import { EnterpriseAppStore } from "./EnterpriseAppStore";
import { EnterpriseAppStoreContext } from "./EnterpriseAppStoreContext";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";

const enterpriseAppStoreModuleNavBarData: ModuleNavBarDataType = {
  header: "Enterprise App Store",
  items: [
    {
      text: "Publish",
      link: "/publish"
    },
    {
      text: "Groups",
      link: "/groups"
    },
    {
      text: "Reports",
      link: "/reports"
    },
    {
      text: "Start Guide",
      link: "/startGuide"
    }
  ],
  selectedItemIndex: 0
};

export function EnterpriseAppStoreModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted EnterpriseAppStore.");
    props.context.createModuleNavBar(enterpriseAppStoreModuleNavBarData);
    return () => console.log("Unmounting EnterpriseAppStore ...");
  }, []);
  return (
    <EnterpriseAppStoreContext.Provider value={props.context}>
      <EnterpriseAppStore navbarData={enterpriseAppStoreModuleNavBarData} />
    </EnterpriseAppStoreContext.Provider>
  );
}
