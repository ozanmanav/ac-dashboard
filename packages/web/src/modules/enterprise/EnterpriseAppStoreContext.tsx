import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type EnterpriseAppStoreContextType = ModuleContext & {};

export const EnterpriseAppStoreContext = createContext<
  EnterpriseAppStoreContextType
>({
  ...InitialModuleContext
});
