import { createContext } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

type AppStoreContextType = ModuleContext & {};

export const AppStoreContext = createContext<AppStoreContextType>({
  ...InitialModuleContext
});
