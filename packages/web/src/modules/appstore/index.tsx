import React, { useEffect } from "react";
import { AppStore } from "./AppStore";
import { AppStoreContext } from "./AppStoreContext";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";

const appStoreModuleNavBarData: ModuleNavBarDataType = {
  header: "App Store",
  items: [
    {
      text: "Publish",
      isSelected: false,
      link: "/publish"
    },
    {
      text: "Groups",
      isSelected: false,
      link: "/groups"
    },
    {
      text: "Reports",
      isSelected: true,
      link: "/reports"
    },
    {
      text: "Start Guide",
      isSelected: false,
      link: "/startGuide"
    }
  ],
  selectedItemIndex: 0
};

export function AppStoreModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted AppStore.");
    props.context.createModuleNavBar(appStoreModuleNavBarData);
    return () => console.log("Unmounting AppStore ...");
  }, []);
  return (
    <AppStoreContext.Provider value={props.context}>
      <AppStore navbarData={appStoreModuleNavBarData} />
    </AppStoreContext.Provider>
  );
}
