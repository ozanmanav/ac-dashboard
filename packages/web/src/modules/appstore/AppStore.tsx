import { useContext } from "react";
import { AppStoreContext } from "./AppStoreContext";
import React from "react";
import { EmptyModule } from "../EmptyModule";

export function AppStore(props: any) {
  const { createLinkUrl } = useContext(AppStoreContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
