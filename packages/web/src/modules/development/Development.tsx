import { useContext } from "react";
import { DevelopmentContext } from "./DevelopmentContext";
import React from "react";
import { EmptyModule } from "../EmptyModule";

export function Development(props: any) {
  const { createLinkUrl } = useContext(DevelopmentContext);
  return (
    <EmptyModule navbarData={props.navbarData} createLinkUrl={createLinkUrl} />
  );
}
