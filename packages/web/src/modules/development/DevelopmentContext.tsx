import { createContext } from "react";
import { ModuleContext, InitialModuleContext } from "@appcircle/shared/lib/Module";

type DevelopmentContextType = ModuleContext & {};

export const DevelopmentContext = createContext<DevelopmentContextType>({
	...InitialModuleContext
});
