import React, { useEffect } from "react";
import { Development } from "./Development";
import { DevelopmentContext } from "./DevelopmentContext";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";

const developmentModuleNavBarData: ModuleNavBarDataType = {
  header: "Development",
  items: [
    {
      text: "Cloud IDE",
      link: "/cloud_ide"
    },
    {
      text: "Groups",
      link: "/groups"
    },
    {
      text: "Reports",
      link: "/reports"
    },
    {
      text: "Start Guide",
      link: "/startGuide"
    }
  ],
  selectedItemIndex: 0
};

export function DevelopmentModule(props: ModuleProps) {
  useEffect(() => {
    console.log("Mounted Development.");
    props.context.createModuleNavBar(developmentModuleNavBarData);
    return () => console.log("Unmounting Development ...");
  }, []);
  return (
    <DevelopmentContext.Provider value={{ ...props.context }}>
      <Development navbarData={developmentModuleNavBarData} />
    </DevelopmentContext.Provider>
  );
}
