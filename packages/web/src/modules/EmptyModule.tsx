import { Switch, Route } from "react-router";
import React from "react";
import { SvgWorkingIcon } from "@appcircle/assets/lib/WorkingIcon";
const style = {
  fontSize: "14px",
  color: "#cdd6e1"
};
export function EmptyModule(props: any) {
  const { createLinkUrl, navbarData } = props;
  return (
    <div className="EmptyPageContent">
      <Switch>
        {props.children}
        {navbarData &&
          navbarData.items &&
          navbarData.items.map((item: any) => (
            <Route
              path={createLinkUrl(item.link)}
              key={item.text}
              render={match => (
                <div style={style}>
                  <SvgWorkingIcon />
                  {"\n\nWe’re working on this feature."}
                </div>
              )}
            />
          ))}
        <Route
          path=""
          key={
            navbarData &&
            navbarData.items[props.navbarData.selectedItemIndex].text
          }
          render={match => (
            <div style={style}>
              <SvgWorkingIcon />
              {"\n\nWe’re working on this feature."}
            </div>
          )}
        />
      </Switch>
    </div>
  );
}
