import { createContext, Context } from "react";
import {
  ModuleContext,
  InitialModuleContext
} from "@appcircle/shared/lib/Module";

export type ProfileContext = ModuleContext & {
  profiles: object[];
};

export const ProfileContext = createContext<ProfileContext>({
  ...InitialModuleContext,
  profiles: []
});
