import { Profile } from "./Profile";
import { ProfileContext } from "./ProfileContext";
import React, { useState } from "react";
import {
  ModuleContext,
  ModuleNavBarDataType,
  ModuleProps
} from "@appcircle/shared/lib/Module";
export type ProfileModuleProps = ModuleProps;

function createTestingModuleNavBarData(rootUrl: string): ModuleNavBarDataType {
  return {
    header: "Testing Distribution",
    items: [
      {
        text: "Profiles",
        link: "/profiles"
      },
      {
        text: "Groups",
        link: "/groups"
      },
      {
        text: "Reports",
        link: "/reports"
      },
      {
        text: "Start Guide",
        link: "/startGuide"
      }
    ],
    selectedItemIndex: 0
  };
}

export function ProfileModule(props: ProfileModuleProps) {
  const [profiles, setProfiles] = useState<object[]>([]);
  return (
    <ProfileContext.Provider value={{ ...props.context, profiles }}>
      <Profile />
    </ProfileContext.Provider>
  );
}
