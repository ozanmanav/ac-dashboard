import React, { useState, useContext, FormEvent } from "react";
import { MaterialTextBox } from "@appcircle/ui/lib/MaterialTextBox";

import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { AppStateType } from "../../reducers";
import { AuthServiceContext } from "./service/AuthService";
import { Message } from "@appcircle/shared/lib/Module";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import {
  SubmitButton,
  SubmitButtonState
} from "@appcircle/ui/lib/SubmitButton";
import { isValidEmail } from "@appcircle/core/lib/utils/isValidEmail";
import { AuthLayout } from "./AuthLayout";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useIntl, IntlShape } from "react-intl";
import messages from "./messages";
import { AuthTokenType } from "@appcircle/shared/lib/services/AuthTokenType";
import { useGTMService } from "shared/domain/useGTMService";

type submitFn = (payload: { email: string; pass: string }) => void;

const LoginComp = (props: {
  isLoggedIn: boolean;
  submit: submitFn;
  state?: any;
  routerState?: any;
}) => {
  useGTMService({ triggerEvent: "login" });

  const { service } = useContext(AuthServiceContext);
  const { pushMessage } = useNotificationsService();
  const _loginData = props.routerState || {};
  const [inputs, setInputs] = useState({
    username: _loginData.email || "",
    password: _loginData.pass || "",
    isSubmit: false,
    renmemberMe: false
  });
  const [validState, setValidState] = useState({
    username: true,
    password: true
  });
  const intl = useIntl();

  return props.isLoggedIn ? (
    <Redirect to="/" />
  ) : (
    <AuthLayout
      title={intl.formatMessage(messages.loginTitle)}
      subtitle={intl.formatMessage(messages.loginSubtitle)}
      subtitleLink={
        <Link
          tabIndex={-1}
          className="LoginForm_main_box_subtitle_link"
          to={"/signup"}
          data-testid="to-signup"
        >
          {intl.formatMessage(messages.loginSignup)}
        </Link>
      }
    >
      {/* TODO : Form component and give error messages in the material-input */}
      <form
        autoComplete="off"
        onSubmit={(e: FormEvent) => {
          e.preventDefault();
          setInputs({ ...inputs, isSubmit: true });
          // handleLoginSubmit(inputs, service, pushMessage);
          let { username, password } = inputs;
          let _validState = {
            username: true,
            password: true
          };
          if (!username || !isValidEmail(username)) {
            _validState.username = false;
          } else if (!password) {
            _validState.password = false;
          } else if (password.length < 8) {
            _validState.password = false;
            // This is not included in the i18n messages
            pushMessage({
              message: "Passwords must be at least 8 characters",
              messageType: "error",
              life: 3000
            });
          }

          if (!_validState.password || !_validState.username) {
            setInputs({ ...inputs, isSubmit: false });
            return setValidState({
              password: _validState.password,
              username: _validState.username
            });
          }
          service.login(inputs, {
            ...createLoginTaskProps(pushMessage, intl),
            onError: error => {
              setInputs({ ...inputs, isSubmit: false });
            }
          });
        }}
      >
        <div className="LoginForm_main_box_input">
          <MaterialTextBox
            autoComplete="off"
            tabIndex={1}
            isValid={validState.username}
            isDisabled={inputs.isSubmit}
            key="username"
            onChange={value => {
              setInputs({ ...inputs, username: value });
            }}
            label={intl.formatMessage(messages.appCommonEmail)}
            text=""
          />
          <div className="LoginForm_main_box_input_separator" />
          <MaterialTextBox
            autoComplete="off"
            tabIndex={2}
            isValid={validState.password}
            isDisabled={inputs.isSubmit}
            type="password"
            key="password"
            onChange={value => {
              setInputs({ ...inputs, password: value });
            }}
            label={intl.formatMessage(messages.appCommonPassword)}
            text=""
          />
        </div>
        <div className="LoginForm_formTools">
          {/*  <div className="LoginForm_main_box_formfooter_leftItem">
              <CheckBox
                check={inputs.renmemberMe}
                onChange={(status: boolean) => {
                  setInputs({ ...inputs, renmemberMe: status });
                }}
              />
              <div className="LoginForm_main_box_formfooter_leftItem_text">
                {language.rememberMe}
              </div>
            </div>*/}
          <div className="LoginForm_main_box_formfooter_rightItem">
            <Link tabIndex={-1} to="/forgotPassword">
              {intl.formatMessage(messages.loginForgotPassword)}
            </Link>
          </div>
        </div>
        <div className="LoginForm_formFooter">
          <LoginButton state={detectButtonState(inputs)} />
        </div>
        {/* <div className="LoginForm_main_box_continueText">
          {language.continueWith}
        </div>
        <IconButton icon={icons.github}>{language.signWithGithub}</IconButton> */}
      </form>
    </AuthLayout>
  );
};

function detectButtonState(state: LoginDataType): SubmitButtonState {
  if (state.username && state.password) {
    if (state.isSubmit) return SubmitButtonState.INPROGRESS;
    return SubmitButtonState.ENABLED;
  }
  return SubmitButtonState.DISABLED;
}

function createLoginTaskProps(
  pushMessage: (message: Message) => void,
  intl: IntlShape
): TaskPropsType<AuthTokenType> {
  return {
    onSuccess: result => {},
    successMessage: result => intl.formatMessage(messages.loginSuccess),
    errorMessage: (result: any) =>
      intl.formatMessage(messages.loginError, {
        error: `${result.status} ${result.statusText}`
      }),
    retryCount: 3
  };
}

export type LoginDataType = {
  username: string;
  password: string;
  isSubmit: boolean;
  renmemberMe: boolean;
};

const LoginButton = (props: { state: SubmitButtonState }) => {
  const intl = useIntl();
  return (
    <React.Fragment>
      <div className="LoginForm_main_submit">
        <SubmitButton
          tabIndex={3}
          state={props.state}
          inProgressText={intl.formatMessage(
            messages.appSubmitButtonLoginProgress
          )}
        >
          {intl.formatMessage(messages.appSubmitButtonLogin)}
        </SubmitButton>
      </div>
    </React.Fragment>
  );
};

export const Login = connect(
  (state: AppStateType) => {
    return {
      isLoggedIn: state.login.isLoggedIn
    };
  },
  (dispatch): { submit: submitFn } => {
    return {
      submit(payload: any) {
        dispatch({ type: "LOG_IN_REQUEST" });
      }
    };
  }
)(LoginComp);
