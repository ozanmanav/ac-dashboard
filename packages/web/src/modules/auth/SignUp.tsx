import React, { useState, useContext, useEffect } from "react";
import {
  MaterialTextBox,
  MaterialTextBoxProps
} from "@appcircle/ui/lib/MaterialTextBox";

import { Link, Redirect } from "react-router-dom";
import { AuthServiceContext } from "./service/AuthService";
import { SubmitButton } from "@appcircle/ui/lib/SubmitButton";
import { AjaxError } from "rxjs/ajax";
import { AuthLayout } from "./AuthLayout";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { PasswordRegexp } from "@appcircle/core/lib/patterns";
import { useIntl } from "react-intl";
import messages from "./messages";
import { Form, FormStatus } from "@appcircle/form/lib/Form";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { useGTMService } from "shared/domain/useGTMService";

export function SignUp() {
  useGTMService({ triggerEvent: "sign_up" });

  const [isSignedUp, setSignedUp] = useState(false);
  const { service } = useContext(AuthServiceContext);
  const { pushMessage } = useNotificationsService();
  const intl = useIntl();

  return isSignedUp ? (
    <Redirect
      to={{
        pathname: "/",
        state: {
          //fast login => pass user state
        }
      }}
    />
  ) : (
    <AuthLayout
      title={intl.formatMessage(messages.signupTitle)}
      subtitle={intl.formatMessage(messages.signupSubtitle)}
      subtitleLink={
        <Link className="LoginForm_main_box_subtitle_link" to={"/login"}>
          {intl.formatMessage(messages.signupLogin)}
        </Link>
      }
    >
      <Form
        className="SignUp"
        formID="sign-up-form"
        taskFactory={dispatch =>
          service.createSignUpTask({
            onSuccess: result => {
              setSignedUp(true);
            },
            successMessage: result =>
              intl.formatMessage(messages.signupSuccess),
            onError: e => {
              dispatch({
                type: "UpdateFormStatus",
                id: "sign-up-form",
                status: {
                  submission: FormStatus.DONE_WITH_ERROR
                }
              });
              if (e instanceof AjaxError && e.status === 401) {
                pushMessage({
                  message: intl.formatMessage(messages.signupErrorExisting),
                  messageType: "error",
                  life: 3000
                });
              }
            },
            errorMessage: result =>
              intl.formatMessage(messages.signupError, {
                status: result.status,
                statusText: result.statusText
              }),
            retryCount: 3
          })
        }
      >
        <div className="LoginForm_main_box_input">
          <FormComponent<MaterialTextBoxProps>
            autoComplete="off"
            // isValid={validState.email}
            key="email"
            tabIndex={1}
            label={intl.formatMessage(messages.appCommonEmail)}
            type="email"
            ariaLabel={"email"}
            element={MaterialTextBox}
            formID="sign-up-form"
            formProps={{
              name: "email",
              required: true,
              rule: {
                type: "email"
              }
            }}
          />
          <FormComponent<MaterialTextBoxProps>
            type="password"
            autoComplete="off"
            key="password"
            tabIndex={2}
            label={intl.formatMessage(messages.appCommonPassword)}
            element={MaterialTextBox}
            formID="sign-up-form"
            formProps={{
              name: "password",
              required: true,
              rule: [
                {
                  type: "string-min",
                  min: 8,
                  errorMessage: intl.formatMessage(
                    messages.signupErrorMinCharRule
                  )
                },
                {
                  type: "regex",
                  testPattern: PasswordRegexp,
                  errorMessage: intl.formatMessage(
                    messages.signupErrorContainRule
                  )
                },
                {
                  type: "confirm-field",
                  confirmfield: "repass"
                }
              ]
            }}
          />
          <FormComponent<MaterialTextBoxProps>
            type="password"
            autoComplete="off"
            key="repassword"
            tabIndex={3}
            label={intl.formatMessage(messages.appCommonConfirmPassword)}
            element={MaterialTextBox}
            formID="sign-up-form"
            formProps={{
              name: "repass",
              required: true,
              rule: [
                {
                  type: "confirm"
                }
              ]
            }}
          />
          <FormComponent<MaterialTextBoxProps>
            autoComplete="off"
            key="invitationCode"
            tabIndex={4}
            label={"Invitation Code"}
            element={MaterialTextBox}
            formID="sign-up-form"
            formProps={{
              name: "invitationCode",
              required: true
            }}
          />
        </div>
        <div className="LoginForm_formFooter">
          <div className="LoginForm_main_submit">
            <SubmitButton
              tabIndex={5}
              formID="sign-up-form"
              ariaLabel={"signup"}
              inProgressText={intl.formatMessage(
                messages.appSubmitButtonSignupProgress
              )}
            >
              {intl.formatMessage(messages.appSubmitButtonSignup)}
            </SubmitButton>
          </div>
        </div>
      </Form>
    </AuthLayout>
  );
}
