import React from "react";
import { AuthPagesProvider } from "../../services/AuthPages";
import { render, fireEvent } from "testing/utils";

let SignupComponent: any;
beforeEach(() => {
  SignupComponent = render(<AuthPagesProvider />, {
    route: "/signup"
  });
});

describe("Signup", () => {
  // test("Renders the Signup Page", () => {
  //   const { getByTestId, queryByLabelText, queryByTestId } = SignupComponent;
  //   expect(queryByTestId("login-form")).not.toBeInTheDocument();
  //   expect(getByTestId("signup-form")).toBeInTheDocument();
  //   let signupButton = queryByLabelText("signup");
  //   expect(signupButton).toBeInTheDocument();
  // });
  test("Signup button should be disabled on load", () => {
    const { queryByLabelText } = SignupComponent;
    let signupButton = queryByLabelText("signup");
    expect(signupButton).toHaveClass("SubmitButton-disabled");
  });
  test("Signup button disabled when all fields are not filled", () => {
    const { queryByLabelText } = SignupComponent;
    let signupButton = queryByLabelText("signup");
    let emailContainer = queryByLabelText("email");
    let emailField = emailContainer.querySelector('input[type="email"]');
    emailField.value = "test";
    fireEvent.click(signupButton);
    expect(signupButton).toHaveClass("SubmitButton-disabled");
  });

  test("Email field validation on blur", () => {
    const { queryByLabelText } = SignupComponent;
    let emailContainer = queryByLabelText("email");
    let emailField = emailContainer.querySelector('input[type="email"]');
    emailField.focus();
    emailField.value = "test";
    emailField.blur();
    expect(emailContainer).toHaveClass("mdc-text-field--invalid");

    emailField.focus();
    emailField.value = "test@test.com";
    emailField.blur();
    expect(emailContainer).not.toHaveClass("mdc-text-field--invalid");
  });
});
