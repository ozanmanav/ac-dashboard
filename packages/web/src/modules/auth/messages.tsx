import { defineMessages } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";

const forgetPassword = "app.modules.auth.forgetPassword.";
const login = "app.modules.auth.login.";
const signup = "app.modules.auth.signup.";
const resetPass = "app.modules.auth.resetPassword.";

export default defineMessages({
  forgetPasswordSuccess: {
    id: forgetPassword + "succcess",
    defaultMessage: "Password reset email sent"
  },
  forgetPasswordError: {
    id: forgetPassword + "error",
    defaultMessage: "An error occured. Please try again momentarily"
  },
  forgetPasswordSubtitle: {
    id: forgetPassword + "subtitle",
    defaultMessage: "Enter the email address associated with your account"
  },
  fortgotPasswordTitle: {
    id: forgetPassword + "title",
    defaultMessage: "Forgot Password?"
  },
  loginTitle: {
    id: login + "title",
    defaultMessage: "Log in"
  },
  loginSubtitle: {
    id: login + "subtitle",
    defaultMessage: "New to Appcircle?"
  },
  loginSignup: {
    id: login + "signup",
    defaultMessage: "Sign Up"
  },
  loginForgotPassword: {
    id: login + "forgotPassword",
    defaultMessage: "Forgot Password?"
  },
  loginSuccess: {
    id: login + "success",
    defaultMessage: "Login successful"
  },
  loginError: {
    id: login + "error",
    defaultMessage: "{error}, please try again"
  },
  signupTitle: {
    id: signup + "title",
    defaultMessage: "Sign up"
  },
  signupSubtitle: {
    id: signup + "subtitle",
    defaultMessage: "Already have an account?"
  },
  signupLogin: {
    id: signup + "signup",
    defaultMessage: "Log in"
  },
  signupForgotPassword: {
    id: signup + "forgotPassword",
    defaultMessage: "Forgot Password?"
  },
  signupSuccess: {
    id: signup + "success",
    defaultMessage: "Sign up complete. Logging in..."
  },
  signupError: {
    id: signup + "error",
    defaultMessage: "{status} {statusText}, please try again"
  },
  signupErrorExisting: {
    id: signup + "error.existingEmail",
    defaultMessage: "This email already exists"
  },
  signupErrorNotMatched: {
    id: signup + "error.notMatched",
    defaultMessage: "The passwords don't match"
  },
  signupErrorContainRule: {
    id: signup + "error.containRule",
    defaultMessage:
      "The password must contain at least one each letter and one number"
  },
  signupErrorContainSpecialCharsRule: {
    id: signup + "error.containSpecialRule",
    defaultMessage:
      "The password must contain at least one each letter and number"
  },
  signupErrorMinCharRule: {
    id: signup + "error.minCharRule",
    defaultMessage: "The password must have at least 8 characters"
  },
  signupErrorMinEmailRule: {
    id: signup + "error.emailRule",
    defaultMessage: "Invalid email address"
  },
  signupErrorRequiredFields: {
    id: signup + "error.requiredFields",
    defaultMessage: "Please complete all required fields"
  },
  resetPasswordTitle: {
    id: resetPass + "title",
    defaultMessage: "Reset Password"
  },
  resetPasswordSubtitle: {
    id: resetPass + "subtitle",
    defaultMessage: "Please check your email for a password reset link"
  },
  resetPasswordSuccess: {
    id: resetPass + "success",
    defaultMessage: "Password successfully reset"
  },
  resetPasswordError: {
    id: resetPass + "error",
    defaultMessage: "An error occured. Please try again momentarily"
  },
  resetPasswordLogin: {
    id: resetPass + "login",
    defaultMessage: "Return to login"
  },
  resetPasswordErrorContainRule: {
    id: resetPass + "error.containRule",
    defaultMessage: "Minimum 8 characters with one letter and one number"
  },
  resetPasswordErrorNotConfirmed: {
    id: resetPass + "error.notConfirmed",
    defaultMessage: "Passwords don't match"
  },
  ...CommonMessages
});
