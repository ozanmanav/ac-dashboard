import React from "react";
import { render } from "testing/utils";
import { AuthPagesProvider } from "../../services/AuthPages";

let LoginComponent: any;
beforeEach(() => {
  LoginComponent = render(<AuthPagesProvider />);
});

describe("Login", () => {
  test("Renders the Login Page", () => {
    const { getByTestId } = LoginComponent;
    expect(getByTestId("login-form")).toBeInTheDocument();
  });
  // test("Sends to Signup page", () => {
  //   const { getByTestId, queryByTestId } = LoginComponent;
  //   fireEvent.click(getByTestId("to-signup"));
  //   expect(queryByTestId("login-form")).not.toBeInTheDocument();
  //   expect(getByTestId("signup-form")).toBeInTheDocument();
  // });
});
