import React, { createContext, useMemo, useEffect, useContext } from "react";
import { ServiceProviderPropsType } from "@appcircle/shared/lib/services/createService";
import { concatMap, map, tap, zipAll, mergeMap } from "rxjs/operators";
import { SignUpDataType } from "../SignUpDataType";
import { LoginDataType } from "../Login";
import { AppModalPaths } from "../../../App";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import {
  ApiResultType,
  useTaskManager,
  TaskPropsType
} from "@appcircle/taskmanager/lib/TaskManagerContext";
import { AjaxError } from "rxjs/ajax";
import { useIntl } from "react-intl";
import { __RouterContext } from "react-router";
import { FormTaskFactory } from "@appcircle/form/lib/Form";
import { of } from "rxjs";
import { useCheckUnauthrizedTask } from "@appcircle/taskmanager/lib/TaskManagerProvider";
import { AuthTokenType } from "@appcircle/shared/lib/services/AuthTokenType";
import { UserResponse } from "@appcircle/shared/lib/services/UserResponse";
import { Result } from "@appcircle/core/lib/services/Result";
export function createNonToken() {
  return {
    access_token: "",
    expires_in: -1,
    token_type: ""
  };
}
export type AuthServiceType = {
  login: (data: LoginDataType, props: TaskPropsType) => void;
  createSignUpTask: (props: TaskPropsType) => FormTaskFactory<SignUpDataType>;
} & { logout: () => void };

export const AuthServiceContext = createContext<{
  token: AuthTokenType | null;
  service: AuthServiceType;
}>({
  token: null,
  service: {
    login: () => {
      throw new Error("Not implemented function");
    },
    logout: () => {
      throw new Error("Not implemented function");
    },
    createSignUpTask: () => {
      throw new Error("Not implemented function");
    }
  }
});

const API_CONSTANTS = {
  client_id: "appcircle_ui_client",
  client_secret: "mMh3Bk2kP4JRBzVZAQw9GGyVsT2uj6rBGK2Tu4naFquvcGhXBM"
};

function subscriptionHandler(
  data: ApiResultType<AuthTokenType> | AjaxError,
  onSuccess?: (data: Result<AuthTokenType>) => void,
  onFailiure?: (data: AjaxError) => void
) {
  if (!(data instanceof Error) && (data as Result).status === 200) {
    onSuccess && onSuccess(data as Result);
  } else {
    onFailiure && onFailiure(data as AjaxError);
  }
}

export function AuthServiceProvider(
  props: ServiceProviderPropsType<{ token: AuthTokenType }, AuthServiceType>
) {
  // const { pushMessage } = useNotificationsService();
  const intl = useIntl();
  const {
    token,
    updateToken,
    removeToken,
    createApiConsumer,
    createAuthConsumer
  } = useServiceContext();
  const apiConsumer = createApiConsumer();
  const authConsumer = createAuthConsumer();
  const [hasUnauth, reset] = useCheckUnauthrizedTask();
  const router = useContext(__RouterContext);
  const { createApiTask, addTask } = useTaskManager();
  type AuthApiData<T> = {
    grant_type: string;
    scope: string;
  } & typeof API_CONSTANTS &
    T;

  useEffect(() => {
    hasUnauth &&
      token &&
      (() => {
        router.history.push(
          router.history.createHref({
            pathname: "",
            search: "?modal=" + AppModalPaths.sessionExpired
          })
        );
        const unregister = router.history.block(false);
        setTimeout(() => {
          unregister();
        }, 200);
        reset();
        // removeToken();
      })();
  }, [hasUnauth, removeToken, reset, token, intl, router.history]);
  const service: AuthServiceType = {
    login: (data: LoginDataType, props: TaskPropsType) => {
      const task = createApiTask(
        authConsumer({
          method: "POST",
          endpoint: "connect/token",
          data: {
            ...data,
            grant_type: "password",
            scope:
              "openid profile distribution_server signing_server build_server organization",
            ...API_CONSTANTS
          }
        }),
        {
          ...props,
          pipe: [
            mergeMap(tokenRes => {
              return [
                of(tokenRes),
                authConsumer({
                  method: "GET",
                  endpoint: "connect/userinfo",
                  headers: {
                    Authorization:
                      "Bearer " + (tokenRes as Result).json.access_token,
                    "Content-Type": "application/json"
                  }
                })()
              ];
            }),
            zipAll(),
            map<[Result<any>, Result<UserResponse>], AuthTokenType>(
              ([tokenRes, info]) => {
                return {
                  ...(tokenRes as Result).json,
                  ...(info as Result).json
                } as AuthTokenType;
              }
            ),
            tap<AuthTokenType>(data => {
              // subscriptionHandler(data);
              updateToken(data);
            })
          ]
        }
      );

      task.entityID = "uath/login";
      addTask(task);
    },
    createSignUpTask: props => data => {
      return createApiTask(
        authConsumer({
          method: "POST",
          endpoint: "connect/token",
          data: {
            ...API_CONSTANTS,
            grant_type: "client_credentials"
          }
        }),
        {
          ...props,
          pipe: [
            concatMap(response => {
              return apiConsumer<AuthTokenType>({
                method: "POST",
                endpoint: "v1/signup",
                data,
                headers: {
                  Authorization:
                    "Bearer " + (response as Result).json.access_token,
                  "Content-Type": "application/json"
                }
              })();
            })
          ]
        }
      );
    },
    logout: () => {
      removeToken();
    }
  };

  const value = useMemo(() => ({ service: props.service || service, token }), [
    props.service,
    service,
    token
  ]);
  return (
    <AuthServiceContext.Provider value={value}>
      {props.children}
    </AuthServiceContext.Provider>
  );
}
