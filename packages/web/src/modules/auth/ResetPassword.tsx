import React, { useState } from "react";
import { AuthLayout } from "./AuthLayout";
import { SubmitButton } from "@appcircle/ui/lib/SubmitButton";
import { Link } from "react-router-dom";
import { MaterialTextBox } from "@appcircle/ui/lib/MaterialTextBox";
import {
  Form,
  FormFieldType,
  FormFieldErrorType
} from "@appcircle/form/lib/Form";
import { useIntl } from "react-intl";
import messages from "./messages";
import { PasswordRegexp } from "@appcircle/core/lib/patterns";

export function ResetPassword(props: { data: string }) {
  const parsedData = props.data.split("=");
  const token = parsedData[1].substring(0, parsedData[1].length - 6);
  const email = parsedData[2];
  const [fields, setFields] = useState<FormFieldType[]>(() => [
    {
      name: "newPassword",
      required: true,
      rule: [
        {
          type: "regex",
          testPattern: PasswordRegexp
        },
        {
          type: "confirm-field",
          confirmfield: "confirm-password"
        }
      ]
    },
    {
      name: "confirm-password",
      rule: {
        type: "confirm"
      }
    },
    {
      name: "passwordResetToken",
      value: token
    }
  ]);
  const [errors, setErrors] = useState<FormFieldErrorType[]>([]);
  const intl = useIntl();
  return (
    <AuthLayout
      title={intl.formatMessage(messages.resetPasswordTitle)}
      subtitle={intl.formatMessage(messages.resetPasswordSubtitle)}
    >
      <Form
        targetPath={`account/v1/users/${email}?action=confirmPassword&passwordResetToken=${token}&newPassword=${fields[0].value}`}
        taskProps={{
          successMessage: () =>
            intl.formatMessage(messages.resetPasswordSuccess),
          errorMessage: () => intl.formatMessage(messages.resetPasswordError),
          onSuccess: resp => {}
        }}
        fields={fields}
        formID="forgotpassword-form"
        onFieldsError={errors => {
          setErrors(errors);
        }}
      >
        <div className="LoginForm_main_box_input">
          <MaterialTextBox
            autoComplete="off"
            tabIndex={1}
            isValid={errors.length === 0 || !fields[0].value}
            type="password"
            // isDisabled={inputs.isSubmit}
            onChange={value => {
              setFields([
                {
                  ...fields[0],
                  value
                },
                fields[1]
              ]);
            }}
            label={
              errors.some(error => error.type === "regex") && fields[0].value
                ? intl.formatMessage(messages.signupErrorContainRule)
                : errors.length > 0 && fields[0].value
                ? intl.formatMessage(messages.resetPasswordErrorNotConfirmed)
                : intl.formatMessage(messages.appCommonPassword)
            }
            text={""}
          />
          <MaterialTextBox
            autoComplete="off"
            type="password"
            tabIndex={2}
            isValid={errors.length === 0 || !fields[1].value}
            // isDisabled={inputs.isSubmit}
            onChange={value => {
              setFields([
                fields[0],
                {
                  ...fields[1],
                  value
                }
              ]);
            }}
            label={
              errors.length > 0 && fields[1].value
                ? intl.formatMessage(messages.resetPasswordErrorNotConfirmed)
                : intl.formatMessage(messages.appCommonConfirmPassword)
            }
            text={""}
          />
        </div>
        <div className="LoginForm_formFooter">
          <SubmitButton inProgressText="Sending" formID="forgotpassword-form">
            {intl.formatMessage(messages.appSubmitButtonSend)}
          </SubmitButton>

          <Link className="inFooter" to="/login">
            {intl.formatMessage(messages.resetPasswordLogin)}
          </Link>
        </div>
      </Form>
    </AuthLayout>
  );
}
