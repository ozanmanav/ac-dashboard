import React, { useState } from "react";
import { AuthLayout } from "./AuthLayout";
import { SubmitButton } from "@appcircle/ui/lib/SubmitButton";
import { Link } from "react-router-dom";
import { MaterialTextBox } from "@appcircle/ui/lib/MaterialTextBox";
import { Form, FormFieldType } from "@appcircle/form/lib/Form";
import { useIntl } from "react-intl";
import messages from "./messages";

export function ForgotPassword() {
  const [fields, setFields] = useState<FormFieldType[]>(() => [
    {
      name: "email",
      required: true,
      rule: {
        type: "email"
      }
    }
  ]);
  const intl = useIntl();

  const [hasError, setHasError] = useState(false);
  return (
    <AuthLayout
      title={intl.formatMessage(messages.fortgotPasswordTitle)}
      subtitle={intl.formatMessage(messages.forgetPasswordSubtitle)}
    >
      <Form
        targetPath={`account/v1/users/${fields[0].value}?action=resetPassword`}
        taskProps={{
          successMessage: () =>
            intl.formatMessage(messages.forgetPasswordSuccess),
          errorMessage: () => intl.formatMessage(messages.forgetPasswordError),
          onSuccess: resp => {}
        }}
        fields={fields}
        formID="forgotpassword-form"
        onFieldsError={errors => {
          setHasError(errors.length > 0);
        }}
      >
        <div className="LoginForm_main_box_input">
          <MaterialTextBox
            autoComplete="on"
            tabIndex={1}
            isValid={!hasError || !fields[0].value}
            // isDisabled={inputs.isSubmit}
            onChange={value => {
              setFields([
                {
                  ...fields[0],
                  value
                }
              ]);
            }}
            label={intl.formatMessage(messages.appCommonEmail)}
            text={""}
          />
        </div>
        <div className="LoginForm_formFooter">
          <SubmitButton
            inProgressText={intl.formatMessage(messages.appSubmitButtonSend)}
            formID="forgotpassword-form"
          >
            Send
          </SubmitButton>
          <Link className="inFooter" to="/login">
            Back to login
          </Link>
        </div>
      </Form>
    </AuthLayout>
  );
}
