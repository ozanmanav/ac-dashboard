import React, { PropsWithChildren } from "react";
import { SvgAppcircleLogo } from "icons/AppcircleLogo";
import { SvgIllustration } from "icons/Illustration";
import { SvgAppcircleLogoVertical } from "icons/AppcircleLogoVertical";

export function AuthLayout(
  props: PropsWithChildren<{
    title: string;
    subtitle?: string;
    subtitleLink?: JSX.Element;
  }>
) {
  return (
    <div className="LoginForm" data-testid="login-form">
      <div className="LoginForm_sidebar">
        <SvgAppcircleLogo
          className="LoginForm_sidebar_logo"
          viewBox="0 0 816 169"
        />
        <SvgIllustration
          className="LoginForm_sidebar_advertisement"
          viewBox="0 0 491 618"
        />
        <div className="LoginForm_sidebar_text">
          {"Next\nGeneration\nMobile\nDevelopment"}
        </div>
      </div>
      <div className={"LoginForm_main"}>
        <SvgAppcircleLogoVertical
          className="LoginForm_main_logo"
          viewBox="0 0 609 451"
        />
        <div className="LoginForm_main_box">
          <div className="LoginForm_main_box_title">{props.title}</div>
          <div className="LoginForm_main_box_subtitle">
            <div className="LoginForm_main_box_subtitle_defination">
              {props.subtitle}
            </div>
            {props.subtitleLink}
          </div>
          {props.children}
        </div>
      </div>
    </div>
  );
}
