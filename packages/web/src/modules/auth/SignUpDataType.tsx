export type SignUpDataType = {
  email: string;
  pass: string;
  repass: string;
  username?: string;
  isSubmit?: boolean;
};
