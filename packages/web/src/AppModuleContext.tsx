import {
  createContext,
  useContext,
  useState,
  useMemo,
  useEffect,
  PropsWithChildren
} from "react";
import { ModuleContextValueType } from "@appcircle/shared/lib/ModuleContext";
import { Store, Unsubscribe } from "redux";
import {
  InitialModuleContext,
  ModuleContext
} from "@appcircle/shared/lib/Module";
import React from "react";
import { NotificationProvider } from "@appcircle/shared/lib/notification/NotificationContext";
import { AppContextType } from "./AppContext";
import { NotificationModal } from "modules/notification/NotificationModal";

const AppModuleContext = createContext<{
  value: ModuleContextValueType<Store>;
  setContext: React.Dispatch<any>;
  setStore: React.Dispatch<any>;
}>({
  value: InitialModuleContext,
  setStore: () => {},
  setContext: () => {}
} as any);

export function useAppModuleContext<
  TStore extends Store<any, any> = Store,
  TContext extends ModuleContext = ModuleContext
>(): {
  value: ModuleContextValueType<TStore>;
  setStore: React.Dispatch<TStore>;
  setContext: React.Dispatch<TContext>;
} {
  const contextValue = useContext(AppModuleContext);
  return contextValue;
}
export function ModuleContextProvider(
  props: PropsWithChildren<{ context: AppContextType }>
) {
  const [store, setStore] = useState<Store<any, any>>();
  useEffect(() => {
    let unsubscribe: Unsubscribe | undefined;
    unsubscribe =
      store &&
      store.subscribe(() => {
        setState(store.getState());
      });
    return () => {
      setState({});
      unsubscribe && unsubscribe();
    };
  }, [store]);
  const [context, setContext] = useState();
  const [state, setState] = useState();
  const value = useMemo(
    () => ({
      // TODO: Change this appContext: props.context
      value: {
        ...context,
        state,
        dispatch: (store && store.dispatch) || null
      },
      setStore,
      setContext
    }),
    [context, store, state]
  );
  return (
    <AppModuleContext.Provider value={value}>
      <NotificationProvider modal={NotificationModal} context={props.context}>
        {props.children}
      </NotificationProvider>
    </AppModuleContext.Provider>
  );
}
