import React, { PropsWithChildren } from "react";

type Props = PropsWithChildren<{
  className?: string;
  onClick?: () => void;
}>;

export function Overlay(props: Props) {
  return (
    <div className={props.className || "layer"} onClick={props.onClick}>
      {props.children}
    </div>
  );
}
