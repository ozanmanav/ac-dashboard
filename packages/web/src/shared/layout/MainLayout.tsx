import React, {
  PropsWithChildren,
  useMemo,
  useCallback,
  FunctionComponent,
  useEffect
} from "react";
import { connect, DispatchProp } from "react-redux";
import { Content } from "./Content";
import { ModuleNavBar } from "@appcircle/ui/lib/ModuleNavBar";
import { MainNavBar } from "./MainNavBar";
import { NavBarListHeader } from "@appcircle/ui/lib/NavBarListHeader";
import {
  ModuleNavBarMenuItemProps,
  ModuleNavBarMenuItem
} from "@appcircle/ui/lib/ModuleNavBarMenuItem";
import { RouteComponentProps } from "react-router";
import { AppStateType } from "../../reducers";
import { MainNavBarItemType } from "../../reducers/mainNavBarData";
import { ModalContextProvider } from "@appcircle/shared/lib/services/ModalContext";
import { PortalRootProvider } from "@appcircle/core/lib/portal/Portal";
import { ModalRoute } from "@appcircle/shared/lib/helpers/modal/ModalRoute";
import { AppActions } from "reducers/AppActions";
import { MainNavBarItemProps } from "shared/component/MainNavBarItem";
import { generatePageTitle } from "shared/helpers/generatePageTitle";
import { useIntl, defineMessages } from "react-intl";

const messages = defineMessages({
  title: {
    id: "app.layout.mainLayout.title",
    defaultMessage: "Appcircle"
  }
});

export type PropsWithLayoutSize<P = {}> = P & {
  size?: "sm" | "md" | "lg";
};

// const CloseIcon = icons.close;

function LayoutConnect(state: AppStateType) {
  return {
    modulePath: state.moduleNavBar.module,
    moduleNavBar: state.moduleNavBar,
    modalRoutes: state.modalRoutes,
    mainNavBar: {
      topItems: state.mainNavBar.topItems.map(
        (item: MainNavBarItemType, index: number): MainNavBarItemProps => {
          return {
            ...item,
            isSelected: state.mainNavBar.selectedItemIndex === index
          };
        }
      ),
      bottomItems: state.mainNavBar.bottomItems.map(
        (item: MainNavBarItemType, index): MainNavBarItemProps => {
          return {
            ...item,
            isSelected:
              state.mainNavBar.selectedItemIndex -
                state.mainNavBar.topItems.length ===
              index
          };
        }
      )
    }
  };
}

type LayoutProps = PropsWithChildren<
  ReturnType<typeof LayoutConnect> &
    RouteComponentProps &
    DispatchProp<AppActions> & { isHome?: boolean }
>;

// TODO: change any to Type
const LayoutComp: FunctionComponent<LayoutProps> = props => {
  const { isHome } = props;
  const intl = useIntl();

  useEffect(() => {
    document.title = generatePageTitle(
      props.moduleNavBar,
      intl.formatMessage(messages.title)
    );
  }, [props.moduleNavBar]);

  const onModuleNavClick = useCallback(
    index => {
      props.dispatch({
        type: "app/module-nav/selectedIndex/update",
        index
      });
    },
    // TODO : Add eslint ignore and clear deps
    [props]
  );
  const navBarItems = useMemo(
    () =>
      props.mainNavBar.topItems.map((item, index: number) => {
        item.setSelectedItemIndex = () => {
          props.dispatch({
            type: "app/navbar/selectedIndex/update",
            index
          });
        };
        return item;
      }),
    [props]
  );
  return (
    <React.Fragment>
      <ModalContextProvider>
        <PortalRootProvider>
          <div className="MainLayout">
            <nav>
              <MainNavBar
                topItems={navBarItems}
                bottomItems={props.mainNavBar.bottomItems}
              />
            </nav>
            <ModuleNavBar isBlock={isHome}>
              <NavBarListHeader text={props.moduleNavBar.header} />
              {props.moduleNavBar.items.map(
                (item: ModuleNavBarMenuItemProps, index: number) => (
                  <ModuleNavBarMenuItem
                    index={index}
                    text={item.text}
                    children={item.children}
                    isSelected={index === props.moduleNavBar.selectedItemIndex}
                    link={props.modulePath + item.link}
                    length={item.length}
                    key={props.modulePath + item.link}
                    onClick={onModuleNavClick}
                  />
                )
              )}
            </ModuleNavBar>
            <div className={"container"}>
              <Content pushRight={isHome}>{props.children}</Content>
            </div>
            <ModalRoute
              prefix="/app"
              modalRoutes={props.modalRoutes}
            ></ModalRoute>
          </div>
        </PortalRootProvider>
      </ModalContextProvider>
    </React.Fragment>
  );
};

export const Layout = connect(LayoutConnect)(LayoutComp);
