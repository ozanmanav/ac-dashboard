import React, { PropsWithChildren, ComponentType } from "react";

type Props = PropsWithChildren<{
  sidebar: ComponentType;
}>;
export function withSideNav(Layout: ComponentType) {
  return function LayoutWithSideNav(props: Props) {
    return (
      <React.Fragment>
        <nav>{props.sidebar}</nav>
        <Layout>{props.children}</Layout>
      </React.Fragment>
    );
  };
}
