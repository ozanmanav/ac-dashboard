import React, { PropsWithChildren } from "react";
import { Link } from "react-router-dom";
import { useUserService } from "shared/domain/user/userService";
import {
  MainNavBarItem,
  MainNavBarItemProps
} from "shared/component/MainNavBarItem";
import { useDisconnected } from "@appcircle/shared/lib/services/DisconnectedContext";
import { NotificationNavBarItem } from "shared/component/NotificationNavBarItem";
import { useNotificationsModel } from "@appcircle/shared/lib/notification/useNotificationsModel";

export type MainNavBarProps = PropsWithChildren<{
  topItems: Array<MainNavBarItemProps>;
  bottomItems: Array<MainNavBarItemProps>;
}>;

const gravatar = `https://s.gravatar.com/avatar/fa00145b86180f33d7bc97326d03289c?s=80`;

export function MainNavBar(props: MainNavBarProps) {
  const { userInfo } = useUserService();
  const [{ isDisconnected }] = useDisconnected();
  const { getIsNewNotificationExistInfo } = useNotificationsModel();

  const isNewNotificationExist = getIsNewNotificationExistInfo();

  //TODO: Ask kibar about the case in which the user has no picture. We need to handle it here
  return (
    <div
      className={
        isDisconnected ? "MainNavBar MainNavBar--narrow" : "MainNavBar"
      }
    >
      <div className="MainNavBar_root">
        <div className="MainNavBar_items MainNavBar_items_top">
          <div className="MainNavBarItem MainNavBar_logo" />
          {props.topItems.map((item: MainNavBarItemProps) => (
            <MainNavBarItem {...item} key={item.id} />
          ))}
        </div>
        <div className="MainNavBar_items MainNavBar_items_bottom">
          {props.bottomItems.map((item: MainNavBarItemProps) => {
            if (item.name && item.name === "notifications") {
              return (
                <NotificationNavBarItem
                  {...item}
                  key={item.id}
                  isNewNotificationExist={isNewNotificationExist}
                />
              );
            }

            return <MainNavBarItem {...item} key={item.id} />;
          })}
          <Link className="MainNavBar_user" to="/account">
            <div
              className="MainNavBar_user"
              style={{
                backgroundImage: `url(${
                  userInfo && userInfo.picture ? userInfo.picture : gravatar
                })`
              }}
            />
          </Link>
        </div>
      </div>
    </div>
  );
}
