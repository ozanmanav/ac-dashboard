import React, { PropsWithChildren } from "react";
import { PropsWithLayoutSize } from "./MainLayout";
import classNames from "classnames";

type ContentProps = PropsWithChildren<
  PropsWithLayoutSize & {
    pushRight?: boolean;
  }
>;

export function Content(props: ContentProps) {
  return (
    <main className={classNames({ pushRight: props.pushRight })}>
      {props.children}
    </main>
  );
}
