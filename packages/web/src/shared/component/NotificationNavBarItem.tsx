import React, { PropsWithChildren } from "react";

import { MainNavBarItem } from "./MainNavBarItem";
import { MainNavBarItemType } from "reducers/mainNavBarData";
import { SvgNotificationWithDot } from "@appcircle/assets/lib/NotificationWithDot";
import { SvgNotification } from "@appcircle/assets/lib/Notification";

export type NotificationNavBarItemProps = MainNavBarItemType &
  PropsWithChildren<{
    isSelected?: boolean;
    isNewNotificationExist: boolean;
  }>;

export function NotificationNavBarItem(props: NotificationNavBarItemProps) {
  return (
    <MainNavBarItem
      {...props}
      icon={
        props.isNewNotificationExist ? SvgNotificationWithDot : SvgNotification
      }
    />
  );
}
