import { Link } from "react-router-dom";
import React, {
  PropsWithChildren,
  FunctionComponent,
  SVGProps,
  useState,
  useRef
} from "react";
import classNames from "classnames";
import { MainNavBarItemType } from "../../reducers/mainNavBarData";
import { ToolTip } from "@appcircle/ui/lib/TootlTip";

export type MainNavBarItemProps = MainNavBarItemType &
  PropsWithChildren<{
    isSelected?: boolean;
  }>;

export function MainNavBarItem(props: MainNavBarItemProps) {
  const Icon: FunctionComponent<SVGProps<SVGSVGElement>> = props.icon;
  const [isOpen, setIsOpen] = useState(false);
  const inputEl = useRef<HTMLInputElement>(null);

  const onBodyClick = (e: MouseEvent) => {
    if (
      inputEl.current &&
      inputEl.current.getBoundingClientRect &&
      checkMousePointerInElement(
        e.clientX,
        e.clientY,
        inputEl.current.getBoundingClientRect()
      )
    )
      return;
    setTimeout(closeAndRemoveEventListener);
  };
  const onBodyTouch = (e: TouchEvent) => {
    var touch = e.touches[0];
    if (!touch) return;
    var x = touch.pageX;
    var y = touch.pageY;
    if (
      inputEl.current &&
      inputEl.current.getBoundingClientRect &&
      checkMousePointerInElement(x, y, inputEl.current.getBoundingClientRect())
    )
      return;
    setTimeout(closeAndRemoveEventListener);
  };

  const closeAndRemoveEventListener = () => {
    setIsOpen(false);
    document.body.removeEventListener("click", onBodyClick);
    document.body.removeEventListener("touchstart", onBodyTouch);
  };
  if (isOpen) {
    document.body.addEventListener("click", onBodyClick);
    document.body.addEventListener("touchstart", onBodyTouch);
  }

  return (
    <React.Fragment>
      <div
        className={classNames("MainNavBarItem", {
          "MainNavBarItem-selected": props.isSelected
        })}
      >
        <div className="MainNavBarItem_overlay" />
        <Link
          className={"MainNavBarItem_link"}
          data-tooltip="right"
          to={props.link}
        >
          <Icon />
        </Link>
        <ToolTip left={70}>{props.toolTipText}</ToolTip>
      </div>
    </React.Fragment>
  );
}

//Can be put in a util
function checkMousePointerInElement(
  mouseX: number,
  mouseY: number,
  positionRect: ClientRect
) {
  return (
    positionRect.left <= mouseX &&
    mouseX <= positionRect.left + positionRect.width &&
    positionRect.top <= mouseY &&
    mouseY <= positionRect.top + positionRect.height
  );
}
