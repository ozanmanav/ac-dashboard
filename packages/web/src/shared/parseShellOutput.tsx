import React, {
  useRef,
  FunctionComponent,
  FunctionComponentElement,
  ComponentProps
} from "react";

type SectionComponent = FunctionComponent<{
  content: string;
  id: string;
  childIndexes?: number[];
  hasError?: boolean;
}>;
type SectionElement = FunctionComponentElement<{ content: string }>;
type SectionItem = {
  tag: Tags;
  startpos: number;
  endpos: number;
  children?: number[];
  startSectionIndex: number;
};

function parseShellOutput(content: string): [string, SectionItem[]] {
  let startpos: number,
    endpos: number,
    tag: Tags | "",
    collection: SectionItem[] = [],
    lastStartPos: number = 0,
    lastEndPos: number = 0,
    lastParentIndex: number = 0;

  // Loop if any tag exists.
  while ((startpos = content.indexOf("@[", lastStartPos)) > -1) {
    // parses current tag
    tag =
      (content
        .substr(startpos)
        .match(/(@\[[\w:]*\]?)/i)
        ?.shift() as Tags) || "";
    lastStartPos = startpos + tag.length;

    // if it's an unexpected tag then gives an error.
    if (itemsComponentMap[tag] === undefined)
      throw new Error(`${tag} is unexpected tag.`);

    let mapItem = itemsComponentMap[tag];
    let currentItem: SectionItem = {
      tag,
      startpos: startpos + tag.length,
      endpos: 0,
      startSectionIndex: lastParentIndex
    };

    if (
      !mapItem.sectionStart &&
      !mapItem.sectionEnd &&
      collection[lastParentIndex] !== undefined
    ) {
      collection[lastParentIndex].children =
        collection[lastParentIndex].children || [];
      collection[lastParentIndex].children?.push(collection.length);
    } else if (mapItem.sectionStart) {
      lastParentIndex = collection.length;
    } else if (mapItem.sectionEnd) {
      currentItem.children =
        collection[lastParentIndex]?.children?.slice() || [];
    }
    endpos = content.indexOf(mapItem.endString, startpos + tag.length);
    if (endpos === -1) {
      endpos = content.length;
    }
    // Sometimes section's context has no tag then context display is broken.
    // To prevent invalid display, it adds a section-content tag as default
    // after every section-begin and section-end tag.
    if (tag === "@[section:begin]" || tag === "@[section:end]") {
      content =
        content.substring(0, endpos) +
        "@[section:content]" +
        content.substr(endpos).trim();
    }
    lastEndPos = endpos;
    currentItem.endpos = endpos;
    collection.push(currentItem);
  }
  return [content, collection];
}

const SectionErrorNode: SectionComponent = props => {
  return (
    <pre className="logsError" id={props.id}>
      {(props.content || "").replace(/(^\s* | @\[error\]*)/gm, "")}
    </pre>
  );
};

function close(indexes: number[], self: HTMLDivElement) {
  indexes.forEach(index => {
    const element: HTMLSpanElement | null = document.getElementById(
      `logsSection-${index}`
    );

    if (element) {
      const toggle = element.style.display !== "none";
      self.classList.remove("logsSection_start-opened--" + toggle.toString());
      self.classList.add("logsSection_start-opened--" + (!toggle).toString());
      element.style.setProperty("display", toggle ? "none" : "block");
    }
  });
}

function SectionStartNode(props: ComponentProps<SectionComponent>) {
  const ref = useRef<HTMLDivElement>(null);
  return (
    <div
      className={
        "logsSection logsSection_start logsSection_start-opened--true logsSection-error--" +
        !!props.hasError
      }
      id={props.id}
      ref={ref}
      onClick={() =>
        ref.current?.parentElement &&
        close(props.childIndexes || [], ref.current)
      }
    >
      {props.content}
    </div>
  );
}
function SectionLogsNode(props: ComponentProps<SectionComponent>) {
  return props.content.trim() ? (
    <span id={props.id} className="logsSection_logs">
      <pre>$ {props.content.trim()}</pre>
    </span>
  ) : null;
}

function SectionEndNode(props: ComponentProps<SectionComponent>) {
  return (
    <span
      id={props.id}
      className={
        "logsSection logsSection_end logsSection-error--" + !!props.hasError
      }
    >
      {props.content}
    </span>
  );
}

type TagMapItem = {
  component: SectionComponent;
  endString: string;
  sectionStart?: boolean;
  sectionEnd?: boolean;
};
const itemsComponentMap: { [key in Tags]: TagMapItem } = {
  "@[section:begin]": {
    component: SectionStartNode,
    endString: `\n`,
    sectionStart: true
  },
  "@[section:content]": { component: SectionLogsNode, endString: "@[" },
  "@[section:end]": {
    component: SectionEndNode,
    endString: `\n`,
    sectionEnd: true
  },
  "@[command]": { component: SectionLogsNode, endString: "@[" },
  "@[error]": { component: SectionErrorNode, endString: `@[` }
};

type Tags =
  | "@[section:begin]"
  | "@[section:content]"
  | "@[section:end]"
  | "@[command]"
  | "@[error]";

export function outputParser(logs: string) {
  const content = logs.replace(/^=*([\n\r]*)$/gm, "");
  const [updatedContent, items] = parseShellOutput(content);

  return items.map((item, index) => {
    const Comp: SectionComponent = itemsComponentMap[item.tag].component;
    return (
      <Comp
        key={index}
        id={`logsSection-${index}`}
        content={updatedContent.substring(item.startpos, item.endpos)}
        childIndexes={item.children}
        hasError={
          item.children &&
          item.children.some(child => items[child].tag === "@[error]")
        }
      ></Comp>
    );
  });
}
