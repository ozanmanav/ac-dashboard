// Google Tag Manager Service
import TagManager from "react-gtm-module";
import { useEffect } from "react";

type GTMServiceOptions = {
  initialize?: boolean;
  triggerEvent?: string;
};

export const useGTMService = (
  options: GTMServiceOptions = { initialize: false, triggerEvent: undefined }
) => {
  useEffect(() => {
    const gaTagManagerID =
      process.env.REACT_APP_GA_TAG_MANAGER_ID ||
      window.REACT_APP_GA_TAG_MANAGER_ID;

    if (options.initialize && gaTagManagerID) {
      TagManager.initialize({
        gtmId: gaTagManagerID,
        dataLayer: {
          dataLayerName: "ACDataLayer"
        }
      });
    }
  }, [options.initialize]);

  useEffect(() => {
    if (options.triggerEvent) {
      triggerGTMEvent(options.triggerEvent);
    }
  }, [options.triggerEvent]);

  const triggerGTMEvent = (eventName: string) => {
    TagManager.dataLayer({
      dataLayer: {
        event: eventName
      }
    });
  };

  const setGTMInfo = (infoName: string, infoValue: any) => {
    TagManager.dataLayer({
      dataLayer: {
        [infoName]: infoValue
      }
    });
  };

  return { triggerGTMEvent, setGTMInfo };
};
