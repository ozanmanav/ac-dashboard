import { useState, useEffect } from "react";
import { store } from "App";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { ConsumerFactoryFn } from "@appcircle/shared/lib/services/createService";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useIntl, defineMessages } from "react-intl";
import { UserResponse } from "@appcircle/shared/lib/services/UserResponse";
import { AuthTokenType } from "@appcircle/shared/lib/services/AuthTokenType";
import { Result } from "@appcircle/core/lib/services/Result";
import { useGTMService } from "../useGTMService";
import Cookie from "js-cookie";

const messages = defineMessages({
  error: {
    id: "app.service.user.getData.error",
    defaultMessage:
      "{subject}, something went wrong while fetching the user data."
  }
});

//TODO: Add a setUserInfo() method that sends API request to update userinfo eg. update picture
export function useUserService() {
  const { createAuthConsumer, token } = useServiceContext();
  const { addTask, createApiTask } = useTaskManager();
  const currentUserInfo = store.getState().userInfo;
  const [userInfo, setUserInfo] = useState<UserResponse | null>(
    currentUserInfo
  );
  const intl = useIntl();
  const { setGTMInfo } = useGTMService();

  useEffect(() => {
    if (!token) return setUserInfo(null);
    const services = createBoundry(createAuthConsumer, token);
    if (!userInfo) {
      const task =
        createApiTask &&
        createApiTask(services.getUserInfo(), {
          onSuccess: result => {
            const resultJSON = (result as Result).json;

            setUserInfo(resultJSON);

            if (resultJSON) {
              Cookie.set("userId", resultJSON.sub);
              setGTMInfo("userId", resultJSON.sub);
              setGTMInfo("user_properties", {
                userId: resultJSON.sub
              });
            }

            store.dispatch({
              type: "ADD_USER_INFO_SUCCESS",
              userInfo: resultJSON
            });
          },
          onError: e => {
            setUserInfo(null);
          },
          errorMessage: result =>
            intl.formatMessage(messages.error, {
              subject: `${(result as Result).status} ${
                (result as Result).statusText
              }`
            }),
          retryCount: 3
        });
      task && addTask(task);
    }
    // eslint-disable-next-line
  }, [token]);

  return { userInfo };
}

function createBoundry(
  createAuthConsumer: ConsumerFactoryFn,
  token: AuthTokenType
) {
  const authConsumer = createAuthConsumer();
  const services = {
    getUserInfo() {
      return authConsumer<UserResponse>({
        method: "GET",
        endpoint: "connect/userinfo",
        headers: {
          Authorization: "Bearer " + token.access_token,
          "Content-Type": "application/json"
        }
      });
    }
  };
  return services;
}
