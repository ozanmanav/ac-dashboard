import { ModuleNavBarState } from "reducers/moduleNavbarReducer";

export const generatePageTitle = (
  moduleNavBar: ModuleNavBarState,
  defaultTitle: string
) => {
  let title = defaultTitle;

  if (moduleNavBar.header && moduleNavBar.header !== "") {
    title = `${moduleNavBar.header} - ${defaultTitle}`;

    if (moduleNavBar.selectedItem && moduleNavBar.selectedItem.text !== "") {
      title = `${moduleNavBar.selectedItem.text} - ${moduleNavBar.header} - ${defaultTitle}`;
    }
  } else {
    title = `${defaultTitle}`;
  }

  return title;
};
