import React, { useRef } from "react";
import { outputParser } from "./parseShellOutput";

export type ShellOuputProps = {
  code: string;
  plugins: string[];
  language: string;
};

export function ShellOutput(props: ShellOuputProps) {
  const ref = useRef<HTMLDivElement | null>(null);

  const result = outputParser(props.code);

  return (
    <div ref={ref} className="ShellOutput">
      {result.length ? result : <pre>{props.code}</pre>}
    </div>
  );
}
