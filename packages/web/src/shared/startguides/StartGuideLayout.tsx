import React, { PropsWithChildren } from "react";
import { ScrollView } from "@appcircle/ui/lib/ScrollView";

export function StartGuideLayout(props: PropsWithChildren<{}>) {
  return (
    <div className="startGuideLayout">
      <ScrollView>
        <div className="static-content">{props.children}</div>
      </ScrollView>
    </div>
  );
}
