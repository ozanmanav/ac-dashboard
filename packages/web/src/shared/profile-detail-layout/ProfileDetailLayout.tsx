import React from "react";
import classNames from "classnames";
import { SvgHamburger } from "@appcircle/assets/lib/Hamburger";

export type ProfileDetailLayout = {
  sideBar: JSX.Element | null;
  details: JSX.Element;
  inProgress?: boolean;
  selectedItemID?: any;
  progressText?: string;
  isSideBarExpanded: boolean;
  leftHandlerClick: () => void;
  className?: string;
};
export function ProfileDetailLayout(props: ProfileDetailLayout) {
  return (
    <div className={"ProfileDetail " + (props.className || "")}>
      {props.sideBar && (
        <div
          className={classNames("ProfileDetail_sideBar  flex-column", {
            "ProfileDetail_sideBar-collapsed": !props.isSideBarExpanded
          })}
        >
          {props.sideBar}
        </div>
      )}
      <div
        className={classNames("leftBarHandler", {
          "leftBarHandler-show": !props.isSideBarExpanded
        })}
        onClick={props.leftHandlerClick}
      >
        <SvgHamburger />
      </div>
      {props.details}
    </div>
  );
}
