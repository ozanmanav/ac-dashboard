import React, { ReactElement } from "react";
import {
  BasicDataGridWithProgress,
  SelectedItemsType,
  BasicDataGridItemsSelectedHandler
} from "@appcircle/ui/lib/datagrid/BasicDataGrid";
import {
  BasicDataGridHeaderTemplateType,
  BasicDataGridRowTemplateType
} from "@appcircle/ui/lib/datagrid/BasicDataGridRow";
import { ProfileDetailsTableProps } from "modules/distribute/pages/profile/components/ProfileTestersTable";

export type ProfileDetailProps = {
  header: JSX.Element;
  gridData: any;
  gridHeaderClassName?: string;
  gridHeaderTemplate: BasicDataGridHeaderTemplateType;
  gridRowTemplate: BasicDataGridRowTemplateType<ProfileDetailsTableProps>;
  gridRowSeperatorTemplate: () => ReactElement;
  onItemSelected: BasicDataGridItemsSelectedHandler;
  defaultSelected?: SelectedItemsType;
  updateParameter: string;
  inProgress?: boolean;
  fallback?: JSX.Element;
};
export function ProfileDetailTable(props: ProfileDetailProps) {
  return (
    <div className="ProfileDetailTable">
      <div className="ProfileDetailTable_header">{props.header}</div>
      <BasicDataGridWithProgress
        updateParameter={props.updateParameter}
        selectedItems={props.defaultSelected}
        onItemsSelected={props.onItemSelected}
        key={"grid"}
        data={props.gridData}
        headerClassName={props.gridHeaderClassName || ""}
        headerTemplate={props.gridHeaderTemplate}
        rowClassName={"ProfileTestersTableItem"}
        rowTemplate={props.gridRowTemplate}
        rowSeperatorTemplate={props.gridRowSeperatorTemplate}
        inProgress={props.inProgress}
      />
      {props.fallback && props.gridData.length === 0 && props.fallback}
    </div>
  );
}
