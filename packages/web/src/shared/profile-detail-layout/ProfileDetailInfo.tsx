import React, { PropsWithChildren } from "react";
import classNames from "classnames";

export type ProfileDetailInfoProps = PropsWithChildren<{ className?: string }>;

export function ProfileDetailInfo(props: ProfileDetailInfoProps) {
  return (
    <div className={classNames("ProfileDetailInfo", props.className || "")}>
      {props.children}
    </div>
  );
}
