import React, { PropsWithChildren } from "react";
import { DataListItem } from "@appcircle/ui/lib/datalist/DataListItem";

export type ProfileDetailListItemProps = PropsWithChildren<{
  onClick?: () => void;
  onKillFocus?: () => void;
  isSelected: boolean;
  isTemporary?: boolean;
  excludeKillTarget?: string;
}>;

export function ProfileDetailListItem(props: ProfileDetailListItemProps) {
  return (
    <DataListItem
      className={"ProfileDetailListItem"}
      isSelected={props.isSelected}
      isTemporary={props.isTemporary}
      onClick={props.onClick}
      onKillFocus={props.onKillFocus}
      excludeKillTarget={props.excludeKillTarget}
    >
      {props.children}
    </DataListItem>
  );
}
