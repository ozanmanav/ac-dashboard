import React, { PropsWithChildren } from "react";
import classNames from "classnames";

export type ProfileDetailHeaderProps = PropsWithChildren<{
  leftItem?: JSX.Element;
  rightItem?: JSX.Element;
  className?: string;
}>;

export function ProfileDetailHeader(props: ProfileDetailHeaderProps) {

  return (
    <div className={classNames("ProfileDetailHeader", props.className)}>
      <div className="ProfileDetailHeader_leftItem">{props.leftItem}</div>
      {props.rightItem ? (
        <div className="ProfileDetailHeader_rightItem">{props.rightItem}</div>
      ) : null}
    </div>
  );
}
