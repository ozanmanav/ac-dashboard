import React, { PropsWithChildren } from "react";
import { DataList } from "@appcircle/ui/lib/datalist/DataList";
import classNames from "classnames";

export type ProfileDetailListProps = PropsWithChildren<{ className?: string }>;

export function ProfileDetailList(props: ProfileDetailListProps) {
  return (
    <div className={classNames("ProfileDetailList", props.className || "")}>
      <DataList>{props.children}</DataList>
    </div>
  );
}
