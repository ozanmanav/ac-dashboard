import React, { PropsWithChildren } from "react";

export type ProfileDetailListHeaderProps = PropsWithChildren<{}>;
export function ProfileDetailListHeader(props: ProfileDetailListHeaderProps) {
  return <div className="ProfileDetailListHeader">{props.children}</div>;
}
