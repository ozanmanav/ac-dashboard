import React, { useMemo } from "react";
import { useAppModuleContext } from "../../AppModuleContext";
import { AnyAction } from "redux";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { DeleteModalRouteParams } from "modules/signing_identities/modals/SIDDeleteModalDialog";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { DeleteModalDialog } from "@appcircle/ui/lib/modal/DeleteModalDialog";

export type DeleteModalDialogRouteState = {
  title: string;
  iconURL?: string;
  name: string;
  endpoint: string;
  placeHolder: string;
  action: AnyAction;
  successMessage: string;
  errorMessage: string;
  entityID?: string;
  ignoreTaskId?: boolean;
};
export type DeleteModalDialogViewProps<
  TState extends DeleteModalDialogRouteState = DeleteModalDialogRouteState
> = ModalComponentPropsType<
  { ignoreTaskId?: boolean },
  TState,
  DeleteModalRouteParams
>;
export function DeleteModalDialogView(props: DeleteModalDialogViewProps) {
  const state = props.routeProps.state;
  const moduleContext = useAppModuleContext();
  const taskProps = useMemo<TaskPropsType>(
    () => ({
      ignoreTaskId: !isEmptyValue(props.ignoreTaskId)
        ? props.ignoreTaskId
        : props.routeProps.state.ignoreTaskId,
      entityID: state.entityID,
      errorMessage: () => state.errorMessage,
      successMessage: () => state.successMessage,
      onSuccess: () => {
        moduleContext.value.dispatch(state.action);
        props.onModalClose();
      }
    }),
    [
      moduleContext.value,
      props,
      state.action,
      state.entityID,
      state.errorMessage,
      state.successMessage
    ]
  );
  return (
    <DeleteModalDialog
      entityID={props.routeProps.params.id || state.entityID || ""}
      modalID={props.modalID}
      routeProps={props.routeProps}
      onModalData={props.onModalData}
      errorMessage={state.errorMessage}
      endpoint={state.endpoint}
      header={""}
      iconURL={state.iconURL}
      onModalClose={props.onModalClose}
      title={state.title}
      onSuccess={taskProps.onSuccess}
      successMessage={state.successMessage}
      name={
        (props.routeProps.params && props.routeProps.params.name) || state.name
      }
      placeHolder={
        (props.routeProps.params && props.routeProps.params.name) ||
        state.placeHolder
      }
    />
  );
}
