import React, { useCallback, useState } from "react";
import { useAppModuleContext } from "AppModuleContext";
import {
  useTaskManager,
  TaskCallbackFn
} from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useIntl, defineMessages } from "react-intl";
import { Result } from "@appcircle/core/lib/services/Result";
import { AddNewFileProps, AddNewFile } from "@appcircle/ui/lib/AddNewFile";
import { ModalDialog } from "@appcircle/ui/lib/ModalDialog";

const messages = defineMessages({
  fileuploadSuccess: {
    id: "app.dialog.addNewFile.success",
    defaultMessage: "File has been uploaded"
  },
  fileuploadError: {
    id: "app.dialog.addNewFile.error",
    defaultMessage: "File can't be uploaded"
  }
});

export type AddNewFileModalViewProps<
  TState extends AddNewFileModalState = AddNewFileModalState
> = {
  icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
  header: string;
  text: string;
  onStateChange?: AddNewFileProps["onStateChange"];
  onFileUpload?: (file: File) => void;
  onModalClose: () => void;
  routeProps: {
    state: TState;
  };
  dispatch?: React.Dispatch<any>;
  onSuccess?: TaskCallbackFn<any>;
};
export type AddNewFileModalState<
  TActions extends { type: string } = { type: string },
  TIcons extends string[] = string[]
> = {
  fileTypes: string[];
  endpoint: string;
  key?: string;
  action?: TActions["type"];
  icons?: TIcons;
  text?: string;
};

export function AddNewFileModalDialog(props: AddNewFileModalViewProps) {
  const routeState =
    (props.routeProps && props.routeProps.state && props.routeProps.state) ||
    {};
  // const [file, setFile] = useState<File>();
  const context = useAppModuleContext();
  const { createApiConsumer } = useServiceContext();
  const taskManager = useTaskManager();
  const service = createApiConsumer();
  const [progress, setProgress] = useState<number>(0);
  const [status, setStatus] = useState<AddNewFileProps["status"]>("idle");
  const intl = useIntl();
  const dispatch = props.dispatch || context.value.dispatch;

  const onFileUpload = useCallback(
    file => {
      setStatus("uploading");
      const task = taskManager.createApiTask(
        service({
          endpoint: props.routeProps.state.endpoint,
          data: {
            [props.routeProps.state && props.routeProps.state.key
              ? props.routeProps.state.key
              : file.name]: file
          },
          method: "POST"
        }),
        {
          onProgress(e) {
            setProgress(e.loaded / e.total);
          },
          onSuccess(resp) {
            setStatus("idle");
            !!props.onSuccess
              ? props.onSuccess(resp)
              : routeState.action &&
                dispatch({
                  type: routeState.action,
                  payload: (resp as Result).json
                });
            props.onModalClose();
          },
          onError(err) {
            setStatus("error");
          },
          onFinally() {},
          successMessage: () => intl.formatMessage(messages.fileuploadSuccess),
          errorMessage: () => intl.formatMessage(messages.fileuploadError)
        }
      );
      taskManager.addTask(task);
    },
    [intl, props, routeState.action, service, taskManager, dispatch]
  );

  return (
    <ModalDialog
      onModalClose={props.onModalClose}
      header={props.header}
      className="AddNewFileModalView"
    >
      <AddNewFile
        status={status}
        progress={progress}
        icons={props.icons}
        onFileUpload={onFileUpload}
        onStateChange={status => {
          props.onStateChange && props.onStateChange(status);
          status === "idle" && setStatus(status);
        }}
        types={routeState.fileTypes}
        text={
          props.text ||
          (props.routeProps.state && props.routeProps.state.text) ||
          ""
        }
        autoUpload={true}
      />
    </ModalDialog>
  );
}
