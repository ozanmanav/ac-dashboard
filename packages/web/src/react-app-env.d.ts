/// <reference types="react-scripts" />

declare namespace NodeJS {
  interface ProcessEnv {
    REACT_APP_UI_SERVER: string;
    REACT_APP_API_SERVER: string;
    REACT_APP_AUTH_SERVER: string;
    REACT_APP_GA_TRACKING_ID: string;
    REACT_APP_GA_TAG_MANAGER_ID: string;
  }
}

declare interface Window {
  REACT_APP_UI_SERVER: string;
  REACT_APP_API_SERVER: string;
  REACT_APP_AUTH_SERVER: string;
  REACT_APP_GA_TRACKING_ID: string;
  REACT_APP_GA_TAG_MANAGER_ID: string;
}

declare namespace JSX {}
