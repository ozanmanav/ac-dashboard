import { useMemo, useEffect } from "react";
import { createHookConnection } from "createHookConnection";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useDisconnected } from "@appcircle/shared/lib/services/DisconnectedContext";

export function useHookService() {
  const { token } = useServiceContext();
  const { pushMessage, pushEvent } = useNotificationsService();
  // const tokendata = useMemo(() => token && jwt.decode(token.access_token), [
  //   token
  // ]);
  // console.log(tokendata);
  const [, setDisconnected] = useDisconnected();
  const [eventSource, serverListener$] = useMemo(
    () => createHookConnection(token, pushMessage, pushEvent, setDisconnected),
    // eslint-disable-next-line
    [token]
  );

  useEffect(() => {
    const unsubs =
      serverListener$ &&
      serverListener$.subscribe({
        next: () => {}
      });
    return () => {
      eventSource && eventSource.close();
      unsubs && unsubs.unsubscribe();
    };
  }, [serverListener$]);

  return serverListener$;
}
