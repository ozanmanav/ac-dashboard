import { isEmptyValue } from "./isEmptyValue";

export function isEmptyId(id: string) {
  return isEmptyValue(id) || id.indexOf("00000") === 0;
}
