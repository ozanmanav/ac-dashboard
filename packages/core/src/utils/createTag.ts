import shortid from "shortid";
export function createTag(
  text: string,
  modifiable: boolean = true,
  icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>
) {
  let tag: any = {
    id: shortid.generate(),
    modifiable,
    text
  };
  icon && (tag.icon = icon);
  return tag;
}
