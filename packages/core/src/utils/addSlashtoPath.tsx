export function addSlashtoPath(url: string) {
  return url.indexOf("/") === -1 ? "/" + url : url;
}
