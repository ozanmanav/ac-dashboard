export default function runPipe<T = string>(
  initialValue: T,
  ...args: ((content: T) => T)[]
) {
  return args.reduce<T>((acc, curr) => {
    return curr(acc);
  }, initialValue);
}
