import shortid from "shortid";

const prefix = `fakeID`;
export function createFakeID() {
  return prefix + shortid.generate();
}

export function convertFakeID(id: string) {
  return prefix + id;
}

export function isFakeID(id: string) {
  return id.startsWith(prefix);
}
