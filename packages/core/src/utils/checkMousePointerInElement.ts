export function checkMousePointerInElement(
  mouseX: number,
  mouseY: number,
  positionRect: ClientRect
) {
  return (
    positionRect.left <= mouseX &&
    mouseX <= positionRect.left + positionRect.width &&
    positionRect.top <= mouseY &&
    mouseY <= positionRect.top + positionRect.height
  );
}
