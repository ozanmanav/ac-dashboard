export function isObject(val: any) {
  return val !== null && !Array.isArray(val) && typeof val === "object";
}
