export function createCopyName(
  entity: {
    id: string;
    name: string;
  },
  checkExistingFn: (name: string, id: string) => boolean
) {
  let num = entity.name.match("[0-9]+$");
  let counter = num ? parseInt(num[0]) + 1 : 1;
  let namePattern = num
    ? entity.name.substring(0, num.index)
    : entity.name + "_";
  let newName = namePattern + counter;
  while (checkExistingFn(newName, entity.id) !== false) {
    newName = namePattern + ++counter;
  }
  return newName;
}
