import {
  MAX_NAME_LENGTH,
  ProfileNameRegexp
} from "@appcircle/core/lib/constants";

export function alphaNummericTextMaybe(
  oldText: string,
  newText: string,
  max: number = MAX_NAME_LENGTH
) {
  if (
    newText.length <= max &&
    new RegExp(`^(?=.*$)[\\w]*(?:(?=.{1})[\\s]\\w+)*[\\s]?$`).test(newText)
  )
    return newText;
  else return oldText;
}

export function textWithSpecialsMaybe(
  oldText: string,
  newText: string,
  max: number = MAX_NAME_LENGTH
) {
  // if (newText.length <= max && new RegExp(`^(?=.*$)[\\w]*(?:(?=.{1})[\\s][\\w-_.]+)*[\\s]?$`).test(newText))
  if (newText.length <= max && new RegExp(ProfileNameRegexp).test(newText))
    return newText;
  else return oldText;
}
