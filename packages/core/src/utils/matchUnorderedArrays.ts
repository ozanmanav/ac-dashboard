import { shallowEqual } from "./shallowEqual"

export function matchUnorderedArrays(arr1: string[], arr2: string[]) {
    return arr1.length === arr2.length && shallowEqual(arr1.sort(), arr2.sort());
}
