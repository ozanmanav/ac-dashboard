export function createQueryString(...params: any[]) {
  return "?" + params.map(param => param.join("=")).join("&");
}
