export function isEmptyValue(val: any): boolean {
  return val === undefined || val === null || val === "";
}
