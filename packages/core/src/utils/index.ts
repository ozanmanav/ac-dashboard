import shortid from "shortid";
import { OSType } from "../enums/OSType";

export function debounce(func: Function, delay: number) {
  let inDebounce: any;
  return function(this: any, ...args: []) {
    const context = this;
    clearTimeout(inDebounce);
    inDebounce = setTimeout(() => func.apply(context, args), delay);
  };
}

export function setIdsTo(arr: Array<any>): Array<any> {
  return arr.map(el => (el.id = shortid.generate()));
}

export function compareable(condition: () => boolean) {
  return <T = any, R = any>(left: T, right: R): T | R => {
    return condition() ? left : right;
  };
}

export function matchAnyCase(src: string, match: string) {
  return src.toLowerCase() === match;
}

export function iOSMaybe(os: OSType) {
  return os.toLowerCase() === "ios" ? "ios" : "android";
}

export function androidMaybe(os: OSType) {
  return os.toLowerCase() === "android" ? "android" : "ios";
}
