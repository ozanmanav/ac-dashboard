export function slashCount(str: string): number {
  return str.split("/").length;
}
