export function patchFn<T>(
  targetFn: (...args: T[]) => void = function() {},
  additionFn: Function,
  callAferTargetCall: boolean = false
): (...args: T[]) => void {
  let _targetFn = targetFn;
  targetFn = (...arg: T[]) => {
    !callAferTargetCall && additionFn(...arg);
    _targetFn(...arg);
    callAferTargetCall && additionFn(...arg);
  };
  return targetFn;
}
