import { isEmptyValue } from "./isEmptyValue";

export function emptyValueMaybe(val: any, orVal: any) {
  return isEmptyValue(val) ? orVal : val;
}
