export function readFilename(headers: string | XMLHttpRequest | null): string | null {
  if (headers instanceof XMLHttpRequest){
    return readFilename(readContentDisposition(headers));
  } else if (headers && headers.indexOf('content-disposition:') !== -1) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(headers);
    if (matches != null && matches[1]) {
      return matches[1].replace(/['"]/g, '');
    }
  }

  return null;
}

export function readContentDisposition(req: XMLHttpRequest){
  try {
    const header = req.getAllResponseHeaders();
    if (header.indexOf('content-disposition:') !== -1)
      return header;
    else
      return null;
  } catch (error) {
    return null;
  }
}
