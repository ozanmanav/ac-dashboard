import { assert } from "./assert";

export class StateMutator<
  TEntity extends { id: string } = any,
  TParent extends { [key: string]: any } = { [key: string]: any },
  TSourceKey extends keyof TParent = TParent[any]
  > {
  constructor(
    private parentBranch: TParent,
    private sourceKey: TParent[TSourceKey],
    private indexKey: string = "ByID"
  ) {}

  private getIndexKey(): TSourceKey {
    return (this.sourceKey + this.indexKey) as TParent[TSourceKey];
  }

  private getSortedKey(): TSourceKey {
    return (this.sourceKey + "Sorted") as TParent[TSourceKey];
  }

  public setIndexKey(key: string) {
    this.indexKey = key;
    return this;
  }

  private getIndexOf(id: string) {
    return this.parentBranch[this.getIndexKey()][id];
  }
  public updaOrUnshift(element: TEntity) {
    const index = this.parentBranch[this.getIndexKey()][element.id];
    if (index !== undefined) {
      this.parentBranch[this.sourceKey][this.getIndexOf(element.id)] = element;
    } else {
      this.unshift(element);
    }
  }

  public updateOrAdd(element: TEntity) {
    const index = this.parentBranch[this.getIndexKey()][element.id];
    if (index !== undefined) {
      this.parentBranch[this.sourceKey][this.getIndexOf(element.id)] = element;
    } else {
      this.add(element);
    }
  }

  public update(element: Partial<TEntity> & Pick<TEntity, 'id'>) {
    const index = this.parentBranch[this.getIndexKey()][element.id];
    Object.assign(this.parentBranch[this.sourceKey][this.getIndexOf(element.id)], element);
  }


  public find(id: string, existingFn?: ((res: TEntity, self: StateMutator<TEntity, TParent, TSourceKey>) => void) | undefined) 
  : TEntity | undefined {
    const res = (this.parentBranch[this.getIndexKey()] !== undefined &&
        this.parentBranch[this.getIndexKey()][id] !== undefined &&
        this.parentBranch[this.sourceKey][
          this.parentBranch[this.getIndexKey()][id]
        ]) ||
      undefined;
    return existingFn && res ? existingFn(res, this) : res;
  }

  public add(element: TEntity, unshift = false) {
    element.id &&
      (this.parentBranch[this.getIndexKey()][element.id] = this.parentBranch[
        this.sourceKey
      ].length);

    assert(
      this.parentBranch.hasOwnProperty(this.sourceKey),
      new ReferenceError(`Property [${this.sourceKey}]`)
    );
    if (unshift) this.parentBranch[this.sourceKey].unshift(element);
    else this.parentBranch[this.sourceKey].push(element);

    this.invalidateIndexesByKey();
    return this;
  }

  public unshift(element: TEntity) {
    element.id && (this.parentBranch[this.getIndexKey()][element.id] = 0);
    assert(
      this.parentBranch.hasOwnProperty(this.sourceKey),
      new ReferenceError(`Property [${this.sourceKey}]`)
    );
    this.parentBranch[this.sourceKey].unshift(element);
    this.invalidateIndexesByKey();
    return this;
  }

  public delete(id: string) {
    if (!id) return this;
    const index = this.parentBranch[this.getIndexKey()][id];
    const curr = this.parentBranch[this.sourceKey][index];
    if (index === undefined)
      return this;
    // assert(
    //   curr && curr.id !== id,
    //   `Wrong indexes has been found on the source of ${this.sourceKey}.`
    // );

    delete this.parentBranch[this.getIndexKey()][id];
    this.parentBranch[this.sourceKey].splice(index, 1);
    this.invalidateIndexesByKey();
    return this;
  }

  public createSorted(sortFn: (a: any, b: any) => 0 | 1 | 2 | -1 | -2) {
    Object.assign(this.parentBranch, {
      [this.getSortedKey()]: this.getSource().slice()
    });
    // this.parentBranch[this.getSortedKey()] = this.getSource().slice();
    this.parentBranch[this.getSortedKey()].sort(sortFn);
    // this.parentBranch[this.getSortedKey()] = this.getSource();
    return this;
  }

  protected getSource(): TEntity[] {
    return this.parentBranch[this.sourceKey];
  }

  public invalidateIndexesByKey() {
    Object.assign(this.parentBranch, {
      [this.getIndexKey()]: {}
    });
    this.parentBranch[this.sourceKey].forEach(
      (element: TEntity, index: number) => {
        this.parentBranch[this.getIndexKey()][element.id] = index;
      }
    );

    // assert(
    //   this.parentBranch[this.sourceKey].length ===
    //     Object.keys(this.parentBranch[this.getIndexKey()]).length,
    //   "Wrong indexes"
    // );
    return this;
  }

  public findAndAssign(id: string, patch: {}) {
    this.find(id, item => Object.assign(item, patch));

    return this;
  }
}
