export type ReqestMethod =
  | "POST"
  | "GET"
  | "PATCH"
  | "PUT"
  | "DELETE"
  | "OPTIONS";
