import { useState, useEffect, WheelEventHandler } from "react";

export function useScrollDirection() {
  const [direction, setDirection] = useState<"up" | "down" | "idle">("idle");
  useEffect(() => {
    let lastScroll = 0;
    const handler: WheelEventHandler<HTMLDivElement> = e => {
      const element: HTMLDivElement = e.target as HTMLDivElement;
      const currentScroll = element.scrollTop;
      if (currentScroll === 0) {
        return;
      }

      if (currentScroll > lastScroll) {
        setDirection("down");
      } else if (currentScroll < lastScroll) {
        setDirection("up");
      }
      // setPosition(element.scrollTop);
      lastScroll = element.scrollTop;
    };
    document.addEventListener("scroll", handler as any, true);

    return () => {
      document.removeEventListener("scroll", handler as any, true);
    };
  }, []);

  return direction;
}
