import { useRef, useEffect, useCallback } from "react";

export function useDebouncedCall(cb: (...args: any[]) => void, time: number, deps: readonly any[], argsFn?: (...args: any[]) => any[]) {
  const timeout = useRef<NodeJS.Timeout>();
  const caller = useCallback((...args) => {
    // const val = args.length === 1 ?  && (args[0].target || {}]).value : undefined;
    // if (args[0].nativeEvent)
    timeout.current && clearTimeout(timeout.current);
    const params = argsFn && argsFn(...args);
    timeout.current = setTimeout(() => {
      cb(...(params || args));
    }, time);
  }, [...deps, time]);

  useEffect(() => {
    return () => {
      timeout.current && clearTimeout(timeout.current);
    }
  }, [])


  return caller;
}
