import { Result } from "./Result";

// import { Result } from "@appcircle/shared/lib/services/Result";
// import {
//   NoResult,
//   TASK_MANAGER_API_INITIAL_VALUE,
//   TASK_MANAGER_API_INITIAL_VALUE
// } from "@appcircle/taskmanager/lib/TaskManagerContext";
export const API_INITIAL_VALUE = "API_INITIAL_VALUE";
export type API_INITIAL_VALUE = typeof API_INITIAL_VALUE;
export const API_NO_RESULT = "API_NO_RESULT";
export type API_NO_RESULT = typeof API_NO_RESULT;
export declare type ApiResultType<T = any> =
  | Result<T>
  | Result<T[]>
  | API_NO_RESULT
  | API_INITIAL_VALUE;
