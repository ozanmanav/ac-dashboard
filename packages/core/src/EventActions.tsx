enum EventActions {
  Cancelled = "Cancelled",
  Completed = "Completed"
}

export default EventActions;
