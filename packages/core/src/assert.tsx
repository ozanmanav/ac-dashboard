export function assert(cond: any, error: Error | string) {
  if (!cond) throw error;
}
