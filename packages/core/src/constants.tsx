export const MIN_NAME_LENGTH = 3;
export const MAX_NAME_LENGTH = 31;
export const ProfileNameRegexp = `^[A-Za-z\\d-_\.\& ]{0,${MAX_NAME_LENGTH}}$`;
