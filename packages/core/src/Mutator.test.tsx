import "jest-dom/extend-expect";
import "@testing-library/cleanup-after-each";
import { StateMutator } from "./Mutator";
import "jest";

type child = { id: string; name: string };
let mock: {
  parent: { children: child[]; childrenByID: { [key: string]: number } };
};

describe("Mutator", () => {
  beforeEach(() => {
    mock = {
      parent: {
        children: [
          { id: "1", name: "child1" },
          { id: "2", name: "child2" },
          { id: "3", name: "child3" }
        ],
        childrenByID: { "1": 0, "2": 1, "3": 2 }
      }
    };
  });
  it("finds a child", () => {
    const mutator = new StateMutator<child, typeof mock.parent>(
      mock.parent,
      "children"
    );
    expect(mutator.find("3")).toEqual({
      id: "3",
      name: "child3"
    });
  });

  it("adds a child", () => {
    const mutator = new StateMutator<child, typeof mock.parent>(
      mock.parent,
      "children"
    );
    mutator.add({
      id: "4",
      name: "child4"
    });
    expect(mutator.find("4")).toEqual({
      id: "4",
      name: "child4"
    });
  });

  it("deletes a child", () => {
    const mutator = new StateMutator<child, typeof mock.parent>(
      mock.parent,
      "children"
    );
    expect(mutator.find("3")).toEqual({
      id: "3",
      name: "child3"
    });
    mutator.delete("3");
    expect(mutator.find("3")).toEqual(undefined);
  });
});
