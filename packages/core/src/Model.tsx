export interface ISearchable<T = any> extends IListable<T> {
  search(text: string): T[];
  findById(id: string): T | undefined;
}

export interface IListable<T> {
  getAll(): T[];
}

export type ModelStateBase = {};

export abstract class Model<TState extends ModelStateBase = any, TResult = any>
  implements ISearchable<TResult> {
  constructor(protected state: TState) {
    this.init();
  }
  protected abstract init(): void;
  abstract findById(id: string): TResult | undefined;
  toObject() {
    return this.state;
  }

  abstract search(text: string): TResult[];

  abstract getAll(): TResult[];

  abstract length(): number;

  abstract isLoaded(): boolean;
}
