export enum ActionMethods {
  Update = "update",
  Add = "add",
  Delete = "delete",
  Get = "get"
}
