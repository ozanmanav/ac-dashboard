import { createContext, PropsWithChildren, useState } from "react";
import React from "react";

const CacheContext = createContext(new WeakMap());
const UpdateCacheContext = createContext<React.Dispatch<React.SetStateAction<WeakMap<object, any>>>>(() => { });

export function CircuitBreakerCacheProvider(props: PropsWithChildren<{}>) {
  const [cache, setCache] = useState(() => new WeakMap());

  return <CacheContext.Provider value={cache}>
    <UpdateCacheContext.Provider value={setCache}>
      {props.children}
    </UpdateCacheContext.Provider>
  </CacheContext.Provider>
}
