import React, {
  createContext,
  useState,
  useCallback,
  PropsWithChildren,
  useContext,
  useEffect,
  useReducer
} from "react";
import { CircuitBreakerCacheProvider } from "./cacheProvider";
import produce from "immer";

// https://github.com/abhinavdhasmana/circuitBreaker/blob/master/client/src/circuitBreaker.js
// https://itnext.io/understand-circuitbreaker-design-pattern-with-simple-practical-example-92a752615b42
// https://martinfowler.com/bliki/CircuitBreaker.html

// const axios = require('axios');

export type CircuitBreakerStatusType = "OPEN" | "CLOSED" | "HALF-OPEN";
const StatusCotext = createContext<CircuitBreakerStatusType>("CLOSED");
const UpdateStatusCotext = createContext({});

function CircuitBreaker(props: PropsWithChildren<{}>) {
  const status = useCircuitBreakerStatus();
  const state = useCircuitBreakerState();

  return <React.Fragment>{props.children}</React.Fragment>;
}

type actions = { type: "reset" } | { type: "addFailure"; payload: {} };
type state = { failures: {}[] };
const initialState = { failures: [] };
const CircuitBreakerState = createContext<state>(initialState);
const UpdateCircuitBreakerState = createContext<React.Dispatch<actions>>(
  () => {}
);

const reducer: React.Reducer<state, actions> = (state, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case "addFailure":
        draft.failures.push(action.payload);
        break;
      case "reset":
        draft.failures = [];
        break;
    }
  });
};

export function useCircuitBreakerStatus() {
  return useContext(StatusCotext);
}

export function useCircuitBreakerDispatch() {
  return useContext(UpdateCircuitBreakerState);
}

export function useCircuitBreakerState() {
  return useContext(CircuitBreakerState);
}

export function CircuitBreakerProvider(props: PropsWithChildren<{}>) {
  const [status, setStatus] = useState<CircuitBreakerStatusType>("CLOSED");
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <StatusCotext.Provider value={status}>
      <UpdateStatusCotext.Provider value={setStatus}>
        <CircuitBreakerCacheProvider>
          <CircuitBreakerState.Provider value={state}>
            <UpdateCircuitBreakerState.Provider value={dispatch}>
              <CircuitBreaker>{props.children}</CircuitBreaker>
            </UpdateCircuitBreakerState.Provider>
          </CircuitBreakerState.Provider>
        </CircuitBreakerCacheProvider>
      </UpdateStatusCotext.Provider>
    </StatusCotext.Provider>
  );
}

export function useCircuitBreaker() {
  const runner = useCallback(() => [], []);

  return runner;
}

// class CircuitBreaker {
//   constructor(timeout, failureThreshold, retryTimePeriod) {
//     // We start in a closed state hoping that everything is fine
//     this.state = 'CLOSED';
//     // Number of failures we receive from the depended service before we change the state to 'OPEN'
//     this.failureThreshold = failureThreshold;
//     // Timeout for the API request.
//     this.timeout = timeout;
//     // Time period after which a fresh request be made to the dependent
//     // service to check if service is up.
//     this.retryTimePeriod = retryTimePeriod;
//     this.lastFailureTime = null;
//     this.failureCount = 0;
//   }

//   // reset all the parameters to the initial state when circuit is initialized
//   reset() {
//     this.failureCount = 0;
//     this.lastFailureTime = null;
//     this.state = 'CLOSED';
//   }

//   // Set the current state of our circuit breaker.
//   setState() {
//     if (this.failureCount > this.failureThreshold) {
//       if ((Date.now() - this.lastFailureTime) > this.retryTimePeriod) {
//         this.state = 'HALF-OPEN';
//       } else {
//         this.state = 'OPEN';
//       }
//     } else {
//       this.state = 'CLOSED';
//     }
//   }

//   recordFailure() {
//     this.failureCount += 1;
//     this.lastFailureTime = Date.now();
//   }

// async call(urlToCall) {
//   // Determine the current state of the circuit.
//   this.setState();
//   switch (this.state) {
//     case 'OPEN':
//       // return  cached response if no the circuit is in OPEN state
//       return { data: 'this is stale response' };
//     // Make the API request if the circuit is not OPEN
//     case 'HALF-OPEN':
//     case 'CLOSED':
//       try {
//         const response = await axios({
//           url: urlToCall,
//           timeout: this.timeout,
//           method: 'get',
//         });
//         // Yay!! the API responded fine. Lets reset everything.
//         this.reset();
//         return response;
//       } catch (err) {
//         // Uh-oh!! the call still failed. Lets update that in our records.
//         this.recordFailure();
//         throw new Error(err);
//       }
//     default:
//       console.log('This state should never be reached');
//       return 'unexpected state in the state machine';
//   }
//   }
// }

// module.exports = CircuitBreaker;
