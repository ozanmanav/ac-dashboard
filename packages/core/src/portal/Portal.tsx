import React, {
  createContext,
  useRef,
  useEffect,
  useMemo,
  useContext,
  PropsWithChildren
} from "react";
import { createPortal } from "react-dom";

const PortalRootContext = createContext<HTMLDivElement | null>(null);
export function PortalRootProvider(props: PropsWithChildren<{}>) {
  const rootElemRef = useRef<HTMLDivElement>(null);

  return (
    <PortalRootContext.Provider value={rootElemRef.current}>
      <div ref={rootElemRef}></div>
      {props.children}
    </PortalRootContext.Provider>
  );
}

export function Portal(props: PropsWithChildren<{}>): React.ReactPortal {
  const root = useContext(PortalRootContext);
  const parent = useRef<HTMLDivElement>(
    useMemo(() => document.createElement("div"), [])
  );
  useEffect(() => {
    if (root) {
      root.appendChild(parent.current);
    }
    return () => {
      parent.current.remove();
    };
  }, [root]);

  return createPortal(props.children, parent.current);
}
