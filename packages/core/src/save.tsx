import { saveAs } from "file-saver";

const FileTypes = {
  binary: "application/octet-stream",
  text: "text/plain;charset=utf-8"
};

export function save(
  binary: BlobPart,
  name: (string | null)[],
  type: keyof typeof FileTypes = "binary"
) {
  var blob = new Blob([binary], {
    type: FileTypes[type]
  });

  saveAs(blob, name[0] || name[1] || "downloaded");
}
