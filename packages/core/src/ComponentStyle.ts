export enum ComponentStyle {
  Danger = "danger",
  Warning = "warning",
  Secondary = "secondary",
  Primary = "primary"
}
