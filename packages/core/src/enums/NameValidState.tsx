export enum NameValidState {
  VALID = "valid",
  TOO_LONG = "long",
  TOO_SHORT = "short",
  INVALID_CHARACTER = "invalid-character",
  NAME_EXISTS = "name-exists"
}
