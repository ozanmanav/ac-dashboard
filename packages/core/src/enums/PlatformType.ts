import { OS } from "./OS";

export enum PlatformType {
  "Swift/Objective-C" = 1,
  "Java/Kotlin" = 2,
  "Smartface" = 3
}

export const PlatformTypeToOS = [OS.ios, OS.android, OS.ios, OS.android];

export const platformTypeToText = (buildPlatformType: PlatformType, os: OS) => {
  switch (buildPlatformType) {
    case 1:
      return PlatformType[buildPlatformType];

    case 2:
      return PlatformType[buildPlatformType];

    case 3:
      if (os === OS.android) {
        return PlatformType[buildPlatformType] + "/Android";
      } else if (os === OS.ios) {
        return PlatformType[buildPlatformType] + "/iOS";
      }
  }
};
