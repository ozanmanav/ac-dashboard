import { OSType } from "./OSType";

export const OSValue: { [key: number]: OSType } = { 1: "ios", 2: "android" };
