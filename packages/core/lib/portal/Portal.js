import React, { createContext, useRef, useEffect, useMemo, useContext } from "react";
import { createPortal } from "react-dom";
var PortalRootContext = createContext(null);
export function PortalRootProvider(props) {
    var rootElemRef = useRef(null);
    return (React.createElement(PortalRootContext.Provider, { value: rootElemRef.current },
        React.createElement("div", { ref: rootElemRef }),
        props.children));
}
export function Portal(props) {
    var root = useContext(PortalRootContext);
    var parent = useRef(useMemo(function () { return document.createElement("div"); }, []));
    useEffect(function () {
        if (root) {
            root.appendChild(parent.current);
        }
        return function () {
            parent.current.remove();
        };
    }, [root]);
    return createPortal(props.children, parent.current);
}
//# sourceMappingURL=Portal.js.map