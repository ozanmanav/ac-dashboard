import React, { PropsWithChildren } from "react";
export declare function PortalRootProvider(props: PropsWithChildren<{}>): JSX.Element;
export declare function Portal(props: PropsWithChildren<{}>): React.ReactPortal;
//# sourceMappingURL=Portal.d.ts.map