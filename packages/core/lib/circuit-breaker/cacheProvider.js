import { createContext, useState } from "react";
import React from "react";
var CacheContext = createContext(new WeakMap());
var UpdateCacheContext = createContext(function () { });
export function CircuitBreakerCacheProvider(props) {
    var _a = useState(function () { return new WeakMap(); }), cache = _a[0], setCache = _a[1];
    return React.createElement(CacheContext.Provider, { value: cache },
        React.createElement(UpdateCacheContext.Provider, { value: setCache }, props.children));
}
//# sourceMappingURL=cacheProvider.js.map