import React, { createContext, useState, useCallback, useContext, useReducer } from "react";
import { CircuitBreakerCacheProvider } from "./cacheProvider";
import produce from "immer";
var StatusCotext = createContext("CLOSED");
var UpdateStatusCotext = createContext({});
function CircuitBreaker(props) {
    var status = useCircuitBreakerStatus();
    var state = useCircuitBreakerState();
    return React.createElement(React.Fragment, null, props.children);
}
var initialState = { failures: [] };
var CircuitBreakerState = createContext(initialState);
var UpdateCircuitBreakerState = createContext(function () { });
var reducer = function (state, action) {
    return produce(state, function (draft) {
        switch (action.type) {
            case "addFailure":
                draft.failures.push(action.payload);
                break;
            case "reset":
                draft.failures = [];
                break;
        }
    });
};
export function useCircuitBreakerStatus() {
    return useContext(StatusCotext);
}
export function useCircuitBreakerDispatch() {
    return useContext(UpdateCircuitBreakerState);
}
export function useCircuitBreakerState() {
    return useContext(CircuitBreakerState);
}
export function CircuitBreakerProvider(props) {
    var _a = useState("CLOSED"), status = _a[0], setStatus = _a[1];
    var _b = useReducer(reducer, initialState), state = _b[0], dispatch = _b[1];
    return (React.createElement(StatusCotext.Provider, { value: status },
        React.createElement(UpdateStatusCotext.Provider, { value: setStatus },
            React.createElement(CircuitBreakerCacheProvider, null,
                React.createElement(CircuitBreakerState.Provider, { value: state },
                    React.createElement(UpdateCircuitBreakerState.Provider, { value: dispatch },
                        React.createElement(CircuitBreaker, null, props.children)))))));
}
export function useCircuitBreaker() {
    var runner = useCallback(function () { return []; }, []);
    return runner;
}
// class CircuitBreaker {
//   constructor(timeout, failureThreshold, retryTimePeriod) {
//     // We start in a closed state hoping that everything is fine
//     this.state = 'CLOSED';
//     // Number of failures we receive from the depended service before we change the state to 'OPEN'
//     this.failureThreshold = failureThreshold;
//     // Timeout for the API request.
//     this.timeout = timeout;
//     // Time period after which a fresh request be made to the dependent
//     // service to check if service is up.
//     this.retryTimePeriod = retryTimePeriod;
//     this.lastFailureTime = null;
//     this.failureCount = 0;
//   }
//   // reset all the parameters to the initial state when circuit is initialized
//   reset() {
//     this.failureCount = 0;
//     this.lastFailureTime = null;
//     this.state = 'CLOSED';
//   }
//   // Set the current state of our circuit breaker.
//   setState() {
//     if (this.failureCount > this.failureThreshold) {
//       if ((Date.now() - this.lastFailureTime) > this.retryTimePeriod) {
//         this.state = 'HALF-OPEN';
//       } else {
//         this.state = 'OPEN';
//       }
//     } else {
//       this.state = 'CLOSED';
//     }
//   }
//   recordFailure() {
//     this.failureCount += 1;
//     this.lastFailureTime = Date.now();
//   }
// async call(urlToCall) {
//   // Determine the current state of the circuit.
//   this.setState();
//   switch (this.state) {
//     case 'OPEN':
//       // return  cached response if no the circuit is in OPEN state
//       return { data: 'this is stale response' };
//     // Make the API request if the circuit is not OPEN
//     case 'HALF-OPEN':
//     case 'CLOSED':
//       try {
//         const response = await axios({
//           url: urlToCall,
//           timeout: this.timeout,
//           method: 'get',
//         });
//         // Yay!! the API responded fine. Lets reset everything.
//         this.reset();
//         return response;
//       } catch (err) {
//         // Uh-oh!! the call still failed. Lets update that in our records.
//         this.recordFailure();
//         throw new Error(err);
//       }
//     default:
//       console.log('This state should never be reached');
//       return 'unexpected state in the state machine';
//   }
//   }
// }
// module.exports = CircuitBreaker;
//# sourceMappingURL=circuitBreaker.js.map