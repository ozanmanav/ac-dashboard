import React, { PropsWithChildren } from "react";
export declare type CircuitBreakerStatusType = "OPEN" | "CLOSED" | "HALF-OPEN";
declare type actions = {
    type: "reset";
} | {
    type: "addFailure";
    payload: {};
};
declare type state = {
    failures: {}[];
};
export declare function useCircuitBreakerStatus(): CircuitBreakerStatusType;
export declare function useCircuitBreakerDispatch(): React.Dispatch<actions>;
export declare function useCircuitBreakerState(): state;
export declare function CircuitBreakerProvider(props: PropsWithChildren<{}>): JSX.Element;
export declare function useCircuitBreaker(): () => never[];
export {};
//# sourceMappingURL=circuitBreaker.d.ts.map