var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import { useRef, useEffect, useCallback } from "react";
export function useDebouncedCall(cb, time, deps, argsFn) {
    var timeout = useRef();
    var caller = useCallback(function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        // const val = args.length === 1 ?  && (args[0].target || {}]).value : undefined;
        // if (args[0].nativeEvent)
        timeout.current && clearTimeout(timeout.current);
        var params = argsFn && argsFn.apply(void 0, args);
        timeout.current = setTimeout(function () {
            cb.apply(void 0, (params || args));
        }, time);
    }, __spreadArrays(deps, [time]));
    useEffect(function () {
        return function () {
            timeout.current && clearTimeout(timeout.current);
        };
    }, []);
    return caller;
}
//# sourceMappingURL=useDebouncedCall.js.map