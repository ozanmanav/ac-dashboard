import { useState, useEffect } from "react";
export function useScrollDirection() {
    var _a = useState("idle"), direction = _a[0], setDirection = _a[1];
    useEffect(function () {
        var lastScroll = 0;
        var handler = function (e) {
            var element = e.target;
            var currentScroll = element.scrollTop;
            if (currentScroll === 0) {
                return;
            }
            if (currentScroll > lastScroll) {
                setDirection("down");
            }
            else if (currentScroll < lastScroll) {
                setDirection("up");
            }
            // setPosition(element.scrollTop);
            lastScroll = element.scrollTop;
        };
        document.addEventListener("scroll", handler, true);
        return function () {
            document.removeEventListener("scroll", handler, true);
        };
    }, []);
    return direction;
}
//# sourceMappingURL=useScrollDirection.js.map