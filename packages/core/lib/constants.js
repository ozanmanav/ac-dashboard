export var MIN_NAME_LENGTH = 3;
export var MAX_NAME_LENGTH = 31;
export var ProfileNameRegexp = "^[A-Za-z\\d-_.& ]{0," + MAX_NAME_LENGTH + "}$";
//# sourceMappingURL=constants.js.map