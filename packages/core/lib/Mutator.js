import { assert } from "./assert";
var StateMutator = /** @class */ (function () {
    function StateMutator(parentBranch, sourceKey, indexKey) {
        if (indexKey === void 0) { indexKey = "ByID"; }
        this.parentBranch = parentBranch;
        this.sourceKey = sourceKey;
        this.indexKey = indexKey;
    }
    StateMutator.prototype.getIndexKey = function () {
        return (this.sourceKey + this.indexKey);
    };
    StateMutator.prototype.getSortedKey = function () {
        return (this.sourceKey + "Sorted");
    };
    StateMutator.prototype.setIndexKey = function (key) {
        this.indexKey = key;
        return this;
    };
    StateMutator.prototype.getIndexOf = function (id) {
        return this.parentBranch[this.getIndexKey()][id];
    };
    StateMutator.prototype.updaOrUnshift = function (element) {
        var index = this.parentBranch[this.getIndexKey()][element.id];
        if (index !== undefined) {
            this.parentBranch[this.sourceKey][this.getIndexOf(element.id)] = element;
        }
        else {
            this.unshift(element);
        }
    };
    StateMutator.prototype.updateOrAdd = function (element) {
        var index = this.parentBranch[this.getIndexKey()][element.id];
        if (index !== undefined) {
            this.parentBranch[this.sourceKey][this.getIndexOf(element.id)] = element;
        }
        else {
            this.add(element);
        }
    };
    StateMutator.prototype.update = function (element) {
        var index = this.parentBranch[this.getIndexKey()][element.id];
        Object.assign(this.parentBranch[this.sourceKey][this.getIndexOf(element.id)], element);
    };
    StateMutator.prototype.find = function (id, existingFn) {
        var res = (this.parentBranch[this.getIndexKey()] !== undefined &&
            this.parentBranch[this.getIndexKey()][id] !== undefined &&
            this.parentBranch[this.sourceKey][this.parentBranch[this.getIndexKey()][id]]) ||
            undefined;
        return existingFn && res ? existingFn(res, this) : res;
    };
    StateMutator.prototype.add = function (element, unshift) {
        if (unshift === void 0) { unshift = false; }
        element.id &&
            (this.parentBranch[this.getIndexKey()][element.id] = this.parentBranch[this.sourceKey].length);
        assert(this.parentBranch.hasOwnProperty(this.sourceKey), new ReferenceError("Property [" + this.sourceKey + "]"));
        if (unshift)
            this.parentBranch[this.sourceKey].unshift(element);
        else
            this.parentBranch[this.sourceKey].push(element);
        this.invalidateIndexesByKey();
        return this;
    };
    StateMutator.prototype.unshift = function (element) {
        element.id && (this.parentBranch[this.getIndexKey()][element.id] = 0);
        assert(this.parentBranch.hasOwnProperty(this.sourceKey), new ReferenceError("Property [" + this.sourceKey + "]"));
        this.parentBranch[this.sourceKey].unshift(element);
        this.invalidateIndexesByKey();
        return this;
    };
    StateMutator.prototype.delete = function (id) {
        if (!id)
            return this;
        var index = this.parentBranch[this.getIndexKey()][id];
        var curr = this.parentBranch[this.sourceKey][index];
        if (index === undefined)
            return this;
        // assert(
        //   curr && curr.id !== id,
        //   `Wrong indexes has been found on the source of ${this.sourceKey}.`
        // );
        delete this.parentBranch[this.getIndexKey()][id];
        this.parentBranch[this.sourceKey].splice(index, 1);
        this.invalidateIndexesByKey();
        return this;
    };
    StateMutator.prototype.createSorted = function (sortFn) {
        var _a;
        Object.assign(this.parentBranch, (_a = {},
            _a[this.getSortedKey()] = this.getSource().slice(),
            _a));
        // this.parentBranch[this.getSortedKey()] = this.getSource().slice();
        this.parentBranch[this.getSortedKey()].sort(sortFn);
        // this.parentBranch[this.getSortedKey()] = this.getSource();
        return this;
    };
    StateMutator.prototype.getSource = function () {
        return this.parentBranch[this.sourceKey];
    };
    StateMutator.prototype.invalidateIndexesByKey = function () {
        var _a;
        var _this = this;
        Object.assign(this.parentBranch, (_a = {},
            _a[this.getIndexKey()] = {},
            _a));
        this.parentBranch[this.sourceKey].forEach(function (element, index) {
            _this.parentBranch[_this.getIndexKey()][element.id] = index;
        });
        // assert(
        //   this.parentBranch[this.sourceKey].length ===
        //     Object.keys(this.parentBranch[this.getIndexKey()]).length,
        //   "Wrong indexes"
        // );
        return this;
    };
    StateMutator.prototype.findAndAssign = function (id, patch) {
        this.find(id, function (item) { return Object.assign(item, patch); });
        return this;
    };
    return StateMutator;
}());
export { StateMutator };
//# sourceMappingURL=Mutator.js.map