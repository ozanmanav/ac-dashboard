export declare function createFakeID(): string;
export declare function convertFakeID(id: string): string;
export declare function isFakeID(id: string): boolean;
//# sourceMappingURL=fakeID.d.ts.map