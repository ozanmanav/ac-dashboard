import { MAX_NAME_LENGTH, ProfileNameRegexp } from "@appcircle/core/lib/constants";
export function alphaNummericTextMaybe(oldText, newText, max) {
    if (max === void 0) { max = MAX_NAME_LENGTH; }
    if (newText.length <= max &&
        new RegExp("^(?=.*$)[\\w]*(?:(?=.{1})[\\s]\\w+)*[\\s]?$").test(newText))
        return newText;
    else
        return oldText;
}
export function textWithSpecialsMaybe(oldText, newText, max) {
    if (max === void 0) { max = MAX_NAME_LENGTH; }
    // if (newText.length <= max && new RegExp(`^(?=.*$)[\\w]*(?:(?=.{1})[\\s][\\w-_.]+)*[\\s]?$`).test(newText))
    if (newText.length <= max && new RegExp(ProfileNameRegexp).test(newText))
        return newText;
    else
        return oldText;
}
//# sourceMappingURL=alphaNumericTextMaybe.js.map