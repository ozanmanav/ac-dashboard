export function createCopyName(entity, checkExistingFn) {
    var num = entity.name.match("[0-9]+$");
    var counter = num ? parseInt(num[0]) + 1 : 1;
    var namePattern = num
        ? entity.name.substring(0, num.index)
        : entity.name + "_";
    var newName = namePattern + counter;
    while (checkExistingFn(newName, entity.id) !== false) {
        newName = namePattern + ++counter;
    }
    return newName;
}
//# sourceMappingURL=createCopyName.js.map