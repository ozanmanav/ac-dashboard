import { isEmptyValue } from "./isEmptyValue";
export function emptyValueMaybe(val, orVal) {
    return isEmptyValue(val) ? orVal : val;
}
//# sourceMappingURL=emptyValueMaybe.js.map