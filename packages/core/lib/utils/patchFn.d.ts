export declare function patchFn<T>(targetFn: ((...args: T[]) => void) | undefined, additionFn: Function, callAferTargetCall?: boolean): (...args: T[]) => void;
//# sourceMappingURL=patchFn.d.ts.map