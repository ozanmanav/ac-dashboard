export declare function alphaNummericTextMaybe(oldText: string, newText: string, max?: number): string;
export declare function textWithSpecialsMaybe(oldText: string, newText: string, max?: number): string;
//# sourceMappingURL=alphaNumericTextMaybe.d.ts.map