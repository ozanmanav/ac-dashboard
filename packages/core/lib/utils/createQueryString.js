export function createQueryString() {
    var params = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        params[_i] = arguments[_i];
    }
    return "?" + params.map(function (param) { return param.join("="); }).join("&");
}
//# sourceMappingURL=createQueryString.js.map