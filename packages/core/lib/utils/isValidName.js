import { ProfileNameRegexp, MAX_NAME_LENGTH, MIN_NAME_LENGTH } from "../constants";
import { NameValidState } from "../enums/NameValidState";
export function isValidName(name) {
    if (new RegExp(ProfileNameRegexp).test(name))
        return NameValidState.VALID;
    var res = name.length > MAX_NAME_LENGTH
        ? NameValidState.TOO_LONG
        : name.length < MIN_NAME_LENGTH
            ? NameValidState.TOO_SHORT
            : NameValidState.INVALID_CHARACTER;
    return res;
}
//# sourceMappingURL=isValidName.js.map