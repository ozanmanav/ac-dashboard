import shortid from "shortid";
export function debounce(func, delay) {
    var inDebounce;
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var context = this;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(function () { return func.apply(context, args); }, delay);
    };
}
export function setIdsTo(arr) {
    return arr.map(function (el) { return (el.id = shortid.generate()); });
}
export function compareable(condition) {
    return function (left, right) {
        return condition() ? left : right;
    };
}
export function matchAnyCase(src, match) {
    return src.toLowerCase() === match;
}
export function iOSMaybe(os) {
    return os.toLowerCase() === "ios" ? "ios" : "android";
}
export function androidMaybe(os) {
    return os.toLowerCase() === "android" ? "android" : "ios";
}
//# sourceMappingURL=index.js.map