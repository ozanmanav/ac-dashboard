export function isEmptyValue(val) {
    return val === undefined || val === null || val === "";
}
//# sourceMappingURL=isEmptyValue.js.map