export function addSlashtoPath(url) {
    return url.indexOf("/") === -1 ? "/" + url : url;
}
//# sourceMappingURL=addSlashtoPath.js.map