export declare function readFilename(headers: string | XMLHttpRequest | null): string | null;
export declare function readContentDisposition(req: XMLHttpRequest): string | null;
//# sourceMappingURL=readContentDisposition.d.ts.map