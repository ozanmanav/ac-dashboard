import { shallowEqual } from "./shallowEqual";
export function compareUnorderedArray(arr1, arr2) {
    console.log(arr1, arr2);
    return arr1.length === arr2.length && shallowEqual(arr1.sort(), arr2.sort());
}
//# sourceMappingURL=compareUnorderedArray.js.map