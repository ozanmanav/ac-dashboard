export declare function createCopyName(entity: {
    id: string;
    name: string;
}, checkExistingFn: (name: string, id: string) => boolean): string;
//# sourceMappingURL=createCopyName.d.ts.map