export function checkMousePointerInElement(mouseX, mouseY, positionRect) {
    return (positionRect.left <= mouseX &&
        mouseX <= positionRect.left + positionRect.width &&
        positionRect.top <= mouseY &&
        mouseY <= positionRect.top + positionRect.height);
}
//# sourceMappingURL=checkMousePointerInElement.js.map