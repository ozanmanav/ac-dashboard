export function isObject(val) {
    return val !== null && !Array.isArray(val) && typeof val === "object";
}
//# sourceMappingURL=isObject.js.map