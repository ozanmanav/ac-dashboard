import { shallowEqual } from "./shallowEqual";
export function matchUnorderedArrays(arr1, arr2) {
    return arr1.length === arr2.length && shallowEqual(arr1.sort(), arr2.sort());
}
//# sourceMappingURL=matchUnorderedArrays.js.map