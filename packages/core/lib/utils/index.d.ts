import { OSType } from "../enums/OSType";
export declare function debounce(func: Function, delay: number): (this: any) => void;
export declare function setIdsTo(arr: Array<any>): Array<any>;
export declare function compareable(condition: () => boolean): <T = any, R = any>(left: T, right: R) => T | R;
export declare function matchAnyCase(src: string, match: string): boolean;
export declare function iOSMaybe(os: OSType): "android" | "ios";
export declare function androidMaybe(os: OSType): "android" | "ios";
//# sourceMappingURL=index.d.ts.map