export default function runPipe<T = string>(initialValue: T, ...args: ((content: T) => T)[]): T;
//# sourceMappingURL=runPipe.d.ts.map