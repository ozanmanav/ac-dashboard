export default function runPipe(initialValue) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return args.reduce(function (acc, curr) {
        return curr(acc);
    }, initialValue);
}
//# sourceMappingURL=runPipe.js.map