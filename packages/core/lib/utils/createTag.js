import shortid from "shortid";
export function createTag(text, modifiable, icon) {
    if (modifiable === void 0) { modifiable = true; }
    var tag = {
        id: shortid.generate(),
        modifiable: modifiable,
        text: text
    };
    icon && (tag.icon = icon);
    return tag;
}
//# sourceMappingURL=createTag.js.map