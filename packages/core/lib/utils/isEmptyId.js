import { isEmptyValue } from "./isEmptyValue";
export function isEmptyId(id) {
    return isEmptyValue(id) || id.indexOf("00000") === 0;
}
//# sourceMappingURL=isEmptyId.js.map