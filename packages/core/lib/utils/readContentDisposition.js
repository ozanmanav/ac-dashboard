export function readFilename(headers) {
    if (headers instanceof XMLHttpRequest) {
        return readFilename(readContentDisposition(headers));
    }
    else if (headers && headers.indexOf('content-disposition:') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(headers);
        if (matches != null && matches[1]) {
            return matches[1].replace(/['"]/g, '');
        }
    }
    return null;
}
export function readContentDisposition(req) {
    try {
        var header = req.getAllResponseHeaders();
        if (header.indexOf('content-disposition:') !== -1)
            return header;
        else
            return null;
    }
    catch (error) {
        return null;
    }
}
//# sourceMappingURL=readContentDisposition.js.map