export function patchFn(targetFn, additionFn, callAferTargetCall) {
    if (targetFn === void 0) { targetFn = function () { }; }
    if (callAferTargetCall === void 0) { callAferTargetCall = false; }
    var _targetFn = targetFn;
    targetFn = function () {
        var arg = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arg[_i] = arguments[_i];
        }
        !callAferTargetCall && additionFn.apply(void 0, arg);
        _targetFn.apply(void 0, arg);
        callAferTargetCall && additionFn.apply(void 0, arg);
    };
    return targetFn;
}
//# sourceMappingURL=patchFn.js.map