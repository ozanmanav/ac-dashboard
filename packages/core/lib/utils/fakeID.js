import shortid from "shortid";
var prefix = "fakeID";
export function createFakeID() {
    return prefix + shortid.generate();
}
export function convertFakeID(id) {
    return prefix + id;
}
export function isFakeID(id) {
    return id.startsWith(prefix);
}
//# sourceMappingURL=fakeID.js.map