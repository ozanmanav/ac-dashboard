export var ActionMethods;
(function (ActionMethods) {
    ActionMethods["Update"] = "update";
    ActionMethods["Add"] = "add";
    ActionMethods["Delete"] = "delete";
    ActionMethods["Get"] = "get";
})(ActionMethods || (ActionMethods = {}));
//# sourceMappingURL=ActionMethods.js.map