export declare enum ActionMethods {
    Update = "update",
    Add = "add",
    Delete = "delete",
    Get = "get"
}
//# sourceMappingURL=ActionMethods.d.ts.map