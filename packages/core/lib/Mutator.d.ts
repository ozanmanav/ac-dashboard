export declare class StateMutator<TEntity extends {
    id: string;
} = any, TParent extends {
    [key: string]: any;
} = {
    [key: string]: any;
}, TSourceKey extends keyof TParent = TParent[any]> {
    private parentBranch;
    private sourceKey;
    private indexKey;
    constructor(parentBranch: TParent, sourceKey: TParent[TSourceKey], indexKey?: string);
    private getIndexKey;
    private getSortedKey;
    setIndexKey(key: string): this;
    private getIndexOf;
    updaOrUnshift(element: TEntity): void;
    updateOrAdd(element: TEntity): void;
    update(element: Partial<TEntity> & Pick<TEntity, 'id'>): void;
    find(id: string, existingFn?: ((res: TEntity, self: StateMutator<TEntity, TParent, TSourceKey>) => void) | undefined): TEntity | undefined;
    add(element: TEntity, unshift?: boolean): this;
    unshift(element: TEntity): this;
    delete(id: string): this;
    createSorted(sortFn: (a: any, b: any) => 0 | 1 | 2 | -1 | -2): this;
    protected getSource(): TEntity[];
    invalidateIndexesByKey(): this;
    findAndAssign(id: string, patch: {}): this;
}
//# sourceMappingURL=Mutator.d.ts.map