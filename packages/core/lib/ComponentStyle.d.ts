export declare enum ComponentStyle {
    Danger = "danger",
    Warning = "warning",
    Secondary = "secondary",
    Primary = "primary"
}
//# sourceMappingURL=ComponentStyle.d.ts.map