export interface Result<TJson = any> {
    readonly headers: string;
    readonly ok: boolean;
    readonly status: number;
    readonly statusText: string;
    readonly type: string;
    readonly json: TJson;
    readonly url: string;
}
//# sourceMappingURL=Result.d.ts.map