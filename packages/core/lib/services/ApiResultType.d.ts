import { Result } from "./Result";
export declare const API_INITIAL_VALUE = "API_INITIAL_VALUE";
export declare type API_INITIAL_VALUE = typeof API_INITIAL_VALUE;
export declare const API_NO_RESULT = "API_NO_RESULT";
export declare type API_NO_RESULT = typeof API_NO_RESULT;
export declare type ApiResultType<T = any> = Result<T> | Result<T[]> | API_NO_RESULT | API_INITIAL_VALUE;
//# sourceMappingURL=ApiResultType.d.ts.map