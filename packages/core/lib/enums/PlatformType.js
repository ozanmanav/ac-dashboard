import { OS } from "./OS";
export var PlatformType;
(function (PlatformType) {
    PlatformType[PlatformType["Swift/Objective-C"] = 1] = "Swift/Objective-C";
    PlatformType[PlatformType["Java/Kotlin"] = 2] = "Java/Kotlin";
    PlatformType[PlatformType["Smartface"] = 3] = "Smartface";
})(PlatformType || (PlatformType = {}));
export var PlatformTypeToOS = [OS.ios, OS.android, OS.ios, OS.android];
export var platformTypeToText = function (buildPlatformType, os) {
    switch (buildPlatformType) {
        case 1:
            return PlatformType[buildPlatformType];
        case 2:
            return PlatformType[buildPlatformType];
        case 3:
            if (os === OS.android) {
                return PlatformType[buildPlatformType] + "/Android";
            }
            else if (os === OS.ios) {
                return PlatformType[buildPlatformType] + "/iOS";
            }
    }
};
//# sourceMappingURL=PlatformType.js.map