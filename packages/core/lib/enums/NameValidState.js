export var NameValidState;
(function (NameValidState) {
    NameValidState["VALID"] = "valid";
    NameValidState["TOO_LONG"] = "long";
    NameValidState["TOO_SHORT"] = "short";
    NameValidState["INVALID_CHARACTER"] = "invalid-character";
    NameValidState["NAME_EXISTS"] = "name-exists";
})(NameValidState || (NameValidState = {}));
//# sourceMappingURL=NameValidState.js.map