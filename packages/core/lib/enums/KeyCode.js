export var KeyCode;
(function (KeyCode) {
    KeyCode[KeyCode["ESC_KEY"] = 27] = "ESC_KEY";
    KeyCode[KeyCode["ENTER_KEY"] = 13] = "ENTER_KEY";
    KeyCode[KeyCode["BACKSPACE_KEY"] = 8] = "BACKSPACE_KEY";
    KeyCode[KeyCode["LEFT_ARROW"] = 37] = "LEFT_ARROW";
    KeyCode[KeyCode["UP_ARROW"] = 38] = "UP_ARROW";
    KeyCode[KeyCode["RIGHT_ARROW"] = 39] = "RIGHT_ARROW";
    KeyCode[KeyCode["DOWN_ARROW"] = 40] = "DOWN_ARROW";
})(KeyCode || (KeyCode = {}));
//# sourceMappingURL=KeyCode.js.map