export declare enum KeyCode {
    ESC_KEY = 27,
    ENTER_KEY = 13,
    BACKSPACE_KEY = 8,
    LEFT_ARROW = 37,
    UP_ARROW = 38,
    RIGHT_ARROW = 39,
    DOWN_ARROW = 40
}
//# sourceMappingURL=KeyCode.d.ts.map