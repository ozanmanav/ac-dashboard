import { OS } from "./OS";
export declare enum PlatformType {
    "Swift/Objective-C" = 1,
    "Java/Kotlin" = 2,
    "Smartface" = 3
}
export declare const PlatformTypeToOS: OS[];
export declare const platformTypeToText: (buildPlatformType: PlatformType, os: OS) => string | undefined;
//# sourceMappingURL=PlatformType.d.ts.map