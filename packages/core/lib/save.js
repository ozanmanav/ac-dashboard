import { saveAs } from "file-saver";
var FileTypes = {
    binary: "application/octet-stream",
    text: "text/plain;charset=utf-8"
};
export function save(binary, name, type) {
    if (type === void 0) { type = "binary"; }
    var blob = new Blob([binary], {
        type: FileTypes[type]
    });
    saveAs(blob, name[0] || name[1] || "downloaded");
}
//# sourceMappingURL=save.js.map