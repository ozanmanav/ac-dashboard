export interface ISearchable<T = any> extends IListable<T> {
    search(text: string): T[];
    findById(id: string): T | undefined;
}
export interface IListable<T> {
    getAll(): T[];
}
export declare type ModelStateBase = {};
export declare abstract class Model<TState extends ModelStateBase = any, TResult = any> implements ISearchable<TResult> {
    protected state: TState;
    constructor(state: TState);
    protected abstract init(): void;
    abstract findById(id: string): TResult | undefined;
    toObject(): TState;
    abstract search(text: string): TResult[];
    abstract getAll(): TResult[];
    abstract length(): number;
    abstract isLoaded(): boolean;
}
//# sourceMappingURL=Model.d.ts.map