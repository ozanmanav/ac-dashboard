var Model = /** @class */ (function () {
    function Model(state) {
        this.state = state;
        this.init();
    }
    Model.prototype.toObject = function () {
        return this.state;
    };
    return Model;
}());
export { Model };
//# sourceMappingURL=Model.js.map