export var ComponentStyle;
(function (ComponentStyle) {
    ComponentStyle["Danger"] = "danger";
    ComponentStyle["Warning"] = "warning";
    ComponentStyle["Secondary"] = "secondary";
    ComponentStyle["Primary"] = "primary";
})(ComponentStyle || (ComponentStyle = {}));
//# sourceMappingURL=ComponentStyle.js.map