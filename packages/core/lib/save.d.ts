declare const FileTypes: {
    binary: string;
    text: string;
};
export declare function save(binary: BlobPart, name: (string | null)[], type?: keyof typeof FileTypes): void;
export {};
//# sourceMappingURL=save.d.ts.map