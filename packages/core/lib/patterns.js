export var ComplexPasswordRegexp = "^(?=[a-zA-Z0-9#_\\-\\*@\\$\\!?]{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[#_\\-\\*@\\$\\!])(?=.*?[0-9]).*";
export var PasswordRegexp = "^(?=.{8,}$)(?=.*?[a-zA-Z])(?=.*?[0-9]).*";
//# sourceMappingURL=patterns.js.map