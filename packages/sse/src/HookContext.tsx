import { createContext } from "react";
import { Subject } from "rxjs";
import { HookResultEvent } from "@appcircle/shared/lib/services/HookResultEvent";

export const HookContext = createContext<Subject<HookResultEvent>>(
  new Subject()
);
