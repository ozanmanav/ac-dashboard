export type HookEventActions =
  | "Progress"
  | "Created"
  | "Running"
  | "Completed"
  | "Cancelled"
  | "Failed";
