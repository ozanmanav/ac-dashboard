import { PropsWithChildren } from "react";
import React from "react";
import { HookContext } from "./HookContext";
import { Subject } from "rxjs";
import { HookResultEvent } from "@appcircle/shared/lib/services/HookResultEvent";
export function HookProvider(
  props: PropsWithChildren<{ hookListener$: Subject<HookResultEvent> }>
) {
  return (
    <HookContext.Provider value={props.hookListener$}>
      {props.children}
    </HookContext.Provider>
  );
}
