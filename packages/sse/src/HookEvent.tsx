import { HookEventActions } from "./HookEventActions";
import { ClientAlertType } from "@appcircle/shared/lib/ClientAlertType";
import { ClientMessageType } from "@appcircle/shared/lib/ClientMessageType";
import { EventTaskStatus } from "@appcircle/shared/lib/EventTaskStatus";

export type HookEvent<
  T extends {
    id: string;
  } = any
> = {
  clientAlertType: ClientAlertType;
  clientMessageType: ClientMessageType;
  cultureInfo: null;
  data: T;
  eventAction: HookEventActions;
  eventName: string;
  isSuccess: boolean;
  message: string;
  metadata: {
    [key: string]: any;
  };
  moduleName: string;
  notificationSendDate: string;
  organizationId: string;
  relatedAction: string;
  targetName: string;
  taskId: string;
  title: string;
  userId: string;
  status: EventTaskStatus;
};
