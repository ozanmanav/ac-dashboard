/// <reference types="react" />
import { Subject } from "rxjs";
import { HookResultEvent } from "@appcircle/shared/lib/services/HookResultEvent";
export declare const HookContext: import("react").Context<Subject<HookResultEvent>>;
//# sourceMappingURL=HookContext.d.ts.map