export declare type HookEventActions = "Progress" | "Created" | "Running" | "Completed" | "Cancelled" | "Failed";
//# sourceMappingURL=HookEventActions.d.ts.map