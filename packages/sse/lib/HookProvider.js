import React from "react";
import { HookContext } from "./HookContext";
export function HookProvider(props) {
    return (React.createElement(HookContext.Provider, { value: props.hookListener$ }, props.children));
}
//# sourceMappingURL=HookProvider.js.map