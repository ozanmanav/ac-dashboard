import { PropsWithChildren } from "react";
import { Subject } from "rxjs";
import { HookResultEvent } from "@appcircle/shared/lib/services/HookResultEvent";
export declare function HookProvider(props: PropsWithChildren<{
    hookListener$: Subject<HookResultEvent>;
}>): JSX.Element;
//# sourceMappingURL=HookProvider.d.ts.map