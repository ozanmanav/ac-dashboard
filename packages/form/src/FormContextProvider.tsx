import React, { PropsWithChildren, useReducer, Reducer, useMemo } from "react";
import { FormState } from "./Form";
import { FormActions } from "./FormActions";
import { formReducer, FormContext } from "./FormContext";
export function FormContextProvider(props: PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer<
    Reducer<
      {
        [id: string]: FormState;
      },
      FormActions
    >
  >(formReducer, {});
  //TODO: Maybe create two contexts for state and dispatch each
  const value = useMemo(() => {
    return {
      state,
      dispatch
    };
  }, [state]);
  return (
    <FormContext.Provider value={value}>{props.children}</FormContext.Provider>
  );
}
