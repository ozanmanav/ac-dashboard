import { initialFormStatus } from "./FormContext";
export function createEmptyForm(id: string) {
  return {
    id: id,
    fields: {},
    initialData: {},
    data: {},
    status: { ...initialFormStatus }
  };
}
