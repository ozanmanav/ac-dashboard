import {
  useMemo,
  useEffect,
  useCallback,
  useState,
  useRef,
  ReactElement
} from "react";
import { useFormContext } from "./useFormContext";
import {
  FormFieldType,
  FormStatus,
  FormElementType,
  WithDisabled,
  WithoutDisabled
} from "./Form";
import React from "react";
import classNames from "classnames";
import { FormComponentError } from "./FormComponentError";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { FormLabel } from "./FormLabel";

export function FormComponent<
  TElProps extends FormElementType = FormElementType
>(props: FormComponentProps<TElProps> & TElProps & WithDisabled): ReactElement;
export function FormComponent<
  TElProps extends FormElementType = FormElementType
>(
  props: FormComponentProps<TElProps> & TElProps & WithoutDisabled
): ReactElement;
export function FormComponent<
  TElProps extends FormElementType = FormElementType
>(props: FormComponentProps<TElProps> & TElProps) {
  const Element = props.element;
  const [formState, formService] = useFormContext(props.formID);
  const [initialValue, setinitialValue] = useState(props.value);
  const autoUnload = useRef(false);
  autoUnload.current = !!formState?.autoUnload;

  useEffect(() => {
    props.formProps &&
      formService.registerField(props.formID, {
        ...props.formProps,
        value: isEmptyValue(props.value) ? props.formProps.value : props.value
      });
    return () => {
      if (autoUnload.current === true)
        props.formProps &&
          formService.removeField(props.formID, props.formProps.name);
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (!props.formProps) return;
    if (initialValue === undefined && props.value !== undefined) {
      formService.updateFormDataValue(props.formProps.name, props.value);
      setinitialValue(props.value);
    } else if (initialValue !== undefined && props.value === undefined) {
      formService.updateFormDataValue(props.formProps.name, props.value);
      setinitialValue(props.value);
    }
  }, [props.value]);

  const field =
    props.formProps && formState && formState.fields[props.formProps.name];
  const isDirty = field && field.isDirty;
  // const fieldIndex = field.index;
  const onChange = useCallback(
    param => {
      const val =
        typeof param === "object" &&
        param !== null &&
        param.target &&
        param.target.value !== undefined
          ? param.target.value
          : param;
      formService.updateFormDataValue(props.formProps.name, val);
      props.onChange && props.onChange(val);
    },
    [formService, props]
  );

  const className = classNames("FormComponent_wrappedElement", props.className);
  const errors = useMemo(
    () =>
      (props.formProps &&
        formState &&
        formState.errors &&
        formState.errors.filter(
          error => error.name === props.formProps.name
        )) ||
      [],
    [formState, props.formProps && props.formProps.name]
  );

  const hasError = useMemo(
    () =>
      isDirty &&
      !!errors.length &&
      formState?.status.fieldsStatus === FormStatus.FIELD_ERROR &&
      formState?.status.isDirty,
    [errors.length, formState, isDirty]
  );

  const errorMessage =
    errors[0] && errors[0].message !== undefined
      ? errors[0].message
      : formState &&
        formState.errorMessages &&
        errors[0] &&
        formState.errorMessages[errors[0].type]
      ? formState.errorMessages[errors[0].type]
      : "";

  const comp = useMemo(
    () =>
      !Element ? (
        Element
      ) : (
        <div
          className={
            "FormComponent " + (props.isDisabled ? props.disabledClassName : "")
          }
        >
          <FormLabel label={props.formFieldLabel} tip={props.tip} />
          {props.formProps.disableErrorIndicator !== true &&
            props.formProps.required === true && (
              <div
                className={
                  "requiredIndicator requiredIndicator-pass--" +
                  (!hasError && (!errors || !errors.length))
                }
              ></div>
            )}
          <Element
            {...props}
            onChange={onChange}
            value={
              props.formProps &&
              formState &&
              formState.fields[props.formProps.name] &&
              formState.fields[props.formProps.name].value !== undefined
                ? formState.fields[props.formProps.name].value
                : props.value !== initialValue
                ? props.value
                : props.formProps && props.formProps.value
            }
            className={
              hasError
                ? className + " " + props.formProps.errorClass
                : className
            }
          />
          <div className="FormComponent-margin"> </div>
          {hasError && props.formProps && !!errorMessage && (
            <FormComponentError
              errorMessage={errorMessage}
              name={props.formProps.name}
            />
          )}
        </div>
      ),
    [className, formState, hasError, onChange, props, errorMessage, Element]
  );
  return comp;
}

export type FormComponentProps<
  TElementProps extends FormElementType = any,
  U = TElementProps["value"] extends infer U ? U : any
> = {
  element: React.ComponentType<TElementProps> | null;
  formFieldLabel?: string;
  formID: string;
  tip?: string;
  formProps: FormFieldType & { errorClass?: string };
  initialValue?: any;
} & FormElementType<U>;
