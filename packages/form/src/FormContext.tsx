import { createContext, Dispatch } from "react";
import {
  FormState,
  FormStatus,
  FormFieldType,
  FormFieldErrorType,
  FormFieldsDict,
  FormStatusType,
  AnyFormFieldRule,
  Rule
} from "./Form";
import produce from "immer";
import { ValidValue, hasRuleType } from "./formValidators";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { arrayMaybe } from "@appcircle/taskmanager/lib/TaskManagerReducer";
import { formValidator } from "./formValidator";
import { shallowEqual } from "@appcircle/core/lib/utils/shallowEqual";
import { FormActions } from "./FormActions";
import { isDirty } from "./isDirty";
import { createEmptyForm } from "./createEmptyForm";

export type FormContextType = {
  state: { [id: string]: FormState };
  dispatch: Dispatch<FormActions>;
};

export const FormContext = createContext<FormContextType>({
  dispatch: () => {},
  state: {}
});

export type ValidationReducer<T extends Rule = AnyFormFieldRule> = (
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: T
) => typeof ValidValue | FormFieldErrorType;

export const fields: { [key: string]: FormFieldType[] } = {};
type StateFormType = { [id: string]: FormState };

function updateInitialData(
  state: StateFormType,
  formID: string,
  fields: FormFieldType[] | FormFieldType
) {
  (Array.isArray(fields) ? fields : [fields]).forEach(field => {
    if (
      state[formID].initialData &&
      !state[formID].initialData.hasOwnProperty(field.name)
    ) {
      state[formID].initialData[field.name] = field.value;
    }
  });
}

function updateFields(
  state: StateFormType,
  formID: string,
  fields: FormFieldType[] | FormFieldType,
  dirty = false
) {
  if (!state[formID]) {
    return;
  }

  (Array.isArray(fields) ? fields : [fields]).forEach(field => {
    if (state[formID].fields[field.name]) {
      Object.assign(state[formID].fields[field.name], field);
    } else {
      state[formID].fields[field.name] = field;
    }

    if (!isEmptyValue(field.value))
      state[formID].fields[field.name].isDirty = state[formID].initialData
        ? !shallowEqual(state[formID].initialData[field.name], field.value)
        : dirty;
    state[formID].data[field.name] = field.value;
  });

  state[formID].errors = formValidator(state[formID].fields);
}

export const initialFormStatus: FormStatusType = {
  fieldsStatus: FormStatus.IDLE,
  isDirty: false,
  submission: FormStatus.IDLE
};

function updateStatus(state: StateFormType, formID: string) {
  if (Object.keys(state[formID].fields).length === 0 && !state[formID].data) {
    state[formID].status.fieldsStatus = FormStatus.INITIALIZED;
    state[formID].status.submission = FormStatus.IDLE;
    state[formID].status.isDirty = false;
  } else if (Object.keys(state[formID].fields).length > 0) {
    (state[formID].errors || []).length
      ? (state[formID].status.fieldsStatus = FormStatus.FIELD_ERROR)
      : (state[formID].status.fieldsStatus = FormStatus.READY);
  }

  state[formID].status.isDirty = isDirty(
    state[formID].data,
    state[formID].initialData,
    state[formID].fields
  );
}

function updateFormData(state: FormState) {
  state.data = {};
  if (!state.fields) throw new TypeError(`Form fields cannot be empty`);
  Object.values(state.fields).forEach(field => {
    ((field.rule && !hasRuleType("confirm", arrayMaybe(field.rule))) ||
      !field.rule) &&
      state.fields[field.name] &&
      (state.data[field.name] =
        field.rule && hasRuleType("number", arrayMaybe(field.rule))
          ? Number(field.value)
          : field.value);
  });
}

export const formReducer = (state: StateFormType, action: FormActions) => {
  switch (action.type) {
    case "form/clear":
      return produce(state, draft => {
        draft[action.payload] = createEmptyForm(action.payload);
        updateFormData(draft[action.payload]);
      });
    case "form/initialData/add":
      return produce(state, draft => {
        // draft[action.payload.formId] = createEmptyForm(action.payload);
        if (action.payload && draft[action.payload.formId]) {
          draft[action.payload.formId].initialData = action.payload.state;
        }
        // updateFormData(draft[action.payload]);
      });
    case "form/initialData/reload":
      return produce(state, draft => {
        // draft[action.payload.formId] = createEmptyForm(action.payload);
        if (
          draft[action.payload.formId]?.fields &&
          draft[action.payload.formId]?.initialData !== undefined
        ) {
          draft[action.payload.formId].initialData = {
            ...draft[action.payload.formId].data
          };
          updateInitialData(
            draft,
            action.payload.formId,
            Object.values(draft[action.payload.formId].fields)
          );
          updateFields(
            draft,
            action.payload.formId,
            Object.values(draft[action.payload.formId].fields)
          );
          updateFormData(draft[action.payload.formId]);
          updateStatus(draft, action.payload.formId);
        }
        // updateFormData(draft[action.payload]);
      });
    case "AddField":
      return produce(state, draft => {
        !draft[action.payload.formId] &&
          (draft[action.payload.formId] = createEmptyForm(
            action.payload.formId
          ));
        draft[action.payload.formId].status.fieldsStatus =
          FormStatus.INITIALIZED;
        if (action.payload.ignoreInitialData !== true)
          draft[action.payload.formId].initialData = {};
        // !draft[
        //   action.payload.formId
        // ].status
        //   ? draft[action.payload.formId].status
        //   : {};

        if (action.payload.ignoreInitialData !== true)
          updateInitialData(
            draft,
            action.payload.formId,
            action.payload.fields
          );
        updateFields(draft, action.payload.formId, action.payload.fields);
        updateFormData(draft[action.payload.formId]);
        updateStatus(draft, action.payload.formId);
      });
    case "RemoveField":
      return produce(state, draft => {
        if (!draft[action.payload.formId]) return;
        const form = draft[action.payload.formId];
        delete form.fields[action.payload.fieldName];
        form.errors =
          form.errors &&
          form.errors.filter(item => item.name !== action.payload.fieldName);
        updateFields(draft, action.payload.formId, [], false);
        updateFormData(form);
        updateStatus(draft, action.payload.formId);
      });
    case "UpdateField":
      return produce(state, draft => {
        if (!state[action.id]) return state;
        updateFields(draft, action.id, action.payload, true);
        // (draft[action.id].errors || []).length
        //   ? (draft[action.id].status.fieldsStatus = FormStatus.FIELD_ERROR)
        //   : (draft[action.id].status.fieldsStatus = FormStatus.READY);
        // draft[action.id].status.isDirty = true;
        updateFormData(draft[action.id]);
        updateStatus(draft, action.id);
        // draft[action.id].status.isDirty = isDIrty(
        //   draft[action.id].data,
        //   draft[action.id].initialData
        // );
      });
    case "AddFormState":
      return produce(state, draft => {
        if (draft[action.payload.state.id]) return;
        updateFields(
          draft,
          action.payload.state.id,
          action.payload.fields as FormFieldType[]
        );
        !draft[action.payload.state.id] &&
          (draft[action.payload.state.id] = action.payload.state);
        updateStatus(draft, action.payload.state.id);
        draft[action.payload.state.id].errorMessages =
          action.payload.state.errorMessages;
        if (action.payload.fields.length) {
          draft[action.payload.state.id].status.fieldsStatus =
            FormStatus.INITIALIZED;
          updateFormData(draft[action.payload.state.id]);
        }
      });
    case "RemoveFormState":
      return produce(state, draft => {
        delete draft[action.id];
      });
    case "UpdateFormState":
      return produce(state, draft => {
        draft[action.payload.id] &&
          Object.assign(draft[action.payload.id], action.payload.state);
      });
    case "UpdateFormData":
      return produce(state, draft => {
        draft[action.payload.id] && (draft[action.payload.id].data = action);
      });
    case "UpdateFormStatus":
      return produce(state, draft => {
        draft[action.id] &&
          Object.assign(draft[action.id].status, action.status);
      });
    default:
      return state;
  }
};
