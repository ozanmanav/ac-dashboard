import {
  FormFieldErrorType,
  FormFieldsDict,
  FormFieldRule,
  Rule
} from "./Form";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { formFieldValidatorFactory } from "./formFieldValidatorFactory";
import { fieldErrorMaybe } from "./fieldErrorMaybe";
export function formValidator(fields: FormFieldsDict) {
  const errors: FormFieldErrorType[] = [];
  Object.values(fields).forEach(field => {
    const rules: FormFieldRule[] = Array.isArray(field.rule)
      ? [...field.rule]
      : field.rule
      ? [field.rule]
      : [];
    rules.unshift({ type: "require", required: !!field.required });
    rules.forEach(
      rule =>
        ((!field.required && !isEmptyValue(field.value)) || field.required) &&
        fieldErrorMaybe(formFieldValidatorFactory(field, fields, rule), error =>
          (errs =>
            errs.forEach(err =>
              errors.push({ ...err, message: rule.errorMessage })
            ))(Array.isArray(error) ? error : [error])
        )
    );
  });
  return errors;
}
