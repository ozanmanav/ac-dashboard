import { FormState, FormFieldType, FormStatusType } from "./Form";
type RemoveFormAction = {
  type: "RemoveFormState";
  id: string;
};
type AddFormStateAction = {
  type: "AddFormState";
  payload: {
    state: FormState;
    fields: FormFieldType[];
  };
};
type UpdateFormStatusAction = {
  type: "UpdateFormStatus";
  id: string;
  status: Partial<FormStatusType>;
};
type UpdateFormState = {
  type: "UpdateFormState";
  payload: {
    id: string;
    state: Partial<FormState>;
  };
};
type UpdateFormData = {
  type: "UpdateFormData";
  payload: {
    id: string;
    data: any;
  };
};
type AddField = {
  type: "AddField";
  payload: {
    formId: string;
    fields: FormFieldType | FormFieldType[];
    ignoreInitialData?: boolean;
  };
};
type RemoveField = {
  type: "RemoveField";
  payload: {
    formId: string;
    fieldName: string;
  };
};
type AddInitialDataAction = {
  type: "form/initialData/add";
  payload: {
    formId: string;
    state: any;
  };
};
type initialDataReload = {
  type: "form/initialData/reload";
  payload: {
    formId: string;
  };
};
type UpdateFieldAction = {
  type: "UpdateField";
  id: string;
  payload: FormFieldType | FormFieldType[];
};
type ClearFormAction = {
  type: "form/clear";
  payload: string;
};
export type FormActions =
  | AddFormStateAction
  | RemoveFormAction
  | UpdateFormStatusAction
  | UpdateFormState
  | UpdateFormData
  | AddField
  | RemoveField
  | UpdateFieldAction
  | ClearFormAction
  | AddInitialDataAction
  | initialDataReload;
