import { FormState, FormFieldType } from "./Form";
export type FormService = {
  reload(id?: string): void;
  clear(): void;
  removeFormState(formId: string): void;
  getState(id?: string): FormState<any> | null;
  setDirty(val: boolean): void;
  removeField(formId: string, fieldName: string): void;
  registerField(
    formId: string,
    field: FormFieldType<any>,
    ignoreInitialData?: boolean
  ): void;
  setInitialData(data: any): void;
  updateField: (field: FormFieldType<any>, initialData?: boolean) => void;
  updateFormDataValue: (name: string, value: any) => void;
  updateFormData: (formData: { [key: string]: any }) => void;
};
