import { FormFieldErrorType } from "./Form";
import { ValidValue } from "./formValidators";
export function fieldErrorMaybe(
  field: typeof ValidValue | FormFieldErrorType,
  fn: (error: FormFieldErrorType | FormFieldErrorType[]) => void
) {
  field !== ValidValue && fn(field);
}
