import { useMemo, useContext, useRef } from "react";
import { FormState, FormStatus, FormFieldType } from "./Form";
import { FormService } from "./FormService";
import { FormContext, fields } from "./FormContext";
// const fieldsById: { [key: string]: { [key: string]: number } } = {};
export function useFormContext<TValue = any>(formId: string) {
  const { dispatch, state } = useContext(FormContext);
  const tasks = useRef<(() => void)[]>([]);
  const service = useMemo<FormService>(
    () => ({
      reload(id?: string) {
        dispatch({
          type: "UpdateFormStatus",
          id: id || formId,
          status: {
            isDirty: false,
            fieldsStatus: FormStatus.INITIALIZED,
            submission: FormStatus.IDLE
          }
        });
        dispatch({
          type: "form/initialData/reload",
          payload: {
            formId
          }
        });
      },
      removeFormState(formId: string) {
        dispatch({
          type: "RemoveFormState",
          id: formId
        });
      },
      setDirty(val) {
        dispatch({
          type: "UpdateFormStatus",
          status: {
            isDirty: val
          },
          id: formId
        });
      },
      clear() {
        dispatch({
          type: "form/clear",
          payload: formId
        });
      },
      getState(id?: string) {
        return state[id || formId] || null;
      },
      removeField(formId: string, fieldName: string) {
        dispatch({
          type: "RemoveField",
          payload: {
            formId,
            fieldName
          }
        });
      },
      setInitialData(data) {},
      registerField(
        formId: string,
        field: FormFieldType,
        ignoreInitialData = false
      ) {
        if (state[formId] && state[formId].fields[field.name]) return;
        !fields.hasOwnProperty(formId) && (fields[formId] = []);
        // fieldsById[formID][field.name] = fields[formID].length;
        fields[formId].push(field);
        timeout.current && clearTimeout(timeout.current);
        timeout.current = setTimeout(() => {
          fields &&
            Object.keys(fields).forEach(formId => {
              fields &&
                dispatch({
                  type: "AddField",
                  payload: {
                    formId,
                    fields: fields[formId],
                    ignoreInitialData
                  }
                });
            });
          delete fields[formId];
          tasks.current.forEach(fn => {
            fn();
          });
          tasks.current = [];
          timeout.current = undefined;
        }, 16);
      },
      updateField: (field: FormFieldType) => {
        if (!timeout.current) {
          dispatch({
            type: "UpdateField",
            id: formId,
            payload: field
          });
        } else {
          tasks.current.push(() =>
            dispatch({
              type: "UpdateField",
              id: formId,
              payload: field
            })
          );
        }
      },
      updateFormDataValue: (name: string, value: any) => {
        if (!timeout.current) {
          dispatch({
            type: "UpdateField",
            id: formId,
            payload: {
              name,
              value
            }
          });
        } else {
          tasks.current.push(() =>
            dispatch({
              type: "UpdateField",
              id: formId,
              payload: {
                name,
                value
              }
            })
          );
        }
      },
      updateFormData(formData) {
        dispatch({
          type: "UpdateFormData",
          payload: {
            data: formData,
            id: formId
          }
        });
      }
      // getFieldByName(name: string) {
      //   return fields[formID][fieldsById[formID][name]];
      // }
    }),
    [dispatch, formId, state]
  );
  const value = useMemo<[FormState | undefined, FormService]>(
    () => [state[formId], service],
    [formId, service, state]
  );
  const timeout = useRef<NodeJS.Timeout>();
  return value;
}
