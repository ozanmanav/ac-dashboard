import { FormFieldsDict } from "./Form";
import { shallowEqual } from "@appcircle/core/lib/utils/shallowEqual";
export function isDirty(
  data: {
    [x: string]: any;
  },
  initialData: {
    [x: string]: any;
  } = {},
  fields: FormFieldsDict
) {
  const dataKeys = Object.keys(data || {});
  const initialDataKeys = Object.keys(initialData || {});
  return (
    dataKeys.length !== initialDataKeys.length ||
    dataKeys.some(key => {
      const getDirty = fields[key] && fields[key].getDirty;
      return (
        !data ||
        !data.hasOwnProperty(key) ||
        (getDirty !== undefined
          ? getDirty(data[key], initialData[key])
          : !shallowEqual(data[key], initialData[key]))
      );
    })
  );
}
