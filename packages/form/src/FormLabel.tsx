import React from "react";
export function FormLabel(props: { label: string | undefined; tip?: string }) {
  return props.label !== undefined ? (
    <label className={"Form_label Form_inner"}>
      {props.label}
      {props.tip && <div className={"Form_tip Form_inner"}>{props.tip}</div>}
    </label>
  ) : null;
}
