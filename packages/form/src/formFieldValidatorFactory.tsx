import { DefaultFormFieldRulesType } from "./Form";
import {
  ValidValue,
  stringValidation,
  numberValidation,
  regexValidation,
  confirmFieldValidation,
  confirmValueValidation,
  inlistValidation,
  notinlistValidation,
  emailValidation,
  requireValidation,
  stringMinValidation,
  stringMaxValidation,
  requireWhenFieldhasValue
} from "./formValidators";
import { ValidationReducer } from "./FormContext";
export const formFieldValidatorFactory: ValidationReducer<DefaultFormFieldRulesType> = (
  field,
  fields,
  rule
) => {
  switch (rule.type) {
    case "string":
      return stringValidation(field, fields, rule);
    case "string-min":
      return stringMinValidation(field, fields, rule);
    case "string-max":
      return stringMaxValidation(field, fields, rule);
    case "number":
      return numberValidation(field, fields, rule);
    case "regex":
      return regexValidation(field, fields, rule);
    case "confirm-field":
      return confirmFieldValidation(field, fields, rule);
    case "confirm-value":
      return confirmValueValidation(field, fields, rule);
    case "in-list":
      return inlistValidation(field, fields, rule);
    case "not-in-list":
      return notinlistValidation(field, fields, rule);
    case "email":
      return emailValidation(field, fields, rule);
    case "require":
      return requireValidation(field, fields, rule);
    case "requireWhenFieldhasValue":
      return requireWhenFieldhasValue(field, fields, rule);
  }
  return ValidValue;
};
