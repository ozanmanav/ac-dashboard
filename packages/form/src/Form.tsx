import React, {
  PropsWithChildren,
  useCallback,
  useRef,
  useContext,
  useEffect,
  FormEvent,
  useMemo,
  Dispatch
} from "react";
import {
  useTaskManager,
  TaskPropsType,
  TaskRunnerType
} from "@appcircle/taskmanager/lib/TaskManagerContext";
import { FormContext, initialFormStatus } from "./FormContext";
import { FormActions } from "./FormActions";
import { ServiceFactoryFn } from "@appcircle/shared/lib/services/createService";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useFormErrors } from "./formErrors";
import { shallowEqual } from "@appcircle/core/lib/utils/shallowEqual";
import { ReqestMethod } from "@appcircle/core/lib/ReqestMethod";

export type WithDisabled = {
  isDisabled: boolean;
  disabledClassName: string;
};
export type WithoutDisabled = {
  isDisabled?: undefined;
};
export type FormElementType<TValue = any> = {
  className?: string;
  onChange?: (value: TValue) => void;
  onBlur?: (value: TValue) => void;
  isValid?: boolean;
  isDisabled?: boolean;
  disabledClassName?: string;
  value?: TValue;
  tabIndex?: number;
  isSelected?: boolean;
  index?: number;
  placeHolder?: string;
};

export type Rule<T = any> = {
  type: string;
  customType?: string;
  errorMessage?: string;
  [extraProps: string]: any;
} & T;

export type AnyFormFieldRule = Rule<any>;

export type StringRule = Rule<{ type: "string" }>;
export type StringMinRule = Rule<{ type: "string-min"; min: number }>;
export type StringMaxRule = Rule<{ type: "string-max"; max: number }>;
export type RequireRule = Rule<{ type: "require"; required: boolean }>;
export type GroupRule = Rule<{ type: "group" }>;
export type ConfirmRule = Rule<{ type: "confirm" }>;
export type BooleanRule = Rule<{ type: "boolean" }>;
export type NumberRule = Rule<{ type: "number" }>;
export type RegexRule = Rule<{ type: "regex"; testPattern: string }>;
export type EmailRule = Rule<{ type: "email" }>;
export type InListRule = Rule<{ type: "in-list"; list: any[] }>;
export type ConfirmValueRule = Rule<{
  type: "confirm-value";
  confirmValue: any;
}>;
export type ConfirmFieldRule = Rule<{
  type: "confirm-field";
  confirmfield: string;
}>;
export type NotinListRule = Rule<{ type: "not-in-list"; list: any[] }>;
export type FileRule = Rule<{ type: "file" }>;
export type RequireWhenFieldhasValueRule = Rule<{
  type: "requireWhenFieldhasValue";
  field: string;
  value: any;
}>;

export type DefaultFormFieldRulesType =
  | StringRule
  | GroupRule
  | BooleanRule
  | NumberRule
  | RegexRule
  | EmailRule
  | InListRule
  | ConfirmValueRule
  | ConfirmFieldRule
  | NotinListRule
  | FileRule
  | StringMinRule
  | StringMaxRule
  | RequireRule
  | RequireWhenFieldhasValueRule
  | ConfirmRule;
export type FormFieldRule = DefaultFormFieldRulesType;

export type FormFieldType<TValue = any> = {
  disableErrorIndicator?: boolean;
  required?: boolean;
  rule?: FormFieldRule | FormFieldRule[];
  value?: TValue;
  name: string;
  isDirty?: boolean;
  getDirty?: (value: TValue, initialValue: TValue) => boolean;
};

export enum FormStatus {
  IDLE = "@@FORM/IDLE",
  INITIALIZED = "@@FORM/INITIALIZED",
  READY = "@@FORM/READY",
  FIELD_ERROR = "@@FORM/FIELD_ERROR",
  SENDING = "@@FORM/SENDING",
  DONE_WITH_ERROR = "@@FORM/DONE_WITH_ERROR",
  DONE_WITH_SUCCESS = "@@FORM/DONE_WITH_SUCCESS"
}

export type FormFieldErrorsMap =
  | { [key in FormFieldErrorType["type"]]?: any }
  | { [key: string]: any };
export type FormFieldErrorType<T = any> = {
  name: string;
  message: T;
  type: DefaultFormFieldRulesType["type"] | string;
};

export type FormState<TResp = any> = {
  id: string;
  data: TResp;
  status: FormStatusType;
  errors?: FormFieldErrorType[];
  fields: FormFieldsDict;
  errorMessages?: FormFieldErrorsMap;
  autoUnload?: boolean;
  initialData: FormData;
};

export type FormStatusType = {
  fieldsStatus:
    | FormStatus.IDLE
    | FormStatus.INITIALIZED
    | FormStatus.FIELD_ERROR
    | FormStatus.READY;
  isDirty: boolean;
  submission:
    | FormStatus.IDLE
    | FormStatus.DONE_WITH_ERROR
    | FormStatus.DONE_WITH_SUCCESS
    | FormStatus.SENDING;
};

export type FormFieldsDict = { [key: string]: FormFieldType };

export type FormProps<
  TResp = any,
  TFormData extends FormData = any,
  TRequestData extends FormData = any
> = PropsWithChildren<{
  taskFactory?: (dispatch: Dispatch<FormActions>) => FormTaskFactory<TFormData>;
  formID: string;
  fields?: FormFieldType[];
  onChange?: (state: FormState<TResp>) => void;
  onInvalidFields?: (fields: FormFieldType[]) => void;
  onDone?: () => void;
  onStatusChange?: (state: FormState<TResp>) => void;
  status?: FormStatusType;
  targetPath?: string;
  method?: ReqestMethod;
  contentType?: "application/json" | "application/x-www-form-urlencoded";
  taskProps?: TaskPropsType<TResp> & { entityID?: string };
  isJSON?: boolean;
  onFieldsError?: (errors: FormFieldErrorType[]) => void;
  className?: string;
  onSubmit?: (formData: TFormData) => boolean;
  onBeforeSubmit?: (formData: TFormData) => TRequestData;
  errorMessages?: FormFieldErrorsMap;
  consumerFactory?: (url?: string | undefined) => ServiceFactoryFn;
  autoUnload?: boolean;
  // Sets once if props ready is true as initialData to calculate dirty-state.
  initialData?: TFormData;
  // If ready state is false then form waites to initialize form's state until getting true.
  ready?: boolean;
}>;

type FormData = {
  [id: string]: any;
};

export type FormTaskFactory<
  TData extends {
    [id: string]: any;
  } = {
    [id: string]: any;
  }
> = (formData: TData) => TaskRunnerType;

function areEqualStatuses(s1: FormStatusType, s2: FormStatusType) {
  return (
    s1.fieldsStatus === s2.fieldsStatus &&
    s1.isDirty === s2.isDirty &&
    s1.submission === s2.submission
  );
}

export function Form<TResp = any, TData = any, TRequestData = any>(
  props: FormProps<TResp, TData, TRequestData>
) {
  const { addTask, createApiTask } = useTaskManager();
  const { dispatch, state } = useContext(FormContext);
  // TODO: Move createApiConsumer to the FormCOntext as a parameter
  const { createApiConsumer } = useServiceContext();
  const response = useRef(null);
  const serviceCreate = useMemo(
    () =>
      props.consumerFactory ? props.consumerFactory() : createApiConsumer(),
    [createApiConsumer, props]
  );
  const isJSON = props.isJSON === undefined ? true : props.isJSON;
  const timeoutRef = useRef<NodeJS.Timeout>();
  const errorMessages = useFormErrors(props.errorMessages);
  const ready = props.ready !== false;
  const formState = useMemo(
    () =>
      state[props.formID] || {
        status: initialFormStatus
      },
    // eslint-disable-next-line
    [state]
  );
  const autoUnload = useRef(true);

  autoUnload.current = props.autoUnload !== false;

  const status = formState.status;
  //Initialize Form
  useEffect(() => {
    ready === true &&
      status.fieldsStatus !== FormStatus.IDLE &&
      status.isDirty === false &&
      dispatch({
        type: "form/initialData/reload",
        payload: {
          formId: props.formID
        }
      });
    // eslint-disable-next-line
  }, [ready, status.fieldsStatus !== FormStatus.IDLE]);
  useEffect(() => {
    dispatch({
      type: "AddFormState",
      payload: {
        fields: props.fields || [],
        state: {
          id: props.formID,
          data: {},
          errorMessages,
          status: initialFormStatus,
          fields: {},
          initialData: props.initialData || {},
          autoUnload: autoUnload.current
        }
      }
    });

    return () => {
      if (autoUnload.current === true)
        dispatch({
          type: "RemoveFormState",
          id: props.formID
        });
      response.current = null;
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    timeoutRef.current && clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      if (props.fields)
        formState.status.fieldsStatus === FormStatus.IDLE &&
        formState.status.submission === FormStatus.IDLE
          ? //Sets initial values
            dispatch({
              type: "AddField",
              payload: {
                formId: props.formID,
                fields: props.fields
              }
            })
          : //Updates fields
            dispatch({
              type: "UpdateField",
              id: props.formID,
              payload: props.fields
            });
    }, 15);
    // eslint-disable-next-line
  }, [props.fields]);

  useEffect(() => {
    if (!shallowEqual(props.initialData, formState.initialData)) {
      dispatch({
        type: "form/initialData/add",
        payload: {
          formId: props.formID,
          state: props.initialData || {}
        }
      });
    }
  }, [props.initialData]);

  useEffect(() => {
    (formState.status.fieldsStatus === FormStatus.FIELD_ERROR ||
      formState.status.fieldsStatus === FormStatus.READY) &&
      props.onFieldsError &&
      props.onFieldsError(formState.errors || []);
    // eslint-disable-next-line
  }, [formState.errors, props.onFieldsError]);
  const currentStatus = useRef<FormStatusType>();
  const currentState = useRef(formState);

  useEffect(() => {
    // status.fieldsStatus !== FormStatus.IDLE &&
    props.onStatusChange &&
      (currentStatus.current === undefined ||
        !areEqualStatuses(currentStatus.current, status)) &&
      props.onStatusChange(formState);
    currentStatus.current = status;
    // eslint-disable-next-line
  }, [status]);

  useEffect(() => {
    props.onChange &&
      // shallowEqual(formState, currentState.current) &&
      props.onChange(formState);
    currentState.current = formState;
  }, [formState]);

  useEffect(() => {
    props.status &&
      !areEqualStatuses({ ...status, ...props.status }, status) &&
      dispatch({
        type: "UpdateFormStatus",
        id: props.formID,
        status: props.status
      });
  }, [dispatch, props.formID, props.status, status]);

  //onFormSubmit
  const onSubmit = useCallback(
    (e: FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      props.onFieldsError && props.onFieldsError(formState.errors || []);
      if (formState.status.fieldsStatus !== FormStatus.READY) {
        return false;
      }

      if (!formState.data) return false;

      const formData = props.onBeforeSubmit
        ? props.onBeforeSubmit(formState.data)
        : formState.data;

      if (formData === undefined) return false;

      if (!props.targetPath && props.taskFactory === undefined) {
        const res = props.onSubmit && props.onSubmit(formData);

        res !== false &&
          dispatch({
            type: "UpdateFormStatus",
            id: props.formID,
            status: {
              submission: FormStatus.SENDING
            }
          });
        return false;
      }

      dispatch({
        type: "UpdateFormStatus",
        id: props.formID,
        status: {
          submission: FormStatus.SENDING
        }
      });
      let task = props.taskFactory
        ? props.taskFactory(dispatch)(
            formData && isJSON ? JSON.stringify(formData) : formData
          )
        : createApiTask(
            serviceCreate({
              method: props.method || "POST",
              endpoint: props.targetPath ? props.targetPath : "",
              data: formData && isJSON ? JSON.stringify(formData) : formData
              // headers: { "Content-Type": props.contentType || "application/json" }
            }),
            {
              ...props.taskProps,
              onSuccess: (resp: any) => {
                dispatch({
                  type: "UpdateFormStatus",
                  id: props.formID,
                  status: {
                    isDirty: false,
                    fieldsStatus: FormStatus.IDLE,
                    submission: FormStatus.DONE_WITH_SUCCESS
                  }
                });
                dispatch({
                  type: "form/initialData/reload",
                  payload: {
                    formId: props.formID
                  }
                });
                props.taskProps &&
                  props.taskProps.onSuccess &&
                  props.taskProps.onSuccess(resp);
              },
              onError: (resp: any) => {
                dispatch({
                  type: "UpdateFormStatus",
                  id: props.formID,
                  status: {
                    isDirty: true,
                    fieldsStatus: FormStatus.IDLE,
                    submission: FormStatus.DONE_WITH_ERROR
                  }
                });
                props.taskProps &&
                  props.taskProps.onError &&
                  props.taskProps.onError(resp);
              }
            }
          );
      // props.taskProps &&
      //   props.taskProps.entityIDs &&
      //   (task.entityID = props.taskProps.entityID);
      addTask(task);
    },
    [addTask, createApiTask, dispatch, formState, isJSON, props, serviceCreate]
  );

  return (
    <form
      noValidate
      className={"Form " + (props.className || "")}
      onSubmit={onSubmit}
    >
      {props.children}
    </form>
  );
}
