import {
  FormFieldType,
  FormFieldErrorType,
  FormFieldsDict,
  FormFieldRule,
  AnyFormFieldRule,
  StringRule,
  InListRule,
  NotinListRule,
  NumberRule,
  ConfirmValueRule,
  ConfirmFieldRule,
  RegexRule,
  EmailRule,
  StringMinRule,
  StringMaxRule,
  RequireWhenFieldhasValueRule,
  Rule
} from "./Form";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";

const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const ValidValue = Symbol("ValidValue");

export function hasType(type: FormFieldRule["type"], types: string | string[]) {
  return Array.isArray(types) ? types.some(t => t === type) : type === types;
}

export function hasRuleType(type: FormFieldRule["type"], types: Rule<{}>[]) {
  return types.some(t => t.type === type);
}

export function emailValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: EmailRule
) {
  // const rule = ruleMaybe(field.rule);
  return hasType("email", rule.type)
    ? invalidMaybe(
        !emailRegx.test(field.value as string),
        fieldError(field, rule)
      )
    : ValidValue;
}

export function stringValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: StringRule
) {
  const isString = typeof field.value === "string";

  return invalidMaybe(!isString, fieldError(field, rule));
}

export function stringMinValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: StringMinRule
) {
  return !isEmptyValue(rule.min) && (rule.min || 0) > (field.value || "").length
    ? fieldError(field, rule)
    : ValidValue;
}

export function stringMaxValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: StringMaxRule
) {
  return !isEmptyValue(rule.max) && (rule.max || 0) < (field.value || "").length
    ? fieldError(field, rule)
    : ValidValue;
}

export function inlistValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: InListRule
) {
  return hasType("in-list", rule.type) && Array.isArray(rule.list)
    ? invalidMaybe(
        !rule.list.some(val => field.value === val),
        fieldError(field, rule)
      )
    : ValidValue;
}

export function notinlistValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: NotinListRule
) {
  return Array.isArray(rule.list)
    ? invalidMaybe(
        rule.list.some(val => field.value === val),
        fieldError(field, rule)
      )
    : ValidValue;
}

export function numberValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: NumberRule
) {
  return hasType("number", rule.type)
    ? invalidMaybe(
        /[0-9]*/.test(field.value) === false,
        fieldError(field, rule)
      )
    : ValidValue;
}

export function confirmValueValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: ConfirmValueRule
) {
  return invalidMaybe(
    rule.confirmValue !== field.value,
    fieldError(field, rule)
  );
}

export function ruleMaybe(
  rule: AnyFormFieldRule | AnyFormFieldRule[] | undefined
): FormFieldRule {
  return Array.isArray(rule)
    ? rule[0]
    : rule === undefined
    ? { type: "none" }
    : rule;
}

export function fieldError(
  field: FormFieldType<any>,
  rule: FormFieldRule
): FormFieldErrorType<any> {
  return {
    name: field.name,
    type: rule.customType || rule.type,
    message: ""
  };
}

export function requireValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: FormFieldRule
) {
  return !!rule.required
    ? invalidMaybe(isEmptyValue(field.value), fieldError(field, rule))
    : ValidValue;
}

export function confirmFieldValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: ConfirmFieldRule
) {
  const confirmField = rule.confirmfield;

  confirmField !== undefined &&
    raiseErrorMaybe(
      !isEmptyValue(fields[confirmField]),
      new TypeError(`ConfirmField of [ ${field.name} ] is invalid`)
    );

  return confirmField !== undefined
    ? invalidMaybe(
        !!confirmField && fields[confirmField].value !== field.value,
        {
          name: fields[rule.confirmfield].isDirty
            ? rule.confirmfield
            : field.name,
          type: rule.customType || rule.type,
          message: ""
        }
      )
    : ValidValue;
}

export function regexValidation(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: RegexRule
) {
  const has = hasType("regex", rule.type);
  if (!has) {
    return ValidValue;
  } else if (has) {
    raiseErrorMaybe(
      !!rule.testPattern,
      new TypeError(`[ ${field.name} ] Field's testPattern cannot be empty`)
    );

    !isEmptyValue(field.value) &&
      raiseErrorMaybe(
        typeof field.value === "string",
        new TypeError(
          `[ ${field.name} ] Field's value [${typeof field.value ===
            "string"}] must be string`
        )
      );
  }

  return invalidMaybe(
    !!rule.testPattern &&
      !new RegExp(rule.testPattern).test(field.value as string),
    fieldError(field, rule)
  );
}
export function requireWhenFieldhasValue(
  field: FormFieldType,
  fields: FormFieldsDict,
  rule: RequireWhenFieldhasValueRule
) {
  const has = hasType("requireWhenFieldhasValue", rule.type);
  if (!has) {
    return ValidValue;
  } else if (has) {
    raiseErrorMaybe(
      !isEmptyValue(rule.value) || !isEmptyValue(rule.field),
      new TypeError(`[ ${field.name} ] Rule's value and field cannot be empty`)
    );
  }

  const available = rule.value === fields[rule.field].value;
  fields[rule.field].isDirty = !isEmptyValue(rule.value) && available;
  fields[rule.field].required = available;
  return available
    ? requireValidation(field, fields, { type: "require", required: true })
    : ValidValue;
}

export function raiseErrorMaybe(valid: boolean, error: string | Error) {
  if (!valid) throw error;
}

export function invalidMaybe(
  invalid: boolean,
  error: FormFieldErrorType
): FormFieldErrorType | typeof ValidValue {
  return !invalid ? ValidValue : error;
}
