import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { FormFieldErrorsMap } from "./Form";
import { useMemo } from "react";
import { defineMessages } from "react-intl";
const messages = {
  "string-min": CommonMessages.appCommonFormFieldErrorTooShort,
  "string-max": CommonMessages.appCommonFormFieldErrorTooLong,
  // require: CommonMessages.appCommonFormFieldErrorContainsIvalidChars,
  "in-list": {
    id: "app.common.form.error.inList",
    defaultMessage: "The entry must be unique"
  },
  number: {
    id: "app.common.form.error.number",
    defaultMessage: "Only numbers are accepted"
  },
  require: {
    id: "app.common.form.error.require",
    defaultMessage: "The field cannot be empty"
  },
  email: {
    id: "app.common.form.error.email",
    defaultMessage: "Please enter a valid email"
  },
  string: {
    id: "app.common.form.error.string",
    defaultMessage: "Only alphanumeric characters are accepted"
  },
  "password-min": {
    id: "app.common.form.error.string",
    defaultMessage: "The password can be min. {min} characters."
  },
  "confirm-value": {
    id: "app.common.form.error.notConfirmedValue",
    defaultMessage: "Please confirm the entry"
  },
  "confirm-field": {
    id: "app.common.form.error.notConfirmed",
    defaultMessage: "The entry is not confirmed"
  },
  fullname: {
    id: "app.common.form.error.notConfirmed",
    defaultMessage: "Please type in your full name."
  }
};
export const FormErrorsMessages = defineMessages(messages);
export function useFormErrors(update: FormFieldErrorsMap = {}) {
  const errors = useMemo<FormFieldErrorsMap>(
    () => ({ ...FormErrorsMessages, ...update }),
    [update]
  );

  return errors;
}
