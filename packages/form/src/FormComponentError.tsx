import React from "react";
import { useIntl, MessageDescriptor } from "react-intl";
export function FormComponentError({
  errorMessage,
  name
}: {
  errorMessage?: MessageDescriptor;
  name: string;
}) {
  const intl = useIntl();
  return (
    <div className="Form_error firstLetterUpper">
      {errorMessage && errorMessage.id
        ? intl.formatMessage(errorMessage, {
            subject: name
          })
        : errorMessage}
    </div>
  );
}
