export declare function createEmptyForm(id: string): {
    id: string;
    fields: {};
    initialData: {};
    data: {};
    status: {
        fieldsStatus: import("./Form").FormStatus.IDLE | import("./Form").FormStatus.INITIALIZED | import("./Form").FormStatus.READY | import("./Form").FormStatus.FIELD_ERROR;
        isDirty: boolean;
        submission: import("./Form").FormStatus.IDLE | import("./Form").FormStatus.SENDING | import("./Form").FormStatus.DONE_WITH_ERROR | import("./Form").FormStatus.DONE_WITH_SUCCESS;
    };
};
//# sourceMappingURL=createEmptyForm.d.ts.map