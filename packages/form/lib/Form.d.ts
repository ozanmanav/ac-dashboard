import { PropsWithChildren, Dispatch } from "react";
import { TaskPropsType, TaskRunnerType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { FormActions } from "./FormActions";
import { ServiceFactoryFn } from "@appcircle/shared/lib/services/createService";
import { ReqestMethod } from "@appcircle/core/lib/ReqestMethod";
export declare type WithDisabled = {
    isDisabled: boolean;
    disabledClassName: string;
};
export declare type WithoutDisabled = {
    isDisabled?: undefined;
};
export declare type FormElementType<TValue = any> = {
    className?: string;
    onChange?: (value: TValue) => void;
    onBlur?: (value: TValue) => void;
    isValid?: boolean;
    isDisabled?: boolean;
    disabledClassName?: string;
    value?: TValue;
    tabIndex?: number;
    isSelected?: boolean;
    index?: number;
    placeHolder?: string;
};
export declare type Rule<T = any> = {
    type: string;
    customType?: string;
    errorMessage?: string;
    [extraProps: string]: any;
} & T;
export declare type AnyFormFieldRule = Rule<any>;
export declare type StringRule = Rule<{
    type: "string";
}>;
export declare type StringMinRule = Rule<{
    type: "string-min";
    min: number;
}>;
export declare type StringMaxRule = Rule<{
    type: "string-max";
    max: number;
}>;
export declare type RequireRule = Rule<{
    type: "require";
    required: boolean;
}>;
export declare type GroupRule = Rule<{
    type: "group";
}>;
export declare type ConfirmRule = Rule<{
    type: "confirm";
}>;
export declare type BooleanRule = Rule<{
    type: "boolean";
}>;
export declare type NumberRule = Rule<{
    type: "number";
}>;
export declare type RegexRule = Rule<{
    type: "regex";
    testPattern: string;
}>;
export declare type EmailRule = Rule<{
    type: "email";
}>;
export declare type InListRule = Rule<{
    type: "in-list";
    list: any[];
}>;
export declare type ConfirmValueRule = Rule<{
    type: "confirm-value";
    confirmValue: any;
}>;
export declare type ConfirmFieldRule = Rule<{
    type: "confirm-field";
    confirmfield: string;
}>;
export declare type NotinListRule = Rule<{
    type: "not-in-list";
    list: any[];
}>;
export declare type FileRule = Rule<{
    type: "file";
}>;
export declare type RequireWhenFieldhasValueRule = Rule<{
    type: "requireWhenFieldhasValue";
    field: string;
    value: any;
}>;
export declare type DefaultFormFieldRulesType = StringRule | GroupRule | BooleanRule | NumberRule | RegexRule | EmailRule | InListRule | ConfirmValueRule | ConfirmFieldRule | NotinListRule | FileRule | StringMinRule | StringMaxRule | RequireRule | RequireWhenFieldhasValueRule | ConfirmRule;
export declare type FormFieldRule = DefaultFormFieldRulesType;
export declare type FormFieldType<TValue = any> = {
    disableErrorIndicator?: boolean;
    required?: boolean;
    rule?: FormFieldRule | FormFieldRule[];
    value?: TValue;
    name: string;
    isDirty?: boolean;
    getDirty?: (value: TValue, initialValue: TValue) => boolean;
};
export declare enum FormStatus {
    IDLE = "@@FORM/IDLE",
    INITIALIZED = "@@FORM/INITIALIZED",
    READY = "@@FORM/READY",
    FIELD_ERROR = "@@FORM/FIELD_ERROR",
    SENDING = "@@FORM/SENDING",
    DONE_WITH_ERROR = "@@FORM/DONE_WITH_ERROR",
    DONE_WITH_SUCCESS = "@@FORM/DONE_WITH_SUCCESS"
}
export declare type FormFieldErrorsMap = {
    [key in FormFieldErrorType["type"]]?: any;
} | {
    [key: string]: any;
};
export declare type FormFieldErrorType<T = any> = {
    name: string;
    message: T;
    type: DefaultFormFieldRulesType["type"] | string;
};
export declare type FormState<TResp = any> = {
    id: string;
    data: TResp;
    status: FormStatusType;
    errors?: FormFieldErrorType[];
    fields: FormFieldsDict;
    errorMessages?: FormFieldErrorsMap;
    autoUnload?: boolean;
    initialData: FormData;
};
export declare type FormStatusType = {
    fieldsStatus: FormStatus.IDLE | FormStatus.INITIALIZED | FormStatus.FIELD_ERROR | FormStatus.READY;
    isDirty: boolean;
    submission: FormStatus.IDLE | FormStatus.DONE_WITH_ERROR | FormStatus.DONE_WITH_SUCCESS | FormStatus.SENDING;
};
export declare type FormFieldsDict = {
    [key: string]: FormFieldType;
};
export declare type FormProps<TResp = any, TFormData extends FormData = any, TRequestData extends FormData = any> = PropsWithChildren<{
    taskFactory?: (dispatch: Dispatch<FormActions>) => FormTaskFactory<TFormData>;
    formID: string;
    fields?: FormFieldType[];
    onChange?: (state: FormState<TResp>) => void;
    onInvalidFields?: (fields: FormFieldType[]) => void;
    onDone?: () => void;
    onStatusChange?: (state: FormState<TResp>) => void;
    status?: FormStatusType;
    targetPath?: string;
    method?: ReqestMethod;
    contentType?: "application/json" | "application/x-www-form-urlencoded";
    taskProps?: TaskPropsType<TResp> & {
        entityID?: string;
    };
    isJSON?: boolean;
    onFieldsError?: (errors: FormFieldErrorType[]) => void;
    className?: string;
    onSubmit?: (formData: TFormData) => boolean;
    onBeforeSubmit?: (formData: TFormData) => TRequestData;
    errorMessages?: FormFieldErrorsMap;
    consumerFactory?: (url?: string | undefined) => ServiceFactoryFn;
    autoUnload?: boolean;
    initialData?: TFormData;
    ready?: boolean;
}>;
declare type FormData = {
    [id: string]: any;
};
export declare type FormTaskFactory<TData extends {
    [id: string]: any;
} = {
    [id: string]: any;
}> = (formData: TData) => TaskRunnerType;
export declare function Form<TResp = any, TData = any, TRequestData = any>(props: FormProps<TResp, TData, TRequestData>): JSX.Element;
export {};
//# sourceMappingURL=Form.d.ts.map