import { useMemo, useContext, useRef } from "react";
import { FormStatus } from "./Form";
import { FormContext, fields } from "./FormContext";
// const fieldsById: { [key: string]: { [key: string]: number } } = {};
export function useFormContext(formId) {
    var _a = useContext(FormContext), dispatch = _a.dispatch, state = _a.state;
    var tasks = useRef([]);
    var service = useMemo(function () { return ({
        reload: function (id) {
            dispatch({
                type: "UpdateFormStatus",
                id: id || formId,
                status: {
                    isDirty: false,
                    fieldsStatus: FormStatus.INITIALIZED,
                    submission: FormStatus.IDLE
                }
            });
            dispatch({
                type: "form/initialData/reload",
                payload: {
                    formId: formId
                }
            });
        },
        removeFormState: function (formId) {
            dispatch({
                type: "RemoveFormState",
                id: formId
            });
        },
        setDirty: function (val) {
            dispatch({
                type: "UpdateFormStatus",
                status: {
                    isDirty: val
                },
                id: formId
            });
        },
        clear: function () {
            dispatch({
                type: "form/clear",
                payload: formId
            });
        },
        getState: function (id) {
            return state[id || formId] || null;
        },
        removeField: function (formId, fieldName) {
            dispatch({
                type: "RemoveField",
                payload: {
                    formId: formId,
                    fieldName: fieldName
                }
            });
        },
        setInitialData: function (data) { },
        registerField: function (formId, field, ignoreInitialData) {
            if (ignoreInitialData === void 0) { ignoreInitialData = false; }
            if (state[formId] && state[formId].fields[field.name])
                return;
            !fields.hasOwnProperty(formId) && (fields[formId] = []);
            // fieldsById[formID][field.name] = fields[formID].length;
            fields[formId].push(field);
            timeout.current && clearTimeout(timeout.current);
            timeout.current = setTimeout(function () {
                fields &&
                    Object.keys(fields).forEach(function (formId) {
                        fields &&
                            dispatch({
                                type: "AddField",
                                payload: {
                                    formId: formId,
                                    fields: fields[formId],
                                    ignoreInitialData: ignoreInitialData
                                }
                            });
                    });
                delete fields[formId];
                tasks.current.forEach(function (fn) {
                    fn();
                });
                tasks.current = [];
                timeout.current = undefined;
            }, 16);
        },
        updateField: function (field) {
            if (!timeout.current) {
                dispatch({
                    type: "UpdateField",
                    id: formId,
                    payload: field
                });
            }
            else {
                tasks.current.push(function () {
                    return dispatch({
                        type: "UpdateField",
                        id: formId,
                        payload: field
                    });
                });
            }
        },
        updateFormDataValue: function (name, value) {
            if (!timeout.current) {
                dispatch({
                    type: "UpdateField",
                    id: formId,
                    payload: {
                        name: name,
                        value: value
                    }
                });
            }
            else {
                tasks.current.push(function () {
                    return dispatch({
                        type: "UpdateField",
                        id: formId,
                        payload: {
                            name: name,
                            value: value
                        }
                    });
                });
            }
        },
        updateFormData: function (formData) {
            dispatch({
                type: "UpdateFormData",
                payload: {
                    data: formData,
                    id: formId
                }
            });
        }
        // getFieldByName(name: string) {
        //   return fields[formID][fieldsById[formID][name]];
        // }
    }); }, [dispatch, formId, state]);
    var value = useMemo(function () { return [state[formId], service]; }, [formId, service, state]);
    var timeout = useRef();
    return value;
}
//# sourceMappingURL=useFormContext.js.map