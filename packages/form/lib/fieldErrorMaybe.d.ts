import { FormFieldErrorType } from "./Form";
import { ValidValue } from "./formValidators";
export declare function fieldErrorMaybe(field: typeof ValidValue | FormFieldErrorType, fn: (error: FormFieldErrorType | FormFieldErrorType[]) => void): void;
//# sourceMappingURL=fieldErrorMaybe.d.ts.map