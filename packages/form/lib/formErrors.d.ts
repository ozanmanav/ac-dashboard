import { FormFieldErrorsMap } from "./Form";
export declare const FormErrorsMessages: {
    "string-min": {
        id: string;
        defaultMessage: string;
    };
    "string-max": {
        id: string;
        defaultMessage: string;
    };
    "in-list": {
        id: string;
        defaultMessage: string;
    };
    number: {
        id: string;
        defaultMessage: string;
    };
    require: {
        id: string;
        defaultMessage: string;
    };
    email: {
        id: string;
        defaultMessage: string;
    };
    string: {
        id: string;
        defaultMessage: string;
    };
    "password-min": {
        id: string;
        defaultMessage: string;
    };
    "confirm-value": {
        id: string;
        defaultMessage: string;
    };
    "confirm-field": {
        id: string;
        defaultMessage: string;
    };
    fullname: {
        id: string;
        defaultMessage: string;
    };
};
export declare function useFormErrors(update?: FormFieldErrorsMap): FormFieldErrorsMap;
//# sourceMappingURL=formErrors.d.ts.map