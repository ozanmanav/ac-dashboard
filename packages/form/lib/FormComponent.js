var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { useMemo, useEffect, useCallback, useState, useRef } from "react";
import { useFormContext } from "./useFormContext";
import { FormStatus } from "./Form";
import React from "react";
import classNames from "classnames";
import { FormComponentError } from "./FormComponentError";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { FormLabel } from "./FormLabel";
export function FormComponent(props) {
    var _a;
    var Element = props.element;
    var _b = useFormContext(props.formID), formState = _b[0], formService = _b[1];
    var _c = useState(props.value), initialValue = _c[0], setinitialValue = _c[1];
    var autoUnload = useRef(false);
    autoUnload.current = !!((_a = formState) === null || _a === void 0 ? void 0 : _a.autoUnload);
    useEffect(function () {
        props.formProps &&
            formService.registerField(props.formID, __assign(__assign({}, props.formProps), { value: isEmptyValue(props.value) ? props.formProps.value : props.value }));
        return function () {
            if (autoUnload.current === true)
                props.formProps &&
                    formService.removeField(props.formID, props.formProps.name);
        };
        // eslint-disable-next-line
    }, []);
    useEffect(function () {
        if (!props.formProps)
            return;
        if (initialValue === undefined && props.value !== undefined) {
            formService.updateFormDataValue(props.formProps.name, props.value);
            setinitialValue(props.value);
        }
        else if (initialValue !== undefined && props.value === undefined) {
            formService.updateFormDataValue(props.formProps.name, props.value);
            setinitialValue(props.value);
        }
    }, [props.value]);
    var field = props.formProps && formState && formState.fields[props.formProps.name];
    var isDirty = field && field.isDirty;
    // const fieldIndex = field.index;
    var onChange = useCallback(function (param) {
        var val = typeof param === "object" &&
            param !== null &&
            param.target &&
            param.target.value !== undefined
            ? param.target.value
            : param;
        formService.updateFormDataValue(props.formProps.name, val);
        props.onChange && props.onChange(val);
    }, [formService, props]);
    var className = classNames("FormComponent_wrappedElement", props.className);
    var errors = useMemo(function () {
        return (props.formProps &&
            formState &&
            formState.errors &&
            formState.errors.filter(function (error) { return error.name === props.formProps.name; })) ||
            [];
    }, [formState, props.formProps && props.formProps.name]);
    var hasError = useMemo(function () {
        var _a, _b;
        return isDirty &&
            !!errors.length &&
            ((_a = formState) === null || _a === void 0 ? void 0 : _a.status.fieldsStatus) === FormStatus.FIELD_ERROR && ((_b = formState) === null || _b === void 0 ? void 0 : _b.status.isDirty);
    }, [errors.length, formState, isDirty]);
    var errorMessage = errors[0] && errors[0].message !== undefined
        ? errors[0].message
        : formState &&
            formState.errorMessages &&
            errors[0] &&
            formState.errorMessages[errors[0].type]
            ? formState.errorMessages[errors[0].type]
            : "";
    var comp = useMemo(function () {
        return !Element ? (Element) : (React.createElement("div", { className: "FormComponent " + (props.isDisabled ? props.disabledClassName : "") },
            React.createElement(FormLabel, { label: props.formFieldLabel, tip: props.tip }),
            props.formProps.disableErrorIndicator !== true &&
                props.formProps.required === true && (React.createElement("div", { className: "requiredIndicator requiredIndicator-pass--" +
                    (!hasError && (!errors || !errors.length)) })),
            React.createElement(Element, __assign({}, props, { onChange: onChange, value: props.formProps &&
                    formState &&
                    formState.fields[props.formProps.name] &&
                    formState.fields[props.formProps.name].value !== undefined
                    ? formState.fields[props.formProps.name].value
                    : props.value !== initialValue
                        ? props.value
                        : props.formProps && props.formProps.value, className: hasError
                    ? className + " " + props.formProps.errorClass
                    : className })),
            React.createElement("div", { className: "FormComponent-margin" }, " "),
            hasError && props.formProps && !!errorMessage && (React.createElement(FormComponentError, { errorMessage: errorMessage, name: props.formProps.name }))));
    }, [className, formState, hasError, onChange, props, errorMessage, Element]);
    return comp;
}
//# sourceMappingURL=FormComponent.js.map