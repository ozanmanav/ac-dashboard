/// <reference types="react" />
export declare function FormLabel(props: {
    label: string | undefined;
    tip?: string;
}): JSX.Element | null;
//# sourceMappingURL=FormLabel.d.ts.map