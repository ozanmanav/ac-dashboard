var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useCallback, useRef, useContext, useEffect, useMemo } from "react";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { FormContext, initialFormStatus } from "./FormContext";
import { useServiceContext } from "@appcircle/shared/lib/services/useServiceContext";
import { useFormErrors } from "./formErrors";
import { shallowEqual } from "@appcircle/core/lib/utils/shallowEqual";
export var FormStatus;
(function (FormStatus) {
    FormStatus["IDLE"] = "@@FORM/IDLE";
    FormStatus["INITIALIZED"] = "@@FORM/INITIALIZED";
    FormStatus["READY"] = "@@FORM/READY";
    FormStatus["FIELD_ERROR"] = "@@FORM/FIELD_ERROR";
    FormStatus["SENDING"] = "@@FORM/SENDING";
    FormStatus["DONE_WITH_ERROR"] = "@@FORM/DONE_WITH_ERROR";
    FormStatus["DONE_WITH_SUCCESS"] = "@@FORM/DONE_WITH_SUCCESS";
})(FormStatus || (FormStatus = {}));
function areEqualStatuses(s1, s2) {
    return (s1.fieldsStatus === s2.fieldsStatus &&
        s1.isDirty === s2.isDirty &&
        s1.submission === s2.submission);
}
export function Form(props) {
    var _a = useTaskManager(), addTask = _a.addTask, createApiTask = _a.createApiTask;
    var _b = useContext(FormContext), dispatch = _b.dispatch, state = _b.state;
    // TODO: Move createApiConsumer to the FormCOntext as a parameter
    var createApiConsumer = useServiceContext().createApiConsumer;
    var response = useRef(null);
    var serviceCreate = useMemo(function () {
        return props.consumerFactory ? props.consumerFactory() : createApiConsumer();
    }, [createApiConsumer, props]);
    var isJSON = props.isJSON === undefined ? true : props.isJSON;
    var timeoutRef = useRef();
    var errorMessages = useFormErrors(props.errorMessages);
    var ready = props.ready !== false;
    var formState = useMemo(function () {
        return state[props.formID] || {
            status: initialFormStatus
        };
    }, 
    // eslint-disable-next-line
    [state]);
    var autoUnload = useRef(true);
    autoUnload.current = props.autoUnload !== false;
    var status = formState.status;
    //Initialize Form
    useEffect(function () {
        ready === true &&
            status.fieldsStatus !== FormStatus.IDLE &&
            status.isDirty === false &&
            dispatch({
                type: "form/initialData/reload",
                payload: {
                    formId: props.formID
                }
            });
        // eslint-disable-next-line
    }, [ready, status.fieldsStatus !== FormStatus.IDLE]);
    useEffect(function () {
        dispatch({
            type: "AddFormState",
            payload: {
                fields: props.fields || [],
                state: {
                    id: props.formID,
                    data: {},
                    errorMessages: errorMessages,
                    status: initialFormStatus,
                    fields: {},
                    initialData: props.initialData || {},
                    autoUnload: autoUnload.current
                }
            }
        });
        return function () {
            if (autoUnload.current === true)
                dispatch({
                    type: "RemoveFormState",
                    id: props.formID
                });
            response.current = null;
        };
        // eslint-disable-next-line
    }, []);
    useEffect(function () {
        timeoutRef.current && clearTimeout(timeoutRef.current);
        timeoutRef.current = setTimeout(function () {
            if (props.fields)
                formState.status.fieldsStatus === FormStatus.IDLE &&
                    formState.status.submission === FormStatus.IDLE
                    ? //Sets initial values
                        dispatch({
                            type: "AddField",
                            payload: {
                                formId: props.formID,
                                fields: props.fields
                            }
                        })
                    : //Updates fields
                        dispatch({
                            type: "UpdateField",
                            id: props.formID,
                            payload: props.fields
                        });
        }, 15);
        // eslint-disable-next-line
    }, [props.fields]);
    useEffect(function () {
        if (!shallowEqual(props.initialData, formState.initialData)) {
            dispatch({
                type: "form/initialData/add",
                payload: {
                    formId: props.formID,
                    state: props.initialData || {}
                }
            });
        }
    }, [props.initialData]);
    useEffect(function () {
        (formState.status.fieldsStatus === FormStatus.FIELD_ERROR ||
            formState.status.fieldsStatus === FormStatus.READY) &&
            props.onFieldsError &&
            props.onFieldsError(formState.errors || []);
        // eslint-disable-next-line
    }, [formState.errors, props.onFieldsError]);
    var currentStatus = useRef();
    var currentState = useRef(formState);
    useEffect(function () {
        // status.fieldsStatus !== FormStatus.IDLE &&
        props.onStatusChange &&
            (currentStatus.current === undefined ||
                !areEqualStatuses(currentStatus.current, status)) &&
            props.onStatusChange(formState);
        currentStatus.current = status;
        // eslint-disable-next-line
    }, [status]);
    useEffect(function () {
        props.onChange &&
            // shallowEqual(formState, currentState.current) &&
            props.onChange(formState);
        currentState.current = formState;
    }, [formState]);
    useEffect(function () {
        props.status &&
            !areEqualStatuses(__assign(__assign({}, status), props.status), status) &&
            dispatch({
                type: "UpdateFormStatus",
                id: props.formID,
                status: props.status
            });
    }, [dispatch, props.formID, props.status, status]);
    //onFormSubmit
    var onSubmit = useCallback(function (e) {
        e.preventDefault();
        props.onFieldsError && props.onFieldsError(formState.errors || []);
        if (formState.status.fieldsStatus !== FormStatus.READY) {
            return false;
        }
        if (!formState.data)
            return false;
        var formData = props.onBeforeSubmit
            ? props.onBeforeSubmit(formState.data)
            : formState.data;
        if (formData === undefined)
            return false;
        if (!props.targetPath && props.taskFactory === undefined) {
            var res = props.onSubmit && props.onSubmit(formData);
            res !== false &&
                dispatch({
                    type: "UpdateFormStatus",
                    id: props.formID,
                    status: {
                        submission: FormStatus.SENDING
                    }
                });
            return false;
        }
        dispatch({
            type: "UpdateFormStatus",
            id: props.formID,
            status: {
                submission: FormStatus.SENDING
            }
        });
        var task = props.taskFactory
            ? props.taskFactory(dispatch)(formData && isJSON ? JSON.stringify(formData) : formData)
            : createApiTask(serviceCreate({
                method: props.method || "POST",
                endpoint: props.targetPath ? props.targetPath : "",
                data: formData && isJSON ? JSON.stringify(formData) : formData
                // headers: { "Content-Type": props.contentType || "application/json" }
            }), __assign(__assign({}, props.taskProps), { onSuccess: function (resp) {
                    dispatch({
                        type: "UpdateFormStatus",
                        id: props.formID,
                        status: {
                            isDirty: false,
                            fieldsStatus: FormStatus.IDLE,
                            submission: FormStatus.DONE_WITH_SUCCESS
                        }
                    });
                    dispatch({
                        type: "form/initialData/reload",
                        payload: {
                            formId: props.formID
                        }
                    });
                    props.taskProps &&
                        props.taskProps.onSuccess &&
                        props.taskProps.onSuccess(resp);
                }, onError: function (resp) {
                    dispatch({
                        type: "UpdateFormStatus",
                        id: props.formID,
                        status: {
                            isDirty: true,
                            fieldsStatus: FormStatus.IDLE,
                            submission: FormStatus.DONE_WITH_ERROR
                        }
                    });
                    props.taskProps &&
                        props.taskProps.onError &&
                        props.taskProps.onError(resp);
                } }));
        // props.taskProps &&
        //   props.taskProps.entityIDs &&
        //   (task.entityID = props.taskProps.entityID);
        addTask(task);
    }, [addTask, createApiTask, dispatch, formState, isJSON, props, serviceCreate]);
    return (React.createElement("form", { noValidate: true, className: "Form " + (props.className || ""), onSubmit: onSubmit }, props.children));
}
//# sourceMappingURL=Form.js.map