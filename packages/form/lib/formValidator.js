var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
import { formFieldValidatorFactory } from "./formFieldValidatorFactory";
import { fieldErrorMaybe } from "./fieldErrorMaybe";
export function formValidator(fields) {
    var errors = [];
    Object.values(fields).forEach(function (field) {
        var rules = Array.isArray(field.rule)
            ? __spreadArrays(field.rule) : field.rule
            ? [field.rule]
            : [];
        rules.unshift({ type: "require", required: !!field.required });
        rules.forEach(function (rule) {
            return ((!field.required && !isEmptyValue(field.value)) || field.required) &&
                fieldErrorMaybe(formFieldValidatorFactory(field, fields, rule), function (error) {
                    return (function (errs) {
                        return errs.forEach(function (err) {
                            return errors.push(__assign(__assign({}, err), { message: rule.errorMessage }));
                        });
                    })(Array.isArray(error) ? error : [error]);
                });
        });
    });
    return errors;
}
//# sourceMappingURL=formValidator.js.map