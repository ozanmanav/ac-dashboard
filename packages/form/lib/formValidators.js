import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
var emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export var ValidValue = Symbol("ValidValue");
export function hasType(type, types) {
    return Array.isArray(types) ? types.some(function (t) { return t === type; }) : type === types;
}
export function hasRuleType(type, types) {
    return types.some(function (t) { return t.type === type; });
}
export function emailValidation(field, fields, rule) {
    // const rule = ruleMaybe(field.rule);
    return hasType("email", rule.type)
        ? invalidMaybe(!emailRegx.test(field.value), fieldError(field, rule))
        : ValidValue;
}
export function stringValidation(field, fields, rule) {
    var isString = typeof field.value === "string";
    return invalidMaybe(!isString, fieldError(field, rule));
}
export function stringMinValidation(field, fields, rule) {
    return !isEmptyValue(rule.min) && (rule.min || 0) > (field.value || "").length
        ? fieldError(field, rule)
        : ValidValue;
}
export function stringMaxValidation(field, fields, rule) {
    return !isEmptyValue(rule.max) && (rule.max || 0) < (field.value || "").length
        ? fieldError(field, rule)
        : ValidValue;
}
export function inlistValidation(field, fields, rule) {
    return hasType("in-list", rule.type) && Array.isArray(rule.list)
        ? invalidMaybe(!rule.list.some(function (val) { return field.value === val; }), fieldError(field, rule))
        : ValidValue;
}
export function notinlistValidation(field, fields, rule) {
    return Array.isArray(rule.list)
        ? invalidMaybe(rule.list.some(function (val) { return field.value === val; }), fieldError(field, rule))
        : ValidValue;
}
export function numberValidation(field, fields, rule) {
    return hasType("number", rule.type)
        ? invalidMaybe(/[0-9]*/.test(field.value) === false, fieldError(field, rule))
        : ValidValue;
}
export function confirmValueValidation(field, fields, rule) {
    return invalidMaybe(rule.confirmValue !== field.value, fieldError(field, rule));
}
export function ruleMaybe(rule) {
    return Array.isArray(rule)
        ? rule[0]
        : rule === undefined
            ? { type: "none" }
            : rule;
}
export function fieldError(field, rule) {
    return {
        name: field.name,
        type: rule.customType || rule.type,
        message: ""
    };
}
export function requireValidation(field, fields, rule) {
    return !!rule.required
        ? invalidMaybe(isEmptyValue(field.value), fieldError(field, rule))
        : ValidValue;
}
export function confirmFieldValidation(field, fields, rule) {
    var confirmField = rule.confirmfield;
    confirmField !== undefined &&
        raiseErrorMaybe(!isEmptyValue(fields[confirmField]), new TypeError("ConfirmField of [ " + field.name + " ] is invalid"));
    return confirmField !== undefined
        ? invalidMaybe(!!confirmField && fields[confirmField].value !== field.value, {
            name: fields[rule.confirmfield].isDirty
                ? rule.confirmfield
                : field.name,
            type: rule.customType || rule.type,
            message: ""
        })
        : ValidValue;
}
export function regexValidation(field, fields, rule) {
    var has = hasType("regex", rule.type);
    if (!has) {
        return ValidValue;
    }
    else if (has) {
        raiseErrorMaybe(!!rule.testPattern, new TypeError("[ " + field.name + " ] Field's testPattern cannot be empty"));
        !isEmptyValue(field.value) &&
            raiseErrorMaybe(typeof field.value === "string", new TypeError("[ " + field.name + " ] Field's value [" + (typeof field.value ===
                "string") + "] must be string"));
    }
    return invalidMaybe(!!rule.testPattern &&
        !new RegExp(rule.testPattern).test(field.value), fieldError(field, rule));
}
export function requireWhenFieldhasValue(field, fields, rule) {
    var has = hasType("requireWhenFieldhasValue", rule.type);
    if (!has) {
        return ValidValue;
    }
    else if (has) {
        raiseErrorMaybe(!isEmptyValue(rule.value) || !isEmptyValue(rule.field), new TypeError("[ " + field.name + " ] Rule's value and field cannot be empty"));
    }
    var available = rule.value === fields[rule.field].value;
    fields[rule.field].isDirty = !isEmptyValue(rule.value) && available;
    fields[rule.field].required = available;
    return available
        ? requireValidation(field, fields, { type: "require", required: true })
        : ValidValue;
}
export function raiseErrorMaybe(valid, error) {
    if (!valid)
        throw error;
}
export function invalidMaybe(invalid, error) {
    return !invalid ? ValidValue : error;
}
//# sourceMappingURL=formValidators.js.map