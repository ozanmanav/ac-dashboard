import { shallowEqual } from "@appcircle/core/lib/utils/shallowEqual";
export function isDirty(data, initialData, fields) {
    if (initialData === void 0) { initialData = {}; }
    var dataKeys = Object.keys(data || {});
    var initialDataKeys = Object.keys(initialData || {});
    return (dataKeys.length !== initialDataKeys.length ||
        dataKeys.some(function (key) {
            var getDirty = fields[key] && fields[key].getDirty;
            return (!data ||
                !data.hasOwnProperty(key) ||
                (getDirty !== undefined
                    ? getDirty(data[key], initialData[key])
                    : !shallowEqual(data[key], initialData[key])));
        }));
}
//# sourceMappingURL=isDirty.js.map