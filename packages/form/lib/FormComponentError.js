import React from "react";
import { useIntl } from "react-intl";
export function FormComponentError(_a) {
    var errorMessage = _a.errorMessage, name = _a.name;
    var intl = useIntl();
    return (React.createElement("div", { className: "Form_error firstLetterUpper" }, errorMessage && errorMessage.id
        ? intl.formatMessage(errorMessage, {
            subject: name
        })
        : errorMessage));
}
//# sourceMappingURL=FormComponentError.js.map