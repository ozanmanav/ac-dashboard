import { FormState, FormFieldType, FormStatusType } from "./Form";
declare type RemoveFormAction = {
    type: "RemoveFormState";
    id: string;
};
declare type AddFormStateAction = {
    type: "AddFormState";
    payload: {
        state: FormState;
        fields: FormFieldType[];
    };
};
declare type UpdateFormStatusAction = {
    type: "UpdateFormStatus";
    id: string;
    status: Partial<FormStatusType>;
};
declare type UpdateFormState = {
    type: "UpdateFormState";
    payload: {
        id: string;
        state: Partial<FormState>;
    };
};
declare type UpdateFormData = {
    type: "UpdateFormData";
    payload: {
        id: string;
        data: any;
    };
};
declare type AddField = {
    type: "AddField";
    payload: {
        formId: string;
        fields: FormFieldType | FormFieldType[];
        ignoreInitialData?: boolean;
    };
};
declare type RemoveField = {
    type: "RemoveField";
    payload: {
        formId: string;
        fieldName: string;
    };
};
declare type AddInitialDataAction = {
    type: "form/initialData/add";
    payload: {
        formId: string;
        state: any;
    };
};
declare type initialDataReload = {
    type: "form/initialData/reload";
    payload: {
        formId: string;
    };
};
declare type UpdateFieldAction = {
    type: "UpdateField";
    id: string;
    payload: FormFieldType | FormFieldType[];
};
declare type ClearFormAction = {
    type: "form/clear";
    payload: string;
};
export declare type FormActions = AddFormStateAction | RemoveFormAction | UpdateFormStatusAction | UpdateFormState | UpdateFormData | AddField | RemoveField | UpdateFieldAction | ClearFormAction | AddInitialDataAction | initialDataReload;
export {};
//# sourceMappingURL=FormActions.d.ts.map