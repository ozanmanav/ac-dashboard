/// <reference types="react" />
import { MessageDescriptor } from "react-intl";
export declare function FormComponentError({ errorMessage, name }: {
    errorMessage?: MessageDescriptor;
    name: string;
}): JSX.Element;
//# sourceMappingURL=FormComponentError.d.ts.map