var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { initialFormStatus } from "./FormContext";
export function createEmptyForm(id) {
    return {
        id: id,
        fields: {},
        initialData: {},
        data: {},
        status: __assign({}, initialFormStatus)
    };
}
//# sourceMappingURL=createEmptyForm.js.map