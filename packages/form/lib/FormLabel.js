import React from "react";
export function FormLabel(props) {
    return props.label !== undefined ? (React.createElement("label", { className: "Form_label Form_inner" },
        props.label,
        props.tip && React.createElement("div", { className: "Form_tip Form_inner" }, props.tip))) : null;
}
//# sourceMappingURL=FormLabel.js.map