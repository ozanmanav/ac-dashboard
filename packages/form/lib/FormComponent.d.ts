import { ReactElement } from "react";
import { FormFieldType, FormElementType, WithDisabled, WithoutDisabled } from "./Form";
import React from "react";
export declare function FormComponent<TElProps extends FormElementType = FormElementType>(props: FormComponentProps<TElProps> & TElProps & WithDisabled): ReactElement;
export declare function FormComponent<TElProps extends FormElementType = FormElementType>(props: FormComponentProps<TElProps> & TElProps & WithoutDisabled): ReactElement;
export declare type FormComponentProps<TElementProps extends FormElementType = any, U = TElementProps["value"] extends infer U ? U : any> = {
    element: React.ComponentType<TElementProps> | null;
    formFieldLabel?: string;
    formID: string;
    tip?: string;
    formProps: FormFieldType & {
        errorClass?: string;
    };
    initialValue?: any;
} & FormElementType<U>;
//# sourceMappingURL=FormComponent.d.ts.map