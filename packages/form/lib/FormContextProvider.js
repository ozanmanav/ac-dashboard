import React, { useReducer, useMemo } from "react";
import { formReducer, FormContext } from "./FormContext";
export function FormContextProvider(props) {
    var _a = useReducer(formReducer, {}), state = _a[0], dispatch = _a[1];
    //TODO: Maybe create two contexts for state and dispatch each
    var value = useMemo(function () {
        return {
            state: state,
            dispatch: dispatch
        };
    }, [state]);
    return (React.createElement(FormContext.Provider, { value: value }, props.children));
}
//# sourceMappingURL=FormContextProvider.js.map