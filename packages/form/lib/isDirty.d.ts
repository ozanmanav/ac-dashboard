import { FormFieldsDict } from "./Form";
export declare function isDirty(data: {
    [x: string]: any;
}, initialData: {
    [x: string]: any;
} | undefined, fields: FormFieldsDict): boolean;
//# sourceMappingURL=isDirty.d.ts.map