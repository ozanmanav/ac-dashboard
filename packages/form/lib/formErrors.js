var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { useMemo } from "react";
import { defineMessages } from "react-intl";
var messages = {
    "string-min": CommonMessages.appCommonFormFieldErrorTooShort,
    "string-max": CommonMessages.appCommonFormFieldErrorTooLong,
    // require: CommonMessages.appCommonFormFieldErrorContainsIvalidChars,
    "in-list": {
        id: "app.common.form.error.inList",
        defaultMessage: "The entry must be unique"
    },
    number: {
        id: "app.common.form.error.number",
        defaultMessage: "Only numbers are accepted"
    },
    require: {
        id: "app.common.form.error.require",
        defaultMessage: "The field cannot be empty"
    },
    email: {
        id: "app.common.form.error.email",
        defaultMessage: "Please enter a valid email"
    },
    string: {
        id: "app.common.form.error.string",
        defaultMessage: "Only alphanumeric characters are accepted"
    },
    "password-min": {
        id: "app.common.form.error.string",
        defaultMessage: "The password can be min. {min} characters."
    },
    "confirm-value": {
        id: "app.common.form.error.notConfirmedValue",
        defaultMessage: "Please confirm the entry"
    },
    "confirm-field": {
        id: "app.common.form.error.notConfirmed",
        defaultMessage: "The entry is not confirmed"
    },
    fullname: {
        id: "app.common.form.error.notConfirmed",
        defaultMessage: "Please type in your full name."
    }
};
export var FormErrorsMessages = defineMessages(messages);
export function useFormErrors(update) {
    if (update === void 0) { update = {}; }
    var errors = useMemo(function () { return (__assign(__assign({}, FormErrorsMessages), update)); }, [update]);
    return errors;
}
//# sourceMappingURL=formErrors.js.map