import { Dispatch } from "react";
import { FormState, FormFieldType, FormFieldErrorType, FormFieldsDict, FormStatusType, AnyFormFieldRule, Rule } from "./Form";
import { ValidValue } from "./formValidators";
import { FormActions } from "./FormActions";
export declare type FormContextType = {
    state: {
        [id: string]: FormState;
    };
    dispatch: Dispatch<FormActions>;
};
export declare const FormContext: import("react").Context<FormContextType>;
export declare type ValidationReducer<T extends Rule = AnyFormFieldRule> = (field: FormFieldType, fields: FormFieldsDict, rule: T) => typeof ValidValue | FormFieldErrorType;
export declare const fields: {
    [key: string]: FormFieldType[];
};
declare type StateFormType = {
    [id: string]: FormState;
};
export declare const initialFormStatus: FormStatusType;
export declare const formReducer: (state: StateFormType, action: FormActions) => StateFormType;
export {};
//# sourceMappingURL=FormContext.d.ts.map