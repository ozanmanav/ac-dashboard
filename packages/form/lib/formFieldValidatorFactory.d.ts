import { DefaultFormFieldRulesType } from "./Form";
import { ValidationReducer } from "./FormContext";
export declare const formFieldValidatorFactory: ValidationReducer<DefaultFormFieldRulesType>;
//# sourceMappingURL=formFieldValidatorFactory.d.ts.map