import { FormState } from "./Form";
import { FormService } from "./FormService";
export declare function useFormContext<TValue = any>(formId: string): [FormState<any> | undefined, FormService];
//# sourceMappingURL=useFormContext.d.ts.map