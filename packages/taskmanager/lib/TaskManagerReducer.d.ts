import { TaskManagerActions } from "./TaskManagerActions";
import { TaskManagerStateType } from "./TaskManagerProvider";
import { Reducer } from "redux";
export declare function arrayMaybe<T extends any = any>(val?: T | T[]): T[];
export declare const TaskManagerReducer: Reducer<TaskManagerStateType, TaskManagerActions>;
//# sourceMappingURL=TaskManagerReducer.d.ts.map