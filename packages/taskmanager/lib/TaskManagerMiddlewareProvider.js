import { useReducer } from "react";
import React from "react";
import { useTaskResumeService } from "./useTaskResumeService";
import { middlewaresReducer, MiddlewaresInitialState, TaskManagerMiddlewareDispatchContext, TaskManagerMiddlewareStateContext } from "./TaskManagerServiceProvider";
export function TaskManagerMiddlewareProvider(props) {
    var _a = useReducer(middlewaresReducer, MiddlewaresInitialState), state = _a[0], dispatch = _a[1];
    // TODO: User this service as middleware
    useTaskResumeService();
    return (React.createElement(TaskManagerMiddlewareDispatchContext.Provider, { value: dispatch },
        React.createElement(TaskManagerMiddlewareStateContext.Provider, { value: state }, props.children)));
}
//# sourceMappingURL=TaskManagerMiddlewareProvider.js.map