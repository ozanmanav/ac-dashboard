var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import { useCallback, useMemo, createContext, useContext, useEffect } from "react";
import { of, from } from "rxjs";
import { filter, tap, catchError, finalize, take, mergeMap, takeUntil, map, startWith } from "rxjs/operators";
import React from "react";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { createApiObserver, createProgressObserver, taskRunner, createApiTask, TaskManagerContext } from "./TaskManagerContext";
import { useTaskManagerDispatch } from "./TaskManagerProvider";
import { pipeFromArray } from "rxjs/internal/util/pipe";
import { HookContext } from "@appcircle/sse/lib/HookContext";
import { EventTaskStatus } from "@appcircle/shared/lib/EventTaskStatus";
import { API_NO_RESULT, API_INITIAL_VALUE } from "@appcircle/core/lib/services/ApiResultType";
var timeout;
export var MiddlewaresInitialState = {
    middlewares: []
};
export var TaskManagerMiddlewareDispatchContext = createContext(null);
export var TaskManagerMiddlewareStateContext = createContext(MiddlewaresInitialState);
export var middlewaresReducer = function (state, action) {
    switch (action.type) {
        case "add":
            var mids = Array.isArray(action.payload)
                ? action.payload
                : [action.payload];
            return { middlewares: __spreadArrays(state.middlewares, mids) };
        case "remove":
            return {
                middlewares: state.middlewares.filter(function (fn) { return fn !== action.payload; })
            };
    }
    return state;
};
export function useGetMiddlewares() {
    var state = useContext(TaskManagerMiddlewareStateContext);
    return state.middlewares;
}
export function useAddMiddlewares() {
    var dispatch = useContext(TaskManagerMiddlewareDispatchContext);
    var fn = useCallback(function (middlewares) {
        dispatch &&
            middlewares &&
            dispatch({
                type: "add",
                payload: middlewares
            });
    }, [dispatch]);
    return dispatch ? fn : null;
}
var observTask = function (task, taskObserver, dispatch) { return function (resp) {
    if (resp === API_NO_RESULT || resp === API_INITIAL_VALUE) {
        return;
    }
    if (resp.json &&
        resp.json.taskId &&
        (resp.json.isSuccess === false ||
            resp.json.eventAction === "Failed" ||
            resp.json.eventAction === "Cancelled")) {
        dispatch({
            type: "updateStatus",
            payload: {
                taskId: task.id,
                status: "COMPLETED",
                data: resp,
                error: resp.json,
                entityID: task.entityID,
                action: resp.json.eventAction
            }
        });
        taskObserver.error(resp);
    }
    else if (resp instanceof Error) {
        dispatch({
            type: "updateStatus",
            payload: {
                taskId: task.id,
                status: "COMPLETED",
                data: null,
                error: resp,
                entityID: task.entityID
            }
        });
        taskObserver.error(resp);
    }
    else {
        dispatch({
            type: "updateStatus",
            payload: {
                taskId: task.id,
                status: "COMPLETED",
                data: resp,
                entityID: task.entityID
            }
        });
        taskObserver.next(resp);
    }
}; };
export function TaskManagerServiceProvider(_a) {
    var state = _a.state, children = _a.children;
    var pushMessage = useNotificationsService().pushMessage;
    var dispatch = useTaskManagerDispatch();
    var hookListener$ = useContext(HookContext);
    var middlewares = useGetMiddlewares();
    var addMiddleware = useAddMiddlewares();
    useEffect(function () {
        addMiddleware &&
            addMiddleware([
                function (task) {
                    return task;
                }
            ]);
    }, [addMiddleware]);
    var addTask = useCallback(function (task, priority) {
        if (priority === void 0) { priority = 2; }
        var entityIDs = task.entityID;
        // [arrayMaybe(task.entityID as string), ...(task.props.entityIDs || [])] as string[];
        dispatch({
            type: "updateStatus",
            payload: {
                taskId: task.id,
                status: "ADDED",
                data: null,
                entityID: entityIDs,
                state: task.props.state
            }
        });
        var taskObserver = createApiObserver(task.props, pushMessage);
        var progressObserver = createProgressObserver(task.id, task.entityID);
        var task$ = (middlewares.length
            ? middlewares.reduce(function (acc, curr) { return curr(acc, task.props); }, task.run(progressObserver))
            : task.run(progressObserver))
            // task.run(progressObserver)
            .pipe(mergeMap(function (resp) {
            var return$ = resp.json &&
                !task.props.ignoreTaskId &&
                resp.json.taskId
                ? // If the response has a taskId then it means a remote task startted and wait its response from HookServver.
                    hookListener$.pipe(takeUntil(task.cancel$), filter(function (response) {
                        return (response.json.taskId === resp.json.taskId &&
                            (response.json.status === EventTaskStatus.Completed ||
                                response.json.status === EventTaskStatus.Cancel));
                    }))
                : // If not just return the response.
                    of(resp);
            resp.json.taskId &&
                dispatch({
                    type: "waitingForRemoteTask",
                    payload: {
                        taskId: task.id,
                        remoteTaskId: resp.json.taskId
                    }
                });
            // If the task has a pipe array
            // then adds hooklistener after the Pipe
            return (task.props.pipe
                ? pipeFromArray(task.props.pipe)(return$)
                : return$).pipe(take(1));
        }), map(function (resp) {
            return task.props.taskPipe
                ? from(task.props.taskPipe.map(function (taskFn) {
                    return taskFn(createProgressObserver(task.id, task.entityID));
                })).pipe(startWith(resp))
                : resp;
        }), catchError(function (err) {
            return of(err);
        }), tap(observTask(task, taskObserver, dispatch)), finalize(function () {
            taskObserver.complete();
            task.cancel$.complete();
        }));
        dispatch({
            type: "add",
            task: [task, task$, taskObserver, priority]
        });
        clearTimeout(timeout);
        timeout = setTimeout(taskRunner, 16.6);
        return task.id || "";
    }, [dispatch, hookListener$, pushMessage, middlewares]);
    // .pipe(filter(response => response.taskId === waitedTaskId))
    var _createApiTask = useCallback(function (runner, props) {
        return createApiTask(props, runner);
    }, []);
    var service = useMemo(function () { return ({
        addTask: addTask,
        createApiTask: _createApiTask,
        getLastTaskID: function () { },
        getStatusByEntityID: function (id) {
            var taskID = state.tasksByEntity.get(id);
            return taskID && state.tasksStatuses.has(taskID)
                ? state.tasksStatuses.get(taskID)
                : undefined;
        },
        getActiveTasksByEntityId: function (entityID) {
            return (!Array.isArray(entityID) ? [entityID || ""] : entityID).reduce(function (acc, id) {
                var task = service.getStatusByEntityID(id);
                if (task !== undefined &&
                    (task.status === "ADDED" ||
                        task.status === "IN_PROGRESS" ||
                        task.status === "WAITING_RESPONSE"))
                    acc.push(task);
                return acc;
            }, []);
        },
        getStatusByTaskId: function (id) {
            return id && state.tasksStatuses.has(id)
                ? state.tasksStatuses.get(id)
                : undefined;
        },
        getStatusByRemoteTaskId: function (id) {
            return id && state.taskIdByRemoteTaskId[id]
                ? service.getStatusByTaskId(state.taskIdByRemoteTaskId[id])
                : undefined;
        }
    }); }, [_createApiTask, addTask, state]);
    return (React.createElement(TaskManagerContext.Provider, { value: service }, children));
}
//# sourceMappingURL=TaskManagerServiceProvider.js.map