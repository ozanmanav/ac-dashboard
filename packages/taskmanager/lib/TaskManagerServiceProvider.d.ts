import { PropsWithChildren, Dispatch } from "react";
import React from "react";
import { TaskManagerStateType, MiddlewareFn } from "./TaskManagerProvider";
declare type MiddlewaresActions = {
    type: "add";
    payload: MiddlewareFn | MiddlewareFn[];
} | {
    type: "remove";
    payload: MiddlewareFn;
};
declare type MiddlewareState = {
    middlewares: MiddlewareFn[];
};
export declare const MiddlewaresInitialState: MiddlewareState;
export declare const TaskManagerMiddlewareDispatchContext: React.Context<Dispatch<MiddlewaresActions> | null>;
export declare const TaskManagerMiddlewareStateContext: React.Context<MiddlewareState>;
export declare const middlewaresReducer: React.Reducer<MiddlewareState, MiddlewaresActions>;
export declare function useGetMiddlewares(): MiddlewareFn[];
export declare function useAddMiddlewares(): ((middlewares: MiddlewareFn[]) => void) | null;
export declare function TaskManagerServiceProvider({ state, children }: PropsWithChildren<{
    state: TaskManagerStateType;
}>): JSX.Element;
export {};
//# sourceMappingURL=TaskManagerServiceProvider.d.ts.map