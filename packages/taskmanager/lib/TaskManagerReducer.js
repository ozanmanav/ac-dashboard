var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import produce from "immer";
import { TaskManagerInitialState } from "./TaskManagerProvider";
import { of, Subject } from "rxjs";
import { Priority, TASK_MANAGER_NO_RESULT } from "./TaskManagerContext";
export function arrayMaybe(val) {
    return val && Array.isArray(val) ? val : val ? [val] : [];
}
export var TaskManagerReducer = function (state, action) {
    if (state === void 0) { state = TaskManagerInitialState; }
    // const newState: TaskManagerStateType = { ...state };
    return produce(state, function (draft) {
        switch (action.type) {
            case "add":
                draft.tasks = new Map(draft.tasks);
                {
                    var task_1 = action.task[0];
                    draft.tasks.set(task_1.id, action.task);
                    if (task_1.entityID || task_1.props.entityIDs) {
                        draft.tasksByEntity = new Map(draft.tasksByEntity);
                        arrayMaybe(task_1.entityID)
                            .concat(arrayMaybe(task_1.props.entityIDs || []))
                            .forEach(function (id) {
                            id && draft.tasksByEntity.set(id, task_1.id);
                        });
                    }
                }
                break;
            case "createTask":
                {
                    draft.tasks = new Map(draft.tasks);
                    var task = action.payload.task;
                    draft.tasks.set(action.payload.id, [
                        {
                            id: action.payload.id,
                            props: action.payload.task,
                            cancel$: new Subject(),
                            entityID: action.payload.task.entityIDs,
                            run: function () { return of(TASK_MANAGER_NO_RESULT); }
                        },
                        of(TASK_MANAGER_NO_RESULT),
                        { next: function () { }, error: function () { }, complete: function () { } },
                        Priority.NORMAL
                    ]);
                    draft.remotetaskIdsByTaskId.set(action.payload.id, action.payload.remoteTaskId);
                    draft.remotetaskIdsByTaskId = new Map(draft.remotetaskIdsByTaskId);
                    draft.taskIdByRemoteTaskId[action.payload.remoteTaskId] =
                        action.payload.id;
                    if (task.entityIDs) {
                        draft.tasksByEntity = new Map(draft.tasksByEntity);
                        arrayMaybe(task.entityIDs)
                            .concat(arrayMaybe(task.entityIDs || []))
                            .forEach(function (id) {
                            id && draft.tasksByEntity.set(id, action.payload.id);
                        });
                    }
                    draft.tasksStatuses.set(action.payload.id, {
                        status: "ADDED",
                        taskId: action.payload.id,
                        state: task.state,
                        data: action.payload.data,
                        entityID: task.entityIDs,
                        error: action.payload.error,
                        remoteTaskId: action.payload.remoteTaskId
                    });
                    draft.tasksStatuses = new Map(draft.tasksStatuses);
                }
                break;
            case "waitingForRemoteTask":
                {
                    draft.remotetaskIdsByTaskId.set(action.payload.taskId, action.payload.remoteTaskId);
                    draft.remotetaskIdsByTaskId = new Map(draft.remotetaskIdsByTaskId);
                    draft.taskIdByRemoteTaskId[action.payload.remoteTaskId] =
                        action.payload.taskId;
                    var status_1 = draft.tasksStatuses.has(action.payload.taskId)
                        ? draft.tasksStatuses.get(action.payload.taskId)
                        : null;
                    if (status_1) {
                        draft.tasksStatuses.set(action.payload.taskId, __assign(__assign({}, status_1), { remoteTaskId: action.payload.remoteTaskId }));
                        draft.tasksStatuses = new Map(draft.tasksStatuses);
                    }
                }
                break;
            case "updateStatus":
                // newState.tasksStatus = new Map(state.tasksStatus);
                var oldStatus = draft.tasksStatuses.has(action.payload.taskId)
                    ? draft.tasksStatuses.get(action.payload.taskId)
                    : {};
                var status_2 = __assign(__assign(__assign({}, oldStatus), action.payload), { remoteTaskId: draft.remotetaskIdsByTaskId.has(action.payload.taskId)
                        ? draft.remotetaskIdsByTaskId.get(action.payload.taskId)
                        : undefined });
                if (action.payload.taskId) {
                    draft.tasksStatuses = new Map(draft.tasksStatuses);
                    draft.tasksStatuses.set(action.payload.taskId, status_2);
                }
                if (action.payload.status === "ERROR") {
                    if (action.payload.error) {
                        draft.tasksByErrors = new Map(draft.tasksByErrors);
                        draft.tasksByErrors.set(action.payload.error.status, action.payload.taskId);
                    }
                }
                break;
            case "remove":
                var ids = void 0;
                if (typeof action.taskId === "string") {
                    ids = [action.taskId];
                }
                else {
                    ids = action.taskId;
                }
                draft.tasks = new Map(draft.tasks);
                ids.forEach(function (id) {
                    draft.tasks.delete(id);
                });
                break;
            case "resetErrors":
                draft.tasksByErrors = new Map();
                break;
        }
    });
};
//# sourceMappingURL=TaskManagerReducer.js.map