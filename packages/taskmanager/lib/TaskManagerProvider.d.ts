import { PropsWithChildren } from "react";
import { TaskItemTupple, TaskPropsType } from "./TaskManagerContext";
import { TaskStatusType } from "./TaskStatusType";
import { TaskManagerActions } from "./TaskManagerActions";
import React from "react";
import { Observable } from "rxjs";
export declare const TaskManagerInitialState: TaskManagerStateType;
export declare type TaskManagerStateType = {
    tasks: Map<string, TaskItemTupple>;
    tasksByEntity: Map<string, string>;
    tasksStatuses: Map<string, TaskStatusType>;
    tasksByErrors: Map<number, string>;
    tasksCache: Map<number, string>;
    remotetaskIdsByTaskId: Map<string, string>;
    remotetaskIdsByEntityId: Map<string, string>;
    taskIdByRemoteTaskId: {
        [remoteId: string]: string;
    };
};
export declare const TaskManagerStoreContext: React.Context<TaskManagerStateType>;
export declare function useTaskManagerDispatch(): import("redux").Dispatch<TaskManagerActions>;
export declare function getTaskManagerDispatch(): import("redux").Dispatch<TaskManagerActions>;
export declare function getTaskManagerState(): TaskManagerStateType;
export declare function useTasksError(code?: number): string[];
export declare function useCheckUnauthrizedTask(): [Boolean, () => void];
export declare type MiddlewareFn = (task: Observable<any>, props: TaskPropsType) => Observable<any>;
export declare function TaskManagerProvider(props: PropsWithChildren<{
    middlewares: MiddlewareFn[];
}>): JSX.Element;
//# sourceMappingURL=TaskManagerProvider.d.ts.map