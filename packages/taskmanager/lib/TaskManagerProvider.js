var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import { useEffect, useState, createContext, useContext, useCallback, useMemo } from "react";
import { createStore } from "redux";
import { TaskManagerReducer } from "./TaskManagerReducer";
import React from "react";
import { TaskManagerServiceProvider } from "./TaskManagerServiceProvider";
import { TaskManagerMiddlewareProvider } from "./TaskManagerMiddlewareProvider";
export var TaskManagerInitialState = {
    tasks: new Map(),
    // tasksStatus: new Map(),
    tasksByEntity: new Map(),
    tasksStatuses: new Map(),
    tasksByErrors: new Map(),
    tasksCache: new Map(),
    remotetaskIdsByTaskId: new Map(),
    remotetaskIdsByEntityId: new Map(),
    taskIdByRemoteTaskId: {}
};
var taskManagerStore = createStore(TaskManagerReducer, TaskManagerInitialState);
export var TaskManagerStoreContext = createContext(TaskManagerInitialState);
export function useTaskManagerDispatch() {
    return taskManagerStore.dispatch;
}
export function getTaskManagerDispatch() {
    return taskManagerStore.dispatch;
}
export function getTaskManagerState() {
    return taskManagerStore.getState();
}
export function useTasksError(code) {
    if (code === void 0) { code = 401; }
    var state = useContext(TaskManagerStoreContext);
    var _a = useState(function () { return state.tasks; }), tasks = _a[0], setTasks = _a[1];
    var _b = useState([]), errors = _b[0], setErrors = _b[1];
    useEffect(function () {
        if (state.tasks.size === 0 && tasks.size !== 0) {
            var _errors_1 = [];
            tasks.forEach(function (task) {
                var status = state.tasksStatuses.has(task[0].id)
                    ? state.tasksStatuses.get(task[0].id)
                    : undefined;
                status &&
                    status.error &&
                    status.error.status === code &&
                    _errors_1.push(task[0].id);
                status &&
                    status.status !== "COMPLETED" &&
                    status.status !== "ERROR" &&
                    tasks.delete(task[0].id);
            });
            setErrors(__spreadArrays(errors, _errors_1));
            setTasks(new Map(tasks));
        }
        // eslint-disable-next-line
    }, [code, errors, state.tasks.size, state.tasksStatuses]);
    return errors;
}
export function useCheckUnauthrizedTask() {
    var state = useContext(TaskManagerStoreContext);
    var dispatch = useTaskManagerDispatch();
    var reset = useCallback(function () {
        dispatch({ type: "resetErrors" });
    }, [dispatch]);
    var value = useMemo(function () { return [state.tasksByErrors.has(401), reset]; }, [reset, state.tasksByErrors]);
    return value;
}
export function TaskManagerProvider(props) {
    var _a = useState(function () { return taskManagerStore.getState(); }), state = _a[0], setState = _a[1];
    useEffect(function () {
        var unsubscribe = taskManagerStore.subscribe(function () {
            setState(taskManagerStore.getState());
        });
        return function () { return unsubscribe(); };
        // eslint-disable-next-line
    }, []);
    return (React.createElement(TaskManagerStoreContext.Provider, { value: state },
        React.createElement(TaskManagerServiceProvider, { state: state },
            React.createElement(TaskManagerMiddlewareProvider, null, props.children))));
}
//# sourceMappingURL=TaskManagerProvider.js.map