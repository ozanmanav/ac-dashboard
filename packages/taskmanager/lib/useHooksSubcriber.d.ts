import { HookEvent } from "@appcircle/sse/lib/HookEvent";
export declare function useHooksSubcriber(eventNames?: string[], remoteTaskId?: string): HookEvent<any> | null;
//# sourceMappingURL=useHooksSubcriber.d.ts.map