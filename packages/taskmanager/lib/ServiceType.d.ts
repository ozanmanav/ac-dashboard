import { ServiceFactory } from "./ServiceFactory";
export declare type ServiceType<T extends {
    [key: string]: ServiceFactory<any, any>;
}> = T;
//# sourceMappingURL=ServiceType.d.ts.map