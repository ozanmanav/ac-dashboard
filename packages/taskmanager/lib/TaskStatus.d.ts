export declare type TaskStatus = "ADDED" | "IN_PROGRESS" | "WAITING_RESPONSE" | "COMPLETED" | "ERROR" | "CANCELLED";
//# sourceMappingURL=TaskStatus.d.ts.map