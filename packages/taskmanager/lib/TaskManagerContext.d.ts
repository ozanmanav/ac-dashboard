/// <reference types="react" />
import { Observable, Observer, Subject, PartialObserver, UnaryFunction } from "rxjs";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";
import { Result } from "@appcircle/core/lib/services/Result";
import { Message } from "@appcircle/shared/lib/Module";
import { API_INITIAL_VALUE, API_NO_RESULT } from "@appcircle/core/lib/services/ApiResultType";
import { TaskManagerContextType } from "./TaskManagerContextType";
export declare const TaskManagerContextTypeInitialValue: TaskManagerContextType;
export declare const TaskManagerContext: import("react").Context<TaskManagerContextType>;
export declare type TaskCallbackFn<T, TRet = void> = (res: T extends Error ? T : ApiResultType<T>) => TRet;
export declare type Pipe<T, R> = UnaryFunction<T, R>[];
export declare type TaskPropsType<TResponse = any, TState = {}> = {
    pipe?: Pipe<any, any>;
    taskPipe?: TaskFactoryFn[];
    ignoreTaskId?: boolean;
    retryCount?: number;
    onProgress?: (e: ProgressEvent) => void;
    progressMessage?: TaskCallbackFn<TResponse, string>;
    successMessage?: TaskCallbackFn<TResponse, string>;
    errorMessage?: TaskCallbackFn<AjaxResultError, string>;
    onSuccess?: TaskCallbackFn<TResponse>;
    onError?: TaskCallbackFn<AjaxResultError | Error>;
    onFinally?: TaskCallbackFn<TResponse>;
    entityIDs?: string[];
    state?: TState;
    /**
     * To pass AbortHandler to client
     */
    onStart?: TaskOnStartFnType;
};
export declare const TASK_MANAGER_NO_RESULT = "API_NO_RESULT";
export declare const TASK_MANAGER_API_INITIAL_VALUE: API_INITIAL_VALUE;
export declare type ResultError = {
    errors: {
        [key: string]: string[];
    };
    status: number;
    title: string;
    traceId: string;
};
export declare type TaskType<T> = {
    id: string;
} & TaskPropsType<T>;
export declare type ApiResultType<T = any> = Result<T> | Result<T[]> | typeof API_NO_RESULT | typeof API_INITIAL_VALUE;
export declare type WebHookResultType<TData = any> = {
    taskId: string;
    ok: boolean;
    message: string;
    __type: "WebHookResultType";
};
export declare type TaskOnStartFnType = (abortFn: TaskAbortFnType) => void;
export declare type TaskRunnerType<TResponse = any, TState = any> = {
    id: string;
    entityID?: string | string[];
    props: TaskPropsType<TResponse, TState>;
    run: TaskFactoryFn;
    cancel$: Subject<any>;
};
export declare type TaskObservableRunnerType<TResponse, TState> = TaskRunnerType<TResponse, TState>;
export declare type TaskFactoryFn<T = any> = (progress?: PartialObserver<ProgressEvent>) => TaskObervableType<T>;
export declare type TaskObervableType<T = any> = Observable<ApiResultType<T> | ApiResultType<T>[]>;
export declare type ApiTaskType<T = any> = TaskPropsType<T>;
declare type ObservableRunnner<T> = Observer<ApiResultType<T>>;
export declare function createApiTask<T, TState>(props: Partial<TaskPropsType<T, TState>>, runner: TaskFactoryFn<T>): TaskObservableRunnerType<T, TState>;
export declare function createApiObserver<T extends ApiResultType<any>>(props: TaskPropsType, pushMessage: (message: Message) => void): ObservableRunnner<T>;
export declare function useTaskManager(): TaskManagerContextType;
export declare type TaskAbortFnType = () => void;
export declare type CreateApiTaskFn = <T = any, TState = any>(runner: TaskFactoryFn<T>, props: TaskPropsType<T, TState>) => TaskObservableRunnerType<T, TState>;
export declare type TaskItemTupple = [TaskRunnerType<any>, Observable<any>, ObservableRunnner<any>, Priority];
export declare enum Priority {
    IMMEDIATE = 0,
    OTHERS_WAIT = 1,
    NORMAL = 2
}
export declare function taskRunner(): void;
export declare function createProgressObserver(taskId: string, entityID?: string | string[]): Observer<ProgressEvent>;
export declare type TaskDescriptorType = {
    runner: TaskFactoryFn<any>;
    props: TaskPropsType<any>;
};
export {};
//# sourceMappingURL=TaskManagerContext.d.ts.map