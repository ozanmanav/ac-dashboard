var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { useContext, createContext } from "react";
import shortid from "shortid";
import { from, Subject } from "rxjs";
import { ApiError } from "@appcircle/shared/lib/services/ApiError";
import { ApiAuthError } from "@appcircle/shared/lib/services/ApiAuthError";
import { createAjaxError } from "@appcircle/shared/lib/services/createAjaxError";
import { createAjaxResult } from "@appcircle/shared/lib/services/createAjaxResult";
import { filter, tap, startWith, mergeAll, take, takeUntil } from "rxjs/operators";
import { getTaskManagerState, getTaskManagerDispatch } from "./TaskManagerProvider";
import { API_INITIAL_VALUE, API_NO_RESULT } from "@appcircle/core/lib/services/ApiResultType";
export var TaskManagerContextTypeInitialValue = {
    addTask: function () { },
    createApiTask: function () { return ({}); },
    getStatusByEntityID: function () { return undefined; },
    getStatusByTaskId: function () { return undefined; },
    getActiveTasksByEntityId: function () { return []; },
    getStatusByRemoteTaskId: function () { return undefined; }
};
export var TaskManagerContext = createContext(TaskManagerContextTypeInitialValue);
export var TASK_MANAGER_NO_RESULT = API_NO_RESULT;
export var TASK_MANAGER_API_INITIAL_VALUE = API_INITIAL_VALUE;
export function createApiTask(props, runner) {
    var cancel$ = new Subject();
    return {
        id: shortid(),
        props: __assign({}, props),
        run: function (progress) {
            return from(runner(progress)).pipe(startWith(TASK_MANAGER_API_INITIAL_VALUE), tap(function (res) {
                res === TASK_MANAGER_API_INITIAL_VALUE &&
                    props.onStart &&
                    props.onStart(function () { });
            }), filter(function (res) { return res !== TASK_MANAGER_API_INITIAL_VALUE; }));
        },
        cancel$: cancel$
    };
}
export function createApiObserver(props, pushMessage) {
    return {
        next: function (result) {
            if (result === TASK_MANAGER_API_INITIAL_VALUE) {
                return;
            }
            props.onSuccess && props.onSuccess(result);
            var message = props.successMessage && props.successMessage(result);
            message &&
                pushMessage({
                    messageType: "success",
                    message: message,
                    life: 3000
                });
            props.onFinally && props.onFinally(result);
        },
        error: function (result) {
            var response = createAjaxError(result);
            props.onError && props.onError(response);
            if (response.error instanceof ApiError) {
                var errorMessage = response.error.message ||
                    (props.errorMessage && props.errorMessage(response)) ||
                    "";
                pushMessage({
                    messageType: "error",
                    message: response.error.innerErrors
                        ? response.error.innerErrors[0].message
                        : errorMessage,
                    life: 5000
                });
            }
            else if (response.error instanceof ApiAuthError) {
                pushMessage({
                    messageType: "error",
                    message: response.error.error_description,
                    messageId: true,
                    life: 5000
                });
            }
            else {
                response &&
                    pushMessage({
                        messageType: "error",
                        message: (props.errorMessage && props.errorMessage(response)) || "",
                        life: 5000
                    });
            }
            props.onFinally && props.onFinally(result);
        },
        complete: function () {
            // console.log("Completed");
        }
    };
}
export function useTaskManager() {
    var value = useContext(TaskManagerContext);
    return value;
}
export var Priority;
(function (Priority) {
    Priority[Priority["IMMEDIATE"] = 0] = "IMMEDIATE";
    Priority[Priority["OTHERS_WAIT"] = 1] = "OTHERS_WAIT";
    Priority[Priority["NORMAL"] = 2] = "NORMAL";
})(Priority || (Priority = {}));
// const taskStore = new Map<string, TaskItemTupple>();
export function taskRunner() {
    var state = getTaskManagerState();
    var taskCache = {};
    var tasksByPriority = {};
    var ids = [];
    var taskSubject = new Subject();
    var dispatch = getTaskManagerDispatch();
    state.tasks.forEach(function (tupple) {
        var task = tupple[0], task$ = tupple[1], observer = tupple[2], priority = tupple[3];
        var hash = Array.isArray(task.entityID)
            ? task.entityID.join("-")
            : task.entityID;
        !tasksByPriority[priority] && (tasksByPriority[priority] = []);
        // hash && !taskCache[hash]
        //   ? tasksByPriority[priority].push(task$.pipe(takeUntil(task.cancel$)))
        //   :
        task$ &&
            tasksByPriority[priority].push(task$.pipe(takeUntil(task.cancel$)));
        // taskStore.has(task.id) && taskStore.delete(task.id);
        ids.push(task.id);
        if (hash) {
            taskCache[hash] = true;
        }
    });
    taskSubject
        .pipe(take(Object.keys(tasksByPriority).length), mergeAll(1))
        .subscribe({
        next: function (resp) { },
        error: function (resp) {
            console.error(resp);
        },
        complete: function () { }
    });
    dispatch({ type: "remove", taskId: ids });
    // Sort task by priority
    Object.keys(tasksByPriority)
        .map(function (key) { return parseInt(key); })
        .sort(function (a, b) { return a - b; })
        .map(function (key) { return from(tasksByPriority[key]).pipe(mergeAll(3)); })
        .forEach(function (task$) { return taskSubject.next(task$); });
}
export function createProgressObserver(taskId, entityID) {
    return {
        next: function (e) {
            var percent = 100 * (e.loaded / e.total);
            getTaskManagerDispatch()({
                type: "updateStatus",
                payload: {
                    taskId: taskId,
                    status: percent < 100 ? "IN_PROGRESS" : "WAITING_RESPONSE",
                    data: createAjaxResult({
                        response: percent
                    }),
                    entityID: entityID
                }
            });
        },
        error: function () { },
        complete: function () {
            getTaskManagerDispatch()({
                type: "updateStatus",
                payload: {
                    taskId: taskId,
                    status: "WAITING_RESPONSE",
                    data: createAjaxResult({
                        response: 100
                    }),
                    entityID: entityID
                }
            });
        }
    };
}
//# sourceMappingURL=TaskManagerContext.js.map