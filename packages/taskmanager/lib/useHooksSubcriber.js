import { useContext, useEffect, useState } from "react";
import { filter } from "rxjs/operators";
import { HookContext } from "@appcircle/sse/lib/HookContext";
export function useHooksSubcriber(eventNames, remoteTaskId) {
    var hookListener$ = useContext(HookContext);
    var _a = useState(null), response = _a[0], setResponse = _a[1];
    useEffect(function () {
        var subscriber;
        if (eventNames) {
            subscriber = hookListener$
                .pipe(filter(function (data) {
                return remoteTaskId ? data.json.taskId === remoteTaskId : true;
            }), filter(function (event) {
                return eventNames.some(function (name) {
                    return event.json.eventName === name || event.json.eventAction === name;
                });
            }))
                .subscribe({
                next: function (resp) {
                    setResponse(resp.json);
                },
                error: function (resp) {
                    setResponse(resp);
                }
            });
        }
        return function () { return subscriber && subscriber.unsubscribe(); };
    }, [hookListener$, remoteTaskId]);
    return response;
}
//# sourceMappingURL=useHooksSubcriber.js.map