import { TaskFactoryFn } from "./TaskManagerContext";
export declare type ServiceFactory<P, U> = (data: P) => TaskFactoryFn<U>;
//# sourceMappingURL=ServiceFactory.d.ts.map