import { TaskItemTupple, Priority, TaskPropsType } from "./TaskManagerContext";
import { TaskStatus } from "./TaskStatus";
import { HookEventActions } from "@appcircle/sse/lib/HookEventActions";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";
export declare type TaskManagerActions = {
    type: "add";
    task: TaskItemTupple;
    priority?: Priority;
    entityID?: string | string[];
} | {
    type: "createTask";
    payload: {
        task: TaskPropsType;
        data?: any;
        error?: any;
        remoteTaskId: string;
        id: string;
        priority?: Priority;
    };
} | {
    type: "resetErrors";
} | {
    type: "remove";
    taskId: string | string[];
    entityID?: string | string[];
} | {
    type: "updateStatus";
    payload: {
        taskId: string;
        status: TaskStatus;
        action?: HookEventActions;
        data?: any;
        entityID?: string | string[];
        error?: AjaxResultError;
        state?: any;
    };
} | {
    type: "waitingForRemoteTask";
    payload: {
        taskId: string;
        remoteTaskId: string;
    };
};
//# sourceMappingURL=TaskManagerActions.d.ts.map