import { useContext, createContext } from "react";
import shortid from "shortid";
import {
  Observable,
  Observer,
  from,
  Subject,
  PartialObserver,
  UnaryFunction
} from "rxjs";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";
import { ApiError } from "@appcircle/shared/lib/services/ApiError";
import { ApiAuthError } from "@appcircle/shared/lib/services/ApiAuthError";
import { Result } from "@appcircle/core/lib/services/Result";
import { createAjaxError } from "@appcircle/shared/lib/services/createAjaxError";
import { createAjaxResult } from "@appcircle/shared/lib/services/createAjaxResult";
import { Message } from "@appcircle/shared/lib/Module";
import {
  filter,
  tap,
  startWith,
  mergeAll,
  take,
  takeUntil
} from "rxjs/operators";
import { AjaxError } from "rxjs/ajax";
import {
  getTaskManagerState,
  getTaskManagerDispatch
} from "./TaskManagerProvider";
import { HookResultEvent } from "@appcircle/shared/lib/services/HookResultEvent";
import {
  API_INITIAL_VALUE,
  API_NO_RESULT
} from "@appcircle/core/lib/services/ApiResultType";
import { TaskManagerContextType } from "./TaskManagerContextType";
export const TaskManagerContextTypeInitialValue: TaskManagerContextType = {
  addTask: () => {},
  createApiTask: () => ({} as any),
  getStatusByEntityID: () => undefined,
  getStatusByTaskId: () => undefined,
  getActiveTasksByEntityId: () => [],
  getStatusByRemoteTaskId: () => undefined
};

export const TaskManagerContext = createContext<TaskManagerContextType>(
  TaskManagerContextTypeInitialValue
);
export type TaskCallbackFn<T, TRet = void> = (
  res: T extends Error ? T : ApiResultType<T>
) => TRet;
export type Pipe<T, R> = UnaryFunction<T, R>[];

export type TaskPropsType<TResponse = any, TState = {}> = {
  pipe?: Pipe<any, any>;
  taskPipe?: TaskFactoryFn[];
  ignoreTaskId?: boolean;
  retryCount?: number;
  onProgress?: (e: ProgressEvent) => void;
  progressMessage?: TaskCallbackFn<TResponse, string>;
  successMessage?: TaskCallbackFn<TResponse, string>;
  errorMessage?: TaskCallbackFn<AjaxResultError, string>;
  onSuccess?: TaskCallbackFn<TResponse>;
  onError?: TaskCallbackFn<AjaxResultError | Error>;
  onFinally?: TaskCallbackFn<TResponse>;
  entityIDs?: string[];
  state?: TState;
  /**
   * To pass AbortHandler to client
   */
  onStart?: TaskOnStartFnType;
};
export const TASK_MANAGER_NO_RESULT = API_NO_RESULT;
export const TASK_MANAGER_API_INITIAL_VALUE: API_INITIAL_VALUE = API_INITIAL_VALUE;
type TASK_MANAGER_API_INITIAL_VALUE = typeof TASK_MANAGER_API_INITIAL_VALUE;
export type ResultError = {
  errors: { [key: string]: string[] };
  status: number;
  title: string;
  traceId: string;
};
export type TaskType<T> = {
  id: string;
} & TaskPropsType<T>;
export type ApiResultType<T = any> =
  | Result<T>
  | Result<T[]>
  | typeof API_NO_RESULT
  | typeof API_INITIAL_VALUE;

export type WebHookResultType<TData = any> = {
  taskId: string;
  ok: boolean;
  message: string;
  __type: "WebHookResultType";
};

export type TaskOnStartFnType = (abortFn: TaskAbortFnType) => void;
export type TaskRunnerType<TResponse = any, TState = any> = {
  id: string;
  entityID?: string | string[];
  props: TaskPropsType<TResponse, TState>;
  run: TaskFactoryFn;
  // (progress?: Observer<ProgressEvent>) => Observable<any>;
  cancel$: Subject<any>;
};

export type TaskObservableRunnerType<TResponse, TState> = TaskRunnerType<
  TResponse,
  TState
>;
export type TaskFactoryFn<T = any> = (
  progress?: PartialObserver<ProgressEvent>
) => TaskObervableType<T>;
export type TaskObervableType<T = any> = Observable<
  ApiResultType<T> | ApiResultType<T>[]
>;

export type ApiTaskType<T = any> = TaskPropsType<T>;

type ObservableRunnner<T> = Observer<ApiResultType<T>>;
type NoResult = typeof TASK_MANAGER_NO_RESULT;

export function createApiTask<T, TState>(
  props: Partial<TaskPropsType<T, TState>>,
  runner: TaskFactoryFn<T>
): TaskObservableRunnerType<T, TState> {
  const cancel$ = new Subject();
  return {
    id: shortid(),
    props: { ...props },
    run: progress => {
      return from<TaskObervableType>(runner(progress)).pipe(
        startWith(TASK_MANAGER_API_INITIAL_VALUE),
        tap(res => {
          res === TASK_MANAGER_API_INITIAL_VALUE &&
            props.onStart &&
            props.onStart(() => {});
        }),
        filter(res => res !== TASK_MANAGER_API_INITIAL_VALUE)
      );
    },
    cancel$
  };
}

export function createApiObserver<T extends ApiResultType<any>>(
  props: TaskPropsType,
  pushMessage: (message: Message) => void
): ObservableRunnner<T> {
  return {
    next(result) {
      if (result === TASK_MANAGER_API_INITIAL_VALUE) {
        return;
      }
      props.onSuccess && props.onSuccess(result as any);
      const message = props.successMessage && props.successMessage(result);
      message &&
        pushMessage({
          messageType: "success",
          message: message,
          life: 3000
        });
      props.onFinally && props.onFinally(result);
    },
    error(result: AjaxError | HookResultEvent) {
      const response = createAjaxError(result);

      props.onError && props.onError(response);
      if (response.error instanceof ApiError) {
        const errorMessage =
          response.error.message ||
          (props.errorMessage && props.errorMessage(response)) ||
          "";

        pushMessage({
          messageType: "error",
          message: response.error.innerErrors
            ? response.error.innerErrors[0].message
            : errorMessage,
          life: 5000
        });
      } else if (response.error instanceof ApiAuthError) {
        pushMessage({
          messageType: "error",
          message: response.error.error_description,
          messageId: true,
          life: 5000
        });
      } else {
        response &&
          pushMessage({
            messageType: "error",
            message: (props.errorMessage && props.errorMessage(response)) || "",
            life: 5000
          });
      }

      props.onFinally && props.onFinally(result);
    },
    complete() {
      // console.log("Completed");
    }
  };
}

export function useTaskManager() {
  const value = useContext(TaskManagerContext);

  return value;
}

export type TaskAbortFnType = () => void;

export type CreateApiTaskFn = <T = any, TState = any>(
  runner: TaskFactoryFn<T>,
  props: TaskPropsType<T, TState>
) => TaskObservableRunnerType<T, TState>;

export type TaskItemTupple = [
  TaskRunnerType<any>,
  Observable<any>,
  ObservableRunnner<any>,
  Priority
];

export enum Priority {
  IMMEDIATE = 0,
  OTHERS_WAIT = 1,
  NORMAL = 2
}

// const taskStore = new Map<string, TaskItemTupple>();

export function taskRunner() {
  const state = getTaskManagerState();
  const taskCache: { [key: string]: boolean } = {};
  const tasksByPriority: { [key: number]: Observable<any>[] } = {};
  const ids: string[] = [];
  const taskSubject = new Subject<Observable<any>>();
  const dispatch = getTaskManagerDispatch();
  state.tasks.forEach(tupple => {
    const [task, task$, observer, priority] = tupple;
    const hash = Array.isArray(task.entityID)
      ? task.entityID.join("-")
      : task.entityID;

    !tasksByPriority[priority] && (tasksByPriority[priority] = []);
    // hash && !taskCache[hash]
    //   ? tasksByPriority[priority].push(task$.pipe(takeUntil(task.cancel$)))
    //   :
    task$ &&
      tasksByPriority[priority].push(task$.pipe(takeUntil(task.cancel$)));
    // taskStore.has(task.id) && taskStore.delete(task.id);
    ids.push(task.id);
    if (hash) {
      taskCache[hash] = true;
    }
  });
  taskSubject
    .pipe(take(Object.keys(tasksByPriority).length), mergeAll(1))
    .subscribe({
      next: resp => {},
      error: resp => {
        console.error(resp);
      },
      complete: () => {}
    });

  dispatch({ type: "remove", taskId: ids });

  // Sort task by priority
  Object.keys(tasksByPriority)
    .map(key => parseInt(key))
    .sort((a: number, b: number) => a - b)
    .map((key: number) => from(tasksByPriority[key]).pipe(mergeAll(3)))
    .forEach(task$ => taskSubject.next(task$));
}

export function createProgressObserver(
  taskId: string,
  entityID?: string | string[]
): Observer<ProgressEvent> {
  return {
    next: e => {
      const percent = 100 * (e.loaded / e.total);
      getTaskManagerDispatch()({
        type: "updateStatus",
        payload: {
          taskId,
          status: percent < 100 ? "IN_PROGRESS" : "WAITING_RESPONSE",
          data: createAjaxResult<number>({
            response: percent
          }),
          entityID
        }
      });
    },
    error: () => {},
    complete: () => {
      getTaskManagerDispatch()({
        type: "updateStatus",
        payload: {
          taskId,
          status: "WAITING_RESPONSE",
          data: createAjaxResult<number>({
            response: 100
          }),
          entityID
        }
      });
    }
  };
}

export type TaskDescriptorType = {
  runner: TaskFactoryFn<any>;
  props: TaskPropsType<any>;
};
