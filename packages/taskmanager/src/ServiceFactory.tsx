import { TaskFactoryFn } from "./TaskManagerContext";
export type ServiceFactory<P, U> = (data: P) => TaskFactoryFn<U>;
