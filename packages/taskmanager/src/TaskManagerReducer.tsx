import { TaskManagerActions } from "./TaskManagerActions";
import produce from "immer";
import {
  TaskManagerStateType,
  TaskManagerInitialState
} from "./TaskManagerProvider";
import { Reducer } from "redux";
import { of, Subject } from "rxjs";
import { Priority, TASK_MANAGER_NO_RESULT } from "./TaskManagerContext";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";

export function arrayMaybe<T extends any = any>(val?: T | T[]): T[] {
  return val && Array.isArray(val) ? val : val ? [val] : [];
}
export const TaskManagerReducer: Reducer<
  TaskManagerStateType,
  TaskManagerActions
> = (state = TaskManagerInitialState, action) => {
  // const newState: TaskManagerStateType = { ...state };
  return produce(state, draft => {
    switch (action.type) {
      case "add":
        draft.tasks = new Map(draft.tasks);
        {
          const [task] = action.task;
          draft.tasks.set(task.id, action.task);
          if (task.entityID || task.props.entityIDs) {
            draft.tasksByEntity = new Map(draft.tasksByEntity);
            arrayMaybe(task.entityID)
              .concat(arrayMaybe(task.props.entityIDs || []))
              .forEach(id => {
                id && draft.tasksByEntity.set(id, task.id);
              });
          }
        }
        break;
      case "createTask":
        {
          draft.tasks = new Map(draft.tasks);
          const task = action.payload.task;
          draft.tasks.set(action.payload.id, [
            {
              id: action.payload.id,
              props: action.payload.task,
              cancel$: new Subject(),
              entityID: action.payload.task.entityIDs,
              run: () => of(TASK_MANAGER_NO_RESULT)
            },
            of(TASK_MANAGER_NO_RESULT),
            { next: () => {}, error: () => {}, complete: () => {} },
            Priority.NORMAL
          ]);
          draft.remotetaskIdsByTaskId.set(
            action.payload.id,
            action.payload.remoteTaskId
          );
          draft.remotetaskIdsByTaskId = new Map(draft.remotetaskIdsByTaskId);
          draft.taskIdByRemoteTaskId[action.payload.remoteTaskId] =
            action.payload.id;
          if (task.entityIDs) {
            draft.tasksByEntity = new Map(draft.tasksByEntity);
            arrayMaybe(task.entityIDs)
              .concat(arrayMaybe(task.entityIDs || []))
              .forEach(id => {
                id && draft.tasksByEntity.set(id, action.payload.id);
              });
          }
          draft.tasksStatuses.set(action.payload.id, {
            status: "ADDED",
            taskId: action.payload.id,
            state: task.state,
            data: action.payload.data,
            entityID: task.entityIDs,
            error: action.payload.error,
            remoteTaskId: action.payload.remoteTaskId
          });
          draft.tasksStatuses = new Map(draft.tasksStatuses);
        }
        break;
      case "waitingForRemoteTask":
        {
          draft.remotetaskIdsByTaskId.set(
            action.payload.taskId,
            action.payload.remoteTaskId
          );
          draft.remotetaskIdsByTaskId = new Map(draft.remotetaskIdsByTaskId);
          draft.taskIdByRemoteTaskId[action.payload.remoteTaskId] =
            action.payload.taskId;

          const status = draft.tasksStatuses.has(action.payload.taskId)
            ? draft.tasksStatuses.get(action.payload.taskId)
            : null;
          if (status) {
            draft.tasksStatuses.set(action.payload.taskId, {
              ...status,
              remoteTaskId: action.payload.remoteTaskId
            });
            draft.tasksStatuses = new Map(draft.tasksStatuses);
          }
        }

        break;
      case "updateStatus":
        // newState.tasksStatus = new Map(state.tasksStatus);
        const oldStatus = draft.tasksStatuses.has(action.payload.taskId)
          ? draft.tasksStatuses.get(action.payload.taskId)
          : {};
        const status = {
          ...oldStatus,
          ...action.payload,
          remoteTaskId: draft.remotetaskIdsByTaskId.has(action.payload.taskId)
            ? draft.remotetaskIdsByTaskId.get(action.payload.taskId)
            : undefined
        };
        if (action.payload.taskId) {
          draft.tasksStatuses = new Map(draft.tasksStatuses);
          draft.tasksStatuses.set(action.payload.taskId, status);
        }
        if (action.payload.status === "ERROR") {
          if (action.payload.error) {
            draft.tasksByErrors = new Map(draft.tasksByErrors);
            draft.tasksByErrors.set(
              (action.payload.error as AjaxResultError).status,
              action.payload.taskId
            );
          }
        }
        break;
      case "remove":
        let ids: string[];
        if (typeof action.taskId === "string") {
          ids = [action.taskId];
        } else {
          ids = action.taskId;
        }
        draft.tasks = new Map(draft.tasks);
        ids.forEach(id => {
          draft.tasks.delete(id);
        });
        break;
      case "resetErrors":
        draft.tasksByErrors = new Map();
        break;
    }
  });
};
