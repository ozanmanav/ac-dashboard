export function createTaskActionId(action: string, id: string) {
  return action + "_" + id;
}
