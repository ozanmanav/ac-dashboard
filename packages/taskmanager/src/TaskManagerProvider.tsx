import {
  PropsWithChildren,
  useEffect,
  useState,
  createContext,
  useContext,
  useCallback,
  useMemo
} from "react";
import { Store, createStore } from "redux";
import { TaskManagerReducer } from "./TaskManagerReducer";
import { TaskItemTupple, TaskPropsType } from "./TaskManagerContext";
import { TaskStatusType } from "./TaskStatusType";
import { TaskManagerActions } from "./TaskManagerActions";
import React from "react";
import { TaskManagerServiceProvider } from "./TaskManagerServiceProvider";
import { TaskManagerMiddlewareProvider } from "./TaskManagerMiddlewareProvider";
// import { HookProvider } from "../../sse/lib/HookProvider";
import { Observable } from "rxjs";

export const TaskManagerInitialState: TaskManagerStateType = {
  tasks: new Map(),
  // tasksStatus: new Map(),
  tasksByEntity: new Map(),
  tasksStatuses: new Map(),
  tasksByErrors: new Map(),
  tasksCache: new Map(),
  remotetaskIdsByTaskId: new Map(),
  remotetaskIdsByEntityId: new Map(),
  taskIdByRemoteTaskId: {}
};
export type TaskManagerStateType = {
  tasks: Map<string, TaskItemTupple>;
  // tasksStatus: Map<string, TaskStatusType>;
  tasksByEntity: Map<string, string>;
  tasksStatuses: Map<string, TaskStatusType>;
  tasksByErrors: Map<number, string>;
  tasksCache: Map<number, string>;
  remotetaskIdsByTaskId: Map<string, string>;
  remotetaskIdsByEntityId: Map<string, string>;
  taskIdByRemoteTaskId: { [remoteId: string]: string };
};
const taskManagerStore: Store<
  TaskManagerStateType,
  TaskManagerActions
> = createStore(TaskManagerReducer, TaskManagerInitialState);

export const TaskManagerStoreContext = createContext<TaskManagerStateType>(
  TaskManagerInitialState
);

export function useTaskManagerDispatch() {
  return taskManagerStore.dispatch;
}

export function getTaskManagerDispatch() {
  return taskManagerStore.dispatch;
}

export function getTaskManagerState() {
  return taskManagerStore.getState();
}

export function useTasksError(code = 401) {
  const state = useContext(TaskManagerStoreContext);
  const [tasks, setTasks] = useState(() => state.tasks);
  const [errors, setErrors] = useState<string[]>([]);
  useEffect(() => {
    if (state.tasks.size === 0 && tasks.size !== 0) {
      const _errors: string[] = [];
      tasks.forEach(task => {
        const status = state.tasksStatuses.has(task[0].id)
          ? state.tasksStatuses.get(task[0].id)
          : undefined;

        status &&
          status.error &&
          status.error.status === code &&
          _errors.push(task[0].id);

        status &&
          status.status !== "COMPLETED" &&
          status.status !== "ERROR" &&
          tasks.delete(task[0].id);
      });
      setErrors([...errors, ..._errors]);
      setTasks(new Map(tasks));
    }
    // eslint-disable-next-line
  }, [code, errors, state.tasks.size, state.tasksStatuses]);

  return errors;
}

export function useCheckUnauthrizedTask() {
  const state = useContext(TaskManagerStoreContext);
  const dispatch = useTaskManagerDispatch();
  const reset = useCallback(() => {
    dispatch({ type: "resetErrors" });
  }, [dispatch]);
  const value = useMemo<[Boolean, () => void]>(
    () => [state.tasksByErrors.has(401), reset],
    [reset, state.tasksByErrors]
  );
  return value;
}
export type MiddlewareFn = (
  task: Observable<any>,
  props: TaskPropsType
) => Observable<any>;
export function TaskManagerProvider(
  props: PropsWithChildren<{ middlewares: MiddlewareFn[] }>
) {
  const [state, setState] = useState(() => taskManagerStore.getState());
  useEffect(() => {
    const unsubscribe = taskManagerStore.subscribe(() => {
      setState(taskManagerStore.getState());
    });
    return () => unsubscribe();
    // eslint-disable-next-line
  }, []);
  return (
    <TaskManagerStoreContext.Provider value={state}>
      <TaskManagerServiceProvider state={state}>
        <TaskManagerMiddlewareProvider>
          {props.children}
        </TaskManagerMiddlewareProvider>
      </TaskManagerServiceProvider>
    </TaskManagerStoreContext.Provider>
  );
}
