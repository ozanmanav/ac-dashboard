import { useEffect, useMemo } from "react";
import { useTaskManager } from "./TaskManagerContext";
import { useTaskManagerDispatch } from "./TaskManagerProvider";
import { useHooksSubcriber } from "./useHooksSubcriber";
import shortid from "shortid";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { arrayMaybe } from "./TaskManagerReducer";
import { EventTaskStatus } from "@appcircle/shared/lib/EventTaskStatus";
import { isEmptyId } from "@appcircle/core/lib/utils/isEmptyId";

export const RESUME_PREFIX: string = "resumed_";
// TODO: Move to web or share
export const useTaskResumeService = () => {
  const event = useHooksSubcriber(
    useMemo(
      () => [
        "Build|BuildProgress",
        "Build|BuildRunning",
        "Build|BuildSuccess",
        "Build|BuildQueued",
        "Build|BuildFailed",
        "Build|FetchProgress",
        "Build|FetchQueued",
        "Build|FetchRunning",
        "Build|SettingRetrieved"
      ],
      []
    )
  );
  const taskManager = useTaskManager();
  const eventId = event && event.data && (event.data.commitId || event.data.id);
  const task =
    (event &&
      taskManager.getStatusByRemoteTaskId(event.taskId || event.data.taskId)) ||
    (eventId && arrayMaybe(taskManager.getActiveTasksByEntityId(eventId))[0]);

  const dispatch = useTaskManagerDispatch();
  const { pushMessage } = useNotificationsService();

  useEffect(() => {
    if (!task && event) {
      const action =
        event.eventName.indexOf("Build|Build") === 0 ? "Build" : "Fetch";
      pushMessage({
        messageType: "general",
        // TODO: Move to messages
        message: `A ${action} task is in progress.`
      });
      dispatch({
        type: "createTask",
        payload: {
          task: {
            entityIDs: ["resumed_" + (event.data.commitId || event.data.id)],
            state: {
              resumed: true,
              action: action.toLowerCase(),
              actionEntityId: event.data.commitId || event.data.id,
              branchId: event.data.branchId,
              profileId: event.data.profileId
            }
          },
          id: shortid(),
          remoteTaskId: event.taskId || event.data.taskId
        }
      });
    }
  }, [event]);

  useEffect(() => {
    if (
      !!event &&
      task &&
      task.state &&
      task.state.resumed &&
      task.status !== "COMPLETED"
    ) {
      if (
        event.status === EventTaskStatus.Started ||
        event.status === EventTaskStatus.InProgress
      ) {
        dispatch({
          type: "updateStatus",
          payload: {
            taskId: task.taskId,
            status: "IN_PROGRESS",
            data: event.data,
            action: event.eventAction,
            state: isEmptyId(task.state.branchId)
              ? {
                  ...task.state,
                  branchId: event.data.branchId,
                  profileId: event.data.profileId
                }
              : task.state
          }
        });
      } else if (
        event.status === EventTaskStatus.Completed ||
        event.status === EventTaskStatus.Cancel
      ) {
        if (event.isSuccess && event.message) {
          pushMessage({
            messageType: "success",
            message: event.message
          });
        } else if (event.message) {
          pushMessage({
            messageType: "error",
            message: event.message
          });
        }
        dispatch({
          type: "updateStatus",
          payload: {
            taskId: task.taskId,
            status: "COMPLETED",
            data: event.data
          }
        });
      }
    }
  }, [event]);
};
