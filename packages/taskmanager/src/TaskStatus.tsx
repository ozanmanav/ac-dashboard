export type TaskStatus =
  | "ADDED"
  | "IN_PROGRESS"
  | "WAITING_RESPONSE"
  | "COMPLETED"
  | "ERROR"
  | "CANCELLED";
