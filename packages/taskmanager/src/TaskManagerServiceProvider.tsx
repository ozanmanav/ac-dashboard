import {
  PropsWithChildren,
  useCallback,
  useMemo,
  createContext,
  useContext,
  useEffect,
  Dispatch
} from "react";
import { of, from, Observable } from "rxjs";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";
import { Result } from "@appcircle/core/lib/services/Result";
import {
  filter,
  tap,
  catchError,
  finalize,
  take,
  mergeMap,
  takeUntil,
  map,
  startWith
} from "rxjs/operators";
import React from "react";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import {
  TaskRunnerType,
  Priority,
  createApiObserver,
  createProgressObserver,
  taskRunner,
  CreateApiTaskFn,
  createApiTask,
  TaskManagerContext,
  ApiResultType
} from "./TaskManagerContext";
import { TaskStatusType } from "./TaskStatusType";
import { TaskManagerContextType } from "./TaskManagerContextType";
import {
  useTaskManagerDispatch,
  TaskManagerStateType,
  MiddlewareFn
} from "./TaskManagerProvider";

import { pipeFromArray } from "rxjs/internal/util/pipe";
import { HookContext } from "@appcircle/sse/lib/HookContext";
import { TaskManagerActions } from "./TaskManagerActions";
import { EventTaskStatus } from "@appcircle/shared/lib/EventTaskStatus";
import {
  API_NO_RESULT,
  API_INITIAL_VALUE
} from "@appcircle/core/lib/services/ApiResultType";

let timeout: NodeJS.Timeout;

type MiddlewaresActions =
  | { type: "add"; payload: MiddlewareFn | MiddlewareFn[] }
  | { type: "remove"; payload: MiddlewareFn };
type MiddlewareState = {
  middlewares: MiddlewareFn[];
};
export const MiddlewaresInitialState: MiddlewareState = {
  middlewares: []
};
export const TaskManagerMiddlewareDispatchContext = createContext<React.Dispatch<
  MiddlewaresActions
> | null>(null);
export const TaskManagerMiddlewareStateContext = createContext<MiddlewareState>(
  MiddlewaresInitialState
);

export const middlewaresReducer: React.Reducer<
  MiddlewareState,
  MiddlewaresActions
> = (state, action) => {
  switch (action.type) {
    case "add":
      const mids = Array.isArray(action.payload)
        ? action.payload
        : [action.payload];
      return { middlewares: [...state.middlewares, ...mids] };
    case "remove":
      return {
        middlewares: state.middlewares.filter(fn => fn !== action.payload)
      };
  }

  return state;
};

export function useGetMiddlewares() {
  const state = useContext(TaskManagerMiddlewareStateContext);

  return state.middlewares;
}

export function useAddMiddlewares() {
  const dispatch = useContext(TaskManagerMiddlewareDispatchContext);
  const fn = useCallback(
    (middlewares: MiddlewareFn[]) => {
      dispatch &&
        middlewares &&
        dispatch({
          type: "add",
          payload: middlewares
        });
    },
    [dispatch]
  );

  return dispatch ? fn : null;
}

const observTask = (
  task: TaskRunnerType,
  taskObserver: ReturnType<typeof createApiObserver>,
  dispatch: Dispatch<TaskManagerActions>
) => (resp: ApiResultType) => {
  if (resp === API_NO_RESULT || resp === API_INITIAL_VALUE) {
    return;
  }
  if (
    resp.json &&
    resp.json.taskId &&
    (resp.json.isSuccess === false ||
      resp.json.eventAction === "Failed" ||
      resp.json.eventAction === "Cancelled")
  ) {
    dispatch({
      type: "updateStatus",
      payload: {
        taskId: task.id,
        status: "COMPLETED",
        data: resp,
        error: resp.json,
        entityID: task.entityID,
        action: resp.json.eventAction
      }
    });
    taskObserver.error(resp);
  } else if (resp instanceof Error) {
    dispatch({
      type: "updateStatus",
      payload: {
        taskId: task.id,
        status: "COMPLETED",
        data: null,
        error: resp as AjaxResultError,
        entityID: task.entityID
      }
    });
    taskObserver.error(resp);
  } else {
    dispatch({
      type: "updateStatus",
      payload: {
        taskId: task.id,
        status: "COMPLETED",
        data: resp,
        entityID: task.entityID
      }
    });
    taskObserver.next(resp);
  }
};

export function TaskManagerServiceProvider({
  state,
  children
}: PropsWithChildren<{ state: TaskManagerStateType }>) {
  const { pushMessage } = useNotificationsService();
  const dispatch = useTaskManagerDispatch();
  const hookListener$ = useContext(HookContext);
  const middlewares = useGetMiddlewares();
  const addMiddleware = useAddMiddlewares();
  useEffect(() => {
    addMiddleware &&
      addMiddleware([
        task => {
          return task;
        }
      ]);
  }, [addMiddleware]);
  const addTask = useCallback<
    (task: TaskRunnerType, priority?: Priority) => string
  >(
    (task, priority = 2) => {
      const entityIDs = task.entityID;
      // [arrayMaybe(task.entityID as string), ...(task.props.entityIDs || [])] as string[];
      dispatch({
        type: "updateStatus",
        payload: {
          taskId: task.id,
          status: "ADDED",
          data: null,
          entityID: entityIDs,
          state: task.props.state
        }
      });
      const taskObserver = createApiObserver(task.props, pushMessage);
      const progressObserver = createProgressObserver(task.id, task.entityID);
      const task$ = (middlewares.length
        ? middlewares.reduce<Observable<any>>(
            (acc, curr) => curr(acc, task.props),
            task.run(progressObserver)
          )
        : task.run(progressObserver)
      )
        // task.run(progressObserver)
        .pipe(
          mergeMap(resp => {
            const return$ =
              (resp as Result).json &&
              !task.props.ignoreTaskId &&
              (resp as Result).json.taskId
                ? // If the response has a taskId then it means a remote task startted and wait its response from HookServver.
                  hookListener$.pipe(
                    takeUntil(task.cancel$),
                    filter(response => {
                      return (
                        response.json.taskId === (resp as Result).json.taskId &&
                        (response.json.status === EventTaskStatus.Completed ||
                          response.json.status === EventTaskStatus.Cancel)
                      );
                    })
                  )
                : // If not just return the response.
                  of(resp);

            (resp as Result).json.taskId &&
              dispatch({
                type: "waitingForRemoteTask",
                payload: {
                  taskId: task.id,
                  remoteTaskId: (resp as Result).json.taskId
                }
              });

            // If the task has a pipe array
            // then adds hooklistener after the Pipe
            return (task.props.pipe
              ? pipeFromArray(task.props.pipe)(return$)
              : return$
            ).pipe(take(1));
          }),
          map(resp =>
            task.props.taskPipe
              ? from(
                  task.props.taskPipe.map(taskFn =>
                    taskFn(createProgressObserver(task.id, task.entityID))
                  )
                ).pipe(startWith(resp))
              : resp
          ),
          catchError(err => {
            return of(err);
          }),
          tap(observTask(task, taskObserver, dispatch)),
          finalize(() => {
            taskObserver.complete();
            task.cancel$.complete();
          })
        );
      dispatch({
        type: "add",
        task: [task, task$, taskObserver, priority]
      });
      clearTimeout(timeout);
      timeout = setTimeout(taskRunner, 16.6);
      return task.id || "";
    },
    [dispatch, hookListener$, pushMessage, middlewares]
  );

  // .pipe(filter(response => response.taskId === waitedTaskId))
  const _createApiTask = useCallback<CreateApiTaskFn>((runner, props) => {
    return createApiTask(props, runner);
  }, []);

  const service: TaskManagerContextType = useMemo(
    () => ({
      addTask,
      createApiTask: _createApiTask,
      getLastTaskID() {},
      getStatusByEntityID(id: string) {
        const taskID = state.tasksByEntity.get(id);
        return taskID && state.tasksStatuses.has(taskID)
          ? state.tasksStatuses.get(taskID)
          : undefined;
      },
      getActiveTasksByEntityId(entityID: string | string[]) {
        return (!Array.isArray(entityID) ? [entityID || ""] : entityID).reduce<
          TaskStatusType[]
        >((acc, id) => {
          const task = service.getStatusByEntityID(id);
          if (
            task !== undefined &&
            (task.status === "ADDED" ||
              task.status === "IN_PROGRESS" ||
              task.status === "WAITING_RESPONSE")
          )
            acc.push(task);
          return acc;
        }, []);
      },
      getStatusByTaskId(id) {
        return id && state.tasksStatuses.has(id)
          ? state.tasksStatuses.get(id)
          : undefined;
      },
      getStatusByRemoteTaskId(id) {
        return id && state.taskIdByRemoteTaskId[id]
          ? service.getStatusByTaskId(state.taskIdByRemoteTaskId[id])
          : undefined;
      }
    }),
    [_createApiTask, addTask, state]
  );
  return (
    <TaskManagerContext.Provider value={service}>
      {children}
    </TaskManagerContext.Provider>
  );
}
