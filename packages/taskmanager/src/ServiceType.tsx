import { ServiceFactory } from "./ServiceFactory";
export type ServiceType<
  T extends {
    [key: string]: ServiceFactory<any, any>;
  }
> = T;
