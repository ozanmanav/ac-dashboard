import { useContext, useEffect, useState } from "react";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { HookEvent } from "@appcircle/sse/lib/HookEvent";
import { HookContext } from "@appcircle/sse/lib/HookContext";
export function useHooksSubcriber(
  eventNames?: string[],
  remoteTaskId?: string
) {
  const hookListener$ = useContext(HookContext);
  const [response, setResponse] = useState<HookEvent | null>(null);
  useEffect(() => {
    let subscriber: Subscription;
    if (eventNames) {
      subscriber = hookListener$
        .pipe(
          filter(data => {
            return remoteTaskId ? data.json.taskId === remoteTaskId : true;
          }),
          filter(event =>
            eventNames.some(
              name =>
                event.json.eventName === name || event.json.eventAction === name
            )
          )
        )
        .subscribe({
          next(resp) {
            setResponse(resp.json);
          },
          error(resp) {
            setResponse(resp);
          }
        });
    }
    return () => subscriber && subscriber.unsubscribe();
  }, [hookListener$, remoteTaskId]);
  return response;
}
