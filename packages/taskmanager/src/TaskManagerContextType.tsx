import {
  TaskRunnerType,
  Priority,
  CreateApiTaskFn
} from "./TaskManagerContext";
import { TaskStatusType } from "./TaskStatusType";
export type TaskManagerContextType = {
  addTask: (task: TaskRunnerType, priority?: Priority) => void;
  createApiTask: CreateApiTaskFn;
  getStatusByEntityID: <TData = any, TState = any>(
    id: string
  ) => TaskStatusType<TData, TState> | undefined;
  getStatusByTaskId: <TData = any, TState = any>(
    id?: string
  ) => TaskStatusType<TData, TState> | undefined;
  getActiveTasksByEntityId: (id: string | string[]) => TaskStatusType[];
  getStatusByRemoteTaskId: (is: string) => TaskStatusType | undefined;
};
