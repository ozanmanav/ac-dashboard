import { PropsWithChildren, useReducer } from "react";
import React from "react";
import { useTaskResumeService } from "./useTaskResumeService";
import {
  middlewaresReducer,
  MiddlewaresInitialState,
  TaskManagerMiddlewareDispatchContext,
  TaskManagerMiddlewareStateContext
} from "./TaskManagerServiceProvider";
export function TaskManagerMiddlewareProvider(props: PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer(
    middlewaresReducer,
    MiddlewaresInitialState
  );
  // TODO: User this service as middleware
  useTaskResumeService();
  return (
    <TaskManagerMiddlewareDispatchContext.Provider value={dispatch}>
      <TaskManagerMiddlewareStateContext.Provider value={state}>
        {props.children}
      </TaskManagerMiddlewareStateContext.Provider>
    </TaskManagerMiddlewareDispatchContext.Provider>
  );
}
