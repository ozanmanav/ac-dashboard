import { TaskStatus } from "./TaskStatus";
import { AjaxResultError } from "@appcircle/shared/lib/services/AjaxResultError";
export type TaskStatusType<TResult = any, TState = any> = {
  taskId: string;
  status: TaskStatus;
  data?: TResult;
  error?: AjaxResultError;
  remoteTaskId?: string;
  state?: TState;
  entityID?: string | string[];
};
