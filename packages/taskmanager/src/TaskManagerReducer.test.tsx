import "jest-dom/extend-expect";
import "react-testing-library/cleanup-after-each";
import { TaskObservableRunnerType, TaskRunnerType } from "./TaskManagerContext";
import { TaskManagerReducer } from "./TaskManagerReducer";
import { empty, Subject } from "rxjs";
import shortid from "shortid";
import { TaskManagerStateType } from "./TaskManagerProvider";

type child = { id: string; name: string };
let state: TaskManagerStateType;

function createEmptyTask(): TaskObservableRunnerType<any, any> {
  return {
    id: shortid(),
    props: {},
    run: progress => {
      return empty();
    },
    cancel$: new Subject()
  };
}

function createObserver() {
  return { next: () => {}, error: () => {}, complete: () => {} };
}

describe("TaskManager Reducer", () => {
  let newstate: ReturnType<typeof TaskManagerReducer>;
  let task: TaskRunnerType<any>;
  beforeEach(() => {
    task = createEmptyTask();
    state = {
      tasks: new Map(),
      tasksByEntity: new Map(),
      tasksStatuses: new Map()
    };
    newstate = TaskManagerReducer(state, {
      type: "add",
      entityID: "1234",
      task: [task, empty(), createObserver(), 1]
    });
  });
  it("adds a new task", () => {
    expect(newstate.tasksByEntity.get("1234")).toBe(task.id);
  });
  it("updates given task's status", () => {
    const _newstate = TaskManagerReducer(newstate, {
      type: "updateStatus",
      status: "ADDED",
      taskId: task.id
    });
    // const taskID = state.tasksByEntity.get(id);
    // return taskID && state.tasksStatuses.has(taskID)
    //   ? state.tasksStatuses.get(taskID)
    //   : undefined;
    expect(_newstate.tasksStatuses.get(task.id)).toEqual({
      status: "ADDED",
      data: undefined
    });
    const taskID: string | undefined = _newstate.tasksByEntity.get("1234");

    expect(taskID).toEqual(task.id);
  });
});
