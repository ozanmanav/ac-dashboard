var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgDevelopment(props) {
    return (React.createElement("svg", __assign({ width: 24, height: 24 }, props),
        React.createElement("path", { d: "M23.37 5.913s-.773-3.693-4.107-4.99C17.695.325 14.833.024 12.022 0 9.21-.001 6.349.3 4.78.898 1.42 2.195.65 5.913.65 5.913.152 8.008-.022 10.129.002 12c.001 1.871.15 4.017.648 6.087 0 0 .771 3.693 4.105 4.99 1.568.598 4.43.899 7.242.923 2.812-.025 5.674-.3 7.242-.923 3.334-1.297 4.106-4.99 4.106-4.99.497-2.095.671-4.216.647-6.087a23.817 23.817 0 00-.623-6.087zm-6.921 1.639l3.6 3.6a1.2 1.2 0 010 1.697l-3.6 3.6a1.2 1.2 0 01-1.698-1.698L17.5 12l-2.75-2.751a1.2 1.2 0 111.698-1.698zm-7.2 0a1.2 1.2 0 010 1.698L6.5 12l2.75 2.75a1.2 1.2 0 11-1.698 1.698l-3.6-3.599a1.2 1.2 0 010-1.698l3.6-3.6a1.2 1.2 0 011.698 0z" })));
}
//# sourceMappingURL=Development.js.map