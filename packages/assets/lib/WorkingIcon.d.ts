import { SVGProps } from "react";
export declare function SvgWorkingIcon(props: SVGProps<SVGSVGElement>): JSX.Element;
