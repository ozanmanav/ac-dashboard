var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgKey(props) {
    return (React.createElement("svg", __assign({ width: 94, height: 87 }, props),
        React.createElement("path", { d: "M40.096 54.264v-.001a36.016 36.016 0 0011.485 7.63c.794.338 1.603.64 2.419.916l-6.94 6.842v-.007h-8.792v8.68h-8.794V87H0V72.172L31.605 41c.23.654.484 1.3.757 1.936a35.44 35.44 0 007.734 11.328zM65.498 0C81.236 0 93.996 12.759 94 28.504 94 44.24 81.241 57 65.498 57 49.756 57 37 44.241 37 28.504 37 12.76 49.755 0 65.498 0zm15.047 23.431c2.783-2.781 2.783-7.298 0-10.076a7.124 7.124 0 00-10.079 0 7.131 7.131 0 000 10.076 7.124 7.124 0 0010.08 0z", fill: "#CDD6E1", fillRule: "evenodd" })));
}
//# sourceMappingURL=Key.js.map