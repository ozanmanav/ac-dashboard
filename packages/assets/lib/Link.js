var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgLink(props) {
    return (React.createElement("svg", __assign({ width: 24, height: 31 }, props),
        React.createElement("path", { d: "M7.683 7.154h3.357V.48c0-.266.217-.48.484-.48h.952a.48.48 0 01.484.48v6.674h3.357c2.127 0 3.843 1.706 3.843 3.816v3.896l.902-.896a.484.484 0 01.682.004l.672.668c.192.19.19.492.001.68l-2.538 2.522a.964.964 0 01-1.358 0l-2.538-2.522a.48.48 0 010-.68l.673-.668a.482.482 0 01.682-.004l.902.896V10.97a1.912 1.912 0 00-1.923-1.908H7.683a1.914 1.914 0 00-1.923 1.907v3.897l.902-.896a.484.484 0 01.682.004l.672.668c.192.19.19.492.001.68L5.48 17.844a.964.964 0 01-1.358 0l-2.538-2.522a.48.48 0 010-.68l.673-.668a.482.482 0 01.682-.004l.902.896V10.97c0-2.109 1.72-3.815 3.843-3.815zM23.04 31h-6.72a.957.957 0 01-.96-.954v-8.584c0-.527.43-.954.96-.954h6.72c.53 0 .96.427.96.954v8.584c0 .527-.43.954-.96.954zM7.68 31H.96a.957.957 0 01-.96-.954v-8.584c0-.527.43-.954.96-.954h6.72c.53 0 .96.427.96.954v8.584c0 .527-.43.954-.96.954z" })));
}
//# sourceMappingURL=Link.js.map