import { SVGProps } from "react";
export declare function SvgLink(props: SVGProps<SVGSVGElement>): JSX.Element;
