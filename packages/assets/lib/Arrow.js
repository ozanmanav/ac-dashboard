var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgArrow(props) {
    return (React.createElement("svg", __assign({ width: 6, height: 10 }, props),
        React.createElement("path", { fill: "#5B799E", fillRule: "evenodd", d: "M1.046 10c-.272 0-.532-.105-.741-.305a1.068 1.068 0 010-1.494L3.465 5 .306 1.802a1.068 1.068 0 010-1.494 1.045 1.045 0 011.481 0l3.912 3.946c.198.2.302.463.302.748 0 .284-.115.547-.302.747L1.786 9.695c-.209.2-.48.305-.74.305z" })));
}
//# sourceMappingURL=Arrow.js.map