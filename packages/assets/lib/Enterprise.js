var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgEnterprise(props) {
    return (React.createElement("svg", __assign({ width: 24, height: 23 }, props),
        React.createElement("path", { d: "M12.83 16.495l-1.657.015-.016-2.235h1.673v2.22zM9.173 14.21a57.76 57.76 0 01-7.455-.82.406.406 0 00-.468.401v6.172c0 1.673 1.34 3.03 3 3.038h15.5c1.659-.007 3-1.365 3-3.038V13.79a.407.407 0 00-.475-.4 57.971 57.971 0 01-7.455.817v2.301c0 .531-.209 1.04-.582 1.415a1.973 1.973 0 01-1.403.587h-1.678a1.993 1.993 0 01-1.984-2.002V14.21zM0 10.335V7.382A2.412 2.412 0 012.402 4.96h4.335V3.1c0-1.71 1.377-3.1 3.076-3.1h4.377c1.699 0 3.075 1.389 3.075 3.101V4.96h4.333A2.412 2.412 0 0124 7.382v2.953a.81.81 0 01-.64.796A56.297 56.297 0 0112 12.258c-3.814.01-7.62-.367-11.36-1.127a.81.81 0 01-.64-.796zM9.3 4.96h5.4V3.104a.506.506 0 00-.5-.504H9.81c-.276 0-.5.226-.5.504L9.3 4.96z" })));
}
//# sourceMappingURL=Enterprise.js.map