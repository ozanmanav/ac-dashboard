var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgDownload(props) {
    return (React.createElement("svg", __assign({ width: 20, height: 17 }, props, { viewBox: "0 0 20 17" }),
        React.createElement("path", { fill: "#617EA2", d: "M15 8l-5 5-5-5 1.4-1.4L9 9.2V0h2v9.2l2.6-2.6L15 8zm3 1h2v8H0V9h2v6h16V9z" })));
}
//# sourceMappingURL=Download.js.map