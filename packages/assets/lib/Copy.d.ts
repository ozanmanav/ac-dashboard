import { SVGProps } from "react";
export declare function SvgCopy(props: SVGProps<SVGSVGElement>): JSX.Element;
