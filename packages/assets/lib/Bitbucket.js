var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgBitbucket(props) {
    return (React.createElement("svg", __assign({ width: 28, height: 28, viewBox: "0 0 62.42 62.42" }, props),
        React.createElement("defs", null,
            React.createElement("linearGradient", { id: "bitbucket_svg__a", x1: 64.01, y1: 30.27, x2: 32.99, y2: 54.48, gradientUnits: "userSpaceOnUse" },
                React.createElement("stop", { offset: 0.18, stopColor: "#0052cc" }),
                React.createElement("stop", { offset: 1, stopColor: "#2684ff" }))),
        React.createElement("g", { "data-name": "Layer 2" },
            React.createElement("path", { d: "M2 3.13a2 2 0 00-2 2.32l8.49 51.54a2.72 2.72 0 002.66 2.27h40.73a2 2 0 002-1.68l8.49-52.12a2 2 0 00-2-2.32zm35.75 37.25h-13l-3.52-18.39H40.9z", fill: "#2684ff" }),
            React.createElement("path", { d: "M59.67 25.12H40.9l-3.15 18.39h-13L9.4 61.73a2.71 2.71 0 001.75.66h40.74a2 2 0 002-1.68z", fill: "url(#bitbucket_svg__a)", transform: "translate(0 -3.13)" }))));
}
//# sourceMappingURL=Bitbucket.js.map