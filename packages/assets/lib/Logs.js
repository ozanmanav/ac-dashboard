var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgLogs(props) {
    return (React.createElement("svg", __assign({ width: 16, height: 20 }, props),
        React.createElement("path", { d: "M6.6 0H16v20H0V6.6L6.6 0zM6 3.4L3.4 6H6V3.4zM14 18V2H8v6H2v10h12zM4 14h8v2H4v-2zm0-4h8v2H4v-2z", fill: "#FFF", fillRule: "nonzero" })));
}
//# sourceMappingURL=Logs.js.map