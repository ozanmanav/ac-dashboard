var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgSubmitPublicStore(props) {
    return (React.createElement("svg", __assign({ width: 20, height: 20 }, props),
        React.createElement("path", { fill: "#5B799E", fillRule: "evenodd", d: "M19.904 5.446l-2.5-4.985A.834.834 0 0016.666 0H3.333a.833.833 0 00-.745.46l-2.5 4.986A.823.823 0 000 5.821v2.512a2.5 2.5 0 001.667 2.347v6.82a2.5 2.5 0 002.5 2.5h11.666a2.5 2.5 0 002.5-2.5v-6.82A2.5 2.5 0 0020 8.333V5.85a.818.818 0 00-.096-.404zM17.819 5h-3.78l-.556-3.333h2.669L17.819 5zm-6.026-3.333L12.35 5h-4.7l.556-3.333h3.587zm-7.946 0h2.67L5.96 5H2.18l1.667-3.333zm4.486 16.666V15c0-.46.373-.833.833-.833h1.667a.833.833 0 01.833.833v3.333H8.333zm7.5 0h-2.5V15a2.5 2.5 0 00-2.5-2.5H9.166a2.5 2.5 0 00-2.5 2.5v3.333h-2.5a.833.833 0 01-.833-.833v-6.667h13.333V17.5a.833.833 0 01-.833.833zm2.5-10a.833.833 0 01-.833.834h-15a.833.833 0 01-.834-.834V6.667h16.667v1.666z" })));
}
//# sourceMappingURL=SubmitPublicStore.js.map