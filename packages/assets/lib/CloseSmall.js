var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgCloseSmall(props) {
    return (React.createElement("svg", __assign({ width: 14, height: 14 }, props),
        React.createElement("path", { fill: "#A5B5C9", fillRule: "evenodd", d: "M.313 12.203L5.517 7 .313 1.797A1.05 1.05 0 01.308.307a1.05 1.05 0 011.49.007l5.201 5.204L12.203.314A1.047 1.047 0 0114 1.053c0 .28-.113.548-.313.744L8.482 7l5.203 5.203a1.05 1.05 0 01.005 1.49 1.05 1.05 0 01-1.49-.007L7 8.482l-5.204 5.204A1.05 1.05 0 01.31 12.203h.002z" })));
}
//# sourceMappingURL=CloseSmall.js.map