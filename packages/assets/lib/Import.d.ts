import { SVGProps } from "react";
export declare function SvgImport(props: SVGProps<SVGSVGElement>): JSX.Element;
