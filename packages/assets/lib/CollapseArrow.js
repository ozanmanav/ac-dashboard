var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgCollapseArrow(props) {
    return (React.createElement("svg", __assign({ width: 10, height: 6 }, props),
        React.createElement("path", { d: "M5.09 5.996a.915.915 0 00.532-.254L9.642 1.9c.202-.18.33-.446.354-.733A1.155 1.155 0 009.77.373a.927.927 0 00-.668-.371.9.9 0 00-.705.278L5 3.528 1.603.28A.9.9 0 00.898.002.927.927 0 00.23.373c-.168.22-.25.506-.226.794.024.287.152.552.355.733l4.019 3.842c.197.189.454.28.712.254z", fill: "#98A1AB", fillRule: "evenodd" })));
}
//# sourceMappingURL=CollapseArrow.js.map