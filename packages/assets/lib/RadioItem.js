var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgRadioItem(props) {
    return (React.createElement("svg", __assign({ width: 19, height: 19 }, props),
        React.createElement("g", { fill: "none", fillRule: "evenodd" },
            React.createElement("circle", { cx: 9.5, cy: 9.5, r: 9 }),
            React.createElement("circle", { cx: 9.5, cy: 9.5, r: 4.5 }))));
}
//# sourceMappingURL=RadioItem.js.map