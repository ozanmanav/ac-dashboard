import { SVGProps } from "react";
export declare function SvgWorkflowIcon(props: SVGProps<SVGSVGElement>): JSX.Element;
