import { SVGProps } from "react";
export declare function SvgProfile(props: SVGProps<SVGSVGElement>): JSX.Element;
