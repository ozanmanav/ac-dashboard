var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgToastClose(props) {
    return (React.createElement("svg", __assign({ width: 8, height: 8 }, props),
        React.createElement("path", { fillOpacity: 0.3, fillRule: "evenodd", d: "M6.067 7.533L3.995 5.46 1.92 7.535a1.036 1.036 0 01-1.46 0 1.036 1.036 0 010-1.46L2.535 4 .462 1.929A1.036 1.036 0 01.463.468 1.036 1.036 0 011.924.467l2.073 2.072L6.07.466A1.035 1.035 0 017.531.464a1.036 1.036 0 010 1.462L5.455 4l2.072 2.072a1.034 1.034 0 010 1.46 1.036 1.036 0 01-1.461.001z" })));
}
//# sourceMappingURL=ToastClose.js.map