import { SVGProps } from "react";
export declare function SvgCloseTag(props: SVGProps<SVGSVGElement>): JSX.Element;
