var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgHelp(props) {
    return (React.createElement("svg", __assign({ width: 22, height: 22 }, props),
        React.createElement("path", { d: "M11.001 0C4.928 0 0 4.92 0 10.994 0 17.074 4.927 22 11 22c6.077 0 11-4.927 11-11.006C22 4.921 17.077 0 11.001 0zm.784 17.115a1.294 1.294 0 01-.887.342c-.347 0-.65-.112-.908-.336-.259-.225-.389-.54-.389-.943 0-.359.126-.66.376-.904s.557-.366.921-.366c.359 0 .66.122.904.366s.367.545.367.904c0 .397-.129.71-.384.937zm3.186-7.897c-.196.364-.43.678-.7.943-.27.264-.754.709-1.454 1.334a5.88 5.88 0 00-.465.465 1.924 1.924 0 00-.26.367 1.73 1.73 0 00-.133.333c-.03.11-.078.306-.14.584-.108.591-.447.887-1.015.887a1.04 1.04 0 01-.747-.29c-.202-.193-.303-.48-.303-.86 0-.479.074-.892.222-1.242.147-.35.344-.656.588-.921.245-.265.574-.578.99-.943.364-.319.627-.558.789-.72.162-.162.298-.343.409-.541a1.3 1.3 0 00.166-.648c0-.456-.169-.84-.507-1.152-.339-.313-.776-.47-1.31-.47-.626 0-1.086.159-1.382.474-.296.316-.546.78-.75 1.394-.194.643-.56.965-1.1.965-.32 0-.587-.113-.806-.337-.219-.225-.329-.468-.329-.73 0-.54.174-1.087.52-1.642.347-.555.854-1.014 1.519-1.378.665-.364 1.441-.546 2.328-.546.824 0 1.552.152 2.183.456.63.304 1.119.718 1.462 1.24.344.524.516 1.092.516 1.707.003.483-.095.906-.291 1.27z" })));
}
//# sourceMappingURL=Help.js.map