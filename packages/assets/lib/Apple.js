var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgApple(props) {
    return (React.createElement("svg", __assign({ width: 14, height: 16 }, props),
        React.createElement("path", { d: "M11.36 8.502c.022 2.42 2.215 3.225 2.24 3.236-.02.055-.351 1.148-1.157 2.276-.696.975-1.417 1.945-2.555 1.964-1.12.02-1.478-.635-2.758-.635-1.279 0-1.678.616-2.736.656-1.098.04-1.935-1.053-2.636-2.024C.322 11.988-.774 8.361.699 5.913a4.115 4.115 0 013.456-2.007c1.078-.018 2.097.697 2.755.697.66 0 1.898-.86 3.198-.734.544.022 2.073.21 3.054 1.585-.078.05-1.824 1.021-1.803 3.048zM9.255 2.555c-.584.675-1.54 1.202-2.477 1.132-.127-.92.346-1.875.886-2.476C8.268.535 9.285.033 10.125 0c.108.937-.287 1.877-.869 2.555z" })));
}
//# sourceMappingURL=Apple.js.map