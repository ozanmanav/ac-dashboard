import { SVGProps } from "react";
export declare function SvgAppIcon(props: SVGProps<SVGSVGElement>): JSX.Element;
