var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgToggleClose(props) {
    return (React.createElement("svg", __assign({ width: 14, height: 9 }, props),
        React.createElement("path", { fill: "#1A3352", fillRule: "evenodd", d: "M5.517 1.482L.313 6.686a1.05 1.05 0 00-.005 1.49 1.05 1.05 0 001.49-.008l5.201-5.203 5.204 5.203A1.047 1.047 0 0014 7.43c0-.279-.113-.547-.313-.743L8.482 1.482 6.999 0 5.517 1.482z" })));
}
//# sourceMappingURL=ToggleClose.js.map