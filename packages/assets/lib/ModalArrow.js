var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgModalArrow(props) {
    return (React.createElement("svg", __assign({ width: 9, height: 14 }, props),
        React.createElement("path", { d: "M.707 6.293l.775-.775L6.686.314a1.047 1.047 0 011.797.739c0 .28-.113.548-.313.744L2.965 7l5.203 5.203a1.05 1.05 0 01.006 1.49 1.05 1.05 0 01-1.49-.007L1.482 8.482l-.775-.775a1 1 0 010-1.414z", fill: "#1A3352", fillRule: "evenodd" })));
}
//# sourceMappingURL=ModalArrow.js.map