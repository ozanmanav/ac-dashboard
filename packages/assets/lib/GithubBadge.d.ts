import { SVGProps } from "react";
export declare function SvgGithubBadge(props: SVGProps<SVGSVGElement>): JSX.Element;
