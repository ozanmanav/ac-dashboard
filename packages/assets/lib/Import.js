var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgImport(props) {
    return (React.createElement("svg", __assign({ width: 18, height: 16 }, props),
        React.createElement("path", { d: "M2.636.341c-3.515 3.583-3.515 9.39 0 12.972a8.88 8.88 0 0012.728 0c3.515-3.582 3.515-9.389 0-12.972a1.132 1.132 0 00-1.62 0 1.184 1.184 0 000 1.652c2.62 2.67 2.62 7 0 9.669a6.62 6.62 0 01-9.488 0c-2.62-2.67-2.62-7 0-9.669a1.184 1.184 0 000-1.652 1.132 1.132 0 00-1.62 0zM8.33 9.947a.94.94 0 001.372-.004l3.247-3.41c.923-.943-.461-2.358-1.384-1.414L10.442 6.27c-.256.264-.463.174-.463-.194V1.49c0-.553-.44-1-.98-1s-.978.451-.978 1v4.587c0 .37-.208.458-.463.194L6.434 5.12c-.923-.944-2.308.471-1.385 1.415l3.282 3.413z", fillRule: "evenodd" })));
}
//# sourceMappingURL=Import.js.map