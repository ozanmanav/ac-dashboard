import { SVGProps } from "react";
export declare function SvgDuplicate(props: SVGProps<SVGSVGElement>): JSX.Element;
