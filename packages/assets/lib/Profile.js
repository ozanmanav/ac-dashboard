var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgProfile(props) {
    return (React.createElement("svg", __assign({ width: 94, height: 94 }, props),
        React.createElement("path", { fillRule: "evenodd", d: "M.214 93.998C-.261 88.674 0 83.501 1.552 78.522c.968-3.107 3.44-5.117 6.466-6.07 4.308-1.357 8.726-2.355 13.052-3.657 2.567-.775 5.17-1.616 7.568-2.813 5.441-2.715 6.34-6.638 3.808-12.243-2.807-6.213-5.396-12.565-7.517-19.044-1.941-5.922-2.016-12.1-.421-18.241 2.3-8.882 8.856-14.85 17.918-16.1 6.568-.904 12.838-.231 18.416 3.83 8.316 6.058 9.872 14.768 8.904 24.197-.9 8.76-4.053 16.89-8.121 24.64-3.483 6.632-2.173 10.848 4.746 13.585 4.512 1.78 9.3 2.836 13.962 4.222 1.914.571 3.899.992 5.714 1.796 3.527 1.56 6.128 4.05 6.565 8.182.456 4.33.823 8.668 1.246 13.195H.214v-.002z" })));
}
//# sourceMappingURL=Profile.js.map