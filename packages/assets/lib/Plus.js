var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgPlus(props) {
    return (React.createElement("svg", __assign({ width: 12, height: 12 }, props),
        React.createElement("path", { d: "M5.077 0v5.077H0v1.846h5.077V12h1.846V6.923H12V5.077H6.923V0z", fillRule: "evenodd" })));
}
//# sourceMappingURL=Plus.js.map