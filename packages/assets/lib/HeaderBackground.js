var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgHeaderBackground(props) {
    return (React.createElement("svg", __assign({ width: 500, height: 65 }, props),
        React.createElement("path", { d: "M480.116-184.31l14.414 14.413L44.103 280.53l-14.414-14.414L480.116-184.31zm28.778 28.777l14.414 14.414L72.88 309.308l-14.414-14.414 450.427-450.427zm-59.677-55.434l14.414 14.414L4.011 263.066l-14.413-14.414 459.62-459.62zm88.554 84.311l14.414 14.414-450.427 450.427-14.414-14.414 450.427-450.427zm29.345 29.345l14.414 14.414L131.103 367.53l-14.414-14.414L567.116-97.31zm28.778 28.778l14.414 14.414L159.88 396.308l-14.414-14.414L595.894-68.533zm28.877 28.877l14.414 14.414-450.427 450.427-14.414-14.414L624.771-39.656zM366.205-298.222l14.413 14.413-450.427 450.427-14.413-14.413 450.427-450.427zm28.777 28.777l14.414 14.414L-41.03 195.396l-14.414-14.414 450.427-450.427zM337.427-327l14.414 14.414L-98.586 137.84-113 123.427 337.427-327zm86.433 86.433l14.414 14.414-450.427 450.427-14.414-14.414L423.86-240.567z", fill: "#DA4444", fillRule: "nonzero", fillOpacity: 0.05 })));
}
//# sourceMappingURL=HeaderBackground.js.map