import { SVGProps } from "react";
export declare function SvgCollapseArrow(props: SVGProps<SVGSVGElement>): JSX.Element;
