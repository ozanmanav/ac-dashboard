var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgExternallink(props) {
    return (React.createElement("svg", __assign({ height: 1024, width: 768 }, props),
        React.createElement("path", { fill: "#5b799e", d: "M640 768H128V257.906L256 256V128H0v768h768V576H640v192zM384 128l128 128-192 192 128 128 192-192 128 128V128H384z" })));
}
//# sourceMappingURL=Externallink.js.map