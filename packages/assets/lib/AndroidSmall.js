var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgAndroidSmall(props) {
    return (React.createElement("svg", __assign({ width: 18, height: 16 }, props),
        React.createElement("path", { d: "M16.925 1.262a.736.736 0 000-1.046.757.757 0 00-1.059 0l-2.103 2.08.018.02C10.714-.16 5.893.215 3.27 2.19L1.276.216a.757.757 0 00-1.059 0 .736.736 0 000 1.046l2.04 2.017.019-.019C1.257 4.567.655 6.157.655 7.868V16h15.608V7.867c0-1.649-.558-3.187-1.51-4.465l2.172-2.14zM5.147 7.684a1.844 1.844 0 01-1.854-1.832c0-1.012.83-1.833 1.854-1.833 1.023 0 1.854.82 1.854 1.833a1.841 1.841 0 01-1.854 1.832zm6.623 0a1.844 1.844 0 01-1.853-1.832c0-1.012.83-1.833 1.853-1.833 1.024 0 1.854.82 1.854 1.833 0 1.012-.83 1.832-1.854 1.832z", fillRule: "evenodd" })));
}
//# sourceMappingURL=AndroidSmall.js.map