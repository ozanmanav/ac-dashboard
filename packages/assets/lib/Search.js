var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgSearch(props) {
    return (React.createElement("svg", __assign({ width: 16, height: 16 }, props),
        React.createElement("path", { fillRule: "evenodd", d: "M15.684 14.36l-2.867-2.906a6.923 6.923 0 001.733-4.578C14.55 3.085 11.505 0 7.763 0S.975 3.085.975 6.876c0 3.791 3.046 6.876 6.787 6.876 1.329 0 2.56-.394 3.596-1.05l2.98 3.02c.18.18.422.278.665.278a.942.942 0 00.665-.279.954.954 0 00.016-1.361zM2.87 6.876c0-2.725 2.187-4.956 4.892-4.956 2.706 0 4.876 2.231 4.876 4.956 0 2.724-2.186 4.956-4.875 4.956-2.69 0-4.893-2.216-4.893-4.956z" })));
}
//# sourceMappingURL=Search.js.map