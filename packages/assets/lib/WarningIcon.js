import { SvgWarning } from "./Warning";
import React from "react";
export function WarningIcon() {
    return React.createElement(SvgWarning, { viewBox: "0 0 35 35" });
}
//# sourceMappingURL=WarningIcon.js.map