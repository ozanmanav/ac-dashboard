import { SVGProps } from "react";
export declare function SvgSettings(props: SVGProps<SVGSVGElement>): JSX.Element;
