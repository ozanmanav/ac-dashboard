var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgCode(props) {
    return (React.createElement("svg", __assign({ width: 17, height: 11, viewBox: "0 0 17 11" }, props),
        React.createElement("path", { fill: "#CDD6E1", d: "M6.847 1.233c0 .336-.133.65-.376.887L3.09 5.437l3.383 3.316a1.233 1.233 0 010 1.774 1.287 1.287 0 01-.904.367c-.342 0-.663-.13-.904-.366L.374 6.323A1.25 1.25 0 010 5.437c0-.331.137-.654.375-.887L4.663.345c.467-.46 1.339-.459 1.808 0 .242.238.376.553.376.888zm9.294 3.317c.238.233.374.555.374.886 0 .33-.137.654-.375.887l-4.288 4.204a1.288 1.288 0 01-.904.367c-.34 0-.662-.13-.904-.366a1.238 1.238 0 01-.001-1.774l3.384-3.318-3.384-3.317a1.234 1.234 0 010-1.773c.472-.462 1.34-.46 1.81-.001L16.14 4.55z" })));
}
//# sourceMappingURL=Code.js.map