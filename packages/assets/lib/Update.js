var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgUpdate(props) {
    return (React.createElement("svg", __assign({ width: 24, height: 29 }, props),
        React.createElement("path", { d: "M7.152 26.115H2.754A2.741 2.741 0 010 23.365V2.75A2.741 2.741 0 012.754 0h11.324a2.741 2.741 0 012.754 2.75v6.378a10.572 10.572 0 00-2.143-.064V5.502H1.836v14.194h3.058a10.516 10.516 0 002.258 6.42zm8.3-15.066c4.694 0 8.51 3.815 8.548 8.547 0 4.732-3.854 8.548-8.548 8.548-4.732 0-8.547-3.854-8.547-8.548a8.533 8.533 0 018.547-8.547zm-.127 2.8L9.97 19.203l1.709 1.708 2.449-2.449v5.923c.341.228.74.341 1.196.341.398 0 .854-.113 1.196-.341V18.46l2.449 2.449 1.708-1.708-5.353-5.353z" })));
}
//# sourceMappingURL=Update.js.map