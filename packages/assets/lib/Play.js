var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgPlay(props) {
    return (React.createElement("svg", __assign({ width: 15, height: 16 }, props),
        React.createElement("path", { fill: "#5B799E", fillRule: "evenodd", d: "M13.596 9.03L2.33 15.327a1 1 0 01-1.488-.872V1.862A1 1 0 012.33.99l11.266 6.296a1 1 0 010 1.746z" })));
}
//# sourceMappingURL=Play.js.map