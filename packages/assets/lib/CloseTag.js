var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgCloseTag(props) {
    return (React.createElement("svg", __assign({ width: 9, height: 9 }, props),
        React.createElement("path", { fill: "#CDD6E1", fillRule: "evenodd", d: "M6.781 8.39L4.5 6.107 2.218 8.39a1.14 1.14 0 01-1.607 0 1.14 1.14 0 010-1.609L2.892 4.5.611 2.219A1.14 1.14 0 01.61.61a1.14 1.14 0 011.608 0L4.5 2.893 6.78.612A1.139 1.139 0 018.39.61a1.14 1.14 0 010 1.608L6.107 4.5 8.39 6.782a1.138 1.138 0 010 1.607 1.14 1.14 0 01-1.608 0z" })));
}
//# sourceMappingURL=CloseTag.js.map