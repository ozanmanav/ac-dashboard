var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgSubmitEnterpriseStore(props) {
    return (React.createElement("svg", __assign({ width: 19, height: 19 }, props),
        React.createElement("path", { fill: "#5B799E", fillRule: "evenodd", d: "M.95 13.301v4.753a.95.95 0 00.948.946h15.204a.947.947 0 00.949-.946V13.3a.95.95 0 00.949-.946v-7.61a.95.95 0 00-.95-.946h-3.8V.954A.951.951 0 0013.303 0H5.697a.951.951 0 00-.947.954V3.8H.95a.947.947 0 00-.95.946v7.61a.95.95 0 00.95.946zm15.2 3.8H2.856v-3.8h3.796v1.476c0 .234.19.425.424.425h4.852c.235 0 .425-.19.425-.425V13.3h3.799v3.8zm-7.599-3.8v-1.9h1.9v1.9h-1.9zm-1.9-11.404h5.698v1.9L6.651 3.8V1.897zM1.898 5.7h15.204v5.7H12.35V9.925a.425.425 0 00-.425-.426H7.074a.425.425 0 00-.426.426v1.476h-4.75V5.699z" })));
}
//# sourceMappingURL=SubmitEnterpriseStore.js.map