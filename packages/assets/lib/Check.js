var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgCheck(props) {
    return (React.createElement("svg", __assign({ width: 9, height: 7 }, props),
        React.createElement("path", { d: "M3.482 4.093L1.446 2.047 0 3.5 3.482 7 9 1.453 7.554 0z", fillRule: "evenodd" })));
}
//# sourceMappingURL=Check.js.map