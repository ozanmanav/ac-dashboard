var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgUpload(props) {
    return (React.createElement("svg", __assign({ width: 20, height: 17 }, props, { viewBox: "0 0 20 17" }),
        React.createElement("path", { fill: "#617EA2", d: "M15 5l-1.4 1.4L11 3.8V13H9V3.8L6.4 6.4 5 5l5-5 5 5zm3 4h2v8H0V9h2v6h16V9z" })));
}
//# sourceMappingURL=Upload.js.map