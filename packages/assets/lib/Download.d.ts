import { SVGProps } from "react";
export declare function SvgDownload(props: SVGProps<SVGSVGElement>): JSX.Element;
