var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgHammer(props) {
    return (React.createElement("svg", __assign({ width: 18, height: 17 }, props, { viewBox: "0 0 18 17" }),
        React.createElement("path", { fill: "#5B799E", d: "M4.192 15.984a2.385 2.385 0 01-1.81.827A2.373 2.373 0 010 14.447c0-.719.323-1.362.833-1.796L8.698 5.96l2.238 2.22-6.744 7.805zm12.304-9.443c.365-.08.744.059 1.009.321l.495.493-3.177 3.152-.495-.491c-.265-.263-.404-.64-.323-1.002.054-.246.024-.54-.119-.859a1.821 1.821 0 00-.925-.92c-.55-.241-1.03-.16-1.312.119l-.023.025-2.122-2.105.028-.023a2.032 2.032 0 00-.003-2.89 2.07 2.07 0 00-2.912.003L5.56 1.306a4.517 4.517 0 016.353 0l2.35 2.334c.265.263.405.64.323 1.002-.055.247-.025.542.118.862.182.407.516.74.927.92.321.14.618.172.866.117z" })));
}
//# sourceMappingURL=Hammer.js.map