var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgBitbucketBadge(props) {
    return (React.createElement("svg", __assign({ width: 30, height: 20 }, props),
        React.createElement("g", { fill: "none", fillRule: "evenodd" },
            React.createElement("rect", { fill: "#3D9DEE", width: 30, height: 20, rx: 4 }),
            React.createElement("text", { fontFamily: "HelveticaNeue-Bold, Helvetica Neue", fontSize: 11, fontWeight: "bold", letterSpacing: 0.183, fill: "#FFF" },
                React.createElement("tspan", { x: 4.034, y: 14 }, "BitB")))));
}
//# sourceMappingURL=BitbucketBadge.js.map