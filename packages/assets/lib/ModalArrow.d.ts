import { SVGProps } from "react";
export declare function SvgModalArrow(props: SVGProps<SVGSVGElement>): JSX.Element;
