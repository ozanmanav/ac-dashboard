var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgEmailIcon(props) {
    return (React.createElement("svg", __assign({ width: 17, height: 17 }, props),
        React.createElement("path", { d: "M12.65 9.416c-.04.262.015.526.162.75.156.237.394.4.671.458a1.071 1.071 0 001.258-.823A6.382 6.382 0 008.5 2.124 6.382 6.382 0 002.124 8.5a6.382 6.382 0 008.568 5.989 1.062 1.062 0 11.727 1.996 8.482 8.482 0 01-2.92.515V17C3.813 17 0 13.187 0 8.5 0 3.813 3.813 0 8.5 0 13.187 0 17 3.813 17 8.5c0 .577-.058 1.154-.173 1.713-.366 1.742-2.06 2.852-3.778 2.491a3.16 3.16 0 01-1.743-1.014A4.233 4.233 0 018.5 12.75 4.254 4.254 0 014.25 8.5 4.254 4.254 0 018.5 4.25a4.254 4.254 0 014.15 5.166zm-2.066-.5A2.127 2.127 0 008.5 6.376a2.127 2.127 0 00-2.125 2.124c0 1.172.953 2.125 2.125 2.125a2.128 2.128 0 002.084-1.71z", fillRule: "evenodd" })));
}
//# sourceMappingURL=EmailIcon.js.map