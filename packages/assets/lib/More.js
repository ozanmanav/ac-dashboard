var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
export function SvgMore(props) {
    return (React.createElement("svg", __assign({ width: 16, height: 4 }, props),
        React.createElement("path", { fillRule: "evenodd", d: "M10.057 2c0 1.105-.921 2-2.057 2s-2.057-.895-2.057-2S6.863 0 8 0c1.136 0 2.057.895 2.057 2zM16 2c0 1.105-.92 2-2.057 2-1.136 0-2.057-.895-2.057-2s.921-2 2.057-2S16 .895 16 2zM4.114 2c0 1.105-.921 2-2.057 2S0 3.105 0 2s.92-2 2.057-2c1.136 0 2.057.895 2.057 2z" })));
}
//# sourceMappingURL=More.js.map