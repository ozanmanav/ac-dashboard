module.exports = function template(
  { template },
  opts,
  { imports, componentName, props, jsx, exports }
) {
  const typeScriptTpl = template.smart({ plugins: ["typescript"] });
  return typeScriptTpl.ast`
    import React, { SVGProps } from 'react';
    export function ${componentName}(props: SVGProps<SVGSVGElement>) { 
      return ${jsx};
    }
  `;
};
