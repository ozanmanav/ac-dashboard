import React, { SVGProps } from "react";
export function SvgLock(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={16} height={19} {...props}>
      <path
        d="M13 7h-2V5h2v2c1.7 0 3 1.3 3 3v6c0 1.7-1.3 3-3 3H3c-1.7 0-3-1.3-3-3v-6c0-1.7 1.3-3 3-3V5c0-2.8 2.2-5 5-5s5 2.2 5 5v2zM5 5v2h6V5c0-1.7-1.3-3-3-3S5 3.3 5 5zm9 11v-6c0-.6-.4-1-1-1H3c-.6 0-1 .4-1 1v6c0 .6.4 1 1 1h10c.6 0 1-.4 1-1zm-6-5c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1s-1-.4-1-1v-2c0-.6.4-1 1-1z"
        fill="#AFBED0"
        fillRule="nonzero"
      />
    </svg>
  );
}
