import React, { SVGProps } from "react";
export function SvgGithubBadge(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={30} height={20} {...props}>
      <g fill="none" fillRule="evenodd">
        <rect fill="#6FBA6C" width={30} height={20} rx={4} />
        <text
          fontFamily="HelveticaNeue-Bold, Helvetica Neue"
          fontSize={11}
          fontWeight="bold"
          letterSpacing={0.183}
          fill="#FFF"
        >
          <tspan x={7} y={14}>
            {"Git"}
          </tspan>
        </text>
      </g>
    </svg>
  );
}
