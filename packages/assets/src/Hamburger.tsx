import React, { SVGProps } from "react";
export function SvgHamburger(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={10} height={15} {...props}>
      <path
        d="M.938 0C.42 0 0 .672 0 1.5S.42 3 .938 3h8.124C9.58 3 10 2.328 10 1.5S9.58 0 9.062 0H.938zm0 6C.42 6 0 6.672 0 7.5S.42 9 .938 9h8.124C9.58 9 10 8.328 10 7.5S9.58 6 9.062 6H.938zm0 6C.42 12 0 12.672 0 13.5S.42 15 .938 15h8.124c.518 0 .938-.672.938-1.5S9.58 12 9.062 12H.938z"
        fillRule="nonzero"
      />
    </svg>
  );
}
