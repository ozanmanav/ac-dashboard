import React, { SVGProps } from "react";
export function SvgPlus(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={12} height={12} {...props}>
      <path
        d="M5.077 0v5.077H0v1.846h5.077V12h1.846V6.923H12V5.077H6.923V0z"
        fillRule="evenodd"
      />
    </svg>
  );
}
