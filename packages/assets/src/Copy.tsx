import React, { SVGProps } from "react";
export function SvgCopy(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={18} height={18} {...props}>
      <path d="M17.538 0v13.166H4.37V0h13.167zm-1.543 11.623V1.543H5.915v10.08h10.08zM2.367 5.914h-.824v10.081h10.08v-.875h1.543v2.418H0V4.37h2.367v1.543z" />
    </svg>
  );
}
