import React, { SVGProps } from "react";
export function SvgClose(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={57} height={57} {...props}>
      <defs>
        <filter
          x="-14.3%"
          y="-10.2%"
          width="128.6%"
          height="128.6%"
          filterUnits="objectBoundingBox"
          id="close_svg__a"
        >
          <feOffset dy={2} in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            stdDeviation={2}
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          />
          <feColorMatrix
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0809932255 0"
            in="shadowBlurOuter1"
          />
        </filter>
        <circle id="close_svg__b" cx={24.5} cy={24.5} r={24.5} />
      </defs>
      <g fill="none" fillRule="evenodd">
        <g transform="translate(4 2)">
          <use
            fill="#000"
            filter="url(#close_svg__a)"
            xlinkHref="#close_svg__b"
          />
          <use className="close_svg__oval" xlinkHref="#close_svg__b" />
        </g>
        <path
          className="close_svg__cross"
          d="M20.38 32.818L26.7 26.5l-6.32-6.318a1.276 1.276 0 01-.006-1.81 1.275 1.275 0 011.81.01l6.315 6.318 6.319-6.319A1.272 1.272 0 0137 19.28c0 .339-.138.664-.38.903L30.3 26.5l6.318 6.318a1.276 1.276 0 01.006 1.81 1.275 1.275 0 01-1.809-.01L28.5 28.3l-6.318 6.319a1.275 1.275 0 01-1.803-1.8h.003z"
        />
      </g>
    </svg>
  );
}
