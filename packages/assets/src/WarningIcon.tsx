import { SvgWarning } from "./Warning";
import React from "react";

export function WarningIcon() {
  return <SvgWarning viewBox="0 0 35 35" />;
}
