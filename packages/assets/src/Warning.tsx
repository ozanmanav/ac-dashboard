import React, { SVGProps } from "react";
export function SvgWarning(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={35} height={35} {...props}>
      <path
        d="M20.689 1.32a4.51 4.51 0 00-6.378 0L1.318 14.313a4.515 4.515 0 000 6.377l12.993 12.992a4.516 4.516 0 006.378 0L33.682 20.69a4.515 4.515 0 000-6.377L20.689 1.32zM15 7.337C15 6.046 16.119 5 17.5 5S20 6.046 20 7.337v12.828c0 1.29-1.119 2.335-2.5 2.335S15 21.454 15 20.165V7.337zM17.5 30a2.5 2.5 0 110-5 2.5 2.5 0 010 5z"
        fill="#DA4444"
        fillRule="evenodd"
      />
    </svg>
  );
}
