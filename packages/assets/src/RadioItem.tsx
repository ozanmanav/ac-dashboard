import React, { SVGProps } from "react";
export function SvgRadioItem(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={19} height={19} {...props}>
      <g fill="none" fillRule="evenodd">
        <circle cx={9.5} cy={9.5} r={9} />
        <circle cx={9.5} cy={9.5} r={4.5} />
      </g>
    </svg>
  );
}
