import React, { SVGProps } from "react";
export function SvgAppIcon(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={48} height={48} {...props}>
      <defs>
        <path
          d="M8 0h32a8 8 0 018 8v32a8 8 0 01-8 8H8a8 8 0 01-8-8V8a8 8 0 018-8zm2.562 21.326l6.697 4.888-2.553 7.9a1.372 1.372 0 001.296 1.806c.285 0 .567-.09.81-.274l6.687-4.878 6.708 4.895c.474.34 1.11.34 1.602-.01.478-.354.673-.97.488-1.526l-2.558-7.91 6.698-4.891c.481-.35.68-.964.494-1.533a1.359 1.359 0 00-1.298-.946h-8.279L24.8 10.95a1.35 1.35 0 00-1.3-.95c-.6 0-1.124.384-1.297.936l-2.558 7.91h-8.281a1.36 1.36 0 00-1.298.946 1.37 1.37 0 00.496 1.534z"
          id="appIcon_svg__a"
        />
      </defs>
      <use fillRule="nonzero" xlinkHref="#appIcon_svg__a" fill="none" />
    </svg>
  );
}
