import React, { SVGProps } from "react";
export function SvgModalArrow(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={9} height={14} {...props}>
      <path
        d="M.707 6.293l.775-.775L6.686.314a1.047 1.047 0 011.797.739c0 .28-.113.548-.313.744L2.965 7l5.203 5.203a1.05 1.05 0 01.006 1.49 1.05 1.05 0 01-1.49-.007L1.482 8.482l-.775-.775a1 1 0 010-1.414z"
        fill="#1A3352"
        fillRule="evenodd"
      />
    </svg>
  );
}
