import React, { SVGProps } from "react";
export function SvgLogs(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={16} height={20} {...props}>
      <path
        d="M6.6 0H16v20H0V6.6L6.6 0zM6 3.4L3.4 6H6V3.4zM14 18V2H8v6H2v10h12zM4 14h8v2H4v-2zm0-4h8v2H4v-2z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </svg>
  );
}
