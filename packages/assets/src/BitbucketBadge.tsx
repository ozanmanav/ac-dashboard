import React, { SVGProps } from "react";
export function SvgBitbucketBadge(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={30} height={20} {...props}>
      <g fill="none" fillRule="evenodd">
        <rect fill="#3D9DEE" width={30} height={20} rx={4} />
        <text
          fontFamily="HelveticaNeue-Bold, Helvetica Neue"
          fontSize={11}
          fontWeight="bold"
          letterSpacing={0.183}
          fill="#FFF"
        >
          <tspan x={4.034} y={14}>
            {"BitB"}
          </tspan>
        </text>
      </g>
    </svg>
  );
}
