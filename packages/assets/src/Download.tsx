import React, { SVGProps } from "react";
export function SvgDownload(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={20} height={17} {...props} viewBox="0 0 20 17">
      <path
        fill="#617EA2"
        d="M15 8l-5 5-5-5 1.4-1.4L9 9.2V0h2v9.2l2.6-2.6L15 8zm3 1h2v8H0V9h2v6h16V9z"
      />
    </svg>
  );
}
