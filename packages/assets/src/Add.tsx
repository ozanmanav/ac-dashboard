import React, { SVGProps } from "react";
export function SvgAdd(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={19} height={18} {...props} viewBox="0 0 19 18">
      <path
        fill="#FFF"
        fillRule="evenodd"
        d="M9.104 0C4.138 0 .112 4.03.112 9s4.026 9 8.992 9c4.965 0 8.991-4.03 8.991-9S14.07 0 9.104 0zm3.962 10.034h-2.93v2.933c0 .568-.464 1.033-1.032 1.033a1.036 1.036 0 01-1.033-1.034v-2.932H5.14A1.036 1.036 0 014.108 9c0-.569.465-1.034 1.033-1.034h2.93V5.034c0-.57.464-1.034 1.033-1.034.568 0 1.032.465 1.032 1.034v2.933h2.931a1.035 1.035 0 01-.001 2.067z"
      />
    </svg>
  );
}
