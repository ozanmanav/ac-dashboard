import React, { SVGProps } from "react";
export function SvgCheck(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={9} height={7} {...props}>
      <path
        d="M3.482 4.093L1.446 2.047 0 3.5 3.482 7 9 1.453 7.554 0z"
        fillRule="evenodd"
      />
    </svg>
  );
}
