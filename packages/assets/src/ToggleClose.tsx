import React, { SVGProps } from "react";
export function SvgToggleClose(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={14} height={9} {...props}>
      <path
        fill="#1A3352"
        fillRule="evenodd"
        d="M5.517 1.482L.313 6.686a1.05 1.05 0 00-.005 1.49 1.05 1.05 0 001.49-.008l5.201-5.203 5.204 5.203A1.047 1.047 0 0014 7.43c0-.279-.113-.547-.313-.743L8.482 1.482 6.999 0 5.517 1.482z"
      />
    </svg>
  );
}
