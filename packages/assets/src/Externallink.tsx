import React, { SVGProps } from "react";
export function SvgExternallink(props: SVGProps<SVGSVGElement>) {
  return (
    <svg height={1024} width={768} {...props}>
      <path
        fill="#5b799e"
        d="M640 768H128V257.906L256 256V128H0v768h768V576H640v192zM384 128l128 128-192 192 128 128 192-192 128 128V128H384z"
      />
    </svg>
  );
}
