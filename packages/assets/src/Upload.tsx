import React, { SVGProps } from "react";
export function SvgUpload(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={20} height={17} {...props} viewBox="0 0 20 17">
      <path
        fill="#617EA2"
        d="M15 5l-1.4 1.4L11 3.8V13H9V3.8L6.4 6.4 5 5l5-5 5 5zm3 4h2v8H0V9h2v6h16V9z"
      />
    </svg>
  );
}
