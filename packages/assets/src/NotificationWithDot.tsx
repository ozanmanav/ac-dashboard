import React, { SVGProps } from "react";
export function SvgNotificationWithDot(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={23} height={24} {...props}>
      <path
        fill="#5B799E"
        d="M12.593 21.36c0 1.457-1.16 2.64-2.593 2.64s-2.594-1.183-2.594-2.64h5.187zm4.883-4.961l2.309 2.619c.21.212.273.534.159.813a.736.736 0 01-.68.463H.736a.736.736 0 01-.679-.463.754.754 0 01.159-.813l2.308-2.62v-5.774S2 3.744 8.092 2.567V1.94C8.094.868 8.947 0 10 0s1.906.868 1.908 1.94v.627c6.103 1.175 5.568 8.057 5.568 8.057V16.4z"
      />
      <circle
        cx={17.5}
        cy={6.5}
        r={4.5}
        fill="#FF5757"
        stroke="#1A3352"
        strokeWidth={2}
      />
    </svg>
  );
}
