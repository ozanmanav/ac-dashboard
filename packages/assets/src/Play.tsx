import React, { SVGProps } from "react";
export function SvgPlay(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={15} height={16} {...props}>
      <path
        fill="#5B799E"
        fillRule="evenodd"
        d="M13.596 9.03L2.33 15.327a1 1 0 01-1.488-.872V1.862A1 1 0 012.33.99l11.266 6.296a1 1 0 010 1.746z"
      />
    </svg>
  );
}
