const path = require("path");

module.exports = ({ config }) => {
  config.resolve.modules = ["node_modules"];

  // TypeScript support
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    use: [
      {
        loader: require.resolve("babel-loader"),
        options: {
          presets: [require.resolve("babel-preset-react-app")]
        }
      },

      {
        loader: require.resolve("react-docgen-typescript-loader"),
        options: {
          tsconfigPath: path.join(__dirname, "../tsconfig.json")
        }
      }
    ]
  });

  config.resolve.extensions.push(".ts", ".tsx");

  return config;
};
