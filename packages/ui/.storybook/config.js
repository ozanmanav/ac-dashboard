import React from "react";

import { withInfo } from "@storybook/addon-info";
import { configure, addDecorator, addParameters } from "@storybook/react";
import theme from "./theme";

addDecorator(storyFn => (
  <div style={{ backgroundColor: "#1a3352", padding: "20px 40px 20px" }}>
    {storyFn()}
  </div>
));

addDecorator(
  withInfo({
    inline: true,
    header: false,
    source: false
  })
);

// Option defaults.
addParameters({
  info: {
    inline: true,
    // inherit colors from preview-iframe.html
    styles: {
      infoBody: {
        color: "white",
        backgroundColor: "transparent"
      }
    }
  },
  options: {
    theme
  }
});

configure(require.context("../src/", true, /\.stories\.(tsx|mdx)$/), module);
