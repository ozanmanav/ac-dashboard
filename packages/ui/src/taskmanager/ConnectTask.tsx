import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { TaskStatusType } from "@appcircle/taskmanager/lib/TaskStatusType";

export type ConnectTaskProps = {
  loadingComponent?: (taskStatus: TaskStatusType | undefined) => JSX.Element;
  errorComponent?: (taskStatus: TaskStatusType | undefined) => JSX.Element;
  render: (props: TaskStatusType | undefined) => JSX.Element;
  entityID?: string;
  taskID?: string;
};

export function ConnectTask(props: ConnectTaskProps) {
  const taskManager = useTaskManager();
  const taskStatus = props.entityID
    ? taskManager.getStatusByEntityID(props.entityID)
    : props.taskID
    ? taskManager.getStatusByTaskId(props.taskID)
    : undefined;
  const ADDED =
    taskStatus && taskStatus.status === "ADDED" && props.loadingComponent;
  const LOADING =
    !taskStatus ||
    taskStatus.status === "IN_PROGRESS" ||
    taskStatus.status === "WAITING_RESPONSE";

  // && props.loadingComponent;
  const ERROR = taskStatus && taskStatus.status === "ERROR";
  const component =
    (ADDED || LOADING) && taskStatus && props.loadingComponent
      ? props.loadingComponent(taskStatus)
      : ERROR && props.errorComponent
      ? props.errorComponent(taskStatus)
      : props.render(taskStatus);

  return component;
}
