import React, { useMemo } from "react";

import { Message } from "@appcircle/shared/lib/Module";
import { MAX_MESSAGES } from "@appcircle/shared/lib/notification/messageReducer";
import { useNotificationsModel } from "@appcircle/shared/lib/notification/useNotificationsModel";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { ToastComponent } from "./ToastComponent";

type removeMessageFn = (payload: Message) => void;

export type IToastComponent = {
  message: Message;
  index: number;
  onRemove: removeMessageFn;
};

export type ToastActionCreatorsType = {
  pushToast: (message: Message, payload: any) => void;
  updateToast: (id: string, payload: any) => void;
  removeToast: (id: string, payload: any) => void;
};

export const DEFAULT_MESSAGE_LIFE = 2000;
export const DEFAULT_TOAST_HEIGHT = 30;
export const TOAST_VERTICAL_GAP = 13;

export const ToastService = (props: {}) => {
  const { removeMessage } = useNotificationsService();
  const { getMessages } = useNotificationsModel();
  const messages = useMemo(() => getMessages(), [getMessages]);

  // TODO: Cache messages
  return (
    <React.Fragment>
      {messages.slice(0, MAX_MESSAGES).map((message, index) => (
        <ToastComponent
          key={message.id}
          message={message}
          index={index}
          onRemove={removeMessage}
        />
      ))}
    </React.Fragment>
  );
};

export type MessageStatus = "keep-open" | "show" | "hide" | "delete";
