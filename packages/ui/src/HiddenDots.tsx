import React from "react";

export function HiddenDots() {
  return (
    <React.Fragment>
      <span className="HiddenDot" />
      <span className="HiddenDot" />
      <span className="HiddenDot" />
      <span className="HiddenDot" />
      <span className="HiddenDot" />
      <span className="HiddenDot" />
      <span className="HiddenDot" />
    </React.Fragment>
  );
}
