// src/shared/component/RadioButton.stories.js

import React from "react";

import { Spinner } from "./Spinner";
import "./Spinner.stories.scss";

export default {
  component: Spinner,
  title: "Design System|UI/Spinner",
  decorators: [
    (story: any) => (
      <div
        style={{
          backgroundColor: "#f5f6f9",
          borderRadius: "0.3125rem",
          width: 30
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <Spinner />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Simple = () => {
  return <Spinner />;
};
