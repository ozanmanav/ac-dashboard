import React, { PropsWithChildren, useState } from "react";
import classNames from "classnames";

export enum ButtonType {
  DANGER = "danger",
  PRIMARY = "primary",
  SUCCESS = "success", // TODO: There is no style for success button.
  WARNING = "warning",
  LIGHT = "primaryLight"
}
export type ButtonSize = "md" | "lg" | "sm" | "xsm";

export type ButtonProps = PropsWithChildren<{
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  size?: ButtonSize;
  type?: ButtonType;
  className?: string;
  ariaLabel?: string;
  htmlType?: React.ButtonHTMLAttributes<HTMLButtonElement>["type"];
  showFocus?: boolean;
  tabIndex?: number;
  disabled?: boolean;
}>;

export function Button(props: ButtonProps) {
  const [isFocussed, setisFocussed] = useState(false);
  const className = classNames(
    "Button",
    props.className,
    `Button-${props.size || "md"}`,
    `Button-${props.type || "primary"}`,
    `Button-disabled--${!!props.disabled}`,
    `Button-isFocus--${!!isFocussed}`
  );
  return (
    <button
      tabIndex={props.tabIndex}
      type={props.htmlType || "button"}
      className={className}
      onClick={props.onClick}
      aria-label={props.ariaLabel}
      onFocus={e => {
        props.showFocus && setisFocussed(true);
      }}
      onBlur={e => {
        props.showFocus && setisFocussed(false);
      }}
    >
      {props.children}
    </button>
  );
}
