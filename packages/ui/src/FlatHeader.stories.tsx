import React from "react";
import "./FlatHeader.stories.scss";
import { FlatHeader } from "./FlatHeader";

export default {
  component: FlatHeader,
  title: "Design System|UI/FlatHeader",
  parameters: {
    info: {
      text: `
            ~~~js
            <FlatHeader>Flat Header</FlatHeader>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const FlatHeaderComponent = () => {
  return <FlatHeader>Flat Header</FlatHeader>;
};
