import React, { PropsWithChildren } from "react";
import classNames from "classnames";
import { ButtonType } from "./Button";

export type TextButtonProps = PropsWithChildren<{
  title: string;
  type?: ButtonType;
  onClick?: () => void;
  disabled?: boolean;
}>;

export function TextButton(props: TextButtonProps) {
  const className = classNames(
    "TextButton",
    `TextButton-${props.type || ButtonType.PRIMARY}`,
    {
      "SubmitButton-disabled": !!props.disabled
    }
  );
  return (
    <div className={className} onClick={props.onClick}>
      <div>{props.title}</div>
    </div>
  );
}
