import React from "react";
import { ComponentStyle } from "@appcircle/core/lib/ComponentStyle";
import { SvgCloseSmall } from "@appcircle/assets/lib/CloseSmall";

type TinyCloseButtonProps = {
  onClick: () => void;
  type?: ComponentStyle;
};
export function TinyCloseButton(props: TinyCloseButtonProps) {
  return (
    <div
      className={
        "TinyCloseButton TinyCloseButton_" +
        (props.type || ComponentStyle.Secondary)
      }
      onClick={props.onClick}
    >
      <SvgCloseSmall />
    </div>
  );
}
