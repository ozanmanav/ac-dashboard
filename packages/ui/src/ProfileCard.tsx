import React, {
  PropsWithChildren,
  useState,
  useEffect,
  ReactElement,
  useMemo,
  useCallback
} from "react";
import { Link } from "react-router-dom";
import { ThreeDotsToggle } from "./ThreeDotsToggle";
import { TextField } from "./TextField";
import { isValidName } from "@appcircle/core/lib/utils/isValidName";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import classNames from "classnames";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { Spinner } from "./Spinner";
import { useIntl } from "react-intl";
import { textWithSpecialsMaybe } from "@appcircle/core/lib/utils/alphaNumericTextMaybe";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { TaskStatus } from "@appcircle/taskmanager/lib/TaskStatus";
import { SvgCardPin } from "@appcircle/assets/lib/CardPin";
import { ContextMenu } from "./menus/ContextMenu";
import { ContextMenuItemDataType } from "./menus/ContextMenuItem";

export enum ProfileCardStatus {
  READY = 0,
  EDITING = 1,
  INPROGRESS = 2
}
export type ProfileCardFooterData = {
  header: string;
  text: string;
  enable: boolean;
};

export type ProfileCardDataType = {
  isPinned: boolean;
  id: string;
  isTemporary: boolean;
  name: string;
};

export type ProfileCardServiceType = {
  rename: (id: string, newName: string, isTemporary: boolean) => void;
  pin: (id: string, isPinned: boolean) => void;
  delete: (id: string, isTemporary?: boolean) => void;
  doesNameExist: (name: string) => boolean;
};

export type ProfileCardProps<
  T extends ProfileCardDataType = ProfileCardDataType
> = PropsWithChildren<{
  data: T;
  service: ProfileCardServiceType;
  profileDetailLink: string;
  footerData: ProfileCardFooterData[];
  index: number;
  onBlur: () => void;
  onEditModeChange: (editMode: boolean) => void;
  contextMenuItems: ContextMenuItemDataType[];
  editMode: boolean;
  onRenameError: (error: NameValidState, newProfileName: string) => void;
  onContextMenuItemSelected: (index: number) => void;
  IconTemplate?: ReactElement;
  onStatusChange: (status?: TaskStatus) => void;
  highlighted?: boolean;
}>;
export function ProfileCard(props: ProfileCardProps) {
  const { getStatusByEntityID } = useTaskManager();
  const taskStatus = getStatusByEntityID(props.data.id) || {
    status: undefined
  };
  const [tempText, setTempText] = useState<string>(props.data.name);
  useEffect(() => {
    props.onEditModeChange && props.editMode && props.onEditModeChange(true);
    // eslint-disable-next-line
  }, [props.editMode]);
  // const [highlighted, setHighlighted] = useState<boolean | undefined>();
  // useEffect(() => {
  //   props.onStatusChange && props.onStatusChange(taskStatus.status);
  //   if (highlighted === false) {
  //     taskStatus.status === 'COMPLETED' && setHighlighted(true);
  //     taskStatus.status === 'COMPLETED' && setTimeout(() => setHighlighted(false), 400);
  //   }
  //   // eslint-disable-next-line
  // }, [taskStatus.status]);

  // useEffect(() => {
  //   if (highlighted === undefined) {
  //     setHighlighted(false);
  //   }
  // },
  //   [highlighted]
  // )

  const intl = useIntl();
  const onChangeEnd = useCallback(
    (newProfileName, isCancel) => {
      if (!newProfileName || isCancel === true) {
        props.onEditModeChange(false);
        return;
      }
      setTempText(newProfileName);
      if (props.data.isTemporary && !newProfileName) {
        props.service.delete(props.data.id, isFakeID(props.data.id));
      } else if (
        (!isFakeID(props.data.id) && // editmode false
          props.data.name === newProfileName) ||
        !newProfileName
      ) {
        props.onEditModeChange(false);
      } else {
        // rename profile
        const validState = isValidName(newProfileName.toString());
        const nameExists = props.service.doesNameExist(newProfileName);
        if (validState === NameValidState.VALID && !nameExists) {
          if (
            props.service.rename(
              props.data.id,
              newProfileName,
              isFakeID(props.data.id)
            ) !== undefined
          )
            props.onEditModeChange(false);
        } else {
          props.onRenameError(
            nameExists ? NameValidState.NAME_EXISTS : validState,
            newProfileName
          );
        }
      }
    },
    [props]
  );

  const titleMarkup = useMemo(
    () => (
      <TextField
        text={
          isFakeID(props.data.id) && tempText === props.data.name
            ? ""
            : tempText || ""
        }
        editMode={isFakeID(props.data.id) || props.editMode}
        multiLineMode={true}
        placeHolder={props.data.name}
        onBlurOrEscape={props.onBlur}
        onValidate={textWithSpecialsMaybe}
        onChangeEnd={onChangeEnd}
      />
    ),
    [
      props.data.name,
      props.onBlur,
      props.editMode,
      props.data.id,
      tempText,
      onChangeEnd
    ]
  );

  const body = useMemo(
    () => (
      <React.Fragment>
        <div className="ProfileCard_head">
          {props.IconTemplate && (
            <div className="ProfileCard_head_icon">{props.IconTemplate}</div>
          )}
          <div className="ProfileCard_head_title">{titleMarkup}</div>
        </div>
        {props.children}
        <div className="ProfileCard_downseperator" />
        <div className="ProfileCard_setting">
          {props.footerData.map(item => (
            <div className="ProfileCard_setting_item" key={item.header}>
              <div
                className={classNames({
                  "ProfileCard-enabledText": item.enable
                })}
              >
                {item.header}:{" "}
              </div>
              <div
                className={classNames({
                  "ProfileCard-enabledText": item.enable
                })}
              >
                {item.text}
              </div>
            </div>
          ))}
        </div>
        {taskStatus.status === "ADDED" ||
        taskStatus.status === "IN_PROGRESS" ? (
          <div className="ProfileCard_overlay">
            <Spinner />
          </div>
        ) : null}
      </React.Fragment>
    ),
    [
      props.IconTemplate,
      props.children,
      props.footerData,
      taskStatus.status,
      titleMarkup
    ]
  );
  return (
    <div
      className={classNames("ProfileCard", {
        "ProfileCard-inprogress":
          taskStatus.status === "IN_PROGRESS" || taskStatus.status === "ADDED",
        "ProfileCard-inEditMode": props.editMode,
        "ProfileCard-highlighted": props.highlighted
      })}
    >
      {props.data.isPinned ? (
        <SvgCardPin
          onClick={() => props.onContextMenuItemSelected(0)}
          className="ProfileCard_pin"
        />
      ) : null}
      <div className="ProfileCard_rightTop">
        <ThreeDotsToggle>
          <ContextMenu
            items={props.contextMenuItems}
            onMenuItemSelected={props.onContextMenuItemSelected}
          />
        </ThreeDotsToggle>
      </div>
      {!props.editMode ? (
        <Link to={props.profileDetailLink}>{body}</Link>
      ) : (
        body
      )}
    </div>
  );
}
