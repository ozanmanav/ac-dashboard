import React, {
  PropsWithChildren,
  useRef,
  useCallback,
  useEffect,
  useState
} from "react";
import classNames from "classnames";
import { useSpring, animated } from "react-spring";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { ScrollView } from "./ScrollView";
import { Spinner } from "./Spinner";
import { SvgModalArrow } from "@appcircle/assets/lib/ModalArrow";
import { SvgCloseSmall } from "@appcircle/assets/lib/CloseSmall";

export type ModalWindowType<T> = T & {
  header: string | JSX.Element;
  className?: string;
  onModalClose: () => void;
  onBeforeModalClose?: () => boolean;
  showBackButton?: boolean;
  onBackButton?: () => void;
  id?: string;
  spinner?: boolean;
};
export type ModalWindowProps = ModalWindowType<PropsWithChildren<{}>>;

// const CloseIcon = icons.closeSmall;

export function useModalAnim(id: string, onModalClose: () => void) {
  const [animProps, setAnim] = useSpring(() => ({
    config: { mass: 2, tension: 1000, friction: 70 },
    transform: `translateX(0)`,
    opacity: 1,
    from: { opacity: 0, transform: `translateX(20%)` }
  }));
  // TODO: Move modal's status to useModalStatus
  const [status, setStatus] = useState<
    "willOpen" | "willClose" | "closing" | "opening" | "opened" | "closed"
  >("closed");
  // const [setModalStatus, modalStatus] = useModalStatus(id);
  useEffect(() => {
    switch (status) {
      case "willClose":
        setAnim({
          opacity: 0,
          transform: `translateX(20%)`,
          config: { mass: 8, tension: 1000, friction: 40 }
        });
        setStatus("closing");
        setTimeout(() => onModalClose(), 200);
        break;
    }
  }, [onModalClose, setAnim, status]);

  return { status, setStatus, animProps };
}

export function ModalWindow(props: ModalWindowProps) {
  const rootRef = useRef(null);
  const { status, setStatus, animProps } = useModalAnim(
    props.id || "",
    props.onModalClose
  );

  const onBeforeModalClose = useRef(props.onBeforeModalClose);
  onBeforeModalClose.current = props.onBeforeModalClose;

  const closeModal = useCallback(
    () =>
      (!onBeforeModalClose.current || onBeforeModalClose.current()) &&
      status !== "closing" &&
      setStatus("willClose"),
    [setStatus, status]
  );
  const outsideClickHandler = useCallback(closeModal, [props]);
  useGlobalClickService([], rootRef, outsideClickHandler, "mousedown");

  return (
    <animated.div
      ref={rootRef}
      style={animProps as any}
      className={classNames("ModalWindow", props.className)}
    >
      <div className="ModalWindow_header">
        {props.showBackButton ? (
          <div
            className="ModalWindow_header_backButton"
            onClick={props.onBackButton}
          >
            <SvgModalArrow />
          </div>
        ) : null}
        <div className="ModalWindow_header_label">
          {props.spinner === true && <Spinner />} {props.header}
        </div>
        <div className="ModalWindow_header_close" onClick={closeModal}>
          <SvgCloseSmall />
        </div>
      </div>
      <div className={"ModalWindow_body"}>{props.children}</div>
    </animated.div>
  );
}

export function ModalContent(props: PropsWithChildren<{}>) {
  return <ScrollView>{props.children}</ScrollView>;
}
