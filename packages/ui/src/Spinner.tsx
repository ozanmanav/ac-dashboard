import React from "react";

export const Spinner = () => (
  <div className="Spinner">
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);
