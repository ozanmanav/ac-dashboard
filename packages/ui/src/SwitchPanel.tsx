import React, { useState, useEffect } from "react";
import { TogglePanelProps } from "./TogglePanel";
import { Toggle } from "./form/Toggle";
import { Switch } from "./form/Switch";

export function SwitchPanel(props: TogglePanelProps) {
  const [isSelected, setIsSelected] = useState(!!props.isSelected);

  useEffect(() => {
    !!props.isSelected !== isSelected && setIsSelected(!!props.isSelected);
    // eslint-disable-next-line
  }, [props.isSelected]);

  useEffect(() => {
    props.onChange && props.onChange(isSelected);
  }, [isSelected]);

  return (
    <div className={props.className + " SwitchPanel"}>
      <Toggle
        title={props.title}
        selected={isSelected}
        disabled={!!props.isDisabled}
        onClick={() => {
          setIsSelected(!isSelected);
        }}
      >
        <Switch isSelected={isSelected} />
      </Toggle>
      {isSelected && props.children !== undefined && props.children}
    </div>
  );
}
