import React, { ReactElement, useCallback, useState } from "react";
import { ScrollView } from "./ScrollView";
export type ProfileGridViewProfileCardProps<TData = any> = {
  data: TData;
  detailUrl: string;
  index: number;
  onEditMode: (val: boolean, index: number) => void;
  className: string;
  highlighted?: boolean;
};
type ProfileGridViewProfileCard<TData = any> = (
  props: ProfileGridViewProfileCardProps<TData>
) => ReactElement;

type ProfilesGridProps<P> = {
  profilesData: P[];
  profileCardComponent: ProfileGridViewProfileCard;
  link: string;
};
export function ProfileGridView<P extends { id: string } = any>(
  props: ProfilesGridProps<P>
) {
  const {
    profilesData: profileData,
    profileCardComponent: ProfileCardComponent
  } = props;
  const [editModeCards, setEditcModeCards] = useState<number[]>([]);
  const onEditMode = useCallback((val, index) => {
    setEditcModeCards(val ? [index] : []);
  }, []);

  return (
    <ScrollView>
      <div className="ProfilesGridView_ProfilesGrid">
        {profileData.map((item, index: number) => {
          const inEditMode = editModeCards.some(i => index === i);
          return (
            <ProfileCardComponent
              className={inEditMode ? "ProfileCard-inEditMode" : ""}
              key={item.id}
              data={item}
              detailUrl={props.link + item.id}
              index={index}
              onEditMode={onEditMode}
            />
          );
        })}
        {
          <div
            className={
              "editModeOverlay editModeOverlay-show--" + !!editModeCards.length
            }
          ></div>
        }
      </div>
    </ScrollView>
  );
}
