import React, {
  FunctionComponent,
  SVGProps,
  useMemo,
  ReactElement
} from "react";
import classNames from "classnames";
import { TextField } from "./TextField";
import { TaskProgressView } from "./TaskProgressView";
import { ScrollView } from "./ScrollView";
import { isValidName } from "@appcircle/core/lib/utils/isValidName";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import messages from "@appcircle/shared/lib/messages/messages";
import { useIntl } from "react-intl";

export type GroupListItemType = {
  id: any;
  name: string;
  icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
  count?: number;
  editMode?: boolean;
};
export type GroupListType = {
  items: GroupListItemType[];
  selectedGroupID: any;
  icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
  fallback: ReactElement;
};

type GroupListProps = {
  onError: (err: string) => void;
  onChange?: (name: string) => void;
  onEnter: (name: string) => void;
  onBlurOrEscape?: () => void;
  onItemSelect?: (item: GroupListItemType, index: number) => void;
  onValidate?: (oldName: string, groupName: string) => string;
} & GroupListType;
export function GroupList(props: GroupListProps) {
  const intl = useIntl();
  const ListItems = useMemo(
    () =>
      props.items.map((item, index) => {
        const editMode = item.editMode;
        // TODO: Progressbar doesn't work.
        return (
          <div
            key={item.id}
            className={classNames("GroupList_item", {
              "GroupList_item-selected": props.selectedGroupID === item.id,
              "GroupList_item-editMode": editMode
            })}
            onClick={() =>
              !editMode && props.onItemSelect && props.onItemSelect(item, index)
            }
          >
            <TaskProgressView
              entityID={item.id}
              isOverlay={true}
              hasRelativeRoot={true}
              progressType="stripe"
            >
              <TextField
                onValidate={props.onValidate}
                className="GroupList_item_title"
                text={item.name}
                editMode={editMode}
                onChange={props.onChange}
                onEnter={groupName => {
                  if (isGroupDuplicate(groupName.trim(), props.items)) {
                    // return services.showError(
                    // intl.formatMessage(groupName.trim()
                    //   ? messages.appCommonSubjectExists
                    //   : messages.appCommonMustNotbeEmpty
                    //   , {
                    //     subject: intl.formatMessage(messages.appCommonGroupName)
                    //   })
                    // );
                    props.onError(
                      intl.formatMessage(
                        groupName.trim()
                          ? messages.appCommonSubjectExists
                          : messages.appCommonMustNotbeEmpty,
                        {
                          subject: intl.formatMessage(
                            messages.appCommonGroupName
                          )
                        }
                      )
                    );
                  }

                  //If existing group is being edited
                  else {
                    const validState = isValidName(groupName);
                    switch (validState) {
                      case NameValidState.VALID:
                        props.onEnter(groupName);
                        break;
                      case NameValidState.TOO_LONG:
                        props.onError(
                          intl.formatMessage(
                            messages.appCommonFormFieldErrorTooLong,
                            {
                              subject: intl.formatMessage(
                                messages.appCommonGroupName
                              )
                            }
                          )
                        );
                        break;
                      case NameValidState.TOO_SHORT:
                        props.onError(
                          intl.formatMessage(
                            messages.appCommonFormFieldErrorTooShort,
                            {
                              subject: intl.formatMessage(
                                messages.appCommonGroupName
                              )
                            }
                          )
                        );
                        break;
                      case NameValidState.INVALID_CHARACTER:
                        props.onError(
                          intl.formatMessage(
                            messages.appCommonFormFieldErrorContainsIvalidChars,
                            {
                              subject: intl.formatMessage(
                                messages.appCommonGroupName
                              )
                            }
                          )
                        );
                        break;
                    }
                  }
                }}
                onBlurOrEscape={props.onBlurOrEscape}
              />
              <div className="GroupList_item_rightItem">
                <div className="GroupList_item_rightItem_icon">
                  {item.icon ? (
                    <item.icon />
                  ) : props.icon ? (
                    <props.icon />
                  ) : null}
                </div>
                <div className="GroupList_item_rightItem_key">
                  {item.count}{" "}
                </div>
              </div>
            </TaskProgressView>
          </div>
        );
      }),
    [props]
  );
  return (
    <div className="GroupList flex-column">
      <ScrollView className="GroupList_scrollbar">{ListItems}</ScrollView>
      {props.items.length === 0 && <div>{props.fallback}</div>}
    </div>
  );
}

function isGroupDuplicate(name: string, array: any[]) {
  return array.some(e => e.name === name);
}
