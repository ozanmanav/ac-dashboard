import React, { useMemo } from "react";
import classNames from "classnames";
import { FormElementType } from "@appcircle/form/lib/Form";
import { ComponentGroupType } from "./form/ComponentGroup";
import { SwitchPanel } from "./SwitchPanel";

export type SwitchPanelGroupProps = ComponentGroupType &
  FormElementType<(string | number | null)[]>;

export function SwitchPanelGroup(props: SwitchPanelGroupProps) {
  const value = useMemo(() => props.value || [], [props.value]);

  return (
    <div
      className={classNames(props.className, "SwitchPanelGroup", {
        "SwitchPanelGroup-disabled": props.isDisabled,
        "SwitchPanelGroup-invalid": props.isValid === false
      })}
    >
      {
        props.items.map((item, index: number) => (
          <div
            key={item.value}
            className={classNames("ComponentGroup_item", {
              ".ComponentGroup_item-selected": props.value === item.value
            })}
          >
            <SwitchPanel
              onChange={val => {
                const values = val === true
                  ? [...value.filter(v => item.value !== v), item.value]
                  : value.filter(v => item.value !== v);
                props.onChange && props.onChange(values);
              }}
              title={item.label}
              isSelected={value.some(v => v === item.value)}
            />
          </div>
        ))
      }
    </div>
  );
}
