import React from "react";
import "./InlineTabButtonGroup.stories.scss";
import { InlineTabButtonGroup } from "./InlineTabButtonGroup";
import { action } from "@storybook/addon-actions";

export default {
  component: InlineTabButtonGroup,
  title: "Design System|UI/InlineTabButtonGroup",
  parameters: {
    info: {
      text: `
            ~~~js
            const items:string[] = ["Tab-1", "Tab-2", "Tab-3"];

            <InlineTabButtonGroup
              items={items}
              onChange={onChange}
              selectedIndex={0}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const items: string[] = ["Tab-1", "Tab-2", "Tab-3"];

export const InlineTabButtonGroupComponent = () => {
  return (
    <InlineTabButtonGroup
      items={items}
      onChange={action("onChange")}
      selectedIndex={0}
    />
  );
};
