export type ListItemType = { label: string; value: any; expression ?: string };
export type ListItemsType = ListItemType[];