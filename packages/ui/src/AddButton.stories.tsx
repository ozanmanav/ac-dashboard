import React from "react";

import "./AddButton.stories.scss";
import { AddButton } from "./AddButton";
import { action } from "@storybook/addon-actions";
import { IconButtonState } from "./IconButton";

const text = "Add";
// change
export default {
  component: AddButton,
  title: "Design System|UI/AddButton",
  parameters: {
    info: {
      text: `
            ~~~js

             <AddButton
             text={"${text}"}
             size={"xsm" | "sm" | "md" | "lg"}
             state={IconButtonState.ENABLED | IconButtonState.DISABLED}
             onClick={action("onClick")}
             />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

// export const EnabledAddButtonXSmall = () => {
//   return (
//     <AddButton
//       text={text}
//       size="xsm"
//       state={IconButtonState.ENABLED}
//       onClick={action("onClick")}
//     />
//   );
// };

export const EnabledAddButtonSmall = () => {
  return (
    <AddButton
      text={text}
      size="sm"
      state={IconButtonState.ENABLED}
      onClick={action("onClick")}
    />
  );
};

export const EnabledAddButtonMedium = () => {
  return (
    <AddButton
      text={text}
      size="md"
      state={IconButtonState.ENABLED}
      onClick={action("onClick")}
    />
  );
};

export const EnabledAddButtonLarge = () => {
  return (
    <AddButton
      text={text}
      size="lg"
      state={IconButtonState.ENABLED}
      onClick={action("onClick")}
    />
  );
};

export const AddButtonDisabled = () => {
  return (
    <AddButton
      text={text}
      state={IconButtonState.DISABLED}
      onClick={action("onClick")}
    />
  );
};
