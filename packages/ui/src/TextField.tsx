import React, { PropsWithChildren, useState, useEffect } from "react";
import classNames from "classnames";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";

export type TextFieldProps = PropsWithChildren<{
  text: string;
  placeHolder?: string;
  className?: string;
  onChange?: (text: string) => void;
  onChangeEnd?: (text: string, isCancel?: boolean) => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  onBlur?: () => void;
  editMode?: boolean;
  disabled?: boolean;
  onEnter?: (text: string) => void;
  onBlurOrEscape?: () => void;
  onValidate?: (oldName: string, groupName: string) => string;
  multiLineMode?: boolean;
}>;

export function TextField(props: TextFieldProps) {
  const [text, setText] = useState("");
  const className = classNames("TextField", props.className, {
    "TextField-readonly": !props.editMode
  });
  useEffect(() => {
    //update text when props.text change.
    setText(props.text);
  }, [props.text]);

  const inputProps = {
    className,
    autoFocus: true,
    spellCheck: false,
    value: text || "",
    placeholder: props.placeHolder,
    disabled: props.disabled,
    onChange: (
      e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
      let val = props.onValidate
        ? props.onValidate(text, e.target.value)
        : e.target.value;
      setText(val);
      props.onChange && val !== text && props.onChange(val);
    },
    onBlur:
      props.onBlur ||
      (() => {
        setText(props.text);
        props.onChangeEnd && props.onChangeEnd(props.text, true);
        props.onBlurOrEscape && props.onBlurOrEscape();
      }),
    onKeyDown: (
      e: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
      e.stopPropagation();
      if (e.keyCode === KeyCode.ESC_KEY) {
        setText(props.text);
        props.onChangeEnd && props.onChangeEnd(props.text, true);
        props.onBlurOrEscape && props.onBlurOrEscape();
      } else if (e.keyCode === KeyCode.ENTER_KEY) {
        props.onChangeEnd && props.onChangeEnd(text);
        props.onEnter && props.onEnter(text);
      }
    }
  };

  return props.editMode ? (
    <>
      {props.multiLineMode ? (
        <textarea {...inputProps} style={{ resize: "none" }} />
      ) : (
        <input {...inputProps} style={{ resize: "none" }} />
      )}
    </>
  ) : (
    <span className={className}>{text}</span>
  );
}
