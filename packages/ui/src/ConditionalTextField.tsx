import React, {
  PropsWithChildren,
  useState,
  FunctionComponent,
  SVGProps,
  useEffect,
  useCallback
} from "react";
import { IconTextField } from "./IconTextField";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import classNames from "classnames";

export type ConditionalTextFieldProps = PropsWithChildren<{
  placeHolder: string;
  text: string;
  resetState?: boolean;
  icon: FunctionComponent<SVGProps<SVGSVGElement>>;
  rules: Array<(text: string) => boolean>;
  onWrongInput?: (texts: string[]) => void;
  onSuccessInput: (texts: string[]) => void;
  onChange?: (text: string) => void;
  onEnterKey?: (text: string) => void;
  tabIndex?: number;
}>;

export enum TextState {
  READY = 0,
  WRITING = 1,
  SUCCESS = 2,
  ERROR = 3
}
function validateInput(text: string, rules: Array<(text: string) => boolean>) {
  return !rules.some(rule => !rule(text));
}

function valideState(text: string, rules: ConditionalTextFieldProps["rules"]) {
  const valids: string[] = [],
    invalids: string[] = [];
  const texts = text.split(/\s|,|;|:/);
  texts.forEach(text => {
    if (!text) return;
    if (validateInput(text, rules)) {
      valids.push(text);
    } else {
      invalids.push(text);
    }
  });

  return [invalids, valids];
}
export function ConditionalTextField(props: ConditionalTextFieldProps) {
  const [state, setState] = useState(TextState.READY);
  const {
    placeHolder,
    icon,
    rules,
    onSuccessInput,
    onWrongInput,
    onChange
  } = props;
  useEffect(() => {
    const [invalids, valids] = valideState(props.text, rules);
    if (invalids.length) {
      onWrongInput && onWrongInput(invalids);
      setState(TextState.ERROR);
    } else if (valids.length) {
      setState(TextState.SUCCESS);
      onSuccessInput(valids);
    }
  }, [onSuccessInput, onWrongInput, props.text, rules]);

  const onTextChange = useCallback(
    text => {
      onChange && onChange(text);
    },
    [onChange]
  );

  const onKeyDown = useCallback(
    e => {
      if (e.keyCode === KeyCode.ENTER_KEY && state === TextState.SUCCESS) {
        // valideState(props.text, rules);
        props.onEnterKey && props.onEnterKey(props.text);
        // setState({ ...state, text: "" });
      } else if (e.keyCode === KeyCode.ESC_KEY) {
        setState(TextState.READY);
      } else {
      }
    },
    // eslint-disable-next-line
    [props.onEnterKey]
  );

  return (
    <IconTextField
      classNameModifier={classNames("ConditionalTextField", {
        "ConditionalTextField-error": TextState.ERROR === state,
        "ConditionalTextField-success": TextState.SUCCESS === state
      })}
      text={props.text}
      placeHolder={placeHolder}
      onChange={onTextChange}
      icon={icon}
      onKeyDown={onKeyDown}
      tabIndex={props.tabIndex}
    />
  );
}
