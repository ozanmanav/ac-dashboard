import React, { useState } from "react";
import "./SearchField.stories.scss";
import { SearchField } from "./SearchField";
import { action } from "@storybook/addon-actions";

export default {
  component: SearchField,
  title: "Design System|UI/SearchField",
  parameters: {
    info: {
      text: `
            ~~~js
             <SearchField
              text={text}
              placeHolder="Placeholder"
              onChange={onChange}
             />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const text =
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora esse possimus natus eum aperiam reiciendis minus, perferendis excepturi voluptatum eveniet ad accusamus mollitia corrupti modi iste illum quos, quo quis. Obcaecati voluptates totam, corporis sunt ipsum, quis perspiciatis in pariatur nisi quisquam sequi dolore eos! Ut, sunt. Modi harum mollitia ratione delectus minima, consequatur qui quos debitis maiores quaerat blanditiis?  Minima tempora perferendis commodi tempore natus repudiandae quas eaque, numquam adipisci aliquid aspernatur impedit velit in quod labore animi quidem vitae eos! Excepturi quidem, perspiciatis illo reprehenderit eaque repellendus obcaecati!  Inventore, eum tempore at illum quod vero voluptate hic quaerat doloremque cupiditate. Molestiae, nulla! Dolor id qui voluptatem magni similique neque, voluptas amet, placeat molestiae voluptatum aspernatur illum vitae exercitationem? Fuga eveniet placeat quod, ipsum obcaecati doloribus accusamus animi delectus sed illum. Perspiciatis, tempore? Vitae dolor quae vel asperiores esse assumenda ea beatae tempore. Soluta fugiat autem vel nostrum mollitia. Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora esse possimus natus eum aperiam reiciendis minus, perferendis excepturi voluptatum eveniet ad accusamus mollitia corrupti modi iste illum quos, quo quis. Obcaecati voluptates totam, corporis sunt ipsum, quis perspiciatis in pariatur nisi quisquam sequi dolore eos! Ut, sunt. Modi harum mollitia ratione delectus minima, consequatur qui quos debitis maiores quaerat blanditiis?  Minima tempora perferendis commodi tempore natus repudiandae quas eaque, numquam adipisci aliquid aspernatur impedit velit in quod labore animi quidem vitae eos! Excepturi quidem, perspiciatis illo reprehenderit eaque repellendus obcaecati!  Inventore, eum tempore at illum quod vero voluptate hic quaerat doloremque cupiditate. Molestiae, nulla! Dolor id qui voluptatem magni similique neque, voluptas amet, placeat molestiae voluptatum aspernatur illum vitae exercitationem? Fuga eveniet placeat quod, ipsum obcaecati doloribus accusamus animi delectus sed illum. Perspiciatis, tempore? Vitae dolor quae vel asperiores esse assumenda ea beatae tempore. Soluta fugiat autem vel nostrum mollitia.";

export const SearchFieldWithDefault = () => {
  const [text, setText] = useState<string>("");

  const onChange = (text: string) => {
    setText(text);
  };

  return (
    <SearchField text={text} placeHolder="Placeholder" onChange={onChange} />
  );
};

export const SearchFieldWithAction = () => {
  return (
    <SearchField
      text={""}
      placeHolder="Placeholder"
      onChange={action("onChange")}
    />
  );
};
