import React, { PropsWithChildren, useState, useEffect } from "react";
import classNames from "classnames";

export type TabViewComponentProps<TState = any> = {
  tabName: string;
  onStateChange?: (state: TState, tabName: string) => void;
  state?: TState | undefined;
};
export type TabViewProps = PropsWithChildren<{
  tabs: Array<{
    title: string;
    indicator?: boolean;
    component?: {
      type: string;
      props: TabViewComponentProps;
      key?: any;
    };
  }>;
  selectedIndex?: number;
  onChange?: (index: number) => void;
}>;

export function TabView(props: TabViewProps) {
  // const titles = props.tabs.map(tab => tab.title);
  const [selectedIndex, setSelectedIndex] = useState(props.selectedIndex || 0);
  useEffect(() => {
    props.onChange && props.onChange(selectedIndex);
  }, [selectedIndex]);
  return (
    <div className="TabView">
      <div className="TabView_tabs">
        {props.tabs.map((item, index) => (
          <div
            key={item.title}
            className={classNames("TabView_tabs_tab", {
              "TabView_tabs_tab-selected": selectedIndex === index
            })}
            onClick={(e: any) => {
              selectedIndex !== index && setSelectedIndex(index);
            }}
          >
            <div>
              {item.title}{" "}
              {item.indicator && <div className={"requiredIndicator"}></div>}
            </div>
          </div>
        ))}
      </div>
      <div className={"TabView_content"}>
        {props.tabs[selectedIndex].component}
      </div>
    </div>
  );
}
