import React from "react";
import "./IconButton.stories.scss";
import { IconButton } from "./IconButton";
import { SvgAdd } from "@appcircle/assets/lib/Add";
import { action } from "@storybook/addon-actions";

export default {
  component: IconButton,
  title: "Design System|UI/IconButton",
  parameters: {
    info: {
      text: `
            ~~~js
            <IconButton
              icon={SvgAdd}
              onClick={onClick}
              size={"lg" | "md" | "sm" | "xsm"}
              state={0 | 1}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const IconButtonDefault = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} />;
};

export const IconButtonXSmall = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} size={"xsm"} />;
};

export const IconButtonSmall = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} size={"sm"} />;
};

export const IconButtonMedium = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} size={"md"} />;
};

export const IconButtonLarge = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} size={"lg"} />;
};

export const IconButtonWithDisabledState = () => {
  return <IconButton icon={SvgAdd} onClick={action("onClick")} state={0} />;
};
