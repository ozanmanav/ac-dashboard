import { useEffect, PropsWithChildren } from "react";
import React from "react";
import { checkMousePointerInElement } from "@appcircle/core/lib/utils/checkMousePointerInElement";
import classNames from "classnames";
//

export type DataListItemType = PropsWithChildren<{
  onKillFocus?: () => void;
  onClick?: () => void;
  isTemporary?: boolean;
  isSelected?: boolean;
  excludeKillTarget?: string;
  className?: string;
}>;
export function DataListItem(props: DataListItemType) {
  const rootRef = React.createRef<HTMLDivElement>();

  useEffect(() => {
    if (!props.isTemporary) return;
    const clickHandler = (e: MouseEvent) => {
      if (
        (rootRef.current &&
          checkMousePointerInElement(
            e.clientX,
            e.clientY,
            rootRef.current.getBoundingClientRect()
          )) ||
        (props.excludeKillTarget &&
          checkIsInProfileDetailPage(
            e.target as HTMLElement,
            props.excludeKillTarget
          ))
      )
        return;
      props.onKillFocus && props.onKillFocus();
    };
    document.addEventListener("click", clickHandler);
    return () => {
      setTimeout(
        () => document.removeEventListener("click", clickHandler),
        300
      );
    };
  }, [props, rootRef]);
  return (
    <div
      ref={rootRef}
      className={classNames("DataListItem", props.className, {
        "DataListItem-selected": props.isSelected
      })}
      onClick={() => {
        props.onClick && props.onClick();
      }}
    >
      {props.children}
    </div>
  );
}

function checkIsInProfileDetailPage(
  target: HTMLElement,
  excludeClassName: string
): boolean {
  if (target.classList.contains(excludeClassName)) return true;
  if (target.parentElement)
    return checkIsInProfileDetailPage(target.parentElement, excludeClassName);
  return false;
}
