import { PropsWithChildren } from "react";
import React from "react";
type DataListProps = PropsWithChildren<{}>;
export function DataList(props: DataListProps) {
  return (
    <div className="DataList">
      <div className="DataList_header">{/*props.header*/}</div>
      <div className="DataList_items" data-simplebar>
        <div>{props.children}</div>
      </div>
    </div>
  );
}
