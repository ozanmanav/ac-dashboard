import React, { PropsWithChildren, FunctionComponent, SVGProps } from "react";
import classNames from "classnames";

export type SwitchButtonProps = PropsWithChildren<{
  items: Array<FunctionComponent<SVGProps<SVGSVGElement>>>;
  selectedIndex: number;
  onChange?: (index: number) => void;
}>;

export function SwitchButtonList(props: SwitchButtonProps) {
  return (
    <div className="SwitchButton">
      {props.items.map(
        (item: FunctionComponent<SVGProps<SVGSVGElement>>, index: number) => {
          const Icon = item;
          return (
            <div
              className={classNames("SwitchButton_item", {
                "SwitchButton_item-selected": index === props.selectedIndex
              })}
              key={index}
              onClick={() => {
                props.selectedIndex !== index &&
                  props.onChange &&
                  props.onChange(index);
              }}
            >
              <Icon className="SwitchButton_item_icon" />
            </div>
          );
        }
      )}
    </div>
  );
}
