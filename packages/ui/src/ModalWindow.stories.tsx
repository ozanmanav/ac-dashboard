import React from "react";
import "./ModalWindow.stories.scss";
import { ModalWindow } from "./ModalWindow";
import { action } from "@storybook/addon-actions";

export default {
  component: ModalWindow,
  title: "Design System|UI/ModalWindow",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative",
          height: "500px"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js

            <ModalWindow
                header="Modal Window"
                onModalClose={onModalClose}
                onBeforeModalClose={onBeforeModalClose}
                onBackButton={onBackButton}
                showBackButton={true}
            >
                <Inner Component />
            </ModalWindow>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ModalWindowDefault = () => {
  return (
    <ModalWindow
      header="Modal Window"
      onModalClose={action("onModalClose")}
      onBeforeModalClose={() => {
        action("onBeforeModalClose");
        return true;
      }}
      onBackButton={action("onBackButton")}
    >
      <div className="innerComponent">Inner Component</div>
    </ModalWindow>
  );
};

export const ModalWindowWithBackButton = () => {
  return (
    <ModalWindow
      header="Modal Window"
      onModalClose={action("onModalClose")}
      onBeforeModalClose={() => {
        action("onBeforeModalClose");
        return true;
      }}
      onBackButton={action("onBackButton")}
      showBackButton={true}
    >
      <div className="innerComponent">Inner Component</div>
    </ModalWindow>
  );
};

export const ModalWindowWithSpinner = () => {
  return (
    <ModalWindow
      header="Modal Window"
      onModalClose={action("onModalClose")}
      onBeforeModalClose={() => {
        action("onBeforeModalClose");
        return true;
      }}
      onBackButton={action("onBackButton")}
      spinner={true}
    >
      <div className="innerComponent">Inner Component</div>
    </ModalWindow>
  );
};
