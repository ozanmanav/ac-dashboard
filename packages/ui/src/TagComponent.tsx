import React, { PropsWithChildren, FunctionComponent, SVGProps } from "react";
import classNames from "classnames";
import { SvgCloseTag } from "@appcircle/assets/lib/CloseTag";

export type TagComponentProps = PropsWithChildren<{
  id?: any;
  icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
  text: string;
  className?: string;
  onClick?: () => void;
  onClose?: (id: any) => void;
  modifiable?: boolean;
  isDisabled?: boolean;
}>;

export function TagComponent(props: TagComponentProps) {
  const Icon = props.icon;
  return (
    <div
      className={classNames("TagComponent", props.className, {
        "TagComponent-disabled": props.isDisabled
      })}
      onClick={props.onClick}
    >
      {Icon ? <Icon className="TagComponent_icon" /> : null}
      <span>{props.text}</span>
      {props.modifiable && props.onClose ? (
        <SvgCloseTag
          className="TagComponent_close"
          onClick={() => props.onClose && props.onClose(props.id)}
        />
      ) : null}
    </div>
  );
}
