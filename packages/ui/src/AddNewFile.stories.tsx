import React from "react";

import "./AddNewFile.stories.scss";
import { AddNewFile } from "./AddNewFile";
import { action } from "@storybook/addon-actions";
import { IntlProvider } from "react-intl";

import { SvgAdd } from "@appcircle/assets/lib/Add";
const types: string[] = ["jpg", "gif", "png"];
const text: string = "Add New File";
const progress: number = 75;

export default {
  component: AddNewFile,
  title: "Design System|UI/AddNewFile",
  parameters: {
    info: {
      text: `
            ~~~js
            const types: string[] = ["jpg", "gif", "png"];

            <AddNewFile
              types={types}
              text={"${text}"}
              onFileUpload={action("onFileUpload")}
              onStateChange={action("onStateChange")}
              icons={[SvgAdd]}
              status={"idle" | "file-selected" | "uploading" | "error" | "invalid" | "dragging" | "processing"}
              autoUpload={true | false}
              disabled={true | false}
              progress={number} // ${progress} is used in example.
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const AddNewFileIdle = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
      />
    </IntlProvider>
  );
};

export const AddNewFileIdleWithNoAutoUpload = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        autoUpload={false}
        icons={[SvgAdd]}
      />
    </IntlProvider>
  );
};

export const AddNewFileSelected = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"file-selected"}
      />
    </IntlProvider>
  );
};

export const AddNewFileUploading = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"uploading"}
      />
    </IntlProvider>
  );
};

export const AddNewFileUploadingWithProgress = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"uploading"}
        progress={progress}
      />
    </IntlProvider>
  );
};

export const AddNewFileError = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"error"}
      />
    </IntlProvider>
  );
};

export const AddNewFileInvalid = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"invalid"}
      />
    </IntlProvider>
  );
};

export const AddNewFileDragging = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"dragging"}
      />
    </IntlProvider>
  );
};

export const AddNewFileProcessing = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={types}
        text={text}
        onFileUpload={action("onFileUpload")}
        onStateChange={action("onStateChange")}
        icons={[SvgAdd]}
        status={"processing"}
      />
    </IntlProvider>
  );
};

export const AddNewFileDisabled = () => {
  return (
    <IntlProvider locale="en">
      <AddNewFile
        types={[]}
        text={text}
        onFileUpload={action("onFileUpload")}
        icons={[SvgAdd]}
        disabled={true}
      />
    </IntlProvider>
  );
};
