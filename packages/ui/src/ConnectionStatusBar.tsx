import React, { FunctionComponent, useEffect, useState } from "react";
import classNames from "classnames";

const animationTimeout = 750;

type IToastProps = {
  statusText?: string;
  isDisconnected?: boolean;
};

const ConnectionStatusBar: FunctionComponent<IToastProps> = ({
  statusText = "",
  isDisconnected = false
}) => {
  const [isFadedIn, setIsFadedIn] = useState<boolean>(false);

  useEffect(() => {
    if (isDisconnected) {
      setIsFadedIn(true);
    } else {
      setTimeout(() => setIsFadedIn(false), animationTimeout);
    }
  }, [isDisconnected]);
  return (
    <div
      className={classNames("ConnectionStatusBar", {
        "ConnectionStatusBar-fadeIn": isFadedIn,
        "ConnectionStatusBar-onlineStatus": !isDisconnected,
        "ConnectionStatusBar-hide": !isFadedIn && !isDisconnected
      })}
    >
      {statusText}
    </div>
  );
};

export default ConnectionStatusBar;
