import {
  TaskPropsType,
  useTaskManager,
  TaskFactoryFn
} from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ComponentType, useState, useCallback } from "react";
import React from "react";
import { Spinner } from "./Spinner";
import { ProgressView } from "./ProgressView";

export enum LoadableContentStatus {
  IDLE,
  LOADED,
  IN_PROGRESS,
  ERROR
}
export function withLoadableContent<TComp = {}, TPreload = {}, TError = {}>(
  Component: ComponentType<{
    onRunTask: (task: () => [TaskFactoryFn<any>, TaskPropsType<any>]) => void;
  }>,
  PreloaderComponent: ComponentType<any> = Spinner, // set default component if it exists
  ErrorComponent?: ComponentType<any>
) {
  return (props: TComp) => {
    const [status, setStatus] = useState<LoadableContentStatus>(
      LoadableContentStatus.IDLE
    );
    const { addTask, createApiTask } = useTaskManager();
    const onRunTask = useCallback(
      (task: () => [TaskFactoryFn<any>, TaskPropsType<any>]) => {
        setStatus(LoadableContentStatus.IN_PROGRESS);
        addTask(createApiTask(...task()));
      },
      [addTask, createApiTask]
    );
    return (
      <div className="LoadableContent">
        {ErrorComponent && status === LoadableContentStatus.ERROR ? (
          <ErrorComponent />
        ) : (
          <ProgressView
            indicatorComp={<PreloaderComponent />}
            isInProgress={LoadableContentStatus.IN_PROGRESS === status}
          >
            <Component onRunTask={onRunTask} />
          </ProgressView>
        )}
      </div>
    );
  };
}
