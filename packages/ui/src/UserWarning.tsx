import React, { PropsWithChildren, Children } from "react";
import { SvgWarning } from "@appcircle/assets/lib/Warning";

export function UserWarning(props: PropsWithChildren<{ messages?: string[] }>) {
  return Children.count(props.children) > 0 ||
    (props.messages && props.messages.length > 0) ? (
    <div className="UserWarning">
      <div>
        <SvgWarning />
      </div>
      {props.messages && props.messages.length > 0 ? (
        <div>
          {props.messages.map((message, index) => (
            <div className="UserWarning_message" key={index}>
              {message}
            </div>
          ))}
        </div>
      ) : (
        <div>{props.children}</div>
      )}
    </div>
  ) : (
    <React.Fragment></React.Fragment>
  );
}
