import React, { PropsWithChildren, useMemo, CSSProperties } from "react";

type Props = PropsWithChildren<{
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
  // position: "bottom" | "top" | "right" | "left";
}>;
export function ToolTip(props: Props) {
  const style = useMemo<CSSProperties>(() => {
    return {
      left: props.left,
      top: props.top,
      bottom: props.bottom,
      right: props.right
    };
  }, [props.left, props.right, props.bottom, props.top]);
  return props.children ? (
    <div className={"ToolTip"} style={style}>
      <div>{props.children}</div>
    </div>
  ) : null;
}
