import React, {
  PropsWithChildren,
  useState,
  useRef,
  useCallback,
  useEffect,
  EventHandler,
  FormEvent,
  useMemo
} from "react";
import classNames from "classnames";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { ScrollView } from "./ScrollView";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";

export type EditableTextViewProps = PropsWithChildren<{
  value?: string | number | null | undefined;
  className?: string;
  onChange?: (text: string) => void;
  onFocus?: () => void;
  onBlur?: (
    text?: string,
    keyCode?: KeyCode.ENTER_KEY | KeyCode.ESC_KEY
  ) => void;
  onKeyEnter?: (text?: string) => void;
  editable?: boolean;
  placeHolder?: string;
  tabIndex?: number;
  multiline?: boolean;
  maxHeight?: "auto" | number;
  activated?: boolean;
}>;

export function EditableTextView(props: EditableTextViewProps) {
  const [text, setText] = useState(props.value);
  const [active, setActive] = useState(false);
  const [invalid, setInvalid] = useState(false);
  const activated = active && props.activated !== false;
  // const [editable, setEditable] = useState(false);
  const editable = props.editable === undefined ? false : props.editable;
  const elemRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    elemRef.current && text !== props.value && setText(props.value ?? "");
  }, [props.value]);

  useEffect(() => {
    elemRef.current &&
      elemRef.current.innerText !== text &&
      (elemRef.current.innerText = text as string);
  }, [text]);

  function activate(active: boolean, keyCode?: number) {
    editable &&
      elemRef.current &&
      (elemRef.current.contentEditable = active ? "true" : "false");
    active
      ? props.onFocus && props.onFocus()
      : props.onBlur &&
        elemRef.current &&
        props.onBlur(elemRef.current.innerText, keyCode);
    setActive(active);
    setInvalid(false);
  }

  useGlobalClickService(
    null,
    elemRef,
    useCallback(() => {
      active &&
        setTimeout(() => {
          activate(false);
        }, 1);
    }, [active, invalid])
  );

  useEffect(() => {
    !activated &&
      active &&
      elemRef.current &&
      (() => {
        elemRef.current.blur();
        activate(false);
      })();
  }, [activated]);

  return (
    <div
      className={
        "EditableTextView EditableTextView-multiline--" +
        !!props.multiline +
        "EditableTextView-active--" +
        !!active
      }
      style={useMemo(
        () => ({
          maxHeight: props.maxHeight
        }),
        [props.maxHeight]
      )}
    >
      <ScrollView>
        <div
          ref={elemRef}
          tabIndex={props.tabIndex || -1}
          className={classNames(
            "EditableTextView_textArea",
            props.className,
            "EditableTextView_textArea-active--" + !!active,
            "EditableTextView_textArea-multiline--" + !!props.multiline
          )}
          spellCheck={false}
          style={useMemo(
            () => ({
              height: props.multiline ? "auto" : ""
            }),
            [props.multiline]
          )}
          onClick={e => {
            !active && activate(true);
            elemRef.current && elemRef.current.focus();
          }}
          onFocus={() => {
            !active && activate(true);
            elemRef.current && elemRef.current.focus();
          }}
          // onBlurCapture={() => {
          //   !active && activate(false);
          //   props.onBlur && props.onBlur();
          // }}
          data-placeholder={props.placeHolder}
          suppressContentEditableWarning={true}
          onPaste={(event: React.ClipboardEvent) => {
            event.clipboardData.setData(
              event.clipboardData.types[0],
              event.clipboardData.getData(event.clipboardData.types[0])
            );
            let data = event.clipboardData.getData(
              event.clipboardData.types[0]
            );
            // remove useless whitespaces if no multiline.
            props.multiline !== true &&
              (data = data.replace(/[\r\n\t]*/gm, ""));
            // elemRef.current && (elemRef.current.innerText = data);
            document.execCommand("inserttext", false, data);
            event.preventDefault();
          }}
          onInput={useCallback<EventHandler<FormEvent<HTMLDivElement>>>(
            e => {
              setInvalid(true);
              props.onChange && props.onChange(e.currentTarget.innerText);
              // setText(e.currentTarget.innerText);
            },
            [props.onChange]
          )}
          onKeyDown={e => {
            if (props.multiline !== true && e.keyCode === KeyCode.ENTER_KEY) {
              e.preventDefault();
              activate(false, KeyCode.ENTER_KEY);
            } else if (e.keyCode === KeyCode.ESC_KEY) {
              e.preventDefault();
              activate(false, KeyCode.ESC_KEY);
            }
            e.stopPropagation();
            e.defaultPrevented = true;
          }}
        ></div>
      </ScrollView>
    </div>
  );
}
