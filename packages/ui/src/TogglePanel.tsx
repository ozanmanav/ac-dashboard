import React, { PropsWithChildren, useState, useEffect } from "react";
import classNames from "classnames";
import { SvgToggleClose } from "@appcircle/assets/lib/ToggleClose";

export type TogglePanelProps = PropsWithChildren<{
  title: string;
  clickableTitle?: boolean;
  className?: string;
  onChange?: (isSelected: boolean) => void;
  isSelected?: boolean;
  isDisabled?: boolean;
}>;

// const ToggleClose = icons.toggleClose;

export function TogglePanel(props: TogglePanelProps) {
  const [isSelected, setIsSelected] = useState(props.isSelected || false);

  useEffect(() => {
    props.onChange && props.onChange(isSelected);
    // eslint-disable-next-line
  }, [isSelected]);

  return (
    <div
      className={classNames("Toggle", props.className, {
        "Toggle-disabled": props.isDisabled
      })}
    >
      <div
        className="Toggle_header"
        onClick={() => {
          setIsSelected(!isSelected);
        }}
      >
        <div className="Toggle_header_title">{props.title}</div>
        <div
          className={classNames("Toggle_header_closeIcon", {
            "Toggle_header_closeIcon-up": !isSelected,
            "Toggle_header_closeicon-down": isSelected
          })}
        >
          <SvgToggleClose />
        </div>
      </div>
      {isSelected ? props.children : null}
    </div>
  );
}
