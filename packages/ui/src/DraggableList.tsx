import React, { useRef, useCallback, useState, useEffect } from "react";
import { useDrag, useDrop, DropTargetMonitor } from "react-dnd";
import { XYCoord } from "dnd-core";
import produce from "immer";
import { SvgArrow } from "@appcircle/assets/lib/Arrow";

const style = {};

type DraggableListItemProps = {
  id: any;
  text: string;
  index: number;
  moveCard: (dragIndex: number, hoverIndex: number) => void;
  onDragEnd: (index: number) => void;
  onSelect: (index: number) => void;
  description?: string;
  disabled?: boolean;
};

type DragItem = {
  index: number;
  id: string;
  type: string;
};

function DraggableListItem(props: DraggableListItemProps) {
  const ref = useRef<HTMLDivElement>(null);
  const disabled = props.disabled;

  const [, drop] = useDrop({
    accept: "DraggableListItem",
    drop(item: DragItem, monitor: DropTargetMonitor) {
      props.onDragEnd(item.index);
    },
    hover(item: DragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = props.index;

      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current!.getBoundingClientRect();

      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      // Time to actually perform the action
      //
      props.moveCard(dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex;
    }
  });

  const [{ isDragging }, drag] = useDrag({
    item: { type: "DraggableListItem", id: props.id, index: props.index },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging()
    }),
    end: () => {
      props.onDragEnd(props.index);
    }
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));
  return (
    <div
      ref={ref}
      className={
        "DraggableListItem DraggableListItem-dragging--" +
        !!isDragging +
        " DraggableListItem-disabled--" +
        !!disabled
      }
      style={{ ...style, opacity }}
      onClick={() => !isDragging && props.onSelect(props.index)}
    >
      <div className="DraggableListItem_hamburger">&#9776;</div>
      <div>
        <div className="DraggableListItem_title">{props.text}</div>
        <div className="DraggableListItem_description">{props.description}</div>
      </div>
      <div className="DraggableListItem_arrow">
        <SvgArrow />
      </div>
    </div>
  );
}

export type ListItemDataType = {
  id: number;
  title: string;
  description: string;
  disabled?: boolean;
};

export type ContainerState = {
  items: ListItemDataType[];
};

type DraggableListProps = {
  items: ListItemDataType[];
  onSelect: DraggableListItemProps["onDragEnd"];
  onChange: (newItems: ListItemDataType[]) => void;
};

export function DraggableList(props: DraggableListProps) {
  const [items, setItems] = useState(props.items);
  useEffect(() => {
    setItems(props.items);
  }, [props.items]);

  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = items[dragIndex];
      const newItems = produce(items, draft => {
        draft.splice(dragIndex, 1);
        draft.splice(hoverIndex, 0, dragCard);
      });
      setItems(newItems);
    },
    [items]
  );

  const renderItem = (item: ListItemDataType, index: number) => {
    return (
      <DraggableListItem
        key={item.id}
        index={index}
        id={item.id}
        text={item.title}
        moveCard={moveCard}
        onDragEnd={() => {
          props.onChange(items);
        }}
        onSelect={index => {
          props.onSelect(index);
        }}
        description={item.description}
        disabled={item.disabled}
      />
    );
  };

  return (
    <div className="DraggableList">
      {items.map((item, i) => renderItem(item, i))}
    </div>
  );
}
