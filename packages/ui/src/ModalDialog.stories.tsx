import React from "react";
import "./ModalDialog.stories.scss";
import { ModalDialog } from "./ModalDialog";
import { action } from "@storybook/addon-actions";

export default {
  component: ModalDialog,
  title: "Design System|UI/ModalDialog",
  parameters: {
    info: {
      text: `
            ~~~js

            <ModalDialog header="Modal Dialog" onModalClose={onModalClose}>
              <InnerComponent />
            </ModalDialog>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ModalDialogComponent = () => {
  return (
    <ModalDialog header="Modal Dialog" onModalClose={action("onModalClose")}>
      <div className="innerComponent">Inner Component</div>
    </ModalDialog>
  );
};
