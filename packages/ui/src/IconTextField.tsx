import React, {
  PropsWithChildren,
  FunctionComponent,
  SVGProps,
  useState,
  useCallback,
  useRef,
  useEffect
} from "react";
import classNames from "classnames";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";

export type IconTextFieldProps = PropsWithChildren<{
  text?: string;
  placeHolder: string;
  onChange: (text: string) => void;
  icon: FunctionComponent<SVGProps<SVGSVGElement>>;
  classNameModifier?: string;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  tabIndex?: number;
}>;

export function IconTextField(props: IconTextFieldProps) {
  const { icon: Icon, classNameModifier } = props;
  const [text, setText] = useState(props.text || "");
  const ref = useRef<HTMLInputElement>(null);
  const onChange = useCallback(
    e => {
      setText(e.target.value);
      props.onChange(e.target.value);
    },
    [props, text]
  );
  useEffect(() => {
    if (text !== props.text && props.text !== undefined)
      setText(props.text || "");
  }, [props.text]);
  const onKeyDown = useCallback(
    e => {
      e.stopPropagation();
      if (e.keyCode === KeyCode.ESC_KEY) {
        props.onChange("");
        setText("");
      }
      props.onKeyDown && props.onKeyDown(e);
    },
    [props]
  );
  return (
    <div className={classNames("IconTextField", classNameModifier)}>
      <Icon className="IconTextField_icon" />
      <input
        ref={ref}
        tabIndex={props.tabIndex}
        type="text"
        className="IconTextField_input"
        spellCheck={false}
        value={text}
        placeholder={props.placeHolder}
        onChange={onChange}
        onKeyDown={onKeyDown}
      />
    </div>
  );
}
