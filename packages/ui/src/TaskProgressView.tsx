import { ReactNode, useCallback, useMemo } from "react";
import { ProgressView, ProgressViewProps } from "./ProgressView";
import React from "react";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { TaskStatusType } from "@appcircle/taskmanager/lib/TaskStatusType";
import { ConnectTaskProps } from "./taskmanager/ConnectTask";

export type TaskSpinnerProgressViewProps = {
  progressText?: string;
  entityID?: string | string[] | undefined;
  taskID?: string;
  errorComponent?: ConnectTaskProps["errorComponent"];
  hasRelativeRoot?: boolean;
  isOverlay?: boolean;
  progressType?: ProgressViewProps["type"];
  children?: ((status: TaskStatusType | undefined) => ReactNode) | ReactNode;
  tasks?: TaskStatusType[];
  onClick?: () => void;
};
export function TaskProgressView(props: TaskSpinnerProgressViewProps) {
  const isOverlay = props.isOverlay === undefined ? true : props.isOverlay;
  const children = useCallback(
    task =>
      typeof props.children === "function"
        ? props.children(task)
        : props.children,
    [props]
  );
  const taskmanager = useTaskManager();
  const tasks = useMemo<TaskStatusType[]>(() => {
    return (
      props.tasks || taskmanager.getActiveTasksByEntityId(props.entityID || [])
    );
  }, [taskmanager, props.entityID, props.tasks]);

  const isInProgress = tasks.length > 0;

  const progressComp = useMemo(() => {
    return !props.hasRelativeRoot ? (
      <div
        style={{
          position: "relative",
          overflow: "auto",
          display: "auto",
          height: "inherit"
        }}
      >
        <ProgressView
          isOverlay={isOverlay}
          isInProgress={isInProgress}
          progressText={props.progressText}
          type={props.progressType}
          onClick={props.onClick}
        >
          {children(tasks[0])}
        </ProgressView>
      </div>
    ) : (
      <ProgressView
        isOverlay={isOverlay}
        isInProgress={isInProgress}
        progressText={props.progressText}
        type={props.progressType}
        onClick={props.onClick}
      >
        {children(tasks[0])}
      </ProgressView>
    );
  }, [isOverlay, props, children, isInProgress, tasks]);
  return !props.entityID && !props.taskID ? (
    <React.Fragment>{children(undefined)}</React.Fragment>
  ) : (
    progressComp
  );
}
