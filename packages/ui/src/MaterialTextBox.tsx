import React, { useState, FormEvent, PropsWithChildren } from "react";
import TextField, { Input } from "@material/react-text-field";
import { FormElementType } from "@appcircle/form/lib/Form";
import { SvgEye } from "@appcircle/assets/lib/Eye";
import { SvgEyeOff } from "@appcircle/assets/lib/EyeOff";

// const EyeIcon = icons.eye;
// const EyeOffIcon = icons.eyeOff;

export type MaterialTextBoxProps = PropsWithChildren<{
  label: string;
  autoComplete?: "on" | "off";
  text?: string;
  type?: string;
  isValid?: boolean;
  tabIndex?: number;
  ariaLabel?: string;
}> &
  FormElementType<string>;

export function MaterialTextBox(props: MaterialTextBoxProps) {
  const [text, setText] = useState(props.text || "");
  const [type, setType] = useState(props.type || "text");
  return (
    <TextField
      autoComplete={props.autoComplete}
      label={props.label}
      className="MaterialTextBox"
      disabled={props.isDisabled}
      aria-label={props.ariaLabel}
      onTrailingIconSelect={
        props.type === "password"
          ? () => {
              setType(type === "text" ? "password" : "text");
            }
          : undefined
      }
      trailingIcon={
        props.type === "password" ? (
          type === "password" ? (
            <SvgEye />
          ) : (
            <SvgEyeOff />
          )
        ) : (
          undefined
        )
      }
    >
      <Input
        tabIndex={props.tabIndex}
        autoComplete={props.autoComplete}
        isValid={props.isValid}
        type={type}
        disabled={props.isDisabled}
        spellCheck={false}
        value={text}
        id={props.label}
        onChange={(e: FormEvent<HTMLInputElement>) => {
          setText(e.currentTarget.value);
          props.onChange && props.onChange(e.currentTarget.value);
        }}
        onBlur={(e: FormEvent<HTMLInputElement>) => {
          setText(e.currentTarget.value);
          props.onBlur && props.onBlur(e.currentTarget.value);
        }}
      />
    </TextField>
  );
}
