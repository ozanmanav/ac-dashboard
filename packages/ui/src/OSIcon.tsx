import React, { useMemo } from "react";
import { OS } from "@appcircle/core/lib/enums/OS";
import classNames from "classnames";
import { SvgApple } from "@appcircle/assets/lib/Apple";
import { SvgAndroid } from "@appcircle/assets/lib/Android";
type OSIconProps = {
  os: OS;
  className?: string;
  display?: "appIcon" | "small" | "tiny";
  style?: "dark" | "light";
};

export function OSIcon(props: OSIconProps) {
  const className = classNames(
    "OSIcon",
    props.className,
    props.display && "OSIcon-" + props.display,
    "OSIcon-" + (props.style || "dark")
  );
  const Icon = useMemo(() => {
    return props.os === OS.ios ? (
      <SvgApple className={className} viewBox="0 0 14 16" />
    ) : (
      <SvgAndroid className={className} viewBox="0 0 17 19" />
    );
  }, [props.os, className]);
  return Icon;
}
