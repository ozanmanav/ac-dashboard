import React from "react";
import "./HiddenDots.stories.scss";
import { HiddenDots } from "./HiddenDots";

export default {
  component: HiddenDots,
  title: "Design System|UI/HiddenDots",
  parameters: {
    info: {
      text: `
            ~~~js
            <HiddenDots />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const HiddenDotsComponent = () => {
  return <HiddenDots />;
};
