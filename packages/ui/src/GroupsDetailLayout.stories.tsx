import React, { useState } from "react";
import "./GroupsDetailLayout.stories.scss";
import { GroupsDetailLayout } from "./GroupsDetailLayout";
import { action } from "@storybook/addon-actions";

export default {
  component: GroupsDetailLayout,
  title: "Design System|UI/GroupsDetailLayout",
  parameters: {
    info: {
      text: `
            ~~~js
            <GroupsDetailLayout
                mobileMode={false}
                sideBar={<div>Sidebar Content Component</div>}
                detail={<div>Detail Content Component</div>}
                title="Groups Detail Layout"
                onMobileModeChange={action("onMobileModeChange")}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};
// TODO: sidebar and details props need react component
export const GroupsDetailLayoutPure = () => {
  return (
    <GroupsDetailLayout
      mobileMode={false}
      sideBar={<div>Sidebar Content Component</div>}
      detail={<div>Detail Content Component</div>}
      title="Groups Detail Layout"
      onMobileModeChange={action("onMobileModeChange")}
    />
  );
};
