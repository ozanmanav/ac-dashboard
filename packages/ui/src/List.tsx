import React, { useEffect, useState, useRef, forwardRef } from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import { ListItemsType, ListItemType } from "./ListItem";
import { ScrollView } from "./ScrollView";

type ListProps = {
  data: ListItemsType;
  onItemSelect: (item: ListItemType, index: number) => void;
  onBlur?: () => void;
  isOpened: boolean;
};
export const List = forwardRef<HTMLDivElement, ListProps>((props, ref) => {
  const target = useRef<HTMLUListElement>(null);
  const [selectedItem, setSelectedItem] = useState(-1);

  useEffect(() => {
    if (!props.isOpened) return;
    let index = selectedItem;
    const handler = (e: KeyboardEvent) => {
      switch (e.keyCode) {
        case KeyCode.ESC_KEY:
          e.preventDefault();
          e.stopImmediatePropagation();
          props.onBlur && props.onBlur();
          break;
        case KeyCode.ENTER_KEY:
          e.preventDefault();
          props.data[index] && props.onItemSelect(props.data[index], index);
          break;
        case KeyCode.DOWN_ARROW:
          e.preventDefault();
          index++;
          break;
        case KeyCode.UP_ARROW:
          e.preventDefault();
          index--;
          break;
        default:
          break;
      }
      if (target.current) {
        // index = Math.min(Math.min(target.current.children.length, index), -1);
        // index = target.current.children.length - 1;

        if (target.current.children.length < index) index = 0;
        else if (-1 > index) index = target.current.children.length - 1;
        Math.max(target.current.children.length - 1, 0);
        setSelectedItem(index);
      }
    };
    document.body.addEventListener("keydown", handler);
    return () => document.body.removeEventListener("keydown", handler);
  }, [props.onItemSelect, props.isOpened]);

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     target.current && ;
  //   }, 25);
  // }, [])

  // target.current && console.log(, target.current.offsetParent && target.current.offsetParent.scrollTop)

  return (
    <div
      ref={ref}
      className={
        "List List-isOpened--" +
        (props.isOpened === undefined || props.isOpened) +
        " List-noItem--" +
        !!!props.data.length
      }
    >
      <ScrollView>
        <ul ref={target}>
          {props.data.map((item, index) => {
            return (
              <li
                className={selectedItem === index ? "List_item-selected" : ""}
                key={item.value}
                onClick={e => {
                  e.stopPropagation();
                  props.onItemSelect(item, index);
                  setSelectedItem(index);
                }}
              >
                <span>{item.label}</span>
              </li>
            );
          })}
        </ul>
      </ScrollView>
    </div>
  );
});
