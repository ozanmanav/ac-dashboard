import React, { useState, useRef, useMemo } from "react";
import { TaggableTextArea, TaggableTextAreaProps } from "./TaggableTextArea";
import classNames from "classnames";
import { TagComponents, Tag } from "./Tags";
import { MessageDescriptor } from "react-intl";

export type TaggableTextFieldErrors = {
  duplicate?: string | MessageDescriptor;
  match?: string | MessageDescriptor;
};
export type TaggableTextFieldProps = {
  value?: Tag[];
  suggestions?: string[];
  onChange?: (tag: Tag[]) => void;
  deleteTag: (id: any) => void;
  matchRule?: (tagText: string) => boolean;
  allowDuplicates?: boolean;
  placeHolder?: string;
  separator?: RegExp;
  errors?: TaggableTextFieldErrors;
  className?: string;
  onError?: TaggableTextAreaProps["onError"];
  text?: string;
  onTextChange?: (text?: string) => void;
  autofocus?: boolean;
};

export function TaggableTextField(props: TaggableTextFieldProps) {
  const [isTagInvalid, setTagInvalid] = useState(false);
  let containerRef = useRef<HTMLDivElement>(null);

  return (
    <div className="TaggableTextField" ref={containerRef}>
      <div
        className={classNames(
          "TaggableTextField_child",
          {
            "TaggableTextField-error": isTagInvalid,
            "TaggableTextField-isEmpty": !(props.value && props.value.length)
          },
          props.className
        )}
      >
        <TagComponents
          onDelete={props.deleteTag}
          showCloseIcon={true}
          tags={props.value}
        />
        <TaggableTextArea
          {...props}
          autofocus={props.autofocus}
          blurIgnoreList={useMemo(() => ["TaggableTextField_child"], [])}
          tags={props.value}
          onTextChange={props.onTextChange}
          value={props.text}
          parentRef={containerRef}
          setTagInvalid={setTagInvalid}
          placeHolder={
            props.value && props.value.length ? "" : props.placeHolder
          }
          isTagInvalid={isTagInvalid}
        />
      </div>
    </div>
  );
}
