import React, {
  useState,
  useCallback,
  useEffect,
  useRef,
  Ref,
  RefObject
} from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import { Tag } from "./Tags";
import shortid from "shortid";
import { TaggableTextFieldErrors } from "./TaggableTextField";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";

const lineHeight = 22;
export type TaggableTextAreaProps = {
  autofocus?: boolean;
  tags?: Tag[];
  allowDuplicates?: boolean;
  isTagInvalid: boolean;
  placeHolder?: string;
  separator?: RegExp;
  errors?: TaggableTextFieldErrors;
  onBlur?: () => void;
  onFocus?: () => void;
  onChange?: (tag: Tag[]) => void;
  deleteTag: (id: any) => void;
  matchRule?: (tagText: string) => boolean;
  setTagInvalid: (val: boolean) => void;
  value?: string;
  onTextChange?: (val?: string) => void;
  onRef?: (ref: HTMLTextAreaElement | null) => void;
  parentRef?: RefObject<HTMLElement>;
  onError?: (error: any) => void;
  blurIgnoreList?: string[];
};

export const TaggableTextArea = (props: TaggableTextAreaProps) => {
  const target = useRef<HTMLTextAreaElement>(null);
  const parentRef = useRef<HTMLDivElement>(null);
  const { setTagInvalid, deleteTag, onChange, matchRule } = props;
  const { pushMessage } = useNotificationsService();
  const separator = props.separator || /,/g;
  const focusTextAreaClickHandler = useCallback(
    () => target.current && target.current.focus(),
    [target]
  );
  const [canAdd, setCanAdd] = useState(true);

  useEffect(() => {
    props.parentRef &&
      props.parentRef.current &&
      props.parentRef.current.addEventListener(
        "click",
        focusTextAreaClickHandler
      );
    return () => {
      props.parentRef &&
        props.parentRef.current &&
        props.parentRef.current.removeEventListener(
          "click",
          focusTextAreaClickHandler
        );
    };
  }, [props, focusTextAreaClickHandler]);
  const [textValue, setTextValue] = useState("");

  const [rows, setRows] = useState(1);

  const initiateAddTagSequence = useCallback(
    addTagSequence(
      target,
      separator,
      matchRule,
      props,
      setTagInvalid,
      props.onChange
    ),
    [
      props.onChange,
      matchRule,
      props.allowDuplicates,
      props.errors,
      props.tags,
      pushMessage,
      separator,
      setTagInvalid
    ]
  );

  const calculateSizeByValue = useCallback(
    (value: string) => {
      const textArea = target.current;
      if (!textArea) return value;
      let textAreaFontSize = (
        getComputedStyle(textArea).fontSize || "0"
      ).replace(/[^0-9]/g, "");

      textArea.style.width =
        String(value.length * 10 + parseInt(textAreaFontSize)) + "px";

      const oldRows = textArea.rows;
      textArea.rows = 1;
      const newRows = ~~(textArea.scrollHeight / lineHeight);
      if (newRows === oldRows) {
        textArea.rows = newRows;
      }

      if (!!value.match(separator) && canAdd) {
        value = initiateAddTagSequence();
        setRows(newRows);
      }

      return value;
    },
    [canAdd, initiateAddTagSequence, separator]
  );
  useEffect(() => {
    props.value && setTextValue(calculateSizeByValue(props.value));
  }, [props.value, calculateSizeByValue]);

  const [blurred, setBlurred] = useState(true);

  useEffect(() => {
    if (props.value && blurred) {
      setTextValue(initiateAddTagSequence(props.value));
      props.onTextChange && props.onTextChange(undefined);
    }
  }, [props.value, blurred]);

  function onOutSideClick() {
    setBlurred(true);
    props.onBlur && props.onBlur();
  }
  useEffect(() => {
    props.autofocus === true &&
      target.current &&
      setTimeout(() => target.current && target.current.focus(), 1);
  }, [target]);

  useGlobalClickService(props.blurIgnoreList || [], parentRef, onOutSideClick);

  return (
    <div
      ref={parentRef}
      className={
        "TaggableTextArea " +
        (props.placeHolder ? "TaggableTextArea_empty" : "")
      }
    >
      <textarea
        autoFocus={!!props.autofocus}
        ref={target}
        rows={rows}
        value={textValue}
        placeholder={props.placeHolder || ""}
        onFocus={() => {
          setBlurred(false);
          props.onFocus && props.onFocus();
        }}
        onBlur={() => {
          setTextValue(initiateAddTagSequence());
          props.onTextChange && props.onTextChange(undefined);
          props.onBlur && props.onBlur();
        }}
        onKeyDown={(e: React.KeyboardEvent<HTMLTextAreaElement>) => {
          if (e.keyCode === KeyCode.ESC_KEY) {
            e.preventDefault();
            props.onTextChange && props.onTextChange(undefined);
            props.onBlur && props.onBlur();
          } else if (e.keyCode === KeyCode.ENTER_KEY) {
            e.preventDefault();
            setTextValue(initiateAddTagSequence());
            props.onTextChange && props.onTextChange(undefined);
            props.onBlur && props.onBlur();
          } else if (
            e.keyCode === KeyCode.BACKSPACE_KEY &&
            props.tags &&
            props.tags.length > 0
          ) {
            let textArea = e.target as HTMLTextAreaElement;
            let currentValue = textArea.value;
            if (!currentValue) {
              let lastTag = props.tags[props.tags.length - 1];
              deleteTag(lastTag.id);
            }
            setCanAdd(false);
            props.onTextChange && props.onTextChange(undefined);
            props.onError && props.onError(undefined);
          } else {
            setCanAdd(true);
          }
        }}
        onInput={e => {
          // let textArea = e.target as HTMLTextAreaElement;
          if (props.isTagInvalid) {
            setTagInvalid(false);
          }

          setTextValue(calculateSizeByValue(e.currentTarget.value));
        }}
        onChange={e => props.onTextChange && props.onTextChange(e.target.value)}
      />
    </div>
  );
};
function addTagSequence(
  target: React.RefObject<HTMLTextAreaElement>,
  separator: RegExp,
  matchRule: ((tagText: string) => boolean) | undefined,
  props: TaggableTextAreaProps,
  setTagInvalid: (val: boolean) => void,
  addTag?: (tag: Tag[]) => void
): (val?: string | undefined) => string {
  return (val?: string) => {
    const textArea = target.current;
    if (!textArea) return "";
    const entries = (val || textArea.value)
      .split(separator)
      .filter((item, index, self) => self.indexOf(item) === index);
    let currentValue: string = (entries || [""]).join(",");
    const invalidTags: string[] = [];
    const validTags: Tag[] = [];
    let showMatchError = false;
    let showDuplicateError = false;
    entries.forEach(entry => {
      if (entry) {
        if (matchRule && matchRule(entry) === false) {
          let entryWithoutWhiteSpace = entry.replace(/ /g, "");
          if (matchRule(entryWithoutWhiteSpace) === true) {
            entry = entryWithoutWhiteSpace;
            validTags.push({
              id: shortid.generate(),
              text: entry
            });
          } else {
            invalidTags.push(entry);
            showMatchError = true;
          }
        } else if (
          !props.allowDuplicates &&
          (props.tags || []).map(tag => tag.text).indexOf(entry) > -1
        ) {
          invalidTags.push(entry);
          showDuplicateError = true;
        } else {
          validTags.push(createTag(entry));
        }
      }
    });
    if (validTags.length > 0) {
      addTag && addTag(validTags);
      currentValue = "";
    }
    if (invalidTags.length > 0) {
      setTagInvalid(true);
      currentValue = invalidTags.join(", ");
      if (!!currentValue.match(separator) && validTags.length === 0)
        showMatchError = showDuplicateError = false;
    } else {
      textArea.style.width = "0px";
    }
    if (!props.errors) return currentValue;
    if (props.errors.match && showMatchError) {
      props.onError && props.onError(props.errors.match);
    } else if (props.errors.duplicate && showDuplicateError) {
      props.onError && props.onError(props.errors.duplicate);
    } else {
      props.onError && props.onError(undefined);
    }
    return currentValue;
  };
}

export function createTag(entry: string) {
  return {
    id: shortid.generate(),
    text: entry
  };
}
