import React, { useCallback } from "react";
import { SvgCloseTag } from "@appcircle/assets/lib/CloseTag";

export type Tag = {
  id: any;
  text: string;
};

export type TagComponentsProps = {
  tags?: Tag[];
  showCloseIcon: boolean;
  onDelete?: (id: any) => void;
};
export function TagComponents(props: TagComponentsProps) {
  return (
    <React.Fragment>
      {(props.tags || []).map(tag => (
        <TagComponent
          onDelete={props.onDelete}
          showCloseIcon={props.showCloseIcon}
          key={tag.id}
          tag={tag}
        />
      ))}
    </React.Fragment>
  );
}

type TagComponentProps = {
  tag: Tag;
  showCloseIcon: boolean;
  onDelete?: (id: any) => void;
};
function TagComponent(props: TagComponentProps) {
  const onClick = useCallback(() => {
    props.onDelete && props.onDelete(props.tag.id);
  }, [props]);
  return (
    <div className="TaggableTextField_child_tag">
      <span>{props.tag.text}</span>
      {props.showCloseIcon ? (
        <SvgCloseTag
          className="TaggableTextField_child_tag_close"
          onClick={onClick}
        />
      ) : null}
    </div>
  );
}
