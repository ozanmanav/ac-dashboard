import React, { PropsWithChildren } from "react";

export function InfoMessage(props: PropsWithChildren<{}>) {
  return <div className="InfoMessage">{props.children}</div>;
}
