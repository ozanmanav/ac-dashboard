import { Link } from "react-router-dom";
import React, { PropsWithChildren, useCallback } from "react";
import classNames from "classnames";
import { ModuleNavBarDataItemType } from "@appcircle/shared/lib/Module";

export type ModuleNavBarMenuItemProps = PropsWithChildren<
  ModuleNavBarDataItemType & {
    index: number;
    onClick?: (index: number) => void;
  }
>;

export function ModuleNavBarMenuItem(props: ModuleNavBarMenuItemProps) {
  const onClick = useCallback(
    () => props.onClick && props.onClick(props.index),
    [props]
  );
  return (
    <div
      className={classNames("NavBarListItem", {
        "NavBarListItem-selected": props.isSelected
      })}
      onClick={onClick}
    >
      <Link className="NavBarListItem_link" to={props.link}>
        <div className="NavBarListItem_text">{props.text}</div>
      </Link>
      {props.length ? (
        <div className="NavBarListItem_badge">{props.length}</div>
      ) : null}
    </div>
  );
}
