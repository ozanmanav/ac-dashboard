import React, { PropsWithChildren } from "react";
import classNames from "classnames";

export type NavBarListHeaderProps = PropsWithChildren<{
  text: string;
}>;

export function NavBarListHeader(props: NavBarListHeaderProps) {
  return <div className={classNames("NavBarListHeader")}>{props.text}</div>;
}
