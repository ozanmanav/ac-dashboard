import React, { PropsWithChildren } from "react";
import { Spinner } from "./Spinner";
import classNames from "classnames";
import { Button, ButtonType } from "./Button";
import { useFormContext } from "@appcircle/form/lib/useFormContext";
import { FormStatus } from "@appcircle/form/lib/Form";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";

export enum SubmitButtonState {
  DISABLED = 0,
  ENABLED = 1,
  INPROGRESS = 2
}

export type SubmitButtonProps = PropsWithChildren<{
  className?: string;
  state?: SubmitButtonState;
  type?: ButtonType;
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  ariaLabel?: string;
  inProgressText?: string;
  formID?: string;
  showFocus?: boolean;
  tabIndex?: number;
}>;

export function SubmitButton(props: SubmitButtonProps) {
  const [formState] = useFormContext(props.formID || "");
  // Check if props.state has value if so use it and ignore form status
  const isinProgress = !isEmptyValue(props.state)
    ? props.state === SubmitButtonState.INPROGRESS
    : formState && formState.status.submission === FormStatus.SENDING;
  // Check if props.state has value if so use it and ignore form status
  const isDisabled =
    isinProgress ||
    (!isEmptyValue(props.state)
      ? props.state === SubmitButtonState.DISABLED ||
        (!!formState && (formState.errors || []).length > 0)
      : formState
      ? formState.status.isDirty === false ||
        formState.status.fieldsStatus !== FormStatus.READY
      : true);

  const className = classNames("SubmitButton", props.className, {
    "SubmitButton-disabled": isDisabled,
    // (formState && formState.status.fieldsStatus !== FormStatus.READY),
    "SubmitButton-inprogress": isinProgress
  });
  return (
    <Button
      tabIndex={props.tabIndex}
      ariaLabel={props.ariaLabel}
      type={ButtonType.WARNING}
      onClick={props.onClick}
      className={className}
      htmlType={"submit"}
      showFocus={props.showFocus || true}
    >
      {isinProgress && <Spinner />}
      <span>{isinProgress ? props.inProgressText : props.children}</span>
    </Button>
  );
}
