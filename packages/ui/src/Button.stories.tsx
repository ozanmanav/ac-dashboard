import React from "react";
import "./Button.stories.scss";
import { Button, ButtonType } from "./Button";
import { action } from "@storybook/addon-actions";

export default {
  component: Button,
  title: "Design System|UI/Button",
  parameters: {
    info: {
      text: `
            ~~~js
            <Button
            onClick={onClick}
            type={ButtonType.PRIMARY | ButtonType.DANGER} | ButtonType.SUCCESS | ButtonType.WARNING | ButtonType.LIGHT
            size={"md" | "lg" | "sm" | "xsm"}
            htmlType={"submit" | "reset" | "button"}
            showFocus={true | false}
            disabled={true | false}>
                Button Text
            </Button>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ButtonDefault = () => {
  return <Button onClick={action("onClick")}>Default</Button>;
};

export const ButtonDanger = () => {
  return (
    <Button onClick={action("onClick")} type={ButtonType.DANGER}>
      Danger
    </Button>
  );
};

export const ButtonPrimary = () => {
  return (
    <Button onClick={action("onClick")} type={ButtonType.PRIMARY}>
      Primary
    </Button>
  );
};

export const ButtonWarning = () => {
  return (
    <Button onClick={action("onClick")} type={ButtonType.WARNING}>
      Warning
    </Button>
  );
};

export const ButtonPrimaryLight = () => {
  return (
    <Button onClick={action("onClick")} type={ButtonType.LIGHT}>
      Primary Light
    </Button>
  );
};

export const ButtonWithFocus = () => {
  return (
    <Button onClick={action("onClick")} showFocus={true}>
      Button With Focus
    </Button>
  );
};

export const ButtonDisabled = () => {
  return <Button disabled={true}>Disabled</Button>;
};

export const ButtonHTMLTypes = () => {
  return (
    <div className="ButtonSizes">
      <Button onClick={action("onClickReset")} htmlType="reset">
        Reset
      </Button>
      <Button onClick={action("onClickSubmit")} htmlType="submit">
        Submit
      </Button>
      <Button onClick={action("onClickButton")} htmlType="button">
        Button
      </Button>
    </div>
  );
};

export const ButtonSizes = () => {
  return (
    <div className="ButtonSizes">
      <Button onClick={action("onClick")} size="xsm">
        X-Small
      </Button>
      <Button onClick={action("onClick")} size="sm">
        Small
      </Button>
      <Button onClick={action("onClick")} size="md">
        Medium
      </Button>
      <Button onClick={action("onClick")} size="lg">
        Large
      </Button>
    </div>
  );
};

// TODO: Need to have success button style

// export const ButtonSuccess = () => {
//   return (
//     <Button
//       onClick={action("onClick")}
//       type={ButtonType.SUCCESS}
//     >
//      Success
//     </Button>
//   );
// };
