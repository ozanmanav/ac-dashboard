import {
  PropsWithChildren,
  useEffect,
  useRef,
  CSSProperties,
  forwardRef,
  Ref,
  useCallback,
  useLayoutEffect,
  useState,
  WheelEventHandler
} from "react";

import React from "react";
const style: CSSProperties = {
  overflowY: "auto",
  overflowX: "hidden",
  display: "flex",
  flexDirection: "column",
  height: "100%",
  flexGrow: 1
};
const styleChild: CSSProperties = {
  flexGrow: 1
};

function useCombinedRefs<T = unknown>(...refs: Ref<T | null>[]) {
  const targetRef = useRef<T>(null);
  useEffect(() => {
    refs.forEach(ref => {
      if (!ref) return;

      if (typeof ref === "function") {
        ref(targetRef.current || null);
      } else {
        // @ts-ignore
        ref.current = targetRef.current;
      }
    });
  }, [refs]);

  return targetRef;
}

// export function getScrollBreakPoint(Element: HTMLElement) {
//   return <Element data-scrollview-breakpoint></Element>;
// }

export const ScrollView = forwardRef<
  HTMLDivElement | null,
  PropsWithChildren<{
    className?: string;
    autoScrollToBottom?: boolean;
    showNavBar?: boolean;
    breakPointSelector?: string;
  }>
>(function(props, ref) {
  const innerRef = useCombinedRefs<HTMLDivElement>(ref);
  const breakPointSelector = props.breakPointSelector;
  const elements = useRef<NodeListOf<Element> | null>(null);
  const [attheEnd, setAttheEnd] = useState();
  function getScrollElement(): HTMLDivElement | undefined {
    return (
      (innerRef.current as any).SimpleBar &&
      (innerRef.current as any).SimpleBar.getScrollElement()
    );
  }
  function scrollTo(top: number) {
    const el = getScrollElement();
    el && el.scroll({ top });
  }

  useEffect(() => {
    if (props.autoScrollToBottom === true && attheEnd !== false) {
      if (innerRef !== null) {
        const el = getScrollElement();
        el && scrollTo(el.scrollHeight);
        setAttheEnd(true);
      }
    }
  });
  useLayoutEffect(() => {
    if (props.showNavBar && innerRef.current !== null) {
      const observer = new MutationObserver(() => {
        elements.current =
          (innerRef.current &&
            innerRef.current.querySelectorAll(`${breakPointSelector}`)) ||
          null;
      });
      observer.observe(innerRef.current, { childList: true, subtree: true });
      let handler: WheelEventHandler;
      handler = e => {
        const el = getScrollElement();
        if (!el) return;
        if (
          el.scrollHeight - el.scrollTop - e.currentTarget.scrollHeight <
          50
        ) {
          setAttheEnd(true);
        } else {
          setAttheEnd(false);
        }
      };

      innerRef.current.addEventListener("scroll", handler as any, true);

      return () => {
        innerRef.current?.removeEventListener("scroll", handler as any, true);
        observer.disconnect();
      };
    }
  }, [innerRef, breakPointSelector]);

  const jumpTo = useCallback((jump: "up" | "down") => {
    if (elements.current) {
      let parentRect: { top: number; height: number };
      const scrollHeight = getScrollElement()?.scrollHeight || 0;
      const currentTop: number = getScrollElement()?.scrollTop || 0;
      let nextScrollTop: number | undefined = undefined;
      elements.current.forEach(element => {
        const rect = element.getBoundingClientRect();
        if (parentRect === undefined)
          parentRect = element.parentElement?.getBoundingClientRect() || {
            top: 0,
            height: 0
          };
        const diff = parentRect.top + currentTop;
        if (rect.top - diff < 0 && jump === "up")
          nextScrollTop = currentTop + rect.top - diff;
        else if (
          rect.top - diff > 0 &&
          nextScrollTop === undefined &&
          jump === "down"
        ) {
          nextScrollTop = currentTop + rect.top - diff;
        }
        console.log(scrollHeight, nextScrollTop);
      });
      nextScrollTop =
        nextScrollTop ||
        (nextScrollTop === undefined ? (jump === "up" ? 0 : scrollHeight) : 0);
      scrollTo(nextScrollTop);
    }
  }, []);
  const onScrollUp = useCallback(() => {
    jumpTo("up");
  }, []);
  const onScrollDown = useCallback(() => {
    jumpTo("down");
  }, []);
  return (
    <>
      <div
        ref={innerRef}
        style={style}
        className={"ScrollView " + (props.className || "")}
        data-simplebar
      >
        <div style={styleChild}>{props.children}</div>
      </div>
      {props.showNavBar && (
        <>
          <div
            onClick={onScrollUp}
            className="ScrollView_scrollNav ScrollView_scrollNav_top"
          >
            >
          </div>
          <div
            onClick={onScrollDown}
            className="ScrollView_scrollNav ScrollView_scrollNav_bottom"
          >
            >
          </div>
        </>
      )}
    </>
  );
});
