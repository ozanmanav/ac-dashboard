import { PropsWithChildren } from "react";
import React from "react";

type FlatHeaderProps = PropsWithChildren<{}>;
export function FlatHeader(props: FlatHeaderProps) {
  return <div className="FlatHeader">{props.children}</div>;
}
