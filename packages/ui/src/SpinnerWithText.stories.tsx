import React from "react";

import { SpinnerWithText } from "./SpinnerWithText";
import "./SpinnerWithText.stories.scss";

export default {
  component: SpinnerWithText,
  title: "Design System|UI/SpinnerWithText",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative",
          height: "100px"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <SpinnerWithText />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const SpinnerWithTextDefault = () => {
  return <SpinnerWithText text="Spinner Text" />;
};
