import React, { PropsWithChildren, useState, useMemo, useRef } from "react";
import classNames from "classnames";
import { useSpring, animated, config } from "react-spring";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { SvgMore } from "@appcircle/assets/lib/More";

// const MoreIcon = icons.more;
export type ThreeDotsToggleProps = PropsWithChildren<{
  size?: "md" | "lg" | "sm";
  isOpen?: false;
}>;
const fadeout = {
  config: config.stiff,
  to: {
    opacity: 0,
    transform: "translateY(-10px)"
  },
  from: {
    zIndex: 25,
    position: "relative",
    opacity: 1,
    transform: "translateY(0)"
  }
};
const fadein = {
  to: { opacity: 1, transform: "translateY(0)" },
  from: {
    opacity: 0,
    transform: "translateY(-10px)"
  }
};

export function ThreeDotsToggle(props: ThreeDotsToggleProps) {
  const [animProps, set] = useSpring(() => fadein);
  const root = useRef<HTMLDivElement>(null);
  useGlobalClickService([], root, () => {
    setIsOpen(false);
  });
  const [isOpen, setIsOpen] = useState(props.isOpen || false);
  set(isOpen ? fadein : fadeout);
  const style = useMemo(
    () => ({
      display: isOpen ? "block" : "none"
    }),
    [isOpen]
  );

  return (
    <div
      ref={root}
      className={"ThreeDotsToggle"}
      onClick={e => {
        setIsOpen(!isOpen);
      }}
    >
      <div
        className={classNames("ThreeDotsToggle_icon", {
          "ThreeDotsToggle_icon-isOpen": isOpen
        })}
      >
        <SvgMore />
      </div>
      {isOpen && (
        <animated.div style={animProps}>{props.children}</animated.div>
      )}
    </div>
  );
}
