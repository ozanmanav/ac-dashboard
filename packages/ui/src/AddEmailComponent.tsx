import React, {
  PropsWithChildren,
  useState,
  useEffect,
  useCallback,
  useMemo
} from "react";
import { ConditionalTextField } from "./ConditionalTextField";
import { AddButton } from "./AddButton";
import { IconButtonState } from "./IconButton";
import { useIntl } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { SvgEmailIcon } from "@appcircle/assets/lib/EmailIcon";
const emailValidationRegexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// eslint-disable-next-line
export type AddEmailComponentProps = PropsWithChildren<{
  emails: Array<string>;
  onAddEmail: (texts: string[]) => void;
  tabIndex?: number;
}>;

export function AddEmailComponent(props: AddEmailComponentProps) {
  const [toggleResetState, setToggleResetState] = useState(false);
  useEffect(() => {
    toggleResetState && setToggleResetState(!toggleResetState);
    setEmails([]);
  }, [toggleResetState]);
  const [emails, setEmails] = useState<string[]>([]);
  const [text, setText] = useState<string>("");

  const onAddEmailEvent = useCallback(() => {
    if (emails.length) {
      props.onAddEmail(emails);
      setToggleResetState(!toggleResetState);
      setEmails([]);
      setText("");
    }
  }, [emails, props, toggleResetState]);

  const onSuccessInput = useCallback((newEmails: string[]) => {
    setEmails(newEmails);
  }, []);

  const onWrongInput = useCallback(
    (wrongEmail: string[]) => {
      emails.length && setEmails([]);
    },
    [emails.length]
  );

  const rules = useMemo(
    () => [
      (email: string) => emailValidationRegexp.test(email),
      (email: string) => !props.emails.some(item => item === email)
    ],
    [props.emails]
  );

  const onChange = useCallback(text => {
    setText(text);
  }, []);

  const intl = useIntl();

  return (
    <div className="AddEmailComponent">
      <form
        onSubmit={e => {
          e.preventDefault();
        }}
      >
        <ConditionalTextField
          text={text}
          tabIndex={props.tabIndex}
          resetState={toggleResetState}
          placeHolder={intl.formatMessage(CommonMessages.appCommonEmail)}
          onSuccessInput={onSuccessInput}
          onWrongInput={onWrongInput}
          onEnterKey={onAddEmailEvent}
          onChange={onChange}
          rules={rules}
          icon={SvgEmailIcon}
        />
        <AddButton
          tabIndex={props.tabIndex}
          text={intl.formatMessage(CommonMessages.appCommonAdd)}
          state={
            emails.length ? IconButtonState.ENABLED : IconButtonState.DISABLED
          }
          onClick={onAddEmailEvent}
        />
      </form>
    </div>
  );
}
