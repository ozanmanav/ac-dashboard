import React, { useState } from "react";
import "./GroupList.stories.scss";
import { GroupList, GroupListItemType } from "./GroupList";
import { action } from "@storybook/addon-actions";
import { IntlProvider } from "react-intl";
import { SvgAdd } from "@appcircle/assets/lib/Add";

export default {
  component: GroupList,
  title: "Design System|UI/GroupList",
  parameters: {
    info: {
      text: `
            ~~~js
            const items: Array<GroupListItemType> = [
              {
                id: "1",
                name: "List Item-1",
                editMode: false
              },
              {
                id: "2",
                name: "List Item-2",
                editMode: false
              },
              {
                id: "3",
                name: "List Item-3",
                editMode: false
              }
            ];

            <GroupList
              onError={action("onError")}
              onEnter={action("onEnter")}
              onChange={action("onChange")}
              onBlurOrEscape={action("onBlurOrEscape")}
              onItemSelect={(item, index) => setSelectedGroupID(item.id)}
              items={items}
              selectedGroupID={selectedGroupID}
              fallback={<div>fallback</div>}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const items: Array<GroupListItemType> = [
  { id: "1", name: "List Item-1", editMode: false },
  {
    id: "2",
    name: "List Item-2",

    editMode: false
  },
  { id: "3", name: "List Item-3", editMode: false }
];

const itemsWithCounts: Array<GroupListItemType> = [
  { id: "1", name: "List Item-1", count: 20, editMode: false },
  {
    id: "2",
    name: "List Item-2",
    count: 50,
    editMode: false
  },
  { id: "3", name: "List Item-3", count: 30, editMode: false }
];

export const GroupListDefault = () => {
  const [selectedGroupID, setSelectedGroupID] = useState<string>("1");

  return (
    <IntlProvider locale="en">
      <GroupList
        onError={action("onError")}
        onEnter={action("onEnter")}
        onChange={action("onChange")}
        onBlurOrEscape={action("onBlurOrEscape")}
        onItemSelect={(item, index) => setSelectedGroupID(item.id)}
        items={items}
        selectedGroupID={selectedGroupID}
        fallback={<div>fallback</div>}
      />
    </IntlProvider>
  );
};

export const GroupListDefaultWithCounts = () => {
  const [selectedGroupID, setSelectedGroupID] = useState<string>("1");

  return (
    <IntlProvider locale="en">
      <GroupList
        onError={action("onError")}
        onEnter={action("onEnter")}
        onChange={action("onChange")}
        onBlurOrEscape={action("onBlurOrEscape")}
        onItemSelect={(item, index) => setSelectedGroupID(item.id)}
        items={itemsWithCounts}
        selectedGroupID={selectedGroupID}
        fallback={<div>fallback</div>}
      />
    </IntlProvider>
  );
};

export const GroupListDefaultWithIcons = () => {
  const [selectedGroupID, setSelectedGroupID] = useState<string>("1");

  return (
    <IntlProvider locale="en">
      <GroupList
        onError={action("onError")}
        onEnter={action("onEnter")}
        onChange={action("onChange")}
        onBlurOrEscape={action("onBlurOrEscape")}
        onItemSelect={(item, index) => setSelectedGroupID(item.id)}
        items={items}
        selectedGroupID={selectedGroupID}
        fallback={<div>fallback</div>}
        icon={SvgAdd}
      />
    </IntlProvider>
  );
};

export const GroupListDefaultWithCountsAndIcons = () => {
  const [selectedGroupID, setSelectedGroupID] = useState<string>("1");

  return (
    <IntlProvider locale="en">
      <GroupList
        onError={action("onError")}
        onEnter={action("onEnter")}
        onChange={action("onChange")}
        onBlurOrEscape={action("onBlurOrEscape")}
        onItemSelect={(item, index) => setSelectedGroupID(item.id)}
        items={itemsWithCounts}
        selectedGroupID={selectedGroupID}
        fallback={<div>fallback</div>}
        icon={SvgAdd}
      />
    </IntlProvider>
  );
};

export const GroupListEditable = () => {
  const [selectedGroupID, setSelectedGroupID] = useState<string>("1");
  const [itemsEditable, setItemsEditable] = useState<Array<GroupListItemType>>([
    { id: "0", name: "List Item-1", editMode: false },
    {
      id: "1",
      name: "List Item-2",

      editMode: false
    },
    { id: "2", name: "List Item-3", editMode: false }
  ]);

  const setItemEditable = (index: number) => {
    setSelectedGroupID(index.toString());
    let newItemsEditable: Array<GroupListItemType> = [];
    itemsEditable.map((item, innerIndex) => {
      if (index !== innerIndex) {
        newItemsEditable[innerIndex] = {
          id: item.id,
          name: item.name,
          editMode: false
        };
      } else {
        newItemsEditable[innerIndex] = {
          id: item.id,
          name: item.name,
          editMode: true
        };
      }
    });
    setItemsEditable(newItemsEditable);
    action("onItemSelect");
  };

  const onBlurOrEscape = () => {
    let newItemsEditable: Array<GroupListItemType> = [];
    itemsEditable.map((item, innerIndex) => {
      newItemsEditable[innerIndex] = {
        id: item.id,
        name: item.name,
        editMode: false
      };
    });
    setItemsEditable(newItemsEditable);
    action("onBlurOrEscape");
  };

  const onChange = (value: string) => {
    let newItemsEditable: Array<GroupListItemType> = [];
    itemsEditable.map((item, innerIndex) => {
      if (innerIndex.toString() !== selectedGroupID) {
        newItemsEditable[innerIndex] = {
          id: item.id,
          name: item.name,
          editMode: item.editMode
        };
      } else {
        newItemsEditable[innerIndex] = {
          id: item.id,
          name: value,
          editMode: item.editMode
        };
      }
    });
    setItemsEditable(newItemsEditable);
    action("onChange");
  };

  return (
    <IntlProvider locale="en">
      <GroupList
        onError={action("onError")}
        onEnter={action("onEnter")}
        onChange={onChange}
        onBlurOrEscape={onBlurOrEscape}
        onItemSelect={(item, index) => setItemEditable(index)}
        items={itemsEditable}
        selectedGroupID={selectedGroupID}
        fallback={<div>fallback</div>}
      />
    </IntlProvider>
  );
};
