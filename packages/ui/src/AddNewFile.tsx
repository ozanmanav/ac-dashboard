import React, {
  PropsWithChildren,
  useRef,
  useState,
  useMemo,
  useCallback,
  useEffect,
  ReactElement,
  DragEventHandler
} from "react";
import classNames from "classnames";
import { animated, useSpring } from "react-spring";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useIntl, defineMessages } from "react-intl";
import CommonMessages from "@appcircle/shared/lib/messages/common";
import { Result } from "@appcircle/core/lib/services/Result";
import { TextButton } from "./TextButton";
const messages = defineMessages({
  uploadError: {
    id: "app.component.AddNewFile.error",
    defaultMessage: "Error occurred when the file uploading"
  }
});
export enum AddNewFileState {
  IDLE = "IDLE",
  UPLOADING = "UPLOADING",
  ERROR = "ERROR"
}
export type AddNewFileProps = PropsWithChildren<{
  types: string[];
  text: string;
  onFileUpload: (file: File) => void;
  icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
  onStateChange?: (state: AddNewFileStatus) => void;
  autoUpload?: boolean;
  status?: AddNewFileStatus;
  disabled?: boolean;
  progress?: number;
  entityID?: string;
}>;

export type AddNewFileStatus =
  | "idle"
  | "file-selected"
  | "uploading"
  | "error"
  | "invalid"
  | "dragging"
  | "processing";

const IdleComponent = (props: {
  onClick: () => void;
  text: string;
  errorText: string;
  disabled?: boolean;
  input: ReactElement;
  hidden: boolean;
  invalid: boolean;
}) => {
  const intl = useIntl();

  return (
    <React.Fragment>
      {props.input}
      <div className={"AddNewFile_text"}>{props.text}</div>
      <div className={"AddNewFile_text AddNewFile-invalid--" + props.invalid}>
        {props.errorText}
      </div>
      <div className="AddNewFile_input">
        {!props.hidden && (
          <TextButton
            title={intl.formatMessage(CommonMessages.appCommonBrowse)}
            disabled={!!props.disabled}
            onClick={() => {
              props.onClick();
              // inputRef.current && inputRef.current.click();
            }}
          />
        )}
      </div>
    </React.Fragment>
  );
};

const FileSelectedComponent = (props: {
  onClick: () => void;
  text: string;
  input: ReactElement;
}) => {
  const intl = useIntl();

  return (
    <React.Fragment>
      {props.input}
      <div className="AddNewFile_selectedText">{props.text}</div>
      <div className="AddNewFile_input">
        <TextButton
          title={intl.formatMessage(CommonMessages.appCommonChange)}
          onClick={() => {
            props.onClick();
          }}
        />
      </div>
    </React.Fragment>
  );
};
//
const UploadingComponent = (props: {
  progress?: number;
  fileName?: string;
}) => {
  const intl = useIntl();

  return (
    <React.Fragment>
      <div className="AddNewFile_progressBar">
        <div
          style={{
            width: (props.progress !== undefined ? props.progress : 0) + "%"
          }}
        />
      </div>
      <div className="AddNewFile_text">
        {intl.formatMessage(CommonMessages.appCommonUploading)}
      </div>
      <div className="AddNewFile_text">{props.fileName}</div>
    </React.Fragment>
  );
};

const ProcessingComponent = (props: { fileName?: string }) => {
  const intl = useIntl();

  return (
    <React.Fragment>
      <div className="AddNewFile_progressBar">
        <div
          style={{
            width: "100%"
          }}
        />
      </div>
      <div className="AddNewFile_text">
        {intl.formatMessage(CommonMessages.appCommonUploadCompleted)}{" "}
        {intl.formatMessage(CommonMessages.appCommonProcessing)}
      </div>
      <div className="AddNewFile_text">{props.fileName}</div>
    </React.Fragment>
  );
};

const ErrorComponent = (props: { onRetry: () => void }) => {
  const intl = useIntl();

  return (
    <React.Fragment>
      <div className="AddNewFile_warning">Warning!</div>
      <div className="AddNewFile_text">
        {intl.formatMessage(messages.uploadError)}
      </div>
      <div className="AddNewFile_input">
        <TextButton
          title={intl.formatMessage(CommonMessages.appCommonRetry)}
          onClick={() => {
            props.onRetry();
          }}
        />
      </div>
    </React.Fragment>
  );
};

export function AddNewFile(props: AddNewFileProps) {
  const [status, setStatus] = useState<AddNewFileStatus>(
    props.status || "idle"
  );
  useEffect(() => {
    props.onStateChange && props.onStateChange(status);
  }, [props, status]);

  const [isDropCaptured, setIsDropCaptured] = useState(false);
  useEffect(() => {
    props.status && setStatus(props.status);
  }, [props.status]);
  const { getStatusByEntityID } = useTaskManager();
  const task = props.entityID
    ? getStatusByEntityID<Result<number>>(props.entityID)
    : null;

  useEffect(() => {
    const task = props.entityID && getStatusByEntityID<number>(props.entityID);
    task &&
      setStatus(
        task.status === "ADDED" ||
          task.status === "IN_PROGRESS" ||
          task.status === "WAITING_RESPONSE"
          ? task.status === "WAITING_RESPONSE" || status === "processing"
            ? "processing"
            : "uploading"
          : task.status === "ERROR"
          ? "error"
          : "idle"
      );
  }, [getStatusByEntityID, props.entityID, status, task]);
  // const taskStatus = task ? task.status : "";

  const inputRef = useRef<HTMLInputElement>(null);
  const uploadedFileName = useRef<string>("");
  const extensions = props.types || [];

  const acceptedExtensions = useMemo(
    () => extensions.map(type => formatType(type)).join(","),
    [extensions]
  );

  const onChange = useCallback(
    (e: any) => {
      e.preventDefault();
      let fileName =
        e.currentTarget &&
        e.currentTarget.files &&
        e.currentTarget.files[0] &&
        e.currentTarget.files[0].name;
      let isValidFileName = true;
      if (!fileName) {
        return;
      }
      uploadedFileName.current = fileName;

      //CHECK FILE(S)
      isValidFileName = props.types.length
        ? extensions.some(type => {
            let comparator = formatType(type);
            if (fileName.includes(comparator)) {
              return true;
            }
            return false;
          })
        : true;

      //INVALID FILE(S)
      if (!isValidFileName) {
        e.currentTarget.value = null;

        uploadedFileName.current = "";
        // props.onStateChange && props.onStateChange("error");
        setDragging(false);
        setIsDropCaptured(false);
        setStatus("invalid");
        return;
      }

      props.onFileUpload(e.currentTarget.files[0]);

      if (props.autoUpload === false) {
        setStatus("file-selected");
        return;
      }

      //UPLOAD FILE(S)
      // props.onStateChange && props.onStateChange("uploading");
      setIsDropCaptured(false);
      setStatus("uploading");
      inputRef.current && (inputRef.current.value = "");
    },
    [extensions, props]
  );
  const [dragging, setDragging] = useState(false);

  const progress =
    props.progress === undefined
      ? task && task.data
        ? task.data.json
        : 0
      : props.progress;

  const onDragOverCapture = useCallback<DragEventHandler<HTMLInputElement>>(
    e => {
      setStatus("idle");
      setDragging(true);
      !isDropCaptured && setIsDropCaptured(true);
    },
    [isDropCaptured]
  );
  const onDragLeaveCapture = useCallback<
    DragEventHandler<HTMLInputElement>
  >(() => {
    setDragging(false);
    isDropCaptured && setIsDropCaptured(false);
  }, [isDropCaptured]);

  const Icons = useMemo(
    () => props.icons.map((Icon, index: number) => <Icon key={index} />),
    [props.icons]
  );

  const inputComponent = useMemo(
    () => (
      <input
        ref={inputRef}
        type="file"
        accept={acceptedExtensions}
        onDragOverCapture={onDragOverCapture}
        onDragLeaveCapture={onDragLeaveCapture}
        onChange={onChange}
      />
    ),
    [acceptedExtensions, onChange, onDragLeaveCapture, onDragOverCapture]
  );
  // TODO: Seperate components
  const Partial = useMemo(() => {
    switch (status) {
      case "invalid":
      case "idle":
        return (
          <IdleComponent
            input={inputComponent}
            onClick={() => {
              inputRef.current && inputRef.current.click();
            }}
            hidden={dragging}
            text={props.text}
            errorText={"Invalid file type"}
            disabled={props.disabled}
            invalid={status === "invalid"}
          />
        );
      case "file-selected":
        return (
          <FileSelectedComponent
            input={inputComponent}
            onClick={() => {
              inputRef.current && inputRef.current.click();
            }}
            text={uploadedFileName.current}
          />
        );
      case "uploading":
        if (progress === 100) {
          setStatus("processing");
        }
        return (
          <UploadingComponent
            progress={progress}
            fileName={uploadedFileName.current}
          />
        );
      case "error":
        return (
          <ErrorComponent
            onRetry={() => {
              setStatus("idle");
              // props.onStateChange && props.onStateChange("idle");
            }}
          />
        );
      case "processing":
        return <ProcessingComponent fileName={uploadedFileName.current} />;
    }
  }, [inputComponent, progress, props.disabled, props.text, status]);

  return (
    <div
      className={classNames("AddNewFile", {
        "AddNewFile-isDropCaptured": isDropCaptured
      })}
    >
      <div>
        <div className="AddNewFile_icons">{Icons}</div>
        {Partial}
      </div>
    </div>
  );
}

const calc = (x: number, y: number) => [
  -(y - 75 / 2) / 40,
  (x - 75 / 2) / 40,
  2
];
const trans = (x: number, y: number, s: number) =>
  `perspective(800px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;

function AnimatedIcon(props: PropsWithChildren<{}>) {
  const [styles, set] = useSpring(() => ({
    xys: [0, 0, 1],
    config: { mass: 5, tension: 650, friction: 60 }
  }));

  return (
    <animated.div
      onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
      onMouseLeave={() => set({ xys: [0, 0, 1] })}
      style={{ transform: styles.xys.interpolate<any>(trans as any) }}
    >
      {props.children}
    </animated.div>
  );
}

function formatType(type: string) {
  return type[0] === "." ? type : `.${type}`;
}
