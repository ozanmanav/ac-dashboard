import React, { useEffect, useState, useRef } from "react";
import classNames from "classnames";
import { SvgToastClose } from "@appcircle/assets/lib/ToastClose";
import { useIntl } from "react-intl";
import {
  IToastComponent,
  DEFAULT_MESSAGE_LIFE,
  MessageStatus,
  DEFAULT_TOAST_HEIGHT,
  TOAST_VERTICAL_GAP
} from "./ToastService";
export const ToastComponent = (props: IToastComponent) => {
  const messageLife = props.message.life || DEFAULT_MESSAGE_LIFE;
  const { message } = props;
  const [status, setStatus] = useState<MessageStatus>("show");
  const style = {
    transform: `translateY(${-props.index *
      (DEFAULT_TOAST_HEIGHT + TOAST_VERTICAL_GAP)}px)`,
    height: DEFAULT_TOAST_HEIGHT
  };
  const intl = useIntl();
  const timer = useRef<NodeJS.Timeout>();
  useEffect(() => {
    clearTimeout(timer.current as NodeJS.Timeout);
    switch (status) {
      case "delete":
        props.onRemove(message);
        break;
      case "hide":
        timer.current = setTimeout(() => {
          setStatus("delete");
        }, 300);
        break;
      case "show":
        timer.current = setTimeout(() => {
          setStatus("hide");
        }, messageLife);
        break;
      default:
        break;
    }
    // eslint-disable-next-line
  }, [status]);
  return (
    <div
      onClick={() => {
        setStatus("hide");
      }}
      className={classNames("toast", message.messageType, {
        show: status === "show",
        hide: status === "hide"
      })}
      style={style}
    >
      <label className="toast__message">
        {message.messageId === true
          ? intl.formatMessage({ id: message.message })
          : message.message}
      </label>
      <SvgToastClose className="toast__close" />
    </div>
  );
};
