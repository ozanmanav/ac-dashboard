// import React, { useState, useCallback } from "react";

// import "./AutoSuggestionField.stories.scss";
// import { AutoSuggestionField } from "./AutoSuggestionField";
// import { TextInput } from "./form/TextInput/TextInput";
// import "simplebar/dist/simplebar.min.css";

// export default {
//   component: AutoSuggestionField,
//   title: "Design System|UI/AutoSuggestionField",
//   parameters: {
//     info: {
//       text: `
//             ~~~js

//             ~~~
//           `
//     }
//   },
//   // Our exports that end in "Data" are not stories.
//   excludeStories: /.*Data$/
// };

// // TODO: Not Working, Check Again

// export const AutoSuggestionFieldPure = () => {
//   const [value, setValue] = useState<string | undefined>("");
//   const [filteredData, setFilteredData] = useState<Array<string>>([""]);
//   const data = ["Suggestion-1", "Suggestion-2"];

//   const onFilter = (val?: string) => {
//     if (value) {
//       setFilteredData(data.filter(item => item.includes(value)));
//     }
//     return filteredData;
//   };

//   return (
//     <AutoSuggestionField value={filteredData} onFilter={onFilter}>
//       <TextInput
//         value={value}
//         onChange={val => {
//           setValue(val);
//           onFilter();
//         }}
//       />
//     </AutoSuggestionField>
//   );
// };
