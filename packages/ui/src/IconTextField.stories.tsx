import React from "react";
import "./IconTextField.stories.scss";
import { IconTextField } from "./IconTextField";
import { SvgAdd } from "@appcircle/assets/lib/Add";
import { action } from "@storybook/addon-actions";

export default {
  component: IconTextField,
  title: "Design System|UI/IconTextField",
  parameters: {
    info: {
      text: `
            ~~~js
            <IconTextField
              icon={SvgAdd}
              placeHolder={"placeholder"}
              onChange={onChange}
              onKeyDown={onKeyDown}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const IconTextFieldComponent = () => {
  return (
    <IconTextField
      icon={SvgAdd}
      placeHolder={"placeholder"}
      onChange={action("onChange")}
      onKeyDown={action("onKeyDown")}
    />
  );
};
