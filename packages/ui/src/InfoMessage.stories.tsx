import React from "react";
import "./InfoMessage.stories.scss";
import { InfoMessage } from "./InfoMessage";
import { action } from "@storybook/addon-actions";

export default {
  component: InfoMessage,
  title: "Design System|UI/InfoMessage",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <InfoMessage> Info Message </InfoMessage>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const InfoMessageComponent = () => {
  return <InfoMessage> Info Message </InfoMessage>;
};
