import React, { useState } from "react";
import "./ConditionalTextField.stories.scss";
import { ConditionalTextField } from "./ConditionalTextField";
import { action } from "@storybook/addon-actions";
import { SvgSearch } from "@appcircle/assets/lib/Search";

export default {
  component: ConditionalTextField,
  title: "Design System|UI/ConditionalTextField",
  parameters: {
    info: {
      text: `
            ~~~js
            // Checks input has "error" word
            const rule = (): boolean => (text.includes("error") ? false : true);

            const rulesArray: Array<() => boolean> = [rule];

            <ConditionalTextField
              placeHolder="Placeholder"
              text={text}
              icon={SvgSearch}
              rules={rulesArray}
              onSuccessInput={onSuccessInput}
              onWrongInput={onWrongInput}
              onEnterKey={onEnterKey}
              onChange={setText(text)}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ConditionalTextFieldWithRule = () => {
  const [text, setText] = useState<string>("");

  const rule = (): boolean => (text.includes("error") ? false : true);

  const rulesArray: Array<() => boolean> = [rule];

  return (
    <ConditionalTextField
      placeHolder="Placeholder"
      text={text}
      icon={SvgSearch}
      rules={rulesArray}
      onSuccessInput={action("onSuccessInput")}
      onWrongInput={action("onWrongInput")}
      onEnterKey={action("onEnterKey")}
      onChange={text => setText(text)}
    />
  );
};
