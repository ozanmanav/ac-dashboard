import React, { useState, useCallback, useEffect, useRef } from "react";
import { List } from "./List";
import { ListItemsType } from "./ListItem";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";

type AutoSuggestionFieldProps = {
  onFilter: (val?: string) => string[];
  onSelect?: (val: string) => void;
  onChange?: (val: string) => void;
  value?: any;
  children: React.ReactElement<{
    onTextChange?: (val: any) => void;
    text: string;
    onFocus?: () => void;
    onChange: (val: string) => void;
  }>;
};
function convertSuggestions(suggestions: string[]): ListItemsType {
  return suggestions.map(item => ({ label: item, value: item }));
}

export function AutoSuggestionField(props: AutoSuggestionFieldProps) {
  const [currentValue, setCurrentValue] = useState<string>();
  const [suggestions, setSuggestions] = useState<ListItemsType>(() =>
    convertSuggestions(props.onFilter(undefined))
  );
  const [focussed, setFocussed] = useState<boolean>(false);
  const element = React.Children.toArray(props.children)[0];
  const onTextChange = useCallback(
    (val: string) => {
      setSuggestions(convertSuggestions(props.onFilter(val)));
      setCurrentValue(val);
      val && setFocussed(true);
    },
    [props]
  );

  const onSelect = props.onSelect;
  const onBlur = useCallback(() => {
    setFocussed(false);
    setCurrentValue(undefined);
  }, []);
  const onFocus = useCallback(() => {
    setFocussed(true);
  }, []);

  const root = useRef<HTMLDivElement>(null);
  const blurHandler = useCallback(() => {
    setFocussed(false);
  }, []);

  useGlobalClickService(["List"], root, focussed ? blurHandler : null);

  return (
    <div ref={root} className="AutoSuggestionField">
      {React.isValidElement(element) &&
        React.cloneElement(element, {
          onFocus: onFocus,
          text: currentValue,
          onTextChange: onTextChange
        })}
      <List
        onBlur={onBlur}
        isOpened={focussed && !!suggestions.length}
        data={suggestions}
        onItemSelect={useCallback(
          val => {
            setCurrentValue(val.label);
            onSelect && onSelect(val.label);
          },
          [onSelect]
        )}
      />
    </div>
  );
}
