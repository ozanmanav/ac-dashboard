import React, { PropsWithChildren } from "react";
import classNames from "classnames";
import { ButtonProps, Button, ButtonType } from "./Button";

export enum IconButtonState {
  DISABLED = 0,
  ENABLED = 1
}
export type IconButtonProps = PropsWithChildren<{
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  onClick?: () => void;
  size?: ButtonProps["size"];
  className?: string;
  state?: IconButtonState;
  tabIndex?: number;
}>;

//TODO: Refactor this too using  new Button

export function IconButton(props: IconButtonProps) {
  const Icon = props.icon;
  return (
    <Button
      size={props.size || "sm"}
      className={classNames("IconButton", props.className, {
        "IconButton-disabled": props.state === IconButtonState.DISABLED
      })}
      tabIndex={props.tabIndex}
      onClick={props.onClick}
      type={ButtonType.WARNING}
    >
      <Icon className="IconButton_icon" />
      {props.children ? (
        <span className="IconButton_text"> {props.children} </span>
      ) : null}
    </Button>
  );
}
