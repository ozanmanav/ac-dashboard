import { PropsWithChildren } from "react";
import React from "react";
import { SpinnerWithText } from "./SpinnerWithText";

export type ProgressViewProps = PropsWithChildren<{
  indicatorComp?: JSX.Element;
  isInProgress?: boolean;
  isOverlay?: boolean;
  progressText?: string;
  type?: "spinner" | "stripe";
  onClick?: () => void;
}>;

export function ProgressView(props: ProgressViewProps) {
  const type = props.type || "spinner";
  let indicatorComp =
    props.indicatorComp || type === "spinner" ? (
      <SpinnerWithText text={props.progressText || ""} />
    ) : (
      <div className="stripeAnimation SpinnerWithText">
        {props.progressText ? props.progressText : null}
      </div>
    );
  const isOverlay = props.isInProgress ? !!props.isOverlay : "none";
  return (
    <React.Fragment>
      <div
        className={
          "ProgressView ProgressView-is-overlay--" +
          isOverlay +
          " ProgressView-show--" +
          !!props.isInProgress
        }
      >
        <div onClick={props.onClick} className="ProgressView_overlay">
          {indicatorComp}
        </div>
      </div>
      {props.children}
    </React.Fragment>
  );
}
