import React, { useState } from "react";
import "./EditableTextView.stories.scss";
import { EditableTextView } from "./EditableTextView";
import { action } from "@storybook/addon-actions";

export default {
  component: EditableTextView,
  title: "Design System|UI/EditableTextView",
  parameters: {
    info: {
      text: `
            ~~~js

            <EditableTextView
              value={value}
              onChange={setValue(value)}
              onBlur={onBlur}
              onFocus={onFocus}
              editable={true | false}
              multiline={true | false}
              activated={true | false}
              maxHeight={100 | undefined}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const EditableTextViewDefault = () => {
  const [value, setValue] = useState<string>("Editable Text View");

  return (
    <EditableTextView
      value={value}
      onChange={value => setValue(value)}
      onBlur={action("onBlur")}
      onFocus={action("onFocus")}
    />
  );
};

export const EditableTextViewEditable = () => {
  const [value, setValue] = useState<string>("Editable Text View");

  return (
    <EditableTextView
      value={value}
      onChange={value => setValue(value)}
      onBlur={action("onBlur")}
      onFocus={action("onFocus")}
      editable={true}
    />
  );
};

export const EditableTextViewActivated = () => {
  const [value, setValue] = useState<string>("Editable Text View");

  return (
    <EditableTextView
      value={value}
      onChange={value => setValue(value)}
      onBlur={action("onBlur")}
      onFocus={action("onFocus")}
      editable={true}
      activated={true}
    />
  );
};

export const EditableTextViewMultiline = () => {
  const [value, setValue] = useState<string>("Editable Text View");

  return (
    <EditableTextView
      value={value}
      onChange={value => setValue(value)}
      onBlur={action("onBlur")}
      onFocus={action("onFocus")}
      editable={true}
      multiline={true}
    />
  );
};

export const EditableTextViewMultilineWithMaxHeight = () => {
  const [value, setValue] = useState<string>("Editable Text View");

  return (
    <EditableTextView
      value={value}
      onChange={value => setValue(value)}
      onBlur={action("onBlur")}
      onFocus={action("onFocus")}
      editable={true}
      multiline={true}
      maxHeight={100}
    />
  );
};
