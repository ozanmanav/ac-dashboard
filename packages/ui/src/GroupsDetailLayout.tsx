import React, { useCallback } from "react";
import classNames from "classnames";
import { SvgHamburger } from "@appcircle/assets/lib/Hamburger";
import { IconButtonState } from "./IconButton";
import { AddButton } from "./AddButton";

type GroupsDetailLayoutProps = {
  mobileMode: boolean;
  displayNewButton?: boolean;
  onNew?: () => void;
  sideBar: React.ReactElement<any>;
  detail: React.ReactElement<any>;
  onMobileModeChange: (mode: boolean) => void;
  newButtonState?: IconButtonState;
  title: string;
};

export function GroupsDetailLayout(props: GroupsDetailLayoutProps) {
  return (
    <div className="GroupsDetailLayout">
      <div
        className={classNames("GroupsDetailLayout_sideBar", {
          "GroupsDetailLayout_sideBar-collapsed": !props.mobileMode
        })}
      >
        <div className="GroupsDetailLayout_header">
          <div className="GroupsDetailLayout_title">{props.title}</div>
          <AddButton
            text={""}
            state={props.newButtonState || IconButtonState.ENABLED}
            onClick={props.onNew}
          />
        </div>
        {props.sideBar}
      </div>

      <div className="GroupsDetailLayout_detail">{props.detail}</div>
      <div
        className={classNames("leftBarHandler", {
          "leftBarHandler-show": !props.mobileMode
        })}
        onClick={useCallback(() => {
          props.onMobileModeChange(true);
        }, [props.onMobileModeChange])}
      >
        <SvgHamburger />
      </div>
    </div>
  );
}
