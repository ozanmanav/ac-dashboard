import React, { PropsWithChildren } from "react";
import { IconButton, IconButtonState } from "./IconButton";
import { ButtonProps } from "./Button";
import { SvgAdd } from "@appcircle/assets/lib/Add";

export type AddButtonProps = PropsWithChildren<{
  text: string;
  onClick?: () => void;
  size?: ButtonProps["size"];
  className?: string;
  state?: IconButtonState;
  tabIndex?: number;
}>;

export function AddButton(props: AddButtonProps) {
  const className = "AddButton";
  return (
    <IconButton
      className={className}
      icon={SvgAdd}
      onClick={props.onClick}
      size={props.size}
      state={props.state}
      tabIndex={props.tabIndex}
    >
      {props.text}
    </IconButton>
  );
}
