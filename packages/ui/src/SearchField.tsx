import React, { PropsWithChildren } from "react";
import { IconTextField } from "./IconTextField";
import { TinyCloseButton } from "./TinyCloseButton";
import { SvgSearch } from "@appcircle/assets/lib/Search";

export type SearchFieldProps = PropsWithChildren<{
  text: string;
  placeHolder: string;
  onChange: (text: string) => void;
  className?: string;
}>;

export function SearchField(props: SearchFieldProps) {
  return (
    <div className="SearchField">
      <IconTextField
        classNameModifier={props.className}
        text={props.text}
        placeHolder={props.placeHolder}
        onChange={text => {
          props.onChange(text);
        }}
        icon={SvgSearch}
      />
      {props.text !== "" && (
        <TinyCloseButton
          onClick={() => {
            props.onChange("");
          }}
        ></TinyCloseButton>
      )}
    </div>
  );
}
