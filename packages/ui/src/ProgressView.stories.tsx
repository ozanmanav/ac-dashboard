import React from "react";
import "./ProgressView.stories.scss";
import { ProgressView } from "./ProgressView";
import { action } from "@storybook/addon-actions";

export default {
  component: ProgressView,
  title: "Design System|UI/ProgressView",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative",
          height: "300px"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js

            <ProgressView
              progressText="Progress Text"
              isInProgress={true}
              onClick={action("onClick")}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ProgressViewComponent = () => {
  return (
    <ProgressView
      progressText="Progress Text"
      isInProgress={true}
      onClick={action("onClick")}
    />
  );
};
