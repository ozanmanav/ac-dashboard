import React from "react";
import "./ScrollView.stories.scss";
import { ScrollView } from "./ScrollView";

export default {
  component: ScrollView,
  title: "Design System|UI/ScrollView",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative",
          height: "200px",
          background: "#fff",
          color: "#111"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js

            <ScrollView autoScrollToBottom={true | false} showNavBar={true | false}>
              <p>{text}</p>
            </ScrollView>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const text =
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora esse possimus natus eum aperiam reiciendis minus, perferendis excepturi voluptatum eveniet ad accusamus mollitia corrupti modi iste illum quos, quo quis. Obcaecati voluptates totam, corporis sunt ipsum, quis perspiciatis in pariatur nisi quisquam sequi dolore eos! Ut, sunt. Modi harum mollitia ratione delectus minima, consequatur qui quos debitis maiores quaerat blanditiis?  Minima tempora perferendis commodi tempore natus repudiandae quas eaque, numquam adipisci aliquid aspernatur impedit velit in quod labore animi quidem vitae eos! Excepturi quidem, perspiciatis illo reprehenderit eaque repellendus obcaecati!  Inventore, eum tempore at illum quod vero voluptate hic quaerat doloremque cupiditate. Molestiae, nulla! Dolor id qui voluptatem magni similique neque, voluptas amet, placeat molestiae voluptatum aspernatur illum vitae exercitationem? Fuga eveniet placeat quod, ipsum obcaecati doloribus accusamus animi delectus sed illum. Perspiciatis, tempore? Vitae dolor quae vel asperiores esse assumenda ea beatae tempore. Soluta fugiat autem vel nostrum mollitia. Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora esse possimus natus eum aperiam reiciendis minus, perferendis excepturi voluptatum eveniet ad accusamus mollitia corrupti modi iste illum quos, quo quis. Obcaecati voluptates totam, corporis sunt ipsum, quis perspiciatis in pariatur nisi quisquam sequi dolore eos! Ut, sunt. Modi harum mollitia ratione delectus minima, consequatur qui quos debitis maiores quaerat blanditiis?  Minima tempora perferendis commodi tempore natus repudiandae quas eaque, numquam adipisci aliquid aspernatur impedit velit in quod labore animi quidem vitae eos! Excepturi quidem, perspiciatis illo reprehenderit eaque repellendus obcaecati!  Inventore, eum tempore at illum quod vero voluptate hic quaerat doloremque cupiditate. Molestiae, nulla! Dolor id qui voluptatem magni similique neque, voluptas amet, placeat molestiae voluptatum aspernatur illum vitae exercitationem? Fuga eveniet placeat quod, ipsum obcaecati doloribus accusamus animi delectus sed illum. Perspiciatis, tempore? Vitae dolor quae vel asperiores esse assumenda ea beatae tempore. Soluta fugiat autem vel nostrum mollitia.";

export const ScrollViewDefault = () => {
  return (
    <ScrollView>
      <p className="ScrollParagraphExample">{text}</p>
    </ScrollView>
  );
};

export const ScrollViewAutoScrollBottom = () => {
  return (
    <ScrollView autoScrollToBottom={true}>
      <p className="ScrollParagraphExample">{text}</p>
    </ScrollView>
  );
};

export const ScrollViewAutoScrollWithScrollNavs = () => {
  return (
    <ScrollView showNavBar={true}>
      <p className="ScrollParagraphExample">{text}</p>
    </ScrollView>
  );
};
