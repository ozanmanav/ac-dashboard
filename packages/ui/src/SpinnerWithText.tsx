import React, { PropsWithChildren } from "react";
import { Spinner } from "./Spinner";

export type SpinnerWithTextProps = PropsWithChildren<{
  text: string;
}>;

//TODO: Refactor this with the new Button

export function SpinnerWithText(props: SpinnerWithTextProps) {
  return (
    <div className="SpinnerWithText">
      <Spinner />
      {props.text ? <span>{props.text}</span> : null}
    </div>
  );
}
