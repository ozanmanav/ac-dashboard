import React from "react";
import "./OSIcon.stories.scss";
import { OSIcon } from "./OSIcon";
import { OS } from "@appcircle/core/lib/enums/OS";

export default {
  component: OSIcon,
  title: "Design System|UI/OSIcon",
  parameters: {
    info: {
      text: `
            ~~~js
            <OSIcon
              os={OS.android | OS.ios}
              style={"light" | "dark"}
              display={"appIcon" | "small" | "tiny"} // tiny is default prop.
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const OSIconIOS = () => {
  return <OSIcon os={OS.ios} />;
};

export const OSIconIOSLight = () => {
  return <OSIcon os={OS.ios} style={"light"} />;
};

export const OSIconAndroid = () => {
  return <OSIcon os={OS.android} />;
};

export const OSIconAndroidLight = () => {
  return <OSIcon os={OS.android} style={"light"} />;
};

export const OSIconApp = () => {
  return <OSIcon os={OS.android} display={"appIcon"} />;
};

export const OSIconSmall = () => {
  return <OSIcon os={OS.android} display={"small"} />;
};
