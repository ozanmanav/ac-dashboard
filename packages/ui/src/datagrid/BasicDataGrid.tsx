import React, {
  ReactElement,
  useState,
  useCallback,
  useRef,
  useEffect,
  MouseEventHandler
} from "react";
import {
  BasicDataGridRow,
  BasicDataGridRowTemplateType,
  BasicDataGridHeaderTemplateType,
  BasicDataGridColHeaders,
  BasicDataGridRowProps
} from "./BasicDataGridRow";
import { TaskProgressView } from "../TaskProgressView";
import { ScrollView } from "../ScrollView";

export type BasicDataGridProps<TRowData extends { id: string } = any> = {
  data: TRowData[];
  headerData?: string[];
  headerTemplate?: BasicDataGridHeaderTemplateType;
  rowTemplate: BasicDataGridRowTemplateType<TRowData>;
  rowSeperatorTemplate: () => ReactElement;
  headerClassName?: string;
  rowClassName?: string;
  onItemsSelected?: BasicDataGridItemsSelectedHandler;
  selectedItems?: SelectedItemsType;
  updateParameter?: string;
  inProgress?: boolean;
  withCheckbox?: boolean;
  onRowClick?: BasicDataGridRowProps<TRowData>["onRowClick"];
  keyField?: "loog-index" | "id";
  keyMap?: string[];
  showAutoProgressOnRow?: boolean;
};

export type BasicDataGridItemsSelectedHandler = (items: string[]) => void;
export type SelectedItemsType = { [id: string]: boolean };

export function BasicDataGrid<
  TRowData extends { id: string } = any,
  THeaderData = any
>(props: BasicDataGridProps<TRowData>) {
  return (
    <div className="BasicDataGrid">
      <BasicDataGridHeadless<TRowData, THeaderData> {...props} />
    </div>
  );
}

export function BasicDataGridWithCustomHead<
  TRowData extends { id: string } = any,
  THeaderData = any
>(props: BasicDataGridProps<TRowData> & { head: JSX.Element }) {
  return (
    <div className="BasicDataGrid">
      {React.cloneElement(
        props.head,
        {},
        <BasicDataGridHeadless<TRowData, THeaderData> {...props} />
      )}
    </div>
  );
}

export function BasicDataGridHeadless<
  TRowData extends { id: string } = any,
  THeaderData = any
>(props: BasicDataGridProps<TRowData>) {
  const selectedItems = useRef<{ [key: string]: boolean }>({});

  const [allSelected, setAllSelected] = useState(false);

  useEffect(() => {
    if (props.selectedItems) {
      selectedItems.current = props.selectedItems;
    }
  }, [props.selectedItems]);
  const onSelected = useCallback(
    (id, name) => {
      const status = id !== undefined;
      selectedItems.current = { ...selectedItems.current, [name]: status };
      props.onItemsSelected &&
        props.onItemsSelected(
          Object.keys(selectedItems.current).filter(
            nm => selectedItems.current[nm]
          )
        );
      setAllSelected(
        Object.keys(selectedItems.current).filter(
          (key: string) => selectedItems.current[key] === true
        ).length === props.data.length
      );
    },
    [props, selectedItems]
  );

  const onSelectAll = useCallback(
    val => {
      const status = val !== undefined;
      setAllSelected(status);
      selectedItems.current = props.data.reduce(
        (acc: SelectedItemsType, item) => {
          acc[item.id] = status;
          return acc;
        },
        {}
      );

      props.onItemsSelected &&
        props.onItemsSelected(
          Object.keys(selectedItems.current).filter(
            id => selectedItems.current[id]
          )
        );
    },
    [props, selectedItems]
  );
  const click: MouseEventHandler<HTMLDivElement> = e => {};

  return (
    <React.Fragment>
      {props.headerTemplate && (
        <BasicDataGridColHeaders
          key={"header"}
          headerTemplate={props.headerTemplate}
          data={props.headerData}
          className={props.headerClassName}
          onSelectAll={onSelectAll}
          isSelected={allSelected}
          withCheckbox={props.withCheckbox}
        />
      )}
      <ScrollView>
        {
          <div className="BasicDataGrid_body" onMouseDown={click}>
            {(props.data || []).map((item: TRowData, index: number) => (
              <BasicDataGridRow
                showProgressonRow={props.showAutoProgressOnRow}
                index={index}
                key={
                  props.keyField !== undefined &&
                  props.keyField === "loog-index"
                    ? index
                    : item[props.keyField || "id"]
                }
                data={item}
                rowTemplate={props.rowTemplate}
                className={props.rowClassName}
                onSelected={onSelected}
                isSelected={!!selectedItems.current[item.id]}
                withCheckbox={props.withCheckbox}
                onRowClick={props.onRowClick}
              />
            ))}
          </div>
        }
      </ScrollView>
    </React.Fragment>
  );
}

export function BasicDataGridWithProgress<
  TRowData extends { id: string } = any,
  THeaderData = any
>(props: BasicDataGridProps<TRowData> & { connectTask?: string }) {
  return (
    <div className="BasicDataGrid">
      <TaskProgressView entityID={props.connectTask} hasRelativeRoot={true}>
        <BasicDataGridHeadless<TRowData, THeaderData> {...props} />
      </TaskProgressView>
    </div>
  );
}
