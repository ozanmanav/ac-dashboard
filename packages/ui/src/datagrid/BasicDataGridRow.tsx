import React, { PropsWithChildren, ReactElement } from "react";
import classNames from "classnames";
import { CheckBox, CheckBoxProps } from "../form/CheckBox";
import { TaskProgressView } from "../TaskProgressView";

export type BasicDataGridRowProps<
  TRowData extends { id: string }
> = PropsWithChildren<{
  data: TRowData;
  rowTemplate: BasicDataGridRowTemplateType<TRowData>;
  index: number;
  isHeader?: boolean;
  className?: string;
  isSelected?: boolean;
  onSelected: CheckBoxProps["onChange"];
  withCheckbox?: boolean;
  onRowClick?: (data: TRowData) => void;
  showProgressonRow?: boolean;
}>;

export type BasicDataGridRowTemplateType<TRowData = any> = (
  props: BasicDataGridRowTemplateProps<TRowData>
) => ReactElement;

export type BasicDataGridRowTemplateProps<TRowData = any> = {
  id: string;
  data: TRowData;
  index: number;
};

export type BasicDataGridHeaderTemplateType = (props?: any) => ReactElement;

export type BasicDataGridColHeadersProps<THeader> = {
  data: THeader;
  headerTemplate: BasicDataGridHeaderTemplateType;
  className?: string;
  isSelected?: boolean;
  onSelectAll: (selected: boolean) => void;
  withCheckbox?: boolean;
};

export function BasicDataGridColHeaders<TRowData = any>(
  props: BasicDataGridColHeadersProps<TRowData>
) {
  return (
    <div
      className={"BasicDataGridRow BasicDataGridRow_header " + props.className}
    >
      {props.withCheckbox !== false ? (
        <CheckBox
          isSelected={props.isSelected || false}
          value="header"
          name="header"
          onChange={value => {
            props.onSelectAll(value as boolean);
          }}
        />
      ) : null}
      <props.headerTemplate data={props.data} />
    </div>
  );
}
export function BasicDataGridRow<TRowData extends { id: string } = any>(
  props: BasicDataGridRowProps<TRowData>
) {
  return (
    <div
      className={classNames("BasicDataGridRow", {
        "BasicDataGridRow-selected": props.isSelected
      })}
      onClick={() => props.onRowClick && props.onRowClick(props.data)}
    >
      {props.showProgressonRow !== false ? (
        <TaskProgressView
          entityID={props.data.id}
          hasRelativeRoot={true}
          progressType="stripe"
        ></TaskProgressView>
      ) : null}
      {props.withCheckbox !== false && (
        <CheckBox
          isSelected={props.isSelected || false}
          value={props.data.id}
          name={props.data.id}
          onChange={props.onSelected}
        />
      )}
      <props.rowTemplate
        id={props.data.id}
        data={props.data}
        index={props.index}
      />
    </div>
  );
}
