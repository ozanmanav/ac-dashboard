import React from "react";
import "./ModuleNavBar.stories.scss";
import { ModuleNavBar } from "./ModuleNavBar";
import { NavBarListHeader } from "./NavBarListHeader";
import { ModuleNavBarMenuItem } from "./ModuleNavBarMenuItem";
import { BrowserRouter } from "react-router-dom";

export default {
  component: ModuleNavBar,
  title: "Design System|UI/ModuleNavBar",
  decorators: [
    (story: any) => (
      <div
        style={{
          position: "relative",
          height: "500px",
          marginLeft: "-75px"
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <BrowserRouter>
              <ModuleNavBar isBlock={true}>
              <NavBarListHeader text="Title" />
              <ModuleNavBarMenuItem index={0} text="Menu Item 1" link="#/" />
              <ModuleNavBarMenuItem index={1} text="Menu Item 2" link="#/" />
              </ModuleNavBar>
            </BrowserRouter>
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ModalWindowDefault = () => {
  return (
    <BrowserRouter>
      <ModuleNavBar>
        <NavBarListHeader text="Title" />
        <ModuleNavBarMenuItem index={0} text="Menu Item 1" link="#/" />
        <ModuleNavBarMenuItem index={1} text="Menu Item 2" link="#/" />
      </ModuleNavBar>
    </BrowserRouter>
  );
};

export const ModalWindowBlock = () => {
  return (
    <BrowserRouter>
      <ModuleNavBar isBlock={true}>
        <NavBarListHeader text="Title" />
        <ModuleNavBarMenuItem index={0} text="Menu Item 1" link="#/" />
        <ModuleNavBarMenuItem index={1} text="Menu Item 2" link="#/" />
      </ModuleNavBar>
    </BrowserRouter>
  );
};
