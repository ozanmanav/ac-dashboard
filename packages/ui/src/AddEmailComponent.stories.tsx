import React from "react";

import "./AddEmailComponent.stories.scss";
import { AddEmailComponent } from "./AddEmailComponent";
import { action } from "@storybook/addon-actions";
import { IntlProvider } from "react-intl";

const emails: string[] = ["email1@smartface.io", "email2@smartface.io"];

export default {
  component: AddEmailComponent,
  title: "Design System|UI/AddEmailComponent",
  parameters: {
    info: {
      text: `
            ~~~js

            const emails: string[] = [
              "email1@smartface.io",
              "email2@smartface.io"
            ];

            // Can not add emails which have been existed already.

            <AddEmailComponent
              emails={emails}
              onAddEmail={action("onAddEmail")}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const AddEmail = () => {
  return (
    <IntlProvider locale="en">
      <AddEmailComponent emails={emails} onAddEmail={action("onAddEmail")} />
    </IntlProvider>
  );
};
