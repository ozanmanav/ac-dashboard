import React, { PropsWithChildren, FunctionComponent, SVGProps } from "react";
import classNames from "classnames";
import { Link, LinkProps } from "react-router-dom";

export type ContextMenuItemDataType = PropsWithChildren<{
  text: string;
  icon: FunctionComponent<SVGProps<SVGSVGElement>>;
  size?: "md" | "lg" | "sm";
  linkProps?: LinkProps;
  disabled?: boolean;
}>;

export type ContextMenuItemProps = PropsWithChildren<
  {
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getDynamicText?: () => string;
  } & ContextMenuItemDataType
>;

export function ContextMenuItem(props: ContextMenuItemProps) {
  const Icon = props.icon;
  const text = props.getDynamicText ? props.getDynamicText() : props.text;
  return (
    <div
      className={classNames("ContextMenuItem", {
        "ContextMenuItem-selected": false,
        "disabled": props.disabled
      })}
      onClick={(e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        props.onClick && props.onClick(e);
      }}
    >
      <Icon />
      {text ? <span>{text}</span> : null}
      {props.linkProps ? <Link {...props.linkProps} /> : null}
    </div>
  );
}
