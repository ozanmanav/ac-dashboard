import React, { PropsWithChildren } from "react";
import { ContextMenuItem, ContextMenuItemProps } from "./ContextMenuItem";
import classNames from "classnames";

export type ContextMenuProps = PropsWithChildren<{
  size?: "md" | "lg" | "sm";
  items: Array<ContextMenuItemProps>;
  isOpen?: false;
  onMenuItemSelected?: (selectedIndex: number) => void;
  theme?: "dark" | "light";
}>;

export const ContextMenu = (props: ContextMenuProps) => {
  return (
    <div
      className={classNames("ContextMenu", {
        "ContextMenu-dark": props.theme === "dark",
        "ContextMenu-light": props.theme === "light"
      })}
    >
      <div className="ContextMenu_items">
        {props.items.map((item: ContextMenuItemProps, index: number) => (
          <ContextMenuItem
            key={item.text}
            {...item}
            onClick={() => {
              props.onMenuItemSelected && props.onMenuItemSelected(index);
            }}
          />
        ))}
      </div>
    </div>
  );
};
