import React, { PropsWithChildren, useState, useEffect } from "react";
import classNames from "classnames";
import { useSpring, animated } from "react-spring";
import { SvgClose } from "@appcircle/assets/lib/Close";
export type ModalDialogProps = PropsWithChildren<{
  header: string | JSX.Element;
  className?: string;
  onModalClose?: () => void;
}>;
// const CloseIcon = icons.close;

export function ModalDialogHeader(
  props: PropsWithChildren<{ align?: "center" | "left" | "right" }>
) {
  return (
    <div
      className={
        "ModalDialogHeader ModalDialogHeader-align--" +
        (props.align || "center")
      }
    >
      {props.children}
    </div>
  );
}

export function ModalDialog(props: ModalDialogProps) {
  const [close, setClose] = useState(false);
  useEffect(() => {
    if (close) {
      setTimeout(() => {
        props.onModalClose && props.onModalClose();
      }, 200);
      set({ opacity: 0, transform: `scale(0.95)` });
    }
    // eslint-disable-next-line
  }, [close, props.onModalClose]);
  const [animProps, set] = useSpring(() => ({
    config: { mass: 2, tension: 1000, friction: 70 },
    opacity: 1,
    transform: `scale(1)`,
    from: { opacity: 0, transform: `scale(1.03)` }
  }));
  return (
    <animated.div style={animProps as any} className="ModalDialog-container">
      <div className="modalContainer_close">
        <SvgClose onClick={() => setClose(true)} />
        <div>ESC</div>
      </div>
      <div className={classNames("ModalDialog", props.className)}>
        <header
          className={classNames("ModalDialog_header", {
            "ModalDialog_header-string": typeof props.header === "string"
          })}
        >
          {props.header}
        </header>
        <div className="ModalDialog_content"> {props.children}</div>
      </div>
    </animated.div>
  );
}
