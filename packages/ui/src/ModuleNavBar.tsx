import React, { PropsWithChildren } from "react";
import classNames from "classnames";
import { SvgHamburger } from "@appcircle/assets/lib/Hamburger";

type Props = PropsWithChildren<{
  isBlock?: boolean;
}>;

export function ModuleNavBar(props: Props) {
  return (
    <div
      className={classNames("ModuleNavBarContainer", {
        "ModuleNavBarContainer-block": props.isBlock
      })}
    >
      <div className="ModuleNavBar_handler">
        <SvgHamburger />
      </div>
      <div
        className={classNames("ModuleNavBar", {
          "ModuleNavBar-block": props.isBlock
        })}
      >
        {props.children}
      </div>
    </div>
  );
}
