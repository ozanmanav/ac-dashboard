import React, { useState, PropsWithChildren, useMemo } from "react";
import { SearchField } from "./SearchField";
import { debounce } from "@appcircle/core/lib/utils";
import { IconButtonState } from "./IconButton";
import { Model } from "@appcircle/core/lib/Model";
import { useIntl, IntlShape } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { SvgSearch } from "@appcircle/assets/lib/Search";
import { AddButton } from "./AddButton";
import { ProgressView } from "./ProgressView";
import { FlatHeader } from "./FlatHeader";

type SearchLayoutProps<TState, TResult> = PropsWithChildren<{
  onAddElement?: () => void;
  addButtonEnabled?: boolean;
  children: (result: TResult[]) => JSX.Element;
  isLoaded?: boolean;
  model: Model<TState, TResult>;
  header: string;
  noElementsText?: string;
  noElementsIcon: React.ElementType;
}>;
const DEBOUNCE_DELAY = 300;

export function SearchLayout<TModelState = any, TModelResult = any>(
  props: SearchLayoutProps<TModelState, TModelResult>
) {
  const items = props.model.getAll();
  const [searchText, setSearchText] = useState("");

  const searchItems = useMemo(
    () =>
      debounce((_text: string) => {
        setSearchText(_text);
        setSearchResult(props.model.search(_text));
      }, DEBOUNCE_DELAY),
    [props.model]
  );

  const [searchResult, setSearchResult] = useState<TModelResult[]>(() => []);
  const intl = useIntl();
  // const isTemporaryProfileExists = profilesModel.hasTemporary();
  const result: TModelResult[] = searchText ? searchResult : items;
  return (
    <div className="SearchLayout SearchLayout_results">
      <FlatHeader>
        <div className="FlatHeader__child-left" />
        <div className="FlatHeader__child-right">
          <SearchField
            text={searchText}
            placeHolder={intl.formatMessage(messages.appCommonSearch)}
            onChange={searchItems}
          />
          <AddButton
            state={
              searchText || props.addButtonEnabled
                ? IconButtonState.DISABLED
                : IconButtonState.ENABLED
            }
            text={intl.formatMessage(messages.appCommonAddNew)}
            size={"sm"}
            onClick={props.onAddElement}
          />
        </div>
      </FlatHeader>
      <ProgressView
        isInProgress={!props.isLoaded}
        progressText={intl.formatMessage(messages.appCommonLoading)}
        isOverlay={true}
      >
        <React.Fragment>
          {items.length === 0 &&
            (!searchText ? (
              <NoElements
                icon={props.noElementsIcon}
                text={
                  props.noElementsText ||
                  intl.formatMessage(messages.appCommonNoResult)
                }
              />
            ) : (
              <NoSearchResult searchText={searchText} intl={intl} />
            ))}
          <div
            style={useMemo(
              () => ({
                display: result.length === 0 ? "none" : "block",
                flexGrow: 1,
                overflow: "hidden"
              }),
              [result]
            )}
          >
            {props.children(result as TModelResult[])}
          </div>
        </React.Fragment>
      </ProgressView>
    </div>
  );
}

function NoElements(props: { icon: React.ElementType; text: string }) {
  return (
    <div className="NoProfile">
      <props.icon className="NoProfile_icon" />
      <div className="NoProfile_text">{props.text}</div>
    </div>
  );
}

function NoSearchResult(props: { searchText: string; intl: IntlShape }) {
  return (
    <div className="NoProfile">
      <SvgSearch className="NoProfile_icon" />
      <div className="NoProfile_text">
        {props.intl.formatMessage(messages.appCommonNoResultFor, {
          keyword: props.searchText
        })}
      </div>
    </div>
  );
}
