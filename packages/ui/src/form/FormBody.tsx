import { PropsWithChildren } from "react";
import React from "react";

export function FormBody(props: PropsWithChildren<{ noScroll?: boolean}>) {
  return (
    <div className="FormBody" data-simplebar={props.noScroll ? '' : 'init'}>
      <div>{props.children}</div>
    </div>
  );
}
