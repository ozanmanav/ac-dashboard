// src/shared/component/Toggle.stories.js

import React, { useState } from "react";

import { Toggle } from "./Toggle";
import "./Toggle.stories.scss";
import { action } from "@storybook/addon-actions";

export default {
  component: Toggle,
  title: "Design System|UI/Toggle",
  decorators: [
    (story: any) => (
      <div
        style={{
          width: 120
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <Toggle
              title="Toggle Text"
              selected={isSelected}
              onClick={onSwitchChange}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const ToggleText = () => {
  const [isSelected, setIsSelected] = useState(false);

  const onSwitchChange = () => {
    setIsSelected(!isSelected);
    action("onChange");
  };

  return (
    <Toggle
      title="Toggle Text"
      selected={isSelected}
      onClick={onSwitchChange}
    />
  );
};
