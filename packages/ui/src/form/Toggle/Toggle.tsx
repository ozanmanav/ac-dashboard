import { PropsWithChildren, ReactElement } from "react";
import React from "react";
import classNames from "classnames";

export type ToggleProps = PropsWithChildren<{
  title: string | ReactElement;
  selected: boolean;
  disabled?: boolean;
  onClick?: () => void;
}>;
export function Toggle(props: ToggleProps) {
  return (
    <div className="Toggle_header" onClick={props.onClick}>
      <div
        className={classNames("Toggle_header_title", {
          Toggle_selected: props.selected,
          "Toggle-disabled": props.disabled
        })}
      >
        {props.title}
      </div>
      <div className="Toggle_element">{props.children}</div>
    </div>
  );
}
