import React, { useState, useEffect, useCallback } from "react";
import {
  TaggableTextField,
  TaggableTextFieldProps
} from "../../TaggableTextField/TaggableTextField";
import { Tag } from "../../TaggableTextField/Tags";
import { createFakeID } from "@appcircle/core/lib/utils/fakeID";
import { FormElementType } from "@appcircle/form/lib/Form";

export type TagsFieldProps = Omit<
  TaggableTextFieldProps,
  "onError" | "addTag" | "deleteTag" | "value" | "onChange"
> &
  FormElementType<string[] | undefined> & {
    addTag?: TaggableTextFieldProps["onChange"];
    deleteTag?: TaggableTextFieldProps["deleteTag"];
  };

function createTags(value: string[] | undefined): Tag[] {
  return (value || []).map(text => ({
    id: createFakeID(),
    text
  }));
}

function createValue(value: Tag[] | undefined): string[] {
  return (value || []).map(tag => tag.text);
}

export function TagsField(props: TagsFieldProps) {
  const [tags, setTags] = useState<Tag[] | undefined>(() =>
    createTags(props.value)
  );
  useEffect(() => {
    props.value && setTags(createTags(props.value));
  }, [props.value]);

  return (
    <div className={"TagsField " + (props.className || "")}>
      <TaggableTextField
        allowDuplicates={props.allowDuplicates}
        matchRule={props.matchRule}
        errors={props.errors}
        placeHolder={props.placeHolder}
        separator={props.separator}
        suggestions={props.suggestions}
        onTextChange={props.onTextChange}
        autofocus={props.autofocus}
        text={props.text}
        className={"Form_field "}
        value={tags || []}
        onChange={useCallback(
          (newTag: Tag[]) => {
            props.onChange
              ? props.onChange(createValue(tags ? tags.concat(newTag) : newTag))
              : setTags(tags ? tags.concat(newTag) : newTag);
          },
          [props.onChange, tags]
        )}
        deleteTag={useCallback(
          id => {
            // const res = ;
            const _tags = (tags || []).filter(t => t.id !== id);
            props.onChange
              ? props.onChange(createValue(_tags))
              : setTags(_tags);
          },
          [props.onChange, tags]
        )}
      />
    </div>
  );
}
