// src/shared/component/RadioGroup.stories.js

import React, { useState } from "react";

import { RadioGroup } from "./RadioGroup";
import "./RadioGroup.stories.scss";
import { action } from "@storybook/addon-actions";

export default {
  component: RadioGroup,
  title: "Design System|UI/RadioGroup",
  decorators: [
    (story: any) => (
      <div
        style={{
          width: 150
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
          const radioData = [
            { label: "Radio1", value: "Radio1" },
            { label: "Radio2", value: "Radio2" }
          ];

            ~~~js
            <RadioGroup
              items={radioData}
              value={isSelected}
              onChange={onRadioButtonChange}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

let radioData = [
  { label: "Radio1", value: "Radio1" },
  { label: "Radio2", value: "Radio2" }
];

export const RadioGroupWithArray = () => {
  const [isSelected, setIsSelected] = useState("Radio 1");

  const onRadioButtonChange = (value: any) => {
    if (typeof value === "string") {
      setIsSelected(value);
      action("onChange");
    }
  };

  return (
    <RadioGroup
      items={radioData}
      value={isSelected}
      onChange={onRadioButtonChange}
    />
  );
};
