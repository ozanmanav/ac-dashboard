import React, { useMemo } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
import { RadioButton } from "../RadioButton";
import classNames from "classnames";
import { ComponentGroupType } from "../ComponentGroup";

export type RadioGroupProps = ComponentGroupType & FormElementType;

export function RadioGroup(props: RadioGroupProps) {
  //TODO: Give classes to child on selected and unselected
  const RadioButtons = useMemo(
    () =>
      props.items.map((child, index: number) => (
        <div
          key={index}
          onClick={() => {
            props.onChange && props.onChange(child.value);
          }}
          className={classNames("RadioGroup_item", {
            "RadioGroup_item-selected": props.value === child.value
          })}
        >
          <RadioButton
            title={child.label}
            value={child.value}
            // key={shortid.generate()}
            expression={child.expression}
            isSelected={props.value === child.value}
          />
        </div>
      )),
    [props]
  );

  return (
    <div
      className={classNames(props.className, "RadioGroup", {
        "RadioGroup-disabled": props.isDisabled,
        "RadioGroup-invalid": props.isValid === false
      })}
    >
      {RadioButtons}
    </div>
  );
}
