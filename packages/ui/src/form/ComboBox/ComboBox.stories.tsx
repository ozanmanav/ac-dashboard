// src/shared/component/ComboBox.stories.js

import "./ComboBox.stories.scss";

import React, { useState, useMemo } from "react";

import { ComboBox } from "./ComboBox";
import "./ComboBox.stories.scss";

export default {
  component: ComboBox,
  title: "Design System|UI/ComboBox",
  decorators: [(story: any) => <div>{story()}</div>],
  parameters: {
    info: {
      text: `
            ~~~js
            <ComboBox
            placeHolder="Provisioning Profile"
            items={useMemo(() => comboBoxData, [comboBoxData])}
            onChange={onSwitchChange}
          />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

let comboBoxData = [
  { label: "Radio1", value: "Radio1" },
  { label: "Radio2", value: "Radio2" }
];

export const ComboBoxDefault = () => {
  const [isSelected, setIsSelected] = useState(false);

  const onSwitchChange = () => {
    setIsSelected(!isSelected);
  };

  return (
    <div className="ComboBoxContainer">
      <ComboBox
        isSelected={true}
        value={"Radio1"}
        placeHolder="Provisioning Profile"
        items={useMemo(() => comboBoxData, [comboBoxData])}
        onChange={onSwitchChange}
      />
    </div>
  );
};
