import React, {
  useState,
  useCallback,
  useMemo,
  useEffect,
  useRef
} from "react";
import { EditableTextView } from "../../EditableTextView";
import { List } from "../../List";
import { ListItemsType } from "../../ListItem";
import { FormElementType } from "@appcircle/form/lib/Form";

type ComboBoxProps = FormElementType<string> & {
  editable?: boolean;
  items: ListItemsType;
  placeHolder?: string;
  style?: "danger" | "normal";
};
let blurTimeout: NodeJS.Timeout;
const MAX_DISPLAY_ITEM = 5;
const MAX_DISPLAY_HEIGHT = 225;

function isAtttheBottom(el: HTMLElement, offset: number = 42) {
  var top = el.offsetTop;
  var scrollTop = 0;
  var scrollerHeight = 0;
  var parent = el;

  while (parent.offsetParent) {
    parent = parent.offsetParent as HTMLElement;
    if (
      parent.scrollHeight >= parent.clientHeight &&
      parent.style.overflow.indexOf("hidden") > -1
    ) {
      scrollerHeight = parent.clientHeight;
      scrollTop = parent.scrollTop;
      if (scrollerHeight + scrollTop - (top + offset) < 0)
        return scrollerHeight + scrollTop - (top + offset);
    }

    top += parent.offsetTop;
  }

  return false;
}
const LIST_ITEM_HEIGHT = 48;
const TEXTAREA_HEIGHT = 42;
export function ComboBox(props: ComboBoxProps) {
  const styleType = props.style || "normal";
  const [opened, setOpened] = useState(false);
  const [textItems, setTextItems] = useState<ListItemsType>(() => []);
  const items = useMemo(() => [...props.items, ...textItems], [
    props.items,
    textItems
  ]);
  const selectedItem = useMemo(
    () => props.items.find(item => item.value === props.value),
    [props.items, props.value]
  );
  const selfRef = useRef<HTMLDivElement>(null);
  const addTextItem = useCallback(
    (value: string) => {
      !items.some(item => item.label === value) &&
        setTextItems([...textItems, { value, label: value }]);
    },
    [items, textItems]
  );
  const listRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    return () => {
      blurTimeout && clearTimeout(blurTimeout);
    };
  }, []);

  return (
    <div ref={selfRef} className={"ComboBox ComboBox-" + styleType}>
      <EditableTextView
        placeHolder={props.placeHolder}
        editable={props.editable}
        className={"Form_field " + props.className}
        onFocus={useCallback(() => {
          setOpened(true);
          if (selfRef.current && listRef.current) {
            const itemLen = Math.min(items.length, MAX_DISPLAY_ITEM);
            const h = itemLen * LIST_ITEM_HEIGHT;
            const maxh = Math.min(h, MAX_DISPLAY_HEIGHT);
            const listHeight = maxh + TEXTAREA_HEIGHT;
            const listOffset = Math.max(maxh - LIST_ITEM_HEIGHT, 0);
            const diff = isAtttheBottom(selfRef.current, listHeight);
            if (diff < 0) {
              listRef.current.style.top = "-" + listOffset + "px";
            } else {
              listRef.current.style.top = "auto";
            }
          }
        }, [items])}
        onBlur={useCallback((text?: string) => {
          blurTimeout = setTimeout(() => setOpened(false), 50);
          // text && addTextItem(text);
        }, [])}
        onChange={useCallback(
          val => {
            props.onChange && props.onChange(val);
          },
          [props]
        )}
        activated={opened}
        value={selectedItem ? selectedItem.label : props.value || ""}
      />
      <List
        ref={listRef}
        onBlur={() => {
          blurTimeout = setTimeout(() => setOpened(false), 50);
        }}
        isOpened={opened}
        data={items}
        onItemSelect={useCallback(
          data => {
            props.onChange && props.onChange(data.value);
          },
          [props.onChange]
        )}
      ></List>
      {items && items.length > 0 ? (
        <div className="ComboBoxTriangleIcon" />
      ) : null}
    </div>
  );
}
