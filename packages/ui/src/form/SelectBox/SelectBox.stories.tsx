// src/shared/component/SelectBox.stories.js

import React from "react";

import { SelectBox } from "./SelectBox";
import "./SelectBox.stories.scss";
import { action } from "@storybook/addon-actions";

export default {
  component: SelectBox,
  title: "Design System|UI/SelectBox",
  decorators: [
    (story: any) => (
      <div
        style={{
          width: 150
        }}
      >
        {story()}
      </div>
    )
  ],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

let selectBoxData = [
  { label: "Selection1", value: "Selection1" },
  { label: "Selection2", value: "Selection2" }
];

export const Single = () => {
  return (
    <SelectBox onChange={action("onChange")}>
      {selectBoxData.map(({ value, label }) => {
        return (
          <option key={label} value={value}>
            {label}
          </option>
        );
      })}{" "}
    </SelectBox>
  );
};
