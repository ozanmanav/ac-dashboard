import React, { useCallback, ReactNodeArray, FunctionComponent } from "react";
import classNames from "classnames";
import { FormElementType } from "@appcircle/form/lib/Form";

export type SelectBoxProps = FormElementType<
  string | string[] | number | undefined
> & {
  children?: ReactNodeArray;
  visible?: boolean;
};
export const SelectBox: FunctionComponent<SelectBoxProps> = props => {
  const onChange = useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      props.onChange &&
        props.onChange(e.target.options[e.target.selectedIndex].value);
    },
    [props]
  );
  return (
    <div className={"SelectBox SelectBox-visible--" + props.visible}>
      <select
        placeholder={props.placeHolder || ""}
        disabled={props.isDisabled}
        value={props.value === null ? "" : props.value}
        onChange={onChange}
        className={classNames(props.className, "Form_field", {
          "SelectBox-disabled": props.isDisabled,
          "SelectBox-invalid": props.isValid === false
        })}
      >
        {props.children}
      </select>
      <div className="SelectBoxTriangleIcon" />
    </div>
  );
};
