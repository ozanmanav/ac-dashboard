// src/shared/component/Switch.stories.js

import React, { useState } from "react";

import { Switch } from "./Switch";
import "./Switch.stories.scss";
import { action } from "@storybook/addon-actions";

export default {
  component: Switch,
  title: "Design System|UI/Switch",
  parameters: {
    info: {
      text: `
            ~~~js
            <Switch isSelected={true|false} onChange={onSwitchChange} />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Single = () => {
  const [isSelected, setIsSelected] = useState(false);

  const onSwitchChange = (value: boolean) => {
    setIsSelected(value);
    action("onChange");
  };

  return <Switch isSelected={isSelected} onChange={onSwitchChange} />;
};
