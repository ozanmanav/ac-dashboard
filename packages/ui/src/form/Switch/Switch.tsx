import React, { PropsWithChildren, useCallback } from "react";
import classNames from "classnames";
import { FormElementType } from "@appcircle/form/lib/Form";

export enum SwitchType {
  DANGER = "danger",
  PRIMARY = "primary",
  SUCCESS = "success",
  WARNING = "warning"
}
export type SwitchProps = PropsWithChildren<{
  type?: SwitchType;
  className?: string;
}> &
  FormElementType<boolean>;

export function Switch(props: SwitchProps) {
  const className = classNames("Switch", `Switch-${props.type || "primary"}`, {
    "Switch-disabled": props.isDisabled,
    Switch_on: props.isSelected,
    "Switch-invalid": props.isValid === false
  });

  const onClick = useCallback(() => {
    props.onChange && props.onChange(!props.isSelected);
  }, [props]);

  return (
    <div tabIndex={props.tabIndex} className={className} onClick={onClick}>
      <div className="Switch_line" />
      <div className="Switch_thumb" />
    </div>
  );
}
