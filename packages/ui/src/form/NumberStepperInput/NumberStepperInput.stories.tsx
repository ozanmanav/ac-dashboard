// src/shared/component/NumberStepperInput.stories.js

import React from "react";

import { NumberStepperInput } from "./NumberStepperInput";
import "./NumberStepperInput.stories.scss";

export default {
  component: NumberStepperInput,
  title: "Design System|UI/NumberStepperInput",
  decorators: [
    (story: any) => (
      <div
        style={{
          width: 200
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <NumberStepperInput value={"12"} min={10} max={20} step={2} />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const MinMaxTwoByTwoStepper = () => {
  return <NumberStepperInput value={"12"} min={10} max={20} step={2} />;
};
