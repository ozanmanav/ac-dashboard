import React, { useRef, useMemo, useCallback } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
import classNames from "classnames";

export type NumberStepperInputProps = {
  min: number;
  max: number;
  step?: number;
} & FormElementType<string>;
export function NumberStepperInput(props: NumberStepperInputProps) {
  const inputRef = useRef<HTMLInputElement>(null);
  const stepUp = useCallback(() => {
    inputRef.current && inputRef.current.stepUp();
    inputRef.current &&
      props.onChange &&
      props.onChange(inputRef.current.value);
  }, [props]);
  const stepDown = useCallback(() => {
    inputRef.current && inputRef.current.stepDown();
    inputRef.current &&
      props.onChange &&
      props.onChange(inputRef.current.value);
  }, [props]);
  const onChange = useCallback(
    e => {
      props.onChange && props.onChange(e.target.value);
    },
    [props]
  );

  return (
    <div
      className={classNames(
        "Form_field NumberStepperInput",
        {
          "NumberStepperInput-disabled": props.isDisabled,
          "NumberStepperInput-invalid": props.isValid === false
        },
        props.className
      )}
    >
      <input
        step={props.step}
        ref={inputRef}
        tabIndex={props.tabIndex}
        type="number"
        className="NumberStepperInput_input"
        min={props.min}
        max={props.max}
        value={String(props.value)}
        onChange={onChange}
      />
      <div className="NumberStepperInput_stepperControls">
        <StepperControls orientation="up" onClick={stepUp} />
        <StepperControls orientation="down" onClick={stepDown} />
      </div>
    </div>
  );
}

type StepperControls = {
  orientation: "up" | "down";
  onClick: () => void;
};
function StepperControls(props: StepperControls) {
  return (
    <div
      onClick={props.onClick}
      className={classNames("StepperControls", {
        "StepperControls-up": props.orientation === "up",
        "StepperControls-down": props.orientation === "down"
      })}
    >
      <div
        className={classNames("StepperControls_arrow", {
          "StepperControls_arrow-up": props.orientation === "up",
          "StepperControls_arrow-down": props.orientation === "down"
        })}
      />
    </div>
  );
}
