// src/shared/component/CheckBox.stories.js

import React from "react";

import "./CheckboxGroup.stories.scss";
import { withKnobs, text } from "@storybook/addon-knobs";
import { CheckboxGroup } from "./CheckboxGroup";
import { action } from "@storybook/addon-actions";

export default {
  component: CheckboxGroup,
  title: "Design System|UI/CheckboxGroup",
  decorators: [
    (story: any) => (
      <div
        style={{
          width: 200
        }}
      >
        {story()}
      </div>
    ),
    withKnobs
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            const items = [
              { value: "value1", label: "label1" },
              { value: "value2", label: "label2" }
            ];

             <CheckboxGroup items={items} onChange={action("onChange")} />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const items = [
  { value: "value1", label: "label1" },
  { value: "value2", label: "label2" }
];

export const DynamicCheckboxesWithLabel = () => {
  let parsedArray = items;

  try {
    parsedArray = JSON.parse(
      `${text("Items", JSON.stringify(items))}`.replace(/&quot;/g, '"')
    );
  } catch (error) {
    console.log(error);
  }

  return (
    <CheckboxGroup items={parsedArray} onChange={action("selectedItem")} />
  );
};

export const DisabledCheckboxesWithLabel = () => {
  return (
    <CheckboxGroup
      isDisabled={true}
      items={items}
      onChange={action("selectedItem")}
    />
  );
};
