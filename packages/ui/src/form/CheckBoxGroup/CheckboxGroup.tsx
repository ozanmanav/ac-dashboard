import React, { useMemo } from "react";
import classNames from "classnames";
import { FormElementType } from "@appcircle/form/lib/Form";
import { ComponentGroupType } from "../ComponentGroup";
import { CheckBox } from "../CheckBox";

export type CheckboxGroupProps = ComponentGroupType &
  FormElementType<(string | number | null)[]>;

export function CheckboxGroup(props: CheckboxGroupProps) {
  const value = useMemo(() => props.value || [], [props.value]);
  const Checkboxes = useMemo(
    () =>
      props.items.map((child, index: number) => (
        <div
          key={index}
          className={classNames("ComponentGroup_item", {
            ".ComponentGroup_item-selected": props.value === child.value
          })}
        >
          <CheckBox
            onChange={val => {
              // props.onChange && props.onChange(child.value);
              const values =
                val !== undefined
                  ? [...value, child.value]
                  : value.filter(v => child.value !== v);
              props.onChange && props.onChange(values);
            }}
            title={child.label}
            value={child.value}
            // key={shortid.generate()}
            expression={child.expression}
            isSelected={value.some(v => v === child.value)}
          />
        </div>
      )),
    [value, props.items, props.onChange]
  );

  return (
    <div
      className={classNames(props.className, "RadioGroup", {
        "RadioGroup-disabled": props.isDisabled,
        "RadioGroup-invalid": props.isValid === false
      })}
    >
      {Checkboxes}
    </div>
  );
}
