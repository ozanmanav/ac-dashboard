import { ListItemType } from "../ListItem";

export type ComponentGroupItemType = ListItemType;
export type ComponentGroupType = {
  /**
   * Value-Label Array
   */
  items: ComponentGroupItemType[];
};
