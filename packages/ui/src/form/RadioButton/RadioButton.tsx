import React, {
  useCallback,
  PropsWithChildren,
  useState,
  useEffect,
  useMemo
} from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
import classNames from "classnames";
import { SvgRadioItem } from "@appcircle/assets/lib/RadioItem";
import { Toggle } from "../Toggle";

export type RadioButtonType = PropsWithChildren<
  FormElementType<string | number | boolean> & {
    title?: string;
    value?: string | number | boolean;
    expression?: string;
    isSelected?: boolean;
  }
>;

export function RadioButton<TValue = any>(props: RadioButtonType) {
  const [defaultValue, setDefaultValue] = useState();
  useEffect(() => {
    setSelect(
      props.isSelected === undefined
        ? props.value === defaultValue
        : props.isSelected
    );
  }, [props.value, props.isSelected, defaultValue]);

  const [select, setSelect] = useState(false);
  useEffect(() => {
    // setSelect(false);
    setDefaultValue(props.value);
    // eslint-disable-next-line
  }, []);

  const onClick = useCallback(() => {
    props.onChange && props.onChange(defaultValue);
    setSelect(true);
  }, [defaultValue, props]);
  const title = useMemo(
    () => (
      <React.Fragment>
        <div className="RadioButton_title">{props.title}</div>
        {props.expression && (
          <div className={"RadioButton_expression"}>{props.expression}</div>
        )}
      </React.Fragment>
    ),
    [props.title, props.expression]
  );
  return (
    <div
      className={classNames("RadioButton", {
        "RadioButton-selected": select,
        "RadioButton-disabled": props.isDisabled,
        "RadioButton-invalid": props.isValid === false
      })}
      onClick={onClick}
      tabIndex={props.tabIndex}
    >
      {props.title ? (
        <Toggle title={title} selected={select}>
          <div className="RadioButton_circle">
            <SvgRadioItem />
          </div>
        </Toggle>
      ) : (
        <div className="RadioButton_circle">
          <SvgRadioItem />
        </div>
      )}
    </div>
  );
}
