// src/shared/component/RadioButton.stories.js

import React, { useState } from "react";

import { RadioButton } from "./RadioButton";
import "./RadioButton.stories.scss";
import { action } from "@storybook/addon-actions";

export default {
  component: RadioButton,
  title: "Design System|UI/RadioButton",
  decorators: [
    (story: any) => (
      <div
        style={{
          backgroundColor: "#f5f6f9",
          borderRadius: "0.3125rem",
          width: 150
        }}
      >
        {story()}
      </div>
    )
  ],
  parameters: {
    info: {
      text: `
            ~~~js
            <RadioButton
            isSelected={true|false}
            value="Radio 1"
            title="Radio 1"
            onChange={onRadioButtonChange} 
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Single = () => {
  const [isSelected, setIsSelected] = useState("Radio 1");

  const onRadioButtonChange = (value: string | number | boolean) => {
    if (typeof value === "string") {
      setIsSelected(value);
      action("onChange");
    }
  };

  return (
    <div>
      <RadioButton
        isSelected={isSelected === "Radio 1"}
        value={"Radio 1"}
        title="Radio 1"
        onChange={onRadioButtonChange}
      />
    </div>
  );
};
