import React, {
  PropsWithChildren,
  useCallback,
  useMemo,
  useState,
  useEffect
} from "react";
import classNames from "classnames";
import { FormElementType } from "@appcircle/form/lib/Form";
import { Toggle } from "../Toggle";
import { SvgCheck } from "@appcircle/assets/lib/Check";

export type CheckBoxProps = PropsWithChildren<{}> &
  Omit<FormElementType<string | number | boolean | undefined>, "onChange"> & {
    title?: string;
    value?: string | number | boolean;
    expression?: string;
    isSelected?: boolean;
    onChange: (
      value: boolean | number | string | null | undefined,
      name?: string
    ) => void;
    name?: string;
  };

export function CheckBox(props: CheckBoxProps) {
  const [defaultValue, setDefaultValue] = useState();
  useEffect(() => {
    setSelect(
      props.isSelected === undefined
        ? props.value === defaultValue
        : props.isSelected
    );
  }, [props.value, props.isSelected, defaultValue]);
  const [select, setSelect] = useState(false);

  useEffect(() => {
    // setSelect(false);
    setDefaultValue(props.value);
    // eslint-disable-next-line
  }, []);

  const onClick = useCallback(() => {
    props.onChange &&
      props.onChange(!select ? defaultValue : undefined, props.name);
    setSelect(!select);
  }, [defaultValue, props, select]);
  const title = useMemo(
    () => (
      <React.Fragment>
        <div className="CheckBox_title">{props.title}</div>
        {props.expression && (
          <div className={"CheckBox_expression"}>{props.expression}</div>
        )}
      </React.Fragment>
    ),
    [props.title, props.expression]
  );
  return (
    <div
      tabIndex={props.tabIndex}
      className={classNames("CheckBox", {
        "CheckBox-selected": select,
        "CheckBox-disabled": props.isDisabled,
        "CheckBox-invalid": props.isValid === false
      })}
      onClick={onClick}
    >
      {props.title ? (
        <Toggle title={title} selected={select}>
          <div className="CheckBox_rect">
            <SvgCheck />
          </div>
        </Toggle>
      ) : (
        <div className="CheckBox_rect">
          <SvgCheck />
        </div>
      )}
    </div>
  );
}
