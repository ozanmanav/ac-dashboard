import React, { useState, useEffect, useCallback } from "react";
import { useDebouncedCall } from "@appcircle/core/lib/hooks/useDebouncedCall";
import { FormElementType } from "@appcircle/form/lib/Form";

export type TextInputProps = {
  type?: "text" | "password";
  placeholder?: string;
  autoFocus?: boolean;
} & FormElementType<string | undefined>;

export function TextInput(props: TextInputProps) {
  const [val, setVal] = useState<string>("");
  const onChange = useDebouncedCall(
    () => {
      if (props.onChange) props.onChange && props.onChange(val);
    },
    200,
    [val]
  );
  const onBlur = props.onBlur;
  useEffect(() => {
    onChange(!val ? props.value : val);
  }, [val, onChange]);

  useEffect(() => {
    props.value !== undefined && val !== props.value && setVal(props.value);
  }, [props.value]);

  return (
    <input
      ref={ref => ref && props.autoFocus && ref.focus()}
      placeholder={props.placeholder}
      type={props.type}
      className={"Form_field " + props.className}
      value={val}
      onChange={useCallback(e => setVal(e.target.value), [])}
      onBlur={useCallback(() => onBlur && onBlur(val), [onBlur, val])}
    />
  );
}
