import React, { useState, MouseEventHandler, useEffect } from "react";

type InlineTabButtonGroupProps = {
  items: string[];
  onChange?: (index: number) => void;
  selectedIndex?: number;
};

type InlineTabButtonProps = {
  label: string;
  active?: boolean;
  onClick: MouseEventHandler;
};

function InlineTabButton(props: InlineTabButtonProps) {
  return (
    <div
      onClick={props.onClick}
      className={
        "Button InlineTabButton InlineTabButton-active--" + !!props.active
      }
    >
      {props.label}
    </div>
  );
}

export function InlineTabButtonGroup(props: InlineTabButtonGroupProps) {
  const [active, setActive] = useState(props.selectedIndex);
  useEffect(() => {
    active !== undefined && props.onChange && props.onChange(active);
  }, [active]);

  useEffect(() => {
    setActive(props.selectedIndex);
  }, [props.selectedIndex]);

  return (
    <div className={"InlineTabButtonGroup"}>
      {props.items.map((item, index) => {
        return (
          <InlineTabButton
            onClick={() => setActive(index)}
            active={active === index}
            label={item}
            key={index}
          ></InlineTabButton>
        );
      })}
    </div>
  );
}
