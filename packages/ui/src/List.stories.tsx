import React from "react";
import "./List.stories.scss";
import { List } from "./List";
import { action } from "@storybook/addon-actions";
import { ListItemsType } from "./ListItem";

export default {
  component: List,
  title: "Design System|UI/List",
  parameters: {
    info: {
      text: `
            ~~~js

            const listItems: ListItemsType = [
              { label: "List Item-1", value: 1 },
              { label: "List Item-2", value: 2 }
            ];

            <List
              data={listItems}
              onItemSelect={onItemSelect}
              onBlur={onBlur}
              isOpened={true}
            />
            ~~~
          `
    }
  },
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

const listItems: ListItemsType = [
  { label: "List Item-1", value: 1 },
  { label: "List Item-2", value: 2 }
];

export const ListWithListItems = () => {
  return (
    <List
      data={listItems}
      onItemSelect={action("onItemSelect")}
      onBlur={action("onBlur")}
      isOpened={true}
    />
  );
};
