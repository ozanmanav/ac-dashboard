import React from "react";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { ModalDialog, ModalDialogHeader } from "../ModalDialog";
import { SvgWarning } from "@appcircle/assets/lib/Warning";

type DeleteModalDialogProps = ModalComponentPropsType<{}> & {
  title: string;
  okButton: React.ReactElement;
  style?: Styles;
};
type Styles = "warning" | "info" | "danger";

export function ConfirmationModalDialog(props: DeleteModalDialogProps) {
  return (
    <ModalDialog
      header={
        <ModalDialogHeader align="center">
          <SvgWarning />
        </ModalDialogHeader>
      }
      className="ConfirmationModalDialog"
      onModalClose={props.onModalClose}
    >
      <div className="ConfirmationModalDialog_body">
        <div className="ConfirmationModalDialog_title">{props.title}</div>
        <footer className="">{props.okButton}</footer>
      </div>
    </ModalDialog>
  );
}
