import React from "react";
import { ButtonType } from "../Button";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { SubmitButton, SubmitButtonState } from "../SubmitButton";

export type ModalFooterPropsType = {
  state?: SubmitButtonState;
  onSave?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  formID?: string;
  text?: string;
  buttonStyle?: ButtonType;
  inProgressText?: string;
};
export function ModalFooter(props: ModalFooterPropsType) {
  const intl = useIntl();
  return (
    <footer>
      <SubmitButton
        type={props.buttonStyle}
        className={"Button_primaryButton "}
        formID={props.formID}
        state={props.state}
        onClick={props.onSave}
        inProgressText={
          props.inProgressText || intl.formatMessage(messages.appCommonSaving)
        }
      >
        {props.text || intl.formatMessage(messages.appCommonSave)}
      </SubmitButton>
    </footer>
  );
}
