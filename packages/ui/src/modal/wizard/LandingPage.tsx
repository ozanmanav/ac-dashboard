import React, { PropsWithChildren } from "react";
import { animated } from "react-spring";
import { FormBody } from "../../form/FormBody";
import { WizardPageType } from "./ModalWizard";
import { SvgArrow } from "@appcircle/assets/lib/Arrow";

export type LandingPageProps = {
  onPageChange: (index: number) => void;
  pages: WizardPageType[];
  animationStyle?: {};
  desc?: string;
  children?: PropsWithChildren<{}>["children"];
};
export function LandingPage(props: LandingPageProps) {
  return (
    <FormBody>
      <animated.div
        key={1}
        style={props.animationStyle}
        className="landingPage LandingPage"
      >
        <div className="LandingPage_tip">{props.desc || ""}</div>
        <div className="LandingPage_title">
          {props.children
            ? props.children
            : props.pages.map((page, index) => {
                return (
                  <div
                    key={index}
                    className="LandingPage_pageButton"
                    onClick={() => {
                      props.onPageChange(index);
                    }}
                  >
                    {page.header}
                    <div className="LandingPage_arrow">
                      <SvgArrow />
                    </div>
                  </div>
                );
              })}
        </div>
      </animated.div>
    </FormBody>
  );
}
