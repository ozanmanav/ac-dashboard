import React, {
  useState,
  useCallback,
  useMemo,
  ElementType,
  ReactElement
} from "react";
import { ModalWindow, ModalWindowType } from "../../ModalWindow";
import { LandingPageProps, LandingPage } from "./LandingPage";

export type WizardPageType = {
  name: string;
  header: string;
  component: ReactElement;
};

type ModalProps = Omit<
  ModalWindowType<any>,
  "showBackButton" | "onBackButton" | "header" | "className" | "children"
>;
// const timeout: NodeJS.Timeout;
export type ModalWizardProps = {
  landingPage?: { header: string; element?: ElementType<LandingPageProps> };
  subPages: WizardPageType[];
  header: string;
} & ModalProps;
export const ModalWizard = (props: ModalWizardProps) => {
  const landingPage = useMemo<WizardPageType>(() => {
    const LandingPageComp =
      (props.landingPage && props.landingPage.element) || LandingPage;
    return {
      name: "LANDING_PAGE",
      header: props.header,
      component: (
        <LandingPageComp
          onPageChange={index => setCurrentPageIndex(index + 1)}
          pages={props.subPages}
        />
      )
    };
  }, [props.header, props.landingPage, props.subPages]);

  const pages = useMemo(() => [landingPage, ...props.subPages], [
    landingPage,
    props.subPages
  ]);

  const [currentPageIndex, setCurrentPageIndex] = useState<number>(0);

  const onBackButton = useCallback(() => setCurrentPageIndex(0), []);

  return (
    <ModalWindow
      showBackButton={pages[currentPageIndex].name !== landingPage.name}
      onBackButton={onBackButton}
      header={pages[currentPageIndex].header}
      onModalClose={props.onModalClose}
    >
      {pages[currentPageIndex].component}
    </ModalWindow>
  );
};

