import React, {
  useState,
  useMemo,
  ReactElement,
  ComponentType,
  useEffect
} from "react";
import { LandingPageProps, LandingPage } from "./LandingPage";

export type WizardPageType = {
  name: string;
  header: string;
  component: ReactElement;
};

// const timeout: NodeJS.Timeout;
export type LandingDetailLayoutProps = {
  landingPage?: { header: string; element?: ComponentType<LandingPageProps> };
  header?: string;
  subPages: WizardPageType[];
  onPageChange?: (page: WizardPageType, index: number) => void;
  selectedPageIndex?: number;
};
export const LandingDetailLayout = (props: LandingDetailLayoutProps) => {
  const landingPage = useMemo<WizardPageType>(() => {
    const LandingPageComp =
      (props.landingPage && props.landingPage.element) || LandingPage;
    return {
      name: "LANDING_PAGE",
      header: props.header || "",
      component: (
        <LandingPageComp
          onPageChange={index => {
            setCurrentPageIndex(index + 1);
            props.onPageChange &&
              props.onPageChange(props.subPages[index], index + 1);
          }}
          pages={props.subPages}
        />
      )
    };
  }, [props.header, props.landingPage, props.subPages]);

  const pages = useMemo(() => [landingPage, ...props.subPages], [
    landingPage,
    props.subPages
  ]);

  const [currentPageIndex, setCurrentPageIndex] = useState<number>(0);
  useEffect(() => {
    if (props.selectedPageIndex !== undefined) {
      setCurrentPageIndex(props.selectedPageIndex);
    }
  }, [props.selectedPageIndex]);

  return pages[currentPageIndex].component;
};
