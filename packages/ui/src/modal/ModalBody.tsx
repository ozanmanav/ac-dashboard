import React, { PropsWithChildren } from "react";
import { ScrollView } from "../ScrollView";

export function ModalBody(
  props: PropsWithChildren<{
    autoScrollToBottom?: boolean;
    showScrollbarNav?: boolean;
    scrollBreakpointSelector?: string;
  }>
) {
  return (
    <ScrollView
      breakPointSelector={props.scrollBreakpointSelector}
      showNavBar={props.showScrollbarNav}
      className="ModalBody"
      autoScrollToBottom={props.autoScrollToBottom}
    >
      {props.children}
    </ScrollView>
  );
}
