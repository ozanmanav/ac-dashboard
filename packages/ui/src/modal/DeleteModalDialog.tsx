import React, { useMemo } from "react";
import { Form, FormProps, FormStatusType } from "@appcircle/form/lib/Form";
import { FormBody } from "../form/FormBody";
import { ModalFooter } from "./ModalFooter";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { TextInputProps, TextInput } from "../form/TextInput";
import { ButtonType } from "../Button";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
import { ModalDialog } from "../ModalDialog";
import { WarningIcon } from "@appcircle/assets/lib/WarningIcon";

export type DeleteModalDialogProps = {
  // icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
  ignoreTaskId?: boolean;
  errorMessage: string;
  successMessage: string;
  onSuccess: TaskPropsType["onSuccess"];
  title: string;
  endpoint: string;
  iconURL?: string;
  name: string;
  placeHolder: string;
  header?: string;
  entityID?: string;
  onBeforeSubmmit?: FormProps["onBeforeSubmit"];
  skipNameValidation?: boolean;
};

export function DeleteModalDialog(
  props: ModalComponentPropsType<DeleteModalDialogProps>
) {
  const intl = useIntl();
  const taskProps = useMemo<TaskPropsType>(
    () => ({
      entityID: props.entityID,
      errorMessage: () => props.errorMessage,
      successMessage: () => props.successMessage,
      onSuccess: props.onSuccess,
      ignoreTaskId: props.ignoreTaskId
    }),
    [props.errorMessage, props.entityID, props.onSuccess, props.successMessage]
  );
  return (
    <ModalDialog
      header={props.header || ""}
      className="DeleteModalView"
      onModalClose={props.onModalClose}
    >
      <div className="DeleteModalView_icons">
        <WarningIcon />
      </div>
      <div className="DeleteModalView_content">
        <span>{props.title}</span>
        <Form
          formID="deleteEntityModal"
          method="DELETE"
          targetPath={props.endpoint}
          taskProps={taskProps}
          onBeforeSubmit={props.onBeforeSubmmit}
          status={
            props.skipNameValidation === true
              ? ({
                  isDirty: true
                } as FormStatusType)
              : undefined
          }
        >
          <FormBody noScroll>
            <div className="DeleteModalView_appInfo">
              {!!props.iconURL && <img src={props.iconURL} />}
              <div>{props.name}</div>
            </div>
            {
              <FormComponent<TextInputProps>
                formID="deleteEntityModal"
                placeholder={props.placeHolder}
                autoFocus={true}
                formProps={{
                  rule: {
                    confirmValue: props.name,
                    type: "confirm-value"
                  },
                  value: !!props.skipNameValidation
                    ? !!props.name
                      ? props.name
                      : undefined
                    : undefined,
                  name: "name",
                  required: true
                }}
                element={!!!props.skipNameValidation ? TextInput : null}
              />
            }
          </FormBody>
          <ModalFooter
            text={intl.formatMessage(messages.appCommonDelete)}
            buttonStyle={ButtonType.DANGER}
            inProgressText={intl.formatMessage(messages.appCommonDeleting)}
            formID="deleteEntityModal"
          />
        </Form>
      </div>
    </ModalDialog>
  );
}
