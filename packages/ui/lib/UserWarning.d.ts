import { PropsWithChildren } from "react";
export declare function UserWarning(props: PropsWithChildren<{
    messages?: string[];
}>): JSX.Element;
//# sourceMappingURL=UserWarning.d.ts.map