var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React, { useMemo } from "react";
import classNames from "classnames";
import { SwitchPanel } from "./SwitchPanel";
export function SwitchPanelGroup(props) {
    var value = useMemo(function () { return props.value || []; }, [props.value]);
    return (React.createElement("div", { className: classNames(props.className, "SwitchPanelGroup", {
            "SwitchPanelGroup-disabled": props.isDisabled,
            "SwitchPanelGroup-invalid": props.isValid === false
        }) }, props.items.map(function (item, index) { return (React.createElement("div", { key: item.value, className: classNames("ComponentGroup_item", {
            ".ComponentGroup_item-selected": props.value === item.value
        }) },
        React.createElement(SwitchPanel, { onChange: function (val) {
                var values = val === true
                    ? __spreadArrays(value.filter(function (v) { return item.value !== v; }), [item.value]) : value.filter(function (v) { return item.value !== v; });
                props.onChange && props.onChange(values);
            }, title: item.label, isSelected: value.some(function (v) { return v === item.value; }) }))); })));
}
//# sourceMappingURL=SwitchPanelGroup.js.map