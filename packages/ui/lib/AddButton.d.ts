import { PropsWithChildren } from "react";
import { IconButtonState } from "./IconButton";
import { ButtonProps } from "./Button";
export declare type AddButtonProps = PropsWithChildren<{
    text: string;
    onClick?: () => void;
    size?: ButtonProps["size"];
    className?: string;
    state?: IconButtonState;
    tabIndex?: number;
}>;
export declare function AddButton(props: AddButtonProps): JSX.Element;
//# sourceMappingURL=AddButton.d.ts.map