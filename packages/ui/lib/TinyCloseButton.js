import React from "react";
import { ComponentStyle } from "@appcircle/core/lib/ComponentStyle";
import { SvgCloseSmall } from "@appcircle/assets/lib/CloseSmall";
export function TinyCloseButton(props) {
    return (React.createElement("div", { className: "TinyCloseButton TinyCloseButton_" +
            (props.type || ComponentStyle.Secondary), onClick: props.onClick },
        React.createElement(SvgCloseSmall, null)));
}
//# sourceMappingURL=TinyCloseButton.js.map