import { ReactNode } from "react";
import { ProgressViewProps } from "./ProgressView";
import { TaskStatusType } from "@appcircle/taskmanager/lib/TaskStatusType";
import { ConnectTaskProps } from "./taskmanager/ConnectTask";
export declare type TaskSpinnerProgressViewProps = {
    progressText?: string;
    entityID?: string | string[] | undefined;
    taskID?: string;
    errorComponent?: ConnectTaskProps["errorComponent"];
    hasRelativeRoot?: boolean;
    isOverlay?: boolean;
    progressType?: ProgressViewProps["type"];
    children?: ((status: TaskStatusType | undefined) => ReactNode) | ReactNode;
    tasks?: TaskStatusType[];
    onClick?: () => void;
};
export declare function TaskProgressView(props: TaskSpinnerProgressViewProps): JSX.Element;
//# sourceMappingURL=TaskProgressView.d.ts.map