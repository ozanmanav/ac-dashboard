import { PropsWithChildren } from "react";
export declare type AddEmailComponentProps = PropsWithChildren<{
    emails: Array<string>;
    onAddEmail: (texts: string[]) => void;
    tabIndex?: number;
}>;
export declare function AddEmailComponent(props: AddEmailComponentProps): JSX.Element;
//# sourceMappingURL=AddEmailComponent.d.ts.map