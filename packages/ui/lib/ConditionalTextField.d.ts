import { PropsWithChildren, FunctionComponent, SVGProps } from "react";
export declare type ConditionalTextFieldProps = PropsWithChildren<{
    placeHolder: string;
    text: string;
    resetState?: boolean;
    icon: FunctionComponent<SVGProps<SVGSVGElement>>;
    rules: Array<(text: string) => boolean>;
    onWrongInput?: (texts: string[]) => void;
    onSuccessInput: (texts: string[]) => void;
    onChange?: (text: string) => void;
    onEnterKey?: (text: string) => void;
    tabIndex?: number;
}>;
export declare enum TextState {
    READY = 0,
    WRITING = 1,
    SUCCESS = 2,
    ERROR = 3
}
export declare function ConditionalTextField(props: ConditionalTextFieldProps): JSX.Element;
//# sourceMappingURL=ConditionalTextField.d.ts.map