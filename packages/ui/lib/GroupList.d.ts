import { FunctionComponent, SVGProps, ReactElement } from "react";
export declare type GroupListItemType = {
    id: any;
    name: string;
    icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
    count?: number;
    editMode?: boolean;
};
export declare type GroupListType = {
    items: GroupListItemType[];
    selectedGroupID: any;
    icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
    fallback: ReactElement;
};
declare type GroupListProps = {
    onError: (err: string) => void;
    onChange?: (name: string) => void;
    onEnter: (name: string) => void;
    onBlurOrEscape?: () => void;
    onItemSelect?: (item: GroupListItemType, index: number) => void;
    onValidate?: (oldName: string, groupName: string) => string;
} & GroupListType;
export declare function GroupList(props: GroupListProps): JSX.Element;
export {};
//# sourceMappingURL=GroupList.d.ts.map