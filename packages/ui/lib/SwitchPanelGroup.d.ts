/// <reference types="react" />
import { FormElementType } from "@appcircle/form/lib/Form";
import { ComponentGroupType } from "./form/ComponentGroup";
export declare type SwitchPanelGroupProps = ComponentGroupType & FormElementType<(string | number | null)[]>;
export declare function SwitchPanelGroup(props: SwitchPanelGroupProps): JSX.Element;
//# sourceMappingURL=SwitchPanelGroup.d.ts.map