import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useState, useCallback } from "react";
import React from "react";
import { Spinner } from "./Spinner";
import { ProgressView } from "./ProgressView";
export var LoadableContentStatus;
(function (LoadableContentStatus) {
    LoadableContentStatus[LoadableContentStatus["IDLE"] = 0] = "IDLE";
    LoadableContentStatus[LoadableContentStatus["LOADED"] = 1] = "LOADED";
    LoadableContentStatus[LoadableContentStatus["IN_PROGRESS"] = 2] = "IN_PROGRESS";
    LoadableContentStatus[LoadableContentStatus["ERROR"] = 3] = "ERROR";
})(LoadableContentStatus || (LoadableContentStatus = {}));
export function withLoadableContent(Component, PreloaderComponent, // set default component if it exists
ErrorComponent) {
    if (PreloaderComponent === void 0) { PreloaderComponent = Spinner; }
    return function (props) {
        var _a = useState(LoadableContentStatus.IDLE), status = _a[0], setStatus = _a[1];
        var _b = useTaskManager(), addTask = _b.addTask, createApiTask = _b.createApiTask;
        var onRunTask = useCallback(function (task) {
            setStatus(LoadableContentStatus.IN_PROGRESS);
            addTask(createApiTask.apply(void 0, task()));
        }, [addTask, createApiTask]);
        return (React.createElement("div", { className: "LoadableContent" }, ErrorComponent && status === LoadableContentStatus.ERROR ? (React.createElement(ErrorComponent, null)) : (React.createElement(ProgressView, { indicatorComp: React.createElement(PreloaderComponent, null), isInProgress: LoadableContentStatus.IN_PROGRESS === status },
            React.createElement(Component, { onRunTask: onRunTask })))));
    };
}
//# sourceMappingURL=withLoadableContent.js.map