import React, { useState, useCallback, useRef } from "react";
import { List } from "./List";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
function convertSuggestions(suggestions) {
    return suggestions.map(function (item) { return ({ label: item, value: item }); });
}
export function AutoSuggestionField(props) {
    var _a = useState(), currentValue = _a[0], setCurrentValue = _a[1];
    var _b = useState(function () {
        return convertSuggestions(props.onFilter(undefined));
    }), suggestions = _b[0], setSuggestions = _b[1];
    var _c = useState(false), focussed = _c[0], setFocussed = _c[1];
    var element = React.Children.toArray(props.children)[0];
    var onTextChange = useCallback(function (val) {
        setSuggestions(convertSuggestions(props.onFilter(val)));
        setCurrentValue(val);
        val && setFocussed(true);
    }, [props]);
    var onSelect = props.onSelect;
    var onBlur = useCallback(function () {
        setFocussed(false);
        setCurrentValue(undefined);
    }, []);
    var onFocus = useCallback(function () {
        setFocussed(true);
    }, []);
    var root = useRef(null);
    var blurHandler = useCallback(function () {
        setFocussed(false);
    }, []);
    useGlobalClickService(["List"], root, focussed ? blurHandler : null);
    return (React.createElement("div", { ref: root, className: "AutoSuggestionField" },
        React.isValidElement(element) &&
            React.cloneElement(element, {
                onFocus: onFocus,
                text: currentValue,
                onTextChange: onTextChange
            }),
        React.createElement(List, { onBlur: onBlur, isOpened: focussed && !!suggestions.length, data: suggestions, onItemSelect: useCallback(function (val) {
                setCurrentValue(val.label);
                onSelect && onSelect(val.label);
            }, [onSelect]) })));
}
//# sourceMappingURL=AutoSuggestionField.js.map