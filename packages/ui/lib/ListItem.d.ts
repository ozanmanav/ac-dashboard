export declare type ListItemType = {
    label: string;
    value: any;
    expression?: string;
};
export declare type ListItemsType = ListItemType[];
//# sourceMappingURL=ListItem.d.ts.map