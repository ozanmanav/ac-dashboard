import { PropsWithChildren } from "react";
export declare type NavBarListHeaderProps = PropsWithChildren<{
    text: string;
}>;
export declare function NavBarListHeader(props: NavBarListHeaderProps): JSX.Element;
//# sourceMappingURL=NavBarListHeader.d.ts.map