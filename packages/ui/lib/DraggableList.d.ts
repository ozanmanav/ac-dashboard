/// <reference types="react" />
declare type DraggableListItemProps = {
    id: any;
    text: string;
    index: number;
    moveCard: (dragIndex: number, hoverIndex: number) => void;
    onDragEnd: (index: number) => void;
    onSelect: (index: number) => void;
    description?: string;
    disabled?: boolean;
};
export declare type ListItemDataType = {
    id: number;
    title: string;
    description: string;
    disabled?: boolean;
};
export declare type ContainerState = {
    items: ListItemDataType[];
};
declare type DraggableListProps = {
    items: ListItemDataType[];
    onSelect: DraggableListItemProps["onDragEnd"];
    onChange: (newItems: ListItemDataType[]) => void;
};
export declare function DraggableList(props: DraggableListProps): JSX.Element;
export {};
//# sourceMappingURL=DraggableList.d.ts.map