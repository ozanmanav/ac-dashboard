import { PropsWithChildren } from "react";
export declare type ProgressViewProps = PropsWithChildren<{
    indicatorComp?: JSX.Element;
    isInProgress?: boolean;
    isOverlay?: boolean;
    progressText?: string;
    type?: "spinner" | "stripe";
    onClick?: () => void;
}>;
export declare function ProgressView(props: ProgressViewProps): JSX.Element;
//# sourceMappingURL=ProgressView.d.ts.map