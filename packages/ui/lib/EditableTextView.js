import React, { useState, useRef, useCallback, useEffect, useMemo } from "react";
import classNames from "classnames";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { ScrollView } from "./ScrollView";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
export function EditableTextView(props) {
    var _a = useState(props.value), text = _a[0], setText = _a[1];
    var _b = useState(false), active = _b[0], setActive = _b[1];
    var _c = useState(false), invalid = _c[0], setInvalid = _c[1];
    var activated = active && props.activated !== false;
    // const [editable, setEditable] = useState(false);
    var editable = props.editable === undefined ? false : props.editable;
    var elemRef = useRef(null);
    useEffect(function () {
        var _a;
        elemRef.current && text !== props.value && setText((_a = props.value, (_a !== null && _a !== void 0 ? _a : "")));
    }, [props.value]);
    useEffect(function () {
        elemRef.current &&
            elemRef.current.innerText !== text &&
            (elemRef.current.innerText = text);
    }, [text]);
    function activate(active, keyCode) {
        editable &&
            elemRef.current &&
            (elemRef.current.contentEditable = active ? "true" : "false");
        active
            ? props.onFocus && props.onFocus()
            : props.onBlur &&
                elemRef.current &&
                props.onBlur(elemRef.current.innerText, keyCode);
        setActive(active);
        setInvalid(false);
    }
    useGlobalClickService(null, elemRef, useCallback(function () {
        active &&
            setTimeout(function () {
                activate(false);
            }, 1);
    }, [active, invalid]));
    useEffect(function () {
        !activated &&
            active &&
            elemRef.current &&
            (function () {
                elemRef.current.blur();
                activate(false);
            })();
    }, [activated]);
    return (React.createElement("div", { className: "EditableTextView EditableTextView-multiline--" +
            !!props.multiline +
            "EditableTextView-active--" +
            !!active, style: useMemo(function () { return ({
            maxHeight: props.maxHeight
        }); }, [props.maxHeight]) },
        React.createElement(ScrollView, null,
            React.createElement("div", { ref: elemRef, tabIndex: props.tabIndex || -1, className: classNames("EditableTextView_textArea", props.className, "EditableTextView_textArea-active--" + !!active, "EditableTextView_textArea-multiline--" + !!props.multiline), spellCheck: false, style: useMemo(function () { return ({
                    height: props.multiline ? "auto" : ""
                }); }, [props.multiline]), onClick: function (e) {
                    !active && activate(true);
                    elemRef.current && elemRef.current.focus();
                }, onFocus: function () {
                    !active && activate(true);
                    elemRef.current && elemRef.current.focus();
                }, "data-placeholder": props.placeHolder, suppressContentEditableWarning: true, onPaste: function (event) {
                    event.clipboardData.setData(event.clipboardData.types[0], event.clipboardData.getData(event.clipboardData.types[0]));
                    var data = event.clipboardData.getData(event.clipboardData.types[0]);
                    // remove useless whitespaces if no multiline.
                    props.multiline !== true &&
                        (data = data.replace(/[\r\n\t]*/gm, ""));
                    // elemRef.current && (elemRef.current.innerText = data);
                    document.execCommand("inserttext", false, data);
                    event.preventDefault();
                }, onInput: useCallback(function (e) {
                    setInvalid(true);
                    props.onChange && props.onChange(e.currentTarget.innerText);
                    // setText(e.currentTarget.innerText);
                }, [props.onChange]), onKeyDown: function (e) {
                    if (props.multiline !== true && e.keyCode === KeyCode.ENTER_KEY) {
                        e.preventDefault();
                        activate(false, KeyCode.ENTER_KEY);
                    }
                    else if (e.keyCode === KeyCode.ESC_KEY) {
                        e.preventDefault();
                        activate(false, KeyCode.ESC_KEY);
                    }
                    e.stopPropagation();
                    e.defaultPrevented = true;
                } }))));
}
//# sourceMappingURL=EditableTextView.js.map