import React, { useRef, useState, useMemo, useCallback, useEffect } from "react";
import classNames from "classnames";
import { animated, useSpring } from "react-spring";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { useIntl, defineMessages } from "react-intl";
import CommonMessages from "@appcircle/shared/lib/messages/common";
import { TextButton } from "./TextButton";
var messages = defineMessages({
    uploadError: {
        id: "app.component.AddNewFile.error",
        defaultMessage: "Error occurred when the file uploading"
    }
});
export var AddNewFileState;
(function (AddNewFileState) {
    AddNewFileState["IDLE"] = "IDLE";
    AddNewFileState["UPLOADING"] = "UPLOADING";
    AddNewFileState["ERROR"] = "ERROR";
})(AddNewFileState || (AddNewFileState = {}));
var IdleComponent = function (props) {
    var intl = useIntl();
    return (React.createElement(React.Fragment, null,
        props.input,
        React.createElement("div", { className: "AddNewFile_text" }, props.text),
        React.createElement("div", { className: "AddNewFile_text AddNewFile-invalid--" + props.invalid }, props.errorText),
        React.createElement("div", { className: "AddNewFile_input" }, !props.hidden && (React.createElement(TextButton, { title: intl.formatMessage(CommonMessages.appCommonBrowse), disabled: !!props.disabled, onClick: function () {
                props.onClick();
                // inputRef.current && inputRef.current.click();
            } })))));
};
var FileSelectedComponent = function (props) {
    var intl = useIntl();
    return (React.createElement(React.Fragment, null,
        props.input,
        React.createElement("div", { className: "AddNewFile_selectedText" }, props.text),
        React.createElement("div", { className: "AddNewFile_input" },
            React.createElement(TextButton, { title: intl.formatMessage(CommonMessages.appCommonChange), onClick: function () {
                    props.onClick();
                } }))));
};
//
var UploadingComponent = function (props) {
    var intl = useIntl();
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "AddNewFile_progressBar" },
            React.createElement("div", { style: {
                    width: (props.progress !== undefined ? props.progress : 0) + "%"
                } })),
        React.createElement("div", { className: "AddNewFile_text" }, intl.formatMessage(CommonMessages.appCommonUploading)),
        React.createElement("div", { className: "AddNewFile_text" }, props.fileName)));
};
var ProcessingComponent = function (props) {
    var intl = useIntl();
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "AddNewFile_progressBar" },
            React.createElement("div", { style: {
                    width: "100%"
                } })),
        React.createElement("div", { className: "AddNewFile_text" },
            intl.formatMessage(CommonMessages.appCommonUploadCompleted),
            " ",
            intl.formatMessage(CommonMessages.appCommonProcessing)),
        React.createElement("div", { className: "AddNewFile_text" }, props.fileName)));
};
var ErrorComponent = function (props) {
    var intl = useIntl();
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "AddNewFile_warning" }, "Warning!"),
        React.createElement("div", { className: "AddNewFile_text" }, intl.formatMessage(messages.uploadError)),
        React.createElement("div", { className: "AddNewFile_input" },
            React.createElement(TextButton, { title: intl.formatMessage(CommonMessages.appCommonRetry), onClick: function () {
                    props.onRetry();
                } }))));
};
export function AddNewFile(props) {
    var _a = useState(props.status || "idle"), status = _a[0], setStatus = _a[1];
    useEffect(function () {
        props.onStateChange && props.onStateChange(status);
    }, [props, status]);
    var _b = useState(false), isDropCaptured = _b[0], setIsDropCaptured = _b[1];
    useEffect(function () {
        props.status && setStatus(props.status);
    }, [props.status]);
    var getStatusByEntityID = useTaskManager().getStatusByEntityID;
    var task = props.entityID
        ? getStatusByEntityID(props.entityID)
        : null;
    useEffect(function () {
        var task = props.entityID && getStatusByEntityID(props.entityID);
        task &&
            setStatus(task.status === "ADDED" ||
                task.status === "IN_PROGRESS" ||
                task.status === "WAITING_RESPONSE"
                ? task.status === "WAITING_RESPONSE" || status === "processing"
                    ? "processing"
                    : "uploading"
                : task.status === "ERROR"
                    ? "error"
                    : "idle");
    }, [getStatusByEntityID, props.entityID, status, task]);
    // const taskStatus = task ? task.status : "";
    var inputRef = useRef(null);
    var uploadedFileName = useRef("");
    var extensions = props.types || [];
    var acceptedExtensions = useMemo(function () { return extensions.map(function (type) { return formatType(type); }).join(","); }, [extensions]);
    var onChange = useCallback(function (e) {
        e.preventDefault();
        var fileName = e.currentTarget &&
            e.currentTarget.files &&
            e.currentTarget.files[0] &&
            e.currentTarget.files[0].name;
        var isValidFileName = true;
        if (!fileName) {
            return;
        }
        uploadedFileName.current = fileName;
        //CHECK FILE(S)
        isValidFileName = props.types.length
            ? extensions.some(function (type) {
                var comparator = formatType(type);
                if (fileName.includes(comparator)) {
                    return true;
                }
                return false;
            })
            : true;
        //INVALID FILE(S)
        if (!isValidFileName) {
            e.currentTarget.value = null;
            uploadedFileName.current = "";
            // props.onStateChange && props.onStateChange("error");
            setDragging(false);
            setIsDropCaptured(false);
            setStatus("invalid");
            return;
        }
        props.onFileUpload(e.currentTarget.files[0]);
        if (props.autoUpload === false) {
            setStatus("file-selected");
            return;
        }
        //UPLOAD FILE(S)
        // props.onStateChange && props.onStateChange("uploading");
        setIsDropCaptured(false);
        setStatus("uploading");
        inputRef.current && (inputRef.current.value = "");
    }, [extensions, props]);
    var _c = useState(false), dragging = _c[0], setDragging = _c[1];
    var progress = props.progress === undefined
        ? task && task.data
            ? task.data.json
            : 0
        : props.progress;
    var onDragOverCapture = useCallback(function (e) {
        setStatus("idle");
        setDragging(true);
        !isDropCaptured && setIsDropCaptured(true);
    }, [isDropCaptured]);
    var onDragLeaveCapture = useCallback(function () {
        setDragging(false);
        isDropCaptured && setIsDropCaptured(false);
    }, [isDropCaptured]);
    var Icons = useMemo(function () { return props.icons.map(function (Icon, index) { return React.createElement(Icon, { key: index }); }); }, [props.icons]);
    var inputComponent = useMemo(function () { return (React.createElement("input", { ref: inputRef, type: "file", accept: acceptedExtensions, onDragOverCapture: onDragOverCapture, onDragLeaveCapture: onDragLeaveCapture, onChange: onChange })); }, [acceptedExtensions, onChange, onDragLeaveCapture, onDragOverCapture]);
    // TODO: Seperate components
    var Partial = useMemo(function () {
        switch (status) {
            case "invalid":
            case "idle":
                return (React.createElement(IdleComponent, { input: inputComponent, onClick: function () {
                        inputRef.current && inputRef.current.click();
                    }, hidden: dragging, text: props.text, errorText: "Invalid file type", disabled: props.disabled, invalid: status === "invalid" }));
            case "file-selected":
                return (React.createElement(FileSelectedComponent, { input: inputComponent, onClick: function () {
                        inputRef.current && inputRef.current.click();
                    }, text: uploadedFileName.current }));
            case "uploading":
                if (progress === 100) {
                    setStatus("processing");
                }
                return (React.createElement(UploadingComponent, { progress: progress, fileName: uploadedFileName.current }));
            case "error":
                return (React.createElement(ErrorComponent, { onRetry: function () {
                        setStatus("idle");
                        // props.onStateChange && props.onStateChange("idle");
                    } }));
            case "processing":
                return React.createElement(ProcessingComponent, { fileName: uploadedFileName.current });
        }
    }, [inputComponent, progress, props.disabled, props.text, status]);
    return (React.createElement("div", { className: classNames("AddNewFile", {
            "AddNewFile-isDropCaptured": isDropCaptured
        }) },
        React.createElement("div", null,
            React.createElement("div", { className: "AddNewFile_icons" }, Icons),
            Partial)));
}
var calc = function (x, y) { return [
    -(y - 75 / 2) / 40,
    (x - 75 / 2) / 40,
    2
]; };
var trans = function (x, y, s) {
    return "perspective(800px) rotateX(" + x + "deg) rotateY(" + y + "deg) scale(" + s + ")";
};
function AnimatedIcon(props) {
    var _a = useSpring(function () { return ({
        xys: [0, 0, 1],
        config: { mass: 5, tension: 650, friction: 60 }
    }); }), styles = _a[0], set = _a[1];
    return (React.createElement(animated.div, { onMouseMove: function (_a) {
            var x = _a.clientX, y = _a.clientY;
            return set({ xys: calc(x, y) });
        }, onMouseLeave: function () { return set({ xys: [0, 0, 1] }); }, style: { transform: styles.xys.interpolate(trans) } }, props.children));
}
function formatType(type) {
    return type[0] === "." ? type : "." + type;
}
//# sourceMappingURL=AddNewFile.js.map