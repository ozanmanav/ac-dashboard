import { PropsWithChildren } from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
export declare type EditableTextViewProps = PropsWithChildren<{
    value?: string | number | null | undefined;
    className?: string;
    onChange?: (text: string) => void;
    onFocus?: () => void;
    onBlur?: (text?: string, keyCode?: KeyCode.ENTER_KEY | KeyCode.ESC_KEY) => void;
    onKeyEnter?: (text?: string) => void;
    editable?: boolean;
    placeHolder?: string;
    tabIndex?: number;
    multiline?: boolean;
    maxHeight?: "auto" | number;
    activated?: boolean;
}>;
export declare function EditableTextView(props: EditableTextViewProps): JSX.Element;
//# sourceMappingURL=EditableTextView.d.ts.map