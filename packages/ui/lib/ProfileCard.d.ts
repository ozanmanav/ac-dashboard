import { PropsWithChildren, ReactElement } from "react";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import { TaskStatus } from "@appcircle/taskmanager/lib/TaskStatus";
import { ContextMenuItemDataType } from "./menus/ContextMenuItem";
export declare enum ProfileCardStatus {
    READY = 0,
    EDITING = 1,
    INPROGRESS = 2
}
export declare type ProfileCardFooterData = {
    header: string;
    text: string;
    enable: boolean;
};
export declare type ProfileCardDataType = {
    isPinned: boolean;
    id: string;
    isTemporary: boolean;
    name: string;
};
export declare type ProfileCardServiceType = {
    rename: (id: string, newName: string, isTemporary: boolean) => void;
    pin: (id: string, isPinned: boolean) => void;
    delete: (id: string, isTemporary?: boolean) => void;
    doesNameExist: (name: string) => boolean;
};
export declare type ProfileCardProps<T extends ProfileCardDataType = ProfileCardDataType> = PropsWithChildren<{
    data: T;
    service: ProfileCardServiceType;
    profileDetailLink: string;
    footerData: ProfileCardFooterData[];
    index: number;
    onBlur: () => void;
    onEditModeChange: (editMode: boolean) => void;
    contextMenuItems: ContextMenuItemDataType[];
    editMode: boolean;
    onRenameError: (error: NameValidState, newProfileName: string) => void;
    onContextMenuItemSelected: (index: number) => void;
    IconTemplate?: ReactElement;
    onStatusChange: (status?: TaskStatus) => void;
    highlighted?: boolean;
}>;
export declare function ProfileCard(props: ProfileCardProps): JSX.Element;
//# sourceMappingURL=ProfileCard.d.ts.map