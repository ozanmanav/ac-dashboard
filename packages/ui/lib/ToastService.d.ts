/// <reference types="react" />
import { Message } from "@appcircle/shared/lib/Module";
declare type removeMessageFn = (payload: Message) => void;
export declare type IToastComponent = {
    message: Message;
    index: number;
    onRemove: removeMessageFn;
};
export declare type ToastActionCreatorsType = {
    pushToast: (message: Message, payload: any) => void;
    updateToast: (id: string, payload: any) => void;
    removeToast: (id: string, payload: any) => void;
};
export declare const DEFAULT_MESSAGE_LIFE = 2000;
export declare const DEFAULT_TOAST_HEIGHT = 30;
export declare const TOAST_VERTICAL_GAP = 13;
export declare const ToastService: (props: {}) => JSX.Element;
export declare type MessageStatus = "keep-open" | "show" | "hide" | "delete";
export {};
//# sourceMappingURL=ToastService.d.ts.map