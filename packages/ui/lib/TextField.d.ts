import React, { PropsWithChildren } from "react";
export declare type TextFieldProps = PropsWithChildren<{
    text: string;
    placeHolder?: string;
    className?: string;
    onChange?: (text: string) => void;
    onChangeEnd?: (text: string, isCancel?: boolean) => void;
    onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    onBlur?: () => void;
    editMode?: boolean;
    disabled?: boolean;
    onEnter?: (text: string) => void;
    onBlurOrEscape?: () => void;
    onValidate?: (oldName: string, groupName: string) => string;
    multiLineMode?: boolean;
}>;
export declare function TextField(props: TextFieldProps): JSX.Element;
//# sourceMappingURL=TextField.d.ts.map