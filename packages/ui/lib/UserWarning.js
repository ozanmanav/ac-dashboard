import React, { Children } from "react";
import { SvgWarning } from "@appcircle/assets/lib/Warning";
export function UserWarning(props) {
    return Children.count(props.children) > 0 ||
        (props.messages && props.messages.length > 0) ? (React.createElement("div", { className: "UserWarning" },
        React.createElement("div", null,
            React.createElement(SvgWarning, null)),
        props.messages && props.messages.length > 0 ? (React.createElement("div", null, props.messages.map(function (message, index) { return (React.createElement("div", { className: "UserWarning_message", key: index }, message)); }))) : (React.createElement("div", null, props.children)))) : (React.createElement(React.Fragment, null));
}
//# sourceMappingURL=UserWarning.js.map