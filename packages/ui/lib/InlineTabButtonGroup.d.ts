/// <reference types="react" />
declare type InlineTabButtonGroupProps = {
    items: string[];
    onChange?: (index: number) => void;
    selectedIndex?: number;
};
export declare function InlineTabButtonGroup(props: InlineTabButtonGroupProps): JSX.Element;
export {};
//# sourceMappingURL=InlineTabButtonGroup.d.ts.map