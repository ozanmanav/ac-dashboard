import React, { useState, useEffect } from "react";
import classNames from "classnames";
export function TabView(props) {
    // const titles = props.tabs.map(tab => tab.title);
    var _a = useState(props.selectedIndex || 0), selectedIndex = _a[0], setSelectedIndex = _a[1];
    useEffect(function () {
        props.onChange && props.onChange(selectedIndex);
    }, [selectedIndex]);
    return (React.createElement("div", { className: "TabView" },
        React.createElement("div", { className: "TabView_tabs" }, props.tabs.map(function (item, index) { return (React.createElement("div", { key: item.title, className: classNames("TabView_tabs_tab", {
                "TabView_tabs_tab-selected": selectedIndex === index
            }), onClick: function (e) {
                selectedIndex !== index && setSelectedIndex(index);
            } },
            React.createElement("div", null,
                item.title,
                " ",
                item.indicator && React.createElement("div", { className: "requiredIndicator" })))); })),
        React.createElement("div", { className: "TabView_content" }, props.tabs[selectedIndex].component)));
}
//# sourceMappingURL=TabView.js.map