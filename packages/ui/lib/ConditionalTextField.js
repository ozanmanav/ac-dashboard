import React, { useState, useEffect, useCallback } from "react";
import { IconTextField } from "./IconTextField";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import classNames from "classnames";
export var TextState;
(function (TextState) {
    TextState[TextState["READY"] = 0] = "READY";
    TextState[TextState["WRITING"] = 1] = "WRITING";
    TextState[TextState["SUCCESS"] = 2] = "SUCCESS";
    TextState[TextState["ERROR"] = 3] = "ERROR";
})(TextState || (TextState = {}));
function validateInput(text, rules) {
    return !rules.some(function (rule) { return !rule(text); });
}
function valideState(text, rules) {
    var valids = [], invalids = [];
    var texts = text.split(/\s|,|;|:/);
    texts.forEach(function (text) {
        if (!text)
            return;
        if (validateInput(text, rules)) {
            valids.push(text);
        }
        else {
            invalids.push(text);
        }
    });
    return [invalids, valids];
}
export function ConditionalTextField(props) {
    var _a = useState(TextState.READY), state = _a[0], setState = _a[1];
    var placeHolder = props.placeHolder, icon = props.icon, rules = props.rules, onSuccessInput = props.onSuccessInput, onWrongInput = props.onWrongInput, onChange = props.onChange;
    useEffect(function () {
        var _a = valideState(props.text, rules), invalids = _a[0], valids = _a[1];
        if (invalids.length) {
            onWrongInput && onWrongInput(invalids);
            setState(TextState.ERROR);
        }
        else if (valids.length) {
            setState(TextState.SUCCESS);
            onSuccessInput(valids);
        }
    }, [onSuccessInput, onWrongInput, props.text, rules]);
    var onTextChange = useCallback(function (text) {
        onChange && onChange(text);
    }, [onChange]);
    var onKeyDown = useCallback(function (e) {
        if (e.keyCode === KeyCode.ENTER_KEY && state === TextState.SUCCESS) {
            // valideState(props.text, rules);
            props.onEnterKey && props.onEnterKey(props.text);
            // setState({ ...state, text: "" });
        }
        else if (e.keyCode === KeyCode.ESC_KEY) {
            setState(TextState.READY);
        }
        else {
        }
    }, 
    // eslint-disable-next-line
    [props.onEnterKey]);
    return (React.createElement(IconTextField, { classNameModifier: classNames("ConditionalTextField", {
            "ConditionalTextField-error": TextState.ERROR === state,
            "ConditionalTextField-success": TextState.SUCCESS === state
        }), text: props.text, placeHolder: placeHolder, onChange: onTextChange, icon: icon, onKeyDown: onKeyDown, tabIndex: props.tabIndex }));
}
//# sourceMappingURL=ConditionalTextField.js.map