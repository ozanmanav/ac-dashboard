import { PropsWithChildren } from "react";
export declare type ThreeDotsToggleProps = PropsWithChildren<{
    size?: "md" | "lg" | "sm";
    isOpen?: false;
}>;
export declare function ThreeDotsToggle(props: ThreeDotsToggleProps): JSX.Element;
//# sourceMappingURL=ThreeDotsToggle.d.ts.map