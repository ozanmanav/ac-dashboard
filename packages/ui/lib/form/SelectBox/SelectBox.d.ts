import { ReactNodeArray, FunctionComponent } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type SelectBoxProps = FormElementType<string | string[] | number | undefined> & {
    children?: ReactNodeArray;
    visible?: boolean;
};
export declare const SelectBox: FunctionComponent<SelectBoxProps>;
//# sourceMappingURL=SelectBox.d.ts.map