import React, { useCallback } from "react";
import classNames from "classnames";
export var SelectBox = function (props) {
    var onChange = useCallback(function (e) {
        props.onChange &&
            props.onChange(e.target.options[e.target.selectedIndex].value);
    }, [props]);
    return (React.createElement("div", { className: "SelectBox SelectBox-visible--" + props.visible },
        React.createElement("select", { placeholder: props.placeHolder || "", disabled: props.isDisabled, value: props.value === null ? "" : props.value, onChange: onChange, className: classNames(props.className, "Form_field", {
                "SelectBox-disabled": props.isDisabled,
                "SelectBox-invalid": props.isValid === false
            }) }, props.children),
        React.createElement("div", { className: "SelectBoxTriangleIcon" })));
};
//# sourceMappingURL=SelectBox.js.map