import { PropsWithChildren } from "react";
export declare function FormBody(props: PropsWithChildren<{
    noScroll?: boolean;
}>): JSX.Element;
//# sourceMappingURL=FormBody.d.ts.map