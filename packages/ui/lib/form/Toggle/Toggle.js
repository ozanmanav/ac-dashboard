import React from "react";
import classNames from "classnames";
export function Toggle(props) {
    return (React.createElement("div", { className: "Toggle_header", onClick: props.onClick },
        React.createElement("div", { className: classNames("Toggle_header_title", {
                Toggle_selected: props.selected,
                "Toggle-disabled": props.disabled
            }) }, props.title),
        React.createElement("div", { className: "Toggle_element" }, props.children)));
}
//# sourceMappingURL=Toggle.js.map