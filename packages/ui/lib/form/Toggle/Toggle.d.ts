import { PropsWithChildren, ReactElement } from "react";
export declare type ToggleProps = PropsWithChildren<{
    title: string | ReactElement;
    selected: boolean;
    disabled?: boolean;
    onClick?: () => void;
}>;
export declare function Toggle(props: ToggleProps): JSX.Element;
//# sourceMappingURL=Toggle.d.ts.map