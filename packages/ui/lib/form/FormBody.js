import React from "react";
export function FormBody(props) {
    return (React.createElement("div", { className: "FormBody", "data-simplebar": props.noScroll ? '' : 'init' },
        React.createElement("div", null, props.children)));
}
//# sourceMappingURL=FormBody.js.map