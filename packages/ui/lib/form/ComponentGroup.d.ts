import { ListItemType } from "../ListItem";
export declare type ComponentGroupItemType = ListItemType;
export declare type ComponentGroupType = {
    /**
     * Value-Label Array
     */
    items: ComponentGroupItemType[];
};
//# sourceMappingURL=ComponentGroup.d.ts.map