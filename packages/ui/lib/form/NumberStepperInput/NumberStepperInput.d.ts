/// <reference types="react" />
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type NumberStepperInputProps = {
    min: number;
    max: number;
    step?: number;
} & FormElementType<string>;
export declare function NumberStepperInput(props: NumberStepperInputProps): JSX.Element;
//# sourceMappingURL=NumberStepperInput.d.ts.map