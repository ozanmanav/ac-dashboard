import React, { useRef, useCallback } from "react";
import classNames from "classnames";
export function NumberStepperInput(props) {
    var inputRef = useRef(null);
    var stepUp = useCallback(function () {
        inputRef.current && inputRef.current.stepUp();
        inputRef.current &&
            props.onChange &&
            props.onChange(inputRef.current.value);
    }, [props]);
    var stepDown = useCallback(function () {
        inputRef.current && inputRef.current.stepDown();
        inputRef.current &&
            props.onChange &&
            props.onChange(inputRef.current.value);
    }, [props]);
    var onChange = useCallback(function (e) {
        props.onChange && props.onChange(e.target.value);
    }, [props]);
    return (React.createElement("div", { className: classNames("Form_field NumberStepperInput", {
            "NumberStepperInput-disabled": props.isDisabled,
            "NumberStepperInput-invalid": props.isValid === false
        }, props.className) },
        React.createElement("input", { step: props.step, ref: inputRef, tabIndex: props.tabIndex, type: "number", className: "NumberStepperInput_input", min: props.min, max: props.max, value: String(props.value), onChange: onChange }),
        React.createElement("div", { className: "NumberStepperInput_stepperControls" },
            React.createElement(StepperControls, { orientation: "up", onClick: stepUp }),
            React.createElement(StepperControls, { orientation: "down", onClick: stepDown }))));
}
function StepperControls(props) {
    return (React.createElement("div", { onClick: props.onClick, className: classNames("StepperControls", {
            "StepperControls-up": props.orientation === "up",
            "StepperControls-down": props.orientation === "down"
        }) },
        React.createElement("div", { className: classNames("StepperControls_arrow", {
                "StepperControls_arrow-up": props.orientation === "up",
                "StepperControls_arrow-down": props.orientation === "down"
            }) })));
}
//# sourceMappingURL=NumberStepperInput.js.map