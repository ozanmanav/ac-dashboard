import { PropsWithChildren } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type RadioButtonType = PropsWithChildren<FormElementType<string | number | boolean> & {
    title?: string;
    value?: string | number | boolean;
    expression?: string;
    isSelected?: boolean;
}>;
export declare function RadioButton<TValue = any>(props: RadioButtonType): JSX.Element;
//# sourceMappingURL=RadioButton.d.ts.map