import React, { useCallback, useState, useEffect, useMemo } from "react";
import classNames from "classnames";
import { SvgRadioItem } from "@appcircle/assets/lib/RadioItem";
import { Toggle } from "../Toggle";
export function RadioButton(props) {
    var _a = useState(), defaultValue = _a[0], setDefaultValue = _a[1];
    useEffect(function () {
        setSelect(props.isSelected === undefined
            ? props.value === defaultValue
            : props.isSelected);
    }, [props.value, props.isSelected, defaultValue]);
    var _b = useState(false), select = _b[0], setSelect = _b[1];
    useEffect(function () {
        // setSelect(false);
        setDefaultValue(props.value);
        // eslint-disable-next-line
    }, []);
    var onClick = useCallback(function () {
        props.onChange && props.onChange(defaultValue);
        setSelect(true);
    }, [defaultValue, props]);
    var title = useMemo(function () { return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "RadioButton_title" }, props.title),
        props.expression && (React.createElement("div", { className: "RadioButton_expression" }, props.expression)))); }, [props.title, props.expression]);
    return (React.createElement("div", { className: classNames("RadioButton", {
            "RadioButton-selected": select,
            "RadioButton-disabled": props.isDisabled,
            "RadioButton-invalid": props.isValid === false
        }), onClick: onClick, tabIndex: props.tabIndex }, props.title ? (React.createElement(Toggle, { title: title, selected: select },
        React.createElement("div", { className: "RadioButton_circle" },
            React.createElement(SvgRadioItem, null)))) : (React.createElement("div", { className: "RadioButton_circle" },
        React.createElement(SvgRadioItem, null)))));
}
//# sourceMappingURL=RadioButton.js.map