import React, { useMemo } from "react";
import { RadioButton } from "../RadioButton";
import classNames from "classnames";
export function RadioGroup(props) {
    //TODO: Give classes to child on selected and unselected
    var RadioButtons = useMemo(function () {
        return props.items.map(function (child, index) { return (React.createElement("div", { key: index, onClick: function () {
                props.onChange && props.onChange(child.value);
            }, className: classNames("RadioGroup_item", {
                "RadioGroup_item-selected": props.value === child.value
            }) },
            React.createElement(RadioButton, { title: child.label, value: child.value, 
                // key={shortid.generate()}
                expression: child.expression, isSelected: props.value === child.value }))); });
    }, [props]);
    return (React.createElement("div", { className: classNames(props.className, "RadioGroup", {
            "RadioGroup-disabled": props.isDisabled,
            "RadioGroup-invalid": props.isValid === false
        }) }, RadioButtons));
}
//# sourceMappingURL=RadioGroup.js.map