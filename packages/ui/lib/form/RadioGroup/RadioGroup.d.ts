/// <reference types="react" />
import { FormElementType } from "@appcircle/form/lib/Form";
import { ComponentGroupType } from "../ComponentGroup";
export declare type RadioGroupProps = ComponentGroupType & FormElementType;
export declare function RadioGroup(props: RadioGroupProps): JSX.Element;
//# sourceMappingURL=RadioGroup.d.ts.map