var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React, { useState, useCallback, useMemo, useEffect, useRef } from "react";
import { EditableTextView } from "../../EditableTextView";
import { List } from "../../List";
var blurTimeout;
var MAX_DISPLAY_ITEM = 5;
var MAX_DISPLAY_HEIGHT = 225;
function isAtttheBottom(el, offset) {
    if (offset === void 0) { offset = 42; }
    var top = el.offsetTop;
    var scrollTop = 0;
    var scrollerHeight = 0;
    var parent = el;
    while (parent.offsetParent) {
        parent = parent.offsetParent;
        if (parent.scrollHeight >= parent.clientHeight &&
            parent.style.overflow.indexOf("hidden") > -1) {
            scrollerHeight = parent.clientHeight;
            scrollTop = parent.scrollTop;
            if (scrollerHeight + scrollTop - (top + offset) < 0)
                return scrollerHeight + scrollTop - (top + offset);
        }
        top += parent.offsetTop;
    }
    return false;
}
var LIST_ITEM_HEIGHT = 48;
var TEXTAREA_HEIGHT = 42;
export function ComboBox(props) {
    var styleType = props.style || "normal";
    var _a = useState(false), opened = _a[0], setOpened = _a[1];
    var _b = useState(function () { return []; }), textItems = _b[0], setTextItems = _b[1];
    var items = useMemo(function () { return __spreadArrays(props.items, textItems); }, [
        props.items,
        textItems
    ]);
    var selectedItem = useMemo(function () { return props.items.find(function (item) { return item.value === props.value; }); }, [props.items, props.value]);
    var selfRef = useRef(null);
    var addTextItem = useCallback(function (value) {
        !items.some(function (item) { return item.label === value; }) &&
            setTextItems(__spreadArrays(textItems, [{ value: value, label: value }]));
    }, [items, textItems]);
    var listRef = useRef(null);
    useEffect(function () {
        return function () {
            blurTimeout && clearTimeout(blurTimeout);
        };
    }, []);
    return (React.createElement("div", { ref: selfRef, className: "ComboBox ComboBox-" + styleType },
        React.createElement(EditableTextView, { placeHolder: props.placeHolder, editable: props.editable, className: "Form_field " + props.className, onFocus: useCallback(function () {
                setOpened(true);
                if (selfRef.current && listRef.current) {
                    var itemLen = Math.min(items.length, MAX_DISPLAY_ITEM);
                    var h = itemLen * LIST_ITEM_HEIGHT;
                    var maxh = Math.min(h, MAX_DISPLAY_HEIGHT);
                    var listHeight = maxh + TEXTAREA_HEIGHT;
                    var listOffset = Math.max(maxh - LIST_ITEM_HEIGHT, 0);
                    var diff = isAtttheBottom(selfRef.current, listHeight);
                    if (diff < 0) {
                        listRef.current.style.top = "-" + listOffset + "px";
                    }
                    else {
                        listRef.current.style.top = "auto";
                    }
                }
            }, [items]), onBlur: useCallback(function (text) {
                blurTimeout = setTimeout(function () { return setOpened(false); }, 50);
                // text && addTextItem(text);
            }, []), onChange: useCallback(function (val) {
                props.onChange && props.onChange(val);
            }, [props]), activated: opened, value: selectedItem ? selectedItem.label : props.value || "" }),
        React.createElement(List, { ref: listRef, onBlur: function () {
                blurTimeout = setTimeout(function () { return setOpened(false); }, 50);
            }, isOpened: opened, data: items, onItemSelect: useCallback(function (data) {
                props.onChange && props.onChange(data.value);
            }, [props.onChange]) }),
        items && items.length > 0 ? (React.createElement("div", { className: "ComboBoxTriangleIcon" })) : null));
}
//# sourceMappingURL=ComboBox.js.map