/// <reference types="react" />
import { ListItemsType } from "../../ListItem";
import { FormElementType } from "@appcircle/form/lib/Form";
declare type ComboBoxProps = FormElementType<string> & {
    editable?: boolean;
    items: ListItemsType;
    placeHolder?: string;
    style?: "danger" | "normal";
};
export declare function ComboBox(props: ComboBoxProps): JSX.Element;
export {};
//# sourceMappingURL=ComboBox.d.ts.map