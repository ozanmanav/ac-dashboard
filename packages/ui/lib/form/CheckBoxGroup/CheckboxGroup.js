var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React, { useMemo } from "react";
import classNames from "classnames";
import { CheckBox } from "../CheckBox";
export function CheckboxGroup(props) {
    var value = useMemo(function () { return props.value || []; }, [props.value]);
    var Checkboxes = useMemo(function () {
        return props.items.map(function (child, index) { return (React.createElement("div", { key: index, className: classNames("ComponentGroup_item", {
                ".ComponentGroup_item-selected": props.value === child.value
            }) },
            React.createElement(CheckBox, { onChange: function (val) {
                    // props.onChange && props.onChange(child.value);
                    var values = val !== undefined
                        ? __spreadArrays(value, [child.value]) : value.filter(function (v) { return child.value !== v; });
                    props.onChange && props.onChange(values);
                }, title: child.label, value: child.value, 
                // key={shortid.generate()}
                expression: child.expression, isSelected: value.some(function (v) { return v === child.value; }) }))); });
    }, [value, props.items, props.onChange]);
    return (React.createElement("div", { className: classNames(props.className, "RadioGroup", {
            "RadioGroup-disabled": props.isDisabled,
            "RadioGroup-invalid": props.isValid === false
        }) }, Checkboxes));
}
//# sourceMappingURL=CheckboxGroup.js.map