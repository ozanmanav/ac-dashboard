/// <reference types="react" />
import { FormElementType } from "@appcircle/form/lib/Form";
import { ComponentGroupType } from "../ComponentGroup";
export declare type CheckboxGroupProps = ComponentGroupType & FormElementType<(string | number | null)[]>;
export declare function CheckboxGroup(props: CheckboxGroupProps): JSX.Element;
//# sourceMappingURL=CheckboxGroup.d.ts.map