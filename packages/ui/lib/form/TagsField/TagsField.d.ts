/// <reference types="react" />
import { TaggableTextFieldProps } from "../../TaggableTextField/TaggableTextField";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type TagsFieldProps = Omit<TaggableTextFieldProps, "onError" | "addTag" | "deleteTag" | "value" | "onChange"> & FormElementType<string[] | undefined> & {
    addTag?: TaggableTextFieldProps["onChange"];
    deleteTag?: TaggableTextFieldProps["deleteTag"];
};
export declare function TagsField(props: TagsFieldProps): JSX.Element;
//# sourceMappingURL=TagsField.d.ts.map