import React, { useState, useEffect, useCallback } from "react";
import { TaggableTextField } from "../../TaggableTextField/TaggableTextField";
import { createFakeID } from "@appcircle/core/lib/utils/fakeID";
function createTags(value) {
    return (value || []).map(function (text) { return ({
        id: createFakeID(),
        text: text
    }); });
}
function createValue(value) {
    return (value || []).map(function (tag) { return tag.text; });
}
export function TagsField(props) {
    var _a = useState(function () {
        return createTags(props.value);
    }), tags = _a[0], setTags = _a[1];
    useEffect(function () {
        props.value && setTags(createTags(props.value));
    }, [props.value]);
    return (React.createElement("div", { className: "TagsField " + (props.className || "") },
        React.createElement(TaggableTextField, { allowDuplicates: props.allowDuplicates, matchRule: props.matchRule, errors: props.errors, placeHolder: props.placeHolder, separator: props.separator, suggestions: props.suggestions, onTextChange: props.onTextChange, autofocus: props.autofocus, text: props.text, className: "Form_field ", value: tags || [], onChange: useCallback(function (newTag) {
                props.onChange
                    ? props.onChange(createValue(tags ? tags.concat(newTag) : newTag))
                    : setTags(tags ? tags.concat(newTag) : newTag);
            }, [props.onChange, tags]), deleteTag: useCallback(function (id) {
                // const res = ;
                var _tags = (tags || []).filter(function (t) { return t.id !== id; });
                props.onChange
                    ? props.onChange(createValue(_tags))
                    : setTags(_tags);
            }, [props.onChange, tags]) })));
}
//# sourceMappingURL=TagsField.js.map