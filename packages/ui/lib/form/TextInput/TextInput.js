import React, { useState, useEffect, useCallback } from "react";
import { useDebouncedCall } from "@appcircle/core/lib/hooks/useDebouncedCall";
export function TextInput(props) {
    var _a = useState(""), val = _a[0], setVal = _a[1];
    var onChange = useDebouncedCall(function () {
        if (props.onChange)
            props.onChange && props.onChange(val);
    }, 200, [val]);
    var onBlur = props.onBlur;
    useEffect(function () {
        onChange(!val ? props.value : val);
    }, [val, onChange]);
    useEffect(function () {
        props.value !== undefined && val !== props.value && setVal(props.value);
    }, [props.value]);
    return (React.createElement("input", { ref: function (ref) { return ref && props.autoFocus && ref.focus(); }, placeholder: props.placeholder, type: props.type, className: "Form_field " + props.className, value: val, onChange: useCallback(function (e) { return setVal(e.target.value); }, []), onBlur: useCallback(function () { return onBlur && onBlur(val); }, [onBlur, val]) }));
}
//# sourceMappingURL=TextInput.js.map