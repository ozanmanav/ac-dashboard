/// <reference types="react" />
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type TextInputProps = {
    type?: "text" | "password";
    placeholder?: string;
    autoFocus?: boolean;
} & FormElementType<string | undefined>;
export declare function TextInput(props: TextInputProps): JSX.Element;
//# sourceMappingURL=TextInput.d.ts.map