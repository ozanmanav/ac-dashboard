import { PropsWithChildren } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare enum SwitchType {
    DANGER = "danger",
    PRIMARY = "primary",
    SUCCESS = "success",
    WARNING = "warning"
}
export declare type SwitchProps = PropsWithChildren<{
    type?: SwitchType;
    className?: string;
}> & FormElementType<boolean>;
export declare function Switch(props: SwitchProps): JSX.Element;
//# sourceMappingURL=Switch.d.ts.map