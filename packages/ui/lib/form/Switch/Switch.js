import React, { useCallback } from "react";
import classNames from "classnames";
export var SwitchType;
(function (SwitchType) {
    SwitchType["DANGER"] = "danger";
    SwitchType["PRIMARY"] = "primary";
    SwitchType["SUCCESS"] = "success";
    SwitchType["WARNING"] = "warning";
})(SwitchType || (SwitchType = {}));
export function Switch(props) {
    var className = classNames("Switch", "Switch-" + (props.type || "primary"), {
        "Switch-disabled": props.isDisabled,
        Switch_on: props.isSelected,
        "Switch-invalid": props.isValid === false
    });
    var onClick = useCallback(function () {
        props.onChange && props.onChange(!props.isSelected);
    }, [props]);
    return (React.createElement("div", { tabIndex: props.tabIndex, className: className, onClick: onClick },
        React.createElement("div", { className: "Switch_line" }),
        React.createElement("div", { className: "Switch_thumb" })));
}
//# sourceMappingURL=Switch.js.map