import React, { useCallback, useMemo, useState, useEffect } from "react";
import classNames from "classnames";
import { Toggle } from "../Toggle";
import { SvgCheck } from "@appcircle/assets/lib/Check";
export function CheckBox(props) {
    var _a = useState(), defaultValue = _a[0], setDefaultValue = _a[1];
    useEffect(function () {
        setSelect(props.isSelected === undefined
            ? props.value === defaultValue
            : props.isSelected);
    }, [props.value, props.isSelected, defaultValue]);
    var _b = useState(false), select = _b[0], setSelect = _b[1];
    useEffect(function () {
        // setSelect(false);
        setDefaultValue(props.value);
        // eslint-disable-next-line
    }, []);
    var onClick = useCallback(function () {
        props.onChange &&
            props.onChange(!select ? defaultValue : undefined, props.name);
        setSelect(!select);
    }, [defaultValue, props, select]);
    var title = useMemo(function () { return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "CheckBox_title" }, props.title),
        props.expression && (React.createElement("div", { className: "CheckBox_expression" }, props.expression)))); }, [props.title, props.expression]);
    return (React.createElement("div", { tabIndex: props.tabIndex, className: classNames("CheckBox", {
            "CheckBox-selected": select,
            "CheckBox-disabled": props.isDisabled,
            "CheckBox-invalid": props.isValid === false
        }), onClick: onClick }, props.title ? (React.createElement(Toggle, { title: title, selected: select },
        React.createElement("div", { className: "CheckBox_rect" },
            React.createElement(SvgCheck, null)))) : (React.createElement("div", { className: "CheckBox_rect" },
        React.createElement(SvgCheck, null)))));
}
//# sourceMappingURL=CheckBox.js.map