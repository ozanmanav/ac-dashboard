import { PropsWithChildren } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type CheckBoxProps = PropsWithChildren<{}> & Omit<FormElementType<string | number | boolean | undefined>, "onChange"> & {
    title?: string;
    value?: string | number | boolean;
    expression?: string;
    isSelected?: boolean;
    onChange: (value: boolean | number | string | null | undefined, name?: string) => void;
    name?: string;
};
export declare function CheckBox(props: CheckBoxProps): JSX.Element;
//# sourceMappingURL=CheckBox.d.ts.map