import React, { PropsWithChildren } from "react";
export declare type ModalWindowType<T> = T & {
    header: string | JSX.Element;
    className?: string;
    onModalClose: () => void;
    onBeforeModalClose?: () => boolean;
    showBackButton?: boolean;
    onBackButton?: () => void;
    id?: string;
    spinner?: boolean;
};
export declare type ModalWindowProps = ModalWindowType<PropsWithChildren<{}>>;
export declare function useModalAnim(id: string, onModalClose: () => void): {
    status: "opened" | "willOpen" | "willClose" | "closing" | "opening" | "closed";
    setStatus: React.Dispatch<React.SetStateAction<"opened" | "willOpen" | "willClose" | "closing" | "opening" | "closed">>;
    animProps: import("react-spring").AnimatedValue<Pick<{
        config: {
            mass: number;
            tension: number;
            friction: number;
        };
        transform: string | undefined;
        opacity: string | number | undefined;
        from: {
            opacity: number;
            transform: string;
        };
    }, "opacity" | "transform">>;
};
export declare function ModalWindow(props: ModalWindowProps): JSX.Element;
export declare function ModalContent(props: PropsWithChildren<{}>): JSX.Element;
//# sourceMappingURL=ModalWindow.d.ts.map