import React, { useRef, useCallback, useEffect, useState } from "react";
import classNames from "classnames";
import { useSpring, animated } from "react-spring";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { ScrollView } from "./ScrollView";
import { Spinner } from "./Spinner";
import { SvgModalArrow } from "@appcircle/assets/lib/ModalArrow";
import { SvgCloseSmall } from "@appcircle/assets/lib/CloseSmall";
// const CloseIcon = icons.closeSmall;
export function useModalAnim(id, onModalClose) {
    var _a = useSpring(function () { return ({
        config: { mass: 2, tension: 1000, friction: 70 },
        transform: "translateX(0)",
        opacity: 1,
        from: { opacity: 0, transform: "translateX(20%)" }
    }); }), animProps = _a[0], setAnim = _a[1];
    // TODO: Move modal's status to useModalStatus
    var _b = useState("closed"), status = _b[0], setStatus = _b[1];
    // const [setModalStatus, modalStatus] = useModalStatus(id);
    useEffect(function () {
        switch (status) {
            case "willClose":
                setAnim({
                    opacity: 0,
                    transform: "translateX(20%)",
                    config: { mass: 8, tension: 1000, friction: 40 }
                });
                setStatus("closing");
                setTimeout(function () { return onModalClose(); }, 200);
                break;
        }
    }, [onModalClose, setAnim, status]);
    return { status: status, setStatus: setStatus, animProps: animProps };
}
export function ModalWindow(props) {
    var rootRef = useRef(null);
    var _a = useModalAnim(props.id || "", props.onModalClose), status = _a.status, setStatus = _a.setStatus, animProps = _a.animProps;
    var onBeforeModalClose = useRef(props.onBeforeModalClose);
    onBeforeModalClose.current = props.onBeforeModalClose;
    var closeModal = useCallback(function () {
        return (!onBeforeModalClose.current || onBeforeModalClose.current()) &&
            status !== "closing" &&
            setStatus("willClose");
    }, [setStatus, status]);
    var outsideClickHandler = useCallback(closeModal, [props]);
    useGlobalClickService([], rootRef, outsideClickHandler, "mousedown");
    return (React.createElement(animated.div, { ref: rootRef, style: animProps, className: classNames("ModalWindow", props.className) },
        React.createElement("div", { className: "ModalWindow_header" },
            props.showBackButton ? (React.createElement("div", { className: "ModalWindow_header_backButton", onClick: props.onBackButton },
                React.createElement(SvgModalArrow, null))) : null,
            React.createElement("div", { className: "ModalWindow_header_label" },
                props.spinner === true && React.createElement(Spinner, null),
                " ",
                props.header),
            React.createElement("div", { className: "ModalWindow_header_close", onClick: closeModal },
                React.createElement(SvgCloseSmall, null))),
        React.createElement("div", { className: "ModalWindow_body" }, props.children)));
}
export function ModalContent(props) {
    return React.createElement(ScrollView, null, props.children);
}
//# sourceMappingURL=ModalWindow.js.map