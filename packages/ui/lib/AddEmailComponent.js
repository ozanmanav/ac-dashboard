import React, { useState, useEffect, useCallback, useMemo } from "react";
import { ConditionalTextField } from "./ConditionalTextField";
import { AddButton } from "./AddButton";
import { IconButtonState } from "./IconButton";
import { useIntl } from "react-intl";
import { CommonMessages } from "@appcircle/shared/lib/messages/common";
import { SvgEmailIcon } from "@appcircle/assets/lib/EmailIcon";
var emailValidationRegexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export function AddEmailComponent(props) {
    var _a = useState(false), toggleResetState = _a[0], setToggleResetState = _a[1];
    useEffect(function () {
        toggleResetState && setToggleResetState(!toggleResetState);
        setEmails([]);
    }, [toggleResetState]);
    var _b = useState([]), emails = _b[0], setEmails = _b[1];
    var _c = useState(""), text = _c[0], setText = _c[1];
    var onAddEmailEvent = useCallback(function () {
        if (emails.length) {
            props.onAddEmail(emails);
            setToggleResetState(!toggleResetState);
            setEmails([]);
            setText("");
        }
    }, [emails, props, toggleResetState]);
    var onSuccessInput = useCallback(function (newEmails) {
        setEmails(newEmails);
    }, []);
    var onWrongInput = useCallback(function (wrongEmail) {
        emails.length && setEmails([]);
    }, [emails.length]);
    var rules = useMemo(function () { return [
        function (email) { return emailValidationRegexp.test(email); },
        function (email) { return !props.emails.some(function (item) { return item === email; }); }
    ]; }, [props.emails]);
    var onChange = useCallback(function (text) {
        setText(text);
    }, []);
    var intl = useIntl();
    return (React.createElement("div", { className: "AddEmailComponent" },
        React.createElement("form", { onSubmit: function (e) {
                e.preventDefault();
            } },
            React.createElement(ConditionalTextField, { text: text, tabIndex: props.tabIndex, resetState: toggleResetState, placeHolder: intl.formatMessage(CommonMessages.appCommonEmail), onSuccessInput: onSuccessInput, onWrongInput: onWrongInput, onEnterKey: onAddEmailEvent, onChange: onChange, rules: rules, icon: SvgEmailIcon }),
            React.createElement(AddButton, { tabIndex: props.tabIndex, text: intl.formatMessage(CommonMessages.appCommonAdd), state: emails.length ? IconButtonState.ENABLED : IconButtonState.DISABLED, onClick: onAddEmailEvent }))));
}
//# sourceMappingURL=AddEmailComponent.js.map