import React, { useState, useEffect } from "react";
import classNames from "classnames";
import { SvgToggleClose } from "@appcircle/assets/lib/ToggleClose";
// const ToggleClose = icons.toggleClose;
export function TogglePanel(props) {
    var _a = useState(props.isSelected || false), isSelected = _a[0], setIsSelected = _a[1];
    useEffect(function () {
        props.onChange && props.onChange(isSelected);
        // eslint-disable-next-line
    }, [isSelected]);
    return (React.createElement("div", { className: classNames("Toggle", props.className, {
            "Toggle-disabled": props.isDisabled
        }) },
        React.createElement("div", { className: "Toggle_header", onClick: function () {
                setIsSelected(!isSelected);
            } },
            React.createElement("div", { className: "Toggle_header_title" }, props.title),
            React.createElement("div", { className: classNames("Toggle_header_closeIcon", {
                    "Toggle_header_closeIcon-up": !isSelected,
                    "Toggle_header_closeicon-down": isSelected
                }) },
                React.createElement(SvgToggleClose, null))),
        isSelected ? props.children : null));
}
//# sourceMappingURL=TogglePanel.js.map