var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useState, useEffect } from "react";
import classNames from "classnames";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
export function TextField(props) {
    var _a = useState(""), text = _a[0], setText = _a[1];
    var className = classNames("TextField", props.className, {
        "TextField-readonly": !props.editMode
    });
    useEffect(function () {
        //update text when props.text change.
        setText(props.text);
    }, [props.text]);
    var inputProps = {
        className: className,
        autoFocus: true,
        spellCheck: false,
        value: text || "",
        placeholder: props.placeHolder,
        disabled: props.disabled,
        onChange: function (e) {
            var val = props.onValidate
                ? props.onValidate(text, e.target.value)
                : e.target.value;
            setText(val);
            props.onChange && val !== text && props.onChange(val);
        },
        onBlur: props.onBlur ||
            (function () {
                setText(props.text);
                props.onChangeEnd && props.onChangeEnd(props.text, true);
                props.onBlurOrEscape && props.onBlurOrEscape();
            }),
        onKeyDown: function (e) {
            e.stopPropagation();
            if (e.keyCode === KeyCode.ESC_KEY) {
                setText(props.text);
                props.onChangeEnd && props.onChangeEnd(props.text, true);
                props.onBlurOrEscape && props.onBlurOrEscape();
            }
            else if (e.keyCode === KeyCode.ENTER_KEY) {
                props.onChangeEnd && props.onChangeEnd(text);
                props.onEnter && props.onEnter(text);
            }
        }
    };
    return props.editMode ? (React.createElement(React.Fragment, null, props.multiLineMode ? (React.createElement("textarea", __assign({}, inputProps, { style: { resize: "none" } }))) : (React.createElement("input", __assign({}, inputProps, { style: { resize: "none" } }))))) : (React.createElement("span", { className: className }, text));
}
//# sourceMappingURL=TextField.js.map