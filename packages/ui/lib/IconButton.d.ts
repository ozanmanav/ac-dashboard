import React, { PropsWithChildren } from "react";
import { ButtonProps } from "./Button";
export declare enum IconButtonState {
    DISABLED = 0,
    ENABLED = 1
}
export declare type IconButtonProps = PropsWithChildren<{
    icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
    onClick?: () => void;
    size?: ButtonProps["size"];
    className?: string;
    state?: IconButtonState;
    tabIndex?: number;
}>;
export declare function IconButton(props: IconButtonProps): JSX.Element;
//# sourceMappingURL=IconButton.d.ts.map