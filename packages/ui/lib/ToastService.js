import React, { useMemo } from "react";
import { MAX_MESSAGES } from "@appcircle/shared/lib/notification/messageReducer";
import { useNotificationsModel } from "@appcircle/shared/lib/notification/useNotificationsModel";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { ToastComponent } from "./ToastComponent";
export var DEFAULT_MESSAGE_LIFE = 2000;
export var DEFAULT_TOAST_HEIGHT = 30;
export var TOAST_VERTICAL_GAP = 13;
export var ToastService = function (props) {
    var removeMessage = useNotificationsService().removeMessage;
    var getMessages = useNotificationsModel().getMessages;
    var messages = useMemo(function () { return getMessages(); }, [getMessages]);
    // TODO: Cache messages
    return (React.createElement(React.Fragment, null, messages.slice(0, MAX_MESSAGES).map(function (message, index) { return (React.createElement(ToastComponent, { key: message.id, message: message, index: index, onRemove: removeMessage })); })));
};
//# sourceMappingURL=ToastService.js.map