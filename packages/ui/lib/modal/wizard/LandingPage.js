import React from "react";
import { animated } from "react-spring";
import { FormBody } from "../../form/FormBody";
import { SvgArrow } from "@appcircle/assets/lib/Arrow";
export function LandingPage(props) {
    return (React.createElement(FormBody, null,
        React.createElement(animated.div, { key: 1, style: props.animationStyle, className: "landingPage LandingPage" },
            React.createElement("div", { className: "LandingPage_tip" }, props.desc || ""),
            React.createElement("div", { className: "LandingPage_title" }, props.children
                ? props.children
                : props.pages.map(function (page, index) {
                    return (React.createElement("div", { key: index, className: "LandingPage_pageButton", onClick: function () {
                            props.onPageChange(index);
                        } },
                        page.header,
                        React.createElement("div", { className: "LandingPage_arrow" },
                            React.createElement(SvgArrow, null))));
                })))));
}
//# sourceMappingURL=LandingPage.js.map