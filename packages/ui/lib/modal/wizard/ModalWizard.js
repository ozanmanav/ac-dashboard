var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React, { useState, useCallback, useMemo } from "react";
import { ModalWindow } from "../../ModalWindow";
import { LandingPage } from "./LandingPage";
export var ModalWizard = function (props) {
    var landingPage = useMemo(function () {
        var LandingPageComp = (props.landingPage && props.landingPage.element) || LandingPage;
        return {
            name: "LANDING_PAGE",
            header: props.header,
            component: (React.createElement(LandingPageComp, { onPageChange: function (index) { return setCurrentPageIndex(index + 1); }, pages: props.subPages }))
        };
    }, [props.header, props.landingPage, props.subPages]);
    var pages = useMemo(function () { return __spreadArrays([landingPage], props.subPages); }, [
        landingPage,
        props.subPages
    ]);
    var _a = useState(0), currentPageIndex = _a[0], setCurrentPageIndex = _a[1];
    var onBackButton = useCallback(function () { return setCurrentPageIndex(0); }, []);
    return (React.createElement(ModalWindow, { showBackButton: pages[currentPageIndex].name !== landingPage.name, onBackButton: onBackButton, header: pages[currentPageIndex].header, onModalClose: props.onModalClose }, pages[currentPageIndex].component));
};
//# sourceMappingURL=ModalWizard.js.map