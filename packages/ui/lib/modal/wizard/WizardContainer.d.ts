import React, { ReactElement, ComponentType } from "react";
import { LandingPageProps } from "./LandingPage";
export declare type WizardPageType = {
    name: string;
    header: string;
    component: ReactElement;
};
export declare type LandingDetailLayoutProps = {
    landingPage?: {
        header: string;
        element?: ComponentType<LandingPageProps>;
    };
    header?: string;
    subPages: WizardPageType[];
    onPageChange?: (page: WizardPageType, index: number) => void;
    selectedPageIndex?: number;
};
export declare const LandingDetailLayout: (props: LandingDetailLayoutProps) => React.ReactElement<any, string | ((props: any) => React.ReactElement<any, string | any | (new (props: any) => React.Component<any, any, any>)> | null) | (new (props: any) => React.Component<any, any, any>)>;
//# sourceMappingURL=WizardContainer.d.ts.map