import { ElementType, ReactElement } from "react";
import { ModalWindowType } from "../../ModalWindow";
import { LandingPageProps } from "./LandingPage";
export declare type WizardPageType = {
    name: string;
    header: string;
    component: ReactElement;
};
declare type ModalProps = Omit<ModalWindowType<any>, "showBackButton" | "onBackButton" | "header" | "className" | "children">;
export declare type ModalWizardProps = {
    landingPage?: {
        header: string;
        element?: ElementType<LandingPageProps>;
    };
    subPages: WizardPageType[];
    header: string;
} & ModalProps;
export declare const ModalWizard: (props: ModalWizardProps) => JSX.Element;
export {};
//# sourceMappingURL=ModalWizard.d.ts.map