import { PropsWithChildren } from "react";
import { WizardPageType } from "./ModalWizard";
export declare type LandingPageProps = {
    onPageChange: (index: number) => void;
    pages: WizardPageType[];
    animationStyle?: {};
    desc?: string;
    children?: PropsWithChildren<{}>["children"];
};
export declare function LandingPage(props: LandingPageProps): JSX.Element;
//# sourceMappingURL=LandingPage.d.ts.map