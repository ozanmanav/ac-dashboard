import React from "react";
import { ScrollView } from "../ScrollView";
export function ModalBody(props) {
    return (React.createElement(ScrollView, { breakPointSelector: props.scrollBreakpointSelector, showNavBar: props.showScrollbarNav, className: "ModalBody", autoScrollToBottom: props.autoScrollToBottom }, props.children));
}
//# sourceMappingURL=ModalBody.js.map