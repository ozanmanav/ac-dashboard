import { PropsWithChildren } from "react";
export declare function ModalBody(props: PropsWithChildren<{
    autoScrollToBottom?: boolean;
    showScrollbarNav?: boolean;
    scrollBreakpointSelector?: string;
}>): JSX.Element;
//# sourceMappingURL=ModalBody.d.ts.map