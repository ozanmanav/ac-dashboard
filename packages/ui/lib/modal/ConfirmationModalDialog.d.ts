import React from "react";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
declare type DeleteModalDialogProps = ModalComponentPropsType<{}> & {
    title: string;
    okButton: React.ReactElement;
    style?: Styles;
};
declare type Styles = "warning" | "info" | "danger";
export declare function ConfirmationModalDialog(props: DeleteModalDialogProps): JSX.Element;
export {};
//# sourceMappingURL=ConfirmationModalDialog.d.ts.map