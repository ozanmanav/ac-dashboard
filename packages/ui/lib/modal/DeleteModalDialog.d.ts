/// <reference types="react" />
import { FormProps } from "@appcircle/form/lib/Form";
import { TaskPropsType } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ModalComponentPropsType } from "@appcircle/shared/lib/Module";
export declare type DeleteModalDialogProps = {
    ignoreTaskId?: boolean;
    errorMessage: string;
    successMessage: string;
    onSuccess: TaskPropsType["onSuccess"];
    title: string;
    endpoint: string;
    iconURL?: string;
    name: string;
    placeHolder: string;
    header?: string;
    entityID?: string;
    onBeforeSubmmit?: FormProps["onBeforeSubmit"];
    skipNameValidation?: boolean;
};
export declare function DeleteModalDialog(props: ModalComponentPropsType<DeleteModalDialogProps>): JSX.Element;
//# sourceMappingURL=DeleteModalDialog.d.ts.map