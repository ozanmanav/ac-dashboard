import React from "react";
import { ModalDialog, ModalDialogHeader } from "../ModalDialog";
import { SvgWarning } from "@appcircle/assets/lib/Warning";
export function ConfirmationModalDialog(props) {
    return (React.createElement(ModalDialog, { header: React.createElement(ModalDialogHeader, { align: "center" },
            React.createElement(SvgWarning, null)), className: "ConfirmationModalDialog", onModalClose: props.onModalClose },
        React.createElement("div", { className: "ConfirmationModalDialog_body" },
            React.createElement("div", { className: "ConfirmationModalDialog_title" }, props.title),
            React.createElement("footer", { className: "" }, props.okButton))));
}
//# sourceMappingURL=ConfirmationModalDialog.js.map