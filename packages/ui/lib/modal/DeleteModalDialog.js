import React, { useMemo } from "react";
import { Form } from "@appcircle/form/lib/Form";
import { FormBody } from "../form/FormBody";
import { ModalFooter } from "./ModalFooter";
import { FormComponent } from "@appcircle/form/lib/FormComponent";
import { TextInput } from "../form/TextInput";
import { ButtonType } from "../Button";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { ModalDialog } from "../ModalDialog";
import { WarningIcon } from "@appcircle/assets/lib/WarningIcon";
export function DeleteModalDialog(props) {
    var intl = useIntl();
    var taskProps = useMemo(function () { return ({
        entityID: props.entityID,
        errorMessage: function () { return props.errorMessage; },
        successMessage: function () { return props.successMessage; },
        onSuccess: props.onSuccess,
        ignoreTaskId: props.ignoreTaskId
    }); }, [props.errorMessage, props.entityID, props.onSuccess, props.successMessage]);
    return (React.createElement(ModalDialog, { header: props.header || "", className: "DeleteModalView", onModalClose: props.onModalClose },
        React.createElement("div", { className: "DeleteModalView_icons" },
            React.createElement(WarningIcon, null)),
        React.createElement("div", { className: "DeleteModalView_content" },
            React.createElement("span", null, props.title),
            React.createElement(Form, { formID: "deleteEntityModal", method: "DELETE", targetPath: props.endpoint, taskProps: taskProps, onBeforeSubmit: props.onBeforeSubmmit, status: props.skipNameValidation === true
                    ? {
                        isDirty: true
                    }
                    : undefined },
                React.createElement(FormBody, { noScroll: true },
                    React.createElement("div", { className: "DeleteModalView_appInfo" },
                        !!props.iconURL && React.createElement("img", { src: props.iconURL }),
                        React.createElement("div", null, props.name)),
                    React.createElement(FormComponent, { formID: "deleteEntityModal", placeholder: props.placeHolder, autoFocus: true, formProps: {
                            rule: {
                                confirmValue: props.name,
                                type: "confirm-value"
                            },
                            value: !!props.skipNameValidation
                                ? !!props.name
                                    ? props.name
                                    : undefined
                                : undefined,
                            name: "name",
                            required: true
                        }, element: !!!props.skipNameValidation ? TextInput : null })),
                React.createElement(ModalFooter, { text: intl.formatMessage(messages.appCommonDelete), buttonStyle: ButtonType.DANGER, inProgressText: intl.formatMessage(messages.appCommonDeleting), formID: "deleteEntityModal" })))));
}
//# sourceMappingURL=DeleteModalDialog.js.map