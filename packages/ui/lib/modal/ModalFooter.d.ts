import React from "react";
import { ButtonType } from "../Button";
import { SubmitButtonState } from "../SubmitButton";
export declare type ModalFooterPropsType = {
    state?: SubmitButtonState;
    onSave?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    formID?: string;
    text?: string;
    buttonStyle?: ButtonType;
    inProgressText?: string;
};
export declare function ModalFooter(props: ModalFooterPropsType): JSX.Element;
//# sourceMappingURL=ModalFooter.d.ts.map