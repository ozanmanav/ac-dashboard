import React from "react";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { SubmitButton } from "../SubmitButton";
export function ModalFooter(props) {
    var intl = useIntl();
    return (React.createElement("footer", null,
        React.createElement(SubmitButton, { type: props.buttonStyle, className: "Button_primaryButton ", formID: props.formID, state: props.state, onClick: props.onSave, inProgressText: props.inProgressText || intl.formatMessage(messages.appCommonSaving) }, props.text || intl.formatMessage(messages.appCommonSave))));
}
//# sourceMappingURL=ModalFooter.js.map