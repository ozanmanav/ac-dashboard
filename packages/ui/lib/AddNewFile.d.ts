import React, { PropsWithChildren } from "react";
export declare enum AddNewFileState {
    IDLE = "IDLE",
    UPLOADING = "UPLOADING",
    ERROR = "ERROR"
}
export declare type AddNewFileProps = PropsWithChildren<{
    types: string[];
    text: string;
    onFileUpload: (file: File) => void;
    icons: React.FunctionComponent<React.SVGProps<SVGSVGElement>>[];
    onStateChange?: (state: AddNewFileStatus) => void;
    autoUpload?: boolean;
    status?: AddNewFileStatus;
    disabled?: boolean;
    progress?: number;
    entityID?: string;
}>;
export declare type AddNewFileStatus = "idle" | "file-selected" | "uploading" | "error" | "invalid" | "dragging" | "processing";
export declare function AddNewFile(props: AddNewFileProps): JSX.Element;
//# sourceMappingURL=AddNewFile.d.ts.map