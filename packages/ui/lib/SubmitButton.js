import React from "react";
import { Spinner } from "./Spinner";
import classNames from "classnames";
import { Button, ButtonType } from "./Button";
import { useFormContext } from "@appcircle/form/lib/useFormContext";
import { FormStatus } from "@appcircle/form/lib/Form";
import { isEmptyValue } from "@appcircle/core/lib/utils/isEmptyValue";
export var SubmitButtonState;
(function (SubmitButtonState) {
    SubmitButtonState[SubmitButtonState["DISABLED"] = 0] = "DISABLED";
    SubmitButtonState[SubmitButtonState["ENABLED"] = 1] = "ENABLED";
    SubmitButtonState[SubmitButtonState["INPROGRESS"] = 2] = "INPROGRESS";
})(SubmitButtonState || (SubmitButtonState = {}));
export function SubmitButton(props) {
    var formState = useFormContext(props.formID || "")[0];
    // Check if props.state has value if so use it and ignore form status
    var isinProgress = !isEmptyValue(props.state)
        ? props.state === SubmitButtonState.INPROGRESS
        : formState && formState.status.submission === FormStatus.SENDING;
    // Check if props.state has value if so use it and ignore form status
    var isDisabled = isinProgress ||
        (!isEmptyValue(props.state)
            ? props.state === SubmitButtonState.DISABLED ||
                (!!formState && (formState.errors || []).length > 0)
            : formState
                ? formState.status.isDirty === false ||
                    formState.status.fieldsStatus !== FormStatus.READY
                : true);
    var className = classNames("SubmitButton", props.className, {
        "SubmitButton-disabled": isDisabled,
        // (formState && formState.status.fieldsStatus !== FormStatus.READY),
        "SubmitButton-inprogress": isinProgress
    });
    return (React.createElement(Button, { tabIndex: props.tabIndex, ariaLabel: props.ariaLabel, type: ButtonType.WARNING, onClick: props.onClick, className: className, htmlType: "submit", showFocus: props.showFocus || true },
        isinProgress && React.createElement(Spinner, null),
        React.createElement("span", null, isinProgress ? props.inProgressText : props.children)));
}
//# sourceMappingURL=SubmitButton.js.map