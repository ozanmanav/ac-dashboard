import React, { useMemo } from "react";
import { OS } from "@appcircle/core/lib/enums/OS";
import classNames from "classnames";
import { SvgApple } from "@appcircle/assets/lib/Apple";
import { SvgAndroid } from "@appcircle/assets/lib/Android";
export function OSIcon(props) {
    var className = classNames("OSIcon", props.className, props.display && "OSIcon-" + props.display, "OSIcon-" + (props.style || "dark"));
    var Icon = useMemo(function () {
        return props.os === OS.ios ? (React.createElement(SvgApple, { className: className, viewBox: "0 0 14 16" })) : (React.createElement(SvgAndroid, { className: className, viewBox: "0 0 17 19" }));
    }, [props.os, className]);
    return Icon;
}
//# sourceMappingURL=OSIcon.js.map