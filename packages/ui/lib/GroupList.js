import React, { useMemo } from "react";
import classNames from "classnames";
import { TextField } from "./TextField";
import { TaskProgressView } from "./TaskProgressView";
import { ScrollView } from "./ScrollView";
import { isValidName } from "@appcircle/core/lib/utils/isValidName";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import messages from "@appcircle/shared/lib/messages/messages";
import { useIntl } from "react-intl";
export function GroupList(props) {
    var intl = useIntl();
    var ListItems = useMemo(function () {
        return props.items.map(function (item, index) {
            var editMode = item.editMode;
            // TODO: Progressbar doesn't work.
            return (React.createElement("div", { key: item.id, className: classNames("GroupList_item", {
                    "GroupList_item-selected": props.selectedGroupID === item.id,
                    "GroupList_item-editMode": editMode
                }), onClick: function () {
                    return !editMode && props.onItemSelect && props.onItemSelect(item, index);
                } },
                React.createElement(TaskProgressView, { entityID: item.id, isOverlay: true, hasRelativeRoot: true, progressType: "stripe" },
                    React.createElement(TextField, { onValidate: props.onValidate, className: "GroupList_item_title", text: item.name, editMode: editMode, onChange: props.onChange, onEnter: function (groupName) {
                            if (isGroupDuplicate(groupName.trim(), props.items)) {
                                // return services.showError(
                                // intl.formatMessage(groupName.trim()
                                //   ? messages.appCommonSubjectExists
                                //   : messages.appCommonMustNotbeEmpty
                                //   , {
                                //     subject: intl.formatMessage(messages.appCommonGroupName)
                                //   })
                                // );
                                props.onError(intl.formatMessage(groupName.trim()
                                    ? messages.appCommonSubjectExists
                                    : messages.appCommonMustNotbeEmpty, {
                                    subject: intl.formatMessage(messages.appCommonGroupName)
                                }));
                            }
                            //If existing group is being edited
                            else {
                                var validState = isValidName(groupName);
                                switch (validState) {
                                    case NameValidState.VALID:
                                        props.onEnter(groupName);
                                        break;
                                    case NameValidState.TOO_LONG:
                                        props.onError(intl.formatMessage(messages.appCommonFormFieldErrorTooLong, {
                                            subject: intl.formatMessage(messages.appCommonGroupName)
                                        }));
                                        break;
                                    case NameValidState.TOO_SHORT:
                                        props.onError(intl.formatMessage(messages.appCommonFormFieldErrorTooShort, {
                                            subject: intl.formatMessage(messages.appCommonGroupName)
                                        }));
                                        break;
                                    case NameValidState.INVALID_CHARACTER:
                                        props.onError(intl.formatMessage(messages.appCommonFormFieldErrorContainsIvalidChars, {
                                            subject: intl.formatMessage(messages.appCommonGroupName)
                                        }));
                                        break;
                                }
                            }
                        }, onBlurOrEscape: props.onBlurOrEscape }),
                    React.createElement("div", { className: "GroupList_item_rightItem" },
                        React.createElement("div", { className: "GroupList_item_rightItem_icon" }, item.icon ? (React.createElement(item.icon, null)) : props.icon ? (React.createElement(props.icon, null)) : null),
                        React.createElement("div", { className: "GroupList_item_rightItem_key" },
                            item.count,
                            " ")))));
        });
    }, [props]);
    return (React.createElement("div", { className: "GroupList flex-column" },
        React.createElement(ScrollView, { className: "GroupList_scrollbar" }, ListItems),
        props.items.length === 0 && React.createElement("div", null, props.fallback)));
}
function isGroupDuplicate(name, array) {
    return array.some(function (e) { return e.name === name; });
}
//# sourceMappingURL=GroupList.js.map