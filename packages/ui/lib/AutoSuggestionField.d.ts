import React from "react";
declare type AutoSuggestionFieldProps = {
    onFilter: (val?: string) => string[];
    onSelect?: (val: string) => void;
    onChange?: (val: string) => void;
    value?: any;
    children: React.ReactElement<{
        onTextChange?: (val: any) => void;
        text: string;
        onFocus?: () => void;
        onChange: (val: string) => void;
    }>;
};
export declare function AutoSuggestionField(props: AutoSuggestionFieldProps): JSX.Element;
export {};
//# sourceMappingURL=AutoSuggestionField.d.ts.map