/// <reference types="react" />
import { OS } from "@appcircle/core/lib/enums/OS";
declare type OSIconProps = {
    os: OS;
    className?: string;
    display?: "appIcon" | "small" | "tiny";
    style?: "dark" | "light";
};
export declare function OSIcon(props: OSIconProps): JSX.Element;
export {};
//# sourceMappingURL=OSIcon.d.ts.map