import React from "react";
import classNames from "classnames";
import { ButtonType } from "./Button";
export function TextButton(props) {
    var className = classNames("TextButton", "TextButton-" + (props.type || ButtonType.PRIMARY), {
        "SubmitButton-disabled": !!props.disabled
    });
    return (React.createElement("div", { className: className, onClick: props.onClick },
        React.createElement("div", null, props.title)));
}
//# sourceMappingURL=TextButton.js.map