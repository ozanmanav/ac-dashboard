import React, { useState } from "react";
import classNames from "classnames";
export var ButtonType;
(function (ButtonType) {
    ButtonType["DANGER"] = "danger";
    ButtonType["PRIMARY"] = "primary";
    ButtonType["SUCCESS"] = "success";
    ButtonType["WARNING"] = "warning";
    ButtonType["LIGHT"] = "primaryLight";
})(ButtonType || (ButtonType = {}));
export function Button(props) {
    var _a = useState(false), isFocussed = _a[0], setisFocussed = _a[1];
    var className = classNames("Button", props.className, "Button-" + (props.size || "md"), "Button-" + (props.type || "primary"), "Button-disabled--" + !!props.disabled, "Button-isFocus--" + !!isFocussed);
    return (React.createElement("button", { tabIndex: props.tabIndex, type: props.htmlType || "button", className: className, onClick: props.onClick, "aria-label": props.ariaLabel, onFocus: function (e) {
            props.showFocus && setisFocussed(true);
        }, onBlur: function (e) {
            props.showFocus && setisFocussed(false);
        } }, props.children));
}
//# sourceMappingURL=Button.js.map