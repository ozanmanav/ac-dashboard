import React, { PropsWithChildren } from "react";
import { ContextMenuItemProps } from "./ContextMenuItem";
export declare type ContextMenuProps = PropsWithChildren<{
    size?: "md" | "lg" | "sm";
    items: Array<ContextMenuItemProps>;
    isOpen?: false;
    onMenuItemSelected?: (selectedIndex: number) => void;
    theme?: "dark" | "light";
}>;
export declare const ContextMenu: (props: React.PropsWithChildren<{
    size?: "md" | "lg" | "sm" | undefined;
    items: ({
        onClick?: ((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void) | undefined;
        getDynamicText?: (() => string) | undefined;
    } & {
        text: string;
        icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
        size?: "md" | "lg" | "sm" | undefined;
        linkProps?: import("react-router-dom").LinkProps<{} | null | undefined> | undefined;
        disabled?: boolean | undefined;
    } & {
        children?: React.ReactNode;
    })[];
    isOpen?: false | undefined;
    onMenuItemSelected?: ((selectedIndex: number) => void) | undefined;
    theme?: "dark" | "light" | undefined;
}>) => JSX.Element;
//# sourceMappingURL=ContextMenu.d.ts.map