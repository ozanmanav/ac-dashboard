var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import classNames from "classnames";
import { Link } from "react-router-dom";
export function ContextMenuItem(props) {
    var Icon = props.icon;
    var text = props.getDynamicText ? props.getDynamicText() : props.text;
    return (React.createElement("div", { className: classNames("ContextMenuItem", {
            "ContextMenuItem-selected": false,
            "disabled": props.disabled
        }), onClick: function (e) {
            props.onClick && props.onClick(e);
        } },
        React.createElement(Icon, null),
        text ? React.createElement("span", null, text) : null,
        props.linkProps ? React.createElement(Link, __assign({}, props.linkProps)) : null));
}
//# sourceMappingURL=ContextMenuItem.js.map