var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import { ContextMenuItem } from "./ContextMenuItem";
import classNames from "classnames";
export var ContextMenu = function (props) {
    return (React.createElement("div", { className: classNames("ContextMenu", {
            "ContextMenu-dark": props.theme === "dark",
            "ContextMenu-light": props.theme === "light"
        }) },
        React.createElement("div", { className: "ContextMenu_items" }, props.items.map(function (item, index) { return (React.createElement(ContextMenuItem, __assign({ key: item.text }, item, { onClick: function () {
                props.onMenuItemSelected && props.onMenuItemSelected(index);
            } }))); }))));
};
//# sourceMappingURL=ContextMenu.js.map