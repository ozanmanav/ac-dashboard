import React, { PropsWithChildren, FunctionComponent, SVGProps } from "react";
import { LinkProps } from "react-router-dom";
export declare type ContextMenuItemDataType = PropsWithChildren<{
    text: string;
    icon: FunctionComponent<SVGProps<SVGSVGElement>>;
    size?: "md" | "lg" | "sm";
    linkProps?: LinkProps;
    disabled?: boolean;
}>;
export declare type ContextMenuItemProps = PropsWithChildren<{
    onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    getDynamicText?: () => string;
} & ContextMenuItemDataType>;
export declare function ContextMenuItem(props: ContextMenuItemProps): JSX.Element;
//# sourceMappingURL=ContextMenuItem.d.ts.map