import { PropsWithChildren } from "react";
export declare type TogglePanelProps = PropsWithChildren<{
    title: string;
    clickableTitle?: boolean;
    className?: string;
    onChange?: (isSelected: boolean) => void;
    isSelected?: boolean;
    isDisabled?: boolean;
}>;
export declare function TogglePanel(props: TogglePanelProps): JSX.Element;
//# sourceMappingURL=TogglePanel.d.ts.map