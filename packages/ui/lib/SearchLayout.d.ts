import React, { PropsWithChildren } from "react";
import { Model } from "@appcircle/core/lib/Model";
declare type SearchLayoutProps<TState, TResult> = PropsWithChildren<{
    onAddElement?: () => void;
    addButtonEnabled?: boolean;
    children: (result: TResult[]) => JSX.Element;
    isLoaded?: boolean;
    model: Model<TState, TResult>;
    header: string;
    noElementsText?: string;
    noElementsIcon: React.ElementType;
}>;
export declare function SearchLayout<TModelState = any, TModelResult = any>(props: SearchLayoutProps<TModelState, TModelResult>): JSX.Element;
export {};
//# sourceMappingURL=SearchLayout.d.ts.map