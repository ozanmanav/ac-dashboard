import React, { PropsWithChildren } from "react";
export declare enum ButtonType {
    DANGER = "danger",
    PRIMARY = "primary",
    SUCCESS = "success",
    WARNING = "warning",
    LIGHT = "primaryLight"
}
export declare type ButtonSize = "md" | "lg" | "sm" | "xsm";
export declare type ButtonProps = PropsWithChildren<{
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    size?: ButtonSize;
    type?: ButtonType;
    className?: string;
    ariaLabel?: string;
    htmlType?: React.ButtonHTMLAttributes<HTMLButtonElement>["type"];
    showFocus?: boolean;
    tabIndex?: number;
    disabled?: boolean;
}>;
export declare function Button(props: ButtonProps): JSX.Element;
//# sourceMappingURL=Button.d.ts.map