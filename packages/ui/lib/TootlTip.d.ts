import { PropsWithChildren } from "react";
declare type Props = PropsWithChildren<{
    left?: number;
    right?: number;
    top?: number;
    bottom?: number;
}>;
export declare function ToolTip(props: Props): JSX.Element | null;
export {};
//# sourceMappingURL=TootlTip.d.ts.map