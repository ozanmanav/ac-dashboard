import React from "react";
import { IconButton } from "./IconButton";
import { SvgAdd } from "@appcircle/assets/lib/Add";
export function AddButton(props) {
    var className = "AddButton";
    return (React.createElement(IconButton, { className: className, icon: SvgAdd, onClick: props.onClick, size: props.size, state: props.state, tabIndex: props.tabIndex }, props.text));
}
//# sourceMappingURL=AddButton.js.map