import { ReactElement } from "react";
export declare type ProfileGridViewProfileCardProps<TData = any> = {
    data: TData;
    detailUrl: string;
    index: number;
    onEditMode: (val: boolean, index: number) => void;
    className: string;
    highlighted?: boolean;
};
declare type ProfileGridViewProfileCard<TData = any> = (props: ProfileGridViewProfileCardProps<TData>) => ReactElement;
declare type ProfilesGridProps<P> = {
    profilesData: P[];
    profileCardComponent: ProfileGridViewProfileCard;
    link: string;
};
export declare function ProfileGridView<P extends {
    id: string;
} = any>(props: ProfilesGridProps<P>): JSX.Element;
export {};
//# sourceMappingURL=ProfileGridView.d.ts.map