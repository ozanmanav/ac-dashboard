import React from "react";
import { Spinner } from "./Spinner";
//TODO: Refactor this with the new Button
export function SpinnerWithText(props) {
    return (React.createElement("div", { className: "SpinnerWithText" },
        React.createElement(Spinner, null),
        props.text ? React.createElement("span", null, props.text) : null));
}
//# sourceMappingURL=SpinnerWithText.js.map