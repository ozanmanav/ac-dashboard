import { PropsWithChildren } from "react";
import { ModuleNavBarDataItemType } from "@appcircle/shared/lib/Module";
export declare type ModuleNavBarMenuItemProps = PropsWithChildren<ModuleNavBarDataItemType & {
    index: number;
    onClick?: (index: number) => void;
}>;
export declare function ModuleNavBarMenuItem(props: ModuleNavBarMenuItemProps): JSX.Element;
//# sourceMappingURL=ModuleNavBarMenuItem.d.ts.map