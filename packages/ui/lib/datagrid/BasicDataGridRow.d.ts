import { PropsWithChildren, ReactElement } from "react";
import { CheckBoxProps } from "../form/CheckBox";
export declare type BasicDataGridRowProps<TRowData extends {
    id: string;
}> = PropsWithChildren<{
    data: TRowData;
    rowTemplate: BasicDataGridRowTemplateType<TRowData>;
    index: number;
    isHeader?: boolean;
    className?: string;
    isSelected?: boolean;
    onSelected: CheckBoxProps["onChange"];
    withCheckbox?: boolean;
    onRowClick?: (data: TRowData) => void;
    showProgressonRow?: boolean;
}>;
export declare type BasicDataGridRowTemplateType<TRowData = any> = (props: BasicDataGridRowTemplateProps<TRowData>) => ReactElement;
export declare type BasicDataGridRowTemplateProps<TRowData = any> = {
    id: string;
    data: TRowData;
    index: number;
};
export declare type BasicDataGridHeaderTemplateType = (props?: any) => ReactElement;
export declare type BasicDataGridColHeadersProps<THeader> = {
    data: THeader;
    headerTemplate: BasicDataGridHeaderTemplateType;
    className?: string;
    isSelected?: boolean;
    onSelectAll: (selected: boolean) => void;
    withCheckbox?: boolean;
};
export declare function BasicDataGridColHeaders<TRowData = any>(props: BasicDataGridColHeadersProps<TRowData>): JSX.Element;
export declare function BasicDataGridRow<TRowData extends {
    id: string;
} = any>(props: BasicDataGridRowProps<TRowData>): JSX.Element;
//# sourceMappingURL=BasicDataGridRow.d.ts.map