import React from "react";
import classNames from "classnames";
import { CheckBox } from "../form/CheckBox";
import { TaskProgressView } from "../TaskProgressView";
export function BasicDataGridColHeaders(props) {
    return (React.createElement("div", { className: "BasicDataGridRow BasicDataGridRow_header " + props.className },
        props.withCheckbox !== false ? (React.createElement(CheckBox, { isSelected: props.isSelected || false, value: "header", name: "header", onChange: function (value) {
                props.onSelectAll(value);
            } })) : null,
        React.createElement(props.headerTemplate, { data: props.data })));
}
export function BasicDataGridRow(props) {
    return (React.createElement("div", { className: classNames("BasicDataGridRow", {
            "BasicDataGridRow-selected": props.isSelected
        }), onClick: function () { return props.onRowClick && props.onRowClick(props.data); } },
        props.showProgressonRow !== false ? (React.createElement(TaskProgressView, { entityID: props.data.id, hasRelativeRoot: true, progressType: "stripe" })) : null,
        props.withCheckbox !== false && (React.createElement(CheckBox, { isSelected: props.isSelected || false, value: props.data.id, name: props.data.id, onChange: props.onSelected })),
        React.createElement(props.rowTemplate, { id: props.data.id, data: props.data, index: props.index })));
}
//# sourceMappingURL=BasicDataGridRow.js.map