import { ReactElement } from "react";
import { BasicDataGridRowTemplateType, BasicDataGridHeaderTemplateType, BasicDataGridRowProps } from "./BasicDataGridRow";
export declare type BasicDataGridProps<TRowData extends {
    id: string;
} = any> = {
    data: TRowData[];
    headerData?: string[];
    headerTemplate?: BasicDataGridHeaderTemplateType;
    rowTemplate: BasicDataGridRowTemplateType<TRowData>;
    rowSeperatorTemplate: () => ReactElement;
    headerClassName?: string;
    rowClassName?: string;
    onItemsSelected?: BasicDataGridItemsSelectedHandler;
    selectedItems?: SelectedItemsType;
    updateParameter?: string;
    inProgress?: boolean;
    withCheckbox?: boolean;
    onRowClick?: BasicDataGridRowProps<TRowData>["onRowClick"];
    keyField?: "loog-index" | "id";
    keyMap?: string[];
    showAutoProgressOnRow?: boolean;
};
export declare type BasicDataGridItemsSelectedHandler = (items: string[]) => void;
export declare type SelectedItemsType = {
    [id: string]: boolean;
};
export declare function BasicDataGrid<TRowData extends {
    id: string;
} = any, THeaderData = any>(props: BasicDataGridProps<TRowData>): JSX.Element;
export declare function BasicDataGridWithCustomHead<TRowData extends {
    id: string;
} = any, THeaderData = any>(props: BasicDataGridProps<TRowData> & {
    head: JSX.Element;
}): JSX.Element;
export declare function BasicDataGridHeadless<TRowData extends {
    id: string;
} = any, THeaderData = any>(props: BasicDataGridProps<TRowData>): JSX.Element;
export declare function BasicDataGridWithProgress<TRowData extends {
    id: string;
} = any, THeaderData = any>(props: BasicDataGridProps<TRowData> & {
    connectTask?: string;
}): JSX.Element;
//# sourceMappingURL=BasicDataGrid.d.ts.map