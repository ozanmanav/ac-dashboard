var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useState, useCallback, useRef, useEffect } from "react";
import { BasicDataGridRow, BasicDataGridColHeaders } from "./BasicDataGridRow";
import { TaskProgressView } from "../TaskProgressView";
import { ScrollView } from "../ScrollView";
export function BasicDataGrid(props) {
    return (React.createElement("div", { className: "BasicDataGrid" },
        React.createElement(BasicDataGridHeadless, __assign({}, props))));
}
export function BasicDataGridWithCustomHead(props) {
    return (React.createElement("div", { className: "BasicDataGrid" }, React.cloneElement(props.head, {}, React.createElement(BasicDataGridHeadless, __assign({}, props)))));
}
export function BasicDataGridHeadless(props) {
    var selectedItems = useRef({});
    var _a = useState(false), allSelected = _a[0], setAllSelected = _a[1];
    useEffect(function () {
        if (props.selectedItems) {
            selectedItems.current = props.selectedItems;
        }
    }, [props.selectedItems]);
    var onSelected = useCallback(function (id, name) {
        var _a;
        var status = id !== undefined;
        selectedItems.current = __assign(__assign({}, selectedItems.current), (_a = {}, _a[name] = status, _a));
        props.onItemsSelected &&
            props.onItemsSelected(Object.keys(selectedItems.current).filter(function (nm) { return selectedItems.current[nm]; }));
        setAllSelected(Object.keys(selectedItems.current).filter(function (key) { return selectedItems.current[key] === true; }).length === props.data.length);
    }, [props, selectedItems]);
    var onSelectAll = useCallback(function (val) {
        var status = val !== undefined;
        setAllSelected(status);
        selectedItems.current = props.data.reduce(function (acc, item) {
            acc[item.id] = status;
            return acc;
        }, {});
        props.onItemsSelected &&
            props.onItemsSelected(Object.keys(selectedItems.current).filter(function (id) { return selectedItems.current[id]; }));
    }, [props, selectedItems]);
    var click = function (e) { };
    return (React.createElement(React.Fragment, null,
        props.headerTemplate && (React.createElement(BasicDataGridColHeaders, { key: "header", headerTemplate: props.headerTemplate, data: props.headerData, className: props.headerClassName, onSelectAll: onSelectAll, isSelected: allSelected, withCheckbox: props.withCheckbox })),
        React.createElement(ScrollView, null, React.createElement("div", { className: "BasicDataGrid_body", onMouseDown: click }, (props.data || []).map(function (item, index) { return (React.createElement(BasicDataGridRow, { showProgressonRow: props.showAutoProgressOnRow, index: index, key: props.keyField !== undefined &&
                props.keyField === "loog-index"
                ? index
                : item[props.keyField || "id"], data: item, rowTemplate: props.rowTemplate, className: props.rowClassName, onSelected: onSelected, isSelected: !!selectedItems.current[item.id], withCheckbox: props.withCheckbox, onRowClick: props.onRowClick })); })))));
}
export function BasicDataGridWithProgress(props) {
    return (React.createElement("div", { className: "BasicDataGrid" },
        React.createElement(TaskProgressView, { entityID: props.connectTask, hasRelativeRoot: true },
            React.createElement(BasicDataGridHeadless, __assign({}, props)))));
}
//# sourceMappingURL=BasicDataGrid.js.map