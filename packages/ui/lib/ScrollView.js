import { useEffect, useRef, forwardRef, useCallback, useLayoutEffect, useState } from "react";
import React from "react";
var style = {
    overflowY: "auto",
    overflowX: "hidden",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    flexGrow: 1
};
var styleChild = {
    flexGrow: 1
};
function useCombinedRefs() {
    var refs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        refs[_i] = arguments[_i];
    }
    var targetRef = useRef(null);
    useEffect(function () {
        refs.forEach(function (ref) {
            if (!ref)
                return;
            if (typeof ref === "function") {
                ref(targetRef.current || null);
            }
            else {
                // @ts-ignore
                ref.current = targetRef.current;
            }
        });
    }, [refs]);
    return targetRef;
}
// export function getScrollBreakPoint(Element: HTMLElement) {
//   return <Element data-scrollview-breakpoint></Element>;
// }
export var ScrollView = forwardRef(function (props, ref) {
    var innerRef = useCombinedRefs(ref);
    var breakPointSelector = props.breakPointSelector;
    var elements = useRef(null);
    var _a = useState(), attheEnd = _a[0], setAttheEnd = _a[1];
    function getScrollElement() {
        return (innerRef.current.SimpleBar &&
            innerRef.current.SimpleBar.getScrollElement());
    }
    function scrollTo(top) {
        var el = getScrollElement();
        el && el.scroll({ top: top });
    }
    useEffect(function () {
        if (props.autoScrollToBottom === true && attheEnd !== false) {
            if (innerRef !== null) {
                var el = getScrollElement();
                el && scrollTo(el.scrollHeight);
                setAttheEnd(true);
            }
        }
    });
    useLayoutEffect(function () {
        if (props.showNavBar && innerRef.current !== null) {
            var observer_1 = new MutationObserver(function () {
                elements.current =
                    (innerRef.current &&
                        innerRef.current.querySelectorAll("" + breakPointSelector)) ||
                        null;
            });
            observer_1.observe(innerRef.current, { childList: true, subtree: true });
            var handler_1;
            handler_1 = function (e) {
                var el = getScrollElement();
                if (!el)
                    return;
                if (el.scrollHeight - el.scrollTop - e.currentTarget.scrollHeight <
                    50) {
                    setAttheEnd(true);
                }
                else {
                    setAttheEnd(false);
                }
            };
            innerRef.current.addEventListener("scroll", handler_1, true);
            return function () {
                var _a;
                (_a = innerRef.current) === null || _a === void 0 ? void 0 : _a.removeEventListener("scroll", handler_1, true);
                observer_1.disconnect();
            };
        }
    }, [innerRef, breakPointSelector]);
    var jumpTo = useCallback(function (jump) {
        var _a, _b;
        if (elements.current) {
            var parentRect_1;
            var scrollHeight_1 = ((_a = getScrollElement()) === null || _a === void 0 ? void 0 : _a.scrollHeight) || 0;
            var currentTop_1 = ((_b = getScrollElement()) === null || _b === void 0 ? void 0 : _b.scrollTop) || 0;
            var nextScrollTop_1 = undefined;
            elements.current.forEach(function (element) {
                var _a;
                var rect = element.getBoundingClientRect();
                if (parentRect_1 === undefined)
                    parentRect_1 = ((_a = element.parentElement) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect()) || {
                        top: 0,
                        height: 0
                    };
                var diff = parentRect_1.top + currentTop_1;
                if (rect.top - diff < 0 && jump === "up")
                    nextScrollTop_1 = currentTop_1 + rect.top - diff;
                else if (rect.top - diff > 0 &&
                    nextScrollTop_1 === undefined &&
                    jump === "down") {
                    nextScrollTop_1 = currentTop_1 + rect.top - diff;
                }
                console.log(scrollHeight_1, nextScrollTop_1);
            });
            nextScrollTop_1 =
                nextScrollTop_1 ||
                    (nextScrollTop_1 === undefined ? (jump === "up" ? 0 : scrollHeight_1) : 0);
            scrollTo(nextScrollTop_1);
        }
    }, []);
    var onScrollUp = useCallback(function () {
        jumpTo("up");
    }, []);
    var onScrollDown = useCallback(function () {
        jumpTo("down");
    }, []);
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { ref: innerRef, style: style, className: "ScrollView " + (props.className || ""), "data-simplebar": true },
            React.createElement("div", { style: styleChild }, props.children)),
        props.showNavBar && (React.createElement(React.Fragment, null,
            React.createElement("div", { onClick: onScrollUp, className: "ScrollView_scrollNav ScrollView_scrollNav_top" }, ">"),
            React.createElement("div", { onClick: onScrollDown, className: "ScrollView_scrollNav ScrollView_scrollNav_bottom" }, ">")))));
});
//# sourceMappingURL=ScrollView.js.map