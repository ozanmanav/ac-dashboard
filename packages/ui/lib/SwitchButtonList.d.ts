import { PropsWithChildren, FunctionComponent, SVGProps } from "react";
export declare type SwitchButtonProps = PropsWithChildren<{
    items: Array<FunctionComponent<SVGProps<SVGSVGElement>>>;
    selectedIndex: number;
    onChange?: (index: number) => void;
}>;
export declare function SwitchButtonList(props: SwitchButtonProps): JSX.Element;
//# sourceMappingURL=SwitchButtonList.d.ts.map