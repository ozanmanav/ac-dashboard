import React from "react";
import { SpinnerWithText } from "./SpinnerWithText";
export function ProgressView(props) {
    var type = props.type || "spinner";
    var indicatorComp = props.indicatorComp || type === "spinner" ? (React.createElement(SpinnerWithText, { text: props.progressText || "" })) : (React.createElement("div", { className: "stripeAnimation SpinnerWithText" }, props.progressText ? props.progressText : null));
    var isOverlay = props.isInProgress ? !!props.isOverlay : "none";
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "ProgressView ProgressView-is-overlay--" +
                isOverlay +
                " ProgressView-show--" +
                !!props.isInProgress },
            React.createElement("div", { onClick: props.onClick, className: "ProgressView_overlay" }, indicatorComp)),
        props.children));
}
//# sourceMappingURL=ProgressView.js.map