import React, { useState, useMemo } from "react";
import { SearchField } from "./SearchField";
import { debounce } from "@appcircle/core/lib/utils";
import { IconButtonState } from "./IconButton";
import { useIntl } from "react-intl";
import messages from "@appcircle/shared/lib/messages/messages";
import { SvgSearch } from "@appcircle/assets/lib/Search";
import { AddButton } from "./AddButton";
import { ProgressView } from "./ProgressView";
import { FlatHeader } from "./FlatHeader";
var DEBOUNCE_DELAY = 300;
export function SearchLayout(props) {
    var items = props.model.getAll();
    var _a = useState(""), searchText = _a[0], setSearchText = _a[1];
    var searchItems = useMemo(function () {
        return debounce(function (_text) {
            setSearchText(_text);
            setSearchResult(props.model.search(_text));
        }, DEBOUNCE_DELAY);
    }, [props.model]);
    var _b = useState(function () { return []; }), searchResult = _b[0], setSearchResult = _b[1];
    var intl = useIntl();
    // const isTemporaryProfileExists = profilesModel.hasTemporary();
    var result = searchText ? searchResult : items;
    return (React.createElement("div", { className: "SearchLayout SearchLayout_results" },
        React.createElement(FlatHeader, null,
            React.createElement("div", { className: "FlatHeader__child-left" }),
            React.createElement("div", { className: "FlatHeader__child-right" },
                React.createElement(SearchField, { text: searchText, placeHolder: intl.formatMessage(messages.appCommonSearch), onChange: searchItems }),
                React.createElement(AddButton, { state: searchText || props.addButtonEnabled
                        ? IconButtonState.DISABLED
                        : IconButtonState.ENABLED, text: intl.formatMessage(messages.appCommonAddNew), size: "sm", onClick: props.onAddElement }))),
        React.createElement(ProgressView, { isInProgress: !props.isLoaded, progressText: intl.formatMessage(messages.appCommonLoading), isOverlay: true },
            React.createElement(React.Fragment, null,
                items.length === 0 &&
                    (!searchText ? (React.createElement(NoElements, { icon: props.noElementsIcon, text: props.noElementsText ||
                            intl.formatMessage(messages.appCommonNoResult) })) : (React.createElement(NoSearchResult, { searchText: searchText, intl: intl }))),
                React.createElement("div", { style: useMemo(function () { return ({
                        display: result.length === 0 ? "none" : "block",
                        flexGrow: 1,
                        overflow: "hidden"
                    }); }, [result]) }, props.children(result))))));
}
function NoElements(props) {
    return (React.createElement("div", { className: "NoProfile" },
        React.createElement(props.icon, { className: "NoProfile_icon" }),
        React.createElement("div", { className: "NoProfile_text" }, props.text)));
}
function NoSearchResult(props) {
    return (React.createElement("div", { className: "NoProfile" },
        React.createElement(SvgSearch, { className: "NoProfile_icon" }),
        React.createElement("div", { className: "NoProfile_text" }, props.intl.formatMessage(messages.appCommonNoResultFor, {
            keyword: props.searchText
        }))));
}
//# sourceMappingURL=SearchLayout.js.map