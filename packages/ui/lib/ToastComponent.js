import React, { useEffect, useState, useRef } from "react";
import classNames from "classnames";
import { SvgToastClose } from "@appcircle/assets/lib/ToastClose";
import { useIntl } from "react-intl";
import { DEFAULT_MESSAGE_LIFE, DEFAULT_TOAST_HEIGHT, TOAST_VERTICAL_GAP } from "./ToastService";
export var ToastComponent = function (props) {
    var messageLife = props.message.life || DEFAULT_MESSAGE_LIFE;
    var message = props.message;
    var _a = useState("show"), status = _a[0], setStatus = _a[1];
    var style = {
        transform: "translateY(" + -props.index *
            (DEFAULT_TOAST_HEIGHT + TOAST_VERTICAL_GAP) + "px)",
        height: DEFAULT_TOAST_HEIGHT
    };
    var intl = useIntl();
    var timer = useRef();
    useEffect(function () {
        clearTimeout(timer.current);
        switch (status) {
            case "delete":
                props.onRemove(message);
                break;
            case "hide":
                timer.current = setTimeout(function () {
                    setStatus("delete");
                }, 300);
                break;
            case "show":
                timer.current = setTimeout(function () {
                    setStatus("hide");
                }, messageLife);
                break;
            default:
                break;
        }
        // eslint-disable-next-line
    }, [status]);
    return (React.createElement("div", { onClick: function () {
            setStatus("hide");
        }, className: classNames("toast", message.messageType, {
            show: status === "show",
            hide: status === "hide"
        }), style: style },
        React.createElement("label", { className: "toast__message" }, message.messageId === true
            ? intl.formatMessage({ id: message.message })
            : message.message),
        React.createElement(SvgToastClose, { className: "toast__close" })));
};
//# sourceMappingURL=ToastComponent.js.map