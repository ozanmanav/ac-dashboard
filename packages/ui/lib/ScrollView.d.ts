import React from "react";
export declare const ScrollView: React.ForwardRefExoticComponent<{
    className?: string | undefined;
    autoScrollToBottom?: boolean | undefined;
    showNavBar?: boolean | undefined;
    breakPointSelector?: string | undefined;
} & {
    children?: React.ReactNode;
} & React.RefAttributes<HTMLDivElement | null>>;
//# sourceMappingURL=ScrollView.d.ts.map