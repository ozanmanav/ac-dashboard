/// <reference types="react" />
import { TaskStatusType } from "@appcircle/taskmanager/lib/TaskStatusType";
export declare type ConnectTaskProps = {
    loadingComponent?: (taskStatus: TaskStatusType | undefined) => JSX.Element;
    errorComponent?: (taskStatus: TaskStatusType | undefined) => JSX.Element;
    render: (props: TaskStatusType | undefined) => JSX.Element;
    entityID?: string;
    taskID?: string;
};
export declare function ConnectTask(props: ConnectTaskProps): JSX.Element;
//# sourceMappingURL=ConnectTask.d.ts.map