import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
export function ConnectTask(props) {
    var taskManager = useTaskManager();
    var taskStatus = props.entityID
        ? taskManager.getStatusByEntityID(props.entityID)
        : props.taskID
            ? taskManager.getStatusByTaskId(props.taskID)
            : undefined;
    var ADDED = taskStatus && taskStatus.status === "ADDED" && props.loadingComponent;
    var LOADING = !taskStatus ||
        taskStatus.status === "IN_PROGRESS" ||
        taskStatus.status === "WAITING_RESPONSE";
    // && props.loadingComponent;
    var ERROR = taskStatus && taskStatus.status === "ERROR";
    var component = (ADDED || LOADING) && taskStatus && props.loadingComponent
        ? props.loadingComponent(taskStatus)
        : ERROR && props.errorComponent
            ? props.errorComponent(taskStatus)
            : props.render(taskStatus);
    return component;
}
//# sourceMappingURL=ConnectTask.js.map