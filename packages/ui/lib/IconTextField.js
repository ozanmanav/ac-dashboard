import React, { useState, useCallback, useRef, useEffect } from "react";
import classNames from "classnames";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
export function IconTextField(props) {
    var Icon = props.icon, classNameModifier = props.classNameModifier;
    var _a = useState(props.text || ""), text = _a[0], setText = _a[1];
    var ref = useRef(null);
    var onChange = useCallback(function (e) {
        setText(e.target.value);
        props.onChange(e.target.value);
    }, [props, text]);
    useEffect(function () {
        if (text !== props.text && props.text !== undefined)
            setText(props.text || "");
    }, [props.text]);
    var onKeyDown = useCallback(function (e) {
        e.stopPropagation();
        if (e.keyCode === KeyCode.ESC_KEY) {
            props.onChange("");
            setText("");
        }
        props.onKeyDown && props.onKeyDown(e);
    }, [props]);
    return (React.createElement("div", { className: classNames("IconTextField", classNameModifier) },
        React.createElement(Icon, { className: "IconTextField_icon" }),
        React.createElement("input", { ref: ref, tabIndex: props.tabIndex, type: "text", className: "IconTextField_input", spellCheck: false, value: text, placeholder: props.placeHolder, onChange: onChange, onKeyDown: onKeyDown })));
}
//# sourceMappingURL=IconTextField.js.map