import { TaskPropsType, TaskFactoryFn } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { ComponentType } from "react";
export declare enum LoadableContentStatus {
    IDLE = 0,
    LOADED = 1,
    IN_PROGRESS = 2,
    ERROR = 3
}
export declare function withLoadableContent<TComp = {}, TPreload = {}, TError = {}>(Component: ComponentType<{
    onRunTask: (task: () => [TaskFactoryFn<any>, TaskPropsType<any>]) => void;
}>, PreloaderComponent?: ComponentType<any>, // set default component if it exists
ErrorComponent?: ComponentType<any>): (props: TComp) => JSX.Element;
//# sourceMappingURL=withLoadableContent.d.ts.map