import React from "react";
export function DataList(props) {
    return (React.createElement("div", { className: "DataList" },
        React.createElement("div", { className: "DataList_header" }),
        React.createElement("div", { className: "DataList_items", "data-simplebar": true },
            React.createElement("div", null, props.children))));
}
//# sourceMappingURL=DataList.js.map