import { useEffect } from "react";
import React from "react";
import { checkMousePointerInElement } from "@appcircle/core/lib/utils/checkMousePointerInElement";
import classNames from "classnames";
export function DataListItem(props) {
    var rootRef = React.createRef();
    useEffect(function () {
        if (!props.isTemporary)
            return;
        var clickHandler = function (e) {
            if ((rootRef.current &&
                checkMousePointerInElement(e.clientX, e.clientY, rootRef.current.getBoundingClientRect())) ||
                (props.excludeKillTarget &&
                    checkIsInProfileDetailPage(e.target, props.excludeKillTarget)))
                return;
            props.onKillFocus && props.onKillFocus();
        };
        document.addEventListener("click", clickHandler);
        return function () {
            setTimeout(function () { return document.removeEventListener("click", clickHandler); }, 300);
        };
    }, [props, rootRef]);
    return (React.createElement("div", { ref: rootRef, className: classNames("DataListItem", props.className, {
            "DataListItem-selected": props.isSelected
        }), onClick: function () {
            props.onClick && props.onClick();
        } }, props.children));
}
function checkIsInProfileDetailPage(target, excludeClassName) {
    if (target.classList.contains(excludeClassName))
        return true;
    if (target.parentElement)
        return checkIsInProfileDetailPage(target.parentElement, excludeClassName);
    return false;
}
//# sourceMappingURL=DataListItem.js.map