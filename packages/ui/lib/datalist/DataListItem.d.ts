import { PropsWithChildren } from "react";
export declare type DataListItemType = PropsWithChildren<{
    onKillFocus?: () => void;
    onClick?: () => void;
    isTemporary?: boolean;
    isSelected?: boolean;
    excludeKillTarget?: string;
    className?: string;
}>;
export declare function DataListItem(props: DataListItemType): JSX.Element;
//# sourceMappingURL=DataListItem.d.ts.map