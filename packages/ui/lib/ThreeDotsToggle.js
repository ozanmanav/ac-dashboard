import React, { useState, useMemo, useRef } from "react";
import classNames from "classnames";
import { useSpring, animated, config } from "react-spring";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
import { SvgMore } from "@appcircle/assets/lib/More";
var fadeout = {
    config: config.stiff,
    to: {
        opacity: 0,
        transform: "translateY(-10px)"
    },
    from: {
        zIndex: 25,
        position: "relative",
        opacity: 1,
        transform: "translateY(0)"
    }
};
var fadein = {
    to: { opacity: 1, transform: "translateY(0)" },
    from: {
        opacity: 0,
        transform: "translateY(-10px)"
    }
};
export function ThreeDotsToggle(props) {
    var _a = useSpring(function () { return fadein; }), animProps = _a[0], set = _a[1];
    var root = useRef(null);
    useGlobalClickService([], root, function () {
        setIsOpen(false);
    });
    var _b = useState(props.isOpen || false), isOpen = _b[0], setIsOpen = _b[1];
    set(isOpen ? fadein : fadeout);
    var style = useMemo(function () { return ({
        display: isOpen ? "block" : "none"
    }); }, [isOpen]);
    return (React.createElement("div", { ref: root, className: "ThreeDotsToggle", onClick: function (e) {
            setIsOpen(!isOpen);
        } },
        React.createElement("div", { className: classNames("ThreeDotsToggle_icon", {
                "ThreeDotsToggle_icon-isOpen": isOpen
            }) },
            React.createElement(SvgMore, null)),
        isOpen && (React.createElement(animated.div, { style: animProps }, props.children))));
}
//# sourceMappingURL=ThreeDotsToggle.js.map