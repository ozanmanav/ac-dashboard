import React from "react";
import { IconButtonState } from "./IconButton";
declare type GroupsDetailLayoutProps = {
    mobileMode: boolean;
    displayNewButton?: boolean;
    onNew?: () => void;
    sideBar: React.ReactElement<any>;
    detail: React.ReactElement<any>;
    onMobileModeChange: (mode: boolean) => void;
    newButtonState?: IconButtonState;
    title: string;
};
export declare function GroupsDetailLayout(props: GroupsDetailLayoutProps): JSX.Element;
export {};
//# sourceMappingURL=GroupsDetailLayout.d.ts.map