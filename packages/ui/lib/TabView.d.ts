import { PropsWithChildren } from "react";
export declare type TabViewComponentProps<TState = any> = {
    tabName: string;
    onStateChange?: (state: TState, tabName: string) => void;
    state?: TState | undefined;
};
export declare type TabViewProps = PropsWithChildren<{
    tabs: Array<{
        title: string;
        indicator?: boolean;
        component?: {
            type: string;
            props: TabViewComponentProps;
            key?: any;
        };
    }>;
    selectedIndex?: number;
    onChange?: (index: number) => void;
}>;
export declare function TabView(props: TabViewProps): JSX.Element;
//# sourceMappingURL=TabView.d.ts.map