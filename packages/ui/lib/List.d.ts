import React from "react";
import { ListItemsType, ListItemType } from "./ListItem";
declare type ListProps = {
    data: ListItemsType;
    onItemSelect: (item: ListItemType, index: number) => void;
    onBlur?: () => void;
    isOpened: boolean;
};
export declare const List: React.ForwardRefExoticComponent<ListProps & React.RefAttributes<HTMLDivElement>>;
export {};
//# sourceMappingURL=List.d.ts.map