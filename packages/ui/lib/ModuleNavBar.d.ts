import { PropsWithChildren } from "react";
declare type Props = PropsWithChildren<{
    isBlock?: boolean;
}>;
export declare function ModuleNavBar(props: Props): JSX.Element;
export {};
//# sourceMappingURL=ModuleNavBar.d.ts.map