var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useState, useRef, useMemo } from "react";
import { TaggableTextArea } from "./TaggableTextArea";
import classNames from "classnames";
import { TagComponents } from "./Tags";
export function TaggableTextField(props) {
    var _a = useState(false), isTagInvalid = _a[0], setTagInvalid = _a[1];
    var containerRef = useRef(null);
    return (React.createElement("div", { className: "TaggableTextField", ref: containerRef },
        React.createElement("div", { className: classNames("TaggableTextField_child", {
                "TaggableTextField-error": isTagInvalid,
                "TaggableTextField-isEmpty": !(props.value && props.value.length)
            }, props.className) },
            React.createElement(TagComponents, { onDelete: props.deleteTag, showCloseIcon: true, tags: props.value }),
            React.createElement(TaggableTextArea, __assign({}, props, { autofocus: props.autofocus, blurIgnoreList: useMemo(function () { return ["TaggableTextField_child"]; }, []), tags: props.value, onTextChange: props.onTextChange, value: props.text, parentRef: containerRef, setTagInvalid: setTagInvalid, placeHolder: props.value && props.value.length ? "" : props.placeHolder, isTagInvalid: isTagInvalid })))));
}
//# sourceMappingURL=TaggableTextField.js.map