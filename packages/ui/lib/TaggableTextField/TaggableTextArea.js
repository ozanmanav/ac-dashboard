import React, { useState, useCallback, useEffect, useRef } from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import shortid from "shortid";
import { useNotificationsService } from "@appcircle/shared/lib/notification/useNotificationsService";
import { useGlobalClickService } from "@appcircle/shared/lib/services/useGlobalClickService";
var lineHeight = 22;
export var TaggableTextArea = function (props) {
    var target = useRef(null);
    var parentRef = useRef(null);
    var setTagInvalid = props.setTagInvalid, deleteTag = props.deleteTag, onChange = props.onChange, matchRule = props.matchRule;
    var pushMessage = useNotificationsService().pushMessage;
    var separator = props.separator || /,/g;
    var focusTextAreaClickHandler = useCallback(function () { return target.current && target.current.focus(); }, [target]);
    var _a = useState(true), canAdd = _a[0], setCanAdd = _a[1];
    useEffect(function () {
        props.parentRef &&
            props.parentRef.current &&
            props.parentRef.current.addEventListener("click", focusTextAreaClickHandler);
        return function () {
            props.parentRef &&
                props.parentRef.current &&
                props.parentRef.current.removeEventListener("click", focusTextAreaClickHandler);
        };
    }, [props, focusTextAreaClickHandler]);
    var _b = useState(""), textValue = _b[0], setTextValue = _b[1];
    var _c = useState(1), rows = _c[0], setRows = _c[1];
    var initiateAddTagSequence = useCallback(addTagSequence(target, separator, matchRule, props, setTagInvalid, props.onChange), [
        props.onChange,
        matchRule,
        props.allowDuplicates,
        props.errors,
        props.tags,
        pushMessage,
        separator,
        setTagInvalid
    ]);
    var calculateSizeByValue = useCallback(function (value) {
        var textArea = target.current;
        if (!textArea)
            return value;
        var textAreaFontSize = (getComputedStyle(textArea).fontSize || "0").replace(/[^0-9]/g, "");
        textArea.style.width =
            String(value.length * 10 + parseInt(textAreaFontSize)) + "px";
        var oldRows = textArea.rows;
        textArea.rows = 1;
        var newRows = ~~(textArea.scrollHeight / lineHeight);
        if (newRows === oldRows) {
            textArea.rows = newRows;
        }
        if (!!value.match(separator) && canAdd) {
            value = initiateAddTagSequence();
            setRows(newRows);
        }
        return value;
    }, [canAdd, initiateAddTagSequence, separator]);
    useEffect(function () {
        props.value && setTextValue(calculateSizeByValue(props.value));
    }, [props.value, calculateSizeByValue]);
    var _d = useState(true), blurred = _d[0], setBlurred = _d[1];
    useEffect(function () {
        if (props.value && blurred) {
            setTextValue(initiateAddTagSequence(props.value));
            props.onTextChange && props.onTextChange(undefined);
        }
    }, [props.value, blurred]);
    function onOutSideClick() {
        setBlurred(true);
        props.onBlur && props.onBlur();
    }
    useEffect(function () {
        props.autofocus === true &&
            target.current &&
            setTimeout(function () { return target.current && target.current.focus(); }, 1);
    }, [target]);
    useGlobalClickService(props.blurIgnoreList || [], parentRef, onOutSideClick);
    return (React.createElement("div", { ref: parentRef, className: "TaggableTextArea " +
            (props.placeHolder ? "TaggableTextArea_empty" : "") },
        React.createElement("textarea", { autoFocus: !!props.autofocus, ref: target, rows: rows, value: textValue, placeholder: props.placeHolder || "", onFocus: function () {
                setBlurred(false);
                props.onFocus && props.onFocus();
            }, onBlur: function () {
                setTextValue(initiateAddTagSequence());
                props.onTextChange && props.onTextChange(undefined);
                props.onBlur && props.onBlur();
            }, onKeyDown: function (e) {
                if (e.keyCode === KeyCode.ESC_KEY) {
                    e.preventDefault();
                    props.onTextChange && props.onTextChange(undefined);
                    props.onBlur && props.onBlur();
                }
                else if (e.keyCode === KeyCode.ENTER_KEY) {
                    e.preventDefault();
                    setTextValue(initiateAddTagSequence());
                    props.onTextChange && props.onTextChange(undefined);
                    props.onBlur && props.onBlur();
                }
                else if (e.keyCode === KeyCode.BACKSPACE_KEY &&
                    props.tags &&
                    props.tags.length > 0) {
                    var textArea = e.target;
                    var currentValue = textArea.value;
                    if (!currentValue) {
                        var lastTag = props.tags[props.tags.length - 1];
                        deleteTag(lastTag.id);
                    }
                    setCanAdd(false);
                    props.onTextChange && props.onTextChange(undefined);
                    props.onError && props.onError(undefined);
                }
                else {
                    setCanAdd(true);
                }
            }, onInput: function (e) {
                // let textArea = e.target as HTMLTextAreaElement;
                if (props.isTagInvalid) {
                    setTagInvalid(false);
                }
                setTextValue(calculateSizeByValue(e.currentTarget.value));
            }, onChange: function (e) { return props.onTextChange && props.onTextChange(e.target.value); } })));
};
function addTagSequence(target, separator, matchRule, props, setTagInvalid, addTag) {
    return function (val) {
        var textArea = target.current;
        if (!textArea)
            return "";
        var entries = (val || textArea.value)
            .split(separator)
            .filter(function (item, index, self) { return self.indexOf(item) === index; });
        var currentValue = (entries || [""]).join(",");
        var invalidTags = [];
        var validTags = [];
        var showMatchError = false;
        var showDuplicateError = false;
        entries.forEach(function (entry) {
            if (entry) {
                if (matchRule && matchRule(entry) === false) {
                    var entryWithoutWhiteSpace = entry.replace(/ /g, "");
                    if (matchRule(entryWithoutWhiteSpace) === true) {
                        entry = entryWithoutWhiteSpace;
                        validTags.push({
                            id: shortid.generate(),
                            text: entry
                        });
                    }
                    else {
                        invalidTags.push(entry);
                        showMatchError = true;
                    }
                }
                else if (!props.allowDuplicates &&
                    (props.tags || []).map(function (tag) { return tag.text; }).indexOf(entry) > -1) {
                    invalidTags.push(entry);
                    showDuplicateError = true;
                }
                else {
                    validTags.push(createTag(entry));
                }
            }
        });
        if (validTags.length > 0) {
            addTag && addTag(validTags);
            currentValue = "";
        }
        if (invalidTags.length > 0) {
            setTagInvalid(true);
            currentValue = invalidTags.join(", ");
            if (!!currentValue.match(separator) && validTags.length === 0)
                showMatchError = showDuplicateError = false;
        }
        else {
            textArea.style.width = "0px";
        }
        if (!props.errors)
            return currentValue;
        if (props.errors.match && showMatchError) {
            props.onError && props.onError(props.errors.match);
        }
        else if (props.errors.duplicate && showDuplicateError) {
            props.onError && props.onError(props.errors.duplicate);
        }
        else {
            props.onError && props.onError(undefined);
        }
        return currentValue;
    };
}
export function createTag(entry) {
    return {
        id: shortid.generate(),
        text: entry
    };
}
//# sourceMappingURL=TaggableTextArea.js.map