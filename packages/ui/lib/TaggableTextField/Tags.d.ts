/// <reference types="react" />
export declare type Tag = {
    id: any;
    text: string;
};
export declare type TagComponentsProps = {
    tags?: Tag[];
    showCloseIcon: boolean;
    onDelete?: (id: any) => void;
};
export declare function TagComponents(props: TagComponentsProps): JSX.Element;
//# sourceMappingURL=Tags.d.ts.map