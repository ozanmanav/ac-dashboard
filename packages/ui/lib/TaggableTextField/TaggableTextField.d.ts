/// <reference types="react" />
import { TaggableTextAreaProps } from "./TaggableTextArea";
import { Tag } from "./Tags";
import { MessageDescriptor } from "react-intl";
export declare type TaggableTextFieldErrors = {
    duplicate?: string | MessageDescriptor;
    match?: string | MessageDescriptor;
};
export declare type TaggableTextFieldProps = {
    value?: Tag[];
    suggestions?: string[];
    onChange?: (tag: Tag[]) => void;
    deleteTag: (id: any) => void;
    matchRule?: (tagText: string) => boolean;
    allowDuplicates?: boolean;
    placeHolder?: string;
    separator?: RegExp;
    errors?: TaggableTextFieldErrors;
    className?: string;
    onError?: TaggableTextAreaProps["onError"];
    text?: string;
    onTextChange?: (text?: string) => void;
    autofocus?: boolean;
};
export declare function TaggableTextField(props: TaggableTextFieldProps): JSX.Element;
//# sourceMappingURL=TaggableTextField.d.ts.map