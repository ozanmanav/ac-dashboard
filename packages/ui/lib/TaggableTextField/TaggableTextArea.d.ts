import { RefObject } from "react";
import { Tag } from "./Tags";
import { TaggableTextFieldErrors } from "./TaggableTextField";
export declare type TaggableTextAreaProps = {
    autofocus?: boolean;
    tags?: Tag[];
    allowDuplicates?: boolean;
    isTagInvalid: boolean;
    placeHolder?: string;
    separator?: RegExp;
    errors?: TaggableTextFieldErrors;
    onBlur?: () => void;
    onFocus?: () => void;
    onChange?: (tag: Tag[]) => void;
    deleteTag: (id: any) => void;
    matchRule?: (tagText: string) => boolean;
    setTagInvalid: (val: boolean) => void;
    value?: string;
    onTextChange?: (val?: string) => void;
    onRef?: (ref: HTMLTextAreaElement | null) => void;
    parentRef?: RefObject<HTMLElement>;
    onError?: (error: any) => void;
    blurIgnoreList?: string[];
};
export declare const TaggableTextArea: (props: TaggableTextAreaProps) => JSX.Element;
export declare function createTag(entry: string): {
    id: string;
    text: string;
};
//# sourceMappingURL=TaggableTextArea.d.ts.map