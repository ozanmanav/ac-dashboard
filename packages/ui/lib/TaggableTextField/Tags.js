import React, { useCallback } from "react";
import { SvgCloseTag } from "@appcircle/assets/lib/CloseTag";
export function TagComponents(props) {
    return (React.createElement(React.Fragment, null, (props.tags || []).map(function (tag) { return (React.createElement(TagComponent, { onDelete: props.onDelete, showCloseIcon: props.showCloseIcon, key: tag.id, tag: tag })); })));
}
function TagComponent(props) {
    var onClick = useCallback(function () {
        props.onDelete && props.onDelete(props.tag.id);
    }, [props]);
    return (React.createElement("div", { className: "TaggableTextField_child_tag" },
        React.createElement("span", null, props.tag.text),
        props.showCloseIcon ? (React.createElement(SvgCloseTag, { className: "TaggableTextField_child_tag_close", onClick: onClick })) : null));
}
//# sourceMappingURL=Tags.js.map