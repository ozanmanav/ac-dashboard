import { useCallback, useMemo } from "react";
import { ProgressView } from "./ProgressView";
import React from "react";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
export function TaskProgressView(props) {
    var isOverlay = props.isOverlay === undefined ? true : props.isOverlay;
    var children = useCallback(function (task) {
        return typeof props.children === "function"
            ? props.children(task)
            : props.children;
    }, [props]);
    var taskmanager = useTaskManager();
    var tasks = useMemo(function () {
        return (props.tasks || taskmanager.getActiveTasksByEntityId(props.entityID || []));
    }, [taskmanager, props.entityID, props.tasks]);
    var isInProgress = tasks.length > 0;
    var progressComp = useMemo(function () {
        return !props.hasRelativeRoot ? (React.createElement("div", { style: {
                position: "relative",
                overflow: "auto",
                display: "auto",
                height: "inherit"
            } },
            React.createElement(ProgressView, { isOverlay: isOverlay, isInProgress: isInProgress, progressText: props.progressText, type: props.progressType, onClick: props.onClick }, children(tasks[0])))) : (React.createElement(ProgressView, { isOverlay: isOverlay, isInProgress: isInProgress, progressText: props.progressText, type: props.progressType, onClick: props.onClick }, children(tasks[0])));
    }, [isOverlay, props, children, isInProgress, tasks]);
    return !props.entityID && !props.taskID ? (React.createElement(React.Fragment, null, children(undefined))) : (progressComp);
}
//# sourceMappingURL=TaskProgressView.js.map