import React, { PropsWithChildren, FunctionComponent, SVGProps } from "react";
export declare type IconTextFieldProps = PropsWithChildren<{
    text?: string;
    placeHolder: string;
    onChange: (text: string) => void;
    icon: FunctionComponent<SVGProps<SVGSVGElement>>;
    classNameModifier?: string;
    onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    tabIndex?: number;
}>;
export declare function IconTextField(props: IconTextFieldProps): JSX.Element;
//# sourceMappingURL=IconTextField.d.ts.map