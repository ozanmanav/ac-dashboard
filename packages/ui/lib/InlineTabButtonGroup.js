import React, { useState, useEffect } from "react";
function InlineTabButton(props) {
    return (React.createElement("div", { onClick: props.onClick, className: "Button InlineTabButton InlineTabButton-active--" + !!props.active }, props.label));
}
export function InlineTabButtonGroup(props) {
    var _a = useState(props.selectedIndex), active = _a[0], setActive = _a[1];
    useEffect(function () {
        active !== undefined && props.onChange && props.onChange(active);
    }, [active]);
    useEffect(function () {
        setActive(props.selectedIndex);
    }, [props.selectedIndex]);
    return (React.createElement("div", { className: "InlineTabButtonGroup" }, props.items.map(function (item, index) {
        return (React.createElement(InlineTabButton, { onClick: function () { return setActive(index); }, active: active === index, label: item, key: index }));
    })));
}
//# sourceMappingURL=InlineTabButtonGroup.js.map