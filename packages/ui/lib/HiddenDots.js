import React from "react";
export function HiddenDots() {
    return (React.createElement(React.Fragment, null,
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" }),
        React.createElement("span", { className: "HiddenDot" })));
}
//# sourceMappingURL=HiddenDots.js.map