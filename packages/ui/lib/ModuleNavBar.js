import React from "react";
import classNames from "classnames";
import { SvgHamburger } from "@appcircle/assets/lib/Hamburger";
export function ModuleNavBar(props) {
    return (React.createElement("div", { className: classNames("ModuleNavBarContainer", {
            "ModuleNavBarContainer-block": props.isBlock
        }) },
        React.createElement("div", { className: "ModuleNavBar_handler" },
            React.createElement(SvgHamburger, null)),
        React.createElement("div", { className: classNames("ModuleNavBar", {
                "ModuleNavBar-block": props.isBlock
            }) }, props.children)));
}
//# sourceMappingURL=ModuleNavBar.js.map