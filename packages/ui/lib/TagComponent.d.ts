import { PropsWithChildren, FunctionComponent, SVGProps } from "react";
export declare type TagComponentProps = PropsWithChildren<{
    id?: any;
    icon?: FunctionComponent<SVGProps<SVGSVGElement>>;
    text: string;
    className?: string;
    onClick?: () => void;
    onClose?: (id: any) => void;
    modifiable?: boolean;
    isDisabled?: boolean;
}>;
export declare function TagComponent(props: TagComponentProps): JSX.Element;
//# sourceMappingURL=TagComponent.d.ts.map