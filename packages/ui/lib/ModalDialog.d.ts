import { PropsWithChildren } from "react";
export declare type ModalDialogProps = PropsWithChildren<{
    header: string | JSX.Element;
    className?: string;
    onModalClose?: () => void;
}>;
export declare function ModalDialogHeader(props: PropsWithChildren<{
    align?: "center" | "left" | "right";
}>): JSX.Element;
export declare function ModalDialog(props: ModalDialogProps): JSX.Element;
//# sourceMappingURL=ModalDialog.d.ts.map