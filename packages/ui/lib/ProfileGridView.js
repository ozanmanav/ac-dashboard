import React, { useCallback, useState } from "react";
import { ScrollView } from "./ScrollView";
export function ProfileGridView(props) {
    var profileData = props.profilesData, ProfileCardComponent = props.profileCardComponent;
    var _a = useState([]), editModeCards = _a[0], setEditcModeCards = _a[1];
    var onEditMode = useCallback(function (val, index) {
        setEditcModeCards(val ? [index] : []);
    }, []);
    return (React.createElement(ScrollView, null,
        React.createElement("div", { className: "ProfilesGridView_ProfilesGrid" },
            profileData.map(function (item, index) {
                var inEditMode = editModeCards.some(function (i) { return index === i; });
                return (React.createElement(ProfileCardComponent, { className: inEditMode ? "ProfileCard-inEditMode" : "", key: item.id, data: item, detailUrl: props.link + item.id, index: index, onEditMode: onEditMode }));
            }),
            React.createElement("div", { className: "editModeOverlay editModeOverlay-show--" + !!editModeCards.length }))));
}
//# sourceMappingURL=ProfileGridView.js.map