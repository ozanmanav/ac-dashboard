import React, { useState } from "react";
import TextField, { Input } from "@material/react-text-field";
import { SvgEye } from "@appcircle/assets/lib/Eye";
import { SvgEyeOff } from "@appcircle/assets/lib/EyeOff";
export function MaterialTextBox(props) {
    var _a = useState(props.text || ""), text = _a[0], setText = _a[1];
    var _b = useState(props.type || "text"), type = _b[0], setType = _b[1];
    return (React.createElement(TextField, { autoComplete: props.autoComplete, label: props.label, className: "MaterialTextBox", disabled: props.isDisabled, "aria-label": props.ariaLabel, onTrailingIconSelect: props.type === "password"
            ? function () {
                setType(type === "text" ? "password" : "text");
            }
            : undefined, trailingIcon: props.type === "password" ? (type === "password" ? (React.createElement(SvgEye, null)) : (React.createElement(SvgEyeOff, null))) : (undefined) },
        React.createElement(Input, { tabIndex: props.tabIndex, autoComplete: props.autoComplete, isValid: props.isValid, type: type, disabled: props.isDisabled, spellCheck: false, value: text, id: props.label, onChange: function (e) {
                setText(e.currentTarget.value);
                props.onChange && props.onChange(e.currentTarget.value);
            }, onBlur: function (e) {
                setText(e.currentTarget.value);
                props.onBlur && props.onBlur(e.currentTarget.value);
            } })));
}
//# sourceMappingURL=MaterialTextBox.js.map