import React, { useEffect, useState } from "react";
import classNames from "classnames";
var animationTimeout = 750;
var ConnectionStatusBar = function (_a) {
    var _b = _a.statusText, statusText = _b === void 0 ? "" : _b, _c = _a.isDisconnected, isDisconnected = _c === void 0 ? false : _c;
    var _d = useState(false), isFadedIn = _d[0], setIsFadedIn = _d[1];
    useEffect(function () {
        if (isDisconnected) {
            setIsFadedIn(true);
        }
        else {
            setTimeout(function () { return setIsFadedIn(false); }, animationTimeout);
        }
    }, [isDisconnected]);
    return (React.createElement("div", { className: classNames("ConnectionStatusBar", {
            "ConnectionStatusBar-fadeIn": isFadedIn,
            "ConnectionStatusBar-onlineStatus": !isDisconnected,
            "ConnectionStatusBar-hide": !isFadedIn && !isDisconnected
        }) }, statusText));
};
export default ConnectionStatusBar;
//# sourceMappingURL=ConnectionStatusBar.js.map