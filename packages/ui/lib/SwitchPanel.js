import React, { useState, useEffect } from "react";
import { Toggle } from "./form/Toggle";
import { Switch } from "./form/Switch";
export function SwitchPanel(props) {
    var _a = useState(!!props.isSelected), isSelected = _a[0], setIsSelected = _a[1];
    useEffect(function () {
        !!props.isSelected !== isSelected && setIsSelected(!!props.isSelected);
        // eslint-disable-next-line
    }, [props.isSelected]);
    useEffect(function () {
        props.onChange && props.onChange(isSelected);
    }, [isSelected]);
    return (React.createElement("div", { className: props.className + " SwitchPanel" },
        React.createElement(Toggle, { title: props.title, selected: isSelected, disabled: !!props.isDisabled, onClick: function () {
                setIsSelected(!isSelected);
            } },
            React.createElement(Switch, { isSelected: isSelected })),
        isSelected && props.children !== undefined && props.children));
}
//# sourceMappingURL=SwitchPanel.js.map