import React, { useEffect, useState, useRef, forwardRef } from "react";
import { KeyCode } from "@appcircle/core/lib/enums/KeyCode";
import { ScrollView } from "./ScrollView";
export var List = forwardRef(function (props, ref) {
    var target = useRef(null);
    var _a = useState(-1), selectedItem = _a[0], setSelectedItem = _a[1];
    useEffect(function () {
        if (!props.isOpened)
            return;
        var index = selectedItem;
        var handler = function (e) {
            switch (e.keyCode) {
                case KeyCode.ESC_KEY:
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    props.onBlur && props.onBlur();
                    break;
                case KeyCode.ENTER_KEY:
                    e.preventDefault();
                    props.data[index] && props.onItemSelect(props.data[index], index);
                    break;
                case KeyCode.DOWN_ARROW:
                    e.preventDefault();
                    index++;
                    break;
                case KeyCode.UP_ARROW:
                    e.preventDefault();
                    index--;
                    break;
                default:
                    break;
            }
            if (target.current) {
                // index = Math.min(Math.min(target.current.children.length, index), -1);
                // index = target.current.children.length - 1;
                if (target.current.children.length < index)
                    index = 0;
                else if (-1 > index)
                    index = target.current.children.length - 1;
                Math.max(target.current.children.length - 1, 0);
                setSelectedItem(index);
            }
        };
        document.body.addEventListener("keydown", handler);
        return function () { return document.body.removeEventListener("keydown", handler); };
    }, [props.onItemSelect, props.isOpened]);
    // useEffect(() => {
    //   const interval = setInterval(() => {
    //     target.current && ;
    //   }, 25);
    // }, [])
    // target.current && console.log(, target.current.offsetParent && target.current.offsetParent.scrollTop)
    return (React.createElement("div", { ref: ref, className: "List List-isOpened--" +
            (props.isOpened === undefined || props.isOpened) +
            " List-noItem--" +
            !!!props.data.length },
        React.createElement(ScrollView, null,
            React.createElement("ul", { ref: target }, props.data.map(function (item, index) {
                return (React.createElement("li", { className: selectedItem === index ? "List_item-selected" : "", key: item.value, onClick: function (e) {
                        e.stopPropagation();
                        props.onItemSelect(item, index);
                        setSelectedItem(index);
                    } },
                    React.createElement("span", null, item.label)));
            })))));
});
//# sourceMappingURL=List.js.map