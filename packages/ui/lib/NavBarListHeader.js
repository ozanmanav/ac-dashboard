import React from "react";
import classNames from "classnames";
export function NavBarListHeader(props) {
    return React.createElement("div", { className: classNames("NavBarListHeader") }, props.text);
}
//# sourceMappingURL=NavBarListHeader.js.map