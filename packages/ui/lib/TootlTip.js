import React, { useMemo } from "react";
export function ToolTip(props) {
    var style = useMemo(function () {
        return {
            left: props.left,
            top: props.top,
            bottom: props.bottom,
            right: props.right
        };
    }, [props.left, props.right, props.bottom, props.top]);
    return props.children ? (React.createElement("div", { className: "ToolTip", style: style },
        React.createElement("div", null, props.children))) : null;
}
//# sourceMappingURL=TootlTip.js.map