import React from "react";
import classNames from "classnames";
export function SwitchButtonList(props) {
    return (React.createElement("div", { className: "SwitchButton" }, props.items.map(function (item, index) {
        var Icon = item;
        return (React.createElement("div", { className: classNames("SwitchButton_item", {
                "SwitchButton_item-selected": index === props.selectedIndex
            }), key: index, onClick: function () {
                props.selectedIndex !== index &&
                    props.onChange &&
                    props.onChange(index);
            } },
            React.createElement(Icon, { className: "SwitchButton_item_icon" })));
    })));
}
//# sourceMappingURL=SwitchButtonList.js.map