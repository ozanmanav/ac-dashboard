import { Link } from "react-router-dom";
import React, { useCallback } from "react";
import classNames from "classnames";
export function ModuleNavBarMenuItem(props) {
    var onClick = useCallback(function () { return props.onClick && props.onClick(props.index); }, [props]);
    return (React.createElement("div", { className: classNames("NavBarListItem", {
            "NavBarListItem-selected": props.isSelected
        }), onClick: onClick },
        React.createElement(Link, { className: "NavBarListItem_link", to: props.link },
            React.createElement("div", { className: "NavBarListItem_text" }, props.text)),
        props.length ? (React.createElement("div", { className: "NavBarListItem_badge" }, props.length)) : null));
}
//# sourceMappingURL=ModuleNavBarMenuItem.js.map