var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useRef, useCallback, useState, useEffect } from "react";
import { useDrag, useDrop } from "react-dnd";
import produce from "immer";
import { SvgArrow } from "@appcircle/assets/lib/Arrow";
var style = {};
function DraggableListItem(props) {
    var ref = useRef(null);
    var disabled = props.disabled;
    var _a = useDrop({
        accept: "DraggableListItem",
        drop: function (item, monitor) {
            props.onDragEnd(item.index);
        },
        hover: function (item, monitor) {
            if (!ref.current) {
                return;
            }
            var dragIndex = item.index;
            var hoverIndex = props.index;
            if (dragIndex === hoverIndex) {
                return;
            }
            var hoverBoundingRect = ref.current.getBoundingClientRect();
            var hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
            var clientOffset = monitor.getClientOffset();
            // Get pixels to the top
            var hoverClientY = clientOffset.y - hoverBoundingRect.top;
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }
            // Time to actually perform the action
            //
            props.moveCard(dragIndex, hoverIndex);
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex;
        }
    }), drop = _a[1];
    var _b = useDrag({
        item: { type: "DraggableListItem", id: props.id, index: props.index },
        collect: function (monitor) { return ({
            isDragging: monitor.isDragging()
        }); },
        end: function () {
            props.onDragEnd(props.index);
        }
    }), isDragging = _b[0].isDragging, drag = _b[1];
    var opacity = isDragging ? 0 : 1;
    drag(drop(ref));
    return (React.createElement("div", { ref: ref, className: "DraggableListItem DraggableListItem-dragging--" +
            !!isDragging +
            " DraggableListItem-disabled--" +
            !!disabled, style: __assign(__assign({}, style), { opacity: opacity }), onClick: function () { return !isDragging && props.onSelect(props.index); } },
        React.createElement("div", { className: "DraggableListItem_hamburger" }, "\u2630"),
        React.createElement("div", null,
            React.createElement("div", { className: "DraggableListItem_title" }, props.text),
            React.createElement("div", { className: "DraggableListItem_description" }, props.description)),
        React.createElement("div", { className: "DraggableListItem_arrow" },
            React.createElement(SvgArrow, null))));
}
export function DraggableList(props) {
    var _a = useState(props.items), items = _a[0], setItems = _a[1];
    useEffect(function () {
        setItems(props.items);
    }, [props.items]);
    var moveCard = useCallback(function (dragIndex, hoverIndex) {
        var dragCard = items[dragIndex];
        var newItems = produce(items, function (draft) {
            draft.splice(dragIndex, 1);
            draft.splice(hoverIndex, 0, dragCard);
        });
        setItems(newItems);
    }, [items]);
    var renderItem = function (item, index) {
        return (React.createElement(DraggableListItem, { key: item.id, index: index, id: item.id, text: item.title, moveCard: moveCard, onDragEnd: function () {
                props.onChange(items);
            }, onSelect: function (index) {
                props.onSelect(index);
            }, description: item.description, disabled: item.disabled }));
    };
    return (React.createElement("div", { className: "DraggableList" }, items.map(function (item, i) { return renderItem(item, i); })));
}
//# sourceMappingURL=DraggableList.js.map