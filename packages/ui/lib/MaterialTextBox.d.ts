import { PropsWithChildren } from "react";
import { FormElementType } from "@appcircle/form/lib/Form";
export declare type MaterialTextBoxProps = PropsWithChildren<{
    label: string;
    autoComplete?: "on" | "off";
    text?: string;
    type?: string;
    isValid?: boolean;
    tabIndex?: number;
    ariaLabel?: string;
}> & FormElementType<string>;
export declare function MaterialTextBox(props: MaterialTextBoxProps): JSX.Element;
//# sourceMappingURL=MaterialTextBox.d.ts.map