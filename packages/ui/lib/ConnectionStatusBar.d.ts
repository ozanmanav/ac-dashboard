import { FunctionComponent } from "react";
declare type IToastProps = {
    statusText?: string;
    isDisconnected?: boolean;
};
declare const ConnectionStatusBar: FunctionComponent<IToastProps>;
export default ConnectionStatusBar;
//# sourceMappingURL=ConnectionStatusBar.d.ts.map