import React from "react";
import { IconTextField } from "./IconTextField";
import { TinyCloseButton } from "./TinyCloseButton";
import { SvgSearch } from "@appcircle/assets/lib/Search";
export function SearchField(props) {
    return (React.createElement("div", { className: "SearchField" },
        React.createElement(IconTextField, { classNameModifier: props.className, text: props.text, placeHolder: props.placeHolder, onChange: function (text) {
                props.onChange(text);
            }, icon: SvgSearch }),
        props.text !== "" && (React.createElement(TinyCloseButton, { onClick: function () {
                props.onChange("");
            } }))));
}
//# sourceMappingURL=SearchField.js.map