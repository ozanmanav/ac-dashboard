import { PropsWithChildren } from "react";
export declare type SearchFieldProps = PropsWithChildren<{
    text: string;
    placeHolder: string;
    onChange: (text: string) => void;
    className?: string;
}>;
export declare function SearchField(props: SearchFieldProps): JSX.Element;
//# sourceMappingURL=SearchField.d.ts.map