import { PropsWithChildren } from "react";
import { ButtonType } from "./Button";
export declare type TextButtonProps = PropsWithChildren<{
    title: string;
    type?: ButtonType;
    onClick?: () => void;
    disabled?: boolean;
}>;
export declare function TextButton(props: TextButtonProps): JSX.Element;
//# sourceMappingURL=TextButton.d.ts.map