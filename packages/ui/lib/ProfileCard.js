import React, { useState, useEffect, useMemo, useCallback } from "react";
import { Link } from "react-router-dom";
import { ThreeDotsToggle } from "./ThreeDotsToggle";
import { TextField } from "./TextField";
import { isValidName } from "@appcircle/core/lib/utils/isValidName";
import { NameValidState } from "@appcircle/core/lib/enums/NameValidState";
import classNames from "classnames";
import { useTaskManager } from "@appcircle/taskmanager/lib/TaskManagerContext";
import { Spinner } from "./Spinner";
import { useIntl } from "react-intl";
import { textWithSpecialsMaybe } from "@appcircle/core/lib/utils/alphaNumericTextMaybe";
import { isFakeID } from "@appcircle/core/lib/utils/fakeID";
import { SvgCardPin } from "@appcircle/assets/lib/CardPin";
import { ContextMenu } from "./menus/ContextMenu";
export var ProfileCardStatus;
(function (ProfileCardStatus) {
    ProfileCardStatus[ProfileCardStatus["READY"] = 0] = "READY";
    ProfileCardStatus[ProfileCardStatus["EDITING"] = 1] = "EDITING";
    ProfileCardStatus[ProfileCardStatus["INPROGRESS"] = 2] = "INPROGRESS";
})(ProfileCardStatus || (ProfileCardStatus = {}));
export function ProfileCard(props) {
    var getStatusByEntityID = useTaskManager().getStatusByEntityID;
    var taskStatus = getStatusByEntityID(props.data.id) || {
        status: undefined
    };
    var _a = useState(props.data.name), tempText = _a[0], setTempText = _a[1];
    useEffect(function () {
        props.onEditModeChange && props.editMode && props.onEditModeChange(true);
        // eslint-disable-next-line
    }, [props.editMode]);
    // const [highlighted, setHighlighted] = useState<boolean | undefined>();
    // useEffect(() => {
    //   props.onStatusChange && props.onStatusChange(taskStatus.status);
    //   if (highlighted === false) {
    //     taskStatus.status === 'COMPLETED' && setHighlighted(true);
    //     taskStatus.status === 'COMPLETED' && setTimeout(() => setHighlighted(false), 400);
    //   }
    //   // eslint-disable-next-line
    // }, [taskStatus.status]);
    // useEffect(() => {
    //   if (highlighted === undefined) {
    //     setHighlighted(false);
    //   }
    // },
    //   [highlighted]
    // )
    var intl = useIntl();
    var onChangeEnd = useCallback(function (newProfileName, isCancel) {
        if (!newProfileName || isCancel === true) {
            props.onEditModeChange(false);
            return;
        }
        setTempText(newProfileName);
        if (props.data.isTemporary && !newProfileName) {
            props.service.delete(props.data.id, isFakeID(props.data.id));
        }
        else if ((!isFakeID(props.data.id) && // editmode false
            props.data.name === newProfileName) ||
            !newProfileName) {
            props.onEditModeChange(false);
        }
        else {
            // rename profile
            var validState = isValidName(newProfileName.toString());
            var nameExists = props.service.doesNameExist(newProfileName);
            if (validState === NameValidState.VALID && !nameExists) {
                if (props.service.rename(props.data.id, newProfileName, isFakeID(props.data.id)) !== undefined)
                    props.onEditModeChange(false);
            }
            else {
                props.onRenameError(nameExists ? NameValidState.NAME_EXISTS : validState, newProfileName);
            }
        }
    }, [props]);
    var titleMarkup = useMemo(function () { return (React.createElement(TextField, { text: isFakeID(props.data.id) && tempText === props.data.name
            ? ""
            : tempText || "", editMode: isFakeID(props.data.id) || props.editMode, multiLineMode: true, placeHolder: props.data.name, onBlurOrEscape: props.onBlur, onValidate: textWithSpecialsMaybe, onChangeEnd: onChangeEnd })); }, [
        props.data.name,
        props.onBlur,
        props.editMode,
        props.data.id,
        tempText,
        onChangeEnd
    ]);
    var body = useMemo(function () { return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "ProfileCard_head" },
            props.IconTemplate && (React.createElement("div", { className: "ProfileCard_head_icon" }, props.IconTemplate)),
            React.createElement("div", { className: "ProfileCard_head_title" }, titleMarkup)),
        props.children,
        React.createElement("div", { className: "ProfileCard_downseperator" }),
        React.createElement("div", { className: "ProfileCard_setting" }, props.footerData.map(function (item) { return (React.createElement("div", { className: "ProfileCard_setting_item", key: item.header },
            React.createElement("div", { className: classNames({
                    "ProfileCard-enabledText": item.enable
                }) },
                item.header,
                ":",
                " "),
            React.createElement("div", { className: classNames({
                    "ProfileCard-enabledText": item.enable
                }) }, item.text))); })),
        taskStatus.status === "ADDED" ||
            taskStatus.status === "IN_PROGRESS" ? (React.createElement("div", { className: "ProfileCard_overlay" },
            React.createElement(Spinner, null))) : null)); }, [
        props.IconTemplate,
        props.children,
        props.footerData,
        taskStatus.status,
        titleMarkup
    ]);
    return (React.createElement("div", { className: classNames("ProfileCard", {
            "ProfileCard-inprogress": taskStatus.status === "IN_PROGRESS" || taskStatus.status === "ADDED",
            "ProfileCard-inEditMode": props.editMode,
            "ProfileCard-highlighted": props.highlighted
        }) },
        props.data.isPinned ? (React.createElement(SvgCardPin, { onClick: function () { return props.onContextMenuItemSelected(0); }, className: "ProfileCard_pin" })) : null,
        React.createElement("div", { className: "ProfileCard_rightTop" },
            React.createElement(ThreeDotsToggle, null,
                React.createElement(ContextMenu, { items: props.contextMenuItems, onMenuItemSelected: props.onContextMenuItemSelected }))),
        !props.editMode ? (React.createElement(Link, { to: props.profileDetailLink }, body)) : (body)));
}
//# sourceMappingURL=ProfileCard.js.map