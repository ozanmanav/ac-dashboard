import React, { PropsWithChildren } from "react";
import { ButtonType } from "./Button";
export declare enum SubmitButtonState {
    DISABLED = 0,
    ENABLED = 1,
    INPROGRESS = 2
}
export declare type SubmitButtonProps = PropsWithChildren<{
    className?: string;
    state?: SubmitButtonState;
    type?: ButtonType;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    ariaLabel?: string;
    inProgressText?: string;
    formID?: string;
    showFocus?: boolean;
    tabIndex?: number;
}>;
export declare function SubmitButton(props: SubmitButtonProps): JSX.Element;
//# sourceMappingURL=SubmitButton.d.ts.map