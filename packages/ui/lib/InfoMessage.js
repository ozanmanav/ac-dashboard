import React from "react";
export function InfoMessage(props) {
    return React.createElement("div", { className: "InfoMessage" }, props.children);
}
//# sourceMappingURL=InfoMessage.js.map