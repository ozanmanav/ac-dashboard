import React from "react";
import classNames from "classnames";
import { SvgCloseTag } from "@appcircle/assets/lib/CloseTag";
export function TagComponent(props) {
    var Icon = props.icon;
    return (React.createElement("div", { className: classNames("TagComponent", props.className, {
            "TagComponent-disabled": props.isDisabled
        }), onClick: props.onClick },
        Icon ? React.createElement(Icon, { className: "TagComponent_icon" }) : null,
        React.createElement("span", null, props.text),
        props.modifiable && props.onClose ? (React.createElement(SvgCloseTag, { className: "TagComponent_close", onClick: function () { return props.onClose && props.onClose(props.id); } })) : null));
}
//# sourceMappingURL=TagComponent.js.map