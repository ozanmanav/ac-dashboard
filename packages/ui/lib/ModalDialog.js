import React, { useState, useEffect } from "react";
import classNames from "classnames";
import { useSpring, animated } from "react-spring";
import { SvgClose } from "@appcircle/assets/lib/Close";
// const CloseIcon = icons.close;
export function ModalDialogHeader(props) {
    return (React.createElement("div", { className: "ModalDialogHeader ModalDialogHeader-align--" +
            (props.align || "center") }, props.children));
}
export function ModalDialog(props) {
    var _a = useState(false), close = _a[0], setClose = _a[1];
    useEffect(function () {
        if (close) {
            setTimeout(function () {
                props.onModalClose && props.onModalClose();
            }, 200);
            set({ opacity: 0, transform: "scale(0.95)" });
        }
        // eslint-disable-next-line
    }, [close, props.onModalClose]);
    var _b = useSpring(function () { return ({
        config: { mass: 2, tension: 1000, friction: 70 },
        opacity: 1,
        transform: "scale(1)",
        from: { opacity: 0, transform: "scale(1.03)" }
    }); }), animProps = _b[0], set = _b[1];
    return (React.createElement(animated.div, { style: animProps, className: "ModalDialog-container" },
        React.createElement("div", { className: "modalContainer_close" },
            React.createElement(SvgClose, { onClick: function () { return setClose(true); } }),
            React.createElement("div", null, "ESC")),
        React.createElement("div", { className: classNames("ModalDialog", props.className) },
            React.createElement("header", { className: classNames("ModalDialog_header", {
                    "ModalDialog_header-string": typeof props.header === "string"
                }) }, props.header),
            React.createElement("div", { className: "ModalDialog_content" },
                " ",
                props.children))));
}
//# sourceMappingURL=ModalDialog.js.map