import React from "react";
import classNames from "classnames";
import { Button, ButtonType } from "./Button";
export var IconButtonState;
(function (IconButtonState) {
    IconButtonState[IconButtonState["DISABLED"] = 0] = "DISABLED";
    IconButtonState[IconButtonState["ENABLED"] = 1] = "ENABLED";
})(IconButtonState || (IconButtonState = {}));
//TODO: Refactor this too using  new Button
export function IconButton(props) {
    var Icon = props.icon;
    return (React.createElement(Button, { size: props.size || "sm", className: classNames("IconButton", props.className, {
            "IconButton-disabled": props.state === IconButtonState.DISABLED
        }), tabIndex: props.tabIndex, onClick: props.onClick, type: ButtonType.WARNING },
        React.createElement(Icon, { className: "IconButton_icon" }),
        props.children ? (React.createElement("span", { className: "IconButton_text" },
            " ",
            props.children,
            " ")) : null));
}
//# sourceMappingURL=IconButton.js.map