import React from "react";
export function FlatHeader(props) {
    return React.createElement("div", { className: "FlatHeader" }, props.children);
}
//# sourceMappingURL=FlatHeader.js.map