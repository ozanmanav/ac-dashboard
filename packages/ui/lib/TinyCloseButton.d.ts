/// <reference types="react" />
import { ComponentStyle } from "@appcircle/core/lib/ComponentStyle";
declare type TinyCloseButtonProps = {
    onClick: () => void;
    type?: ComponentStyle;
};
export declare function TinyCloseButton(props: TinyCloseButtonProps): JSX.Element;
export {};
//# sourceMappingURL=TinyCloseButton.d.ts.map