import React, { useCallback } from "react";
import classNames from "classnames";
import { SvgHamburger } from "@appcircle/assets/lib/Hamburger";
import { IconButtonState } from "./IconButton";
import { AddButton } from "./AddButton";
export function GroupsDetailLayout(props) {
    return (React.createElement("div", { className: "GroupsDetailLayout" },
        React.createElement("div", { className: classNames("GroupsDetailLayout_sideBar", {
                "GroupsDetailLayout_sideBar-collapsed": !props.mobileMode
            }) },
            React.createElement("div", { className: "GroupsDetailLayout_header" },
                React.createElement("div", { className: "GroupsDetailLayout_title" }, props.title),
                React.createElement(AddButton, { text: "", state: props.newButtonState || IconButtonState.ENABLED, onClick: props.onNew })),
            props.sideBar),
        React.createElement("div", { className: "GroupsDetailLayout_detail" }, props.detail),
        React.createElement("div", { className: classNames("leftBarHandler", {
                "leftBarHandler-show": !props.mobileMode
            }), onClick: useCallback(function () {
                props.onMobileModeChange(true);
            }, [props.onMobileModeChange]) },
            React.createElement(SvgHamburger, null))));
}
//# sourceMappingURL=GroupsDetailLayout.js.map