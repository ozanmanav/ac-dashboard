import { PropsWithChildren } from "react";
export declare type SpinnerWithTextProps = PropsWithChildren<{
    text: string;
}>;
export declare function SpinnerWithText(props: SpinnerWithTextProps): JSX.Element;
//# sourceMappingURL=SpinnerWithText.d.ts.map