FROM node:12.16.1 as build
WORKDIR /src
COPY . .
RUN yarn --frozen-lockfile
RUN yarn bootstrap
RUN yarn build:web:docker

FROM nginx:1.17.6-alpine
COPY --from=build /src/packages/web/build/ /usr/share/nginx/html
