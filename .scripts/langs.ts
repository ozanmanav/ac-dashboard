import ts from "typescript";
import glob from "glob";
import pify from "pify";
const merge = require("lodash.merge");
const mergeWith = require("lodash.mergewith");

const pattern: string = "src/**/*.tsx";
const locales: string[] = ["en", "ja"];

async function readFiles() {
  const files: string[] = await pify(glob)(pattern);
  if (files.length === 0) {
    throw new Error(`File not found (${pattern})`);
  }

  const arr = await Promise.all(
    files.filter(file => file.endsWith("messages.tsx")).map(parseFiles)
  );
}

function parseFiles(filePath: string) {
  // create a program instance, which is a collection of source files
  // in this case we only have one source file
  const program = ts.createProgram([filePath], {});

  // pull off the typechecker instance from our program
  const checker = program.getTypeChecker();

  // get our models.ts source file AST
  const source: ts.SourceFile | undefined = program.getSourceFile(filePath);
  if (!source) return;

  // create TS printer instance which gives us utilities to pretty print our final AST
  const printer: ts.Printer = ts.createPrinter();

  // helper to give us Node string type given kind
  // const syntaxToKind = kind => {
  //   return ts.SyntaxKind[kind];
  // };
  const transformer = <T extends ts.Node>(
    context: ts.TransformationContext
  ) => (rootNode: T) => {
    function visit(node: ts.Node): ts.Node {
      switch (node.kind) {
        case ts.SyntaxKind.VariableDeclaration:
          const varDef = node as ts.VariableStatement;
          (node as ts.VariableDeclaration).name;

          console.log(
            "Visiting ",
            checker.getTypeAtLocation(node).symbol.declarations[0].getText()
            // (node as ts.VariableDeclaration).name.getText()
          );
          break;
        case ts.SyntaxKind.TypeLiteral:
          console.log("Visiting ");
          console.log(node.getFullText());
          break;
      }
      // if (node.kind)
      return ts.visitEachChild(node, visit, context);
    }
    return ts.visitNode(rootNode, visit);
  };

  // ts.transform(source, [transformer]);
  // visit each node in the root AST and log its kind
  ts.forEachChild(source, node => {
    // transformer<any>(node);
    const result: ts.TransformationResult<ts.Node> = ts.transform(node, [
      transformer
    ]);

    // console.log(node);
  });
}

function getTypeDescriptor(
  property: ts.PropertyDeclaration | ts.PropertySignature,
  typeChecker: ts.TypeChecker
): any {
  const { type } = property;

  if (!type) {
    if (property.initializer) {
      switch (property.initializer.kind) {
        case ts.SyntaxKind.StringLiteral:
          return ts.createLiteral("string");
        case ts.SyntaxKind.FirstLiteralToken:
          return ts.createLiteral("number");
      }
    }

    return ts.createLiteral("any");
  }

  return getDescriptor(type, typeChecker);
}

export function getDescriptor(
  type: ts.Node | undefined,
  typeChecker: ts.TypeChecker
): any {
  if (!type) {
    return ts.createLiteral("unknown");
  }

  switch (type.kind) {
    case ts.SyntaxKind.PropertySignature:
      return getDescriptor((type as ts.PropertySignature).type, typeChecker);
    case ts.SyntaxKind.StringKeyword:
      return ts.createLiteral("string");
    case ts.SyntaxKind.NumberKeyword:
      return ts.createLiteral("number");
    case ts.SyntaxKind.BooleanKeyword:
      return ts.createLiteral("boolean");
    case ts.SyntaxKind.AnyKeyword:
      return ts.createLiteral("any");
    case ts.SyntaxKind.TypeReference:
      const symbol = typeChecker.getSymbolAtLocation(
        (type as ts.TypeReferenceNode).typeName
      );
      const declaration = ((symbol && symbol.declarations) || [])[0];
      return getDescriptor(declaration, typeChecker);
    case ts.SyntaxKind.ArrayType:
      return ts.createArrayLiteral([
        getDescriptor((type as ts.ArrayTypeNode).elementType, typeChecker)
      ]);
    case ts.SyntaxKind.InterfaceDeclaration:
    case ts.SyntaxKind.TypeLiteral:
      return ts.createObjectLiteral(
        (type as ts.TypeLiteralNode).members.map(member =>
          ts.createPropertyAssignment(
            member.name || "",
            getTypeDescriptor(member as ts.PropertySignature, typeChecker)
          )
        )
      );
    default:
    // throw new Error("Unknown type " + ts.SyntaxKind[type.kind]);
  }
}
// function report(node: ts.Node, message: string, sourceFile) {
//   const { line, character } = sourceFile.getLineAndCharacterOfPosition(
//     node.getStart()
//   );
//   console.log(
//     `${sourceFile.fileName} (${line + 1},${character + 1}): ${message}`
//   );
// }

readFiles();
