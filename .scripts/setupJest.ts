// global.fetch = require("jest-fetch-mock");
// import "react-testing-library/cleanup-after-each";
// this adds jest-dom's custom assertions
// import "jest-dom/extend-expect";

declare const global: { window: {}; localStorage: typeof localStorageMock };

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn()
};
global.localStorage = localStorageMock;
global.window = { localStorage: localStorageMock };

export {};
